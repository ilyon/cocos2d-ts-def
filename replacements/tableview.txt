
        // See jsb_cocos2dx_extension_manual manual bindings for more information about this
        public setDelegate(delegate : cc.TableViewDelegate) : void;
        public setDataSource(dataSource : cc.TableViewDataSource) : void;

        public static create(dataSource: cc.TableViewDataSource, size: ctype.value_type<cc.Size>, container?: cc.Node): cc.TableView;
        
