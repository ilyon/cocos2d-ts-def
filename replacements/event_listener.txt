        // event listener type
        /**
         * The type code of unknown event listener.
         * @constant
         * @type {number}
         */
        public static UNKNOWN:number;

        /**
         * The type code of one by one touch event listener.
         * @constant
         * @type {number}
         */
        public static TOUCH_ONE_BY_ONE:number;

        /**
         * The type code of all at once touch event listener.
         * @constant
         * @type {number}
         */
        public static TOUCH_ALL_AT_ONCE:number;

        /**
         * The type code of keyboard event listener.
         * @constant
         * @type {number}
         */
        public static KEYBOARD:number;

        /**
         * The type code of mouse event listener.
         * @constant
         * @type {number}
         */
        public static MOUSE:number;

        /**
         * The type code of acceleration event listener.
         * @constant
         * @type {number}
         */
        public static ACCELERATION:number;

        ///**
        // * The type code of focus event listener.
        // * @constant
        // * @type {number}
        // */
        //public static ACCELERATION:number;

        /**
         * The type code of custom event listener.
         * @constant
         * @type {number}
         */
        public static CUSTOM:number;

        /**
         * The type code of Focus change event listener.
         * @constant
         * @type {number}
         */
        public static FOCUS:number;

        // *****************************************************************
        // *****************************************************************
        // ******IMPORTANT NOTE: *******************************************
        // This section is relay on 
        // cocos2d-x/cocos/scripting/js-bindings/manual/cocos2d_specifics.cpp
        // *****************************************************************
        // *****************************************************************
        
        /**
         * Currently JavaScript Bindings (JSB), in some cases, needs to use retain and release. This is a bug in JSB,
         * and the ugly workaround is to use retain/release. So, these 2 methods were added to be compatible with JSB.
         * This is a hack, and should be removed once JSB fixes the retain/release bug
         * You will need to retain an object if you created a listener and haven't added it any target node during the same frame.
         * Otherwise, JSB's native autorelease pool will consider this object a useless one and release it directly,
         * when you want to use it later, a "Invalid Native Object" error will be raised.
         * The retain function can increase a reference count for the native object to avoid it being released,
         * you need to manually invoke release function when you think this object is no longer needed, otherwise, there will be memory learks.
         * retain and release function call should be paired in developer's game code.
         * @function
         * @see cc.EventListener#release
         */
        retain():void;

        
        /**
         * Currently JavaScript Bindings (JSB), in some cases, needs to use retain and release. This is a bug in JSB,
         * and the ugly workaround is to use retain/release. So, these 2 methods were added to be compatible with JSB.
         * This is a hack, and should be removed once JSB fixes the retain/release bug
         * You will need to retain an object if you created a listener and haven't added it any target node during the same frame.
         * Otherwise, JSB's native autorelease pool will consider this object a useless one and release it directly,
         * when you want to use it later, a "Invalid Native Object" error will be raised.
         * The retain function can increase a reference count for the native object to avoid it being released,
         * you need to manually invoke release function when you think this object is no longer needed, otherwise, there will be memory learks.
         * retain and release function call should be paired in developer's game code.
         * @function
         * @see cc.EventListener#retain
         */
        public release():void;
        
        // *****************************************************************
        // *****************************************************************
        // *****************************************************************
        // *****************************************************************
        // *****************************************************************
        