        /**
         * Get this action speed.
         * @return {Number}
         */
        public getSpeed():number;

        /**
         * Set this action speed.
         * @param {Number} speed
         * @returns {cc.ActionInterval}
         */
        public setSpeed(speed:number):ActionInterval;
        
        
        // *****************************************************************
        // *****************************************************************
        // ******IMPORTANT NOTE: *******************************************
        // This section is relay on 
        // cocos2d-x/cocos/scripting/js-bindings/manual/cocos2d_specifics.cpp
        // *****************************************************************
        // *****************************************************************
        
        
        /**
         * Repeats an action a number of times.
         * To repeat an action forever use the CCRepeatForever action.
         * @param times
         * @returns {cc.ActionInterval}
         */
        public repeat(times:number):ActionInterval;

        /**
         * Repeats an action for ever.  <br/>
         * To repeat the an action for a limited number of times use the Repeat action. <br/>
         * @returns {cc.ActionInterval}
         */
        public repeatForever():ActionInterval;
        
        /**
         * Implementation of ease motion.
         *
         * @example
         * //example
         * action.easeing(cc.easeIn(3.0));
         * @param {Object} easeObj
         * @returns {cc.ActionInterval}
         */
        // TODO: Shouldn't this parameter type be ActionEase instead of any?
        public easing(easeObj:any):ActionInterval;
        
        /**
         * Changes the speed of an action, making it take longer (speed>1)
         * or less (speed<1) time. <br/>
         * Useful to simulate 'slow motion' or 'fast forward' effect.
         *
         * @param speed
         * @returns {cc.Action}
         */
        public speed(speed:number):ActionInterval;

        // *****************************************************************
        // *****************************************************************
        // *****************************************************************
        // *****************************************************************
        // *****************************************************************
        // *****************************************************************
        
        