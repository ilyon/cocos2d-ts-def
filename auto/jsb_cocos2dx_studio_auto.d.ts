/**
 * @module cocos2dx_studio
 */
declare namespace ccs {
    /**
     * @class ActionObject
     * @native
     */
    export class ActionObject 
    {

        /**
         * Sets the current time of frame.<br>
         * -- param fTime   the current time of frame
         * @method setCurrentTime
         * @param {number} _fTime
         */
        public setCurrentTime (
            _fTime : number 
        ) : void;

        /**
         * Pause the action.
         * @method pause
         */
        public pause (
        ) : void;

        /**
         * Sets name for object<br>
         * -- param name    name of object
         * @method setName
         * @param {string} _name
         */
        public setName (
            _name : string 
        ) : void;

        /**
         * Sets the time interval of frame.<br>
         * -- param fTime   the time interval of frame
         * @method setUnitTime
         * @param {number} _fTime
         */
        public setUnitTime (
            _fTime : number 
        ) : void;

        /**
         * Gets the total time of frame.<br>
         * -- return the total time of frame
         * @method getTotalTime
         * @return {number}
         */
        public getTotalTime (
        ) : number;

        /**
         * Gets name of object<br>
         * -- return name of object
         * @method getName
         * @return {string}
         */
        public getName (
        ) : string;

        /**
         * Stop the action.
         * @method stop
         */
        public stop (
        ) : void;

        /**
         * Play the action.<br>
         * -- param func Action Call Back
         * @method play
         * @param {(cc.CallFunc)} _func?
         */
        public play (
            _func? : (cc.CallFunc)
        ) : void;

        /**
         * Gets the current time of frame.<br>
         * -- return the current time of frame
         * @method getCurrentTime
         * @return {number}
         */
        public getCurrentTime (
        ) : number;

        /**
         * Removes a ActionNode which play the action.<br>
         * -- param node    the ActionNode which play the action
         * @method removeActionNode
         * @param {ccs.ActionNode} _node
         */
        public removeActionNode (
            _node : ccs.ActionNode 
        ) : void;

        /**
         * Gets if the action will loop play.<br>
         * -- return   that if the action will loop play
         * @method getLoop
         * @return {boolean}
         */
        public getLoop (
        ) : boolean;

        /**
         * 
         * @method initWithBinary
         * @param {ccs.CocoLoader} _cocoLoader
         * @param {ccs.stExpCocoNode} _pCocoNode
         * @param {cc.Ref} _root
         */
        public initWithBinary (
            _cocoLoader : ccs.CocoLoader, 
            _pCocoNode : ccs.stExpCocoNode, 
            _root : any 
        ) : void;

        /**
         * Adds a ActionNode to play the action.<br>
         * -- param node    the ActionNode which will play the action
         * @method addActionNode
         * @param {ccs.ActionNode} _node
         */
        public addActionNode (
            _node : ccs.ActionNode 
        ) : void;

        /**
         * Gets the time interval of frame.<br>
         * -- return the time interval of frame
         * @method getUnitTime
         * @return {number}
         */
        public getUnitTime (
        ) : number;

        /**
         * Return if the action is playing.<br>
         * -- return true if the action is playing, false the otherwise
         * @method isPlaying
         * @return {boolean}
         */
        public isPlaying (
        ) : boolean;

        /**
         * 
         * @method updateToFrameByTime
         * @param {number} _fTime
         */
        public updateToFrameByTime (
            _fTime : number 
        ) : void;

        /**
         * Sets if the action will loop play.<br>
         * -- param bLoop     that if the action will loop play
         * @method setLoop
         * @param {boolean} _bLoop
         */
        public setLoop (
            _bLoop : boolean 
        ) : void;

        /**
         * 
         * @method simulationActionUpdate
         * @param {number} _dt
         */
        public simulationActionUpdate (
            _dt : number 
        ) : void;

        /**
         * Default constructor
         * @method ActionObject
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class ActionManagerEx
     * @native
     */
    export abstract class ActionManager 
    {

        /**
         * Stop an Action with a name.<br>
         * -- param jsonName  UI file name<br>
         * -- param actionName  action name in the UIfile.<br>
         * -- return  ActionObject which named as the param name
         * @method stopActionByName
         * @param {string} _jsonName
         * @param {string} _actionName
         * @return {ccs.ActionObject}
         */
        public stopActionByName (
            _jsonName : string, 
            _actionName : string 
        ) : ccs.ActionObject;

        /**
         * Gets an ActionObject with a name.<br>
         * -- param jsonName  UI file name<br>
         * -- param actionName  action name in the UI file.<br>
         * -- return  ActionObject which named as the param name
         * @method getActionByName
         * @param {string} _jsonName
         * @param {string} _actionName
         * @return {ccs.ActionObject}
         */
        public getActionByName (
            _jsonName : string, 
            _actionName : string 
        ) : ccs.ActionObject;

        /**
         * 
         * @method initWithBinary
         * @param {string} _file
         * @param {cc.Ref} _root
         * @param {ccs.CocoLoader} _cocoLoader
         * @param {ccs.stExpCocoNode} _pCocoNode
         */
        public initWithBinary (
            _file : string, 
            _root : any, 
            _cocoLoader : ccs.CocoLoader, 
            _pCocoNode : ccs.stExpCocoNode 
        ) : void;

        /**
         * Play an Action with a name.<br>
         * -- param jsonName  UI file name<br>
         * -- param actionName  action name in the UIfile.<br>
         * -- param func ui action call back
         * @method playActionByName
         * @param {(string)} _jsonName
         * @param {(string)} _actionName
         * @param {(cc.CallFunc)} _func?
         * @return {ccs.ActionObject}
         */
        public playActionByName (
            _jsonName : (string), 
            _actionName : (string), 
            _func? : (cc.CallFunc)
        ) : ccs.ActionObject;

        /**
         * Release all actions.
         * @method releaseActions
         */
        public releaseActions (
        ) : void;

        /**
         * Purges ActionManager point.<br>
         * -- js purge<br>
         * -- lua destroyActionManager
         * @method destroyInstance
         */
        public static destroyInstance (
        ) : void;

        /**
         * Gets the static instance of ActionManager.<br>
         * -- js getInstance<br>
         * -- lua getInstance
         * @method getInstance
         * @return {ccs.ActionManagerEx}
         */
        public static getInstance (
        ) : ccs.ActionManagerEx;

    }
    /**
     * @class BaseData
     * @native
     */
    export class BaseData 
    {

        /**
         * 
         * @method getColor
         * @return {ctype.value_type<cc.Color>}
         */
        public getColor (
        ) : ctype.value_type<cc.Color>;

        /**
         * 
         * @method setColor
         * @param {ctype.value_type<cc.Color>} _color
         */
        public setColor (
            _color : ctype.value_type<cc.Color> 
        ) : void;

        /**
         * 
         * @method create
         * @return {ccs.BaseData}
         */
        public static create (
        ) : ccs.BaseData;

        /**
         * js ctor
         * @method BaseData
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class MovementData
     * @native
     */
    export class MovementData 
    {

        /**
         * 
         * @method getMovementBoneData
         * @param {string} _boneName
         * @return {ccs.MovementBoneData}
         */
        public getMovementBoneData (
            _boneName : string 
        ) : ccs.MovementBoneData;

        /**
         * 
         * @method addMovementBoneData
         * @param {ccs.MovementBoneData} _movBoneData
         */
        public addMovementBoneData (
            _movBoneData : ccs.MovementBoneData 
        ) : void;

        /**
         * 
         * @method create
         * @return {ccs.MovementData}
         */
        public static create (
        ) : ccs.MovementData;

        /**
         * js ctor
         * @method MovementData
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class AnimationData
     * @native
     */
    export class AnimationData 
    {

        /**
         * 
         * @method getMovement
         * @param {string} _movementName
         * @return {ccs.MovementData}
         */
        public getMovement (
            _movementName : string 
        ) : ccs.MovementData;

        /**
         * 
         * @method getMovementCount
         * @return {number}
         */
        public getMovementCount (
        ) : number;

        /**
         * 
         * @method addMovement
         * @param {ccs.MovementData} _movData
         */
        public addMovement (
            _movData : ccs.MovementData 
        ) : void;

        /**
         * 
         * @method create
         * @return {ccs.AnimationData}
         */
        public static create (
        ) : ccs.AnimationData;

        /**
         * js ctor
         * @method AnimationData
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class ContourData
     * @native
     */
    export class ContourData 
    {

        /**
         * 
         * @method init
         * @return {boolean}
         */
        public init (
        ) : boolean;

        /**
         * 
         * @method addVertex
         * @param {ctype.value_type<cc.Point>} _vertex
         */
        public addVertex (
            _vertex : ctype.value_type<cc.Point> 
        ) : void;

        /**
         * 
         * @method create
         * @return {ccs.ContourData}
         */
        public static create (
        ) : ccs.ContourData;

        /**
         * js ctor
         * @method ContourData
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class TextureData
     * @native
     */
    export class TextureData 
    {

        /**
         * 
         * @method getContourData
         * @param {number} _index
         * @return {ccs.ContourData}
         */
        public getContourData (
            _index : number 
        ) : ccs.ContourData;

        /**
         * 
         * @method init
         * @return {boolean}
         */
        public init (
        ) : boolean;

        /**
         * 
         * @method addContourData
         * @param {ccs.ContourData} _contourData
         */
        public addContourData (
            _contourData : ccs.ContourData 
        ) : void;

        /**
         * 
         * @method create
         * @return {ccs.TextureData}
         */
        public static create (
        ) : ccs.TextureData;

        /**
         * js ctor
         * @method TextureData
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class ProcessBase
     * @native
     */
    export class ProcessBase 
    {

        /**
         * Play animation by animation name.<br>
         * -- param  durationTo The frames between two animation changing-over.<br>
         * -- It's meaning is changing to this animation need how many frames<br>
         * -- -1 : use the value from MovementData get from flash design panel<br>
         * -- param  durationTween  The frame count you want to play in the game.<br>
         * -- if  _durationTween is 80, then the animation will played 80 frames in a loop<br>
         * -- -1 : use the value from MovementData get from flash design panel<br>
         * -- param  loop   Whether the animation is loop<br>
         * -- loop < 0 : use the value from MovementData get from flash design panel<br>
         * -- loop = 0 : this animation is not loop<br>
         * -- loop > 0 : this animation is loop<br>
         * -- param  tweenEasing Tween easing is used for calculate easing effect<br>
         * -- TWEEN_EASING_MAX : use the value from MovementData get from flash design panel<br>
         * -- -1 : fade out<br>
         * -- 0  : line<br>
         * -- 1  : fade in<br>
         * -- 2  : fade in and out
         * @method play
         * @param {number} _durationTo
         * @param {number} _durationTween
         * @param {number} _loop
         * @param {number} _tweenEasing
         */
        public play (
            _durationTo : number, 
            _durationTween : number, 
            _loop : number, 
            _tweenEasing : number 
        ) : void;

        /**
         * Pause the Process
         * @method pause
         */
        public pause (
        ) : void;

        /**
         * 
         * @method getRawDuration
         * @return {number}
         */
        public getRawDuration (
        ) : number;

        /**
         * Resume the Process
         * @method resume
         */
        public resume (
        ) : void;

        /**
         * 
         * @method setIsComplete
         * @param {boolean} _complete
         */
        public setIsComplete (
            _complete : boolean 
        ) : void;

        /**
         * Stop the Process
         * @method stop
         */
        public stop (
        ) : void;

        /**
         * You should never call this function, unless you know what you do<br>
         * -- Update the Process, include current process, current frame and son on<br>
         * -- param The duration since last update
         * @method update
         * @param {number} _dt
         */
        public update (
            _dt : number 
        ) : void;

        /**
         * 
         * @method getCurrentFrameIndex
         * @return {number}
         */
        public getCurrentFrameIndex (
        ) : number;

        /**
         * 
         * @method isComplete
         * @return {boolean}
         */
        public isComplete (
        ) : boolean;

        /**
         * 
         * @method getCurrentPercent
         * @return {number}
         */
        public getCurrentPercent (
        ) : number;

        /**
         * 
         * @method setIsPause
         * @param {boolean} _pause
         */
        public setIsPause (
            _pause : boolean 
        ) : void;

        /**
         * 
         * @method getProcessScale
         * @return {number}
         */
        public getProcessScale (
        ) : number;

        /**
         * 
         * @method isPause
         * @return {boolean}
         */
        public isPause (
        ) : boolean;

        /**
         * 
         * @method isPlaying
         * @return {boolean}
         */
        public isPlaying (
        ) : boolean;

        /**
         * 
         * @method setProcessScale
         * @param {number} _processScale
         */
        public setProcessScale (
            _processScale : number 
        ) : void;

        /**
         * 
         * @method setIsPlaying
         * @param {boolean} _playing
         */
        public setIsPlaying (
            _playing : boolean 
        ) : void;

        /**
         * 
         * @method ProcessBase
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class Tween
     * @native
     */
    export class Tween 
        extends ccs.ProcessBase
    {

        /**
         * 
         * @method getAnimation
         * @return {ccs.ArmatureAnimation}
         */
        public getAnimation (
        ) : ccs.ArmatureAnimation;

        /**
         * 
         * @method gotoAndPause
         * @param {number} _frameIndex
         */
        public gotoAndPause (
            _frameIndex : number 
        ) : void;

        /**
         * Start the Process<br>
         * -- param  movementBoneData  the MovementBoneData include all FrameData<br>
         * -- param  durationTo the number of frames changing to this animation needs.<br>
         * -- param  durationTween  the number of frames this animation actual last.<br>
         * -- param  loop   whether the animation is loop<br>
         * -- loop < 0 : use the value from MovementData get from Action Editor<br>
         * -- loop = 0 : this animation is not loop<br>
         * -- loop > 0 : this animation is loop<br>
         * -- param  tweenEasing    tween easing is used for calculate easing effect<br>
         * -- TWEEN_EASING_MAX : use the value from MovementData get from Action Editor<br>
         * -- -1 : fade out<br>
         * -- 0  : line<br>
         * -- 1  : fade in<br>
         * -- 2  : fade in and out
         * @method play
         * @param {ccs.MovementBoneData} _movementBoneData
         * @param {number} _durationTo
         * @param {number} _durationTween
         * @param {number} _loop
         * @param {number} _tweenEasing
         */
        public play (
            _movementBoneData : ccs.MovementBoneData, 
            _durationTo : number, 
            _durationTween : number, 
            _loop : number, 
            _tweenEasing : number 
        ) : void;

        /**
         * 
         * @method gotoAndPlay
         * @param {number} _frameIndex
         */
        public gotoAndPlay (
            _frameIndex : number 
        ) : void;

        /**
         * Init with a Bone<br>
         * -- param bone the Bone Tween will bind to
         * @method init
         * @param {ccs.Bone} _bone
         * @return {boolean}
         */
        public init (
            _bone : ccs.Bone 
        ) : boolean;

        /**
         * 
         * @method setAnimation
         * @param {ccs.ArmatureAnimation} _animation
         */
        public setAnimation (
            _animation : ccs.ArmatureAnimation 
        ) : void;

        /**
         * Create with a Bone<br>
         * -- param bone the Bone Tween will bind to
         * @method create
         * @param {ccs.Bone} _bone
         * @return {ccs.Tween}
         */
        public static create (
            _bone : ccs.Bone 
        ) : ccs.Tween;

        /**
         * 
         * @method Tween
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class ColliderFilter
     * @native
     */
    export abstract class ColliderFilter 
    {

    }
    /**
     * @class ColliderBody
     * @native
     */
    export abstract class ColliderBody 
    {

    }
    /**
     * @class ColliderDetector
     * @native
     */
    export abstract class ColliderDetector 
    {

        /**
         * 
         * @method getBone
         * @return {ccs.Bone}
         */
        public getBone (
        ) : ccs.Bone;

        /**
         * 
         * @method getActive
         * @return {boolean}
         */
        public getActive (
        ) : boolean;

        /**
         * 
         * @method getColliderBodyList
         * @return {ccs.ColliderBody[]}
         */
        public getColliderBodyList (
        ) : ccs.ColliderBody[];

        /**
         * 
         * @method updateTransform
         * @param {ctype.value_type<cc.Mat4>} _t
         */
        public updateTransform (
            _t : ctype.value_type<cc.Mat4> 
        ) : void;

        /**
         * 
         * @method removeAll
         */
        public removeAll (
        ) : void;

        /**
         * 
         * @method init
         * @param {(ccs.Bone)} _bone?
         * @return {boolean}
         */
        public init (
            _bone? : (ccs.Bone)
        ) : boolean;

        /**
         * 
         * @method setActive
         * @param {boolean} _active
         */
        public setActive (
            _active : boolean 
        ) : void;

        /**
         * 
         * @method setBone
         * @param {ccs.Bone} _bone
         */
        public setBone (
            _bone : ccs.Bone 
        ) : void;

        /**
         * 
         * @method create
         * @param {(ccs.Bone)} _bone?
         * @return {ccs.ColliderDetector}
         */
        public static create (
            _bone? : (ccs.Bone)
        ) : ccs.ColliderDetector;

    }
    /**
     * @class DecorativeDisplay
     * @native
     */
    export abstract class DecorativeDisplay 
    {

        /**
         * 
         * @method getColliderDetector
         * @return {ccs.ColliderDetector}
         */
        public getColliderDetector (
        ) : ccs.ColliderDetector;

        /**
         * 
         * @method getDisplay
         * @return {cc.Node}
         */
        public getDisplay (
        ) : cc.Node;

        /**
         * 
         * @method setDisplay
         * @param {cc.Node} _display
         */
        public setDisplay (
            _display : cc.Node 
        ) : void;

        /**
         * 
         * @method init
         * @return {boolean}
         */
        public init (
        ) : boolean;

        /**
         * 
         * @method setDisplayData
         * @param {ccs.DisplayData} _data
         */
        public setDisplayData (
            _data : ccs.DisplayData 
        ) : void;

        /**
         * 
         * @method getDisplayData
         * @return {ccs.DisplayData}
         */
        public getDisplayData (
        ) : ccs.DisplayData;

        /**
         * 
         * @method setColliderDetector
         * @param {ccs.ColliderDetector} _detector
         */
        public setColliderDetector (
            _detector : ccs.ColliderDetector 
        ) : void;

        /**
         * 
         * @method create
         * @return {ccs.DecorativeDisplay}
         */
        public static create (
        ) : ccs.DecorativeDisplay;

    }
    /**
     * @class DisplayManager
     * @native
     */
    export class DisplayManager 
    {

        /**
         * 
         * @method getCurrentDecorativeDisplay
         * @return {ccs.DecorativeDisplay}
         */
        public getCurrentDecorativeDisplay (
        ) : ccs.DecorativeDisplay;

        /**
         * 
         * @method getDisplayRenderNode
         * @return {cc.Node}
         */
        public getDisplayRenderNode (
        ) : cc.Node;

        /**
         * 
         * @method getAnchorPointInPoints
         * @return {ctype.value_type<cc.Point>}
         */
        public getAnchorPointInPoints (
        ) : ctype.value_type<cc.Point>;

        /**
         * 
         * @method setCurrentDecorativeDisplay
         * @param {ccs.DecorativeDisplay} _decoDisplay
         */
        public setCurrentDecorativeDisplay (
            _decoDisplay : ccs.DecorativeDisplay 
        ) : void;

        /**
         * 
         * @method getDisplayRenderNodeType
         * @return {number}
         */
        public getDisplayRenderNodeType (
        ) : number;

        /**
         * 
         * @method removeDisplay
         * @param {number} _index
         */
        public removeDisplay (
            _index : number 
        ) : void;

        /**
         * 
         * @method setForceChangeDisplay
         * @param {boolean} _force
         */
        public setForceChangeDisplay (
            _force : boolean 
        ) : void;

        /**
         * 
         * @method init
         * @param {ccs.Bone} _bone
         * @return {boolean}
         */
        public init (
            _bone : ccs.Bone 
        ) : boolean;

        /**
         * 
         * @method getContentSize
         * @return {ctype.value_type<cc.Size>}
         */
        public getContentSize (
        ) : ctype.value_type<cc.Size>;

        /**
         * 
         * @method getBoundingBox
         * @return {ctype.value_type<cc.Rect>}
         */
        public getBoundingBox (
        ) : ctype.value_type<cc.Rect>;

        /**
         * 
         * @method addDisplay
         * @param {(cc.Node) | (ccs.DisplayData)} _display | displayData
         * @param {(number)} _index
         */
        public addDisplay (
            _display_displayData : (cc.Node) | (ccs.DisplayData), 
            _index : (number)
        ) : void;

        /**
         * Check if the position is inside the bone.
         * @method containPoint
         * @param {(number) | (ctype.value_type<cc.Point>)} _x | _point
         * @param {(number)} _y?
         * @return {boolean}
         */
        public containPoint (
            _x__point : (number) | (ctype.value_type<cc.Point>), 
            _y? : (number)
        ) : boolean;

        /**
         * Use BoneData to init the display list.<br>
         * -- If display is a sprite, and it have texture info in the TexutreData, then use TexutreData to init the display's anchor point<br>
         * -- If the display is a Armature, then create a new Armature
         * @method initDisplayList
         * @param {ccs.BoneData} _boneData
         */
        public initDisplayList (
            _boneData : ccs.BoneData 
        ) : void;

        /**
         * Change display by index. You can just use this method to change display in the display list.<br>
         * -- The display list is just used for this bone, and it is the displays you may use in every frame.<br>
         * -- Note : if index is the same with prev index, the method will not effect<br>
         * -- param index The index of the display you want to change<br>
         * -- param force If true, then force change display to specified display, or current display will set to  display index edit in the flash every key frame.
         * @method changeDisplayWithIndex
         * @param {number} _index
         * @param {boolean} _force
         */
        public changeDisplayWithIndex (
            _index : number, 
            _force : boolean 
        ) : void;

        /**
         * 
         * @method changeDisplayWithName
         * @param {string} _name
         * @param {boolean} _force
         */
        public changeDisplayWithName (
            _name : string, 
            _force : boolean 
        ) : void;

        /**
         * 
         * @method isForceChangeDisplay
         * @return {boolean}
         */
        public isForceChangeDisplay (
        ) : boolean;

        /**
         * 
         * @method getDecorativeDisplayByIndex
         * @param {number} _index
         * @return {ccs.DecorativeDisplay}
         */
        public getDecorativeDisplayByIndex (
            _index : number 
        ) : ccs.DecorativeDisplay;

        /**
         * 
         * @method getCurrentDisplayIndex
         * @return {number}
         */
        public getCurrentDisplayIndex (
        ) : number;

        /**
         * 
         * @method getAnchorPoint
         * @return {ctype.value_type<cc.Point>}
         */
        public getAnchorPoint (
        ) : ctype.value_type<cc.Point>;

        /**
         * 
         * @method getDecorativeDisplayList
         * @return {ccs.DecorativeDisplay[]}
         */
        public getDecorativeDisplayList (
        ) : ccs.DecorativeDisplay[];

        /**
         * Determines if the display is visible<br>
         * -- see setVisible(bool)<br>
         * -- return true if the node is visible, false if the node is hidden.
         * @method isVisible
         * @return {boolean}
         */
        public isVisible (
        ) : boolean;

        /**
         * Sets whether the display is visible<br>
         * -- The default value is true, a node is default to visible<br>
         * -- param visible   true if the node is visible, false if the node is hidden.
         * @method setVisible
         * @param {boolean} _visible
         */
        public setVisible (
            _visible : boolean 
        ) : void;

        /**
         * 
         * @method create
         * @param {ccs.Bone} _bone
         * @return {ccs.DisplayManager}
         */
        public static create (
            _bone : ccs.Bone 
        ) : ccs.DisplayManager;

        /**
         * 
         * @method DisplayManager
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class Bone
     * @native
     */
    export class Bone 
        extends cc.Node
    {

        /**
         * 
         * @method isTransformDirty
         * @return {boolean}
         */
        public isTransformDirty (
        ) : boolean;

        /**
         * 
         * @method setBlendFunc
         * @param {cc.BlendFunc} _blendFunc
         */
        public setBlendFunc (
            _blendFunc : cc.BlendFunc 
        ) : void;

        /**
         * 
         * @method isIgnoreMovementBoneData
         * @return {boolean}
         */
        public isIgnoreMovementBoneData (
        ) : boolean;

        /**
         *  Update zorder
         * @method updateZOrder
         */
        public updateZOrder (
        ) : void;

        /**
         * 
         * @method getDisplayRenderNode
         * @return {cc.Node}
         */
        public getDisplayRenderNode (
        ) : cc.Node;

        /**
         * 
         * @method isBlendDirty
         * @return {boolean}
         */
        public isBlendDirty (
        ) : boolean;

        /**
         * Add a child to this bone, and it will let this child call setParent(Bone *parent) function to set self to it's parent<br>
         * -- param     child  the child you want to add
         * @method addChildBone
         * @param {ccs.Bone} _child
         */
        public addChildBone (
            _child : ccs.Bone 
        ) : void;

        /**
         * 
         * @method getWorldInfo
         * @return {ccs.BaseData}
         */
        public getWorldInfo (
        ) : ccs.BaseData;

        /**
         * 
         * @method getTween
         * @return {ccs.Tween}
         */
        public getTween (
        ) : ccs.Tween;

        /**
         * Get parent bone<br>
         * -- return parent bone
         * @method getParentBone
         * @return {ccs.Bone}
         */
        public getParentBone (
        ) : ccs.Bone;

        /**
         *  Update color to render display
         * @method updateColor
         */
        public updateColor (
        ) : void;

        /**
         * 
         * @method setTransformDirty
         * @param {boolean} _dirty
         */
        public setTransformDirty (
            _dirty : boolean 
        ) : void;

        /**
         * 
         * @method getDisplayRenderNodeType
         * @return {number}
         */
        public getDisplayRenderNodeType (
        ) : number;

        /**
         * 
         * @method removeDisplay
         * @param {number} _index
         */
        public removeDisplay (
            _index : number 
        ) : void;

        /**
         * 
         * @method setBoneData
         * @param {ccs.BoneData} _boneData
         */
        public setBoneData (
            _boneData : ccs.BoneData 
        ) : void;

        /**
         * Initializes a Bone with the specified name<br>
         * -- param name Bone's name.
         * @method init
         * @param {string} _name
         * @return {boolean}
         */
        public init (
            _name : string 
        ) : boolean;

        /**
         * Set parent bone.<br>
         * -- If parent is NUll, then also remove this bone from armature.<br>
         * -- It will not set the Armature, if you want to add the bone to a Armature, you should use Armature::addBone(Bone *bone, const char* parentName).<br>
         * -- param parent  the parent bone.<br>
         * -- nullptr : remove this bone from armature
         * @method setParentBone
         * @param {ccs.Bone} _parent
         */
        public setParentBone (
            _parent : ccs.Bone 
        ) : void;

        /**
         * 
         * @method addDisplay
         * @param {(cc.Node) | (ccs.DisplayData)} _display | displayData
         * @param {(number)} _index
         */
        public addDisplay (
            _display_displayData : (cc.Node) | (ccs.DisplayData), 
            _index : (number)
        ) : void;

        /**
         * lua NA
         * @method setIgnoreMovementBoneData
         * @param {boolean} _ignore
         */
        public setIgnoreMovementBoneData (
            _ignore : boolean 
        ) : void;

        /**
         * 
         * @method getBlendFunc
         * @return {cc.BlendFunc}
         */
        public getBlendFunc (
        ) : cc.BlendFunc;

        /**
         * Remove itself from its parent.<br>
         * -- param recursion    whether or not to remove childBone's display
         * @method removeFromParent
         * @param {boolean} _recursion
         */
        public removeFromParent (
            _recursion : boolean 
        ) : void;

        /**
         * 
         * @method getColliderDetector
         * @return {ccs.ColliderDetector}
         */
        public getColliderDetector (
        ) : ccs.ColliderDetector;

        /**
         * 
         * @method getChildArmature
         * @return {ccs.Armature}
         */
        public getChildArmature (
        ) : ccs.Armature;

        /**
         * 
         * @method changeDisplayWithIndex
         * @param {number} _index
         * @param {boolean} _force
         */
        public changeDisplayWithIndex (
            _index : number, 
            _force : boolean 
        ) : void;

        /**
         * 
         * @method changeDisplayWithName
         * @param {string} _name
         * @param {boolean} _force
         */
        public changeDisplayWithName (
            _name : string, 
            _force : boolean 
        ) : void;

        /**
         * 
         * @method setArmature
         * @param {ccs.Armature} _armature
         */
        public setArmature (
            _armature : ccs.Armature 
        ) : void;

        /**
         * 
         * @method setBlendDirty
         * @param {boolean} _dirty
         */
        public setBlendDirty (
            _dirty : boolean 
        ) : void;

        /**
         * Removes a child Bone<br>
         * -- param     bone   the bone you want to remove
         * @method removeChildBone
         * @param {ccs.Bone} _bone
         * @param {boolean} _recursion
         */
        public removeChildBone (
            _bone : ccs.Bone, 
            _recursion : boolean 
        ) : void;

        /**
         * 
         * @method setChildArmature
         * @param {ccs.Armature} _childArmature
         */
        public setChildArmature (
            _childArmature : ccs.Armature 
        ) : void;

        /**
         * 
         * @method getNodeToArmatureTransform
         * @return {ctype.value_type<cc.Mat4>}
         */
        public getNodeToArmatureTransform (
        ) : ctype.value_type<cc.Mat4>;

        /**
         * 
         * @method getDisplayManager
         * @return {ccs.DisplayManager}
         */
        public getDisplayManager (
        ) : ccs.DisplayManager;

        /**
         * 
         * @method getArmature
         * @return {ccs.Armature}
         */
        public getArmature (
        ) : ccs.Armature;

        /**
         * Allocates and initializes a bone.<br>
         * -- param  name If name is not null, then set name to the bone's name<br>
         * -- return A initialized bone which is marked as "autorelease".
         * @method create
         * @param {(string)} _name?
         * @return {ccs.Bone}
         */
        public static create (
            _name? : (string)
        ) : ccs.Bone;

        /**
         * js ctor
         * @method Bone
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class BatchNode
     * @native
     */
    export abstract class BatchNode 
        extends cc.Node
    {

        /**
         * 
         * @method create
         * @return {ccs.BatchNode}
         */
        public static create (
        ) : ccs.BatchNode;

    }
    /**
     * @class ArmatureAnimation
     * @native
     */
    export class ArmatureAnimation 
        extends ccs.ProcessBase
    {

        /**
         * 
         * @method getSpeedScale
         * @return {number}
         */
        public getSpeedScale (
        ) : number;

        /**
         * Play animation by animation name.<br>
         * -- param  animationName  The animation name you want to play<br>
         * -- param  durationTo The frames between two animation changing-over.<br>
         * -- It's meaning is changing to this animation need how many frames<br>
         * -- -1 : use the value from MovementData get from flash design panel<br>
         * -- param  loop   Whether the animation is loop<br>
         * -- loop < 0 : use the value from MovementData get from flash design panel<br>
         * -- loop = 0 : this animation is not loop<br>
         * -- loop > 0 : this animation is loop
         * @method play
         * @param {string} _animationName
         * @param {number} _durationTo
         * @param {number} _loop
         */
        public play (
            _animationName : string, 
            _durationTo : number, 
            _loop : number 
        ) : void;

        /**
         * Go to specified frame and pause current movement.
         * @method gotoAndPause
         * @param {number} _frameIndex
         */
        public gotoAndPause (
            _frameIndex : number 
        ) : void;

        /**
         * 
         * @method playWithIndexes
         * @param {number[]} _movementIndexes
         * @param {number} _durationTo
         * @param {boolean} _loop
         */
        public playWithIndexes (
            _movementIndexes : number[], 
            _durationTo : number, 
            _loop : boolean 
        ) : void;

        /**
         * 
         * @method setAnimationData
         * @param {ccs.AnimationData} _data
         */
        public setAnimationData (
            _data : ccs.AnimationData 
        ) : void;

        /**
         * Scale animation play speed.<br>
         * -- param animationScale Scale value
         * @method setSpeedScale
         * @param {number} _speedScale
         */
        public setSpeedScale (
            _speedScale : number 
        ) : void;

        /**
         * 
         * @method getAnimationData
         * @return {ccs.AnimationData}
         */
        public getAnimationData (
        ) : ccs.AnimationData;

        /**
         * Go to specified frame and play current movement.<br>
         * -- You need first switch to the movement you want to play, then call this function.<br>
         * -- example : playByIndex(0);<br>
         * -- gotoAndPlay(0);<br>
         * -- playByIndex(1);<br>
         * -- gotoAndPlay(0);<br>
         * -- gotoAndPlay(15);
         * @method gotoAndPlay
         * @param {number} _frameIndex
         */
        public gotoAndPlay (
            _frameIndex : number 
        ) : void;

        /**
         * Init with a Armature<br>
         * -- param armature The Armature ArmatureAnimation will bind to
         * @method init
         * @param {ccs.Armature} _armature
         * @return {boolean}
         */
        public init (
            _armature : ccs.Armature 
        ) : boolean;

        /**
         * 
         * @method playWithNames
         * @param {string[]} _movementNames
         * @param {number} _durationTo
         * @param {boolean} _loop
         */
        public playWithNames (
            _movementNames : string[], 
            _durationTo : number, 
            _loop : boolean 
        ) : void;

        /**
         * Get movement count
         * @method getMovementCount
         * @return {number}
         */
        public getMovementCount (
        ) : number;

        /**
         * 
         * @method playWithIndex
         * @param {number} _animationIndex
         * @param {number} _durationTo
         * @param {number} _loop
         */
        public playWithIndex (
            _animationIndex : number, 
            _durationTo : number, 
            _loop : number 
        ) : void;

        /**
         * Get current movementID<br>
         * -- return The name of current movement
         * @method getCurrentMovementID
         * @return {string}
         */
        public getCurrentMovementID (
        ) : string;

        /**
         * Create with a Armature<br>
         * -- param armature The Armature ArmatureAnimation will bind to
         * @method create
         * @param {ccs.Armature} _armature
         * @return {ccs.ArmatureAnimation}
         */
        public static create (
            _armature : ccs.Armature 
        ) : ccs.ArmatureAnimation;

        /**
         * js ctor
         * @method ArmatureAnimation
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class ArmatureDataManager
     * @native
     */
    export abstract class ArmatureDataManager 
    {

        /**
         * brief    remove animation data<br>
         * -- param     id the id of the animation data
         * @method removeAnimationData
         * @param {string} _id
         */
        public removeAnimationData (
            _id : string 
        ) : void;

        /**
         * Add armature data<br>
         * -- param id The id of the armature data<br>
         * -- param armatureData ArmatureData *
         * @method addArmatureData
         * @param {string} _id
         * @param {ccs.ArmatureData} _armatureData
         * @param {string} _configFilePath
         */
        public addArmatureData (
            _id : string, 
            _armatureData : ccs.ArmatureData, 
            _configFilePath : string 
        ) : void;

        /**
         * brief    Add ArmatureFileInfo, it is managed by ArmatureDataManager.
         * @method addArmatureFileInfo
         * @param {(string)} _imagePath | configFilePath
         * @param {(string)} _plistPath?
         * @param {(string)} _configFilePath?
         */
        public addArmatureFileInfo (
            _imagePath_configFilePath : (string), 
            _plistPath? : (string), 
            _configFilePath? : (string)
        ) : void;

        /**
         * 
         * @method removeArmatureFileInfo
         * @param {string} _configFilePath
         */
        public removeArmatureFileInfo (
            _configFilePath : string 
        ) : void;

        /**
         * brief    get texture data<br>
         * -- param     id the id of the texture data you want to get<br>
         * -- return TextureData *
         * @method getTextureData
         * @param {string} _id
         * @return {ccs.TextureData}
         */
        public getTextureData (
            _id : string 
        ) : ccs.TextureData;

        /**
         * brief    get armature data<br>
         * -- param    id the id of the armature data you want to get<br>
         * -- return    ArmatureData *
         * @method getArmatureData
         * @param {string} _id
         * @return {ccs.ArmatureData}
         */
        public getArmatureData (
            _id : string 
        ) : ccs.ArmatureData;

        /**
         * brief    get animation data from _animationDatas(Dictionary)<br>
         * -- param     id the id of the animation data you want to get<br>
         * -- return AnimationData *
         * @method getAnimationData
         * @param {string} _id
         * @return {ccs.AnimationData}
         */
        public getAnimationData (
            _id : string 
        ) : ccs.AnimationData;

        /**
         * brief    add animation data<br>
         * -- param     id the id of the animation data<br>
         * -- return AnimationData *
         * @method addAnimationData
         * @param {string} _id
         * @param {ccs.AnimationData} _animationData
         * @param {string} _configFilePath
         */
        public addAnimationData (
            _id : string, 
            _animationData : ccs.AnimationData, 
            _configFilePath : string 
        ) : void;

        /**
         * Init ArmatureDataManager
         * @method init
         * @return {boolean}
         */
        public init (
        ) : boolean;

        /**
         * brief    remove armature data<br>
         * -- param    id the id of the armature data you want to get
         * @method removeArmatureData
         * @param {string} _id
         */
        public removeArmatureData (
            _id : string 
        ) : void;

        /**
         * 
         * @method getArmatureDatas
         * @return {any}
         */
        public getArmatureDatas (
        ) : any;

        /**
         * brief    remove texture data<br>
         * -- param     id the id of the texture data you want to get
         * @method removeTextureData
         * @param {string} _id
         */
        public removeTextureData (
            _id : string 
        ) : void;

        /**
         * brief    add texture data<br>
         * -- param     id the id of the texture data<br>
         * -- return TextureData *
         * @method addTextureData
         * @param {string} _id
         * @param {ccs.TextureData} _textureData
         * @param {string} _configFilePath
         */
        public addTextureData (
            _id : string, 
            _textureData : ccs.TextureData, 
            _configFilePath : string 
        ) : void;

        /**
         * 
         * @method getAnimationDatas
         * @return {any}
         */
        public getAnimationDatas (
        ) : any;

        /**
         * brief    Judge whether or not need auto load sprite file
         * @method isAutoLoadSpriteFile
         * @return {boolean}
         */
        public isAutoLoadSpriteFile (
        ) : boolean;

        /**
         * brief    Add sprite frame to CCSpriteFrameCache, it will save display name and it's relative image name
         * @method addSpriteFrameFromFile
         * @param {string} _plistPath
         * @param {string} _imagePath
         * @param {string} _configFilePath
         */
        public addSpriteFrameFromFile (
            _plistPath : string, 
            _imagePath : string, 
            _configFilePath : string 
        ) : void;

        /**
         * 
         * @method destroyInstance
         */
        public static destroyInstance (
        ) : void;

        /**
         * 
         * @method getInstance
         * @return {ccs.ArmatureDataManager}
         */
        public static getInstance (
        ) : ccs.ArmatureDataManager;

    }
    /**
     * @class Armature
     * @native
     */
    export class Armature 
        extends cc.Node
    {

        /**
         * Get a bone with the specified name<br>
         * -- param name The bone's name you want to get
         * @method getBone
         * @param {string} _name
         * @return {ccs.Bone}
         */
        public getBone (
            _name : string 
        ) : ccs.Bone;

        /**
         * Change a bone's parent with the specified parent name.<br>
         * -- param bone The bone you want to change parent<br>
         * -- param parentName The new parent's name.
         * @method changeBoneParent
         * @param {ccs.Bone} _bone
         * @param {string} _parentName
         */
        public changeBoneParent (
            _bone : ccs.Bone, 
            _parentName : string 
        ) : void;

        /**
         * 
         * @method setAnimation
         * @param {ccs.ArmatureAnimation} _animation
         */
        public setAnimation (
            _animation : ccs.ArmatureAnimation 
        ) : void;

        /**
         * 
         * @method getBoneAtPoint
         * @param {number} _x
         * @param {number} _y
         * @return {ccs.Bone}
         */
        public getBoneAtPoint (
            _x : number, 
            _y : number 
        ) : ccs.Bone;

        /**
         * 
         * @method getArmatureTransformDirty
         * @return {boolean}
         */
        public getArmatureTransformDirty (
        ) : boolean;

        /**
         * 
         * @method setVersion
         * @param {number} _version
         */
        public setVersion (
            _version : number 
        ) : void;

        /**
         * Set contentsize and Calculate anchor point.
         * @method updateOffsetPoint
         */
        public updateOffsetPoint (
        ) : void;

        /**
         * 
         * @method getParentBone
         * @return {ccs.Bone}
         */
        public getParentBone (
        ) : ccs.Bone;

        /**
         * Remove a bone with the specified name. If recursion it will also remove child Bone recursionly.<br>
         * -- param bone The bone you want to remove<br>
         * -- param recursion Determine whether remove the bone's child  recursion.
         * @method removeBone
         * @param {ccs.Bone} _bone
         * @param {boolean} _recursion
         */
        public removeBone (
            _bone : ccs.Bone, 
            _recursion : boolean 
        ) : void;

        /**
         * 
         * @method getBatchNode
         * @return {ccs.BatchNode}
         */
        public getBatchNode (
        ) : ccs.BatchNode;

        /**
         * 
         * @method init
         * @param {(string)} _name
         * @param {(ccs.Bone)} _parentBone?
         * @return {boolean}
         */
        public init (
            _name : (string), 
            _parentBone? : (ccs.Bone)
        ) : boolean;

        /**
         * 
         * @method setParentBone
         * @param {ccs.Bone} _parentBone
         */
        public setParentBone (
            _parentBone : ccs.Bone 
        ) : void;

        /**
         * 
         * @method setBatchNode
         * @param {ccs.BatchNode} _batchNode
         */
        public setBatchNode (
            _batchNode : ccs.BatchNode 
        ) : void;

        /**
         * js NA<br>
         * -- lua NA
         * @method getBlendFunc
         * @return {cc.BlendFunc}
         */
        public getBlendFunc (
        ) : cc.BlendFunc;

        /**
         * 
         * @method setArmatureData
         * @param {ccs.ArmatureData} _armatureData
         */
        public setArmatureData (
            _armatureData : ccs.ArmatureData 
        ) : void;

        /**
         * Add a Bone to this Armature,<br>
         * -- param bone  The Bone you want to add to Armature<br>
         * -- param parentName   The parent Bone's name you want to add to . If it's  nullptr, then set Armature to its parent
         * @method addBone
         * @param {ccs.Bone} _bone
         * @param {string} _parentName
         */
        public addBone (
            _bone : ccs.Bone, 
            _parentName : string 
        ) : void;

        /**
         * 
         * @method getArmatureData
         * @return {ccs.ArmatureData}
         */
        public getArmatureData (
        ) : ccs.ArmatureData;

        /**
         * This boundingBox will calculate all bones' boundingBox every time
         * @method getBoundingBox
         * @return {ctype.value_type<cc.Rect>}
         */
        public boundingBox (
        ) : ctype.value_type<cc.Rect>;

        /**
         * 
         * @method getVersion
         * @return {number}
         */
        public getVersion (
        ) : number;

        /**
         * 
         * @method getAnimation
         * @return {ccs.ArmatureAnimation}
         */
        public getAnimation (
        ) : ccs.ArmatureAnimation;

        /**
         * 
         * @method getOffsetPoints
         * @return {ctype.value_type<cc.Point>}
         */
        public getOffsetPoints (
        ) : ctype.value_type<cc.Point>;

        /**
         * js NA<br>
         * -- lua NA
         * @method setBlendFunc
         * @param {cc.BlendFunc} _blendFunc
         */
        public setBlendFunc (
            _blendFunc : cc.BlendFunc 
        ) : void;

        /**
         * Get Armature's bone dictionary<br>
         * -- return Armature's bone dictionary
         * @method getBoneDic
         * @return {any}
         */
        public getBoneDic (
        ) : any;

        /**
         * Allocates an armature, and use the ArmatureData named name in ArmatureDataManager to initializes the armature.<br>
         * -- param  name Armature will use the name to find the ArmatureData to initializes it.<br>
         * -- return A initialized armature which is marked as "autorelease".
         * @method create
         * @param {(string)} _name?
         * @param {(ccs.Bone)} _parentBone?
         * @return {ccs.Armature}
         */
        public static create (
            _name? : (string), 
            _parentBone? : (ccs.Bone)
        ) : ccs.Armature;

        /**
         * js ctor
         * @method Armature
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class Skin
     * @native
     */
    export class Skin 
        extends cc.Sprite
    {

        /**
         * 
         * @method getBone
         * @return {ccs.Bone}
         */
        public getBone (
        ) : ccs.Bone;

        /**
         * 
         * @method getNodeToWorldTransformAR
         * @return {ctype.value_type<cc.Mat4>}
         */
        public getNodeToWorldTransformAR (
        ) : ctype.value_type<cc.Mat4>;

        /**
         * 
         * @method getDisplayName
         * @return {string}
         */
        public getDisplayName (
        ) : string;

        /**
         * 
         * @method updateArmatureTransform
         */
        public updateArmatureTransform (
        ) : void;

        /**
         * 
         * @method setBone
         * @param {ccs.Bone} _bone
         */
        public setBone (
            _bone : ccs.Bone 
        ) : void;

        /**
         * 
         * @method create
         * @param {(string)} _pszFileName?
         * @return {ccs.Skin}
         */
        public static create (
            _pszFileName? : (string)
        ) : ccs.Skin;

        /**
         * 
         * @method createWithSpriteFrameName
         * @param {string} _pszSpriteFrameName
         * @return {ccs.Skin}
         */
        public static createWithSpriteFrameName (
            _pszSpriteFrameName : string 
        ) : ccs.Skin;

        /**
         * js ctor
         * @method Skin
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class ComAttribute
     * @native
     */
    export class ComAttribute 
        extends cc.Component
    {

        /**
         * 
         * @method getFloat
         * @param {string} _key
         * @param {number} _def
         * @return {number}
         */
        public getFloat (
            _key : string, 
            _def : number 
        ) : number;

        /**
         * 
         * @method getBool
         * @param {string} _key
         * @param {boolean} _def
         * @return {boolean}
         */
        public getBool (
            _key : string, 
            _def : boolean 
        ) : boolean;

        /**
         * 
         * @method getString
         * @param {string} _key
         * @param {string} _def
         * @return {string}
         */
        public getString (
            _key : string, 
            _def : string 
        ) : string;

        /**
         * 
         * @method setFloat
         * @param {string} _key
         * @param {number} _value
         */
        public setFloat (
            _key : string, 
            _value : number 
        ) : void;

        /**
         * 
         * @method setString
         * @param {string} _key
         * @param {string} _value
         */
        public setString (
            _key : string, 
            _value : string 
        ) : void;

        /**
         * 
         * @method setInt
         * @param {string} _key
         * @param {number} _value
         */
        public setInt (
            _key : string, 
            _value : number 
        ) : void;

        /**
         * 
         * @method parse
         * @param {string} _jsonFile
         * @return {boolean}
         */
        public parse (
            _jsonFile : string 
        ) : boolean;

        /**
         * 
         * @method getInt
         * @param {string} _key
         * @param {number} _def
         * @return {number}
         */
        public getInt (
            _key : string, 
            _def : number 
        ) : number;

        /**
         * 
         * @method setBool
         * @param {string} _key
         * @param {boolean} _value
         */
        public setBool (
            _key : string, 
            _value : boolean 
        ) : void;

        /**
         * 
         * @method create
         * @return {ccs.ComAttribute}
         */
        public static create (
        ) : ccs.ComAttribute;

        /**
         * js ctor
         * @method ComAttribute
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class ComAudio
     * @native
     */
    export class ComAudio 
        extends cc.Component
    {

        /**
         * 
         * @method stopAllEffects
         */
        public stopAllEffects (
        ) : void;

        /**
         * 
         * @method getEffectsVolume
         * @return {number}
         */
        public getEffectsVolume (
        ) : number;

        /**
         * 
         * @method stopEffect
         * @param {number} _nSoundId
         */
        public stopEffect (
            _nSoundId : number 
        ) : void;

        /**
         * 
         * @method getBackgroundMusicVolume
         * @return {number}
         */
        public getBackgroundMusicVolume (
        ) : number;

        /**
         * 
         * @method willPlayBackgroundMusic
         * @return {boolean}
         */
        public willPlayBackgroundMusic (
        ) : boolean;

        /**
         * 
         * @method setBackgroundMusicVolume
         * @param {number} _volume
         */
        public setBackgroundMusicVolume (
            _volume : number 
        ) : void;

        /**
         * 
         * @method end
         */
        public end (
        ) : void;

        /**
         * 
         * @method stopBackgroundMusic
         * @param {(boolean)} _bReleaseData?
         */
        public stopBackgroundMusic (
            _bReleaseData? : (boolean)
        ) : void;

        /**
         * 
         * @method pauseBackgroundMusic
         */
        public pauseBackgroundMusic (
        ) : void;

        /**
         * 
         * @method isBackgroundMusicPlaying
         * @return {boolean}
         */
        public isBackgroundMusicPlaying (
        ) : boolean;

        /**
         * 
         * @method isLoop
         * @return {boolean}
         */
        public isLoop (
        ) : boolean;

        /**
         * 
         * @method resumeAllEffects
         */
        public resumeAllEffects (
        ) : void;

        /**
         * 
         * @method pauseAllEffects
         */
        public pauseAllEffects (
        ) : void;

        /**
         * 
         * @method preloadBackgroundMusic
         * @param {string} _pszFilePath
         */
        public preloadBackgroundMusic (
            _pszFilePath : string 
        ) : void;

        /**
         * 
         * @method playBackgroundMusic
         * @param {(string)} _pszFilePath?
         * @param {(boolean)} _bLoop?
         */
        public playBackgroundMusic (
            _pszFilePath? : (string), 
            _bLoop? : (boolean)
        ) : void;

        /**
         * 
         * @method playEffect
         * @param {(string)} _pszFilePath?
         * @param {(boolean)} _bLoop?
         * @return {number}
         */
        public playEffect (
            _pszFilePath? : (string), 
            _bLoop? : (boolean)
        ) : number;

        /**
         * 
         * @method preloadEffect
         * @param {string} _pszFilePath
         */
        public preloadEffect (
            _pszFilePath : string 
        ) : void;

        /**
         * 
         * @method setLoop
         * @param {boolean} _bLoop
         */
        public setLoop (
            _bLoop : boolean 
        ) : void;

        /**
         * 
         * @method unloadEffect
         * @param {string} _pszFilePath
         */
        public unloadEffect (
            _pszFilePath : string 
        ) : void;

        /**
         * 
         * @method rewindBackgroundMusic
         */
        public rewindBackgroundMusic (
        ) : void;

        /**
         * 
         * @method pauseEffect
         * @param {number} _nSoundId
         */
        public pauseEffect (
            _nSoundId : number 
        ) : void;

        /**
         * 
         * @method resumeBackgroundMusic
         */
        public resumeBackgroundMusic (
        ) : void;

        /**
         * 
         * @method setFile
         * @param {string} _pszFilePath
         */
        public setFile (
            _pszFilePath : string 
        ) : void;

        /**
         * 
         * @method setEffectsVolume
         * @param {number} _volume
         */
        public setEffectsVolume (
            _volume : number 
        ) : void;

        /**
         * 
         * @method getFile
         * @return {string}
         */
        public getFile (
        ) : string;

        /**
         * 
         * @method resumeEffect
         * @param {number} _nSoundId
         */
        public resumeEffect (
            _nSoundId : number 
        ) : void;

        /**
         * 
         * @method create
         * @return {ccs.ComAudio}
         */
        public static create (
        ) : ccs.ComAudio;

        /**
         * js ctor
         * @method ComAudio
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class InputDelegate
     * @native
     */
    export abstract class InputDelegate 
    {

        /**
         * 
         * @method isAccelerometerEnabled
         * @return {boolean}
         */
        public isAccelerometerEnabled (
        ) : boolean;

        /**
         * 
         * @method setKeypadEnabled
         * @param {boolean} _value
         */
        public setKeypadEnabled (
            _value : boolean 
        ) : void;

        /**
         * 
         * @method getTouchMode
         * @return {number}
         */
        public getTouchMode (
        ) : number;

        /**
         * 
         * @method setAccelerometerEnabled
         * @param {boolean} _value
         */
        public setAccelerometerEnabled (
            _value : boolean 
        ) : void;

        /**
         * 
         * @method isKeypadEnabled
         * @return {boolean}
         */
        public isKeypadEnabled (
        ) : boolean;

        /**
         * 
         * @method isTouchEnabled
         * @return {boolean}
         */
        public isTouchEnabled (
        ) : boolean;

        /**
         * 
         * @method setTouchPriority
         * @param {number} _priority
         */
        public setTouchPriority (
            _priority : number 
        ) : void;

        /**
         * 
         * @method getTouchPriority
         * @return {number}
         */
        public getTouchPriority (
        ) : number;

        /**
         * 
         * @method setTouchEnabled
         * @param {boolean} _value
         */
        public setTouchEnabled (
            _value : boolean 
        ) : void;

        /**
         * 
         * @method setTouchMode
         * @param {cc.Touch::DispatchMode} _mode
         */
        public setTouchMode (
            _mode : number 
        ) : void;

    }
    /**
     * @class ComController
     * @native
     */
    export class ComController 
        extends cc.Component
    {

        /**
         * 
         * @method create
         * @return {ccs.ComController}
         */
        public static create (
        ) : ccs.ComController;

        /**
         * js ctor
         * @method ComController
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class ComRender
     * @native
     */
    export class ComRender 
        extends cc.Component
    {

        /**
         * 
         * @method setNode
         * @param {cc.Node} _node
         */
        public setNode (
            _node : cc.Node 
        ) : void;

        /**
         * 
         * @method getNode
         * @return {cc.Node}
         */
        public getNode (
        ) : cc.Node;

        /**
         * 
         * @method create
         * @param {(cc.Node)} _node?
         * @param {(string)} _comName?
         * @return {ccs.ComRender}
         */
        public static create (
            _node? : (cc.Node), 
            _comName? : (string)
        ) : ccs.ComRender;

        /**
         * 
         * @method ComRender
         * @constructor
         * @param {(cc.Node)} _node?
         * @param {(string)} _comName?
         */
        public constructor (
            _node? : (cc.Node), 
            _comName? : (string)
        );

    }
    /**
     * @class Frame
     * @native
     */
    export class Frame 
    {

        /**
         * 
         * @method clone
         * @return {any}
         */
        public clone (
        ) : any;

        /**
         * 
         * @method setTweenType
         * @param {cc.tweenfunc::TweenType} _tweenType
         */
        public setTweenType (
            _tweenType : number 
        ) : void;

        /**
         * 
         * @method setNode
         * @param {cc.Node} _node
         */
        public setNode (
            _node : cc.Node 
        ) : void;

        /**
         * 
         * @method setTimeline
         * @param {ccs.timeline::Timeline} _timeline
         */
        public setTimeline (
            _timeline : any 
        ) : void;

        /**
         * 
         * @method isEnterWhenPassed
         * @return {boolean}
         */
        public isEnterWhenPassed (
        ) : boolean;

        /**
         * 
         * @method getTweenType
         * @return {number}
         */
        public getTweenType (
        ) : number;

        /**
         * 
         * @method getFrameIndex
         * @return {number}
         */
        public getFrameIndex (
        ) : number;

        /**
         * 
         * @method apply
         * @param {number} _percent
         */
        public apply (
            _percent : number 
        ) : void;

        /**
         * 
         * @method isTween
         * @return {boolean}
         */
        public isTween (
        ) : boolean;

        /**
         * 
         * @method setFrameIndex
         * @param {number} _frameIndex
         */
        public setFrameIndex (
            _frameIndex : number 
        ) : void;

        /**
         * 
         * @method setTween
         * @param {boolean} _tween
         */
        public setTween (
            _tween : boolean 
        ) : void;

        /**
         * 
         * @method getTimeline
         * @return {any}
         */
        public getTimeline (
        ) : any;

        /**
         * 
         * @method getNode
         * @return {cc.Node}
         */
        public getNode (
        ) : cc.Node;

    }
    /**
     * @class VisibleFrame
     * @native
     */
    export class VisibleFrame 
        extends Frame
    {

        /**
         * 
         * @method isVisible
         * @return {boolean}
         */
        public isVisible (
        ) : boolean;

        /**
         * 
         * @method setVisible
         * @param {boolean} _visible
         */
        public setVisible (
            _visible : boolean 
        ) : void;

        /**
         * 
         * @method create
         * @return {any}
         */
        public static create (
        ) : any;

        /**
         * 
         * @method VisibleFrame
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class TextureFrame
     * @native
     */
    export class TextureFrame 
        extends Frame
    {

        /**
         * 
         * @method getTextureName
         * @return {string}
         */
        public getTextureName (
        ) : string;

        /**
         * 
         * @method setTextureName
         * @param {string} _textureName
         */
        public setTextureName (
            _textureName : string 
        ) : void;

        /**
         * 
         * @method create
         * @return {any}
         */
        public static create (
        ) : any;

        /**
         * 
         * @method TextureFrame
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class RotationFrame
     * @native
     */
    export class RotationFrame 
        extends Frame
    {

        /**
         * 
         * @method setRotation
         * @param {number} _rotation
         */
        public setRotation (
            _rotation : number 
        ) : void;

        /**
         * 
         * @method getRotation
         * @return {number}
         */
        public getRotation (
        ) : number;

        /**
         * 
         * @method create
         * @return {any}
         */
        public static create (
        ) : any;

        /**
         * 
         * @method RotationFrame
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class SkewFrame
     * @native
     */
    export class SkewFrame 
        extends Frame
    {

        /**
         * 
         * @method getSkewY
         * @return {number}
         */
        public getSkewY (
        ) : number;

        /**
         * 
         * @method setSkewX
         * @param {number} _skewx
         */
        public setSkewX (
            _skewx : number 
        ) : void;

        /**
         * 
         * @method setSkewY
         * @param {number} _skewy
         */
        public setSkewY (
            _skewy : number 
        ) : void;

        /**
         * 
         * @method getSkewX
         * @return {number}
         */
        public getSkewX (
        ) : number;

        /**
         * 
         * @method create
         * @return {any}
         */
        public static create (
        ) : any;

        /**
         * 
         * @method SkewFrame
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class RotationSkewFrame
     * @native
     */
    export class RotationSkewFrame 
        extends SkewFrame
    {

        /**
         * 
         * @method create
         * @return {any}
         */
        public static create (
        ) : any;

        /**
         * 
         * @method RotationSkewFrame
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class PositionFrame
     * @native
     */
    export class PositionFrame 
        extends Frame
    {

        /**
         * 
         * @method getX
         * @return {number}
         */
        public getX (
        ) : number;

        /**
         * 
         * @method getY
         * @return {number}
         */
        public getY (
        ) : number;

        /**
         * 
         * @method setPosition
         * @param {ctype.value_type<cc.Point>} _position
         */
        public setPosition (
            _position : ctype.value_type<cc.Point> 
        ) : void;

        /**
         * 
         * @method setX
         * @param {number} _x
         */
        public setX (
            _x : number 
        ) : void;

        /**
         * 
         * @method setY
         * @param {number} _y
         */
        public setY (
            _y : number 
        ) : void;

        /**
         * 
         * @method getPosition
         * @return {ctype.value_type<cc.Point>}
         */
        public getPosition (
        ) : ctype.value_type<cc.Point>;

        /**
         * 
         * @method create
         * @return {any}
         */
        public static create (
        ) : any;

        /**
         * 
         * @method PositionFrame
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class ScaleFrame
     * @native
     */
    export class ScaleFrame 
        extends Frame
    {

        /**
         * 
         * @method setScaleY
         * @param {number} _scaleY
         */
        public setScaleY (
            _scaleY : number 
        ) : void;

        /**
         * 
         * @method setScaleX
         * @param {number} _scaleX
         */
        public setScaleX (
            _scaleX : number 
        ) : void;

        /**
         * 
         * @method getScaleY
         * @return {number}
         */
        public getScaleY (
        ) : number;

        /**
         * 
         * @method getScaleX
         * @return {number}
         */
        public getScaleX (
        ) : number;

        /**
         * 
         * @method setScale
         * @param {number} _scale
         */
        public setScale (
            _scale : number 
        ) : void;

        /**
         * 
         * @method create
         * @return {any}
         */
        public static create (
        ) : any;

        /**
         * 
         * @method ScaleFrame
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class AnchorPointFrame
     * @native
     */
    export class AnchorPointFrame 
        extends Frame
    {

        /**
         * 
         * @method setAnchorPoint
         * @param {ctype.value_type<cc.Point>} _point
         */
        public setAnchorPoint (
            _point : ctype.value_type<cc.Point> 
        ) : void;

        /**
         * 
         * @method getAnchorPoint
         * @return {ctype.value_type<cc.Point>}
         */
        public getAnchorPoint (
        ) : ctype.value_type<cc.Point>;

        /**
         * 
         * @method create
         * @return {any}
         */
        public static create (
        ) : any;

        /**
         * 
         * @method AnchorPointFrame
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class InnerActionFrame
     * @native
     */
    export class InnerActionFrame 
        extends Frame
    {

        /**
         * 
         * @method getEndFrameIndex
         * @return {number}
         */
        public getEndFrameIndex (
        ) : number;

        /**
         * 
         * @method getStartFrameIndex
         * @return {number}
         */
        public getStartFrameIndex (
        ) : number;

        /**
         * 
         * @method getInnerActionType
         * @return {number}
         */
        public getInnerActionType (
        ) : number;

        /**
         * 
         * @method setEndFrameIndex
         * @param {number} _frameIndex
         */
        public setEndFrameIndex (
            _frameIndex : number 
        ) : void;

        /**
         * 
         * @method setEnterWithName
         * @param {boolean} _isEnterWithName
         */
        public setEnterWithName (
            _isEnterWithName : boolean 
        ) : void;

        /**
         * 
         * @method setSingleFrameIndex
         * @param {number} _frameIndex
         */
        public setSingleFrameIndex (
            _frameIndex : number 
        ) : void;

        /**
         * 
         * @method setStartFrameIndex
         * @param {number} _frameIndex
         */
        public setStartFrameIndex (
            _frameIndex : number 
        ) : void;

        /**
         * 
         * @method getSingleFrameIndex
         * @return {number}
         */
        public getSingleFrameIndex (
        ) : number;

        /**
         * 
         * @method setInnerActionType
         * @param {ccs.timeline::InnerActionType} _type
         */
        public setInnerActionType (
            _type : number 
        ) : void;

        /**
         * 
         * @method setAnimationName
         * @param {string} _animationNamed
         */
        public setAnimationName (
            _animationNamed : string 
        ) : void;

        /**
         * 
         * @method create
         * @return {any}
         */
        public static create (
        ) : any;

        /**
         * 
         * @method InnerActionFrame
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class ColorFrame
     * @native
     */
    export class ColorFrame 
        extends Frame
    {

        /**
         * 
         * @method getColor
         * @return {ctype.value_type<cc.Color>}
         */
        public getColor (
        ) : ctype.value_type<cc.Color>;

        /**
         * 
         * @method setColor
         * @param {ctype.value_type<cc.Color>} _color
         */
        public setColor (
            _color : ctype.value_type<cc.Color> 
        ) : void;

        /**
         * 
         * @method create
         * @return {any}
         */
        public static create (
        ) : any;

        /**
         * 
         * @method ColorFrame
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class AlphaFrame
     * @native
     */
    export class AlphaFrame 
        extends Frame
    {

        /**
         * 
         * @method getAlpha
         * @return {number}
         */
        public getAlpha (
        ) : number;

        /**
         * 
         * @method setAlpha
         * @param {number} _alpha
         */
        public setAlpha (
            _alpha : number 
        ) : void;

        /**
         * 
         * @method create
         * @return {any}
         */
        public static create (
        ) : any;

        /**
         * 
         * @method AlphaFrame
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class EventFrame
     * @native
     */
    export class EventFrame 
        extends Frame
    {

        /**
         * 
         * @method setEvent
         * @param {string} _event
         */
        public setEvent (
            _event : string 
        ) : void;

        /**
         * 
         * @method init
         */
        public init (
        ) : void;

        /**
         * 
         * @method getEvent
         * @return {string}
         */
        public getEvent (
        ) : string;

        /**
         * 
         * @method create
         * @return {any}
         */
        public static create (
        ) : any;

        /**
         * 
         * @method EventFrame
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class ZOrderFrame
     * @native
     */
    export class ZOrderFrame 
        extends Frame
    {

        /**
         * 
         * @method getZOrder
         * @return {number}
         */
        public getZOrder (
        ) : number;

        /**
         * 
         * @method setZOrder
         * @param {number} _zorder
         */
        public setZOrder (
            _zorder : number 
        ) : void;

        /**
         * 
         * @method create
         * @return {any}
         */
        public static create (
        ) : any;

        /**
         * 
         * @method ZOrderFrame
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class BlendFuncFrame
     * @native
     */
    export class BlendFuncFrame 
        extends Frame
    {

        /**
         * 
         * @method getBlendFunc
         * @return {cc.BlendFunc}
         */
        public getBlendFunc (
        ) : cc.BlendFunc;

        /**
         * 
         * @method setBlendFunc
         * @param {cc.BlendFunc} _blendFunc
         */
        public setBlendFunc (
            _blendFunc : cc.BlendFunc 
        ) : void;

        /**
         * 
         * @method create
         * @return {any}
         */
        public static create (
        ) : any;

        /**
         * 
         * @method BlendFuncFrame
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class Timeline
     * @native
     */
    export class Timeline 
    {

        /**
         * 
         * @method clone
         * @return {any}
         */
        public clone (
        ) : any;

        /**
         * 
         * @method gotoFrame
         * @param {number} _frameIndex
         */
        public gotoFrame (
            _frameIndex : number 
        ) : void;

        /**
         * 
         * @method setNode
         * @param {cc.Node} _node
         */
        public setNode (
            _node : cc.Node 
        ) : void;

        /**
         * 
         * @method getActionTimeline
         * @return {any}
         */
        public getActionTimeline (
        ) : any;

        /**
         * 
         * @method insertFrame
         * @param {ccs.timeline::Frame} _frame
         * @param {number} _index
         */
        public insertFrame (
            _frame : any, 
            _index : number 
        ) : void;

        /**
         * 
         * @method setActionTag
         * @param {number} _tag
         */
        public setActionTag (
            _tag : number 
        ) : void;

        /**
         * 
         * @method addFrame
         * @param {ccs.timeline::Frame} _frame
         */
        public addFrame (
            _frame : any 
        ) : void;

        /**
         * 
         * @method getFrames
         * @return {any[]}
         */
        public getFrames (
        ) : any[];

        /**
         * 
         * @method getActionTag
         * @return {number}
         */
        public getActionTag (
        ) : number;

        /**
         * 
         * @method getNode
         * @return {cc.Node}
         */
        public getNode (
        ) : cc.Node;

        /**
         * 
         * @method removeFrame
         * @param {ccs.timeline::Frame} _frame
         */
        public removeFrame (
            _frame : any 
        ) : void;

        /**
         * 
         * @method setActionTimeline
         * @param {ccs.timeline::ActionTimeline} _action
         */
        public setActionTimeline (
            _action : any 
        ) : void;

        /**
         * 
         * @method stepToFrame
         * @param {number} _frameIndex
         * @param {number} _floatingPart
         */
        public stepToFrame (
            _frameIndex : number, 
            _floatingPart : number 
        ) : void;

        /**
         * 
         * @method create
         * @return {any}
         */
        public static create (
        ) : any;

        /**
         * 
         * @method Timeline
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class ActionTimelineData
     * @native
     */
    export class ActionTimelineData 
    {

        /**
         * 
         * @method setActionTag
         * @param {number} _actionTag
         */
        public setActionTag (
            _actionTag : number 
        ) : void;

        /**
         * 
         * @method init
         * @param {number} _actionTag
         * @return {boolean}
         */
        public init (
            _actionTag : number 
        ) : boolean;

        /**
         * 
         * @method getActionTag
         * @return {number}
         */
        public getActionTag (
        ) : number;

        /**
         * 
         * @method create
         * @param {number} _actionTag
         * @return {any}
         */
        public static create (
            _actionTag : number 
        ) : any;

        /**
         * 
         * @method ActionTimelineData
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class ActionTimeline
     * @native
     */
    export class ActionTimeline 
        extends cc.Action
    {

        /**
         *  Set ActionTimeline's frame event callback function 
         * @method setFrameEventCallFunc
         * @param {(arg0:ccs.timeline::Frame) => void} _listener
         */
        public setFrameEventCallFunc (
            _listener : (arg0:any) => void 
        ) : void;

        /**
         * 
         * @method clearFrameEndCallFuncs
         */
        public clearFrameEndCallFuncs (
        ) : void;

        /**
         * add a frame end call back to animation's end frame<br>
         * -- param animationName  @addFrameEndCallFunc, make the animationName as funcKey<br>
         * -- param func the callback function
         * @method setAnimationEndCallFunc
         * @param {string} _animationName
         * @param {() => void} _func
         */
        public setAnimationEndCallFunc (
            _animationName : string, 
            _func : () => void 
        ) : void;

        /**
         *  add Timeline to ActionTimeline 
         * @method addTimeline
         * @param {ccs.timeline::Timeline} _timeline
         */
        public addTimeline (
            _timeline : any 
        ) : void;

        /**
         *  Get current frame. 
         * @method getCurrentFrame
         * @return {number}
         */
        public getCurrentFrame (
        ) : number;

        /**
         *  Start frame index of this action
         * @method getStartFrame
         * @return {number}
         */
        public getStartFrame (
        ) : number;

        /**
         *  Pause the animation. 
         * @method pause
         */
        public pause (
        ) : void;

        /**
         * 
         * @method init
         * @return {boolean}
         */
        public init (
        ) : boolean;

        /**
         * 
         * @method getAnimationNames
         * @return {string[]}
         */
        public getAnimationNames (
        ) : string[];

        /**
         * 
         * @method removeTimeline
         * @param {ccs.timeline::Timeline} _timeline
         */
        public removeTimeline (
            _timeline : any 
        ) : void;

        /**
         *  Last frame callback will call when arriving last frame 
         * @method setLastFrameCallFunc
         * @param {() => void} _listener
         */
        public setLastFrameCallFunc (
            _listener : () => void 
        ) : void;

        /**
         * 
         * @method IsAnimationInfoExists
         * @param {string} _animationName
         * @return {boolean}
         */
        public isAnimationInfoExists (
            _animationName : string 
        ) : boolean;

        /**
         * 
         * @method getTimelines
         * @return {any[]}
         */
        public getTimelines (
        ) : any[];

        /**
         * 
         * @method play
         * @param {string} _animationName
         * @param {boolean} _loop
         */
        public play (
            _animationName : string, 
            _loop : boolean 
        ) : void;

        /**
         * 
         * @method getAnimationInfo
         * @param {string} _animationName
         * @return {AnimationInfo}
         */
        public getAnimationInfo (
            _animationName : string 
        ) : AnimationInfo;

        /**
         *  Resume the animation. 
         * @method resume
         */
        public resume (
        ) : void;

        /**
         *  add a callback function after played frameIndex<br>
         * -- param frameIndex the frame index call back after<br>
         * -- param funcKey for identity the callback function<br>
         * -- param func the callback function
         * @method addFrameEndCallFunc
         * @param {number} _frameIndex
         * @param {string} _funcKey
         * @param {() => void} _func
         */
        public addFrameEndCallFunc (
            _frameIndex : number, 
            _funcKey : string, 
            _func : () => void 
        ) : void;

        /**
         * 
         * @method removeAnimationInfo
         * @param {string} _animationName
         */
        public removeAnimationInfo (
            _animationName : string 
        ) : void;

        /**
         *  Get current animation speed. 
         * @method getTimeSpeed
         * @return {number}
         */
        public getTimeSpeed (
        ) : number;

        /**
         *  AnimationInfo
         * @method addAnimationInfo
         * @param {AnimationInfo} _animationInfo
         */
        public addAnimationInfo (
            _animationInfo : AnimationInfo 
        ) : void;

        /**
         * 
         * @method getDuration
         * @return {number}
         */
        public getDuration (
        ) : number;

        /**
         *  Goto the specified frame index, and pause at this index.<br>
         * -- param startIndex The animation will pause at this index.
         * @method gotoFrameAndPause
         * @param {number} _startIndex
         */
        public gotoFrameAndPause (
            _startIndex : number 
        ) : void;

        /**
         *  Whether or not Action is playing. 
         * @method isPlaying
         * @return {boolean}
         */
        public isPlaying (
        ) : boolean;

        /**
         * 
         * @method removeFrameEndCallFuncs
         * @param {number} _frameIndex
         */
        public removeFrameEndCallFuncs (
            _frameIndex : number 
        ) : void;

        /**
         *  Goto the specified frame index, and start playing from this index.<br>
         * -- param startIndex The animation will play from this index.<br>
         * -- param loop Whether or not the animation need loop. 
         * @method gotoFrameAndPlay
         * @param {(number)} _startIndex
         * @param {(boolean) | (number)} _loop | endIndex?
         * @param {(boolean) | (number)} _loop | currentFrameIndex?
         * @param {(boolean)} _loop?
         */
        public gotoFrameAndPlay (
            _startIndex : (number), 
            _loop_endIndex? : (boolean) | (number), 
            _loop_currentFrameIndex? : (boolean) | (number), 
            _loop? : (boolean)
        ) : void;

        /**
         * 
         * @method clearFrameEventCallFunc
         */
        public clearFrameEventCallFunc (
        ) : void;

        /**
         *  End frame of this action.<br>
         * -- When action play to this frame, if action is not loop, then it will stop, <br>
         * -- or it will play from start frame again. 
         * @method getEndFrame
         * @return {number}
         */
        public getEndFrame (
        ) : number;

        /**
         *  Set the animation speed, this will speed up or slow down the speed. 
         * @method setTimeSpeed
         * @param {number} _speed
         */
        public setTimeSpeed (
            _speed : number 
        ) : void;

        /**
         * 
         * @method clearLastFrameCallFunc
         */
        public clearLastFrameCallFunc (
        ) : void;

        /**
         *  duration of the whole action
         * @method setDuration
         * @param {number} _duration
         */
        public setDuration (
            _duration : number 
        ) : void;

        /**
         *  Set current frame index, this will cause action plays to this frame. 
         * @method setCurrentFrame
         * @param {number} _frameIndex
         */
        public setCurrentFrame (
            _frameIndex : number 
        ) : void;

        /**
         * 
         * @method removeFrameEndCallFunc
         * @param {number} _frameIndex
         * @param {string} _funcKey
         */
        public removeFrameEndCallFunc (
            _frameIndex : number, 
            _funcKey : string 
        ) : void;

        /**
         * 
         * @method create
         * @return {any}
         */
        public static create (
        ) : any;

        /**
         * 
         * @method ActionTimeline
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class BoneNode
     * @native
     */
    export class BoneNode 
        extends cc.Node
    {

        /**
         * 
         * @method getDebugDrawWidth
         * @return {number}
         */
        public getDebugDrawWidth (
        ) : number;

        /**
         * 
         * @method getChildBones
         * @return {ccs.timeline::BoneNode[]}
         */
        public getChildBones (
        ) : any[];

        /**
         * 
         * @method getBlendFunc
         * @return {cc.BlendFunc}
         */
        public getBlendFunc (
        ) : cc.BlendFunc;

        /**
         * brief: get all bones in this bone tree
         * @method getAllSubBones
         * @return {any[]}
         */
        public getAllSubBones (
        ) : any[];

        /**
         * 
         * @method setBlendFunc
         * @param {cc.BlendFunc} _blendFunc
         */
        public setBlendFunc (
            _blendFunc : cc.BlendFunc 
        ) : void;

        /**
         * 
         * @method setDebugDrawEnabled
         * @param {boolean} _isDebugDraw
         */
        public setDebugDrawEnabled (
            _isDebugDraw : boolean 
        ) : void;

        /**
         * get displayings rect in self transform
         * @method getVisibleSkinsRect
         * @return {ctype.value_type<cc.Rect>}
         */
        public getVisibleSkinsRect (
        ) : ctype.value_type<cc.Rect>;

        /**
         * brief: get all skins in this bone tree
         * @method getAllSubSkins
         * @return {cc.Node[]}
         */
        public getAllSubSkins (
        ) : cc.Node[];

        /**
         * brief: display all skins named skinName, if hide display only one skin,<br>
         * -- prefer to use display(SkinNode* skin, bool hideOthers = false)<br>
         * -- param: hideOthers, set other skins invisible
         * @method displaySkin
         * @param {(string) | (cc.Node)} _skinName | skin
         * @param {(boolean)} _hideOthers
         */
        public displaySkin (
            _skinName_skin : (string) | (cc.Node), 
            _hideOthers : (boolean)
        ) : void;

        /**
         * 
         * @method isDebugDrawEnabled
         * @return {boolean}
         */
        public isDebugDrawEnabled (
        ) : boolean;

        /**
         * brief: add a skin<br>
         * -- param: display, whether display this skin<br>
         * -- param: hideOthers, whether hide other skins added to this bone
         * @method addSkin
         * @param {(cc.Node)} _skin
         * @param {(boolean)} _display
         * @param {(boolean)} _hideOthers?
         */
        public addSkin (
            _skin : (cc.Node), 
            _display : (boolean), 
            _hideOthers? : (boolean)
        ) : void;

        /**
         * 
         * @method getRootSkeletonNode
         * @return {any}
         */
        public getRootSkeletonNode (
        ) : any;

        /**
         * 
         * @method setDebugDrawLength
         * @param {number} _length
         */
        public setDebugDrawLength (
            _length : number 
        ) : void;

        /**
         * 
         * @method getSkins
         * @return {cc.Node[]}
         */
        public getSkins (
        ) : cc.Node[];

        /**
         * 
         * @method getVisibleSkins
         * @return {cc.Node[]}
         */
        public getVisibleSkins (
        ) : cc.Node[];

        /**
         * 
         * @method setDebugDrawWidth
         * @param {number} _width
         */
        public setDebugDrawWidth (
            _width : number 
        ) : void;

        /**
         * 
         * @method getDebugDrawLength
         * @return {number}
         */
        public getDebugDrawLength (
        ) : number;

        /**
         * 
         * @method setDebugDrawColor
         * @param {ctype.value_type<cc.Color>} _color
         */
        public setDebugDrawColor (
            _color : ctype.value_type<cc.Color> 
        ) : void;

        /**
         * 
         * @method getDebugDrawColor
         * @return {ctype.value_type<cc.Color>}
         */
        public getDebugDrawColor (
        ) : ctype.value_type<cc.Color>;

        /**
         * 
         * @method create
         * @param {(number)} _length?
         * @return {ccs.timeline::BoneNode}
         */
        public static create (
            _length? : (number)
        ) : any;

        /**
         * 
         * @method BoneNode
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class SkeletonNode
     * @native
     */
    export class SkeletonNode 
        extends BoneNode
    {

        /**
         * get bonenode in skeleton node by bone name
         * @method getBoneNode
         * @param {string} _boneName
         * @return {any}
         */
        public getBoneNode (
            _boneName : string 
        ) : any;

        /**
         * brief: change displays<br>
         * -- param: skinGroupName have
         * @method changeSkins
         * @param {(string) | (any)} _skinGroupName | boneSkinNameMap
         */
        public changeSkins (
            _skinGroupName_boneSkinNameMap : (string) | (any)
        ) : void;

        /**
         * brief: add a boneSkinNameMap as a SkinGroup named groupName<br>
         * -- param: groupName, key<br>
         * -- param: boneSkinNameMap, map <name of bone, name of skin to display which added to bone>
         * @method addSkinGroup
         * @param {string} _groupName
         * @param {any} _boneSkinNameMap
         */
        public addSkinGroup (
            _groupName : string, 
            _boneSkinNameMap : any 
        ) : void;

        /**
         * get All bones in this skeleton, <bone's name, BoneNode>
         * @method getAllSubBonesMap
         * @return {any}
         */
        public getAllSubBonesMap (
        ) : any;

        /**
         * 
         * @method create
         * @return {any}
         */
        public static create (
        ) : any;

        /**
         * 
         * @method SkeletonNode
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class ComExtensionData
     * @native
     */
    export class ComExtensionData 
        extends cc.Component
    {

        /**
         * 
         * @method setActionTag
         * @param {number} _actionTag
         */
        public setActionTag (
            _actionTag : number 
        ) : void;

        /**
         * 
         * @method getCustomProperty
         * @return {string}
         */
        public getCustomProperty (
        ) : string;

        /**
         * 
         * @method getActionTag
         * @return {number}
         */
        public getActionTag (
        ) : number;

        /**
         * 
         * @method setCustomProperty
         * @param {string} _customProperty
         */
        public setCustomProperty (
            _customProperty : string 
        ) : void;

        /**
         * 
         * @method create
         * @return {ccs.ComExtensionData}
         */
        public static create (
        ) : ccs.ComExtensionData;

        /**
         * 
         * @method ComExtensionData
         * @constructor
         */
        public constructor (
        );

    }
}