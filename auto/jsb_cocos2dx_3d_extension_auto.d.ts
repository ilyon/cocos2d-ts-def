/**
 * @module cocos2dx_3d_extension
 */
declare namespace jsb {
    /**
     * @class ParticleSystem3D
     * @native
     */
    export class ParticleSystem3D 
        extends cc.Node
    {

        /**
         * resume particle
         * @method resumeParticleSystem
         */
        public resumeParticleSystem (
        ) : void;

        /**
         * particle system play control
         * @method startParticleSystem
         */
        public startParticleSystem (
        ) : void;

        /**
         * is enabled
         * @method isEnabled
         * @return {boolean}
         */
        public isEnabled (
        ) : boolean;

        /**
         * return particle render
         * @method getRender
         * @return {cc.Particle3DRender}
         */
        public getRender (
        ) : cc.Particle3DRender;

        /**
         * 
         * @method isKeepLocal
         * @return {boolean}
         */
        public isKeepLocal (
        ) : boolean;

        /**
         * Enables or disables the system.
         * @method setEnabled
         * @param {boolean} _enabled
         */
        public setEnabled (
            _enabled : boolean 
        ) : void;

        /**
         * get particle quota
         * @method getParticleQuota
         * @return {number}
         */
        public getParticleQuota (
        ) : number;

        /**
         * override function
         * @method getBlendFunc
         * @return {cc.BlendFunc}
         */
        public getBlendFunc (
        ) : cc.BlendFunc;

        /**
         * pause particle
         * @method pauseParticleSystem
         */
        public pauseParticleSystem (
        ) : void;

        /**
         * get particle playing state
         * @method getState
         * @return {number}
         */
        public getState (
        ) : number;

        /**
         * get alive particles count
         * @method getAliveParticleCount
         * @return {number}
         */
        public getAliveParticleCount (
        ) : number;

        /**
         * set particle quota
         * @method setParticleQuota
         * @param {number} _quota
         */
        public setParticleQuota (
            _quota : number 
        ) : void;

        /**
         * override function
         * @method setBlendFunc
         * @param {cc.BlendFunc} _blendFunc
         */
        public setBlendFunc (
            _blendFunc : cc.BlendFunc 
        ) : void;

        /**
         * stop particle
         * @method stopParticleSystem
         */
        public stopParticleSystem (
        ) : void;

        /**
         * 
         * @method setKeepLocal
         * @param {boolean} _keepLocal
         */
        public setKeepLocal (
            _keepLocal : boolean 
        ) : void;

        /**
         * 
         * @method ParticleSystem3D
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class PUParticleSystem3D
     * @native
     */
    export class PUParticleSystem3D 
        extends cc.ParticleSystem3D
    {

        /**
         * 
         * @method initWithFilePath
         * @param {string} _filePath
         * @return {boolean}
         */
        public initWithFilePath (
            _filePath : string 
        ) : boolean;

        /**
         * Returns the velocity scale, defined in the particle system, but passed to the technique for convenience.
         * @method getParticleSystemScaleVelocity
         * @return {number}
         */
        public getParticleSystemScaleVelocity (
        ) : number;

        /**
         * 
         * @method setEmittedSystemQuota
         * @param {number} _quota
         */
        public setEmittedSystemQuota (
            _quota : number 
        ) : void;

        /**
         * default particle depth
         * @method getDefaultDepth
         * @return {number}
         */
        public getDefaultDepth (
        ) : number;

        /**
         * 
         * @method getEmittedSystemQuota
         * @return {number}
         */
        public getEmittedSystemQuota (
        ) : number;

        /**
         * 
         * @method initWithFilePathAndMaterialPath
         * @param {string} _filePath
         * @param {string} _materialPath
         * @return {boolean}
         */
        public initWithFilePathAndMaterialPath (
            _filePath : string, 
            _materialPath : string 
        ) : boolean;

        /**
         * 
         * @method clearAllParticles
         */
        public clearAllParticles (
        ) : void;

        /**
         * 
         * @method getMaterialName
         * @return {string}
         */
        public getMaterialName (
        ) : string;

        /**
         * 
         * @method calulateRotationOffset
         */
        public calulateRotationOffset (
        ) : void;

        /**
         * Return the maximum velocity a particle can have, even if the velocity of the particle has been set higher (either by initialisation of the particle or by means of an affector).
         * @method getMaxVelocity
         * @return {number}
         */
        public getMaxVelocity (
        ) : number;

        /**
         * 
         * @method forceUpdate
         * @param {number} _delta
         */
        public forceUpdate (
            _delta : number 
        ) : void;

        /**
         * 
         * @method getTimeElapsedSinceStart
         * @return {number}
         */
        public getTimeElapsedSinceStart (
        ) : number;

        /**
         * 
         * @method getEmittedEmitterQuota
         * @return {number}
         */
        public getEmittedEmitterQuota (
        ) : number;

        /**
         * 
         * @method isMarkedForEmission
         * @return {boolean}
         */
        public isMarkedForEmission (
        ) : boolean;

        /**
         * default particle width
         * @method getDefaultWidth
         * @return {number}
         */
        public getDefaultWidth (
        ) : number;

        /**
         * 
         * @method setEmittedEmitterQuota
         * @param {number} _quota
         */
        public setEmittedEmitterQuota (
            _quota : number 
        ) : void;

        /**
         * 
         * @method setMarkedForEmission
         * @param {boolean} _isMarked
         */
        public setMarkedForEmission (
            _isMarked : boolean 
        ) : void;

        /**
         * 
         * @method clone
         * @return {cc.PUParticleSystem3D}
         */
        public clone (
        ) : cc.PUParticleSystem3D;

        /**
         * 
         * @method setDefaultWidth
         * @param {number} _width
         */
        public setDefaultWidth (
            _width : number 
        ) : void;

        /**
         * 
         * @method copyAttributesTo
         * @param {cc.PUParticleSystem3D} _system
         */
        public copyAttributesTo (
            _system : cc.PUParticleSystem3D 
        ) : void;

        /**
         * 
         * @method setMaterialName
         * @param {string} _name
         */
        public setMaterialName (
            _name : string 
        ) : void;

        /**
         * 
         * @method getParentParticleSystem
         * @return {cc.PUParticleSystem3D}
         */
        public getParentParticleSystem (
        ) : cc.PUParticleSystem3D;

        /**
         * Set the maximum velocity a particle can have.
         * @method setMaxVelocity
         * @param {number} _maxVelocity
         */
        public setMaxVelocity (
            _maxVelocity : number 
        ) : void;

        /**
         * default particle height
         * @method getDefaultHeight
         * @return {number}
         */
        public getDefaultHeight (
        ) : number;

        /**
         * 
         * @method getDerivedPosition
         * @return {ctype.value_type<cc.Vec3>}
         */
        public getDerivedPosition (
        ) : ctype.value_type<cc.Vec3>;

        /**
         * If the orientation of the particle system has been changed since the last update, the passed vector is rotated accordingly.
         * @method rotationOffset
         * @param {ctype.value_type<cc.Vec3>} _pos
         */
        public rotationOffset (
            _pos : ctype.value_type<cc.Vec3> 
        ) : void;

        /**
         * 
         * @method getDerivedOrientation
         * @return {cc.Quaternion}
         */
        public getDerivedOrientation (
        ) : cc.Quaternion;

        /**
         * 
         * @method removeAllEmitter
         */
        public removeAllEmitter (
        ) : void;

        /**
         * 
         * @method setParticleSystemScaleVelocity
         * @param {number} _scaleVelocity
         */
        public setParticleSystemScaleVelocity (
            _scaleVelocity : number 
        ) : void;

        /**
         * 
         * @method getDerivedScale
         * @return {ctype.value_type<cc.Vec3>}
         */
        public getDerivedScale (
        ) : ctype.value_type<cc.Vec3>;

        /**
         * 
         * @method setDefaultHeight
         * @param {number} _height
         */
        public setDefaultHeight (
            _height : number 
        ) : void;

        /**
         * 
         * @method removeAllListener
         */
        public removeAllListener (
        ) : void;

        /**
         * 
         * @method initSystem
         * @param {string} _filePath
         * @return {boolean}
         */
        public initSystem (
            _filePath : string 
        ) : boolean;

        /**
         * 
         * @method setDefaultDepth
         * @param {number} _depth
         */
        public setDefaultDepth (
            _depth : number 
        ) : void;

        /**
         * 
         * @method create
         * @param {(string)} _filePath?
         * @param {(string)} _materialPath?
         * @return {cc.PUParticleSystem3D}
         */
        public static create (
            _filePath? : (string), 
            _materialPath? : (string)
        ) : cc.PUParticleSystem3D;

        /**
         * 
         * @method PUParticleSystem3D
         * @constructor
         */
        public constructor (
        );

    }
}