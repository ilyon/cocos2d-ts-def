/**
 * @module cocos2dx_physics3d
 */
declare namespace jsb {
    /**
     * @class Physics3DShape
     * @native
     */
    export class Physics3DShape 
    {

        /**
         * 
         * @method initConvexHull
         * @param {ctype.value_type<cc.Vec3>} _points
         * @param {number} _numPoints
         * @return {boolean}
         */
        public initConvexHull (
            _points : ctype.value_type<cc.Vec3>, 
            _numPoints : number 
        ) : boolean;

        /**
         * 
         * @method getbtShape
         * @return {btCollisionShape}
         */
        public getbtShape (
        ) : btCollisionShape;

        /**
         * 
         * @method initSphere
         * @param {number} _radius
         * @return {boolean}
         */
        public initSphere (
            _radius : number 
        ) : boolean;

        /**
         * 
         * @method initBox
         * @param {ctype.value_type<cc.Vec3>} _ext
         * @return {boolean}
         */
        public initBox (
            _ext : ctype.value_type<cc.Vec3> 
        ) : boolean;

        /**
         * 
         * @method initCapsule
         * @param {number} _radius
         * @param {number} _height
         * @return {boolean}
         */
        public initCapsule (
            _radius : number, 
            _height : number 
        ) : boolean;

        /**
         * 
         * @method initCylinder
         * @param {number} _radius
         * @param {number} _height
         * @return {boolean}
         */
        public initCylinder (
            _radius : number, 
            _height : number 
        ) : boolean;

        /**
         * get shape type
         * @method getShapeType
         * @return {number}
         */
        public getShapeType (
        ) : number;

        /**
         * create box shape<br>
         * -- param extent The extent of sphere.
         * @method createBox
         * @param {ctype.value_type<cc.Vec3>} _extent
         * @return {cc.Physics3DShape}
         */
        public static createBox (
            _extent : ctype.value_type<cc.Vec3> 
        ) : cc.Physics3DShape;

        /**
         * create cylinder shape<br>
         * -- param radius The radius of cylinder.<br>
         * -- param height The height.
         * @method createCylinder
         * @param {number} _radius
         * @param {number} _height
         * @return {cc.Physics3DShape}
         */
        public static createCylinder (
            _radius : number, 
            _height : number 
        ) : cc.Physics3DShape;

        /**
         * create convex hull<br>
         * -- param points The vertices of convex hull<br>
         * -- param numPoints The number of vertices.
         * @method createConvexHull
         * @param {ctype.value_type<cc.Vec3>} _points
         * @param {number} _numPoints
         * @return {cc.Physics3DShape}
         */
        public static createConvexHull (
            _points : ctype.value_type<cc.Vec3>, 
            _numPoints : number 
        ) : cc.Physics3DShape;

        /**
         * create capsule shape<br>
         * -- param radius The radius of capsule.<br>
         * -- param height The height (cylinder part).
         * @method createCapsule
         * @param {number} _radius
         * @param {number} _height
         * @return {cc.Physics3DShape}
         */
        public static createCapsule (
            _radius : number, 
            _height : number 
        ) : cc.Physics3DShape;

        /**
         * create sphere shape<br>
         * -- param radius The radius of sphere.
         * @method createSphere
         * @param {number} _radius
         * @return {cc.Physics3DShape}
         */
        public static createSphere (
            _radius : number 
        ) : cc.Physics3DShape;

        /**
         * 
         * @method Physics3DShape
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class Physics3DObject
     * @native
     */
    export abstract class Physics3DObject 
    {

        /**
         *  Set the user data. 
         * @method setUserData
         * @param {void} _userData
         */
        public setUserData (
            _userData : void 
        ) : void;

        /**
         *  Get the user data. 
         * @method getUserData
         * @return {void}
         */
        public getUserData (
        ) : void;

        /**
         *  Get the Physics3DObject Type. 
         * @method getObjType
         * @return {number}
         */
        public getObjType (
        ) : number;

        /**
         *  Internal method. Set the pointer of Physics3DWorld. 
         * @method setPhysicsWorld
         * @param {cc.Physics3DWorld} _world
         */
        public setPhysicsWorld (
            _world : cc.Physics3DWorld 
        ) : void;

        /**
         *  Get the world matrix of Physics3DObject. 
         * @method getWorldTransform
         * @return {ctype.value_type<cc.Mat4>}
         */
        public getWorldTransform (
        ) : ctype.value_type<cc.Mat4>;

        /**
         *  Get the pointer of Physics3DWorld. 
         * @method getPhysicsWorld
         * @return {cc.Physics3DWorld}
         */
        public getPhysicsWorld (
        ) : cc.Physics3DWorld;

        /**
         *  Set the mask of Physics3DObject. 
         * @method setMask
         * @param {number} _mask
         */
        public setMask (
            _mask : number 
        ) : void;

        /**
         *  Get the collision callback function. 
         * @method getCollisionCallback
         * @return {(arg0:cc.Physics3DCollisionInfo ) => void}
         */
        public getCollisionCallback (
        ) : (arg0:cc.Physics3DCollisionInfo ) => void;

        /**
         *  Get the mask of Physics3DObject. 
         * @method getMask
         * @return {number}
         */
        public getMask (
        ) : number;

        /**
         *  Check has collision callback function. 
         * @method needCollisionCallback
         * @return {boolean}
         */
        public needCollisionCallback (
        ) : boolean;

    }
    /**
     * @class Physics3DRigidBody
     * @native
     */
    export class Physics3DRigidBody 
        extends cc.Physics3DObject
    {

        /**
         *  Set the acceleration. 
         * @method setGravity
         * @param {ctype.value_type<cc.Vec3>} _acceleration
         */
        public setGravity (
            _acceleration : ctype.value_type<cc.Vec3> 
        ) : void;

        /**
         *  Get friction. 
         * @method getFriction
         * @return {number}
         */
        public getFriction (
        ) : number;

        /**
         *  Set the angular factor, use unified factor. 
         * @method setAngularFactor
         * @param {(number) | (ctype.value_type<cc.Vec3>)} _angFac
         */
        public setAngularFactor (
            _angFac : (number) | (ctype.value_type<cc.Vec3>)
        ) : void;

        /**
         * 
         * @method addConstraint
         * @param {number} _constraint
         */
        public addConstraint (
            _constraint : number 
        ) : void;

        /**
         *  Get the pointer of btRigidBody. 
         * @method getRigidBody
         * @return {btRigidBody}
         */
        public getRigidBody (
        ) : btRigidBody;

        /**
         *  Get total force. 
         * @method getTotalForce
         * @return {ctype.value_type<cc.Vec3>}
         */
        public getTotalForce (
        ) : ctype.value_type<cc.Vec3>;

        /**
         *  Get the total number of constraints. 
         * @method getConstraintCount
         * @return {number}
         */
        public getConstraintCount (
        ) : number;

        /**
         * Apply a central force.<br>
         * -- param   force the value of the force
         * @method applyCentralForce
         * @param {ctype.value_type<cc.Vec3>} _force
         */
        public applyCentralForce (
            _force : ctype.value_type<cc.Vec3> 
        ) : void;

        /**
         *  Set mass and inertia. 
         * @method setMassProps
         * @param {number} _mass
         * @param {ctype.value_type<cc.Vec3>} _inertia
         */
        public setMassProps (
            _mass : number, 
            _inertia : ctype.value_type<cc.Vec3> 
        ) : void;

        /**
         *  Set friction. 
         * @method setFriction
         * @param {number} _frict
         */
        public setFriction (
            _frict : number 
        ) : void;

        /**
         *  Set kinematic object. 
         * @method setKinematic
         * @param {boolean} _kinematic
         */
        public setKinematic (
            _kinematic : boolean 
        ) : void;

        /**
         *  Set linear damping and angular damping. 
         * @method setDamping
         * @param {number} _lin_damping
         * @param {number} _ang_damping
         */
        public setDamping (
            _lin_damping : number, 
            _ang_damping : number 
        ) : void;

        /**
         * Apply a impulse.<br>
         * -- param   impulse the value of the impulse<br>
         * -- param   rel_pos the position of the impulse
         * @method applyImpulse
         * @param {ctype.value_type<cc.Vec3>} _impulse
         * @param {ctype.value_type<cc.Vec3>} _rel_pos
         */
        public applyImpulse (
            _impulse : ctype.value_type<cc.Vec3>, 
            _rel_pos : ctype.value_type<cc.Vec3> 
        ) : void;

        /**
         *  Check rigid body is kinematic object. 
         * @method isKinematic
         * @return {boolean}
         */
        public isKinematic (
        ) : boolean;

        /**
         * Apply a torque.<br>
         * -- param   torque the value of the torque
         * @method applyTorque
         * @param {ctype.value_type<cc.Vec3>} _torque
         */
        public applyTorque (
            _torque : ctype.value_type<cc.Vec3> 
        ) : void;

        /**
         *  Set motion threshold, don't do continuous collision detection if the motion (in one step) is less then ccdMotionThreshold 
         * @method setCcdMotionThreshold
         * @param {number} _ccdMotionThreshold
         */
        public setCcdMotionThreshold (
            _ccdMotionThreshold : number 
        ) : void;

        /**
         *  Set rolling friction. 
         * @method setRollingFriction
         * @param {number} _frict
         */
        public setRollingFriction (
            _frict : number 
        ) : void;

        /**
         *  Get motion threshold. 
         * @method getCcdMotionThreshold
         * @return {number}
         */
        public getCcdMotionThreshold (
        ) : number;

        /**
         *  Get the linear factor. 
         * @method getLinearFactor
         * @return {ctype.value_type<cc.Vec3>}
         */
        public getLinearFactor (
        ) : ctype.value_type<cc.Vec3>;

        /**
         *  Damps the velocity, using the given linearDamping and angularDamping. 
         * @method applyDamping
         * @param {number} _timeStep
         */
        public applyDamping (
            _timeStep : number 
        ) : void;

        /**
         *  Get the angular velocity. 
         * @method getAngularVelocity
         * @return {ctype.value_type<cc.Vec3>}
         */
        public getAngularVelocity (
        ) : ctype.value_type<cc.Vec3>;

        /**
         * 
         * @method init
         * @param {cc.Physics3DRigidBodyDes} _info
         * @return {boolean}
         */
        public init (
            _info : cc.Physics3DRigidBodyDes 
        ) : boolean;

        /**
         * Apply a torque impulse.<br>
         * -- param   torque the value of the torque
         * @method applyTorqueImpulse
         * @param {ctype.value_type<cc.Vec3>} _torque
         */
        public applyTorqueImpulse (
            _torque : ctype.value_type<cc.Vec3> 
        ) : void;

        /**
         *  Active or inactive. 
         * @method setActive
         * @param {boolean} _active
         */
        public setActive (
            _active : boolean 
        ) : void;

        /**
         *  Set the linear factor. 
         * @method setLinearFactor
         * @param {ctype.value_type<cc.Vec3>} _linearFactor
         */
        public setLinearFactor (
            _linearFactor : ctype.value_type<cc.Vec3> 
        ) : void;

        /**
         *  Set the linear velocity. 
         * @method setLinearVelocity
         * @param {ctype.value_type<cc.Vec3>} _lin_vel
         */
        public setLinearVelocity (
            _lin_vel : ctype.value_type<cc.Vec3> 
        ) : void;

        /**
         *  Get the linear velocity. 
         * @method getLinearVelocity
         * @return {ctype.value_type<cc.Vec3>}
         */
        public getLinearVelocity (
        ) : ctype.value_type<cc.Vec3>;

        /**
         *  Set swept sphere radius. 
         * @method setCcdSweptSphereRadius
         * @param {number} _radius
         */
        public setCcdSweptSphereRadius (
            _radius : number 
        ) : void;

        /**
         * Apply a force.<br>
         * -- param   force the value of the force<br>
         * -- param   rel_pos the position of the force
         * @method applyForce
         * @param {ctype.value_type<cc.Vec3>} _force
         * @param {ctype.value_type<cc.Vec3>} _rel_pos
         */
        public applyForce (
            _force : ctype.value_type<cc.Vec3>, 
            _rel_pos : ctype.value_type<cc.Vec3> 
        ) : void;

        /**
         *  Set the angular velocity. 
         * @method setAngularVelocity
         * @param {ctype.value_type<cc.Vec3>} _ang_vel
         */
        public setAngularVelocity (
            _ang_vel : ctype.value_type<cc.Vec3> 
        ) : void;

        /**
         * Apply a central impulse.<br>
         * -- param   impulse the value of the impulse
         * @method applyCentralImpulse
         * @param {ctype.value_type<cc.Vec3>} _impulse
         */
        public applyCentralImpulse (
            _impulse : ctype.value_type<cc.Vec3> 
        ) : void;

        /**
         *  Get the acceleration. 
         * @method getGravity
         * @return {ctype.value_type<cc.Vec3>}
         */
        public getGravity (
        ) : ctype.value_type<cc.Vec3>;

        /**
         *  Get rolling friction. 
         * @method getRollingFriction
         * @return {number}
         */
        public getRollingFriction (
        ) : number;

        /**
         *  Set the center of mass. 
         * @method setCenterOfMassTransform
         * @param {ctype.value_type<cc.Mat4>} _xform
         */
        public setCenterOfMassTransform (
            _xform : ctype.value_type<cc.Mat4> 
        ) : void;

        /**
         *  Set the inverse of local inertia. 
         * @method setInvInertiaDiagLocal
         * @param {ctype.value_type<cc.Vec3>} _diagInvInertia
         */
        public setInvInertiaDiagLocal (
            _diagInvInertia : ctype.value_type<cc.Vec3> 
        ) : void;

        /**
         * 
         * @method removeConstraint
         * @param {(number)} _idx | constraint
         */
        public removeConstraint (
            _idx_constraint : (number)
        ) : void;

        /**
         *  Get total torque. 
         * @method getTotalTorque
         * @return {ctype.value_type<cc.Vec3>}
         */
        public getTotalTorque (
        ) : ctype.value_type<cc.Vec3>;

        /**
         *  Get inverse of mass. 
         * @method getInvMass
         * @return {number}
         */
        public getInvMass (
        ) : number;

        /**
         *  Get constraint by index. 
         * @method getConstraint
         * @param {number} _idx
         * @return {number}
         */
        public getConstraint (
            _idx : number 
        ) : number;

        /**
         *  Get restitution. 
         * @method getRestitution
         * @return {number}
         */
        public getRestitution (
        ) : number;

        /**
         *  Get swept sphere radius. 
         * @method getCcdSweptSphereRadius
         * @return {number}
         */
        public getCcdSweptSphereRadius (
        ) : number;

        /**
         *  Get hit friction. 
         * @method getHitFraction
         * @return {number}
         */
        public getHitFraction (
        ) : number;

        /**
         *  Get angular damping. 
         * @method getAngularDamping
         * @return {number}
         */
        public getAngularDamping (
        ) : number;

        /**
         *  Get the inverse of local inertia. 
         * @method getInvInertiaDiagLocal
         * @return {ctype.value_type<cc.Vec3>}
         */
        public getInvInertiaDiagLocal (
        ) : ctype.value_type<cc.Vec3>;

        /**
         *  Get the center of mass. 
         * @method getCenterOfMassTransform
         * @return {ctype.value_type<cc.Mat4>}
         */
        public getCenterOfMassTransform (
        ) : ctype.value_type<cc.Mat4>;

        /**
         *  Get the angular factor. 
         * @method getAngularFactor
         * @return {ctype.value_type<cc.Vec3>}
         */
        public getAngularFactor (
        ) : ctype.value_type<cc.Vec3>;

        /**
         *  Set restitution. 
         * @method setRestitution
         * @param {number} _rest
         */
        public setRestitution (
            _rest : number 
        ) : void;

        /**
         *  Set hit friction. 
         * @method setHitFraction
         * @param {number} _hitFraction
         */
        public setHitFraction (
            _hitFraction : number 
        ) : void;

        /**
         *  Get linear damping. 
         * @method getLinearDamping
         * @return {number}
         */
        public getLinearDamping (
        ) : number;

        /**
         * 
         * @method Physics3DRigidBody
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class Physics3DComponent
     * @native
     */
    export class Physics3DComponent 
        extends cc.Component
    {

        /**
         * synchronize node transformation to physics
         * @method syncNodeToPhysics
         */
        public syncNodeToPhysics (
        ) : void;

        /**
         * add this component to physics world, called by scene
         * @method addToPhysicsWorld
         * @param {cc.Physics3DWorld} _world
         */
        public addToPhysicsWorld (
            _world : cc.Physics3DWorld 
        ) : void;

        /**
         * synchronize physics transformation to node
         * @method syncPhysicsToNode
         */
        public syncPhysicsToNode (
        ) : void;

        /**
         * get physics object
         * @method getPhysics3DObject
         * @return {cc.Physics3DObject}
         */
        public getPhysics3DObject (
        ) : cc.Physics3DObject;

        /**
         * set Physics object to the component
         * @method setPhysics3DObject
         * @param {cc.Physics3DObject} _physicsObj
         */
        public setPhysics3DObject (
            _physicsObj : cc.Physics3DObject 
        ) : void;

        /**
         * synchronization between node and physics is time consuming, you can skip some synchronization using this function
         * @method setSyncFlag
         * @param {cc.Physics3DComponent::PhysicsSyncFlag} _syncFlag
         */
        public setSyncFlag (
            _syncFlag : number 
        ) : void;

        /**
         * The node's transform in physics object space
         * @method setTransformInPhysics
         * @param {ctype.value_type<cc.Vec3>} _translateInPhysics
         * @param {cc.Quaternion} _rotInPhsyics
         */
        public setTransformInPhysics (
            _translateInPhysics : ctype.value_type<cc.Vec3>, 
            _rotInPhsyics : cc.Quaternion 
        ) : void;

        /**
         * create Physics3DComponent<br>
         * -- param physicsObj pointer to a Physics object contain in the component<br>
         * -- param translateInPhysics offset that the owner node in the physics object's space<br>
         * -- param rotInPhsyics offset rotation that the owner node in the physics object's space<br>
         * -- return created Physics3DComponent
         * @method create
         * @param {(cc.Physics3DObject)} _physicsObj?
         * @param {(ctype.value_type<cc.Vec3>)} _translateInPhysics?
         * @param {(cc.Quaternion)} _rotInPhsyics?
         * @return {cc.Physics3DComponent}
         */
        public static create (
            _physicsObj? : (cc.Physics3DObject), 
            _translateInPhysics? : (ctype.value_type<cc.Vec3>), 
            _rotInPhsyics? : (cc.Quaternion)
        ) : cc.Physics3DComponent;

        /**
         * get the component name, it is used to find whether it is Physics3DComponent
         * @method getPhysics3DComponentName
         * @return {string}
         */
        public static getPhysics3DComponentName (
        ) : string;

        /**
         * 
         * @method Physics3DComponent
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class PhysicsSprite3D
     * @native
     */
    export class PhysicsSprite3D 
        extends cc.Sprite3D
    {

        /**
         *  synchronize node transformation to physics. 
         * @method syncNodeToPhysics
         */
        public syncNodeToPhysics (
        ) : void;

        /**
         *  synchronize physics transformation to node. 
         * @method syncPhysicsToNode
         */
        public syncPhysicsToNode (
        ) : void;

        /**
         *  Get the Physics3DObject. 
         * @method getPhysicsObj
         * @return {cc.Physics3DObject}
         */
        public getPhysicsObj (
        ) : cc.Physics3DObject;

        /**
         *  Set synchronization flag, see Physics3DComponent. 
         * @method setSyncFlag
         * @param {cc.Physics3DComponent::PhysicsSyncFlag} _syncFlag
         */
        public setSyncFlag (
            _syncFlag : number 
        ) : void;

        /**
         * 
         * @method PhysicsSprite3D
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class Physics3DWorld
     * @native
     */
    export class Physics3DWorld 
    {

        /**
         *  set gravity for the physics world 
         * @method setGravity
         * @param {ctype.value_type<cc.Vec3>} _gravity
         */
        public setGravity (
            _gravity : ctype.value_type<cc.Vec3> 
        ) : void;

        /**
         *  Simulate one frame. 
         * @method stepSimulate
         * @param {number} _dt
         */
        public stepSimulate (
            _dt : number 
        ) : void;

        /**
         * 
         * @method needCollisionChecking
         * @return {boolean}
         */
        public needCollisionChecking (
        ) : boolean;

        /**
         * 
         * @method collisionChecking
         */
        public collisionChecking (
        ) : void;

        /**
         * 
         * @method setGhostPairCallback
         */
        public setGhostPairCallback (
        ) : void;

        /**
         * 
         * @method init
         * @param {cc.Physics3DWorldDes} _info
         * @return {boolean}
         */
        public init (
            _info : cc.Physics3DWorldDes 
        ) : boolean;

        /**
         *  Remove all Physics3DObjects. 
         * @method removeAllPhysics3DObjects
         */
        public removeAllPhysics3DObjects (
        ) : void;

        /**
         *  Check debug drawing is enabled. 
         * @method isDebugDrawEnabled
         * @return {boolean}
         */
        public isDebugDrawEnabled (
        ) : boolean;

        /**
         *  Remove all Physics3DConstraint. 
         * @method removeAllPhysics3DConstraints
         */
        public removeAllPhysics3DConstraints (
        ) : void;

        /**
         *  get current gravity 
         * @method getGravity
         * @return {ctype.value_type<cc.Vec3>}
         */
        public getGravity (
        ) : ctype.value_type<cc.Vec3>;

        /**
         *  Remove a Physics3DConstraint. 
         * @method removePhysics3DConstraint
         * @param {number} _constraint
         */
        public removePhysics3DConstraint (
            _constraint : number 
        ) : void;

        /**
         *  Add a Physics3DObject. 
         * @method addPhysics3DObject
         * @param {cc.Physics3DObject} _physicsObj
         */
        public addPhysics3DObject (
            _physicsObj : cc.Physics3DObject 
        ) : void;

        /**
         *  Enable or disable debug drawing. 
         * @method setDebugDrawEnable
         * @param {boolean} _enableDebugDraw
         */
        public setDebugDrawEnable (
            _enableDebugDraw : boolean 
        ) : void;

        /**
         *  Remove a Physics3DObject. 
         * @method removePhysics3DObject
         * @param {cc.Physics3DObject} _physicsObj
         */
        public removePhysics3DObject (
            _physicsObj : cc.Physics3DObject 
        ) : void;

        /**
         * 
         * @method getPhysicsObject
         * @param {btCollisionObject} _btObj
         * @return {cc.Physics3DObject}
         */
        public getPhysicsObject (
            _btObj : btCollisionObject 
        ) : cc.Physics3DObject;

        /**
         *  Add a Physics3DConstraint. 
         * @method addPhysics3DConstraint
         * @param {number} _constraint
         * @param {boolean} _disableCollisionsBetweenLinkedObjs
         */
        public addPhysics3DConstraint (
            _constraint : number, 
            _disableCollisionsBetweenLinkedObjs : boolean 
        ) : void;

        /**
         *  Internal method, the updater of debug drawing, need called each frame. 
         * @method debugDraw
         * @param {cc.Renderer} _renderer
         */
        public debugDraw (
            _renderer : any 
        ) : void;

        /**
         *  Performs a swept shape cast on all objects in the Physics3DWorld. 
         * @method sweepShape
         * @param {cc.Physics3DShape} _shape
         * @param {ctype.value_type<cc.Mat4>} _startTransform
         * @param {ctype.value_type<cc.Mat4>} _endTransform
         * @param {cc.Physics3DWorld::HitResult} _result
         * @return {boolean}
         */
        public sweepShape (
            _shape : cc.Physics3DShape, 
            _startTransform : ctype.value_type<cc.Mat4>, 
            _endTransform : ctype.value_type<cc.Mat4>, 
            _result : any 
        ) : boolean;

        /**
         * Creates a Physics3DWorld with Physics3DWorldDes. <br>
         * -- return An autoreleased Physics3DWorld object.
         * @method create
         * @param {cc.Physics3DWorldDes} _info
         * @return {cc.Physics3DWorld}
         */
        public static create (
            _info : cc.Physics3DWorldDes 
        ) : cc.Physics3DWorld;

        /**
         * 
         * @method Physics3DWorld
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class Physics3DConstraint
     * @native
     */
    export class Physics3DConstraint 
    {

        /**
         * set enable or not
         * @method setEnabled
         * @param {boolean} _enabled
         */
        public setEnabled (
            _enabled : boolean 
        ) : void;

        /**
         * set the impulse that break the constraint
         * @method setBreakingImpulse
         * @param {number} _impulse
         */
        public setBreakingImpulse (
            _impulse : number 
        ) : void;

        /**
         * get user data
         * @method getUserData
         * @return {void}
         */
        public getUserData (
        ) : void;

        /**
         * get the impulse that break the constraint
         * @method getBreakingImpulse
         * @return {number}
         */
        public getBreakingImpulse (
        ) : number;

        /**
         * get rigid body a
         * @method getBodyA
         * @return {cc.Physics3DRigidBody}
         */
        public getBodyA (
        ) : cc.Physics3DRigidBody;

        /**
         * is it enabled
         * @method isEnabled
         * @return {boolean}
         */
        public isEnabled (
        ) : boolean;

        /**
         * get override number of solver iterations
         * @method getOverrideNumSolverIterations
         * @return {number}
         */
        public getOverrideNumSolverIterations (
        ) : number;

        /**
         * get rigid body b
         * @method getBodyB
         * @return {cc.Physics3DRigidBody}
         */
        public getBodyB (
        ) : cc.Physics3DRigidBody;

        /**
         * override the number of constraint solver iterations used to solve this constraint, -1 will use the default number of iterations, as specified in SolverInfo.m_numIterations
         * @method setOverrideNumSolverIterations
         * @param {number} _overideNumIterations
         */
        public setOverrideNumSolverIterations (
            _overideNumIterations : number 
        ) : void;

        /**
         * get constraint type
         * @method getConstraintType
         * @return {number}
         */
        public getConstraintType (
        ) : number;

        /**
         * get user data
         * @method setUserData
         * @param {void} _userData
         */
        public setUserData (
            _userData : void 
        ) : void;

        /**
         * 
         * @method getbtContraint
         * @return {number}
         */
        public getbtContraint (
        ) : number;

    }
    /**
     * @class Physics3DPointToPointConstraint
     * @native
     */
    export class Physics3DPointToPointConstraint 
        extends cc.Physics3DConstraint
    {

        /**
         * get pivot point in A's local space
         * @method getPivotPointInA
         * @return {ctype.value_type<cc.Vec3>}
         */
        public getPivotPointInA (
        ) : ctype.value_type<cc.Vec3>;

        /**
         * get pivot point in B's local space
         * @method getPivotPointInB
         * @return {ctype.value_type<cc.Vec3>}
         */
        public getPivotPointInB (
        ) : ctype.value_type<cc.Vec3>;

        /**
         * 
         * @method init
         * @param {(cc.Physics3DRigidBody)} _rbA
         * @param {(cc.Physics3DRigidBody) | (ctype.value_type<cc.Vec3>)} _rbB | pivotPointInA
         * @param {(ctype.value_type<cc.Vec3>)} _pivotPointInA?
         * @param {(ctype.value_type<cc.Vec3>)} _pivotPointInB?
         * @return {boolean}
         */
        public init (
            _rbA : (cc.Physics3DRigidBody), 
            _rbB_pivotPointInA : (cc.Physics3DRigidBody) | (ctype.value_type<cc.Vec3>), 
            _pivotPointInA? : (ctype.value_type<cc.Vec3>), 
            _pivotPointInB? : (ctype.value_type<cc.Vec3>)
        ) : boolean;

        /**
         * set pivot point in A's local space
         * @method setPivotPointInA
         * @param {ctype.value_type<cc.Vec3>} _pivotA
         */
        public setPivotPointInA (
            _pivotA : ctype.value_type<cc.Vec3> 
        ) : void;

        /**
         * set pivot point in B's local space
         * @method setPivotPointInB
         * @param {ctype.value_type<cc.Vec3>} _pivotB
         */
        public setPivotPointInB (
            _pivotB : ctype.value_type<cc.Vec3> 
        ) : void;

        /**
         * create point to point constraint, make the local pivot points of 2 rigid bodies match in worldspace.<br>
         * -- param rbA The rigid body A going to be fixed<br>
         * -- param rbB The rigid body B going to be fixed<br>
         * -- param pivotPointInA local pivot point in A's local space<br>
         * -- param pivotPointInB local pivot point in B's local space<br>
         * -- return created constraint
         * @method create
         * @param {(cc.Physics3DRigidBody)} _rbA
         * @param {(cc.Physics3DRigidBody) | (ctype.value_type<cc.Vec3>)} _rbB | pivotPointInA
         * @param {(ctype.value_type<cc.Vec3>)} _pivotPointInA?
         * @param {(ctype.value_type<cc.Vec3>)} _pivotPointInB?
         * @return {number}
         */
        public static create (
            _rbA : (cc.Physics3DRigidBody), 
            _rbB_pivotPointInA : (cc.Physics3DRigidBody) | (ctype.value_type<cc.Vec3>), 
            _pivotPointInA? : (ctype.value_type<cc.Vec3>), 
            _pivotPointInB? : (ctype.value_type<cc.Vec3>)
        ) : number;

        /**
         * 
         * @method Physics3DPointToPointConstraint
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class Physics3DHingeConstraint
     * @native
     */
    export class Physics3DHingeConstraint 
        extends cc.Physics3DConstraint
    {

        /**
         * get hinge angle
         * @method getHingeAngle
         * @param {(ctype.value_type<cc.Mat4>)} _transA?
         * @param {(ctype.value_type<cc.Mat4>)} _transB?
         * @return {number}
         */
        public getHingeAngle (
            _transA? : (ctype.value_type<cc.Mat4>), 
            _transB? : (ctype.value_type<cc.Mat4>)
        ) : number;

        /**
         * get motor target velocity
         * @method getMotorTargetVelosity
         * @return {number}
         */
        public getMotorTargetVelosity (
        ) : number;

        /**
         * get rigid body A's frame offset
         * @method getFrameOffsetA
         * @return {ctype.value_type<cc.Mat4>}
         */
        public getFrameOffsetA (
        ) : ctype.value_type<cc.Mat4>;

        /**
         * get rigid body B's frame offset
         * @method getFrameOffsetB
         * @return {ctype.value_type<cc.Mat4>}
         */
        public getFrameOffsetB (
        ) : ctype.value_type<cc.Mat4>;

        /**
         *  set max motor impulse 
         * @method setMaxMotorImpulse
         * @param {number} _maxMotorImpulse
         */
        public setMaxMotorImpulse (
            _maxMotorImpulse : number 
        ) : void;

        /**
         *  enable angular motor 
         * @method enableAngularMotor
         * @param {boolean} _enableMotor
         * @param {number} _targetVelocity
         * @param {number} _maxMotorImpulse
         */
        public enableAngularMotor (
            _enableMotor : boolean, 
            _targetVelocity : number, 
            _maxMotorImpulse : number 
        ) : void;

        /**
         * get upper limit
         * @method getUpperLimit
         * @return {number}
         */
        public getUpperLimit (
        ) : number;

        /**
         * get max motor impulse
         * @method getMaxMotorImpulse
         * @return {number}
         */
        public getMaxMotorImpulse (
        ) : number;

        /**
         * get lower limit
         * @method getLowerLimit
         * @return {number}
         */
        public getLowerLimit (
        ) : number;

        /**
         * set use frame offset
         * @method setUseFrameOffset
         * @param {boolean} _frameOffsetOnOff
         */
        public setUseFrameOffset (
            _frameOffsetOnOff : boolean 
        ) : void;

        /**
         * get enable angular motor
         * @method getEnableAngularMotor
         * @return {boolean}
         */
        public getEnableAngularMotor (
        ) : boolean;

        /**
         * 
         * @method enableMotor
         * @param {boolean} _enableMotor
         */
        public enableMotor (
            _enableMotor : boolean 
        ) : void;

        /**
         * get B's frame
         * @method getBFrame
         * @return {ctype.value_type<cc.Mat4>}
         */
        public getBFrame (
        ) : ctype.value_type<cc.Mat4>;

        /**
         * set frames for rigid body A and B
         * @method setFrames
         * @param {ctype.value_type<cc.Mat4>} _frameA
         * @param {ctype.value_type<cc.Mat4>} _frameB
         */
        public setFrames (
            _frameA : ctype.value_type<cc.Mat4>, 
            _frameB : ctype.value_type<cc.Mat4> 
        ) : void;

        /**
         *  access for UseFrameOffset
         * @method getUseFrameOffset
         * @return {boolean}
         */
        public getUseFrameOffset (
        ) : boolean;

        /**
         * set angular only
         * @method setAngularOnly
         * @param {boolean} _angularOnly
         */
        public setAngularOnly (
            _angularOnly : boolean 
        ) : void;

        /**
         *  set limit 
         * @method setLimit
         * @param {number} _low
         * @param {number} _high
         * @param {number} __softness
         * @param {number} __biasFactor
         * @param {number} __relaxationFactor
         */
        public setLimit (
            _low : number, 
            _high : number, 
            __softness : number, 
            __biasFactor : number, 
            __relaxationFactor : number 
        ) : void;

        /**
         *  set motor target 
         * @method setMotorTarget
         * @param {(number) | (cc.Quaternion)} _targetAngle | qAinB
         * @param {(number)} _dt
         */
        public setMotorTarget (
            _targetAngle_qAinB : (number) | (cc.Quaternion), 
            _dt : (number)
        ) : void;

        /**
         * get angular only
         * @method getAngularOnly
         * @return {boolean}
         */
        public getAngularOnly (
        ) : boolean;

        /**
         * set axis
         * @method setAxis
         * @param {ctype.value_type<cc.Vec3>} _axisInA
         */
        public setAxis (
            _axisInA : ctype.value_type<cc.Vec3> 
        ) : void;

        /**
         * get A's frame 
         * @method getAFrame
         * @return {ctype.value_type<cc.Mat4>}
         */
        public getAFrame (
        ) : ctype.value_type<cc.Mat4>;

        /**
         * create hinge constraint<br>
         * -- param rbA rigid body A<br>
         * -- param pivotInA pivot in rigid body A's local space<br>
         * -- param axisInA axis in rigid body A's local space<br>
         * -- param useReferenceFrameA use frame A as reference
         * @method create
         * @param {(cc.Physics3DRigidBody)} _rbA
         * @param {(ctype.value_type<cc.Vec3>) | (ctype.value_type<cc.Mat4>) | (cc.Physics3DRigidBody)} _pivotInA | rbAFrame | rbB
         * @param {(ctype.value_type<cc.Vec3>) | (boolean) | (ctype.value_type<cc.Mat4>)} _axisInA | useReferenceFrameA | pivotInA | rbAFrame
         * @param {(boolean) | (ctype.value_type<cc.Vec3>) | (ctype.value_type<cc.Mat4>)} _useReferenceFrameA | pivotInB | rbBFrame?
         * @param {(ctype.value_type<cc.Vec3>) | (boolean)} _axisInA | useReferenceFrameA?
         * @param {(ctype.value_type<cc.Vec3>)} _axisInB?
         * @param {(boolean)} _useReferenceFrameA?
         * @return {number}
         */
        public static create (
            _rbA : (cc.Physics3DRigidBody), 
            _pivotInA_rbAFrame_rbB : (ctype.value_type<cc.Vec3>) | (ctype.value_type<cc.Mat4>) | (cc.Physics3DRigidBody), 
            _axisInA_useReferenceFrameA_pivotInA_rbAFrame : (ctype.value_type<cc.Vec3>) | (boolean) | (ctype.value_type<cc.Mat4>), 
            _useReferenceFrameA_pivotInB_rbBFrame? : (boolean) | (ctype.value_type<cc.Vec3>) | (ctype.value_type<cc.Mat4>), 
            _axisInA_useReferenceFrameA? : (ctype.value_type<cc.Vec3>) | (boolean), 
            _axisInB? : (ctype.value_type<cc.Vec3>), 
            _useReferenceFrameA? : (boolean)
        ) : number;

        /**
         * 
         * @method Physics3DHingeConstraint
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class Physics3DSliderConstraint
     * @native
     */
    export class Physics3DSliderConstraint 
        extends cc.Physics3DConstraint
    {

        /**
         * 
         * @method setPoweredAngMotor
         * @param {boolean} _onOff
         */
        public setPoweredAngMotor (
            _onOff : boolean 
        ) : void;

        /**
         * 
         * @method getDampingLimAng
         * @return {number}
         */
        public getDampingLimAng (
        ) : number;

        /**
         * 
         * @method setRestitutionOrthoLin
         * @param {number} _restitutionOrthoLin
         */
        public setRestitutionOrthoLin (
            _restitutionOrthoLin : number 
        ) : void;

        /**
         * 
         * @method setRestitutionDirLin
         * @param {number} _restitutionDirLin
         */
        public setRestitutionDirLin (
            _restitutionDirLin : number 
        ) : void;

        /**
         * 
         * @method getLinearPos
         * @return {number}
         */
        public getLinearPos (
        ) : number;

        /**
         * get A's frame offset
         * @method getFrameOffsetA
         * @return {ctype.value_type<cc.Mat4>}
         */
        public getFrameOffsetA (
        ) : ctype.value_type<cc.Mat4>;

        /**
         * get B's frame offset
         * @method getFrameOffsetB
         * @return {ctype.value_type<cc.Mat4>}
         */
        public getFrameOffsetB (
        ) : ctype.value_type<cc.Mat4>;

        /**
         * 
         * @method setPoweredLinMotor
         * @param {boolean} _onOff
         */
        public setPoweredLinMotor (
            _onOff : boolean 
        ) : void;

        /**
         * 
         * @method getDampingDirAng
         * @return {number}
         */
        public getDampingDirAng (
        ) : number;

        /**
         * 
         * @method getRestitutionLimLin
         * @return {number}
         */
        public getRestitutionLimLin (
        ) : number;

        /**
         * 
         * @method getSoftnessOrthoAng
         * @return {number}
         */
        public getSoftnessOrthoAng (
        ) : number;

        /**
         * 
         * @method setSoftnessOrthoLin
         * @param {number} _softnessOrthoLin
         */
        public setSoftnessOrthoLin (
            _softnessOrthoLin : number 
        ) : void;

        /**
         * 
         * @method setSoftnessLimLin
         * @param {number} _softnessLimLin
         */
        public setSoftnessLimLin (
            _softnessLimLin : number 
        ) : void;

        /**
         * 
         * @method getAngularPos
         * @return {number}
         */
        public getAngularPos (
        ) : number;

        /**
         * 
         * @method setRestitutionLimAng
         * @param {number} _restitutionLimAng
         */
        public setRestitutionLimAng (
            _restitutionLimAng : number 
        ) : void;

        /**
         * set upper linear limit
         * @method setUpperLinLimit
         * @param {number} _upperLimit
         */
        public setUpperLinLimit (
            _upperLimit : number 
        ) : void;

        /**
         * 
         * @method setDampingDirLin
         * @param {number} _dampingDirLin
         */
        public setDampingDirLin (
            _dampingDirLin : number 
        ) : void;

        /**
         * get upper angular limit
         * @method getUpperAngLimit
         * @return {number}
         */
        public getUpperAngLimit (
        ) : number;

        /**
         * 
         * @method getDampingDirLin
         * @return {number}
         */
        public getDampingDirLin (
        ) : number;

        /**
         * 
         * @method getSoftnessDirAng
         * @return {number}
         */
        public getSoftnessDirAng (
        ) : number;

        /**
         * 
         * @method getPoweredAngMotor
         * @return {boolean}
         */
        public getPoweredAngMotor (
        ) : boolean;

        /**
         * set lower angular limit
         * @method setLowerAngLimit
         * @param {number} _lowerLimit
         */
        public setLowerAngLimit (
            _lowerLimit : number 
        ) : void;

        /**
         * set upper angular limit
         * @method setUpperAngLimit
         * @param {number} _upperLimit
         */
        public setUpperAngLimit (
            _upperLimit : number 
        ) : void;

        /**
         * 
         * @method setTargetLinMotorVelocity
         * @param {number} _targetLinMotorVelocity
         */
        public setTargetLinMotorVelocity (
            _targetLinMotorVelocity : number 
        ) : void;

        /**
         * 
         * @method setDampingLimAng
         * @param {number} _dampingLimAng
         */
        public setDampingLimAng (
            _dampingLimAng : number 
        ) : void;

        /**
         * 
         * @method getRestitutionLimAng
         * @return {number}
         */
        public getRestitutionLimAng (
        ) : number;

        /**
         *  access for UseFrameOffset
         * @method getUseFrameOffset
         * @return {boolean}
         */
        public getUseFrameOffset (
        ) : boolean;

        /**
         * 
         * @method getSoftnessOrthoLin
         * @return {number}
         */
        public getSoftnessOrthoLin (
        ) : number;

        /**
         * 
         * @method getDampingOrthoAng
         * @return {number}
         */
        public getDampingOrthoAng (
        ) : number;

        /**
         * set use frame offset
         * @method setUseFrameOffset
         * @param {boolean} _frameOffsetOnOff
         */
        public setUseFrameOffset (
            _frameOffsetOnOff : boolean 
        ) : void;

        /**
         * set lower linear limit
         * @method setLowerLinLimit
         * @param {number} _lowerLimit
         */
        public setLowerLinLimit (
            _lowerLimit : number 
        ) : void;

        /**
         * 
         * @method getRestitutionDirLin
         * @return {number}
         */
        public getRestitutionDirLin (
        ) : number;

        /**
         * 
         * @method getTargetLinMotorVelocity
         * @return {number}
         */
        public getTargetLinMotorVelocity (
        ) : number;

        /**
         * get lower linear limit
         * @method getLowerLinLimit
         * @return {number}
         */
        public getLowerLinLimit (
        ) : number;

        /**
         * 
         * @method getSoftnessLimLin
         * @return {number}
         */
        public getSoftnessLimLin (
        ) : number;

        /**
         * 
         * @method setDampingOrthoAng
         * @param {number} _dampingOrthoAng
         */
        public setDampingOrthoAng (
            _dampingOrthoAng : number 
        ) : void;

        /**
         * 
         * @method setSoftnessDirAng
         * @param {number} _softnessDirAng
         */
        public setSoftnessDirAng (
            _softnessDirAng : number 
        ) : void;

        /**
         * 
         * @method getPoweredLinMotor
         * @return {boolean}
         */
        public getPoweredLinMotor (
        ) : boolean;

        /**
         * 
         * @method setRestitutionOrthoAng
         * @param {number} _restitutionOrthoAng
         */
        public setRestitutionOrthoAng (
            _restitutionOrthoAng : number 
        ) : void;

        /**
         * 
         * @method setDampingDirAng
         * @param {number} _dampingDirAng
         */
        public setDampingDirAng (
            _dampingDirAng : number 
        ) : void;

        /**
         * set frames for rigid body A and B
         * @method setFrames
         * @param {ctype.value_type<cc.Mat4>} _frameA
         * @param {ctype.value_type<cc.Mat4>} _frameB
         */
        public setFrames (
            _frameA : ctype.value_type<cc.Mat4>, 
            _frameB : ctype.value_type<cc.Mat4> 
        ) : void;

        /**
         * 
         * @method getRestitutionOrthoAng
         * @return {number}
         */
        public getRestitutionOrthoAng (
        ) : number;

        /**
         * 
         * @method getMaxAngMotorForce
         * @return {number}
         */
        public getMaxAngMotorForce (
        ) : number;

        /**
         * 
         * @method getDampingOrthoLin
         * @return {number}
         */
        public getDampingOrthoLin (
        ) : number;

        /**
         * get upper linear limit
         * @method getUpperLinLimit
         * @return {number}
         */
        public getUpperLinLimit (
        ) : number;

        /**
         * 
         * @method setMaxLinMotorForce
         * @param {number} _maxLinMotorForce
         */
        public setMaxLinMotorForce (
            _maxLinMotorForce : number 
        ) : void;

        /**
         * 
         * @method getRestitutionOrthoLin
         * @return {number}
         */
        public getRestitutionOrthoLin (
        ) : number;

        /**
         * 
         * @method setTargetAngMotorVelocity
         * @param {number} _targetAngMotorVelocity
         */
        public setTargetAngMotorVelocity (
            _targetAngMotorVelocity : number 
        ) : void;

        /**
         * 
         * @method getSoftnessLimAng
         * @return {number}
         */
        public getSoftnessLimAng (
        ) : number;

        /**
         * 
         * @method setRestitutionDirAng
         * @param {number} _restitutionDirAng
         */
        public setRestitutionDirAng (
            _restitutionDirAng : number 
        ) : void;

        /**
         * 
         * @method getDampingLimLin
         * @return {number}
         */
        public getDampingLimLin (
        ) : number;

        /**
         * get lower angular limit
         * @method getLowerAngLimit
         * @return {number}
         */
        public getLowerAngLimit (
        ) : number;

        /**
         * 
         * @method getRestitutionDirAng
         * @return {number}
         */
        public getRestitutionDirAng (
        ) : number;

        /**
         * 
         * @method getTargetAngMotorVelocity
         * @return {number}
         */
        public getTargetAngMotorVelocity (
        ) : number;

        /**
         * 
         * @method setRestitutionLimLin
         * @param {number} _restitutionLimLin
         */
        public setRestitutionLimLin (
            _restitutionLimLin : number 
        ) : void;

        /**
         * 
         * @method getMaxLinMotorForce
         * @return {number}
         */
        public getMaxLinMotorForce (
        ) : number;

        /**
         * 
         * @method setDampingOrthoLin
         * @param {number} _dampingOrthoLin
         */
        public setDampingOrthoLin (
            _dampingOrthoLin : number 
        ) : void;

        /**
         * 
         * @method setSoftnessOrthoAng
         * @param {number} _softnessOrthoAng
         */
        public setSoftnessOrthoAng (
            _softnessOrthoAng : number 
        ) : void;

        /**
         * 
         * @method setDampingLimLin
         * @param {number} _dampingLimLin
         */
        public setDampingLimLin (
            _dampingLimLin : number 
        ) : void;

        /**
         * 
         * @method setSoftnessDirLin
         * @param {number} _softnessDirLin
         */
        public setSoftnessDirLin (
            _softnessDirLin : number 
        ) : void;

        /**
         * 
         * @method setMaxAngMotorForce
         * @param {number} _maxAngMotorForce
         */
        public setMaxAngMotorForce (
            _maxAngMotorForce : number 
        ) : void;

        /**
         * 
         * @method getSoftnessDirLin
         * @return {number}
         */
        public getSoftnessDirLin (
        ) : number;

        /**
         * 
         * @method setSoftnessLimAng
         * @param {number} _softnessLimAng
         */
        public setSoftnessLimAng (
            _softnessLimAng : number 
        ) : void;

        /**
         * use A's frame as linear reference
         * @method getUseLinearReferenceFrameA
         * @return {boolean}
         */
        public getUseLinearReferenceFrameA (
        ) : boolean;

        /**
         * create slider constraint<br>
         * -- param rbA rigid body A<br>
         * -- param rbB rigid body B<br>
         * -- param frameInA frame in A's local space<br>
         * -- param frameInB frame in B's local space<br>
         * -- param useLinearReferenceFrameA use fixed frame A for linear limits
         * @method create
         * @param {cc.Physics3DRigidBody} _rbA
         * @param {cc.Physics3DRigidBody} _rbB
         * @param {ctype.value_type<cc.Mat4>} _frameInA
         * @param {ctype.value_type<cc.Mat4>} _frameInB
         * @param {boolean} _useLinearReferenceFrameA
         * @return {number}
         */
        public static create (
            _rbA : cc.Physics3DRigidBody, 
            _rbB : cc.Physics3DRigidBody, 
            _frameInA : ctype.value_type<cc.Mat4>, 
            _frameInB : ctype.value_type<cc.Mat4>, 
            _useLinearReferenceFrameA : boolean 
        ) : number;

        /**
         * 
         * @method Physics3DSliderConstraint
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class Physics3DConeTwistConstraint
     * @native
     */
    export class Physics3DConeTwistConstraint 
        extends cc.Physics3DConstraint
    {

        /**
         * get B's frame
         * @method getBFrame
         * @return {ctype.value_type<cc.Mat4>}
         */
        public getBFrame (
        ) : ctype.value_type<cc.Mat4>;

        /**
         * set fix thresh
         * @method setFixThresh
         * @param {number} _fixThresh
         */
        public setFixThresh (
            _fixThresh : number 
        ) : void;

        /**
         * get B's frame offset
         * @method getFrameOffsetB
         * @return {ctype.value_type<cc.Mat4>}
         */
        public getFrameOffsetB (
        ) : ctype.value_type<cc.Mat4>;

        /**
         * get A's frame offset
         * @method getFrameOffsetA
         * @return {ctype.value_type<cc.Mat4>}
         */
        public getFrameOffsetA (
        ) : ctype.value_type<cc.Mat4>;

        /**
         * get fix thresh
         * @method getFixThresh
         * @return {number}
         */
        public getFixThresh (
        ) : number;

        /**
         * get swing span2
         * @method getSwingSpan2
         * @return {number}
         */
        public getSwingSpan2 (
        ) : number;

        /**
         * get swing span1
         * @method getSwingSpan1
         * @return {number}
         */
        public getSwingSpan1 (
        ) : number;

        /**
         * set max motor impulse
         * @method setMaxMotorImpulse
         * @param {number} _maxMotorImpulse
         */
        public setMaxMotorImpulse (
            _maxMotorImpulse : number 
        ) : void;

        /**
         * set A and B's frame
         * @method setFrames
         * @param {ctype.value_type<cc.Mat4>} _frameA
         * @param {ctype.value_type<cc.Mat4>} _frameB
         */
        public setFrames (
            _frameA : ctype.value_type<cc.Mat4>, 
            _frameB : ctype.value_type<cc.Mat4> 
        ) : void;

        /**
         * get twist angle
         * @method getTwistAngle
         * @return {number}
         */
        public getTwistAngle (
        ) : number;

        /**
         * get point for angle
         * @method GetPointForAngle
         * @param {number} _fAngleInRadians
         * @param {number} _fLength
         * @return {ctype.value_type<cc.Vec3>}
         */
        public GetPointForAngle (
            _fAngleInRadians : number, 
            _fLength : number 
        ) : ctype.value_type<cc.Vec3>;

        /**
         * set max motor impulse normalize
         * @method setMaxMotorImpulseNormalized
         * @param {number} _maxMotorImpulse
         */
        public setMaxMotorImpulseNormalized (
            _maxMotorImpulse : number 
        ) : void;

        /**
         * get twist span
         * @method getTwistSpan
         * @return {number}
         */
        public getTwistSpan (
        ) : number;

        /**
         * set damping
         * @method setDamping
         * @param {number} _damping
         */
        public setDamping (
            _damping : number 
        ) : void;

        /**
         * set limits<br>
         * -- param swingSpan1 swing span1<br>
         * -- param swingSpan2 swing span2<br>
         * -- param twistSpan twist span<br>
         * -- param softness 0->1, recommend ~0.8->1. Describes % of limits where movement is free. Beyond this softness %, the limit is gradually enforced until the "hard" (1.0) limit is reached.<br>
         * -- param biasFactor 0->1?, recommend 0.3 +/-0.3 or so. Strength with which constraint resists zeroth order (angular, not angular velocity) limit violation.<br>
         * -- param relaxationFactor 0->1, recommend to stay near 1. the lower the value, the less the constraint will fight velocities which violate the angular limits.
         * @method setLimit
         * @param {number} _swingSpan1
         * @param {number} _swingSpan2
         * @param {number} _twistSpan
         * @param {number} _softness
         * @param {number} _biasFactor
         * @param {number} _relaxationFactor
         */
        public setLimit (
            _swingSpan1 : number, 
            _swingSpan2 : number, 
            _twistSpan : number, 
            _softness : number, 
            _biasFactor : number, 
            _relaxationFactor : number 
        ) : void;

        /**
         * get A's frame
         * @method getAFrame
         * @return {ctype.value_type<cc.Mat4>}
         */
        public getAFrame (
        ) : ctype.value_type<cc.Mat4>;

        /**
         * enable motor
         * @method enableMotor
         * @param {boolean} _b
         */
        public enableMotor (
            _b : boolean 
        ) : void;

        /**
         * create cone twist constraint<br>
         * -- rbA rigid body A<br>
         * -- rbB rigid body B<br>
         * -- frameA rigid body A's local frame<br>
         * -- frameB rigid body B's local frame
         * @method create
         * @param {(cc.Physics3DRigidBody)} _rbA
         * @param {(cc.Physics3DRigidBody) | (ctype.value_type<cc.Mat4>)} _rbB | frameA
         * @param {(ctype.value_type<cc.Mat4>)} _frameA?
         * @param {(ctype.value_type<cc.Mat4>)} _frameB?
         * @return {number}
         */
        public static create (
            _rbA : (cc.Physics3DRigidBody), 
            _rbB_frameA : (cc.Physics3DRigidBody) | (ctype.value_type<cc.Mat4>), 
            _frameA? : (ctype.value_type<cc.Mat4>), 
            _frameB? : (ctype.value_type<cc.Mat4>)
        ) : number;

        /**
         * 
         * @method Physics3DConeTwistConstraint
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class Physics3D6DofConstraint
     * @native
     */
    export class Physics3D6DofConstraint 
        extends cc.Physics3DConstraint
    {

        /**
         * set linear lower limit
         * @method setLinearLowerLimit
         * @param {ctype.value_type<cc.Vec3>} _linearLower
         */
        public setLinearLowerLimit (
            _linearLower : ctype.value_type<cc.Vec3> 
        ) : void;

        /**
         * get linear lower limit
         * @method getLinearLowerLimit
         * @return {ctype.value_type<cc.Vec3>}
         */
        public getLinearLowerLimit (
        ) : ctype.value_type<cc.Vec3>;

        /**
         * get angular upper limit
         * @method getAngularUpperLimit
         * @return {ctype.value_type<cc.Vec3>}
         */
        public getAngularUpperLimit (
        ) : ctype.value_type<cc.Vec3>;

        /**
         *  access for UseFrameOffset
         * @method getUseFrameOffset
         * @return {boolean}
         */
        public getUseFrameOffset (
        ) : boolean;

        /**
         * get linear upper limit
         * @method getLinearUpperLimit
         * @return {ctype.value_type<cc.Vec3>}
         */
        public getLinearUpperLimit (
        ) : ctype.value_type<cc.Vec3>;

        /**
         * set angular lower limit
         * @method setAngularLowerLimit
         * @param {ctype.value_type<cc.Vec3>} _angularLower
         */
        public setAngularLowerLimit (
            _angularLower : ctype.value_type<cc.Vec3> 
        ) : void;

        /**
         * is limited?<br>
         * -- param limitIndex first 3 are linear, next 3 are angular
         * @method isLimited
         * @param {number} _limitIndex
         * @return {boolean}
         */
        public isLimited (
            _limitIndex : number 
        ) : boolean;

        /**
         * set use frame offset
         * @method setUseFrameOffset
         * @param {boolean} _frameOffsetOnOff
         */
        public setUseFrameOffset (
            _frameOffsetOnOff : boolean 
        ) : void;

        /**
         * set linear upper limit
         * @method setLinearUpperLimit
         * @param {ctype.value_type<cc.Vec3>} _linearUpper
         */
        public setLinearUpperLimit (
            _linearUpper : ctype.value_type<cc.Vec3> 
        ) : void;

        /**
         * get angular lower limit
         * @method getAngularLowerLimit
         * @return {ctype.value_type<cc.Vec3>}
         */
        public getAngularLowerLimit (
        ) : ctype.value_type<cc.Vec3>;

        /**
         * set angular upper limit
         * @method setAngularUpperLimit
         * @param {ctype.value_type<cc.Vec3>} _angularUpper
         */
        public setAngularUpperLimit (
            _angularUpper : ctype.value_type<cc.Vec3> 
        ) : void;

        /**
         * create 6 dof constraint<br>
         * -- param rbB rigid body B<br>
         * -- param frameInB frame in B's local space<br>
         * -- param useLinearReferenceFrameB use fixed frame B for linear limits
         * @method create
         * @param {(cc.Physics3DRigidBody)} _rbB | rbA
         * @param {(ctype.value_type<cc.Mat4>) | (cc.Physics3DRigidBody)} _frameInB | rbB
         * @param {(boolean) | (ctype.value_type<cc.Mat4>)} _useLinearReferenceFrameB | frameInA
         * @param {(ctype.value_type<cc.Mat4>)} _frameInB?
         * @param {(boolean)} _useLinearReferenceFrameA?
         * @return {number}
         */
        public static create (
            _rbB_rbA : (cc.Physics3DRigidBody), 
            _frameInB_rbB : (ctype.value_type<cc.Mat4>) | (cc.Physics3DRigidBody), 
            _useLinearReferenceFrameB_frameInA : (boolean) | (ctype.value_type<cc.Mat4>), 
            _frameInB? : (ctype.value_type<cc.Mat4>), 
            _useLinearReferenceFrameA? : (boolean)
        ) : number;

        /**
         * 
         * @method Physics3D6DofConstraint
         * @constructor
         */
        public constructor (
        );

    }
}