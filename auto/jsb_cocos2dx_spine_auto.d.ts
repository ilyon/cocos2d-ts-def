/**
 * @module cocos2dx_spine
 */
declare namespace sp {
    /**
     * @class SkeletonRenderer
     * @native
     */
    export class Skeleton 
        extends cc.Node
    {

        /**
         * 
         * @method setTimeScale
         * @param {number} _scale
         */
        public setTimeScale (
            _scale : number 
        ) : void;

        /**
         * 
         * @method getDebugSlotsEnabled
         * @return {boolean}
         */
        public getDebugSlotsEnabled (
        ) : boolean;

        /**
         * 
         * @method setAttachment
         * @param {(string)} _slotName
         * @param {(string)} _attachmentName
         * @return {boolean}
         */
        public setAttachment (
            _slotName : (string), 
            _attachmentName : (string)
        ) : boolean;

        /**
         * 
         * @method setBonesToSetupPose
         */
        public setBonesToSetupPose (
        ) : void;

        /**
         * 
         * @method initWithData
         * @param {[WARNING] NATIVE CODE NOT SUPPORTING: spSkeletonData*} _skeletonData
         * @param {boolean} _ownsSkeletonData
         */
        public initWithData (
            _skeletonData : any, 
            _ownsSkeletonData : boolean 
        ) : void;

        /**
         * 
         * @method setDebugSlotsEnabled
         * @param {boolean} _enabled
         */
        public setDebugSlotsEnabled (
            _enabled : boolean 
        ) : void;

        /**
         * 
         * @method setSlotsToSetupPose
         */
        public setSlotsToSetupPose (
        ) : void;

        /**
         * 
         * @method setToSetupPose
         */
        public setToSetupPose (
        ) : void;

        /**
         * 
         * @method getBlendFunc
         * @return {cc.BlendFunc}
         */
        public getBlendFunc (
        ) : cc.BlendFunc;

        /**
         * 
         * @method drawSkeleton
         * @param {ctype.value_type<cc.Mat4>} _transform
         * @param {number} _transformFlags
         */
        public drawSkeleton (
            _transform : ctype.value_type<cc.Mat4>, 
            _transformFlags : number 
        ) : void;

        /**
         * 
         * @method updateWorldTransform
         */
        public updateWorldTransform (
        ) : void;

        /**
         * 
         * @method initialize
         */
        public initialize (
        ) : void;

        /**
         * 
         * @method setDebugBonesEnabled
         * @param {boolean} _enabled
         */
        public setDebugBonesEnabled (
            _enabled : boolean 
        ) : void;

        /**
         * 
         * @method getDebugBonesEnabled
         * @return {boolean}
         */
        public getDebugBonesEnabled (
        ) : boolean;

        /**
         * 
         * @method getTimeScale
         * @return {number}
         */
        public getTimeScale (
        ) : number;

        /**
         * 
         * @method initWithFile
         * @param {(string)} _skeletonDataFile
         * @param {(string) | ([WARNING] NATIVE CODE NOT SUPPORTING: spAtlas*)} _atlasFile | atlas
         * @param {(number)} _scale
         */
        public initWithFile (
            _skeletonDataFile : (string), 
            _atlasFile_atlas : (string) | (any), 
            _scale : (number)
        ) : void;

        /**
         * 
         * @method setBlendFunc
         * @param {cc.BlendFunc} _blendFunc
         */
        public setBlendFunc (
            _blendFunc : cc.BlendFunc 
        ) : void;

        /**
         *  @param skin May be 0 for no skin.
         * @method setSkin
         * @param {(string)} _skinName
         * @return {boolean}
         */
        public setSkin (
            _skinName : (string)
        ) : boolean;

        /**
         * 
         * @method getSkeleton
         * @return {any}
         */
        public getSkeleton (
        ) : any;

        /**
         * 
         * @method createWithFile
         * @param {(string)} _skeletonDataFile
         * @param {(string) | ([WARNING] NATIVE CODE NOT SUPPORTING: spAtlas*)} _atlasFile | atlas
         * @param {(number)} _scale
         * @return {sp.SkeletonRenderer}
         */
        public static create (
            _skeletonDataFile : (string), 
            _atlasFile_atlas : (string) | (any), 
            _scale : (number)
        ) : sp.SkeletonRenderer;

        /**
         * 
         * @method SkeletonRenderer
         * @constructor
         * @param {([WARNING] NATIVE CODE NOT SUPPORTING: spSkeletonData*) | (string)} _skeletonData | skeletonDataFile?
         * @param {(boolean) | ([WARNING] NATIVE CODE NOT SUPPORTING: spAtlas*) | (string)} _ownsSkeletonData | atlas | atlasFile?
         * @param {(number)} _scale?
         */
        public constructor (
            _skeletonData_skeletonDataFile? : (any) | (string), 
            _ownsSkeletonData_atlas_atlasFile? : (boolean) | (any) | (string), 
            _scale? : (number)
        );

    }
    /**
     * @class SkeletonAnimation
     * @native
     */
    export class SkeletonAnimation 
        extends sp.Skeleton
    {

        /**
         * 
         * @method setStartListener
         * @param {(arg0:number) => void} _listener
         */
        public setStartListener (
            _listener : (arg0:number) => void 
        ) : void;

        /**
         * 
         * @method setTrackEventListener
         * @param {[WARNING] NATIVE CODE NOT SUPPORTING: spTrackEntry*} _entry
         * @param {(arg0:number, arg1:spEvent) => void} _listener
         */
        public setTrackEventListener (
            _entry : any, 
            _listener : (arg0:number, arg1:spEvent) => void 
        ) : void;

        /**
         * 
         * @method getState
         * @return {any}
         */
        public getState (
        ) : any;

        /**
         * 
         * @method setTrackCompleteListener
         * @param {[WARNING] NATIVE CODE NOT SUPPORTING: spTrackEntry*} _entry
         * @param {(arg0:number, arg1:number) => void} _listener
         */
        public setTrackCompleteListener (
            _entry : any, 
            _listener : (arg0:number, arg1:number) => void 
        ) : void;

        /**
         * 
         * @method onTrackEntryEvent
         * @param {number} _trackIndex
         * @param {spEventType} _type
         * @param {[WARNING] NATIVE CODE NOT SUPPORTING: spEvent*} _event
         * @param {number} _loopCount
         */
        public onTrackEntryEvent (
            _trackIndex : number, 
            _type : number, 
            _event : any, 
            _loopCount : number 
        ) : void;

        /**
         * 
         * @method setTrackStartListener
         * @param {[WARNING] NATIVE CODE NOT SUPPORTING: spTrackEntry*} _entry
         * @param {(arg0:number) => void} _listener
         */
        public setTrackStartListener (
            _entry : any, 
            _listener : (arg0:number) => void 
        ) : void;

        /**
         * 
         * @method update
         * @param {number} _deltaTime
         */
        public update (
            _deltaTime : number 
        ) : void;

        /**
         * 
         * @method setCompleteListener
         * @param {(arg0:number, arg1:number) => void} _listener
         */
        public setCompleteListener (
            _listener : (arg0:number, arg1:number) => void 
        ) : void;

        /**
         * 
         * @method setTrackEndListener
         * @param {[WARNING] NATIVE CODE NOT SUPPORTING: spTrackEntry*} _entry
         * @param {(arg0:number) => void} _listener
         */
        public setTrackEndListener (
            _entry : any, 
            _listener : (arg0:number) => void 
        ) : void;

        /**
         * 
         * @method setEventListener
         * @param {(arg0:number, arg1:spEvent) => void} _listener
         */
        public setEventListener (
            _listener : (arg0:number, arg1:spEvent) => void 
        ) : void;

        /**
         * 
         * @method setMix
         * @param {string} _fromAnimation
         * @param {string} _toAnimation
         * @param {number} _duration
         */
        public setMix (
            _fromAnimation : string, 
            _toAnimation : string, 
            _duration : number 
        ) : void;

        /**
         * 
         * @method setEndListener
         * @param {(arg0:number) => void} _listener
         */
        public setEndListener (
            _listener : (arg0:number) => void 
        ) : void;

        /**
         * 
         * @method initialize
         */
        public initialize (
        ) : void;

        /**
         * 
         * @method clearTracks
         */
        public clearTracks (
        ) : void;

        /**
         * 
         * @method clearTrack
         */
        public clearTrack (
        ) : void;

        /**
         * 
         * @method onAnimationStateEvent
         * @param {number} _trackIndex
         * @param {spEventType} _type
         * @param {[WARNING] NATIVE CODE NOT SUPPORTING: spEvent*} _event
         * @param {number} _loopCount
         */
        public onAnimationStateEvent (
            _trackIndex : number, 
            _type : number, 
            _event : any, 
            _loopCount : number 
        ) : void;

        /**
         * 
         * @method createWithFile
         * @param {(string)} _skeletonDataFile
         * @param {(string) | ([WARNING] NATIVE CODE NOT SUPPORTING: spAtlas*)} _atlasFile | atlas
         * @param {(number)} _scale
         * @return {sp.SkeletonAnimation}
         */
        public static create (
            _skeletonDataFile : (string), 
            _atlasFile_atlas : (string) | (any), 
            _scale : (number)
        ) : sp.SkeletonAnimation;

        /**
         * 
         * @method SkeletonAnimation
         * @constructor
         * @param {([WARNING] NATIVE CODE NOT SUPPORTING: spSkeletonData*) | (string)} _skeletonData | skeletonDataFile?
         * @param {(boolean) | ([WARNING] NATIVE CODE NOT SUPPORTING: spAtlas*) | (string)} _ownsSkeletonData | atlas | atlasFile?
         * @param {(number)} _scale?
         */
        public constructor (
            _skeletonData_skeletonDataFile? : (any) | (string), 
            _ownsSkeletonData_atlas_atlasFile? : (boolean) | (any) | (string), 
            _scale? : (number)
        );

    }
}