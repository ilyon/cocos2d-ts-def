/**
 * @module cocos2dx_builder
 */
declare namespace cc {
    /**
     * @class CCBAnimationManager
     * @native
     */
    export class BuilderAnimationManager 
    {

        /**
         * 
         * @method moveAnimationsFromNode
         * @param {cc.Node} _fromNode
         * @param {cc.Node} _toNode
         */
        public moveAnimationsFromNode (
            _fromNode : cc.Node, 
            _toNode : cc.Node 
        ) : void;

        /**
         * 
         * @method setAutoPlaySequenceId
         * @param {number} _autoPlaySequenceId
         */
        public setAutoPlaySequenceId (
            _autoPlaySequenceId : number 
        ) : void;

        /**
         * 
         * @method getDocumentCallbackNames
         * @return {cc.Value[]}
         */
        public getDocumentCallbackNames (
        ) : cc.Value[];

        /**
         * 
         * @method actionForSoundChannel
         * @param {cc.CCBSequenceProperty} _channel
         * @return {cc.Sequence}
         */
        public actionForSoundChannel (
            _channel : cc.CCBSequenceProperty 
        ) : cc.Sequence;

        /**
         * 
         * @method setBaseValue
         * @param {cc.Value} _value
         * @param {cc.Node} _pNode
         * @param {string} _propName
         */
        public setBaseValue (
            _value : cc.Value, 
            _pNode : cc.Node, 
            _propName : string 
        ) : void;

        /**
         * 
         * @method getDocumentOutletNodes
         * @return {cc.Node[]}
         */
        public getDocumentOutletNodes (
        ) : cc.Node[];

        /**
         * 
         * @method getLastCompletedSequenceName
         * @return {string}
         */
        public getLastCompletedSequenceName (
        ) : string;

        /**
         * 
         * @method setRootNode
         * @param {cc.Node} _pRootNode
         */
        public setRootNode (
            _pRootNode : cc.Node 
        ) : void;

        /**
         * 
         * @method runAnimationsForSequenceNamedTweenDuration
         * @param {string} _pName
         * @param {number} _fTweenDuration
         */
        public runAnimationsForSequenceNamedTweenDuration (
            _pName : string, 
            _fTweenDuration : number 
        ) : void;

        /**
         * 
         * @method addDocumentOutletName
         * @param {string} _name
         */
        public addDocumentOutletName (
            _name : string 
        ) : void;

        /**
         * 
         * @method getRootContainerSize
         * @return {ctype.value_type<cc.Size>}
         */
        public getRootContainerSize (
        ) : ctype.value_type<cc.Size>;

        /**
         * 
         * @method setDocumentControllerName
         * @param {string} _name
         */
        public setDocumentControllerName (
            _name : string 
        ) : void;

        /**
         * 
         * @method setObject
         * @param {cc.Ref} _obj
         * @param {cc.Node} _pNode
         * @param {string} _propName
         */
        public setObject (
            _obj : any, 
            _pNode : cc.Node, 
            _propName : string 
        ) : void;

        /**
         * 
         * @method getContainerSize
         * @param {cc.Node} _pNode
         * @return {ctype.value_type<cc.Size>}
         */
        public getContainerSize (
            _pNode : cc.Node 
        ) : ctype.value_type<cc.Size>;

        /**
         * 
         * @method actionForCallbackChannel
         * @param {cc.CCBSequenceProperty} _channel
         * @return {cc.Sequence}
         */
        public actionForCallbackChannel (
            _channel : cc.CCBSequenceProperty 
        ) : cc.Sequence;

        /**
         * 
         * @method getDocumentOutletNames
         * @return {cc.Value[]}
         */
        public getDocumentOutletNames (
        ) : cc.Value[];

        /**
         * 
         * @method addDocumentCallbackControlEvents
         * @param {cc.Control::EventType} _eventType
         */
        public addDocumentCallbackControlEvents (
            _eventType : number 
        ) : void;

        /**
         * 
         * @method init
         * @return {boolean}
         */
        public init (
        ) : boolean;

        /**
         * 
         * @method getKeyframeCallbacks
         * @return {cc.Value[]}
         */
        public getKeyframeCallbacks (
        ) : cc.Value[];

        /**
         * 
         * @method getDocumentCallbackControlEvents
         * @return {cc.Value[]}
         */
        public getDocumentCallbackControlEvents (
        ) : cc.Value[];

        /**
         * 
         * @method setRootContainerSize
         * @param {ctype.value_type<cc.Size>} _rootContainerSize
         */
        public setRootContainerSize (
            _rootContainerSize : ctype.value_type<cc.Size> 
        ) : void;

        /**
         * 
         * @method runAnimationsForSequenceIdTweenDuration
         * @param {number} _nSeqId
         * @param {number} _fTweenDuraiton
         */
        public runAnimationsForSequenceIdTweenDuration (
            _nSeqId : number, 
            _fTweenDuraiton : number 
        ) : void;

        /**
         * 
         * @method getRunningSequenceName
         * @return {string}
         */
        public getRunningSequenceName (
        ) : string;

        /**
         * 
         * @method getAutoPlaySequenceId
         * @return {number}
         */
        public getAutoPlaySequenceId (
        ) : number;

        /**
         * 
         * @method addDocumentCallbackName
         * @param {string} _name
         */
        public addDocumentCallbackName (
            _name : string 
        ) : void;

        /**
         * 
         * @method getRootNode
         * @return {cc.Node}
         */
        public getRootNode (
        ) : cc.Node;

        /**
         * 
         * @method addDocumentOutletNode
         * @param {cc.Node} _node
         */
        public addDocumentOutletNode (
            _node : cc.Node 
        ) : void;

        /**
         * 
         * @method setDelegate
         * @param {cc.CCBAnimationManagerDelegate} _pDelegate
         */
        public setDelegate (
            _pDelegate : cc.CCBAnimationManagerDelegate 
        ) : void;

        /**
         * 
         * @method getSequenceDuration
         * @param {string} _pSequenceName
         * @return {number}
         */
        public getSequenceDuration (
            _pSequenceName : string 
        ) : number;

        /**
         * 
         * @method addDocumentCallbackNode
         * @param {cc.Node} _node
         */
        public addDocumentCallbackNode (
            _node : cc.Node 
        ) : void;

        /**
         * 
         * @method runAnimationsForSequenceNamed
         * @param {string} _pName
         */
        public runAnimationsForSequenceNamed (
            _pName : string 
        ) : void;

        /**
         * 
         * @method getSequenceId
         * @param {string} _pSequenceName
         * @return {number}
         */
        public getSequenceId (
            _pSequenceName : string 
        ) : number;

        /**
         * js setCallFuncForJSCallbackNamed
         * @method setCallFunc
         * @param {cc.CallFunc} _callFunc
         * @param {string} _callbackNamed
         */
        public setCallFunc (
            _callFunc : cc.CallFunc, 
            _callbackNamed : string 
        ) : void;

        /**
         * 
         * @method getDocumentCallbackNodes
         * @return {cc.Node[]}
         */
        public getDocumentCallbackNodes (
        ) : cc.Node[];

        /**
         * 
         * @method setSequences
         * @param {cc.CCBSequence[]} _seq
         */
        public setSequences (
            _seq : cc.CCBSequence[] 
        ) : void;

        /**
         * 
         * @method debug
         */
        public debug (
        ) : void;

        /**
         * 
         * @method getDocumentControllerName
         * @return {string}
         */
        public getDocumentControllerName (
        ) : string;

        /**
         * js ctor
         * @method CCBAnimationManager
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class CCBReader
     * @native
     */
    export class _Reader 
    {

        /**
         * js getActionManager<br>
         * -- lua getActionManager
         * @method getAnimationManager
         * @return {cc.CCBAnimationManager}
         */
        public getAnimationManager (
        ) : cc.CCBAnimationManager;

        /**
         * js setActionManager<br>
         * -- lua setActionManager
         * @method setAnimationManager
         * @param {cc.CCBAnimationManager} _pAnimationManager
         */
        public setAnimationManager (
            _pAnimationManager : cc.CCBAnimationManager 
        ) : void;

        /**
         * 
         * @method addOwnerOutletName
         * @param {string} _name
         */
        public addOwnerOutletName (
            _name : string 
        ) : void;

        /**
         * 
         * @method getOwnerCallbackNames
         * @return {cc.Value[]}
         */
        public getOwnerCallbackNames (
        ) : cc.Value[];

        /**
         * 
         * @method addDocumentCallbackControlEvents
         * @param {cc.Control::EventType} _eventType
         */
        public addDocumentCallbackControlEvents (
            _eventType : number 
        ) : void;

        /**
         * 
         * @method setCCBRootPath
         * @param {string} _ccbRootPath
         */
        public setCCBRootPath (
            _ccbRootPath : string 
        ) : void;

        /**
         * 
         * @method addOwnerOutletNode
         * @param {cc.Node} _node
         */
        public addOwnerOutletNode (
            _node : cc.Node 
        ) : void;

        /**
         * 
         * @method getOwnerCallbackNodes
         * @return {cc.Node[]}
         */
        public getOwnerCallbackNodes (
        ) : cc.Node[];

        /**
         * 
         * @method readSoundKeyframesForSeq
         * @param {cc.CCBSequence} _seq
         * @return {boolean}
         */
        public readSoundKeyframesForSeq (
            _seq : cc.CCBSequence 
        ) : boolean;

        /**
         * 
         * @method getCCBRootPath
         * @return {string}
         */
        public getCCBRootPath (
        ) : string;

        /**
         * 
         * @method getOwnerCallbackControlEvents
         * @return {cc.Value[]}
         */
        public getOwnerCallbackControlEvents (
        ) : cc.Value[];

        /**
         * 
         * @method getOwnerOutletNodes
         * @return {cc.Node[]}
         */
        public getOwnerOutletNodes (
        ) : cc.Node[];

        /**
         * 
         * @method readUTF8
         * @return {string}
         */
        public readUTF8 (
        ) : string;

        /**
         * 
         * @method addOwnerCallbackControlEvents
         * @param {cc.Control::EventType} _type
         */
        public addOwnerCallbackControlEvents (
            _type : number 
        ) : void;

        /**
         * 
         * @method getOwnerOutletNames
         * @return {cc.Value[]}
         */
        public getOwnerOutletNames (
        ) : cc.Value[];

        /**
         * 
         * @method readCallbackKeyframesForSeq
         * @param {cc.CCBSequence} _seq
         * @return {boolean}
         */
        public readCallbackKeyframesForSeq (
            _seq : cc.CCBSequence 
        ) : boolean;

        /**
         * 
         * @method getAnimationManagersForNodes
         * @return {cc.CCBAnimationManager[]}
         */
        public getAnimationManagersForNodes (
        ) : cc.CCBAnimationManager[];

        /**
         * 
         * @method getNodesWithAnimationManagers
         * @return {cc.Node[]}
         */
        public getNodesWithAnimationManagers (
        ) : cc.Node[];

        /**
         * 
         * @method setResolutionScale
         * @param {number} _scale
         */
        public static setResolutionScale (
            _scale : number 
        ) : void;

        /**
         * js NA<br>
         * -- lua NA
         * @method CCBReader
         * @constructor
         * @param {(cc.CCBReader) | (cc.NodeLoaderLibrary)} _ccbReader | pNodeLoaderLibrary?
         * @param {(cc.CCBMemberVariableAssigner)} _pCCBMemberVariableAssigner?
         * @param {(cc.CCBSelectorResolver)} _pCCBSelectorResolver?
         * @param {(cc.NodeLoaderListener)} _pNodeLoaderListener?
         */
        public constructor (
            _ccbReader_pNodeLoaderLibrary? : (cc.CCBReader) | (cc.NodeLoaderLibrary), 
            _pCCBMemberVariableAssigner? : (cc.CCBMemberVariableAssigner), 
            _pCCBSelectorResolver? : (cc.CCBSelectorResolver), 
            _pNodeLoaderListener? : (cc.NodeLoaderListener)
        );

    }
}