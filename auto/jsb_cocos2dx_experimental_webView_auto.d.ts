/**
 * @module cocos2dx_experimental_webView
 */
declare namespace ccui {
    /**
     * @class WebView
     * @native
     */
    export class WebView 
        extends ccui.Widget
    {

        /**
         * Gets whether this WebView has a back history item.<br>
         * -- return WebView has a back history item.
         * @method canGoBack
         * @return {boolean}
         */
        public canGoBack (
        ) : boolean;

        /**
         * Sets the main page content and base URL.<br>
         * -- param string The content for the main page.<br>
         * -- param baseURL The base URL for the content.
         * @method loadHTMLString
         * @param {string} _string
         * @param {string} _baseURL
         */
        public loadHTMLString (
            _string : string, 
            _baseURL : string 
        ) : void;

        /**
         * Goes forward in the history.
         * @method goForward
         */
        public goForward (
        ) : void;

        /**
         * Goes back in the history.
         * @method goBack
         */
        public goBack (
        ) : void;

        /**
         * Set WebView should support zooming. The default value is false.
         * @method setScalesPageToFit
         * @param {boolean} _scalesPageToFit
         */
        public setScalesPageToFit (
            _scalesPageToFit : boolean 
        ) : void;

        /**
         * Get the callback when WebView has failed loading.
         * @method getOnDidFailLoading
         * @return {(arg0:any, arg1:any) => void}
         */
        public getOnDidFailLoading (
        ) : (arg0:any, arg1:any) => void;

        /**
         * Loads the given fileName.<br>
         * -- param fileName Content fileName.
         * @method loadFile
         * @param {string} _fileName
         */
        public loadFile (
            _fileName : string 
        ) : void;

        /**
         * Loads the given URL.<br>
         * -- param url Content URL.
         * @method loadURL
         * @param {string} _url
         */
        public loadURL (
            _url : string 
        ) : void;

        /**
         * Evaluates JavaScript in the context of the currently displayed page.
         * @method evaluateJS
         * @param {string} _js
         */
        public evaluateJS (
            _js : string 
        ) : void;

        /**
         * Get the Javascript callback.
         * @method getOnJSCallback
         * @return {(arg0:any, arg1:any) => void}
         */
        public getOnJSCallback (
        ) : (arg0:any, arg1:any) => void;

        /**
         * Gets whether this WebView has a forward history item.<br>
         * -- return WebView has a forward history item.
         * @method canGoForward
         * @return {boolean}
         */
        public canGoForward (
        ) : boolean;

        /**
         * Get the callback when WebView is about to start.
         * @method getOnShouldStartLoading
         * @return {(arg0:any, arg1:any) => boolean}
         */
        public getOnShouldStartLoading (
        ) : (arg0:any, arg1:any) => boolean;

        /**
         * Stops the current load.
         * @method stopLoading
         */
        public stopLoading (
        ) : void;

        /**
         * Reloads the current URL.
         * @method reload
         */
        public reload (
        ) : void;

        /**
         * Set javascript interface scheme.<br>
         * -- see WebView::setOnJSCallback()
         * @method setJavascriptInterfaceScheme
         * @param {string} _scheme
         */
        public setJavascriptInterfaceScheme (
            _scheme : string 
        ) : void;

        /**
         * Get the callback when WebView has finished loading.
         * @method getOnDidFinishLoading
         * @return {(arg0:any, arg1:any) => void}
         */
        public getOnDidFinishLoading (
        ) : (arg0:any, arg1:any) => void;

        /**
         * Allocates and initializes a WebView.
         * @method create
         * @return {any}
         */
        public static create (
        ) : any;

        /**
         * Default constructor.
         * @method WebView
         * @constructor
         */
        public constructor (
        );

    }
}