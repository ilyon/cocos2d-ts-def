/**
 * @module cocos2dx_audioengine
 */
declare namespace jsb {
    /**
     * @class AudioProfile
     * @native
     */
    export class AudioProfile 
    {

        /**
         * Default constructor<br>
         * -- lua new
         * @method AudioProfile
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class AudioEngine
     * @native
     */
    export abstract class AudioEngine 
    {

        /**
         * 
         * @method lazyInit
         * @return {boolean}
         */
        public static lazyInit (
        ) : boolean;

        /**
         * Sets the current playback position of an audio instance.<br>
         * -- param audioID   An audioID returned by the play2d function.<br>
         * -- param sec       The offset in seconds from the start to seek to.<br>
         * -- return 
         * @method setCurrentTime
         * @param {number} _audioID
         * @param {number} _sec
         * @return {boolean}
         */
        public static setCurrentTime (
            _audioID : number, 
            _sec : number 
        ) : boolean;

        /**
         * Gets the volume value of an audio instance.<br>
         * -- param audioID An audioID returned by the play2d function.<br>
         * -- return Volume value (range from 0.0 to 1.0).
         * @method getVolume
         * @param {number} _audioID
         * @return {number}
         */
        public static getVolume (
            _audioID : number 
        ) : number;

        /**
         * Uncache the audio data from internal buffer.<br>
         * -- AudioEngine cache audio data on ios,mac, and win32 platform.<br>
         * -- warning This can lead to stop related audio first.<br>
         * -- param filePath Audio file path.
         * @method uncache
         * @param {string} _filePath
         */
        public static uncache (
            _filePath : string 
        ) : void;

        /**
         *  Resume all suspended audio instances. 
         * @method resumeAll
         */
        public static resumeAll (
        ) : void;

        /**
         *  Stop all audio instances. 
         * @method stopAll
         */
        public static stopAll (
        ) : void;

        /**
         * Pause an audio instance.<br>
         * -- param audioID An audioID returned by the play2d function.
         * @method pause
         * @param {number} _audioID
         */
        public static pause (
            _audioID : number 
        ) : void;

        /**
         * Release objects relating to AudioEngine.<br>
         * -- warning It must be called before the application exit.<br>
         * -- lua endToLua
         * @method end
         */
        public static end (
        ) : void;

        /**
         * Gets the maximum number of simultaneous audio instance of AudioEngine.
         * @method getMaxAudioInstance
         * @return {number}
         */
        public static getMaxAudioInstance (
        ) : number;

        /**
         * Check whether AudioEngine is enabled.
         * @method isEnabled
         * @return {boolean}
         */
        public static isEnabled (
        ) : boolean;

        /**
         * Gets the current playback position of an audio instance.<br>
         * -- param audioID An audioID returned by the play2d function.<br>
         * -- return The current playback position of an audio instance.
         * @method getCurrentTime
         * @param {number} _audioID
         * @return {number}
         */
        public static getCurrentTime (
            _audioID : number 
        ) : number;

        /**
         * Sets the maximum number of simultaneous audio instance for AudioEngine.<br>
         * -- param maxInstances The maximum number of simultaneous audio instance.
         * @method setMaxAudioInstance
         * @param {number} _maxInstances
         * @return {boolean}
         */
        public static setMaxAudioInstance (
            _maxInstances : number 
        ) : boolean;

        /**
         * Checks whether an audio instance is loop.<br>
         * -- param audioID An audioID returned by the play2d function.<br>
         * -- return Whether or not an audio instance is loop.
         * @method isLoop
         * @param {number} _audioID
         * @return {boolean}
         */
        public static isLoop (
            _audioID : number 
        ) : boolean;

        /**
         *  Pause all playing audio instances. 
         * @method pauseAll
         */
        public static pauseAll (
        ) : void;

        /**
         * Uncache all audio data from internal buffer.<br>
         * -- warning All audio will be stopped first.
         * @method uncacheAll
         */
        public static uncacheAll (
        ) : void;

        /**
         * Sets volume for an audio instance.<br>
         * -- param audioID An audioID returned by the play2d function.<br>
         * -- param volume Volume value (range from 0.0 to 1.0).
         * @method setVolume
         * @param {number} _audioID
         * @param {number} _volume
         */
        public static setVolume (
            _audioID : number, 
            _volume : number 
        ) : void;

        /**
         * Preload audio file.<br>
         * -- param filePath The file path of an audio.<br>
         * -- param callback A callback which will be called after loading is finished.
         * @method preload
         * @param {(string)} _filePath
         * @param {((arg0:boolean) => void)} _callback?
         */
        public static preload (
            _filePath : (string), 
            _callback? : ((arg0:boolean) => void)
        ) : void;

        /**
         * Whether to enable playing audios<br>
         * -- note If it's disabled, current playing audios will be stopped and the later 'preload', 'play2d' methods will take no effects.
         * @method setEnabled
         * @param {boolean} _isEnabled
         */
        public static setEnabled (
            _isEnabled : boolean 
        ) : void;

        /**
         * Play 2d sound.<br>
         * -- param filePath The path of an audio file.<br>
         * -- param loop Whether audio instance loop or not.<br>
         * -- param volume Volume value (range from 0.0 to 1.0).<br>
         * -- param profile A profile for audio instance. When profile is not specified, default profile will be used.<br>
         * -- return An audio ID. It allows you to dynamically change the behavior of an audio instance on the fly.<br>
         * -- see `AudioProfile`
         * @method play2d
         * @param {string} _filePath
         * @param {boolean} _loop
         * @param {number} _volume
         * @param {cc.experimental::AudioProfile} _profile
         * @return {number}
         */
        public static play2d (
            _filePath : string, 
            _loop : boolean, 
            _volume : number, 
            _profile : any 
        ) : number;

        /**
         * Returns the state of an audio instance.<br>
         * -- param audioID An audioID returned by the play2d function.<br>
         * -- return The status of an audio instance.
         * @method getState
         * @param {number} _audioID
         * @return {number}
         */
        public static getState (
            _audioID : number 
        ) : number;

        /**
         * Resume an audio instance.<br>
         * -- param audioID An audioID returned by the play2d function.
         * @method resume
         * @param {number} _audioID
         */
        public static resume (
            _audioID : number 
        ) : void;

        /**
         * Stop an audio instance.<br>
         * -- param audioID An audioID returned by the play2d function.
         * @method stop
         * @param {number} _audioID
         */
        public static stop (
            _audioID : number 
        ) : void;

        /**
         * Gets the duration of an audio instance.<br>
         * -- param audioID An audioID returned by the play2d function.<br>
         * -- return The duration of an audio instance.
         * @method getDuration
         * @param {number} _audioID
         * @return {number}
         */
        public static getDuration (
            _audioID : number 
        ) : number;

        /**
         * Sets whether an audio instance loop or not.<br>
         * -- param audioID An audioID returned by the play2d function.<br>
         * -- param loop Whether audio instance loop or not.
         * @method setLoop
         * @param {number} _audioID
         * @param {boolean} _loop
         */
        public static setLoop (
            _audioID : number, 
            _loop : boolean 
        ) : void;

        /**
         * Gets the default profile of audio instances.<br>
         * -- return The default profile of audio instances.
         * @method getDefaultProfile
         * @return {any}
         */
        public static getDefaultProfile (
        ) : any;

        /**
         * Register a callback to be invoked when an audio instance has completed playing.<br>
         * -- param audioID An audioID returned by the play2d function.<br>
         * -- param callback
         * @method setFinishCallback
         * @param {number} _audioID
         * @param {(arg0:number, arg1:std::string) => void} _callback
         */
        public static setFinishCallback (
            _audioID : number, 
            _callback : (arg0:number, arg1:any) => void 
        ) : void;

        /**
         * Gets an audio profile by name.<br>
         * -- param profileName A name of audio profile.<br>
         * -- return The audio profile.
         * @method getProfile
         * @param {(string) | (number)} _profileName | audioID
         * @return {cc.experimental::AudioProfile}
         */
        public static getProfile (
            _profileName_audioID : (string) | (number)
        ) : any;

        /**
         * Gets playing audio count.
         * @method getPlayingAudioCount
         * @return {number}
         */
        public static getPlayingAudioCount (
        ) : number;

    }
}