/**
 * @module cocos2dx_ui
 */
declare namespace ccui {
    /**
     * @class LayoutParameter
     * @native
     */
    export class LayoutParameter 
    {

        /**
         * Create a copy of original LayoutParameter.<br>
         * -- return A LayoutParameter pointer.
         * @method clone
         * @return {ccui.LayoutParameter}
         */
        public clone (
        ) : ccui.LayoutParameter;

        /**
         * Gets LayoutParameterType of LayoutParameter.<br>
         * -- see LayoutParameterType.<br>
         * -- return LayoutParameterType
         * @method getLayoutType
         * @return {number}
         */
        public getLayoutType (
        ) : number;

        /**
         * Create a cloned instance of LayoutParameter.<br>
         * -- return A LayoutParameter pointer.
         * @method createCloneInstance
         * @return {ccui.LayoutParameter}
         */
        public createCloneInstance (
        ) : ccui.LayoutParameter;

        /**
         * Copy all the member field from argument LayoutParameter to self.<br>
         * -- param model A LayoutParameter instance.
         * @method copyProperties
         * @param {ccui.LayoutParameter} _model
         */
        public copyProperties (
            _model : ccui.LayoutParameter 
        ) : void;

        /**
         * Create a empty LayoutParameter.<br>
         * -- return A autorelease LayoutParameter instance.
         * @method create
         * @return {ccui.LayoutParameter}
         */
        public static create (
        ) : ccui.LayoutParameter;

        /**
         * Default constructor.<br>
         * -- lua new
         * @method LayoutParameter
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class LinearLayoutParameter
     * @native
     */
    export class LinearLayoutParameter 
        extends ccui.LayoutParameter
    {

        /**
         * Sets LinearGravity parameter for LayoutParameter.<br>
         * -- see LinearGravity<br>
         * -- param gravity Gravity in LinearGravity.
         * @method setGravity
         * @param {ccui.LinearLayoutParameter::LinearGravity} _gravity
         */
        public setGravity (
            _gravity : number 
        ) : void;

        /**
         * Gets LinearGravity parameter for LayoutParameter.<br>
         * -- see LinearGravity<br>
         * -- return LinearGravity
         * @method getGravity
         * @return {number}
         */
        public getGravity (
        ) : number;

        /**
         * Create a empty LinearLayoutParameter instance.<br>
         * -- return A initialized LayoutParameter which is marked as "autorelease".
         * @method create
         * @return {ccui.LinearLayoutParameter}
         */
        public static create (
        ) : ccui.LinearLayoutParameter;

        /**
         * Default constructor.<br>
         * -- lua new
         * @method LinearLayoutParameter
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class RelativeLayoutParameter
     * @native
     */
    export class RelativeLayoutParameter 
        extends ccui.LayoutParameter
    {

        /**
         * Sets RelativeAlign parameter for LayoutParameter.<br>
         * -- see RelativeAlign<br>
         * -- param align Relative align in  `RelativeAlign`.
         * @method setAlign
         * @param {ccui.RelativeLayoutParameter::RelativeAlign} _align
         */
        public setAlign (
            _align : number 
        ) : void;

        /**
         * Set widget name your widget want to relative to.<br>
         * -- param name Relative widget name.
         * @method setRelativeToWidgetName
         * @param {string} _name
         */
        public setRelativeToWidgetName (
            _name : string 
        ) : void;

        /**
         * Get a name of LayoutParameter in Relative Layout.<br>
         * -- return name Relative name in string.
         * @method getRelativeName
         * @return {string}
         */
        public getRelativeName (
        ) : string;

        /**
         * Get the relative widget name.<br>
         * -- return name A relative widget name in string.
         * @method getRelativeToWidgetName
         * @return {string}
         */
        public getRelativeToWidgetName (
        ) : string;

        /**
         * Set a name for LayoutParameter in Relative Layout.<br>
         * -- param name A string name.
         * @method setRelativeName
         * @param {string} _name
         */
        public setRelativeName (
            _name : string 
        ) : void;

        /**
         * Get RelativeAlign parameter for LayoutParameter.<br>
         * -- see RelativeAlign<br>
         * -- return  A RelativeAlign variable.
         * @method getAlign
         * @return {number}
         */
        public getAlign (
        ) : number;

        /**
         * Create a RelativeLayoutParameter instance.<br>
         * -- return A initialized LayoutParameter which is marked as "autorelease".
         * @method create
         * @return {ccui.RelativeLayoutParameter}
         */
        public static create (
        ) : ccui.RelativeLayoutParameter;

        /**
         * Default constructor<br>
         * -- lua new
         * @method RelativeLayoutParameter
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class Widget
     * @native
     */
    export class Widget 
        extends cc.ProtectedNode
    {

        /**
         * Toggle layout component enable.<br>
         * -- param enable Layout Component of a widget<br>
         * -- return void
         * @method setLayoutComponentEnabled
         * @param {boolean} _enable
         */
        public setLayoutComponentEnabled (
            _enable : boolean 
        ) : void;

        /**
         * Changes the percent that is widget's percent size<br>
         * -- param percent that is widget's percent size
         * @method setSizePercent
         * @param {ctype.value_type<cc.Point>} _percent
         */
        public setSizePercent (
            _percent : ctype.value_type<cc.Point> 
        ) : void;

        /**
         * Get the user defined widget size.<br>
         * -- return User defined size.
         * @method getCustomSize
         * @return {ctype.value_type<cc.Size>}
         */
        public getCustomSize (
        ) : ctype.value_type<cc.Size>;

        /**
         * Gets the left boundary position of this widget in parent's coordination system.<br>
         * -- return The left boundary position of this widget.
         * @method getLeftBoundary
         * @return {number}
         */
        public getLeftBoundary (
        ) : number;

        /**
         * Sets whether the widget should be flipped horizontally or not.<br>
         * -- param flippedX true if the widget should be flipped horizontally, false otherwise.
         * @method setFlippedX
         * @param {boolean} _flippedX
         */
        public setFlippedX (
            _flippedX : boolean 
        ) : void;

        /**
         * Set callback name.<br>
         * -- param callbackName A string representation of callback name.
         * @method setCallbackName
         * @param {string} _callbackName
         */
        public setCallbackName (
            _callbackName : string 
        ) : void;

        /**
         * Gets the inner Renderer node of widget.<br>
         * -- For example, a button's Virtual Renderer is it's texture renderer.<br>
         * -- return Node pointer.
         * @method getVirtualRenderer
         * @return {cc.Node}
         */
        public getVirtualRenderer (
        ) : cc.Node;

        /**
         * brief Allow widget touch events to propagate to its parents. Set false will disable propagation<br>
         * -- param isPropagate  True to allow propagation, false otherwise.<br>
         * -- since v3.3
         * @method setPropagateTouchEvents
         * @param {boolean} _isPropagate
         */
        public setPropagateTouchEvents (
            _isPropagate : boolean 
        ) : void;

        /**
         * Query whether unify size enable state. <br>
         * -- return true represent the widget use Unify Size, false represent the widget couldn't use Unify Size
         * @method isUnifySizeEnabled
         * @return {boolean}
         */
        public isUnifySizeEnabled (
        ) : boolean;

        /**
         * Get size percent of widget.<br>
         * -- return Percent size.
         * @method getSizePercent
         * @return {ctype.value_type<cc.Point>}
         */
        public getSizePercent (
        ) : ctype.value_type<cc.Point>;

        /**
         * Set the percent(x,y) of the widget in OpenGL coordinates<br>
         * -- param percent  The percent (x,y) of the widget in OpenGL coordinates
         * @method setPositionPercent
         * @param {ctype.value_type<cc.Point>} _percent
         */
        public setPositionPercent (
            _percent : ctype.value_type<cc.Point> 
        ) : void;

        /**
         * Toggle widget swallow touch option.<br>
         * -- brief Specify widget to swallow touches or not<br>
         * -- param swallow True to swallow touch, false otherwise.<br>
         * -- since v3.3
         * @method setSwallowTouches
         * @param {boolean} _swallow
         */
        public setSwallowTouches (
            _swallow : boolean 
        ) : void;

        /**
         * Get the content size of widget.<br>
         * -- warning This API exists mainly for keeping back compatibility.<br>
         * -- return 
         * @method getLayoutSize
         * @return {ctype.value_type<cc.Size>}
         */
        public getLayoutSize (
        ) : ctype.value_type<cc.Size>;

        /**
         * Sets whether the widget is highlighted<br>
         * -- The default value is false, a widget is default to not highlighted<br>
         * -- param highlight   true if the widget is highlighted, false if the widget is not highlighted.
         * @method setHighlighted
         * @param {boolean} _highlight
         */
        public setHighlighted (
            _highlight : boolean 
        ) : void;

        /**
         * Changes the position type of the widget<br>
         * -- see `PositionType`<br>
         * -- param type  the position type of widget
         * @method setPositionType
         * @param {ccui.Widget::PositionType} _type
         */
        public setPositionType (
            _type : number 
        ) : void;

        /**
         * Query whether the widget ignores user defined content size or not<br>
         * -- return True means ignore user defined content size, false otherwise.
         * @method isIgnoreContentAdaptWithSize
         * @return {boolean}
         */
        public isIgnoreContentAdaptWithSize (
        ) : boolean;

        /**
         * Get the virtual renderer's size<br>
         * -- return Widget virtual renderer size.
         * @method getVirtualRendererSize
         * @return {ctype.value_type<cc.Size>}
         */
        public getVirtualRendererSize (
        ) : ctype.value_type<cc.Size>;

        /**
         * Determines if the widget is highlighted<br>
         * -- return true if the widget is highlighted, false if the widget is not highlighted.
         * @method isHighlighted
         * @return {boolean}
         */
        public isHighlighted (
        ) : boolean;

        /**
         * Gets LayoutParameter of widget.<br>
         * -- see LayoutParameter<br>
         * -- return LayoutParameter
         * @method getLayoutParameter
         * @return {ccui.LayoutParameter}
         */
        public getLayoutParameter (
        ) : ccui.LayoutParameter;

        /**
         * Set a event handler to the widget in order to use cocostudio editor and framework<br>
         * -- param callback The callback in `ccWidgetEventCallback`.<br>
         * -- lua NA
         * @method addCCSEventListener
         * @param {(arg0:cc.Ref, arg1:number) => void} _callback
         */
        public addCCSEventListener (
            _callback : (arg0:any, arg1:number) => void 
        ) : void;

        /**
         * Gets the position type of the widget<br>
         * -- see `PositionType`<br>
         * -- return type  the position type of widget
         * @method getPositionType
         * @return {number}
         */
        public getPositionType (
        ) : number;

        /**
         * Gets the top boundary position of this widget in parent's coordination system.<br>
         * -- return The top boundary position of this widget.
         * @method getTopBoundary
         * @return {number}
         */
        public getTopBoundary (
        ) : number;

        /**
         * Toggle whether ignore user defined content size for widget.<br>
         * -- Set true will ignore user defined content size which means <br>
         * -- the widget size is always equal to the return value of `getVirtualRendererSize`.<br>
         * -- param ignore set member variable _ignoreSize to ignore
         * @method ignoreContentAdaptWithSize
         * @param {boolean} _ignore
         */
        public ignoreContentAdaptWithSize (
            _ignore : boolean 
        ) : void;

        /**
         * When a widget is in a layout, you could call this method to get the next focused widget within a specified direction. <br>
         * -- If the widget is not in a layout, it will return itself<br>
         * -- param direction the direction to look for the next focused widget in a layout<br>
         * -- param current  the current focused widget<br>
         * -- return the next focused widget in a layout
         * @method findNextFocusedWidget
         * @param {ccui.Widget::FocusDirection} _direction
         * @param {ccui.Widget} _current
         * @return {ccui.Widget}
         */
        public findNextFocusedWidget (
            _direction : number, 
            _current : ccui.Widget 
        ) : ccui.Widget;

        /**
         * Determines if the widget is enabled or not.<br>
         * -- return true if the widget is enabled, false if the widget is disabled.
         * @method isEnabled
         * @return {boolean}
         */
        public isEnabled (
        ) : boolean;

        /**
         * Query whether widget is focused or not.<br>
         * -- return  whether the widget is focused or not
         * @method isFocused
         * @return {boolean}
         */
        public isFocused (
        ) : boolean;

        /**
         * Gets the touch began point of widget when widget is selected.<br>
         * -- return the touch began point.
         * @method getTouchBeganPosition
         * @return {ctype.value_type<cc.Point>}
         */
        public getTouchBeganPosition (
        ) : ctype.value_type<cc.Point>;

        /**
         * Determines if the widget is touch enabled<br>
         * -- return true if the widget is touch enabled, false if the widget is touch disabled.
         * @method isTouchEnabled
         * @return {boolean}
         */
        public isTouchEnabled (
        ) : boolean;

        /**
         * Query callback name.<br>
         * -- return The callback name.
         * @method getCallbackName
         * @return {string}
         */
        public getCallbackName (
        ) : string;

        /**
         * Get the action tag.<br>
         * -- return Action tag.
         * @method getActionTag
         * @return {number}
         */
        public getActionTag (
        ) : number;

        /**
         * Gets position of widget in world space.<br>
         * -- return Position of widget in world space.
         * @method getWorldPosition
         * @return {ctype.value_type<cc.Point>}
         */
        public getWorldPosition (
        ) : ctype.value_type<cc.Point>;

        /**
         * Query widget's focus enable state.<br>
         * -- return true represent the widget could accept focus, false represent the widget couldn't accept focus
         * @method isFocusEnabled
         * @return {boolean}
         */
        public isFocusEnabled (
        ) : boolean;

        /**
         * Toggle widget focus status.<br>
         * -- param focus  pass true to let the widget get focus or pass false to let the widget lose focus<br>
         * -- return void
         * @method setFocused
         * @param {boolean} _focus
         */
        public setFocused (
            _focus : boolean 
        ) : void;

        /**
         * Set the tag of action.<br>
         * -- param tag  A integer tag value.
         * @method setActionTag
         * @param {number} _tag
         */
        public setActionTag (
            _tag : number 
        ) : void;

        /**
         * Sets whether the widget is touch enabled.<br>
         * -- The default value is false, a widget is default to touch disabled.<br>
         * -- param enabled   True if the widget is touch enabled, false if the widget is touch disabled.
         * @method setTouchEnabled
         * @param {boolean} _enabled
         */
        public setTouchEnabled (
            _enabled : boolean 
        ) : void;

        /**
         * Sets whether the widget should be flipped vertically or not.<br>
         * -- param flippedY true if the widget should be flipped vertically, false otherwise.
         * @method setFlippedY
         * @param {boolean} _flippedY
         */
        public setFlippedY (
            _flippedY : boolean 
        ) : void;

        /**
         * 
         * @method init
         * @return {boolean}
         */
        public _init (
        ) : boolean;

        /**
         * Sets whether the widget is enabled<br>
         * -- true if the widget is enabled, widget may be touched , false if the widget is disabled, widget cannot be touched.<br>
         * -- Note: If you want to change the widget's appearance  to disabled state, you should also call  `setBright(false)`.<br>
         * -- The default value is true, a widget is default to enable touch.<br>
         * -- param enabled Set to true to enable touch, false otherwise.
         * @method setEnabled
         * @param {boolean} _enabled
         */
        public setEnabled (
            _enabled : boolean 
        ) : void;

        /**
         * Gets the right boundary position of this widget in parent's coordination system.<br>
         * -- return The right boundary position of this widget.
         * @method getRightBoundary
         * @return {number}
         */
        public getRightBoundary (
        ) : number;

        /**
         * To set the bright style of widget.<br>
         * -- see BrightStyle<br>
         * -- param style   BrightStyle::NORMAL means the widget is in normal state, BrightStyle::HIGHLIGHT means the widget is in highlight state.
         * @method setBrightStyle
         * @param {ccui.Widget::BrightStyle} _style
         */
        public setBrightStyle (
            _style : number 
        ) : void;

        /**
         * Sets a LayoutParameter to widget.<br>
         * -- see LayoutParameter<br>
         * -- param parameter LayoutParameter pointer
         * @method setLayoutParameter
         * @param {ccui.LayoutParameter} _parameter
         */
        public setLayoutParameter (
            _parameter : ccui.LayoutParameter 
        ) : void;

        /**
         * Create a new widget copy of the original one.<br>
         * -- return A cloned widget copy of original.
         * @method clone
         * @return {ccui.Widget}
         */
        public clone (
        ) : ccui.Widget;

        /**
         * Allow widget to accept focus.<br>
         * -- param enable pass true/false to enable/disable the focus ability of a widget<br>
         * -- return void
         * @method setFocusEnabled
         * @param {boolean} _enable
         */
        public setFocusEnabled (
            _enable : boolean 
        ) : void;

        /**
         * Gets the bottom boundary position of this widget in parent's coordination system.<br>
         * -- return The bottom boundary position of this widget.
         * @method getBottomBoundary
         * @return {number}
         */
        public getBottomBoundary (
        ) : number;

        /**
         * Determines if the widget is bright<br>
         * -- return true if the widget is bright, false if the widget is dark.
         * @method isBright
         * @return {boolean}
         */
        public isBright (
        ) : boolean;

        /**
         * Dispatch a EventFocus through a EventDispatcher<br>
         * -- param widgetLoseFocus  The widget which lose its focus<br>
         * -- param widgetGetFocus he widget which get its focus<br>
         * -- return void
         * @method dispatchFocusEvent
         * @param {ccui.Widget} _widgetLoseFocus
         * @param {ccui.Widget} _widgetGetFocus
         */
        public dispatchFocusEvent (
            _widgetLoseFocus : ccui.Widget, 
            _widgetGetFocus : ccui.Widget 
        ) : void;

        /**
         * Toggle use unify size.<br>
         * -- param enable True to use unify size, false otherwise.<br>
         * -- return void
         * @method setUnifySizeEnabled
         * @param {boolean} _enable
         */
        public setUnifySizeEnabled (
            _enable : boolean 
        ) : void;

        /**
         * Return whether the widget is propagate touch events to its parents or not<br>
         * -- return whether touch event propagation is allowed or not.<br>
         * -- since v3.3
         * @method isPropagateTouchEvents
         * @return {boolean}
         */
        public isPropagateTouchEvents (
        ) : boolean;

        /**
         * Checks a point is in widget's content space.<br>
         * -- This function is used for determining touch area of widget.<br>
         * -- param pt        The point in `Vec2`.<br>
         * -- param camera    The camera look at widget, used to convert GL screen point to near/far plane.<br>
         * -- param p         Point to a Vec3 for store the intersect point, if don't need them set to nullptr.<br>
         * -- return true if the point is in widget's content space, false otherwise.
         * @method hitTest
         * @param {ctype.value_type<cc.Point>} _pt
         * @param {cc.Camera} _camera
         * @param {ctype.value_type<cc.Vec3>} _p
         * @return {boolean}
         */
        public hitTest (
            _pt : ctype.value_type<cc.Point>, 
            _camera : cc.Camera, 
            _p : ctype.value_type<cc.Vec3> 
        ) : boolean;

        /**
         * Query whether layout component is enabled or not. <br>
         * -- return true represent the widget use Layout Component, false represent the widget couldn't use Layout Component.
         * @method isLayoutComponentEnabled
         * @return {boolean}
         */
        public isLayoutComponentEnabled (
        ) : boolean;

        /**
         * when a widget calls this method, it will get focus immediately.
         * @method requestFocus
         */
        public requestFocus (
        ) : void;

        /**
         * Update all children's contents size and position recursively.
         * @method updateSizeAndPosition
         * @param {(ctype.value_type<cc.Size>)} _parentSize?
         */
        public updateSizeAndPosition (
            _parentSize? : (ctype.value_type<cc.Size>)
        ) : void;

        /**
         * This method is called when a focus change event happens<br>
         * -- param widgetLostFocus  The widget which lose its focus<br>
         * -- param widgetGetFocus  The widget which get its focus<br>
         * -- return void
         * @method onFocusChange
         * @param {ccui.Widget} _widgetLostFocus
         * @param {ccui.Widget} _widgetGetFocus
         */
        public onFocusChange (
            _widgetLostFocus : ccui.Widget, 
            _widgetGetFocus : ccui.Widget 
        ) : void;

        /**
         * 
         * @method getTouchMovePosition
         * @return {ctype.value_type<cc.Point>}
         */
        public getTouchMovePosition (
        ) : ctype.value_type<cc.Point>;

        /**
         * Gets the size type of widget.<br>
         * -- see `SizeType`
         * @method getSizeType
         * @return {number}
         */
        public getSizeType (
        ) : number;

        /**
         * Query callback type.<br>
         * -- return Callback type string.
         * @method getCallbackType
         * @return {string}
         */
        public getCallbackType (
        ) : string;

        /**
         * Set a callback to touch vent listener.<br>
         * -- param callback  The callback in `ccWidgetEventCallback.`
         * @method addTouchEventListener
         * @param {(arg0:cc.Ref, arg1:ccui.Widget::TouchEventType) => void} _callback
         */
        public addTouchEventListener (
            _callback : (arg0:any, arg1:any) => void 
        ) : void;

        /**
         * 
         * @method getTouchEndPosition
         * @return {ctype.value_type<cc.Point>}
         */
        public getTouchEndPosition (
        ) : ctype.value_type<cc.Point>;

        /**
         * Gets the percent (x,y) of the widget in OpenGL coordinates<br>
         * -- see setPosition(const Vec2&)<br>
         * -- return The percent (x,y) of the widget in OpenGL coordinates
         * @method getPositionPercent
         * @return {ctype.value_type<cc.Point>}
         */
        public getPositionPercent (
        ) : ctype.value_type<cc.Point>;

        /**
         * brief Propagate touch events to its parents
         * @method propagateTouchEvent
         * @param {ccui.Widget::TouchEventType} _event
         * @param {ccui.Widget} _sender
         * @param {cc.Touch} _touch
         */
        public propagateTouchEvent (
            _event : number, 
            _sender : ccui.Widget, 
            _touch : cc.Touch 
        ) : void;

        /**
         * Set a click event handler to the widget.<br>
         * -- param callback The callback in `ccWidgetClickCallback`.
         * @method addClickEventListener
         * @param {(arg0:cc.Ref) => void} _callback
         */
        public addClickEventListener (
            _callback : (arg0:any) => void 
        ) : void;

        /**
         * Returns the flag which indicates whether the widget is flipped horizontally or not.<br>
         * -- It not only flips the texture of the widget, but also the texture of the widget's children.<br>
         * -- Also, flipping relies on widget's anchor point.<br>
         * -- Internally, it just use setScaleX(-1) to flip the widget.<br>
         * -- return true if the widget is flipped horizontally, false otherwise.
         * @method isFlippedX
         * @return {boolean}
         */
        public isFlippedX (
        ) : boolean;

        /**
         * Return the flag which indicates whether the widget is flipped vertically or not.<br>
         * -- It not only flips the texture of the widget, but also the texture of the widget's children.<br>
         * -- Also, flipping relies on widget's anchor point.<br>
         * -- Internally, it just use setScaleY(-1) to flip the widget.<br>
         * -- return true if the widget is flipped vertically, false otherwise.
         * @method isFlippedY
         * @return {boolean}
         */
        public isFlippedY (
        ) : boolean;

        /**
         * Checks a point if in parent's area.<br>
         * -- param pt A point in `Vec2`.<br>
         * -- return true if the point is in parent's area, false otherwise.
         * @method isClippingParentContainsPoint
         * @param {ctype.value_type<cc.Point>} _pt
         * @return {boolean}
         */
        public isClippingParentContainsPoint (
            _pt : ctype.value_type<cc.Point> 
        ) : boolean;

        /**
         * Changes the size type of widget.<br>
         * -- see `SizeType`<br>
         * -- param type that is widget's size type
         * @method setSizeType
         * @param {ccui.Widget::SizeType} _type
         */
        public setSizeType (
            _type : number 
        ) : void;

        /**
         * 
         * @method interceptTouchEvent
         * @param {ccui.Widget::TouchEventType} _event
         * @param {ccui.Widget} _sender
         * @param {cc.Touch} _touch
         */
        public interceptTouchEvent (
            _event : number, 
            _sender : ccui.Widget, 
            _touch : cc.Touch 
        ) : void;

        /**
         * Sets whether the widget is bright<br>
         * -- The default value is true, a widget is default to bright<br>
         * -- param bright   true if the widget is bright, false if the widget is dark.
         * @method setBright
         * @param {boolean} _bright
         */
        public setBright (
            _bright : boolean 
        ) : void;

        /**
         * Set callback type.<br>
         * -- param callbackType A string representation of callback type.
         * @method setCallbackType
         * @param {string} _callbackType
         */
        public setCallbackType (
            _callbackType : string 
        ) : void;

        /**
         * Return whether the widget is swallowing touch or not<br>
         * -- return Whether touch is swallowed.<br>
         * -- since v3.3
         * @method isSwallowTouches
         * @return {boolean}
         */
        public isSwallowTouches (
        ) : boolean;

        /**
         * 
         * @method enableDpadNavigation
         * @param {boolean} _enable
         */
        public static enableDpadNavigation (
            _enable : boolean 
        ) : void;

        /**
         * Return a current focused widget in your UI scene.<br>
         * -- No matter what widget object you call this method on , it will return you the exact one focused widget.
         * @method getCurrentFocusedWidget
         * @return {ccui.Widget}
         */
        public static getCurrentFocusedWidget (
        ) : ccui.Widget;

        /**
         * Create and return a empty Widget instance pointer.
         * @method create
         * @return {ccui.Widget}
         */
        public static create (
        ) : ccui.Widget;

        /**
         * Default constructor<br>
         * -- js ctor<br>
         * -- lua new
         * @method Widget
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class Layout
     * @native
     */
    export class Layout 
        extends ccui.Widget
    {

        /**
         * Sets background color vector for layout.<br>
         * -- This setting only take effect when layout's color type is BackGroundColorType::GRADIENT<br>
         * -- param vector The color vector in `Vec2`.
         * @method setBackGroundColorVector
         * @param {ctype.value_type<cc.Point>} _vector
         */
        public setBackGroundColorVector (
            _vector : ctype.value_type<cc.Point> 
        ) : void;

        /**
         * Change the clipping type of layout.<br>
         * -- On default, the clipping type is `ClippingType::STENCIL`.<br>
         * -- see `ClippingType`<br>
         * -- param type The clipping type of layout.
         * @method setClippingType
         * @param {ccui.Layout::ClippingType} _type
         */
        public setClippingType (
            _type : number 
        ) : void;

        /**
         * Sets Color Type for layout's background<br>
         * -- param type   @see `BackGroundColorType`
         * @method setBackGroundColorType
         * @param {ccui.Layout::BackGroundColorType} _type
         */
        public setBackGroundColorType (
            _type : number 
        ) : void;

        /**
         * If a layout is loop focused which means that the focus movement will be inside the layout<br>
         * -- param loop  pass true to let the focus movement loop inside the layout
         * @method setLoopFocus
         * @param {boolean} _loop
         */
        public setLoopFocus (
            _loop : boolean 
        ) : void;

        /**
         * Set layout's background image color.<br>
         * -- param color Background color value in `Color3B`.
         * @method setBackGroundImageColor
         * @param {ctype.value_type<cc.Color>} _color
         */
        public setBackGroundImageColor (
            _color : ctype.value_type<cc.Color> 
        ) : void;

        /**
         * Get the layout's background color vector.<br>
         * -- return Background color vector.
         * @method getBackGroundColorVector
         * @return {ctype.value_type<cc.Point>}
         */
        public getBackGroundColorVector (
        ) : ctype.value_type<cc.Point>;

        /**
         * see `setClippingType(ClippingType)`
         * @method getClippingType
         * @return {number}
         */
        public getClippingType (
        ) : number;

        /**
         * 
         * @method getRenderFile
         * @return {any}
         */
        public getRenderFile (
        ) : any;

        /**
         * return If focus loop is enabled, then it will return true, otherwise it returns false. The default value is false.
         * @method isLoopFocus
         * @return {boolean}
         */
        public isLoopFocus (
        ) : boolean;

        /**
         * Remove the background image of layout.
         * @method removeBackGroundImage
         */
        public removeBackGroundImage (
        ) : void;

        /**
         * Get the layout's background color opacity.<br>
         * -- return Background color opacity value.
         * @method getBackGroundColorOpacity
         * @return {number}
         */
        public getBackGroundColorOpacity (
        ) : number;

        /**
         * Gets if layout is clipping enabled.<br>
         * -- return if layout is clipping enabled.
         * @method isClippingEnabled
         * @return {boolean}
         */
        public isClippingEnabled (
        ) : boolean;

        /**
         * Set opacity of background image.<br>
         * -- param opacity Background image opacity in GLubyte.
         * @method setBackGroundImageOpacity
         * @param {number} _opacity
         */
        public setBackGroundImageOpacity (
            _opacity : number 
        ) : void;

        /**
         * Sets a background image for layout.<br>
         * -- param fileName image file path.<br>
         * -- param texType @see TextureResType. 
         * @method setBackGroundImage
         * @param {string} _fileName
         * @param {ccui.Widget::TextureResType} _texType
         */
        public setBackGroundImage (
            _fileName : string, 
            _texType : number 
        ) : void;

        /**
         * Set start and end background color for layout.<br>
         * -- This setting only take effect when the layout's  color type is BackGroundColorType::GRADIENT<br>
         * -- param startColor Color value in Color3B.<br>
         * -- param endColor Color value in Color3B.
         * @method setBackGroundColor
         * @param {(ctype.value_type<cc.Color>)} _startColor | color
         * @param {(ctype.value_type<cc.Color>)} _endColor?
         */
        public setBackGroundColor (
            _startColor_color : (ctype.value_type<cc.Color>), 
            _endColor? : (ctype.value_type<cc.Color>)
        ) : void;

        /**
         * request to refresh widget layout
         * @method requestDoLayout
         */
        public requestDoLayout (
        ) : void;

        /**
         * Query background image's capInsets size.<br>
         * -- return The background image capInsets.
         * @method getBackGroundImageCapInsets
         * @return {ctype.value_type<cc.Rect>}
         */
        public getBackGroundImageCapInsets (
        ) : ctype.value_type<cc.Rect>;

        /**
         * Query the layout's background color.<br>
         * -- return Background color in Color3B.
         * @method getBackGroundColor
         * @return {ctype.value_type<cc.Color>}
         */
        public getBackGroundColor (
        ) : ctype.value_type<cc.Color>;

        /**
         * Toggle layout clipping.<br>
         * -- If you do need clipping, you pass true to this function.<br>
         * -- param enabled Pass true to enable clipping, false otherwise.
         * @method setClippingEnabled
         * @param {boolean} _enabled
         */
        public setClippingEnabled (
            _enabled : boolean 
        ) : void;

        /**
         * Get color of layout's background image.<br>
         * -- return Layout's background image color.
         * @method getBackGroundImageColor
         * @return {ctype.value_type<cc.Color>}
         */
        public getBackGroundImageColor (
        ) : ctype.value_type<cc.Color>;

        /**
         * Query background image scale9 enable status.<br>
         * -- return Whether background image is scale9 enabled or not.
         * @method isBackGroundImageScale9Enabled
         * @return {boolean}
         */
        public isBackGroundImageScale9Enabled (
        ) : boolean;

        /**
         * Query the layout's background color type.<br>
         * -- return The layout's background color type.
         * @method getBackGroundColorType
         * @return {number}
         */
        public getBackGroundColorType (
        ) : number;

        /**
         * Get the gradient background end color.<br>
         * -- return Gradient background end color value.
         * @method getBackGroundEndColor
         * @return {ctype.value_type<cc.Color>}
         */
        public getBackGroundEndColor (
        ) : ctype.value_type<cc.Color>;

        /**
         * Sets background color opacity of layout.<br>
         * -- param opacity The opacity in `GLubyte`.
         * @method setBackGroundColorOpacity
         * @param {number} _opacity
         */
        public setBackGroundColorOpacity (
            _opacity : number 
        ) : void;

        /**
         * Get the opacity of layout's background image.<br>
         * -- return The opacity of layout's background image.
         * @method getBackGroundImageOpacity
         * @return {number}
         */
        public getBackGroundImageOpacity (
        ) : number;

        /**
         * return To query whether the layout will pass the focus to its children or not. The default value is true
         * @method isPassFocusToChild
         * @return {boolean}
         */
        public isPassFocusToChild (
        ) : boolean;

        /**
         * Sets a background image capinsets for layout, it only affects the scale9 enabled background image<br>
         * -- param capInsets  The capInsets in Rect.
         * @method setBackGroundImageCapInsets
         * @param {ctype.value_type<cc.Rect>} _capInsets
         */
        public setBackGroundImageCapInsets (
            _capInsets : ctype.value_type<cc.Rect> 
        ) : void;

        /**
         * Gets background image texture size.<br>
         * -- return background image texture size.
         * @method getBackGroundImageTextureSize
         * @return {ctype.value_type<cc.Size>}
         */
        public getBackGroundImageTextureSize (
        ) : ctype.value_type<cc.Size>;

        /**
         * force refresh widget layout
         * @method forceDoLayout
         */
        public forceDoLayout (
        ) : void;

        /**
         * Query layout type.<br>
         * -- return Get the layout type.
         * @method getLayoutType
         * @return {number}
         */
        public getLayoutType (
        ) : number;

        /**
         * param pass To specify whether the layout pass its focus to its child
         * @method setPassFocusToChild
         * @param {boolean} _pass
         */
        public setPassFocusToChild (
            _pass : boolean 
        ) : void;

        /**
         * Get the gradient background start color.<br>
         * -- return  Gradient background start color value.
         * @method getBackGroundStartColor
         * @return {ctype.value_type<cc.Color>}
         */
        public getBackGroundStartColor (
        ) : ctype.value_type<cc.Color>;

        /**
         * Enable background image scale9 rendering.<br>
         * -- param enabled  True means enable scale9 rendering for background image, false otherwise.
         * @method setBackGroundImageScale9Enabled
         * @param {boolean} _enabled
         */
        public setBackGroundImageScale9Enabled (
            _enabled : boolean 
        ) : void;

        /**
         * Change the layout type.<br>
         * -- param type Layout type.
         * @method setLayoutType
         * @param {ccui.Layout::Type} _type
         */
        public setLayoutType (
            _type : number 
        ) : void;

        /**
         * Create a empty layout.
         * @method create
         * @return {ccui.Layout}
         */
        public static create (
        ) : ccui.Layout;

        /**
         * Default constructor<br>
         * -- js ctor<br>
         * -- lua new
         * @method Layout
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class Button
     * @native
     */
    export class Button 
        extends ccui.Widget
    {

        /**
         * 
         * @method getNormalTextureSize
         * @return {ctype.value_type<cc.Size>}
         */
        public getNormalTextureSize (
        ) : ctype.value_type<cc.Size>;

        /**
         * Query the button title content.<br>
         * -- return Get the button's title content.
         * @method getTitleText
         * @return {string}
         */
        public getTitleText (
        ) : string;

        /**
         * Change the font size of button's title<br>
         * -- param size Title font size in float.
         * @method setTitleFontSize
         * @param {number} _size
         */
        public setTitleFontSize (
            _size : number 
        ) : void;

        /**
         * 
         * @method resetPressedRender
         */
        public resetPressedRender (
        ) : void;

        /**
         * Enable scale9 renderer.<br>
         * -- param enable Set to true will use scale9 renderer, false otherwise.
         * @method setScale9Enabled
         * @param {boolean} _enable
         */
        public setScale9Enabled (
            _enable : boolean 
        ) : void;

        /**
         * 
         * @method resetDisabledRender
         */
        public resetDisabledRender (
        ) : void;

        /**
         * Return the inner title renderer of Button.<br>
         * -- return The button title.<br>
         * -- since v3.3
         * @method getTitleRenderer
         * @return {cc.Label}
         */
        public getTitleRenderer (
        ) : cc.Label;

        /**
         * brief Return the nine-patch sprite of clicked state<br>
         * -- return the nine-patch sprite of clicked state<br>
         * -- since v3.9
         * @method getRendererClicked
         * @return {ccui.Scale9Sprite}
         */
        public getRendererClicked (
        ) : ccui.Scale9Sprite;

        /**
         * 
         * @method getDisabledFile
         * @return {any}
         */
        public getDisabledFile (
        ) : any;

        /**
         * brief Return a zoom scale<br>
         * -- return the zoom scale in float<br>
         * -- since v3.3
         * @method getZoomScale
         * @return {number}
         */
        public getZoomScale (
        ) : number;

        /**
         * Return the capInsets of disabled state scale9sprite.<br>
         * -- return The disabled scale9 renderer capInsets.
         * @method getCapInsetsDisabledRenderer
         * @return {ctype.value_type<cc.Rect>}
         */
        public getCapInsetsDisabledRenderer (
        ) : ctype.value_type<cc.Rect>;

        /**
         * Change the color of button's title.<br>
         * -- param color The title color in Color3B.
         * @method setTitleColor
         * @param {ctype.value_type<cc.Color>} _color
         */
        public setTitleColor (
            _color : ctype.value_type<cc.Color> 
        ) : void;

        /**
         * 
         * @method getNormalFile
         * @return {any}
         */
        public getNormalFile (
        ) : any;

        /**
         * 
         * @method resetNormalRender
         */
        public resetNormalRender (
        ) : void;

        /**
         * brief Return the nine-patch sprite of disabled state<br>
         * -- return the nine-patch sprite of disabled state<br>
         * -- since v3.9
         * @method getRendererDisabled
         * @return {ccui.Scale9Sprite}
         */
        public getRendererDisabled (
        ) : ccui.Scale9Sprite;

        /**
         * Sets capInsets for button, only the disabled state scale9 renderer will be affected.<br>
         * -- param capInsets  capInsets in Rect.
         * @method setCapInsetsDisabledRenderer
         * @param {ctype.value_type<cc.Rect>} _capInsets
         */
        public setCapInsetsDisabledRenderer (
            _capInsets : ctype.value_type<cc.Rect> 
        ) : void;

        /**
         * Sets capInsets for button.<br>
         * -- The capInset affects  all button scale9 renderer only if `setScale9Enabled(true)` is called<br>
         * -- param capInsets    capInset in Rect.
         * @method setCapInsets
         * @param {ctype.value_type<cc.Rect>} _capInsets
         */
        public setCapInsets (
            _capInsets : ctype.value_type<cc.Rect> 
        ) : void;

        /**
         * Load disabled state texture for button.<br>
         * -- param disabled    dark state texture.<br>
         * -- param texType    @see `TextureResType`
         * @method loadTextureDisabled
         * @param {string} _disabled
         * @param {ccui.Widget::TextureResType} _texType
         */
        public loadTextureDisabled (
            _disabled : string, 
            _texType : number 
        ) : void;

        /**
         * 
         * @method init
         * @param {string} _normalImage
         * @param {string} _selectedImage
         * @param {string} _disableImage
         * @param {ccui.Widget::TextureResType} _texType
         * @return {boolean}
         */
        public init (
            _normalImage : string, 
            _selectedImage : string, 
            _disableImage : string, 
            _texType : number 
        ) : boolean;

        /**
         * Change the content of button's title.<br>
         * -- param text The title in std::string.
         * @method setTitleText
         * @param {string} _text
         */
        public setTitleText (
            _text : string 
        ) : void;

        /**
         * Sets capInsets for button, only the normal state scale9 renderer will be affected.<br>
         * -- param capInsets    capInsets in Rect.
         * @method setCapInsetsNormalRenderer
         * @param {ctype.value_type<cc.Rect>} _capInsets
         */
        public setCapInsetsNormalRenderer (
            _capInsets : ctype.value_type<cc.Rect> 
        ) : void;

        /**
         * Load selected state texture for button.<br>
         * -- param selected    selected state texture.<br>
         * -- param texType    @see `TextureResType`
         * @method loadTexturePressed
         * @param {string} _selected
         * @param {ccui.Widget::TextureResType} _texType
         */
        public loadTexturePressed (
            _selected : string, 
            _texType : number 
        ) : void;

        /**
         * Change the font name of button's title<br>
         * -- param fontName a font name string.
         * @method setTitleFontName
         * @param {string} _fontName
         */
        public setTitleFontName (
            _fontName : string 
        ) : void;

        /**
         * Return the capInsets of normal state scale9sprite.<br>
         * -- return The normal scale9 renderer capInsets.
         * @method getCapInsetsNormalRenderer
         * @return {ctype.value_type<cc.Rect>}
         */
        public getCapInsetsNormalRenderer (
        ) : ctype.value_type<cc.Rect>;

        /**
         * Sets the title's text vertical alignment.<br>
         * -- param hAlignment see TextHAlignment.<br>
         * -- param vAlignment see TextVAlignment.
         * @method setTitleAlignment
         * @param {(cc.TextHAlignment)} _hAlignment
         * @param {(cc.TextVAlignment)} _vAlignment?
         */
        public setTitleAlignment (
            _hAlignment : (number), 
            _vAlignment? : (number)
        ) : void;

        /**
         * Return the capInsets of pressed state scale9sprite.<br>
         * -- return The pressed scale9 renderer capInsets.
         * @method getCapInsetsPressedRenderer
         * @return {ctype.value_type<cc.Rect>}
         */
        public getCapInsetsPressedRenderer (
        ) : ctype.value_type<cc.Rect>;

        /**
         * Load textures for button.<br>
         * -- param normal    normal state texture name.<br>
         * -- param selected    selected state texture name.<br>
         * -- param disabled    disabled state texture name.<br>
         * -- param texType    @see `TextureResType`
         * @method loadTextures
         * @param {string} _normal
         * @param {string} _selected
         * @param {string} _disabled
         * @param {ccui.Widget::TextureResType} _texType
         */
        public loadTextures (
            _normal : string, 
            _selected : string, 
            _disabled : string, 
            _texType : number 
        ) : void;

        /**
         * Query whether button is using scale9 renderer or not.<br>
         * -- return whether button use scale9 renderer or not.
         * @method isScale9Enabled
         * @return {boolean}
         */
        public isScale9Enabled (
        ) : boolean;

        /**
         * Load normal state texture for button.<br>
         * -- param normal    normal state texture.<br>
         * -- param texType    @see `TextureResType`
         * @method loadTextureNormal
         * @param {string} _normal
         * @param {ccui.Widget::TextureResType} _texType
         */
        public loadTextureNormal (
            _normal : string, 
            _texType : number 
        ) : void;

        /**
         * Sets capInsets for button, only the pressed state scale9 renderer will be affected.<br>
         * -- param capInsets    capInsets in Rect
         * @method setCapInsetsPressedRenderer
         * @param {ctype.value_type<cc.Rect>} _capInsets
         */
        public setCapInsetsPressedRenderer (
            _capInsets : ctype.value_type<cc.Rect> 
        ) : void;

        /**
         * 
         * @method getPressedFile
         * @return {any}
         */
        public getPressedFile (
        ) : any;

        /**
         * Query the font size of button title<br>
         * -- return font size in float.
         * @method getTitleFontSize
         * @return {number}
         */
        public getTitleFontSize (
        ) : number;

        /**
         * brief Return the nine-patch sprite of normal state<br>
         * -- return the nine-patch sprite of normal state<br>
         * -- since v3.9
         * @method getRendererNormal
         * @return {ccui.Scale9Sprite}
         */
        public getRendererNormal (
        ) : ccui.Scale9Sprite;

        /**
         * Query the font name of button's title<br>
         * -- return font name in std::string
         * @method getTitleFontName
         * @return {string}
         */
        public getTitleFontName (
        ) : string;

        /**
         * Query the button title color.<br>
         * -- return Color3B of button title.
         * @method getTitleColor
         * @return {ctype.value_type<cc.Color>}
         */
        public getTitleColor (
        ) : ctype.value_type<cc.Color>;

        /**
         * Enable zooming action when button is pressed.<br>
         * -- param enabled Set to true will enable zoom effect, false otherwise.
         * @method setPressedActionEnabled
         * @param {boolean} _enabled
         */
        public setPressedActionEnabled (
            _enabled : boolean 
        ) : void;

        /**
         *  @brief When user pressed the button, the button will zoom to a scale.<br>
         * -- The final scale of the button  equals (button original scale + _zoomScale)<br>
         * -- since v3.3
         * @method setZoomScale
         * @param {number} _scale
         */
        public setZoomScale (
            _scale : number 
        ) : void;

        /**
         * Create a button with custom textures.<br>
         * -- param normalImage normal state texture name.<br>
         * -- param selectedImage  selected state texture name.<br>
         * -- param disableImage disabled state texture name.<br>
         * -- param texType    @see `TextureResType`<br>
         * -- return a Button instance.
         * @method create
         * @param {(string)} _normalImage?
         * @param {(string)} _selectedImage?
         * @param {(string)} _disableImage?
         * @param {(ccui.Widget::TextureResType)} _texType?
         * @return {ccui.Button}
         */
        public static create (
            _normalImage? : (string), 
            _selectedImage? : (string), 
            _disableImage? : (string), 
            _texType? : (number)
        ) : ccui.Button;

        /**
         * Default constructor.
         * @method Button
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class AbstractCheckButton
     * @native
     */
    export abstract class AbstractCheckButton 
        extends ccui.Widget
    {

        /**
         * 
         * @method getCrossDisabledFile
         * @return {any}
         */
        public getCrossDisabledFile (
        ) : any;

        /**
         * 
         * @method getBackDisabledFile
         * @return {any}
         */
        public getBackDisabledFile (
        ) : any;

        /**
         * Load background selected state texture for check button.<br>
         * -- param backGroundSelected    The background selected state image name.<br>
         * -- param texType    @see `Widget::TextureResType`
         * @method loadTextureBackGroundSelected
         * @param {string} _backGroundSelected
         * @param {ccui.Widget::TextureResType} _texType
         */
        public loadTextureBackGroundSelected (
            _backGroundSelected : string, 
            _texType : number 
        ) : void;

        /**
         * Load background disabled state texture for checkbox.<br>
         * -- param backGroundDisabled    The background disabled state texture name.<br>
         * -- param texType    @see `Widget::TextureResType`
         * @method loadTextureBackGroundDisabled
         * @param {string} _backGroundDisabled
         * @param {ccui.Widget::TextureResType} _texType
         */
        public loadTextureBackGroundDisabled (
            _backGroundDisabled : string, 
            _texType : number 
        ) : void;

        /**
         * 
         * @method getCrossNormalFile
         * @return {any}
         */
        public getCrossNormalFile (
        ) : any;

        /**
         * Change CheckBox state.<br>
         * -- Set to true will cause the CheckBox's state to "selected", false otherwise.<br>
         * -- param selected Set to true will change CheckBox to selected state, false otherwise.
         * @method setSelected
         * @param {boolean} _selected
         */
        public setSelected (
            _selected : boolean 
        ) : void;

        /**
         * 
         * @method getBackPressedFile
         * @return {any}
         */
        public getBackPressedFile (
        ) : any;

        /**
         * brief Return the sprite instance of front cross when disabled<br>
         * -- return the sprite instance of front cross when disabled
         * @method getRendererFrontCrossDisabled
         * @return {cc.Sprite}
         */
        public getRendererFrontCrossDisabled (
        ) : cc.Sprite;

        /**
         * brief Return the sprite instance of background<br>
         * -- return the sprite instance of background.
         * @method getRendererBackground
         * @return {cc.Sprite}
         */
        public getRendererBackground (
        ) : cc.Sprite;

        /**
         * Load cross texture for check button.<br>
         * -- param crossTextureName    The cross texture name.<br>
         * -- param texType    @see `Widget::TextureResType`
         * @method loadTextureFrontCross
         * @param {string} _crossTextureName
         * @param {ccui.Widget::TextureResType} _texType
         */
        public loadTextureFrontCross (
            _crossTextureName : string, 
            _texType : number 
        ) : void;

        /**
         * brief Return the sprite instance of background when disabled<br>
         * -- return the sprite instance of background when disabled
         * @method getRendererBackgroundDisabled
         * @return {cc.Sprite}
         */
        public getRendererBackgroundDisabled (
        ) : cc.Sprite;

        /**
         * Query whether CheckBox is selected or not.<br>
         * -- return true means "selected", false otherwise.
         * @method isSelected
         * @return {boolean}
         */
        public isSelected (
        ) : boolean;

        /**
         * 
         * @method init
         * @param {string} _backGround
         * @param {string} _backGroundSeleted
         * @param {string} _cross
         * @param {string} _backGroundDisabled
         * @param {string} _frontCrossDisabled
         * @param {ccui.Widget::TextureResType} _texType
         * @return {boolean}
         */
        public init (
            _backGround : string, 
            _backGroundSeleted : string, 
            _cross : string, 
            _backGroundDisabled : string, 
            _frontCrossDisabled : string, 
            _texType : number 
        ) : boolean;

        /**
         * 
         * @method getBackNormalFile
         * @return {any}
         */
        public getBackNormalFile (
        ) : any;

        /**
         * Load all textures for initializing a check button.<br>
         * -- param background    The background image name.<br>
         * -- param backgroundSelected    The background selected image name.<br>
         * -- param cross    The cross image name.<br>
         * -- param backgroundDisabled    The background disabled state texture.<br>
         * -- param frontCrossDisabled    The front cross disabled state image name.<br>
         * -- param texType    @see `Widget::TextureResType`
         * @method loadTextures
         * @param {string} _background
         * @param {string} _backgroundSelected
         * @param {string} _cross
         * @param {string} _backgroundDisabled
         * @param {string} _frontCrossDisabled
         * @param {ccui.Widget::TextureResType} _texType
         */
        public loadTextures (
            _background : string, 
            _backgroundSelected : string, 
            _cross : string, 
            _backgroundDisabled : string, 
            _frontCrossDisabled : string, 
            _texType : number 
        ) : void;

        /**
         * brief Return a zoom scale<br>
         * -- return A zoom scale of Checkbox.<br>
         * -- since v3.3
         * @method getZoomScale
         * @return {number}
         */
        public getZoomScale (
        ) : number;

        /**
         * brief Return the sprite instance of front cross<br>
         * -- return the sprite instance of front cross
         * @method getRendererFrontCross
         * @return {cc.Sprite}
         */
        public getRendererFrontCross (
        ) : cc.Sprite;

        /**
         * brief Return the sprite instance of background when selected<br>
         * -- return the sprite instance of background when selected
         * @method getRendererBackgroundSelected
         * @return {cc.Sprite}
         */
        public getRendererBackgroundSelected (
        ) : cc.Sprite;

        /**
         * Load background texture for check button.<br>
         * -- param backGround   The background image name.<br>
         * -- param type    @see `Widget::TextureResType`
         * @method loadTextureBackGround
         * @param {string} _backGround
         * @param {ccui.Widget::TextureResType} _type
         */
        public loadTextureBackGround (
            _backGround : string, 
            _type : number 
        ) : void;

        /**
         *  When user pressed the CheckBox, the button will zoom to a scale.<br>
         * -- The final scale of the CheckBox  equals (CheckBox original scale + _zoomScale)<br>
         * -- since v3.3
         * @method setZoomScale
         * @param {number} _scale
         */
        public setZoomScale (
            _scale : number 
        ) : void;

        /**
         * Load frontcross disabled texture for checkbox.<br>
         * -- param frontCrossDisabled    The front cross disabled state texture name.<br>
         * -- param texType    @see `Widget::TextureResType`
         * @method loadTextureFrontCrossDisabled
         * @param {string} _frontCrossDisabled
         * @param {ccui.Widget::TextureResType} _texType
         */
        public loadTextureFrontCrossDisabled (
            _frontCrossDisabled : string, 
            _texType : number 
        ) : void;

    }
    /**
     * @class CheckBox
     * @native
     */
    export class CheckBox 
        extends ccui.AbstractCheckButton
    {

        /**
         * Add a callback function which would be called when checkbox is selected or unselected.<br>
         * -- param callback A std::function with type @see `ccCheckBoxCallback`
         * @method addEventListener
         * @param {(arg0:cc.Ref, arg1:ccui.CheckBox::EventType) => void} _callback
         */
        public addEventListener (
            _callback : (arg0:any, arg1:any) => void 
        ) : void;

        /**
         * Create an checkbox with various images.<br>
         * -- param backGround    backGround texture.<br>
         * -- param backGroundSelected    backGround selected state texture.<br>
         * -- param cross    cross texture.<br>
         * -- param backGroundDisabled    backGround disabled state texture.<br>
         * -- param frontCrossDisabled    cross dark state texture.<br>
         * -- param texType    @see `Widget::TextureResType`<br>
         * -- return A CheckBox instance pointer.
         * @method create
         * @param {(string)} _backGround?
         * @param {(string)} _backGroundSelected | cross?
         * @param {(string) | (ccui.Widget::TextureResType)} _cross | texType?
         * @param {(string)} _backGroundDisabled?
         * @param {(string)} _frontCrossDisabled?
         * @param {(ccui.Widget::TextureResType)} _texType?
         * @return {ccui.CheckBox}
         */
        public static create (
            _backGround? : (string), 
            _backGroundSelected_cross? : (string), 
            _cross_texType? : (string) | (number), 
            _backGroundDisabled? : (string), 
            _frontCrossDisabled? : (string), 
            _texType? : (number)
        ) : ccui.CheckBox;

        /**
         * Default constructor.<br>
         * -- lua new
         * @method CheckBox
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class RadioButton
     * @native
     */
    export class RadioButton 
        extends ccui.AbstractCheckButton
    {

        /**
         * Add a callback function which would be called when radio button is selected or unselected.<br>
         * -- param callback A std::function with type @see `ccRadioButtonCallback`
         * @method addEventListener
         * @param {(arg0:ccui.RadioButton, arg1:ccui.RadioButton::EventType) => void} _callback
         */
        public addEventListener (
            _callback : (arg0:ccui.RadioButton, arg1:any) => void 
        ) : void;

        /**
         * Create a radio button with various images.<br>
         * -- param backGround    backGround texture.<br>
         * -- param backGroundSelected    backGround selected state texture.<br>
         * -- param cross    cross texture.<br>
         * -- param backGroundDisabled    backGround disabled state texture.<br>
         * -- param frontCrossDisabled    cross dark state texture.<br>
         * -- param texType    @see `Widget::TextureResType`<br>
         * -- return A RadioButton instance pointer.
         * @method create
         * @param {(string)} _backGround?
         * @param {(string)} _backGroundSelected | cross?
         * @param {(string) | (ccui.Widget::TextureResType)} _cross | texType?
         * @param {(string)} _backGroundDisabled?
         * @param {(string)} _frontCrossDisabled?
         * @param {(ccui.Widget::TextureResType)} _texType?
         * @return {ccui.RadioButton}
         */
        public static create (
            _backGround? : (string), 
            _backGroundSelected_cross? : (string), 
            _cross_texType? : (string) | (number), 
            _backGroundDisabled? : (string), 
            _frontCrossDisabled? : (string), 
            _texType? : (number)
        ) : ccui.RadioButton;

        /**
         * Default constructor.<br>
         * -- lua new
         * @method RadioButton
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class RadioButtonGroup
     * @native
     */
    export class RadioButtonGroup 
        extends ccui.Widget
    {

        /**
         * Remove a radio button from this group.<br>
         * -- param radio button instance
         * @method removeRadioButton
         * @param {ccui.RadioButton} _radioButton
         */
        public removeRadioButton (
            _radioButton : ccui.RadioButton 
        ) : void;

        /**
         * Query whether no-selection is allowed or not.<br>
         * -- param true means no-selection is allowed, false means no-selection is not allowed.
         * @method isAllowedNoSelection
         * @return {boolean}
         */
        public isAllowedNoSelection (
        ) : boolean;

        /**
         * Get the index of selected radio button.<br>
         * -- return the selected button's index. Returns -1 if no button is selected.
         * @method getSelectedButtonIndex
         * @return {number}
         */
        public getSelectedButtonIndex (
        ) : number;

        /**
         * Set a flag for allowing no-selection feature.<br>
         * -- If it is allowed, no radio button can be selected.<br>
         * -- If it is not allowed, one radio button must be selected all time except it is empty.<br>
         * -- Default is not allowed.<br>
         * -- param true means allowing no-selection, false means disallowing no-selection.
         * @method setAllowedNoSelection
         * @param {boolean} _allowedNoSelection
         */
        public setAllowedNoSelection (
            _allowedNoSelection : boolean 
        ) : void;

        /**
         * Select a radio button by instance without event dispatch.<br>
         * -- param radio button instance
         * @method setSelectedButtonWithoutEvent
         * @param {(ccui.RadioButton) | (number)} _radioButton | index
         */
        public setSelectedButtonWithoutEvent (
            _radioButton_index : (ccui.RadioButton) | (number)
        ) : void;

        /**
         * Add a callback function which would be called when radio button is selected or unselected.<br>
         * -- param callback A std::function with type @see `ccRadioButtonGroupCallback`
         * @method addEventListener
         * @param {(arg0:ccui.RadioButton, arg1:number, arg2:ccui.RadioButtonGroup::EventType) => void} _callback
         */
        public addEventListener (
            _callback : (arg0:ccui.RadioButton, arg1:number, arg2:any) => void 
        ) : void;

        /**
         * Remove all radio button from this group.
         * @method removeAllRadioButtons
         */
        public removeAllRadioButtons (
        ) : void;

        /**
         * Get a radio button in this group by index.<br>
         * -- param index of the radio button<br>
         * -- return radio button instance. Returns nullptr if out of index.
         * @method getRadioButtonByIndex
         * @param {number} _index
         * @return {ccui.RadioButton}
         */
        public getRadioButtonByIndex (
            _index : number 
        ) : ccui.RadioButton;

        /**
         * Get the number of radio buttons in this group.<br>
         * -- return the number of radio buttons in this group
         * @method getNumberOfRadioButtons
         * @return {number}
         */
        public getNumberOfRadioButtons (
        ) : number;

        /**
         * Add a radio button into this group.<br>
         * -- param radio button instance
         * @method addRadioButton
         * @param {ccui.RadioButton} _radioButton
         */
        public addRadioButton (
            _radioButton : ccui.RadioButton 
        ) : void;

        /**
         * Select a radio button by instance.<br>
         * -- param radio button instance
         * @method setSelectedButton
         * @param {(ccui.RadioButton) | (number)} _radioButton | index
         */
        public setSelectedButton (
            _radioButton_index : (ccui.RadioButton) | (number)
        ) : void;

        /**
         * Create and return a empty RadioButtonGroup instance pointer.
         * @method create
         * @return {ccui.RadioButtonGroup}
         */
        public static create (
        ) : ccui.RadioButtonGroup;

        /**
         * Default constructor.<br>
         * -- lua new
         * @method RadioButtonGroup
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class ImageView
     * @native
     */
    export class ImageView 
        extends ccui.Widget
    {

        /**
         * Load texture for imageview.<br>
         * -- param fileName   file name of texture.<br>
         * -- param texType    @see `Widget::TextureResType`
         * @method loadTexture
         * @param {string} _fileName
         * @param {ccui.Widget::TextureResType} _texType
         */
        public loadTexture (
            _fileName : string, 
            _texType : number 
        ) : void;

        /**
         * 
         * @method init
         * @param {string} _imageFileName
         * @param {ccui.Widget::TextureResType} _texType
         * @return {boolean}
         */
        public _init (
            _imageFileName : string, 
            _texType : number 
        ) : boolean;

        /**
         * Enable scale9 renderer.<br>
         * -- param enabled Set to true will use scale9 renderer, false otherwise.
         * @method setScale9Enabled
         * @param {boolean} _enabled
         */
        public setScale9Enabled (
            _enabled : boolean 
        ) : void;

        /**
         * Updates the texture rect of the ImageView in points.<br>
         * -- It will call setTextureRect:rotated:untrimmedSize with rotated = NO, and utrimmedSize = rect.size.
         * @method setTextureRect
         * @param {ctype.value_type<cc.Rect>} _rect
         */
        public setTextureRect (
            _rect : ctype.value_type<cc.Rect> 
        ) : void;

        /**
         * Sets capInsets for imageview.<br>
         * -- The capInsets affects the ImageView's renderer only if `setScale9Enabled(true)` is called.<br>
         * -- param capInsets    capinsets for imageview
         * @method setCapInsets
         * @param {ctype.value_type<cc.Rect>} _capInsets
         */
        public setCapInsets (
            _capInsets : ctype.value_type<cc.Rect> 
        ) : void;

        /**
         * 
         * @method getRenderFile
         * @return {any}
         */
        public getRenderFile (
        ) : any;

        /**
         * Get ImageView's capInsets size.<br>
         * -- return Query capInsets size in Rect<br>
         * -- see `setCapInsets(const Rect&)`
         * @method getCapInsets
         * @return {ctype.value_type<cc.Rect>}
         */
        public getCapInsets (
        ) : ctype.value_type<cc.Rect>;

        /**
         * Query whether button is using scale9 renderer or not.<br>
         * -- return whether button use scale9 renderer or not.
         * @method isScale9Enabled
         * @return {boolean}
         */
        public isScale9Enabled (
        ) : boolean;

        /**
         * Create a  imageview  with a image name.<br>
         * -- param imageFileName   file name of texture.<br>
         * -- param texType    @see `Widget::TextureResType`<br>
         * -- return A ImageView instance.
         * @method create
         * @param {(string)} _imageFileName?
         * @param {(ccui.Widget::TextureResType)} _texType?
         * @return {ccui.ImageView}
         */
        public static create (
            _imageFileName? : (string), 
            _texType? : (number)
        ) : ccui.ImageView;

        /**
         * Default constructor<br>
         * -- js ctor<br>
         * -- lua new
         * @method ImageView
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class Text
     * @native
     */
    export class Text 
        extends ccui.Widget
    {

        /**
         * Enable shadow for the label.<br>
         * -- todo support blur for shadow effect<br>
         * -- param shadowColor The color of shadow effect.<br>
         * -- param offset The offset of shadow effect.<br>
         * -- param blurRadius The blur radius of shadow effect.
         * @method enableShadow
         */
        public enableShadow (
        ) : void;

        /**
         * Gets the font size of label.<br>
         * -- return The font size.
         * @method getFontSize
         * @return {number}
         */
        public getFontSize (
        ) : number;

        /**
         * 
         * @method getString
         * @return {string}
         */
        public getString (
        ) : string;

        /**
         * Disable specific text effect.<br>
         * -- Use LabelEffect parameter to specify which effect should be disabled.<br>
         * -- see `LabelEffect`
         * @method disableEffect
         * @param {(cc.LabelEffect)} _effect?
         */
        public disableEffect (
            _effect? : (number)
        ) : void;

        /**
         * Return current effect type.
         * @method getLabelEffectType
         * @return {number}
         */
        public getLabelEffectType (
        ) : number;

        /**
         *  Gets text color.<br>
         * -- return Text color.
         * @method getTextColor
         * @return {ctype.value_type<cc.Color>}
         */
        public getTextColor (
        ) : ctype.value_type<cc.Color>;

        /**
         *  Sets text vertical alignment.<br>
         * -- param alignment vertical text alignment type
         * @method setTextVerticalAlignment
         * @param {cc.TextVAlignment} _alignment
         */
        public setTextVerticalAlignment (
            _alignment : number 
        ) : void;

        /**
         * Sets the font name of label.<br>
         * -- If you are trying to use a system font, you could just pass a font name<br>
         * -- If you are trying to use a TTF, you should pass a file path to the TTF file<br>
         * -- Usage:<br>
         * -- code<br>
         * -- create a system font UIText<br>
         * -- Text *text = Text::create("Hello", "Arial", 20);<br>
         * -- it will change the font to system font no matter the previous font type is TTF or system font<br>
         * -- text->setFontName("Marfelt");<br>
         * -- it will change the font to TTF font no matter the previous font type is TTF or system font<br>
         * -- text->setFontName("xxxx/xxx.ttf");<br>
         * -- endcode<br>
         * -- param name Font name.
         * @method setFontName
         * @param {string} _name
         */
        public setFontName (
            _name : string 
        ) : void;

        /**
         * Sets the touch scale enabled of label.<br>
         * -- param enabled Touch scale enabled of label.
         * @method setTouchScaleChangeEnabled
         * @param {boolean} _enabled
         */
        public setTouchScaleChangeEnabled (
            _enabled : boolean 
        ) : void;

        /**
         * Return shadow effect offset value.
         * @method getShadowOffset
         * @return {ctype.value_type<cc.Size>}
         */
        public getShadowOffset (
        ) : ctype.value_type<cc.Size>;

        /**
         * 
         * @method startBatchUpdate
         */
        public startBatchUpdate (
        ) : void;

        /**
         * 
         * @method setString
         * @param {string} _text
         */
        public setString (
            _text : string 
        ) : void;

        /**
         * Return the outline effect size value.
         * @method getOutlineSize
         * @return {number}
         */
        public getOutlineSize (
        ) : number;

        /**
         * 
         * @method init
         * @param {string} _textContent
         * @param {string} _fontName
         * @param {number} _fontSize
         * @return {boolean}
         */
        public init (
            _textContent : string, 
            _fontName : string, 
            _fontSize : number 
        ) : boolean;

        /**
         * 
         * @method stopBatchUpdate
         */
        public stopBatchUpdate (
        ) : void;

        /**
         * Return the shadow effect blur radius.
         * @method getShadowBlurRadius
         * @return {number}
         */
        public getShadowBlurRadius (
        ) : number;

        /**
         * Gets the touch scale enabled of label.<br>
         * -- return  Touch scale enabled of label.
         * @method isTouchScaleChangeEnabled
         * @return {boolean}
         */
        public isTouchScaleChangeEnabled (
        ) : boolean;

        /**
         *  Gets the font name.<br>
         * -- return Font name.
         * @method getFontName
         * @return {string}
         */
        public getFontName (
        ) : string;

        /**
         * Sets the rendering size of the text, you should call this method<br>
         * -- along with calling `ignoreContentAdaptWithSize(false)`, otherwise the text area<br>
         * -- size is calculated by the real size of the text content.<br>
         * -- param size The text rendering area size.
         * @method setTextAreaSize
         * @param {ctype.value_type<cc.Size>} _size
         */
        public setTextAreaSize (
            _size : ctype.value_type<cc.Size> 
        ) : void;

        /**
         * Gets the string length of the label.<br>
         * -- Note: This length will be larger than the raw string length,<br>
         * -- if you want to get the raw string length,<br>
         * -- you should call this->getString().size() instead.<br>
         * -- return  String length.
         * @method getStringLength
         * @return {number}
         */
        public getStringLength (
        ) : number;

        /**
         *  Gets the render size in auto mode.<br>
         * -- return The size of render size in auto mode.
         * @method getAutoRenderSize
         * @return {ctype.value_type<cc.Size>}
         */
        public getAutoRenderSize (
        ) : ctype.value_type<cc.Size>;

        /**
         * Enable outline for the label.<br>
         * -- It only works on IOS and Android when you use System fonts.<br>
         * -- param outlineColor The color of outline.<br>
         * -- param outlineSize The size of outline.
         * @method enableOutline
         * @param {ctype.value_type<cc.Color>} _outlineColor
         * @param {number} _outlineSize
         */
        public enableOutline (
            _outlineColor : ctype.value_type<cc.Color>, 
            _outlineSize : number 
        ) : void;

        /**
         * Return current effect color vlaue.
         * @method getEffectColor
         * @return {ctype.value_type<cc.Color>}
         */
        public getEffectColor (
        ) : ctype.value_type<cc.Color>;

        /**
         *  Gets the font type.<br>
         * -- return The font type.
         * @method getType
         * @return {number}
         */
        public getType (
        ) : number;

        /**
         *  Gets text horizontal alignment.<br>
         * -- return Horizontal text alignment type
         * @method getTextHorizontalAlignment
         * @return {number}
         */
        public getTextHorizontalAlignment (
        ) : number;

        /**
         * Return whether the shadow effect is enabled.
         * @method isShadowEnabled
         * @return {boolean}
         */
        public isShadowEnabled (
        ) : boolean;

        /**
         * Sets the font size of label.<br>
         * -- param size The font size.
         * @method setFontSize
         * @param {number} _size
         */
        public setFontSize (
            _size : number 
        ) : void;

        /**
         * Return the shadow effect color value.
         * @method getShadowColor
         * @return {ctype.value_type<cc.Color>}
         */
        public getShadowColor (
        ) : ctype.value_type<cc.Color>;

        /**
         *  Sets text color.<br>
         * -- param color Text color.
         * @method setTextColor
         * @param {ctype.value_type<cc.Color>} _color
         */
        public setTextColor (
            _color : ctype.value_type<cc.Color> 
        ) : void;

        /**
         *  Only support for TTF.<br>
         * -- param glowColor The color of glow.
         * @method enableGlow
         * @param {ctype.value_type<cc.Color>} _glowColor
         */
        public enableGlow (
            _glowColor : ctype.value_type<cc.Color> 
        ) : void;

        /**
         *  Gets text vertical alignment.<br>
         * -- return Vertical text alignment type
         * @method getTextVerticalAlignment
         * @return {number}
         */
        public getTextVerticalAlignment (
        ) : number;

        /**
         *  Return the text rendering area size.<br>
         * -- return The text rendering area size.
         * @method getTextAreaSize
         * @return {ctype.value_type<cc.Size>}
         */
        public getTextAreaSize (
        ) : ctype.value_type<cc.Size>;

        /**
         *  Sets text horizontal alignment.<br>
         * -- param alignment Horizontal text alignment type
         * @method setTextHorizontalAlignment
         * @param {cc.TextHAlignment} _alignment
         */
        public setTextHorizontalAlignment (
            _alignment : number 
        ) : void;

        /**
         * Create a Text object with textContent, fontName and fontSize.<br>
         * -- The fontName could be a system font name or a TTF file path.<br>
         * -- Usage:<br>
         * -- code<br>
         * -- create a system font UIText.<br>
         * -- Text *text = Text::create("Hello", "Arial", 20);<br>
         * -- create a TTF font UIText.<br>
         * -- Text *text = Text::create("Hello", "xxx\xxx.ttf", 20);<br>
         * -- endcode<br>
         * -- param textContent Text content string.<br>
         * -- param fontName A given font name.<br>
         * -- param fontSize A given font size.<br>
         * -- return An autoreleased Text object.
         * @method create
         * @param {(string)} _textContent?
         * @param {(string)} _fontName?
         * @param {(number)} _fontSize?
         * @return {ccui.Text}
         */
        public static create (
            _textContent? : (string), 
            _fontName? : (string), 
            _fontSize? : (number)
        ) : ccui.Text;

        /**
         * Default constructor.<br>
         * -- js ctor<br>
         * -- lua new
         * @method Text
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class TextAtlas
     * @native
     */
    export class TextAtlas 
        extends ccui.Widget
    {

        /**
         * Gets the string length of the label.<br>
         * -- Note: This length will be larger than the raw string length,<br>
         * -- if you want to get the raw string length, you should call this->getString().size() instead<br>
         * -- return  string length.
         * @method getStringLength
         * @return {number}
         */
        public getStringLength (
        ) : number;

        /**
         * 
         * @method getString
         * @return {string}
         */
        public getString (
        ) : string;

        /**
         * 
         * @method setString
         * @param {string} _value
         */
        public setString (
            _value : string 
        ) : void;

        /**
         * 
         * @method getRenderFile
         * @return {any}
         */
        public getRenderFile (
        ) : any;

        /**
         *  Initializes the LabelAtlas with a string, a char map file(the atlas), the width and height of each element and the starting char of the atlas.<br>
         * -- param stringValue A given string needs to be displayed.<br>
         * -- param charMapFile A given char map file name.<br>
         * -- param itemWidth The element width.<br>
         * -- param itemHeight The element height.<br>
         * -- param startCharMap The starting char of the atlas.
         * @method setProperty
         * @param {string} _stringValue
         * @param {string} _charMapFile
         * @param {number} _itemWidth
         * @param {number} _itemHeight
         * @param {string} _startCharMap
         */
        public setProperty (
            _stringValue : string, 
            _charMapFile : string, 
            _itemWidth : number, 
            _itemHeight : number, 
            _startCharMap : string 
        ) : void;

        /**
         * js NA
         * @method adaptRenderers
         */
        public adaptRenderers (
        ) : void;

        /**
         * Create a LabelAtlas from a char map file.<br>
         * -- param stringValue A given string needs to be displayed.<br>
         * -- param charMapFile A given char map file name.<br>
         * -- param itemWidth The element width.<br>
         * -- param itemHeight The element height.<br>
         * -- param startCharMap The starting char of the atlas.<br>
         * -- return An autoreleased TextAtlas object.
         * @method create
         * @param {(string)} _stringValue?
         * @param {(string)} _charMapFile?
         * @param {(number)} _itemWidth?
         * @param {(number)} _itemHeight?
         * @param {(string)} _startCharMap?
         * @return {ccui.TextAtlas}
         */
        public static create (
            _stringValue? : (string), 
            _charMapFile? : (string), 
            _itemWidth? : (number), 
            _itemHeight? : (number), 
            _startCharMap? : (string)
        ) : ccui.TextAtlas;

        /**
         * Default constructor.<br>
         * -- lua new
         * @method TextAtlas
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class LoadingBar
     * @native
     */
    export class LoadingBar 
        extends ccui.Widget
    {

        /**
         * Changes the progress value of LoadingBar.<br>
         * -- param percent   Percent value from 1 to 100.
         * @method setPercent
         * @param {number} _percent
         */
        public setPercent (
            _percent : number 
        ) : void;

        /**
         * Load texture for LoadingBar.<br>
         * -- param texture   File name of texture.<br>
         * -- param texType   Texture resource type,@see TextureResType.
         * @method loadTexture
         * @param {string} _texture
         * @param {ccui.Widget::TextureResType} _texType
         */
        public loadTexture (
            _texture : string, 
            _texType : number 
        ) : void;

        /**
         * Change the progress direction of LoadingBar.<br>
         * -- see Direction  `LEFT` means progress left to right, `RIGHT` otherwise.<br>
         * -- param direction Direction
         * @method setDirection
         * @param {ccui.LoadingBar::Direction} _direction
         */
        public setDirection (
            _direction : number 
        ) : void;

        /**
         * 
         * @method getRenderFile
         * @return {any}
         */
        public getRenderFile (
        ) : any;

        /**
         * Enable scale9 renderer.<br>
         * -- param enabled Set to true will use scale9 renderer, false otherwise.
         * @method setScale9Enabled
         * @param {boolean} _enabled
         */
        public setScale9Enabled (
            _enabled : boolean 
        ) : void;

        /**
         * Set capInsets for LoadingBar.<br>
         * -- This setting only take effect when enable scale9 renderer.<br>
         * -- param capInsets CapInset in `Rect`.
         * @method setCapInsets
         * @param {ctype.value_type<cc.Rect>} _capInsets
         */
        public setCapInsets (
            _capInsets : ctype.value_type<cc.Rect> 
        ) : void;

        /**
         * Get the progress direction of LoadingBar.<br>
         * -- see Direction  `LEFT` means progress left to right, `RIGHT` otherwise.<br>
         * -- return LoadingBar progress direction.
         * @method getDirection
         * @return {number}
         */
        public getDirection (
        ) : number;

        /**
         * brief Query LoadingBar's capInsets.<br>
         * -- return CapInsets of LoadingBar.
         * @method getCapInsets
         * @return {ctype.value_type<cc.Rect>}
         */
        public getCapInsets (
        ) : ctype.value_type<cc.Rect>;

        /**
         * brief Query whether LoadingBar is using scale9 renderer or not.<br>
         * -- return Whether LoadingBar uses scale9 renderer or not.
         * @method isScale9Enabled
         * @return {boolean}
         */
        public isScale9Enabled (
        ) : boolean;

        /**
         * Get the progress value of LoadingBar.<br>
         * -- return Progress value from 1 to 100.
         * @method getPercent
         * @return {number}
         */
        public getPercent (
        ) : number;

        /**
         * brief Create a LoadingBar with a texture name and a predefined progress value.<br>
         * -- param textureName LoadingBar background texture name.<br>
         * -- param percentage A percentage in float.<br>
         * -- return A LoadingBar instance.
         * @method create
         * @param {(string)} _textureName?
         * @param {(number) | (ccui.Widget::TextureResType)} _percentage | texType?
         * @param {(number)} _percentage?
         * @return {ccui.LoadingBar}
         */
        public static create (
            _textureName? : (string), 
            _percentage_texType? : (number), 
            _percentage? : (number)
        ) : ccui.LoadingBar;

        /**
         * Default constructor.<br>
         * -- js ctor<br>
         * -- lua new
         * @method LoadingBar
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class ScrollView
     * @native
     */
    export class ScrollView 
        extends ccui.Layout
    {

        /**
         * Scroll inner container to top boundary of scrollview.<br>
         * -- param timeInSec Time in seconds.<br>
         * -- param attenuated Whether scroll speed attenuate or not.
         * @method scrollToTop
         * @param {number} _timeInSec
         * @param {boolean} _attenuated
         */
        public scrollToTop (
            _timeInSec : number, 
            _attenuated : boolean 
        ) : void;

        /**
         * Scroll inner container to horizontal percent position of scrollview.<br>
         * -- param percent A value between 0 and 100.<br>
         * -- param timeInSec Time in seconds.<br>
         * -- param attenuated Whether scroll speed attenuate or not.
         * @method scrollToPercentHorizontal
         * @param {number} _percent
         * @param {number} _timeInSec
         * @param {boolean} _attenuated
         */
        public scrollToPercentHorizontal (
            _percent : number, 
            _timeInSec : number, 
            _attenuated : boolean 
        ) : void;

        /**
         * brief Set the scroll bar's opacity<br>
         * -- param the scroll bar's opacity
         * @method setScrollBarOpacity
         * @param {number} _opacity
         */
        public setScrollBarOpacity (
            _opacity : number 
        ) : void;

        /**
         * brief Toggle scroll bar enabled.<br>
         * -- param enabled True if enable scroll bar, false otherwise.
         * @method setScrollBarEnabled
         * @param {boolean} _enabled
         */
        public setScrollBarEnabled (
            _enabled : boolean 
        ) : void;

        /**
         * brief Query inertia scroll state.<br>
         * -- return True if inertia is enabled, false otherwise.
         * @method isInertiaScrollEnabled
         * @return {boolean}
         */
        public isInertiaScrollEnabled (
        ) : boolean;

        /**
         * Scroll inner container to both direction percent position of scrollview.<br>
         * -- param percent A value between 0 and 100.<br>
         * -- param timeInSec Time in seconds.<br>
         * -- param attenuated Whether scroll speed attenuate or not.
         * @method scrollToPercentBothDirection
         * @param {ctype.value_type<cc.Point>} _percent
         * @param {number} _timeInSec
         * @param {boolean} _attenuated
         */
        public scrollToPercentBothDirection (
            _percent : ctype.value_type<cc.Point>, 
            _timeInSec : number, 
            _attenuated : boolean 
        ) : void;

        /**
         * Query scroll direction of scrollview.<br>
         * -- see `Direction`      Direction::VERTICAL means vertical scroll, Direction::HORIZONTAL means horizontal scroll<br>
         * -- return Scrollview scroll direction.
         * @method getDirection
         * @return {number}
         */
        public getDirection (
        ) : number;

        /**
         * brief Set the scroll bar's color<br>
         * -- param the scroll bar's color
         * @method setScrollBarColor
         * @param {ctype.value_type<cc.Color>} _color
         */
        public setScrollBarColor (
            _color : ctype.value_type<cc.Color> 
        ) : void;

        /**
         * Scroll inner container to bottom and left boundary of scrollview.<br>
         * -- param timeInSec Time in seconds.<br>
         * -- param attenuated Whether scroll speed attenuate or not.
         * @method scrollToBottomLeft
         * @param {number} _timeInSec
         * @param {boolean} _attenuated
         */
        public scrollToBottomLeft (
            _timeInSec : number, 
            _attenuated : boolean 
        ) : void;

        /**
         * Get inner container of scrollview.<br>
         * -- Inner container is a child of scrollview.<br>
         * -- return Inner container pointer.
         * @method getInnerContainer
         * @return {ccui.Layout}
         */
        public getInnerContainer (
        ) : ccui.Layout;

        /**
         * Move inner container to bottom boundary of scrollview.
         * @method jumpToBottom
         */
        public jumpToBottom (
        ) : void;

        /**
         * Set inner container position<br>
         * -- param pos Inner container position.
         * @method setInnerContainerPosition
         * @param {ctype.value_type<cc.Point>} _pos
         */
        public setInnerContainerPosition (
            _pos : ctype.value_type<cc.Point> 
        ) : void;

        /**
         * Changes scroll direction of scrollview.<br>
         * -- see `Direction`<br>
         * -- param dir Scroll direction enum.
         * @method setDirection
         * @param {ccui.ScrollView::Direction} _dir
         */
        public setDirection (
            _dir : number 
        ) : void;

        /**
         * Scroll inner container to top and left boundary of scrollview.<br>
         * -- param timeInSec Time in seconds.<br>
         * -- param attenuated Whether scroll speed attenuate or not.
         * @method scrollToTopLeft
         * @param {number} _timeInSec
         * @param {boolean} _attenuated
         */
        public scrollToTopLeft (
            _timeInSec : number, 
            _attenuated : boolean 
        ) : void;

        /**
         * Move inner container to top and right boundary of scrollview.
         * @method jumpToTopRight
         */
        public jumpToTopRight (
        ) : void;

        /**
         * Move inner container to bottom and left boundary of scrollview.
         * @method jumpToBottomLeft
         */
        public jumpToBottomLeft (
        ) : void;

        /**
         * Change inner container size of scrollview.<br>
         * -- Inner container size must be larger than or equal scrollview's size.<br>
         * -- param size Inner container size.
         * @method setInnerContainerSize
         * @param {ctype.value_type<cc.Size>} _size
         */
        public setInnerContainerSize (
            _size : ctype.value_type<cc.Size> 
        ) : void;

        /**
         * Get inner container position<br>
         * -- return The inner container position.
         * @method getInnerContainerPosition
         * @return {ctype.value_type<cc.Point>}
         */
        public getInnerContainerPosition (
        ) : ctype.value_type<cc.Point>;

        /**
         * Get inner container size of scrollview.<br>
         * -- Inner container size must be larger than or equal scrollview's size.<br>
         * -- return The inner container size.
         * @method getInnerContainerSize
         * @return {ctype.value_type<cc.Size>}
         */
        public getInnerContainerSize (
        ) : ctype.value_type<cc.Size>;

        /**
         * brief Query bounce state.<br>
         * -- return True if bounce is enabled, false otherwise.
         * @method isBounceEnabled
         * @return {boolean}
         */
        public isBounceEnabled (
        ) : boolean;

        /**
         * Move inner container to vertical percent position of scrollview.<br>
         * -- param percent A value between 0 and 100.
         * @method jumpToPercentVertical
         * @param {number} _percent
         */
        public jumpToPercentVertical (
            _percent : number 
        ) : void;

        /**
         * Add callback function which will be called  when scrollview event triggered.<br>
         * -- param callback A callback function with type of `ccScrollViewCallback`.
         * @method addEventListener
         * @param {(arg0:cc.Ref, arg1:ccui.ScrollView::EventType) => void} _callback
         */
        public addEventListener (
            _callback : (arg0:any, arg1:any) => void 
        ) : void;

        /**
         * brief Set scroll bar auto hide time<br>
         * -- param scroll bar auto hide time
         * @method setScrollBarAutoHideTime
         * @param {number} _autoHideTime
         */
        public setScrollBarAutoHideTime (
            _autoHideTime : number 
        ) : void;

        /**
         * brief Set the horizontal scroll bar position from left-bottom corner.<br>
         * -- param positionFromCorner The position from left-bottom corner
         * @method setScrollBarPositionFromCornerForHorizontal
         * @param {ctype.value_type<cc.Point>} _positionFromCorner
         */
        public setScrollBarPositionFromCornerForHorizontal (
            _positionFromCorner : ctype.value_type<cc.Point> 
        ) : void;

        /**
         * brief Toggle whether enable scroll inertia while scrolling.<br>
         * -- param enabled True if enable inertia, false otherwise.
         * @method setInertiaScrollEnabled
         * @param {boolean} _enabled
         */
        public setInertiaScrollEnabled (
            _enabled : boolean 
        ) : void;

        /**
         * brief Set scroll bar auto hide state<br>
         * -- param scroll bar auto hide state
         * @method setScrollBarAutoHideEnabled
         * @param {boolean} _autoHideEnabled
         */
        public setScrollBarAutoHideEnabled (
            _autoHideEnabled : boolean 
        ) : void;

        /**
         * brief Get the scroll bar's color<br>
         * -- return the scroll bar's color
         * @method getScrollBarColor
         * @return {ctype.value_type<cc.Color>}
         */
        public getScrollBarColor (
        ) : ctype.value_type<cc.Color>;

        /**
         * Move inner container to top and left boundary of scrollview.
         * @method jumpToTopLeft
         */
        public jumpToTopLeft (
        ) : void;

        /**
         * Move inner container to horizontal percent position of scrollview.<br>
         * -- param percent   A value between 0 and 100.
         * @method jumpToPercentHorizontal
         * @param {number} _percent
         */
        public jumpToPercentHorizontal (
            _percent : number 
        ) : void;

        /**
         * Move inner container to bottom and right boundary of scrollview.
         * @method jumpToBottomRight
         */
        public jumpToBottomRight (
        ) : void;

        /**
         * brief Get the horizontal scroll bar's position from right-top corner.<br>
         * -- return positionFromCorner
         * @method getScrollBarPositionFromCornerForHorizontal
         * @return {ctype.value_type<cc.Point>}
         */
        public getScrollBarPositionFromCornerForHorizontal (
        ) : ctype.value_type<cc.Point>;

        /**
         * brief Set the scroll bar's width<br>
         * -- param width The scroll bar's width
         * @method setScrollBarWidth
         * @param {number} _width
         */
        public setScrollBarWidth (
            _width : number 
        ) : void;

        /**
         * brief Toggle bounce enabled when scroll to the edge.<br>
         * -- param enabled True if enable bounce, false otherwise.
         * @method setBounceEnabled
         * @param {boolean} _enabled
         */
        public setBounceEnabled (
            _enabled : boolean 
        ) : void;

        /**
         * Move inner container to top boundary of scrollview.
         * @method jumpToTop
         */
        public jumpToTop (
        ) : void;

        /**
         * Scroll inner container to left boundary of scrollview.<br>
         * -- param timeInSec Time in seconds.<br>
         * -- param attenuated Whether scroll speed attenuate or not.
         * @method scrollToLeft
         * @param {number} _timeInSec
         * @param {boolean} _attenuated
         */
        public scrollToLeft (
            _timeInSec : number, 
            _attenuated : boolean 
        ) : void;

        /**
         * Move inner container to both direction percent position of scrollview.<br>
         * -- param percent   A value between 0 and 100.
         * @method jumpToPercentBothDirection
         * @param {ctype.value_type<cc.Point>} _percent
         */
        public jumpToPercentBothDirection (
            _percent : ctype.value_type<cc.Point> 
        ) : void;

        /**
         * brief Get the vertical scroll bar's position from right-top corner.<br>
         * -- return positionFromCorner
         * @method getScrollBarPositionFromCornerForVertical
         * @return {ctype.value_type<cc.Point>}
         */
        public getScrollBarPositionFromCornerForVertical (
        ) : ctype.value_type<cc.Point>;

        /**
         * Scroll inner container to vertical percent position of scrollview.<br>
         * -- param percent A value between 0 and 100.<br>
         * -- param timeInSec Time in seconds.<br>
         * -- param attenuated Whether scroll speed attenuate or not.
         * @method scrollToPercentVertical
         * @param {number} _percent
         * @param {number} _timeInSec
         * @param {boolean} _attenuated
         */
        public scrollToPercentVertical (
            _percent : number, 
            _timeInSec : number, 
            _attenuated : boolean 
        ) : void;

        /**
         * Scroll inner container to bottom boundary of scrollview.<br>
         * -- param timeInSec Time in seconds.<br>
         * -- param attenuated Whether scroll speed attenuate or not.
         * @method scrollToBottom
         * @param {number} _timeInSec
         * @param {boolean} _attenuated
         */
        public scrollToBottom (
            _timeInSec : number, 
            _attenuated : boolean 
        ) : void;

        /**
         * brief Get the scroll bar's opacity<br>
         * -- return the scroll bar's opacity
         * @method getScrollBarOpacity
         * @return {number}
         */
        public getScrollBarOpacity (
        ) : number;

        /**
         * Scroll inner container to bottom and right boundary of scrollview.<br>
         * -- param timeInSec Time in seconds<br>
         * -- param attenuated Whether scroll speed attenuate or not.
         * @method scrollToBottomRight
         * @param {number} _timeInSec
         * @param {boolean} _attenuated
         */
        public scrollToBottomRight (
            _timeInSec : number, 
            _attenuated : boolean 
        ) : void;

        /**
         * brief Set the scroll bar positions from the left-bottom corner (horizontal) and right-top corner (vertical).<br>
         * -- param positionFromCorner The position from the left-bottom corner (horizontal) and right-top corner (vertical).
         * @method setScrollBarPositionFromCorner
         * @param {ctype.value_type<cc.Point>} _positionFromCorner
         */
        public setScrollBarPositionFromCorner (
            _positionFromCorner : ctype.value_type<cc.Point> 
        ) : void;

        /**
         * brief Set the vertical scroll bar position from right-top corner.<br>
         * -- param positionFromCorner The position from right-top corner
         * @method setScrollBarPositionFromCornerForVertical
         * @param {ctype.value_type<cc.Point>} _positionFromCorner
         */
        public setScrollBarPositionFromCornerForVertical (
            _positionFromCorner : ctype.value_type<cc.Point> 
        ) : void;

        /**
         * brief Get the scroll bar's auto hide time<br>
         * -- return the scroll bar's auto hide time
         * @method getScrollBarAutoHideTime
         * @return {number}
         */
        public getScrollBarAutoHideTime (
        ) : number;

        /**
         * Move inner container to left boundary of scrollview.
         * @method jumpToLeft
         */
        public jumpToLeft (
        ) : void;

        /**
         * Scroll inner container to right boundary of scrollview.<br>
         * -- param timeInSec Time in seconds.<br>
         * -- param attenuated Whether scroll speed attenuate or not.
         * @method scrollToRight
         * @param {number} _timeInSec
         * @param {boolean} _attenuated
         */
        public scrollToRight (
            _timeInSec : number, 
            _attenuated : boolean 
        ) : void;

        /**
         * brief Query scroll bar state.<br>
         * -- return True if scroll bar is enabled, false otherwise.
         * @method isScrollBarEnabled
         * @return {boolean}
         */
        public isScrollBarEnabled (
        ) : boolean;

        /**
         * brief Get the scroll bar's width<br>
         * -- return the scroll bar's width
         * @method getScrollBarWidth
         * @return {number}
         */
        public getScrollBarWidth (
        ) : number;

        /**
         * brief Query scroll bar auto hide state<br>
         * -- return True if scroll bar auto hide is enabled, false otherwise.
         * @method isScrollBarAutoHideEnabled
         * @return {boolean}
         */
        public isScrollBarAutoHideEnabled (
        ) : boolean;

        /**
         * Move inner container to right boundary of scrollview.
         * @method jumpToRight
         */
        public jumpToRight (
        ) : void;

        /**
         * Scroll inner container to top and right boundary of scrollview.<br>
         * -- param timeInSec Time in seconds.<br>
         * -- param attenuated Whether scroll speed attenuate or not.
         * @method scrollToTopRight
         * @param {number} _timeInSec
         * @param {boolean} _attenuated
         */
        public scrollToTopRight (
            _timeInSec : number, 
            _attenuated : boolean 
        ) : void;

        /**
         * Create an empty ScrollView.<br>
         * -- return A ScrollView instance.
         * @method create
         * @return {ccui.ScrollView}
         */
        public static create (
        ) : ccui.ScrollView;

        /**
         * Default constructor<br>
         * -- js ctor<br>
         * -- lua new
         * @method ScrollView
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class ListView
     * @native
     */
    export class ListView 
        extends ccui.ScrollView
    {

        /**
         * Set the gravity of ListView.<br>
         * -- see `ListViewGravity`
         * @method setGravity
         * @param {ccui.ListView::Gravity} _gravity
         */
        public setGravity (
            _gravity : number 
        ) : void;

        /**
         * Removes the last item of ListView.
         * @method removeLastItem
         */
        public removeLastItem (
        ) : void;

        /**
         * brief Query the center item<br>
         * -- return A item instance.
         * @method getCenterItemInCurrentView
         * @return {ccui.Widget}
         */
        public getCenterItemInCurrentView (
        ) : ccui.Widget;

        /**
         * brief Query current selected widget's idnex.<br>
         * -- return A index of a selected item.
         * @method getCurSelectedIndex
         * @return {number}
         */
        public getCurSelectedIndex (
        ) : number;

        /**
         * brief Query margin between each item in ListView.<br>
         * -- return A margin in float.
         * @method getItemsMargin
         * @return {number}
         */
        public getItemsMargin (
        ) : number;

        /**
         * brief Jump to specific item<br>
         * -- param itemIndex Specifies the item's index<br>
         * -- param positionRatioInView Specifies the position with ratio in list view's content size.<br>
         * -- param itemAnchorPoint Specifies an anchor point of each item for position to calculate distance.
         * @method jumpToItem
         * @param {number} _itemIndex
         * @param {ctype.value_type<cc.Point>} _positionRatioInView
         * @param {ctype.value_type<cc.Point>} _itemAnchorPoint
         */
        public jumpToItem (
            _itemIndex : number, 
            _positionRatioInView : ctype.value_type<cc.Point>, 
            _itemAnchorPoint : ctype.value_type<cc.Point> 
        ) : void;

        /**
         * Set magnetic type of ListView.<br>
         * -- see `MagneticType`
         * @method setMagneticType
         * @param {ccui.ListView::MagneticType} _magneticType
         */
        public setMagneticType (
            _magneticType : number 
        ) : void;

        /**
         * Return the index of specified widget.<br>
         * -- param item  A widget pointer.<br>
         * -- return The index of a given widget in ListView.
         * @method getIndex
         * @param {ccui.Widget} _item
         * @return {number}
         */
        public getIndex (
            _item : ccui.Widget 
        ) : number;

        /**
         * Insert a  custom item into the end of ListView.<br>
         * -- param item A item in `Widget*`.
         * @method pushBackCustomItem
         * @param {ccui.Widget} _item
         */
        public pushBackCustomItem (
            _item : ccui.Widget 
        ) : void;

        /**
         * Insert a default item(create by cloning model) into listview at a give index.<br>
         * -- param index  A index in ssize_t.
         * @method insertDefaultItem
         * @param {number} _index
         */
        public insertDefaultItem (
            _index : number 
        ) : void;

        /**
         * Set magnetic allowed out of boundary.
         * @method setMagneticAllowedOutOfBoundary
         * @param {boolean} _magneticAllowedOutOfBoundary
         */
        public setMagneticAllowedOutOfBoundary (
            _magneticAllowedOutOfBoundary : boolean 
        ) : void;

        /**
         * Add a event click callback to ListView, then one item of Listview is clicked, the callback will be called.<br>
         * -- param callback A callback function with type of `ccListViewCallback`.
         * @method addEventListener
         * @param {(arg0:cc.Ref, arg1:ccui.ListView::EventType) => void} _callback
         */
        public addEventListener (
            _callback : (arg0:any, arg1:any) => void 
        ) : void;

        /**
         * 
         * @method doLayout
         */
        public doLayout (
        ) : void;

        /**
         * brief Query the topmost item in horizontal list<br>
         * -- return A item instance.
         * @method getTopmostItemInCurrentView
         * @return {ccui.Widget}
         */
        public getTopmostItemInCurrentView (
        ) : ccui.Widget;

        /**
         * brief Remove all items in current ListView.
         * @method removeAllItems
         */
        public removeAllItems (
        ) : void;

        /**
         * brief Query the bottommost item in horizontal list<br>
         * -- return A item instance.
         * @method getBottommostItemInCurrentView
         * @return {ccui.Widget}
         */
        public getBottommostItemInCurrentView (
        ) : ccui.Widget;

        /**
         * Return all items in a ListView.<br>
         * -- returns A vector of widget pointers.
         * @method getItems
         * @return {ccui.Widget[]}
         */
        public getItems (
        ) : ccui.Widget[];

        /**
         * brief Query the leftmost item in horizontal list<br>
         * -- return A item instance.
         * @method getLeftmostItemInCurrentView
         * @return {ccui.Widget}
         */
        public getLeftmostItemInCurrentView (
        ) : ccui.Widget;

        /**
         * Set the margin between each item in ListView.<br>
         * -- param margin
         * @method setItemsMargin
         * @param {number} _margin
         */
        public setItemsMargin (
            _margin : number 
        ) : void;

        /**
         * Get magnetic type of ListView.
         * @method getMagneticType
         * @return {number}
         */
        public getMagneticType (
        ) : number;

        /**
         * Return a item at a given index.<br>
         * -- param index A given index in ssize_t.<br>
         * -- return A widget instance.
         * @method getItem
         * @param {number} _index
         * @return {ccui.Widget}
         */
        public getItem (
            _index : number 
        ) : ccui.Widget;

        /**
         * Remove a item at given index.<br>
         * -- param index A given index in ssize_t.
         * @method removeItem
         * @param {number} _index
         */
        public removeItem (
            _index : number 
        ) : void;

        /**
         * 
         * @method scrollToItem
         * @param {(number)} _itemIndex
         * @param {(ctype.value_type<cc.Point>)} _positionRatioInView
         * @param {(ctype.value_type<cc.Point>)} _itemAnchorPoint
         * @param {(number)} _timeInSec?
         */
        public scrollToItem (
            _itemIndex : (number), 
            _positionRatioInView : (ctype.value_type<cc.Point>), 
            _itemAnchorPoint : (ctype.value_type<cc.Point>), 
            _timeInSec? : (number)
        ) : void;

        /**
         * Insert a default item(create by a cloned model) at the end of the listview.
         * @method pushBackDefaultItem
         */
        public pushBackDefaultItem (
        ) : void;

        /**
         * Query whether the magnetic out of boundary is allowed.
         * @method getMagneticAllowedOutOfBoundary
         * @return {boolean}
         */
        public getMagneticAllowedOutOfBoundary (
        ) : boolean;

        /**
         * brief Query the closest item to a specific position in inner container.<br>
         * -- param targetPosition Specifies the target position in inner container's coordinates.<br>
         * -- param itemAnchorPoint Specifies an anchor point of each item for position to calculate distance.<br>
         * -- return A item instance if list view is not empty. Otherwise, returns null.
         * @method getClosestItemToPosition
         * @param {ctype.value_type<cc.Point>} _targetPosition
         * @param {ctype.value_type<cc.Point>} _itemAnchorPoint
         * @return {ccui.Widget}
         */
        public getClosestItemToPosition (
            _targetPosition : ctype.value_type<cc.Point>, 
            _itemAnchorPoint : ctype.value_type<cc.Point> 
        ) : ccui.Widget;

        /**
         * brief Query the rightmost item in horizontal list<br>
         * -- return A item instance.
         * @method getRightmostItemInCurrentView
         * @return {ccui.Widget}
         */
        public getRightmostItemInCurrentView (
        ) : ccui.Widget;

        /**
         * brief Query the closest item to a specific position in current view.<br>
         * -- For instance, to find the item in the center of view, call 'getClosestItemToPositionInCurrentView(Vec2::ANCHOR_MIDDLE, Vec2::ANCHOR_MIDDLE)'.<br>
         * -- param positionRatioInView Specifies the target position with ratio in list view's content size.<br>
         * -- param itemAnchorPoint Specifies an anchor point of each item for position to calculate distance.<br>
         * -- return A item instance if list view is not empty. Otherwise, returns null.
         * @method getClosestItemToPositionInCurrentView
         * @param {ctype.value_type<cc.Point>} _positionRatioInView
         * @param {ctype.value_type<cc.Point>} _itemAnchorPoint
         * @return {ccui.Widget}
         */
        public getClosestItemToPositionInCurrentView (
            _positionRatioInView : ctype.value_type<cc.Point>, 
            _itemAnchorPoint : ctype.value_type<cc.Point> 
        ) : ccui.Widget;

        /**
         * Set a item model for listview.<br>
         * -- When calling `pushBackDefaultItem`, the model will be used as a blueprint and new model copy will be inserted into ListView.<br>
         * -- param model  Model in `Widget*`.
         * @method setItemModel
         * @param {ccui.Widget} _model
         */
        public setItemModel (
            _model : ccui.Widget 
        ) : void;

        /**
         * brief Insert a custom widget into ListView at a given index.<br>
         * -- param item A widget pointer to be inserted.<br>
         * -- param index A given index in ssize_t.
         * @method insertCustomItem
         * @param {ccui.Widget} _item
         * @param {number} _index
         */
        public insertCustomItem (
            _item : ccui.Widget, 
            _index : number 
        ) : void;

        /**
         * Create an empty ListView.<br>
         * -- return A ListView instance.
         * @method create
         * @return {ccui.ListView}
         */
        public static create (
        ) : ccui.ListView;

        /**
         * Default constructor<br>
         * -- js ctor<br>
         * -- lua new
         * @method ListView
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class Slider
     * @native
     */
    export class Slider 
        extends ccui.Widget
    {

        /**
         * Changes the progress direction of slider.<br>
         * -- param percent  Percent value from 1 to 100.
         * @method setPercent
         * @param {number} _percent
         */
        public setPercent (
            _percent : number 
        ) : void;

        /**
         * Query the maximum percent of Slider. The default value is 100.<br>
         * -- since v3.7<br>
         * -- return The maximum percent of the Slider.
         * @method getMaxPercent
         * @return {number}
         */
        public getMaxPercent (
        ) : number;

        /**
         * Load normal state texture for slider ball.<br>
         * -- param normal    Normal state texture.<br>
         * -- param resType    @see TextureResType .
         * @method loadSlidBallTextureNormal
         * @param {string} _normal
         * @param {ccui.Widget::TextureResType} _resType
         */
        public loadSlidBallTextureNormal (
            _normal : string, 
            _resType : number 
        ) : void;

        /**
         * Load dark state texture for slider progress bar.<br>
         * -- param fileName   File path of texture.<br>
         * -- param resType    @see TextureResType .
         * @method loadProgressBarTexture
         * @param {string} _fileName
         * @param {ccui.Widget::TextureResType} _resType
         */
        public loadProgressBarTexture (
            _fileName : string, 
            _resType : number 
        ) : void;

        /**
         * 
         * @method getBallNormalFile
         * @return {any}
         */
        public getBallNormalFile (
        ) : any;

        /**
         * Sets if slider is using scale9 renderer.<br>
         * -- param able True that using scale9 renderer, false otherwise.
         * @method setScale9Enabled
         * @param {boolean} _able
         */
        public setScale9Enabled (
            _able : boolean 
        ) : void;

        /**
         * 
         * @method getBallPressedFile
         * @return {any}
         */
        public getBallPressedFile (
        ) : any;

        /**
         * brief Return a zoom scale<br>
         * -- since v3.3
         * @method getZoomScale
         * @return {number}
         */
        public getZoomScale (
        ) : number;

        /**
         * Sets capinsets for progress bar slider, if slider is using scale9 renderer.<br>
         * -- param capInsets Capinsets for progress bar slider.<br>
         * -- js NA
         * @method setCapInsetProgressBarRenderer
         * @param {ctype.value_type<cc.Rect>} _capInsets
         */
        public setCapInsetProgressBarRenderer (
            _capInsets : ctype.value_type<cc.Rect> 
        ) : void;

        /**
         * Load textures for slider ball.<br>
         * -- param normal     Normal state texture.<br>
         * -- param pressed    Pressed state texture.<br>
         * -- param disabled    Disabled state texture.<br>
         * -- param texType    @see TextureResType .
         * @method loadSlidBallTextures
         * @param {string} _normal
         * @param {string} _pressed
         * @param {string} _disabled
         * @param {ccui.Widget::TextureResType} _texType
         */
        public loadSlidBallTextures (
            _normal : string, 
            _pressed : string, 
            _disabled : string, 
            _texType : number 
        ) : void;

        /**
         * Add call back function called when slider's percent has changed to slider.<br>
         * -- param callback An given call back function called when slider's percent has changed to slider.
         * @method addEventListener
         * @param {(arg0:cc.Ref, arg1:ccui.Slider::EventType) => void} _callback
         */
        public addEventListener (
            _callback : (arg0:any, arg1:any) => void 
        ) : void;

        /**
         * Set a large value could give more control to the precision.<br>
         * -- since v3.7<br>
         * -- param percent The max percent of Slider.
         * @method setMaxPercent
         * @param {number} _percent
         */
        public setMaxPercent (
            _percent : number 
        ) : void;

        /**
         * Load texture for slider bar.<br>
         * -- param fileName   File name of texture.<br>
         * -- param resType    @see TextureResType .
         * @method loadBarTexture
         * @param {string} _fileName
         * @param {ccui.Widget::TextureResType} _resType
         */
        public loadBarTexture (
            _fileName : string, 
            _resType : number 
        ) : void;

        /**
         * 
         * @method getProgressBarFile
         * @return {any}
         */
        public getProgressBarFile (
        ) : any;

        /**
         * Gets capinsets for bar slider, if slider is using scale9 renderer.<br>
         * -- return capInsets Capinsets for bar slider.
         * @method getCapInsetsBarRenderer
         * @return {ctype.value_type<cc.Rect>}
         */
        public getCapInsetsBarRenderer (
        ) : ctype.value_type<cc.Rect>;

        /**
         * Gets capinsets for progress bar slider, if slider is using scale9 renderer.<br>
         * -- return Capinsets for progress bar slider.<br>
         * -- js NA
         * @method getCapInsetsProgressBarRenderer
         * @return {ctype.value_type<cc.Rect>}
         */
        public getCapInsetsProgressBarRenderer (
        ) : ctype.value_type<cc.Rect>;

        /**
         * Load pressed state texture for slider ball.<br>
         * -- param pressed    Pressed state texture.<br>
         * -- param resType    @see TextureResType .
         * @method loadSlidBallTexturePressed
         * @param {string} _pressed
         * @param {ccui.Widget::TextureResType} _resType
         */
        public loadSlidBallTexturePressed (
            _pressed : string, 
            _resType : number 
        ) : void;

        /**
         * 
         * @method getBackFile
         * @return {any}
         */
        public getBackFile (
        ) : any;

        /**
         * Gets If slider is using scale9 renderer.<br>
         * -- return True that using scale9 renderer, false otherwise.
         * @method isScale9Enabled
         * @return {boolean}
         */
        public isScale9Enabled (
        ) : boolean;

        /**
         * 
         * @method getBallDisabledFile
         * @return {any}
         */
        public getBallDisabledFile (
        ) : any;

        /**
         * Sets capinsets for bar slider, if slider is using scale9 renderer.<br>
         * -- param capInsets Capinsets for bar slider.
         * @method setCapInsetsBarRenderer
         * @param {ctype.value_type<cc.Rect>} _capInsets
         */
        public setCapInsetsBarRenderer (
            _capInsets : ctype.value_type<cc.Rect> 
        ) : void;

        /**
         * Gets the progress direction of slider.<br>
         * -- return percent Percent value from 1 to 100.
         * @method getPercent
         * @return {number}
         */
        public getPercent (
        ) : number;

        /**
         * Sets capinsets for slider, if slider is using scale9 renderer.<br>
         * -- param capInsets Capinsets for slider.
         * @method setCapInsets
         * @param {ctype.value_type<cc.Rect>} _capInsets
         */
        public setCapInsets (
            _capInsets : ctype.value_type<cc.Rect> 
        ) : void;

        /**
         * Load disabled state texture for slider ball.<br>
         * -- param disabled   Disabled state texture.<br>
         * -- param resType    @see TextureResType .
         * @method loadSlidBallTextureDisabled
         * @param {string} _disabled
         * @param {ccui.Widget::TextureResType} _resType
         */
        public loadSlidBallTextureDisabled (
            _disabled : string, 
            _resType : number 
        ) : void;

        /**
         *  When user pressed the button, the button will zoom to a scale.<br>
         * -- The final scale of the button  equals (button original scale + _zoomScale)<br>
         * -- since v3.3
         * @method setZoomScale
         * @param {number} _scale
         */
        public setZoomScale (
            _scale : number 
        ) : void;

        /**
         *  Create a slider widget with bar texture, ball texture and texture resource type.<br>
         * -- param barTextureName Bar texture file name.<br>
         * -- param normalBallTextureName Ball texture file name.<br>
         * -- param resType Texture resource type.<br>
         * -- return An autoreleased Slider object.
         * @method create
         * @param {(string)} _barTextureName?
         * @param {(string)} _normalBallTextureName?
         * @param {(ccui.Widget::TextureResType)} _resType?
         * @return {ccui.Slider}
         */
        public static create (
            _barTextureName? : (string), 
            _normalBallTextureName? : (string), 
            _resType? : (number)
        ) : ccui.Slider;

        /**
         * Default constructor.<br>
         * -- js ctor<br>
         * -- lua new
         * @method Slider
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class UICCTextField
     * @native
     */
    export class UICCTextField 
        extends cc.TextFieldTTF
    {

        /**
         * 
         * @method onTextFieldAttachWithIME
         * @param {cc.TextFieldTTF} _pSender
         * @return {boolean}
         */
        public onTextFieldAttachWithIME (
            _pSender : cc.TextFieldTTF 
        ) : boolean;

        /**
         * brief Set the password text content.<br>
         * -- param text The content of password.
         * @method setPasswordText
         * @param {string} _text
         */
        public setPasswordText (
            _text : string 
        ) : void;

        /**
         * brief Toggle attach with IME.<br>
         * -- param attach True if attach with IME, false otherwise.
         * @method setAttachWithIME
         * @param {boolean} _attach
         */
        public setAttachWithIME (
            _attach : boolean 
        ) : void;

        /**
         * brief Query whether delete backward is enabled   or not.<br>
         * -- return True if delete backward is enabled, false otherwise.
         * @method getDeleteBackward
         * @return {boolean}
         */
        public getDeleteBackward (
        ) : boolean;

        /**
         * brief Query whether the IME is attached or not.<br>
         * -- return True if IME is attached, false otherwise.
         * @method getAttachWithIME
         * @return {boolean}
         */
        public getAttachWithIME (
        ) : boolean;

        /**
         * 
         * @method onTextFieldDeleteBackward
         * @param {cc.TextFieldTTF} _pSender
         * @param {string} _delText
         * @param {number} _nLen
         * @return {boolean}
         */
        public onTextFieldDeleteBackward (
            _pSender : cc.TextFieldTTF, 
            _delText : string, 
            _nLen : number 
        ) : boolean;

        /**
         * brief Query whether insert text is enabled or not.<br>
         * -- return True if insert text is enabled, false otherwise.
         * @method getInsertText
         * @return {boolean}
         */
        public getInsertText (
        ) : boolean;

        /**
         * brief Toggle enable text insert.<br>
         * -- param insert True if enable insert text, false otherwise.
         * @method setInsertText
         * @param {boolean} _insert
         */
        public setInsertText (
            _insert : boolean 
        ) : void;

        /**
         * brief Query whether IME is detached or not.<br>
         * -- return True if IME is detached, false otherwise.
         * @method getDetachWithIME
         * @return {boolean}
         */
        public getDetachWithIME (
        ) : boolean;

        /**
         * Return the total inputed characters.<br>
         * -- return Total inputed character count.
         * @method getCharCount
         * @return {number}
         */
        public getCharCount (
        ) : number;

        /**
         * Close the IME.
         * @method closeIME
         */
        public closeIME (
        ) : void;

        /**
         * brief Toggle password input mode.<br>
         * -- param enable True if enable password input, false otherwise.
         * @method setPasswordEnabled
         * @param {boolean} _enable
         */
        public setPasswordEnabled (
            _enable : boolean 
        ) : void;

        /**
         * Toggle enable max length limitation.<br>
         * -- param enable True to enable max length, false otherwise.
         * @method setMaxLengthEnabled
         * @param {boolean} _enable
         */
        public setMaxLengthEnabled (
            _enable : boolean 
        ) : void;

        /**
         * brief Query whether password input mode is enabled or not.<br>
         * -- return True if password input is enabled, false otherwise.
         * @method isPasswordEnabled
         * @return {boolean}
         */
        public isPasswordEnabled (
        ) : boolean;

        /**
         * 
         * @method insertText
         * @param {string} _text
         * @param {number} _len
         */
        public insertText (
            _text : string, 
            _len : number 
        ) : void;

        /**
         * brief Change password style text.<br>
         * -- param styleText The styleText for password mask, the default value is "*".
         * @method setPasswordStyleText
         * @param {string} _styleText
         */
        public setPasswordStyleText (
            _styleText : string 
        ) : void;

        /**
         * 
         * @method onTextFieldInsertText
         * @param {cc.TextFieldTTF} _pSender
         * @param {string} _text
         * @param {number} _nLen
         * @return {boolean}
         */
        public onTextFieldInsertText (
            _pSender : cc.TextFieldTTF, 
            _text : string, 
            _nLen : number 
        ) : boolean;

        /**
         * 
         * @method onTextFieldDetachWithIME
         * @param {cc.TextFieldTTF} _pSender
         * @return {boolean}
         */
        public onTextFieldDetachWithIME (
            _pSender : cc.TextFieldTTF 
        ) : boolean;

        /**
         * Get maximize length.<br>
         * -- return Maximize length.
         * @method getMaxLength
         * @return {number}
         */
        public getMaxLength (
        ) : number;

        /**
         * Query max length enable state.<br>
         * -- return Whether max length is enabled or not.
         * @method isMaxLengthEnabled
         * @return {boolean}
         */
        public isMaxLengthEnabled (
        ) : boolean;

        /**
         * Open up the IME.
         * @method openIME
         */
        public openIME (
        ) : void;

        /**
         * brief Toggle detach with IME.<br>
         * -- param detach True if detach with IME, false otherwise.
         * @method setDetachWithIME
         * @param {boolean} _detach
         */
        public setDetachWithIME (
            _detach : boolean 
        ) : void;

        /**
         * Set maximize length.<br>
         * -- param length  The maximize length in integer.
         * @method setMaxLength
         * @param {number} _length
         */
        public setMaxLength (
            _length : number 
        ) : void;

        /**
         * brief Toggle enable delete backward.<br>
         * -- param deleteBackward True if enable delete backward, false otherwise.
         * @method setDeleteBackward
         * @param {boolean} _deleteBackward
         */
        public setDeleteBackward (
            _deleteBackward : boolean 
        ) : void;

        /**
         * Create a UICCTextField instance with a placeholder, a fontName and a fontSize.<br>
         * -- param placeholder Placeholder in string.<br>
         * -- param fontName Font name in string.<br>
         * -- param fontSize Font size in float.<br>
         * -- return A UICCTextField instance.
         * @method create
         * @param {(string)} _placeholder?
         * @param {(string)} _fontName?
         * @param {(number)} _fontSize?
         * @return {ccui.UICCTextField}
         */
        public static create (
            _placeholder? : (string), 
            _fontName? : (string), 
            _fontSize? : (number)
        ) : ccui.UICCTextField;

        /**
         * Default constructor
         * @method UICCTextField
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class TextField
     * @native
     */
    export class TextField 
        extends ccui.Widget
    {

        /**
         * brief Toggle attach with IME.<br>
         * -- param attach True if attach with IME, false otherwise.
         * @method setAttachWithIME
         * @param {boolean} _attach
         */
        public setAttachWithIME (
            _attach : boolean 
        ) : void;

        /**
         * brief Query the font size.<br>
         * -- return The integer font size.
         * @method getFontSize
         * @return {number}
         */
        public getFontSize (
        ) : number;

        /**
         * Query the content of TextField.<br>
         * -- return The string value of TextField.
         * @method getString
         * @return {string}
         */
        public getString (
        ) : string;

        /**
         * brief Change password style text.<br>
         * -- param styleText The styleText for password mask, the default value is "*".
         * @method setPasswordStyleText
         * @param {string} _styleText
         */
        public setPasswordStyleText (
            _styleText : string 
        ) : void;

        /**
         * brief Whether it is ready to delete backward in TextField.<br>
         * -- return True is the delete backward is enabled, false otherwise.
         * @method getDeleteBackward
         * @return {boolean}
         */
        public getDeleteBackward (
        ) : boolean;

        /**
         * brief Query the text string color.<br>
         * -- return The color of the text.
         * @method getTextColor
         * @return {ctype.value_type<cc.Color>}
         */
        public getTextColor (
        ) : ctype.value_type<cc.Color>;

        /**
         * brief Get the placeholder of TextField.<br>
         * -- return A placeholder string.
         * @method getPlaceHolder
         * @return {string}
         */
        public getPlaceHolder (
        ) : string;

        /**
         * brief Query whether the IME is attached or not.<br>
         * -- return True if IME is attached, false otherwise.
         * @method getAttachWithIME
         * @return {boolean}
         */
        public getAttachWithIME (
        ) : boolean;

        /**
         * brief Change the font name of TextField.<br>
         * -- param name The font name string.
         * @method setFontName
         * @param {string} _name
         */
        public setFontName (
            _name : string 
        ) : void;

        /**
         * brief Whether it is ready to get the inserted text or not.<br>
         * -- return True if the insert text is ready, false otherwise.
         * @method getInsertText
         * @return {boolean}
         */
        public getInsertText (
        ) : boolean;

        /**
         * brief Toggle enable insert text mode<br>
         * -- param insertText True if enable insert text, false otherwise.
         * @method setInsertText
         * @param {boolean} _insertText
         */
        public setInsertText (
            _insertText : boolean 
        ) : void;

        /**
         * Change content of TextField.<br>
         * -- param text A string content.
         * @method setString
         * @param {string} _text
         */
        public setString (
            _text : string 
        ) : void;

        /**
         * brief Query whether IME is detached or not.<br>
         * -- return True if IME is detached, false otherwise.
         * @method getDetachWithIME
         * @return {boolean}
         */
        public getDetachWithIME (
        ) : boolean;

        /**
         * brief Change the vertical text alignment.<br>
         * -- param alignment A alignment arguments in @see `TextVAlignment`.
         * @method setTextVerticalAlignment
         * @param {cc.TextVAlignment} _alignment
         */
        public setTextVerticalAlignment (
            _alignment : number 
        ) : void;

        /**
         * Add a event listener to TextField, when some predefined event happens, the callback will be called.<br>
         * -- param callback A callback function with type of `ccTextFieldCallback`.
         * @method addEventListener
         * @param {(arg0:cc.Ref, arg1:ccui.TextField::EventType) => void} _callback
         */
        public addEventListener (
            _callback : (arg0:any, arg1:any) => void 
        ) : void;

        /**
         * brief Detach the IME.
         * @method didNotSelectSelf
         */
        public didNotSelectSelf (
        ) : void;

        /**
         * brief Query the TextField's font name.<br>
         * -- return The font name string.
         * @method getFontName
         * @return {string}
         */
        public getFontName (
        ) : string;

        /**
         * brief Change the text area size.<br>
         * -- param size A delimitation zone.
         * @method setTextAreaSize
         * @param {ctype.value_type<cc.Size>} _size
         */
        public setTextAreaSize (
            _size : ctype.value_type<cc.Size> 
        ) : void;

        /**
         * brief Attach the IME for inputing.
         * @method attachWithIME
         */
        public attachWithIME (
        ) : void;

        /**
         * brief Query the input string length.<br>
         * -- return A integer length value.
         * @method getStringLength
         * @return {number}
         */
        public getStringLength (
        ) : number;

        /**
         * brief Get the renderer size in auto mode.<br>
         * -- return A delimitation zone.
         * @method getAutoRenderSize
         * @return {ctype.value_type<cc.Size>}
         */
        public getAutoRenderSize (
        ) : ctype.value_type<cc.Size>;

        /**
         * brief Toggle enable password input mode.<br>
         * -- param enable True if enable password input mode, false otherwise.
         * @method setPasswordEnabled
         * @param {boolean} _enable
         */
        public setPasswordEnabled (
            _enable : boolean 
        ) : void;

        /**
         * brief Query the placeholder string color.<br>
         * -- return The color of placeholder.
         * @method getPlaceHolderColor
         * @return {ctype.value_type<cc.Color>}
         */
        public getPlaceHolderColor (
        ) : ctype.value_type<cc.Color>;

        /**
         * brief Query the password style text.<br>
         * -- return A password style text.
         * @method getPasswordStyleText
         * @return {string}
         */
        public getPasswordStyleText (
        ) : string;

        /**
         * brief Toggle maximize length enable<br>
         * -- param enable True if enable maximize length, false otherwise.
         * @method setMaxLengthEnabled
         * @param {boolean} _enable
         */
        public setMaxLengthEnabled (
            _enable : boolean 
        ) : void;

        /**
         * brief Query whether password is enabled or not.<br>
         * -- return True if password is enabled, false otherwise.
         * @method isPasswordEnabled
         * @return {boolean}
         */
        public isPasswordEnabled (
        ) : boolean;

        /**
         * brief Toggle enable delete backward mode.<br>
         * -- param deleteBackward True is delete backward is enabled, false otherwise.
         * @method setDeleteBackward
         * @param {boolean} _deleteBackward
         */
        public setDeleteBackward (
            _deleteBackward : boolean 
        ) : void;

        /**
         * Set cursor position, if enabled<br>
         * -- js NA
         * @method setCursorPosition
         * @param {number} _cursorPosition
         */
        public setCursorPosition (
            _cursorPosition : number 
        ) : void;

        /**
         * brief Inquire the horizontal alignment<br>
         * -- return The horizontal alignment
         * @method getTextHorizontalAlignment
         * @return {number}
         */
        public getTextHorizontalAlignment (
        ) : number;

        /**
         * brief Change font size of TextField.<br>
         * -- param size The integer font size.
         * @method setFontSize
         * @param {number} _size
         */
        public setFontSize (
            _size : number 
        ) : void;

        /**
         * brief Set placeholder of TextField.<br>
         * -- param value The string value of placeholder.
         * @method setPlaceHolder
         * @param {string} _value
         */
        public setPlaceHolder (
            _value : string 
        ) : void;

        /**
         * Set cursor position to hit letter, if enabled<br>
         * -- js NA
         * @method setCursorFromPoint
         * @param {ctype.value_type<cc.Point>} _point
         * @param {cc.Camera} _camera
         */
        public setCursorFromPoint (
            _point : ctype.value_type<cc.Point>, 
            _camera : cc.Camera 
        ) : void;

        /**
         * brief Change the placeholder color.<br>
         * -- param color A color value in `Color4B`.
         * @method setPlaceHolderColor
         * @param {(ctype.value_type<cc.Color>)} _color
         */
        public setPlaceHolderColor (
            _color : (ctype.value_type<cc.Color>)
        ) : void;

        /**
         * brief Change horizontal text alignment.<br>
         * -- param alignment A alignment arguments in @see `TextHAlignment`.
         * @method setTextHorizontalAlignment
         * @param {cc.TextHAlignment} _alignment
         */
        public setTextHorizontalAlignment (
            _alignment : number 
        ) : void;

        /**
         * brief Change the text color.<br>
         * -- param textColor The color value in `Color4B`.
         * @method setTextColor
         * @param {ctype.value_type<cc.Color>} _textColor
         */
        public setTextColor (
            _textColor : ctype.value_type<cc.Color> 
        ) : void;

        /**
         * Set char showing cursor.<br>
         * -- js NA
         * @method setCursorChar
         * @param {number} _cursor
         */
        public setCursorChar (
            _cursor : number 
        ) : void;

        /**
         * brief Query maximize input length of TextField.<br>
         * -- return The integer value of maximize input length.
         * @method getMaxLength
         * @return {number}
         */
        public getMaxLength (
        ) : number;

        /**
         * brief Query whether max length is enabled or not.<br>
         * -- return True if maximize length is enabled, false otherwise.
         * @method isMaxLengthEnabled
         * @return {boolean}
         */
        public isMaxLengthEnabled (
        ) : boolean;

        /**
         * brief Toggle detach with IME.<br>
         * -- param detach True if detach with IME, false otherwise.
         * @method setDetachWithIME
         * @param {boolean} _detach
         */
        public setDetachWithIME (
            _detach : boolean 
        ) : void;

        /**
         * brief Inquire the horizontal alignment<br>
         * -- return The horizontal alignment
         * @method getTextVerticalAlignment
         * @return {number}
         */
        public getTextVerticalAlignment (
        ) : number;

        /**
         * brief Toggle enable touch area.<br>
         * -- param enable True if enable touch area, false otherwise.
         * @method setTouchAreaEnabled
         * @param {boolean} _enable
         */
        public setTouchAreaEnabled (
            _enable : boolean 
        ) : void;

        /**
         * brief Change maximize input length limitation.<br>
         * -- param length A character count in integer.
         * @method setMaxLength
         * @param {number} _length
         */
        public setMaxLength (
            _length : number 
        ) : void;

        /**
         * Set enable cursor use.<br>
         * -- js NA
         * @method setCursorEnabled
         * @param {boolean} _enabled
         */
        public setCursorEnabled (
            _enabled : boolean 
        ) : void;

        /**
         * brief Set the touch size<br>
         * -- The touch size is used for @see `hitTest`.<br>
         * -- param size A delimitation zone.
         * @method setTouchSize
         * @param {ctype.value_type<cc.Size>} _size
         */
        public setTouchSize (
            _size : ctype.value_type<cc.Size> 
        ) : void;

        /**
         * brief Get current touch size of TextField.<br>
         * -- return The TextField's touch size.
         * @method getTouchSize
         * @return {ctype.value_type<cc.Size>}
         */
        public getTouchSize (
        ) : ctype.value_type<cc.Size>;

        /**
         * brief Create a TextField with a placeholder, a font name and a font size.<br>
         * -- param placeholder The placeholder string.<br>
         * -- param fontName The font name.<br>
         * -- param fontSize The font size.<br>
         * -- return A TextField instance.
         * @method create
         * @param {(string)} _placeholder?
         * @param {(string)} _fontName?
         * @param {(number)} _fontSize?
         * @return {ccui.TextField}
         */
        public static create (
            _placeholder? : (string), 
            _fontName? : (string), 
            _fontSize? : (number)
        ) : ccui.TextField;

        /**
         * brief Default constructor.
         * @method TextField
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class TextBMFont
     * @native
     */
    export class TextBMFont 
        extends ccui.Widget
    {

        /**
         * Gets the string length of the label.<br>
         * -- Note: This length will be larger than the raw string length,<br>
         * -- if you want to get the raw string length, you should call this->getString().size() instead<br>
         * -- return  string length.
         * @method getStringLength
         * @return {number}
         */
        public getStringLength (
        ) : number;

        /**
         * 
         * @method getString
         * @return {string}
         */
        public getString (
        ) : string;

        /**
         * 
         * @method setString
         * @param {string} _value
         */
        public setString (
            _value : string 
        ) : void;

        /**
         * 
         * @method getRenderFile
         * @return {any}
         */
        public getRenderFile (
        ) : any;

        /**
         *  init a bitmap font atlas with an initial string and the FNT file 
         * @method setFntFile
         * @param {string} _fileName
         */
        public setFntFile (
            _fileName : string 
        ) : void;

        /**
         * reset TextBMFont inner label
         * @method resetRender
         */
        public resetRender (
        ) : void;

        /**
         * 
         * @method create
         * @param {(string)} _text?
         * @param {(string)} _filename?
         * @return {ccui.TextBMFont}
         */
        public static create (
            _text? : (string), 
            _filename? : (string)
        ) : ccui.TextBMFont;

        /**
         * Default constructor<br>
         * -- js ctor<br>
         * -- lua new
         * @method TextBMFont
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class PageView
     * @native
     */
    export class PageView 
        extends ccui.ListView
    {

        /**
         * brief Set space between page indicator's index nodes.<br>
         * -- param spaceBetweenIndexNodes Space between nodes in pixel.
         * @method setIndicatorSpaceBetweenIndexNodes
         * @param {number} _spaceBetweenIndexNodes
         */
        public setIndicatorSpaceBetweenIndexNodes (
            _spaceBetweenIndexNodes : number 
        ) : void;

        /**
         * brief Set color of page indicator's selected index.<br>
         * -- param spaceBetweenIndexNodes Space between nodes in pixel.
         * @method setIndicatorSelectedIndexColor
         * @param {ctype.value_type<cc.Color>} _color
         */
        public setIndicatorSelectedIndexColor (
            _color : ctype.value_type<cc.Color> 
        ) : void;

        /**
         * brief Get the color of page indicator's selected index.<br>
         * -- return color
         * @method getIndicatorSelectedIndexColor
         * @return {ctype.value_type<cc.Color>}
         */
        public getIndicatorSelectedIndexColor (
        ) : ctype.value_type<cc.Color>;

        /**
         * brief Get the page indicator's position as anchor point.<br>
         * -- return positionAsAnchorPoint
         * @method getIndicatorPositionAsAnchorPoint
         * @return {ctype.value_type<cc.Point>}
         */
        public getIndicatorPositionAsAnchorPoint (
        ) : ctype.value_type<cc.Point>;

        /**
         * brief Set the page indicator's position in page view.<br>
         * -- param position The position in page view
         * @method setIndicatorPosition
         * @param {ctype.value_type<cc.Point>} _position
         */
        public setIndicatorPosition (
            _position : ctype.value_type<cc.Point> 
        ) : void;

        /**
         * brief Get the page indicator's position.<br>
         * -- return positionAsAnchorPoint
         * @method getIndicatorPosition
         * @return {ctype.value_type<cc.Point>}
         */
        public getIndicatorPosition (
        ) : ctype.value_type<cc.Point>;

        /**
         * Insert a page into PageView at a given index.<br>
         * -- param page  Page to be inserted.<br>
         * -- param idx   A given index.
         * @method insertPage
         * @param {ccui.Widget} _page
         * @param {number} _idx
         */
        public insertPage (
            _page : ccui.Widget, 
            _idx : number 
        ) : void;

        /**
         * Gets current displayed page index.<br>
         * -- return current page index.
         * @method getCurrentPageIndex
         * @return {number}
         */
        public getCurrentPageIndex (
        ) : number;

        /**
         * Remove a page of PageView.<br>
         * -- param page  Page to be removed.
         * @method removePage
         * @param {ccui.Widget} _page
         */
        public removePage (
            _page : ccui.Widget 
        ) : void;

        /**
         * brief Add a page turn callback to PageView, then when one page is turning, the callback will be called.<br>
         * -- param callback A page turning callback.
         * @method addEventListener
         * @param {(arg0:cc.Ref, arg1:ccui.PageView::EventType) => void} _callback
         */
        public addEventListener (
            _callback : (arg0:any, arg1:any) => void 
        ) : void;

        /**
         * Jump to a page with a given index without scrolling.<br>
         * -- This is the different between scrollToPage.<br>
         * -- param index A given index in PageView. Index start from 0 to pageCount -1.
         * @method setCurrentPageIndex
         * @param {number} _index
         */
        public setCurrentPageIndex (
            _index : number 
        ) : void;

        /**
         * brief Query page indicator state.<br>
         * -- return True if page indicator is enabled, false otherwise.
         * @method getIndicatorEnabled
         * @return {boolean}
         */
        public getIndicatorEnabled (
        ) : boolean;

        /**
         * Scroll to a page with a given index.<br>
         * -- param idx   A given index in the PageView. Index start from 0 to pageCount -1.
         * @method scrollToPage
         * @param {number} _idx
         */
        public scrollToPage (
            _idx : number 
        ) : void;

        /**
         * brief Set the page indicator's position using anchor point.<br>
         * -- param positionAsAnchorPoint The position as anchor point.
         * @method setIndicatorPositionAsAnchorPoint
         * @param {ctype.value_type<cc.Point>} _positionAsAnchorPoint
         */
        public setIndicatorPositionAsAnchorPoint (
            _positionAsAnchorPoint : ctype.value_type<cc.Point> 
        ) : void;

        /**
         * Scroll to a page with a given index.<br>
         * -- param idx   A given index in the PageView. Index start from 0 to pageCount -1.
         * @method scrollToItem
         * @param {number} _itemIndex
         */
        public scrollToItem (
            _itemIndex : number 
        ) : void;

        /**
         * brief Toggle page indicator enabled.<br>
         * -- param enabled True if enable page indicator, false otherwise.
         * @method setIndicatorEnabled
         * @param {boolean} _enabled
         */
        public setIndicatorEnabled (
            _enabled : boolean 
        ) : void;

        /**
         * Insert a page into the end of PageView.<br>
         * -- param page Page to be inserted.
         * @method addPage
         * @param {ccui.Widget} _page
         */
        public addPage (
            _page : ccui.Widget 
        ) : void;

        /**
         * brief Get the space between page indicator's index nodes.<br>
         * -- return spaceBetweenIndexNodes
         * @method getIndicatorSpaceBetweenIndexNodes
         * @return {number}
         */
        public getIndicatorSpaceBetweenIndexNodes (
        ) : number;

        /**
         * brief Remove all pages of the PageView.
         * @method removeAllPages
         */
        public removeAllPages (
        ) : void;

        /**
         * Remove a page at a given index of PageView.<br>
         * -- param index  A given index.
         * @method removePageAtIndex
         * @param {number} _index
         */
        public removePageAtIndex (
            _index : number 
        ) : void;

        /**
         * Create an empty PageView.<br>
         * -- return A PageView instance.
         * @method create
         * @return {ccui.PageView}
         */
        public static create (
        ) : ccui.PageView;

        /**
         * Default constructor<br>
         * -- js ctor<br>
         * -- lua new
         * @method PageView
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class Helper
     * @native
     */
    export abstract class Helper 
    {

        /**
         * 
         * @method seekNodeByName
         * @param {cc.Node} _root
         * @param {string} _name
         * @return {cc.Node}
         */
        public static seekNodeByName (
            _root : cc.Node, 
            _name : string 
        ) : cc.Node;

        /**
         * brief Get a UTF8 substring from a std::string with a given start position and length<br>
         * -- Sample:  std::string str = "中国中国中国";  substr = getSubStringOfUTF8String(str,0,2) will = "中国"<br>
         * -- param str The source string.<br>
         * -- param start The start position of the substring.<br>
         * -- param length The length of the substring in UTF8 count<br>
         * -- return a UTF8 substring<br>
         * -- js NA
         * @method getSubStringOfUTF8String
         * @param {string} _str
         * @param {number} _start
         * @param {number} _length
         * @return {string}
         */
        public static getSubStringOfUTF8String (
            _str : string, 
            _start : number, 
            _length : number 
        ) : string;

        /**
         * brief Convert a node's boundingBox rect into screen coordinates.<br>
         * -- param node Any node pointer.<br>
         * -- return A Rect in screen coordinates.
         * @method convertBoundingBoxToScreen
         * @param {cc.Node} _node
         * @return {ctype.value_type<cc.Rect>}
         */
        public static convertBoundingBoxToScreen (
            _node : cc.Node 
        ) : ctype.value_type<cc.Rect>;

        /**
         * Change the active property of Layout's @see `LayoutComponent`<br>
         * -- param active A boolean value.
         * @method changeLayoutSystemActiveState
         * @param {boolean} _active
         */
        public static changeLayoutSystemActiveState (
            _active : boolean 
        ) : void;

        /**
         * Find a widget with a specific action tag from root widget<br>
         * -- This search will be recursive throught all child widgets.<br>
         * -- param root The be searched root widget.<br>
         * -- param tag The widget action's tag.<br>
         * -- return Widget instance pointer.
         * @method seekActionWidgetByActionTag
         * @param {ccui.Widget} _root
         * @param {number} _tag
         * @return {ccui.Widget}
         */
        public static seekActionWidgetByActionTag (
            _root : ccui.Widget, 
            _tag : number 
        ) : ccui.Widget;

        /**
         * 
         * @method seekNodeByMagicPath
         * @param {cc.Node} _root
         * @param {string} _magicPath
         * @return {cc.Node}
         */
        public static seekNodeByMagicPath (
            _root : cc.Node, 
            _magicPath : string 
        ) : cc.Node;

        /**
         * Find a widget with a specific tag from root widget.<br>
         * -- This search will be recursive throught all child widgets.<br>
         * -- param root      The be seached root widget.<br>
         * -- param tag       The widget tag.<br>
         * -- return Widget instance pointer.
         * @method seekWidgetByTag
         * @param {ccui.Widget} _root
         * @param {number} _tag
         * @return {ccui.Widget}
         */
        public static seekWidgetByTag (
            _root : ccui.Widget, 
            _tag : number 
        ) : ccui.Widget;

        /**
         * Find a widget with a specific name from root widget.<br>
         * -- This search will be recursive throught all child widgets.<br>
         * -- param root      The be searched root widget.<br>
         * -- param name      The widget name.<br>
         * -- return Widget instance pointer.
         * @method seekWidgetByName
         * @param {ccui.Widget} _root
         * @param {string} _name
         * @return {ccui.Widget}
         */
        public static seekWidgetByName (
            _root : ccui.Widget, 
            _name : string 
        ) : ccui.Widget;

        /**
         * brief  restrict capInsetSize, when the capInsets's width is larger than the textureSize, it will restrict to 0,<br>
         * -- the height goes the same way as width.<br>
         * -- param  capInsets A user defined capInsets.<br>
         * -- param  textureSize  The size of a scale9enabled texture<br>
         * -- return a restricted capInset.
         * @method restrictCapInsetRect
         * @param {ctype.value_type<cc.Rect>} _capInsets
         * @param {ctype.value_type<cc.Size>} _textureSize
         * @return {ctype.value_type<cc.Rect>}
         */
        public static restrictCapInsetRect (
            _capInsets : ctype.value_type<cc.Rect>, 
            _textureSize : ctype.value_type<cc.Size> 
        ) : ctype.value_type<cc.Rect>;

        /**
         * Refresh object and it's children layout state<br>
         * -- param rootNode   A Node* or Node* descendant instance pointer.
         * @method doLayout
         * @param {cc.Node} _rootNode
         */
        public static doLayout (
            _rootNode : cc.Node 
        ) : void;

    }
    /**
     * @class RichElement
     * @native
     */
    export class RichElement 
    {

        /**
         * brief Initialize a rich element with different arguments.<br>
         * -- param tag A integer tag value.<br>
         * -- param color A color in @see `Color3B`.<br>
         * -- param opacity A opacity value in `GLubyte`.<br>
         * -- return True if initialize success, false otherwise.
         * @method init
         * @param {number} _tag
         * @param {ctype.value_type<cc.Color>} _color
         * @param {number} _opacity
         * @return {boolean}
         */
        public init (
            _tag : number, 
            _color : ctype.value_type<cc.Color>, 
            _opacity : number 
        ) : boolean;

        /**
         * brief Default constructor.<br>
         * -- js ctor<br>
         * -- lua new
         * @method RichElement
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class RichElementText
     * @native
     */
    export class RichElementText 
        extends ccui.RichElement
    {

        /**
         * brief Initialize a RichElementText with various arguments.<br>
         * -- param tag A integer tag value.<br>
         * -- param color A color in Color3B.<br>
         * -- param opacity A opacity in GLubyte.<br>
         * -- param text Content string.<br>
         * -- param fontName Content font name.<br>
         * -- param fontSize Content font size.<br>
         * -- return True if initialize scucess, false otherwise.
         * @method init
         * @param {number} _tag
         * @param {ctype.value_type<cc.Color>} _color
         * @param {number} _opacity
         * @param {string} _text
         * @param {string} _fontName
         * @param {number} _fontSize
         * @return {boolean}
         */
        public init (
            _tag : number, 
            _color : ctype.value_type<cc.Color>, 
            _opacity : number, 
            _text : string, 
            _fontName : string, 
            _fontSize : number 
        ) : boolean;

        /**
         * brief Create a RichElementText with various arguments.<br>
         * -- param tag A integer tag value.<br>
         * -- param color A color in Color3B.<br>
         * -- param opacity A opacity in GLubyte.<br>
         * -- param text Content string.<br>
         * -- param fontName Content font name.<br>
         * -- param fontSize Content font size.<br>
         * -- return RichElementText instance.
         * @method create
         * @param {number} _tag
         * @param {ctype.value_type<cc.Color>} _color
         * @param {number} _opacity
         * @param {string} _text
         * @param {string} _fontName
         * @param {number} _fontSize
         * @return {ccui.RichElementText}
         */
        public static create (
            _tag : number, 
            _color : ctype.value_type<cc.Color>, 
            _opacity : number, 
            _text : string, 
            _fontName : string, 
            _fontSize : number 
        ) : ccui.RichElementText;

        /**
         * brief Default constructor.<br>
         * -- js ctor<br>
         * -- lua new
         * @method RichElementText
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class RichElementImage
     * @native
     */
    export class RichElementImage 
        extends ccui.RichElement
    {

        /**
         * brief Initialize a RichElementImage with various arguments.<br>
         * -- param tag A integer tag value.<br>
         * -- param color A color in Color3B.<br>
         * -- param opacity A opacity in GLubyte.<br>
         * -- param filePath A image file name.<br>
         * -- return True if initialize success, false otherwise.
         * @method init
         * @param {number} _tag
         * @param {ctype.value_type<cc.Color>} _color
         * @param {number} _opacity
         * @param {string} _filePath
         * @return {boolean}
         */
        public init (
            _tag : number, 
            _color : ctype.value_type<cc.Color>, 
            _opacity : number, 
            _filePath : string 
        ) : boolean;

        /**
         * brief Create a RichElementImage with various arguments.<br>
         * -- param tag A integer tag value.<br>
         * -- param color A color in Color3B.<br>
         * -- param opacity A opacity in GLubyte.<br>
         * -- param filePath A image file name.<br>
         * -- return A RichElementImage instance.
         * @method create
         * @param {number} _tag
         * @param {ctype.value_type<cc.Color>} _color
         * @param {number} _opacity
         * @param {string} _filePath
         * @return {ccui.RichElementImage}
         */
        public static create (
            _tag : number, 
            _color : ctype.value_type<cc.Color>, 
            _opacity : number, 
            _filePath : string 
        ) : ccui.RichElementImage;

        /**
         * brief Default constructor.<br>
         * -- js ctor<br>
         * -- lua new
         * @method RichElementImage
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class RichElementCustomNode
     * @native
     */
    export class RichElementCustomNode 
        extends ccui.RichElement
    {

        /**
         * brief Initialize a RichElementCustomNode with various arguments.<br>
         * -- param tag A integer tag value.<br>
         * -- param color A color in Color3B.<br>
         * -- param opacity A opacity in GLubyte.<br>
         * -- param customNode A custom node pointer.<br>
         * -- return True if initialize success, false otherwise.
         * @method init
         * @param {number} _tag
         * @param {ctype.value_type<cc.Color>} _color
         * @param {number} _opacity
         * @param {cc.Node} _customNode
         * @return {boolean}
         */
        public init (
            _tag : number, 
            _color : ctype.value_type<cc.Color>, 
            _opacity : number, 
            _customNode : cc.Node 
        ) : boolean;

        /**
         * brief Create a RichElementCustomNode with various arguments.<br>
         * -- param tag A integer tag value.<br>
         * -- param color A color in Color3B.<br>
         * -- param opacity A opacity in GLubyte.<br>
         * -- param customNode A custom node pointer.<br>
         * -- return A RichElementCustomNode instance.
         * @method create
         * @param {number} _tag
         * @param {ctype.value_type<cc.Color>} _color
         * @param {number} _opacity
         * @param {cc.Node} _customNode
         * @return {ccui.RichElementCustomNode}
         */
        public static create (
            _tag : number, 
            _color : ctype.value_type<cc.Color>, 
            _opacity : number, 
            _customNode : cc.Node 
        ) : ccui.RichElementCustomNode;

        /**
         * brief Default constructor.<br>
         * -- js ctor<br>
         * -- lua new
         * @method RichElementCustomNode
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class RichText
     * @native
     */
    export class RichText 
        extends ccui.Widget
    {

        /**
         * brief Insert a RichElement at a given index.<br>
         * -- param element A RichElement type.<br>
         * -- param index A given index.
         * @method insertElement
         * @param {ccui.RichElement} _element
         * @param {number} _index
         */
        public insertElement (
            _element : ccui.RichElement, 
            _index : number 
        ) : void;

        /**
         * brief Add a RichElement at the end of RichText.<br>
         * -- param element A RichElement instance.
         * @method pushBackElement
         * @param {ccui.RichElement} _element
         */
        public pushBackElement (
            _element : ccui.RichElement 
        ) : void;

        /**
         * brief Set vertical space between each RichElement.<br>
         * -- param space Point in float.
         * @method setVerticalSpace
         * @param {number} _space
         */
        public setVerticalSpace (
            _space : number 
        ) : void;

        /**
         * brief Rearrange all RichElement in the RichText.<br>
         * -- It's usually called internally.
         * @method formatText
         */
        public formatText (
        ) : void;

        /**
         * brief Remove specific RichElement.<br>
         * -- param element A RichElement type.
         * @method removeElement
         * @param {(ccui.RichElement) | (number)} _element | index
         */
        public removeElement (
            _element_index : (ccui.RichElement) | (number)
        ) : void;

        /**
         * brief Create a empty RichText.<br>
         * -- return RichText instance.
         * @method create
         * @return {ccui.RichText}
         */
        public static create (
        ) : ccui.RichText;

        /**
         * brief Default constructor.<br>
         * -- js ctor<br>
         * -- lua new
         * @method RichText
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class HBox
     * @native
     */
    export class HBox 
        extends ccui.Layout
    {

        /**
         * 
         * @method initWithSize
         * @param {ctype.value_type<cc.Size>} _size
         * @return {boolean}
         */
        public initWithSize (
            _size : ctype.value_type<cc.Size> 
        ) : boolean;

        /**
         * Create a HBox with a certain size.<br>
         * -- param size The content size of the layout.<br>
         * -- return A HBox instance pointer.
         * @method create
         * @param {(ctype.value_type<cc.Size>)} _size?
         * @return {ccui.HBox}
         */
        public static create (
            _size? : (ctype.value_type<cc.Size>)
        ) : ccui.HBox;

        /**
         * Default constructor<br>
         * -- js ctor<br>
         * -- lua new
         * @method HBox
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class VBox
     * @native
     */
    export class VBox 
        extends ccui.Layout
    {

        /**
         * 
         * @method initWithSize
         * @param {ctype.value_type<cc.Size>} _size
         * @return {boolean}
         */
        public initWithSize (
            _size : ctype.value_type<cc.Size> 
        ) : boolean;

        /**
         * Create a VBox with a certain size.<br>
         * -- param size The content size of the layout.<br>
         * -- return A VBox instance pointer.
         * @method create
         * @param {(ctype.value_type<cc.Size>)} _size?
         * @return {ccui.VBox}
         */
        public static create (
            _size? : (ctype.value_type<cc.Size>)
        ) : ccui.VBox;

        /**
         * Default constructor<br>
         * -- js ctor<br>
         * -- lua new
         * @method VBox
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class RelativeBox
     * @native
     */
    export class RelativeBox 
        extends ccui.Layout
    {

        /**
         * 
         * @method initWithSize
         * @param {ctype.value_type<cc.Size>} _size
         * @return {boolean}
         */
        public initWithSize (
            _size : ctype.value_type<cc.Size> 
        ) : boolean;

        /**
         * brief Create a RelativeBox with a fixed size.<br>
         * -- param size Size in `Size`.<br>
         * -- return A RelativeBox instance.
         * @method create
         * @param {(ctype.value_type<cc.Size>)} _size?
         * @return {ccui.RelativeBox}
         */
        public static create (
            _size? : (ctype.value_type<cc.Size>)
        ) : ccui.RelativeBox;

        /**
         * Default constructor.<br>
         * -- js ctor<br>
         * -- lua new
         * @method RelativeBox
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class Scale9Sprite
     * @native
     */
    export class Scale9Sprite 
        extends cc.Node
    {

        /**
         * 
         * @method disableCascadeColor
         */
        public disableCascadeColor (
        ) : void;

        /**
         * brief Update Scale9Sprite with a specified sprite.<br>
         * -- param sprite A sprite pointer.<br>
         * -- param rect A delimitation zone.<br>
         * -- param rotated Whether the sprite is rotated or not.<br>
         * -- param offset The offset when slice the sprite.<br>
         * -- param originalSize The origial size of the sprite.<br>
         * -- param capInsets The Values to use for the cap insets.<br>
         * -- return True if update success, false otherwise.<br>
         * -- js NA
         * @method updateWithSprite
         * @param {(cc.Sprite)} _sprite
         * @param {(ctype.value_type<cc.Rect>)} _rect
         * @param {(boolean)} _rotated
         * @param {(ctype.value_type<cc.Point>) | (ctype.value_type<cc.Rect>)} _offset | capInsets
         * @param {(ctype.value_type<cc.Size>)} _originalSize?
         * @param {(ctype.value_type<cc.Rect>)} _capInsets?
         * @return {boolean}
         */
        public updateWithSprite (
            _sprite : (cc.Sprite), 
            _rect : (ctype.value_type<cc.Rect>), 
            _rotated : (boolean), 
            _offset_capInsets : (ctype.value_type<cc.Point>) | (ctype.value_type<cc.Rect>), 
            _originalSize? : (ctype.value_type<cc.Size>), 
            _capInsets? : (ctype.value_type<cc.Rect>)
        ) : boolean;

        /**
         * Returns the flag which indicates whether the widget is flipped horizontally or not.<br>
         * -- It only flips the texture of the widget, and not the texture of the widget's children.<br>
         * -- Also, flipping the texture doesn't alter the anchorPoint.<br>
         * -- If you want to flip the anchorPoint too, and/or to flip the children too use:<br>
         * -- widget->setScaleX(sprite->getScaleX() * -1);<br>
         * -- return true if the widget is flipped horizontally, false otherwise.
         * @method isFlippedX
         * @return {boolean}
         */
        public isFlippedX (
        ) : boolean;

        /**
         * brief Toggle 9-slice feature.<br>
         * -- If Scale9Sprite is 9-slice disabled, the Scale9Sprite will rendered as a normal sprite.<br>
         * -- warning: Don't use setScale9Enabled(false), use setRenderingType(RenderingType::SIMPLE) instead.<br>
         * -- The setScale9Enabled(false) is kept only for back back compatibility.<br>
         * -- param enabled True to enable 9-slice, false otherwise.<br>
         * -- js NA
         * @method setScale9Enabled
         * @param {boolean} _enabled
         */
        public setScale9Enabled (
            _enabled : boolean 
        ) : void;

        /**
         * Sets whether the widget should be flipped vertically or not.<br>
         * -- param flippedY true if the widget should be flipped vertically, false otherwise.
         * @method setFlippedY
         * @param {boolean} _flippedY
         */
        public setFlippedY (
            _flippedY : boolean 
        ) : void;

        /**
         * Sets whether the widget should be flipped horizontally or not.<br>
         * -- param flippedX true if the widget should be flipped horizontally, false otherwise.
         * @method setFlippedX
         * @param {boolean} _flippedX
         */
        public setFlippedX (
            _flippedX : boolean 
        ) : void;

        /**
         * Creates and returns a new sprite object with the specified cap insets.<br>
         * -- You use this method to add cap insets to a sprite or to change the existing<br>
         * -- cap insets of a sprite. In both cases, you get back a new image and the<br>
         * -- original sprite remains untouched.<br>
         * -- param capInsets The values to use for the cap insets.<br>
         * -- return A Scale9Sprite instance.
         * @method resizableSpriteWithCapInsets
         * @param {ctype.value_type<cc.Rect>} _capInsets
         * @return {ccui.Scale9Sprite}
         */
        public resizableSpriteWithCapInsets (
            _capInsets : ctype.value_type<cc.Rect> 
        ) : ccui.Scale9Sprite;

        /**
         * 
         * @method disableCascadeOpacity
         */
        public disableCascadeOpacity (
        ) : void;

        /**
         * Query the current bright state.<br>
         * -- return @see `State`<br>
         * -- since v3.7
         * @method getState
         * @return {number}
         */
        public getState (
        ) : number;

        /**
         * Change the state of 9-slice sprite.<br>
         * -- see `State`<br>
         * -- param state A enum value in State.<br>
         * -- since v3.4
         * @method setState
         * @param {ccui.Scale9Sprite::State} _state
         */
        public setState (
            _state : number 
        ) : void;

        /**
         * brief Change the bottom sprite's cap inset.<br>
         * -- param bottomInset The values to use for the cap inset.
         * @method setInsetBottom
         * @param {number} _bottomInset
         */
        public setInsetBottom (
            _bottomInset : number 
        ) : void;

        /**
         * Initializes a 9-slice sprite with an sprite frame name.<br>
         * -- Once the sprite is created, you can then call its "setContentSize:" method<br>
         * -- to resize the sprite will all it's 9-slice goodness intract.<br>
         * -- It respects the anchorPoint too.<br>
         * -- param spriteFrameName The sprite frame name.<br>
         * -- return True if initializes success, false otherwise.
         * @method initWithSpriteFrameName
         * @param {(string)} _spriteFrameName
         * @param {(ctype.value_type<cc.Rect>)} _capInsets?
         * @return {boolean}
         */
        public initWithSpriteFrameName (
            _spriteFrameName : (string), 
            _capInsets? : (ctype.value_type<cc.Rect>)
        ) : boolean;

        /**
         * brief Get the original no 9-sliced sprite<br>
         * -- return A sprite instance.
         * @method getSprite
         * @return {cc.Sprite}
         */
        public getSprite (
        ) : cc.Sprite;

        /**
         * brief Change the top sprite's cap inset.<br>
         * -- param topInset The values to use for the cap inset.
         * @method setInsetTop
         * @param {number} _topInset
         */
        public setInsetTop (
            _topInset : number 
        ) : void;

        /**
         * Set the slice sprite rendering type.<br>
         * -- When setting to SIMPLE, only 4 vertexes is used to rendering.<br>
         * -- otherwise 16 vertexes will be used to rendering.<br>
         * -- see RenderingType
         * @method setRenderingType
         * @param {ccui.Scale9Sprite::RenderingType} _type
         */
        public setRenderingType (
            _type : number 
        ) : void;

        /**
         * brief Initializes a 9-slice sprite with an sprite instance.<br>
         * -- Once the sprite is created, you can then call its "setContentSize:" method<br>
         * -- to resize the sprite will all it's 9-slice goodness intract.<br>
         * -- It respects the anchorPoint too.<br>
         * -- param sprite The sprite instance.<br>
         * -- param rect A delimitation zone.<br>
         * -- param capInsets The values to use for the cap insets.<br>
         * -- return True if initializes success, false otherwise.
         * @method init
         * @param {(cc.Sprite)} _sprite
         * @param {(ctype.value_type<cc.Rect>)} _rect
         * @param {(ctype.value_type<cc.Rect>) | (boolean)} _capInsets | rotated
         * @param {(ctype.value_type<cc.Rect>) | (ctype.value_type<cc.Point>)} _capInsets | offset?
         * @param {(ctype.value_type<cc.Size>)} _originalSize?
         * @param {(ctype.value_type<cc.Rect>)} _capInsets?
         * @return {boolean}
         */
        public init (
            _sprite : (cc.Sprite), 
            _rect : (ctype.value_type<cc.Rect>), 
            _capInsets_rotated : (ctype.value_type<cc.Rect>) | (boolean), 
            _capInsets_offset? : (ctype.value_type<cc.Rect>) | (ctype.value_type<cc.Point>), 
            _originalSize? : (ctype.value_type<cc.Size>), 
            _capInsets? : (ctype.value_type<cc.Rect>)
        ) : boolean;

        /**
         * brief Change the preferred size of Scale9Sprite.<br>
         * -- param size A delimitation zone.
         * @method setPreferredSize
         * @param {ctype.value_type<cc.Size>} _size
         */
        public setPreferredSize (
            _size : ctype.value_type<cc.Size> 
        ) : void;

        /**
         * brief Change inner sprite's sprite frame.<br>
         * -- param spriteFrame A sprite frame pointer.<br>
         * -- param capInsets The values to use for the cap insets.
         * @method setSpriteFrame
         * @param {cc.SpriteFrame} _spriteFrame
         * @param {ctype.value_type<cc.Rect>} _capInsets
         */
        public setSpriteFrame (
            _spriteFrame : cc.SpriteFrame, 
            _capInsets : ctype.value_type<cc.Rect> 
        ) : void;

        /**
         * Returns the blending function that is currently being used.<br>
         * -- return A BlendFunc structure with source and destination factor which specified pixel arithmetic.<br>
         * -- js NA<br>
         * -- lua NA
         * @method getBlendFunc
         * @return {cc.BlendFunc}
         */
        public getBlendFunc (
        ) : cc.BlendFunc;

        /**
         * brief Query the bottom sprite's cap inset.<br>
         * -- return The bottom sprite's cap inset.
         * @method getInsetBottom
         * @return {number}
         */
        public getInsetBottom (
        ) : number;

        /**
         * brief Query the Scale9Sprite's preferred size.<br>
         * -- return Scale9Sprite's cap inset.
         * @method getCapInsets
         * @return {ctype.value_type<cc.Rect>}
         */
        public getCapInsets (
        ) : ctype.value_type<cc.Rect>;

        /**
         * brief Query whether the Scale9Sprite is enable 9-slice or not.<br>
         * -- return True if 9-slice is enabled, false otherwise.<br>
         * -- js NA
         * @method isScale9Enabled
         * @return {boolean}
         */
        public isScale9Enabled (
        ) : boolean;

        /**
         * 
         * @method resetRender
         */
        public resetRender (
        ) : void;

        /**
         * Return the slice sprite rendering type.
         * @method getRenderingType
         * @return {number}
         */
        public getRenderingType (
        ) : number;

        /**
         * brief Query the right sprite's cap inset.<br>
         * -- return The right sprite's cap inset.
         * @method getInsetRight
         * @return {number}
         */
        public getInsetRight (
        ) : number;

        /**
         * brief Query the sprite's original size.<br>
         * -- return Sprite size.
         * @method getOriginalSize
         * @return {ctype.value_type<cc.Size>}
         */
        public getOriginalSize (
        ) : ctype.value_type<cc.Size>;

        /**
         * Initializes a 9-slice sprite with a texture file and a delimitation zone. The<br>
         * -- texture will be broken down into a 3×3 grid of equal blocks.<br>
         * -- Once the sprite is created, you can then call its "setContentSize:" method<br>
         * -- to resize the sprite will all it's 9-slice goodness intract.<br>
         * -- It respects the anchorPoint too.<br>
         * -- param file The name of the texture file.<br>
         * -- param rect The rectangle that describes the sub-part of the texture that<br>
         * -- is the whole image. If the shape is the whole texture, set this to the<br>
         * -- texture's full rect.<br>
         * -- return True if initializes success, false otherwise.
         * @method initWithFile
         * @param {(string) | (ctype.value_type<cc.Rect>)} _file | capInsets
         * @param {(ctype.value_type<cc.Rect>) | (string)} _rect | file?
         * @param {(ctype.value_type<cc.Rect>)} _capInsets?
         * @return {boolean}
         */
        public initWithFile (
            _file_capInsets : (string) | (ctype.value_type<cc.Rect>), 
            _rect_file? : (ctype.value_type<cc.Rect>) | (string), 
            _capInsets? : (ctype.value_type<cc.Rect>)
        ) : boolean;

        /**
         * Sets the source blending function.<br>
         * -- param blendFunc A structure with source and destination factor to specify pixel arithmetic. e.g. {GL_ONE, GL_ONE}, {GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA}.<br>
         * -- js NA<br>
         * -- lua NA
         * @method setBlendFunc
         * @param {cc.BlendFunc} _blendFunc
         */
        public setBlendFunc (
            _blendFunc : cc.BlendFunc 
        ) : void;

        /**
         * brief Query the top sprite's cap inset.<br>
         * -- return The top sprite's cap inset.
         * @method getInsetTop
         * @return {number}
         */
        public getInsetTop (
        ) : number;

        /**
         * brief Change the left sprite's cap inset.<br>
         * -- param leftInset The values to use for the cap inset.
         * @method setInsetLeft
         * @param {number} _leftInset
         */
        public setInsetLeft (
            _leftInset : number 
        ) : void;

        /**
         * Initializes a 9-slice sprite with an sprite frame.<br>
         * -- Once the sprite is created, you can then call its "setContentSize:" method<br>
         * -- to resize the sprite will all it's 9-slice goodness intract.<br>
         * -- It respects the anchorPoint too.<br>
         * -- param spriteFrame The sprite frame object.<br>
         * -- return True if initializes success, false otherwise.
         * @method initWithSpriteFrame
         * @param {(cc.SpriteFrame)} _spriteFrame
         * @param {(ctype.value_type<cc.Rect>)} _capInsets?
         * @return {boolean}
         */
        public initWithSpriteFrame (
            _spriteFrame : (cc.SpriteFrame), 
            _capInsets? : (ctype.value_type<cc.Rect>)
        ) : boolean;

        /**
         * brief Query the Scale9Sprite's preferred size.<br>
         * -- return Scale9Sprite's preferred size.
         * @method getPreferredSize
         * @return {ctype.value_type<cc.Size>}
         */
        public getPreferredSize (
        ) : ctype.value_type<cc.Size>;

        /**
         * brief Change the cap inset size.<br>
         * -- param rect A delimitation zone.
         * @method setCapInsets
         * @param {ctype.value_type<cc.Rect>} _rect
         */
        public setCapInsets (
            _rect : ctype.value_type<cc.Rect> 
        ) : void;

        /**
         * Return the flag which indicates whether the widget is flipped vertically or not.<br>
         * -- It only flips the texture of the widget, and not the texture of the widget's children.<br>
         * -- Also, flipping the texture doesn't alter the anchorPoint.<br>
         * -- If you want to flip the anchorPoint too, and/or to flip the children too use:<br>
         * -- widget->setScaleY(widget->getScaleY() * -1);<br>
         * -- return true if the widget is flipped vertically, false otherwise.
         * @method isFlippedY
         * @return {boolean}
         */
        public isFlippedY (
        ) : boolean;

        /**
         * brief Query the left sprite's cap inset.<br>
         * -- return The left sprite's cap inset.
         * @method getInsetLeft
         * @return {number}
         */
        public getInsetLeft (
        ) : number;

        /**
         * brief Change the right sprite's cap inset.<br>
         * -- param rightInset The values to use for the cap inset.
         * @method setInsetRight
         * @param {number} _rightInset
         */
        public setInsetRight (
            _rightInset : number 
        ) : void;

        /**
         * Creates a 9-slice sprite with a texture file, a delimitation zone and<br>
         * -- with the specified cap insets.<br>
         * -- see initWithFile(const char *file, const Rect& rect, const Rect& capInsets)<br>
         * -- param file A texture file name.<br>
         * -- param rect A delimitation zone.<br>
         * -- param capInsets A specified cap insets.<br>
         * -- return A Scale9Sprite instance.
         * @method create
         * @param {(string) | (ctype.value_type<cc.Rect>)} _file | capInsets?
         * @param {(ctype.value_type<cc.Rect>) | (string)} _rect | file?
         * @param {(ctype.value_type<cc.Rect>)} _capInsets?
         * @return {ccui.Scale9Sprite}
         */
        public static create (
            _file_capInsets? : (string) | (ctype.value_type<cc.Rect>), 
            _rect_file? : (ctype.value_type<cc.Rect>) | (string), 
            _capInsets? : (ctype.value_type<cc.Rect>)
        ) : ccui.Scale9Sprite;

        /**
         * Creates a 9-slice sprite with an sprite frame name and the centre of its zone.<br>
         * -- Once the sprite is created, you can then call its "setContentSize:" method<br>
         * -- to resize the sprite will all it's 9-slice goodness intract.<br>
         * -- It respects the anchorPoint too.<br>
         * -- see initWithSpriteFrameName(const char *spriteFrameName, const Rect& capInsets)<br>
         * -- param spriteFrameName A sprite frame name.<br>
         * -- param capInsets A delimitation zone.<br>
         * -- return A Scale9Sprite instance.
         * @method createWithSpriteFrameName
         * @param {(string)} _spriteFrameName
         * @param {(ctype.value_type<cc.Rect>)} _capInsets?
         * @return {ccui.Scale9Sprite}
         */
        public static createWithSpriteFrameName (
            _spriteFrameName : (string), 
            _capInsets? : (ctype.value_type<cc.Rect>)
        ) : ccui.Scale9Sprite;

        /**
         * Creates a 9-slice sprite with an sprite frame and the centre of its zone.<br>
         * -- Once the sprite is created, you can then call its "setContentSize:" method<br>
         * -- to resize the sprite will all it's 9-slice goodness intract.<br>
         * -- It respects the anchorPoint too.<br>
         * -- see initWithSpriteFrame(SpriteFrame *spriteFrame, const Rect& capInsets)<br>
         * -- param spriteFrame A sprite frame pointer.<br>
         * -- param capInsets  A delimitation zone.<br>
         * -- return A Scale9Sprite instance.
         * @method createWithSpriteFrame
         * @param {(cc.SpriteFrame)} _spriteFrame
         * @param {(ctype.value_type<cc.Rect>)} _capInsets?
         * @return {ccui.Scale9Sprite}
         */
        public static createWithSpriteFrame (
            _spriteFrame : (cc.SpriteFrame), 
            _capInsets? : (ctype.value_type<cc.Rect>)
        ) : ccui.Scale9Sprite;

        /**
         * Default constructor.<br>
         * -- js ctor<br>
         * -- lua new
         * @method Scale9Sprite
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class EditBox
     * @native
     */
    export class EditBox 
        extends ccui.Widget
    {

        /**
         * Get the text entered in the edit box.<br>
         * -- return The text entered in the edit box.
         * @method getText
         * @return {string}
         */
        public getString (
        ) : string;

        /**
         * Set the font size.<br>
         * -- param fontSize The font size.
         * @method setFontSize
         * @param {number} _fontSize
         */
        public setFontSize (
            _fontSize : number 
        ) : void;

        /**
         * Set the placeholder's font name. only system font is allowed.<br>
         * -- param pFontName The font name.
         * @method setPlaceholderFontName
         * @param {string} _pFontName
         */
        public setPlaceholderFontName (
            _pFontName : string 
        ) : void;

        /**
         * Get a text in the edit box that acts as a placeholder when an<br>
         * -- edit box is empty.
         * @method getPlaceHolder
         * @return {string}
         */
        public getPlaceHolder (
        ) : string;

        /**
         * Set the font name. Only system font is allowed.<br>
         * -- param pFontName The font name.
         * @method setFontName
         * @param {string} _pFontName
         */
        public setFontName (
            _pFontName : string 
        ) : void;

        /**
         * Set the text entered in the edit box.<br>
         * -- param pText The given text.
         * @method setText
         * @param {string} _pText
         */
        public setString (
            _pText : string 
        ) : void;

        /**
         * Set the placeholder's font size.<br>
         * -- param fontSize The font size.
         * @method setPlaceholderFontSize
         * @param {number} _fontSize
         */
        public setPlaceholderFontSize (
            _fontSize : number 
        ) : void;

        /**
         * Set the input mode of the edit box.<br>
         * -- param inputMode One of the EditBox::InputMode constants.
         * @method setInputMode
         * @param {ccui.EditBox::InputMode} _inputMode
         */
        public setInputMode (
            _inputMode : number 
        ) : void;

        /**
         * Set the font color of the placeholder text when the edit box is empty.
         * @method setPlaceholderFontColor
         * @param {(ctype.value_type<cc.Color>)} _color
         */
        public setPlaceholderFontColor (
            _color : (ctype.value_type<cc.Color>)
        ) : void;

        /**
         * 
         * @method setFontColor
         * @param {(ctype.value_type<cc.Color>)} _color
         */
        public setFontColor (
            _color : (ctype.value_type<cc.Color>)
        ) : void;

        /**
         * Set the placeholder's font. Only system font is allowed.<br>
         * -- param pFontName The font name.<br>
         * -- param fontSize The font size.
         * @method setPlaceholderFont
         * @param {string} _pFontName
         * @param {number} _fontSize
         */
        public setPlaceholderFont (
            _pFontName : string, 
            _fontSize : number 
        ) : void;

        /**
         * Init edit box with specified size. This method should be invoked right after constructor.<br>
         * -- param size The size of edit box.<br>
         * -- param normal9SpriteBg  background image of edit box.<br>
         * -- return Whether initialization is successfully or not.
         * @method initWithSizeAndBackgroundSprite
         * @param {(ctype.value_type<cc.Size>)} _size
         * @param {(ccui.Scale9Sprite) | (string)} _normal9SpriteBg
         * @param {(ccui.Widget::TextureResType)} _texType?
         * @return {boolean}
         */
        public initWithSizeAndBackgroundSprite (
            _size : (ctype.value_type<cc.Size>), 
            _normal9SpriteBg : (ccui.Scale9Sprite) | (string), 
            _texType? : (number)
        ) : boolean;

        /**
         * Set a text in the edit box that acts as a placeholder when an<br>
         * -- edit box is empty.<br>
         * -- param pText The given text.
         * @method setPlaceHolder
         * @param {string} _pText
         */
        public setPlaceHolder (
            _pText : string 
        ) : void;

        /**
         * Set the return type that are to be applied to the edit box.<br>
         * -- param returnType One of the EditBox::KeyboardReturnType constants.
         * @method setReturnType
         * @param {ccui.EditBox::KeyboardReturnType} _returnType
         */
        public setReturnType (
            _returnType : number 
        ) : void;

        /**
         * Set the input flags that are to be applied to the edit box.<br>
         * -- param inputFlag One of the EditBox::InputFlag constants.
         * @method setInputFlag
         * @param {ccui.EditBox::InputFlag} _inputFlag
         */
        public setInputFlag (
            _inputFlag : number 
        ) : void;

        /**
         * Gets the maximum input length of the edit box.<br>
         * -- return Maximum input length.
         * @method getMaxLength
         * @return {number}
         */
        public getMaxLength (
        ) : number;

        /**
         * Sets the maximum input length of the edit box.<br>
         * -- Setting this value enables multiline input mode by default.<br>
         * -- Available on Android, iOS and Windows Phone.<br>
         * -- param maxLength The maximum length.
         * @method setMaxLength
         * @param {number} _maxLength
         */
        public setMaxLength (
            _maxLength : number 
        ) : void;

        /**
         * Set the font. Only system font is allowed.<br>
         * -- param pFontName The font name.<br>
         * -- param fontSize The font size.
         * @method setFont
         * @param {string} _pFontName
         * @param {number} _fontSize
         */
        public setFont (
            _pFontName : string, 
            _fontSize : number 
        ) : void;

        /**
         * create a edit box with size.<br>
         * -- return An autorelease pointer of EditBox, you don't need to release it only if you retain it again.
         * @method create
         * @param {(ctype.value_type<cc.Size>)} _size
         * @param {(string) | (ccui.Scale9Sprite)} _normal9SpriteBg | normalSprite
         * @param {(ccui.Widget::TextureResType) | (ccui.Scale9Sprite)} _texType | pressedSprite
         * @param {(ccui.Scale9Sprite)} _disabledSprite?
         * @return {ccui.EditBox}
         */
        public static create (
            _size : (ctype.value_type<cc.Size>), 
            _normal9SpriteBg_normalSprite : (string) | (ccui.Scale9Sprite), 
            _texType_pressedSprite : (number) | (ccui.Scale9Sprite), 
            _disabledSprite? : (ccui.Scale9Sprite)
        ) : ccui.EditBox;

        /**
         * Constructor.<br>
         * -- js ctor<br>
         * -- lua new
         * @method EditBox
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class LayoutComponent
     * @native
     */
    export class LayoutComponent 
        extends cc.Component
    {

        /**
         * Toggle enable stretch width.<br>
         * -- param isUsed True if enable stretch width, false otherwise.
         * @method setStretchWidthEnabled
         * @param {boolean} _isUsed
         */
        public setStretchWidthEnabled (
            _isUsed : boolean 
        ) : void;

        /**
         * Change percent width of owner.<br>
         * -- param percentWidth Percent Width in float.
         * @method setPercentWidth
         * @param {number} _percentWidth
         */
        public setPercentWidth (
            _percentWidth : number 
        ) : void;

        /**
         * Query the anchor position.<br>
         * -- return Anchor position to it's parent
         * @method getAnchorPosition
         * @return {ctype.value_type<cc.Point>}
         */
        public getAnchorPosition (
        ) : ctype.value_type<cc.Point>;

        /**
         * Toggle position percentX enabled.<br>
         * -- param isUsed  True if enable position percentX, false otherwise.
         * @method setPositionPercentXEnabled
         * @param {boolean} _isUsed
         */
        public setPositionPercentXEnabled (
            _isUsed : boolean 
        ) : void;

        /**
         * Toggle enable stretch height.<br>
         * -- param isUsed True if stretch height is enabled, false otherwise.
         * @method setStretchHeightEnabled
         * @param {boolean} _isUsed
         */
        public setStretchHeightEnabled (
            _isUsed : boolean 
        ) : void;

        /**
         * Toggle active enabled of LayoutComponent's owner.<br>
         * -- param enable True if active layout component, false otherwise.
         * @method setActiveEnabled
         * @param {boolean} _enable
         */
        public setActiveEnabled (
            _enable : boolean 
        ) : void;

        /**
         * Query the right margin of owner relative to its parent.<br>
         * -- return Right margin in float.
         * @method getRightMargin
         * @return {number}
         */
        public getRightMargin (
        ) : number;

        /**
         * Query owner's content size.<br>
         * -- return Owner's content size.
         * @method getSize
         * @return {ctype.value_type<cc.Size>}
         */
        public getSize (
        ) : ctype.value_type<cc.Size>;

        /**
         * Change the anchor position to it's parent.<br>
         * -- param point A value in (x,y) format.
         * @method setAnchorPosition
         * @param {ctype.value_type<cc.Point>} _point
         */
        public setAnchorPosition (
            _point : ctype.value_type<cc.Point> 
        ) : void;

        /**
         * Refresh layout of the owner.
         * @method refreshLayout
         */
        public refreshLayout (
        ) : void;

        /**
         * Query whether percent width is enabled or not.<br>
         * -- return True if percent width is enabled, false, otherwise.
         * @method isPercentWidthEnabled
         * @return {boolean}
         */
        public isPercentWidthEnabled (
        ) : boolean;

        /**
         * Change element's vertical dock type.<br>
         * -- param vEage Vertical dock type @see `VerticalEdge`.
         * @method setVerticalEdge
         * @param {ccui.LayoutComponent::VerticalEdge} _vEage
         */
        public setVerticalEdge (
            _vEage : number 
        ) : void;

        /**
         * Query the top margin of owner relative to its parent.<br>
         * -- return Top margin in float.
         * @method getTopMargin
         * @return {number}
         */
        public getTopMargin (
        ) : number;

        /**
         * Change content size width of owner.<br>
         * -- param width Content size width in float.
         * @method setSizeWidth
         * @param {number} _width
         */
        public setSizeWidth (
            _width : number 
        ) : void;

        /**
         * Query the percent content size value.<br>
         * -- return Percet (x,y) in Vec2.
         * @method getPercentContentSize
         * @return {ctype.value_type<cc.Point>}
         */
        public getPercentContentSize (
        ) : ctype.value_type<cc.Point>;

        /**
         * Query element vertical dock type.<br>
         * -- return Vertical dock type.
         * @method getVerticalEdge
         * @return {number}
         */
        public getVerticalEdge (
        ) : number;

        /**
         * Toggle enable percent width.<br>
         * -- param isUsed True if percent width is enabled, false otherwise.
         * @method setPercentWidthEnabled
         * @param {boolean} _isUsed
         */
        public setPercentWidthEnabled (
            _isUsed : boolean 
        ) : void;

        /**
         * Query whether stretch width is enabled or not.<br>
         * -- return True if stretch width is enabled, false otherwise.
         * @method isStretchWidthEnabled
         * @return {boolean}
         */
        public isStretchWidthEnabled (
        ) : boolean;

        /**
         * Change left margin of owner relative to its parent.<br>
         * -- param margin Margin in float.
         * @method setLeftMargin
         * @param {number} _margin
         */
        public setLeftMargin (
            _margin : number 
        ) : void;

        /**
         * Query content size width of owner.<br>
         * -- return Content size width in float.
         * @method getSizeWidth
         * @return {number}
         */
        public getSizeWidth (
        ) : number;

        /**
         * Toggle position percentY enabled.<br>
         * -- param isUsed True if position percentY is enabled, false otherwise.
         * @method setPositionPercentYEnabled
         * @param {boolean} _isUsed
         */
        public setPositionPercentYEnabled (
            _isUsed : boolean 
        ) : void;

        /**
         * Query size height of owner.<br>
         * -- return Size height in float.
         * @method getSizeHeight
         * @return {number}
         */
        public getSizeHeight (
        ) : number;

        /**
         * Query the position percentY Y value.<br>
         * -- return Position percent Y value in float.
         * @method getPositionPercentY
         * @return {number}
         */
        public getPositionPercentY (
        ) : number;

        /**
         * Query the position percent X value.<br>
         * -- return Position percent X value in float.
         * @method getPositionPercentX
         * @return {number}
         */
        public getPositionPercentX (
        ) : number;

        /**
         * Change the top margin of owner relative to its parent.<br>
         * -- param margin Margin in float.
         * @method setTopMargin
         * @param {number} _margin
         */
        public setTopMargin (
            _margin : number 
        ) : void;

        /**
         * Query percent height of owner.         <br>
         * -- return Percent height in float.
         * @method getPercentHeight
         * @return {number}
         */
        public getPercentHeight (
        ) : number;

        /**
         * Query whether use percent content size or not.<br>
         * -- return True if using percent content size, false otherwise.
         * @method getUsingPercentContentSize
         * @return {boolean}
         */
        public getUsingPercentContentSize (
        ) : boolean;

        /**
         * Change position percentY value.<br>
         * -- param percentMargin Margin in float.
         * @method setPositionPercentY
         * @param {number} _percentMargin
         */
        public setPositionPercentY (
            _percentMargin : number 
        ) : void;

        /**
         * Change position percent X value.<br>
         * -- param percentMargin Margin in float.
         * @method setPositionPercentX
         * @param {number} _percentMargin
         */
        public setPositionPercentX (
            _percentMargin : number 
        ) : void;

        /**
         * Change right margin of owner relative to its parent.<br>
         * -- param margin Margin in float.
         * @method setRightMargin
         * @param {number} _margin
         */
        public setRightMargin (
            _margin : number 
        ) : void;

        /**
         * Whether position percentY is enabled or not.<br>
         * -- see `setPositionPercentYEnabled`<br>
         * -- return True if position percentY is enabled, false otherwise.
         * @method isPositionPercentYEnabled
         * @return {boolean}
         */
        public isPositionPercentYEnabled (
        ) : boolean;

        /**
         * Change percent height value of owner.<br>
         * -- param percentHeight Percent height in float.
         * @method setPercentHeight
         * @param {number} _percentHeight
         */
        public setPercentHeight (
            _percentHeight : number 
        ) : void;

        /**
         * Toggle enable percent only.<br>
         * -- param enable True if percent only is enabled, false otherwise.
         * @method setPercentOnlyEnabled
         * @param {boolean} _enable
         */
        public setPercentOnlyEnabled (
            _enable : boolean 
        ) : void;

        /**
         * Change element's horizontal dock type.<br>
         * -- param hEage Horizontal dock type @see `HorizontalEdge`
         * @method setHorizontalEdge
         * @param {ccui.LayoutComponent::HorizontalEdge} _hEage
         */
        public setHorizontalEdge (
            _hEage : number 
        ) : void;

        /**
         * Change the position of component owner.<br>
         * -- param position A position in (x,y)
         * @method setPosition
         * @param {ctype.value_type<cc.Point>} _position
         */
        public setPosition (
            _position : ctype.value_type<cc.Point> 
        ) : void;

        /**
         * Percent content size is used to adapt node's content size based on parent's content size.<br>
         * -- If set to true then node's content size will be changed based on the value setted by @see setPercentContentSize<br>
         * -- param isUsed True to enable percent content size, false otherwise.
         * @method setUsingPercentContentSize
         * @param {boolean} _isUsed
         */
        public setUsingPercentContentSize (
            _isUsed : boolean 
        ) : void;

        /**
         * Query left margin of owner relative to its parent.<br>
         * -- return Left margin in float.
         * @method getLeftMargin
         * @return {number}
         */
        public getLeftMargin (
        ) : number;

        /**
         * Query the owner's position.<br>
         * -- return The owner's position.
         * @method getPosition
         * @return {ctype.value_type<cc.Point>}
         */
        public getPosition (
        ) : ctype.value_type<cc.Point>;

        /**
         * Change size height of owner.<br>
         * -- param height Size height in float.
         * @method setSizeHeight
         * @param {number} _height
         */
        public setSizeHeight (
            _height : number 
        ) : void;

        /**
         * Whether position percentX is enabled or not. <br>
         * -- return True if position percentX is enable, false otherwise.
         * @method isPositionPercentXEnabled
         * @return {boolean}
         */
        public isPositionPercentXEnabled (
        ) : boolean;

        /**
         * Query the bottom margin of owner relative to its parent.<br>
         * -- return Bottom margin in float.
         * @method getBottomMargin
         * @return {number}
         */
        public getBottomMargin (
        ) : number;

        /**
         * Toggle enable percent height.<br>
         * -- param isUsed True if percent height is enabled, false otherwise.
         * @method setPercentHeightEnabled
         * @param {boolean} _isUsed
         */
        public setPercentHeightEnabled (
            _isUsed : boolean 
        ) : void;

        /**
         * Set percent content size.<br>
         * -- The value should be [0-1], 0 means the child's content size will be 0<br>
         * -- and 1 means the child's content size is the same as its parents.<br>
         * -- param percent The percent (x,y) of the node in [0-1] scope.
         * @method setPercentContentSize
         * @param {ctype.value_type<cc.Point>} _percent
         */
        public setPercentContentSize (
            _percent : ctype.value_type<cc.Point> 
        ) : void;

        /**
         * Query whether percent height is enabled or not.<br>
         * -- return True if percent height is enabled, false otherwise.
         * @method isPercentHeightEnabled
         * @return {boolean}
         */
        public isPercentHeightEnabled (
        ) : boolean;

        /**
         * Query percent width of owner.<br>
         * -- return percent width in float.
         * @method getPercentWidth
         * @return {number}
         */
        public getPercentWidth (
        ) : number;

        /**
         * Query element horizontal dock type.<br>
         * -- return Horizontal dock type.
         * @method getHorizontalEdge
         * @return {number}
         */
        public getHorizontalEdge (
        ) : number;

        /**
         * Query whether stretch height is enabled or not.<br>
         * -- return True if stretch height is enabled, false otherwise.
         * @method isStretchHeightEnabled
         * @return {boolean}
         */
        public isStretchHeightEnabled (
        ) : boolean;

        /**
         * Change the bottom margin of owner relative to its parent.<br>
         * -- param margin in float.
         * @method setBottomMargin
         * @param {number} _margin
         */
        public setBottomMargin (
            _margin : number 
        ) : void;

        /**
         * Change the content size of owner.<br>
         * -- param size Content size in @see `Size`.
         * @method setSize
         * @param {ctype.value_type<cc.Size>} _size
         */
        public setSize (
            _size : ctype.value_type<cc.Size> 
        ) : void;

        /**
         * 
         * @method create
         * @return {ccui.LayoutComponent}
         */
        public static create (
        ) : ccui.LayoutComponent;

        /**
         * Bind a LayoutComponent to a specified node.<br>
         * -- If the node has already binded a LayoutComponent named __LAYOUT_COMPONENT_NAME, just return the LayoutComponent.<br>
         * -- Otherwise, create a new LayoutComponent and bind the LayoutComponent to the node.<br>
         * -- param node A Node* instance pointer.<br>
         * -- return The binded LayoutComponent instance pointer.
         * @method bindLayoutComponent
         * @param {cc.Node} _node
         * @return {ccui.LayoutComponent}
         */
        public static bindLayoutComponent (
            _node : cc.Node 
        ) : ccui.LayoutComponent;

        /**
         * Default constructor<br>
         * -- lua new
         * @method LayoutComponent
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class ScrollViewBar
     * @native
     */
    export class ScrollViewBar 
        extends cc.ProtectedNode
    {

        /**
         * brief Set scroll bar auto hide state<br>
         * -- param scroll bar auto hide state
         * @method setAutoHideEnabled
         * @param {boolean} _autoHideEnabled
         */
        public setAutoHideEnabled (
            _autoHideEnabled : boolean 
        ) : void;

        /**
         * brief This is called by parent ScrollView when the parent is scrolled. Don't call this directly.<br>
         * -- param amount how much the inner container of ScrollView is out of boundary
         * @method onScrolled
         * @param {ctype.value_type<cc.Point>} _outOfBoundary
         */
        public onScrolled (
            _outOfBoundary : ctype.value_type<cc.Point> 
        ) : void;

        /**
         * brief Query scroll bar auto hide state<br>
         * -- return True if scroll bar auto hide is enabled, false otherwise.
         * @method isAutoHideEnabled
         * @return {boolean}
         */
        public isAutoHideEnabled (
        ) : boolean;

        /**
         * brief Set scroll bar auto hide time<br>
         * -- param scroll bar auto hide time
         * @method setAutoHideTime
         * @param {number} _autoHideTime
         */
        public setAutoHideTime (
            _autoHideTime : number 
        ) : void;

        /**
         * brief Get the scroll bar's width<br>
         * -- return the scroll bar's width
         * @method getWidth
         * @return {number}
         */
        public getWidth (
        ) : number;

        /**
         * brief Get the scroll bar position from the left-bottom corner (horizontal) or right-top corner (vertical).<br>
         * -- return positionFromCorner
         * @method getPositionFromCorner
         * @return {ctype.value_type<cc.Point>}
         */
        public getPositionFromCorner (
        ) : ctype.value_type<cc.Point>;

        /**
         * brief Set the scroll bar position from the left-bottom corner (horizontal) or right-top corner (vertical).<br>
         * -- param positionFromCorner The position from the left-bottom corner (horizontal) or right-top corner (vertical).
         * @method setPositionFromCorner
         * @param {ctype.value_type<cc.Point>} _positionFromCorner
         */
        public setPositionFromCorner (
            _positionFromCorner : ctype.value_type<cc.Point> 
        ) : void;

        /**
         * brief Get the scroll bar's auto hide time<br>
         * -- return the scroll bar's auto hide time
         * @method getAutoHideTime
         * @return {number}
         */
        public getAutoHideTime (
        ) : number;

        /**
         * brief Set the scroll bar's width<br>
         * -- param width The scroll bar's width
         * @method setWidth
         * @param {number} _width
         */
        public setWidth (
            _width : number 
        ) : void;

        /**
         * Create a scroll bar with its parent scroll view and direction.<br>
         * -- return A scroll bar instance.
         * @method create
         * @param {ccui.ScrollView} _parent
         * @param {ccui.ScrollView::Direction} _direction
         * @return {ccui.ScrollViewBar}
         */
        public static create (
            _parent : ccui.ScrollView, 
            _direction : number 
        ) : ccui.ScrollViewBar;

        /**
         * Default constructor<br>
         * -- js ctor<br>
         * -- lua new
         * @method ScrollViewBar
         * @constructor
         * @param {ccui.ScrollView} _parent
         * @param {ccui.ScrollView::Direction} _direction
         */
        public constructor (
            _parent : ccui.ScrollView, 
            _direction : number 
        );

    }
}