/**
 * @module cocos2dx_3d
 */
declare namespace jsb {
    /**
     * @class Animation3D
     * @native
     */
    export class Animation3D 
    {

        /**
         * init Animation3D with file name and animation name
         * @method initWithFile
         * @param {string} _filename
         * @param {string} _animationName
         * @return {boolean}
         */
        public initWithFile (
            _filename : string, 
            _animationName : string 
        ) : boolean;

        /**
         * init Animation3D from bundle data
         * @method init
         * @param {cc.Animation3DData} _data
         * @return {boolean}
         */
        public init (
            _data : cc.Animation3DData 
        ) : boolean;

        /**
         * get bone curve<br>
         * -- lua NA
         * @method getBoneCurveByName
         * @param {string} _name
         * @return {any}
         */
        public getBoneCurveByName (
            _name : string 
        ) : any;

        /**
         * get duration
         * @method getDuration
         * @return {number}
         */
        public getDuration (
        ) : number;

        /**
         * 
         * @method Animation3D
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class Animate3D
     * @native
     */
    export class Animate3D 
        extends cc.ActionInterval
    {

        /**
         * 
         * @method setKeyFrameUserInfo
         * @param {number} _keyFrame
         * @param {any} _userInfo
         */
        public setKeyFrameUserInfo (
            _keyFrame : number, 
            _userInfo : any 
        ) : void;

        /**
         * get & set speed, negative speed means playing reverse 
         * @method getSpeed
         * @return {number}
         */
        public getSpeed (
        ) : number;

        /**
         * set animate quality
         * @method setQuality
         * @param {cc.Animate3DQuality} _quality
         */
        public setQuality (
            _quality : number 
        ) : void;

        /**
         * 
         * @method setWeight
         * @param {number} _weight
         */
        public setWeight (
            _weight : number 
        ) : void;

        /**
         * 
         * @method removeFromMap
         */
        public removeFromMap (
        ) : void;

        /**
         * 
         * @method initWithFrames
         * @param {cc.Animation3D} _animation
         * @param {number} _startFrame
         * @param {number} _endFrame
         * @param {number} _frameRate
         * @return {boolean}
         */
        public initWithFrames (
            _animation : cc.Animation3D, 
            _startFrame : number, 
            _endFrame : number, 
            _frameRate : number 
        ) : boolean;

        /**
         * 
         * @method getOriginInterval
         * @return {number}
         */
        public getOriginInterval (
        ) : number;

        /**
         * 
         * @method setSpeed
         * @param {number} _speed
         */
        public setSpeed (
            _speed : number 
        ) : void;

        /**
         * 
         * @method init
         * @param {(cc.Animation3D)} _animation
         * @param {(number)} _fromTime?
         * @param {(number)} _duration?
         * @return {boolean}
         */
        public init (
            _animation : (cc.Animation3D), 
            _fromTime? : (number), 
            _duration? : (number)
        ) : boolean;

        /**
         * get & set origin interval
         * @method setOriginInterval
         * @param {number} _interval
         */
        public setOriginInterval (
            _interval : number 
        ) : void;

        /**
         * get & set blend weight, weight must positive
         * @method getWeight
         * @return {number}
         */
        public getWeight (
        ) : number;

        /**
         * get animate quality
         * @method getQuality
         * @return {number}
         */
        public getQuality (
        ) : number;

        /**
         * create Animate3D<br>
         * -- param animation used to generate animate3D<br>
         * -- param formTime <br>
         * -- param duration Time the Animate3D lasts<br>
         * -- return Animate3D created using animate
         * @method create
         * @param {(cc.Animation3D)} _animation
         * @param {(number)} _fromTime?
         * @param {(number)} _duration?
         * @return {cc.Animate3D}
         */
        public static create (
            _animation : (cc.Animation3D), 
            _fromTime? : (number), 
            _duration? : (number)
        ) : cc.Animate3D;

        /**
         *  get animate transition time between 3d animations 
         * @method getTransitionTime
         * @return {number}
         */
        public static getTransitionTime (
        ) : number;

        /**
         * create Animate3D by frame section, [startFrame, endFrame)<br>
         * -- param animation used to generate animate3D<br>
         * -- param startFrame<br>
         * -- param endFrame<br>
         * -- param frameRate default is 30 per second<br>
         * -- return Animate3D created using animate
         * @method createWithFrames
         * @param {cc.Animation3D} _animation
         * @param {number} _startFrame
         * @param {number} _endFrame
         * @param {number} _frameRate
         * @return {cc.Animate3D}
         */
        public static createWithFrames (
            _animation : cc.Animation3D, 
            _startFrame : number, 
            _endFrame : number, 
            _frameRate : number 
        ) : cc.Animate3D;

        /**
         *  set animate transition time between 3d animations 
         * @method setTransitionTime
         * @param {number} _transTime
         */
        public static setTransitionTime (
            _transTime : number 
        ) : void;

        /**
         * 
         * @method Animate3D
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class TextureCube
     * @native
     */
    export class TextureCube 
        extends cc.Texture2D
    {

        /**
         *  reload texture cube after GLESContext reconstructed.
         * @method reloadTexture
         * @return {boolean}
         */
        public reloadTexture (
        ) : boolean;

        /**
         *  create cube texture from 6 textures.<br>
         * -- param positive_x texture for the right side of the texture cube face.<br>
         * -- param negative_x texture for the up side of the texture cube face.<br>
         * -- param positive_y texture for the top side of the texture cube face<br>
         * -- param negative_y texture for the bottom side of the texture cube face<br>
         * -- param positive_z texture for the forward side of the texture cube face.<br>
         * -- param negative_z texture for the rear side of the texture cube face.<br>
         * -- return  A new texture cube inited with given parameters.
         * @method create
         * @param {string} _positive_x
         * @param {string} _negative_x
         * @param {string} _positive_y
         * @param {string} _negative_y
         * @param {string} _positive_z
         * @param {string} _negative_z
         * @return {cc.TextureCube}
         */
        public static create (
            _positive_x : string, 
            _negative_x : string, 
            _positive_y : string, 
            _negative_y : string, 
            _positive_z : string, 
            _negative_z : string 
        ) : cc.TextureCube;

        /**
         * Constructor.
         * @method TextureCube
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class AttachNode
     * @native
     */
    export class AttachNode 
        extends cc.Node
    {

        /**
         * creates an AttachNode<br>
         * -- param attachBone The bone to which the AttachNode is going to attach, the attacheBone must be a bone of the AttachNode's parent
         * @method create
         * @param {cc.Bone3D} _attachBone
         * @return {cc.AttachNode}
         */
        public static create (
            _attachBone : cc.Bone3D 
        ) : cc.AttachNode;

        /**
         * 
         * @method AttachNode
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class BillBoard
     * @native
     */
    export class BillBoard 
        extends cc.Sprite
    {

        /**
         *  Get the billboard rotation mode. 
         * @method getMode
         * @return {number}
         */
        public getMode (
        ) : number;

        /**
         *  Set the billboard rotation mode. 
         * @method setMode
         * @param {cc.BillBoard::Mode} _mode
         */
        public setMode (
            _mode : number 
        ) : void;

        /**
         * Creates a BillBoard with an image filename.<br>
         * -- After creation, the rect of BillBoard will be the size of the image,<br>
         * -- and the offset will be (0,0).<br>
         * -- param   filename A path to image file, e.g., "scene1/monster.png"<br>
         * -- return  An autoreleased BillBoard object.
         * @method create
         * @param {(string) | (cc.BillBoard::Mode)} _filename | mode
         * @param {(cc.BillBoard::Mode) | (ctype.value_type<cc.Rect>)} _mode | rect?
         * @param {(cc.BillBoard::Mode)} _mode?
         * @return {cc.BillBoard}
         */
        public static create (
            _filename_mode : (string) | (number), 
            _mode_rect? : (number) | (ctype.value_type<cc.Rect>), 
            _mode? : (number)
        ) : cc.BillBoard;

        /**
         * Creates a BillBoard with a Texture2D object.<br>
         * -- After creation, the rect will be the size of the texture, and the offset will be (0,0).<br>
         * -- param   texture    A pointer to a Texture2D object.<br>
         * -- return  An autoreleased BillBoard object
         * @method createWithTexture
         * @param {cc.Texture2D} _texture
         * @param {cc.BillBoard::Mode} _mode
         * @return {cc.BillBoard}
         */
        public static createWithTexture (
            _texture : cc.Texture2D, 
            _mode : number 
        ) : cc.BillBoard;

        /**
         * 
         * @method BillBoard
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class Mesh
     * @native
     */
    export class Mesh 
    {

        /**
         * skin getter<br>
         * -- lua NA
         * @method getSkin
         * @return {cc.MeshSkin}
         */
        public getSkin (
        ) : cc.MeshSkin;

        /**
         *  Returns the Material being used by the Mesh 
         * @method getMaterial
         * @return {cc.Material}
         */
        public getMaterial (
        ) : cc.Material;

        /**
         * get per vertex size in bytes
         * @method getVertexSizeInBytes
         * @return {number}
         */
        public getVertexSizeInBytes (
        ) : number;

        /**
         *  Sets a new Material to the Mesh 
         * @method setMaterial
         * @param {cc.Material} _material
         */
        public setMaterial (
            _material : cc.Material 
        ) : void;

        /**
         * name getter 
         * @method getName
         * @return {string}
         */
        public getName (
        ) : string;

        /**
         * get index format<br>
         * -- lua NA
         * @method getIndexFormat
         * @return {number}
         */
        public getIndexFormat (
        ) : number;

        /**
         * get GLProgramState<br>
         * -- lua NA
         * @method getGLProgramState
         * @return {cc.GLProgramState}
         */
        public getGLProgramState (
        ) : cc.GLProgramState;

        /**
         * get vertex buffer<br>
         * -- lua NA
         * @method getVertexBuffer
         * @return {number}
         */
        public getVertexBuffer (
        ) : number;

        /**
         * calculate the AABB of the mesh<br>
         * -- note the AABB is in the local space, not the world space
         * @method calculateAABB
         */
        public calculateAABB (
        ) : void;

        /**
         * has vertex attribute?<br>
         * -- lua NA
         * @method hasVertexAttrib
         * @param {number} _attrib
         * @return {boolean}
         */
        public hasVertexAttrib (
            _attrib : number 
        ) : boolean;

        /**
         * 
         * @method getBlendFunc
         * @return {cc.BlendFunc}
         */
        public getBlendFunc (
        ) : cc.BlendFunc;

        /**
         * mesh index data getter<br>
         * -- lua NA
         * @method getMeshIndexData
         * @return {cc.MeshIndexData}
         */
        public getMeshIndexData (
        ) : cc.MeshIndexData;

        /**
         * name setter
         * @method setName
         * @param {string} _name
         */
        public setName (
            _name : string 
        ) : void;

        /**
         * get index count<br>
         * -- lua NA
         * @method getIndexCount
         * @return {number}
         */
        public getIndexCount (
        ) : number;

        /**
         * Mesh index data setter
         * @method setMeshIndexData
         * @param {cc.MeshIndexData} _indexdata
         */
        public setMeshIndexData (
            _indexdata : cc.MeshIndexData 
        ) : void;

        /**
         * get mesh vertex attribute count
         * @method getMeshVertexAttribCount
         * @return {number}
         */
        public getMeshVertexAttribCount (
        ) : number;

        /**
         * 
         * @method setBlendFunc
         * @param {cc.BlendFunc} _blendFunc
         */
        public setBlendFunc (
            _blendFunc : cc.BlendFunc 
        ) : void;

        /**
         * force set this Sprite3D to 2D render queue
         * @method setForce2DQueue
         * @param {boolean} _force2D
         */
        public setForce2DQueue (
            _force2D : boolean 
        ) : void;

        /**
         * get primitive type<br>
         * -- lua NA
         * @method getPrimitiveType
         * @return {number}
         */
        public getPrimitiveType (
        ) : number;

        /**
         * skin setter
         * @method setSkin
         * @param {cc.MeshSkin} _skin
         */
        public setSkin (
            _skin : cc.MeshSkin 
        ) : void;

        /**
         * 
         * @method isVisible
         * @return {boolean}
         */
        public isVisible (
        ) : boolean;

        /**
         * get index buffer<br>
         * -- lua NA
         * @method getIndexBuffer
         * @return {number}
         */
        public getIndexBuffer (
        ) : number;

        /**
         *   Sets a new GLProgramState for the Mesh<br>
         * -- A new Material will be created for it
         * @method setGLProgramState
         * @param {cc.GLProgramState} _glProgramState
         */
        public setGLProgramState (
            _glProgramState : cc.GLProgramState 
        ) : void;

        /**
         * visible getter and setter
         * @method setVisible
         * @param {boolean} _visible
         */
        public setVisible (
            _visible : boolean 
        ) : void;

        /**
         * 
         * @method Mesh
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class Skeleton3D
     * @native
     */
    export class Skeleton3D 
    {

        /**
         * remove all bones
         * @method removeAllBones
         */
        public removeAllBones (
        ) : void;

        /**
         * add bone
         * @method addBone
         * @param {cc.Bone3D} _bone
         */
        public addBone (
            _bone : cc.Bone3D 
        ) : void;

        /**
         * 
         * @method getBoneByName
         * @param {string} _id
         * @return {cc.Bone3D}
         */
        public getBoneByName (
            _id : string 
        ) : cc.Bone3D;

        /**
         * 
         * @method getRootBone
         * @param {number} _index
         * @return {cc.Bone3D}
         */
        public getRootBone (
            _index : number 
        ) : cc.Bone3D;

        /**
         * refresh bone world matrix
         * @method updateBoneMatrix
         */
        public updateBoneMatrix (
        ) : void;

        /**
         * get bone
         * @method getBoneByIndex
         * @param {number} _index
         * @return {cc.Bone3D}
         */
        public getBoneByIndex (
            _index : number 
        ) : cc.Bone3D;

        /**
         * get & set root bone
         * @method getRootCount
         * @return {number}
         */
        public getRootCount (
        ) : number;

        /**
         * get bone index
         * @method getBoneIndex
         * @param {cc.Bone3D} _bone
         * @return {number}
         */
        public getBoneIndex (
            _bone : cc.Bone3D 
        ) : number;

        /**
         * get total bone count
         * @method getBoneCount
         * @return {number}
         */
        public getBoneCount (
        ) : number;

        /**
         * 
         * @method Skeleton3D
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class Skybox
     * @native
     */
    export class Skybox 
        extends cc.Node
    {

        /**
         *  reload sky box after GLESContext reconstructed.
         * @method reload
         */
        public reload (
        ) : void;

        /**
         * initialize with texture path
         * @method init
         * @param {string} _positive_x
         * @param {string} _negative_x
         * @param {string} _positive_y
         * @param {string} _negative_y
         * @param {string} _positive_z
         * @param {string} _negative_z
         * @return {boolean}
         */
        public init (
            _positive_x : string, 
            _negative_x : string, 
            _positive_y : string, 
            _negative_y : string, 
            _positive_z : string, 
            _negative_z : string 
        ) : boolean;

        /**
         * texture getter and setter
         * @method setTexture
         * @param {cc.TextureCube} _arg0
         */
        public setTexture (
            _arg0 : cc.TextureCube 
        ) : void;

        /**
         *  create skybox from 6 textures.<br>
         * -- param positive_x texture for the right side of the texture cube face.<br>
         * -- param negative_x texture for the left side of the texture cube face.<br>
         * -- param positive_y texture for the top side of the texture cube face<br>
         * -- param negative_y texture for the bottom side of the texture cube face<br>
         * -- param positive_z texture for the forward side of the texture cube face.<br>
         * -- param negative_z texture for the rear side of the texture cube face.<br>
         * -- return  A new skybox inited with given parameters.
         * @method create
         * @param {(string)} _positive_x?
         * @param {(string)} _negative_x?
         * @param {(string)} _positive_y?
         * @param {(string)} _negative_y?
         * @param {(string)} _positive_z?
         * @param {(string)} _negative_z?
         * @return {cc.Skybox}
         */
        public static create (
            _positive_x? : (string), 
            _negative_x? : (string), 
            _positive_y? : (string), 
            _negative_y? : (string), 
            _positive_z? : (string), 
            _negative_z? : (string)
        ) : cc.Skybox;

        /**
         * Constructor.
         * @method Skybox
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class Sprite3D
     * @native
     */
    export class Sprite3D 
        extends cc.Node
    {

        /**
         * 
         * @method setCullFaceEnabled
         * @param {boolean} _enable
         */
        public setCullFaceEnabled (
            _enable : boolean 
        ) : void;

        /**
         * 
         * @method setTexture
         * @param {(cc.Texture2D) | (string)} _texture | texFile
         */
        public setTexture (
            _texture_texFile : (cc.Texture2D) | (string)
        ) : void;

        /**
         * 
         * @method getLightMask
         * @return {number}
         */
        public getLightMask (
        ) : number;

        /**
         * 
         * @method createAttachSprite3DNode
         * @param {cc.NodeData} _nodedata
         * @param {cc.MaterialDatas} _matrialdatas
         */
        public createAttachSprite3DNode (
            _nodedata : cc.NodeData, 
            _matrialdatas : cc.MaterialDatas 
        ) : void;

        /**
         *  load file and set it to meshedatas, nodedatas and materialdatas, obj file .mtl file should be at the same directory if exist 
         * @method loadFromFile
         * @param {string} _path
         * @param {cc.NodeDatas} _nodedatas
         * @param {cc.MeshDatas} _meshdatas
         * @param {cc.MaterialDatas} _materialdatas
         * @return {boolean}
         */
        public loadFromFile (
            _path : string, 
            _nodedatas : cc.NodeDatas, 
            _meshdatas : cc.MeshDatas, 
            _materialdatas : cc.MaterialDatas 
        ) : boolean;

        /**
         *  Adds a new material to a particular mesh of the sprite.<br>
         * -- meshIndex is the mesh that will be applied to.<br>
         * -- if meshIndex == -1, then it will be applied to all the meshes that belong to the sprite.
         * @method getMaterial
         * @param {number} _meshIndex
         * @return {cc.Material}
         */
        public getMaterial (
            _meshIndex : number 
        ) : cc.Material;

        /**
         * 
         * @method setCullFace
         * @param {number} _cullFace
         */
        public setCullFace (
            _cullFace : number 
        ) : void;

        /**
         * Get meshes used in sprite 3d
         * @method getMeshes
         * @return {cc.Mesh[]}
         */
        public getMeshes (
        ) : cc.Mesh[];

        /**
         * 
         * @method addMesh
         * @param {cc.Mesh} _mesh
         */
        public addMesh (
            _mesh : cc.Mesh 
        ) : void;

        /**
         * remove all attach nodes
         * @method removeAllAttachNode
         */
        public removeAllAttachNode (
        ) : void;

        /**
         *  Adds a new material to a particular mesh of the sprite.<br>
         * -- meshIndex is the mesh that will be applied to.<br>
         * -- if meshIndex == -1, then it will be applied to all the meshes that belong to the sprite.
         * @method setMaterial
         * @param {(cc.Material)} _material
         * @param {(number)} _meshIndex?
         */
        public setMaterial (
            _material : (cc.Material), 
            _meshIndex? : (number)
        ) : void;

        /**
         * get mesh
         * @method getMesh
         * @return {cc.Mesh}
         */
        public getMesh (
        ) : cc.Mesh;

        /**
         * 
         * @method createSprite3DNode
         * @param {cc.NodeData} _nodedata
         * @param {cc.ModelData} _modeldata
         * @param {cc.MaterialDatas} _matrialdatas
         * @return {cc.Sprite3D}
         */
        public createSprite3DNode (
            _nodedata : cc.NodeData, 
            _modeldata : cc.ModelData, 
            _matrialdatas : cc.MaterialDatas 
        ) : cc.Sprite3D;

        /**
         *  get mesh count 
         * @method getMeshCount
         * @return {number}
         */
        public getMeshCount (
        ) : number;

        /**
         * 
         * @method onAABBDirty
         */
        public onAABBDirty (
        ) : void;

        /**
         * get Mesh by index
         * @method getMeshByIndex
         * @param {number} _index
         * @return {cc.Mesh}
         */
        public getMeshByIndex (
            _index : number 
        ) : cc.Mesh;

        /**
         * 
         * @method createNode
         * @param {cc.NodeData} _nodedata
         * @param {cc.Node} _root
         * @param {cc.MaterialDatas} _matrialdatas
         * @param {boolean} _singleSprite
         */
        public createNode (
            _nodedata : cc.NodeData, 
            _root : cc.Node, 
            _matrialdatas : cc.MaterialDatas, 
            _singleSprite : boolean 
        ) : void;

        /**
         * 
         * @method isForceDepthWrite
         * @return {boolean}
         */
        public isForceDepthWrite (
        ) : boolean;

        /**
         * 
         * @method getBlendFunc
         * @return {cc.BlendFunc}
         */
        public getBlendFunc (
        ) : cc.BlendFunc;

        /**
         * get MeshIndexData by Id
         * @method getMeshIndexData
         * @param {string} _indexId
         * @return {cc.MeshIndexData}
         */
        public getMeshIndexData (
            _indexId : string 
        ) : cc.MeshIndexData;

        /**
         *  light mask getter & setter, light works only when _lightmask & light's flag is true, default value of _lightmask is 0xffff 
         * @method setLightMask
         * @param {number} _mask
         */
        public setLightMask (
            _mask : number 
        ) : void;

        /**
         * 
         * @method afterAsyncLoad
         * @param {void} _param
         */
        public afterAsyncLoad (
            _param : void 
        ) : void;

        /**
         * load sprite3d from cache, return true if succeed, false otherwise
         * @method loadFromCache
         * @param {string} _path
         * @return {boolean}
         */
        public loadFromCache (
            _path : string 
        ) : boolean;

        /**
         * 
         * @method initFrom
         * @param {cc.NodeDatas} _nodedatas
         * @param {cc.MeshDatas} _meshdatas
         * @param {cc.MaterialDatas} _materialdatas
         * @return {boolean}
         */
        public initFrom (
            _nodedatas : cc.NodeDatas, 
            _meshdatas : cc.MeshDatas, 
            _materialdatas : cc.MaterialDatas 
        ) : boolean;

        /**
         * get AttachNode by bone name, return nullptr if not exist
         * @method getAttachNode
         * @param {string} _boneName
         * @return {cc.AttachNode}
         */
        public getAttachNode (
            _boneName : string 
        ) : cc.AttachNode;

        /**
         * 
         * @method initWithFile
         * @param {string} _path
         * @return {boolean}
         */
        public initWithFile (
            _path : string 
        ) : boolean;

        /**
         * 
         * @method setBlendFunc
         * @param {cc.BlendFunc} _blendFunc
         */
        public setBlendFunc (
            _blendFunc : cc.BlendFunc 
        ) : void;

        /**
         * force set this Sprite3D to 2D render queue
         * @method setForce2DQueue
         * @param {boolean} _force2D
         */
        public setForce2DQueue (
            _force2D : boolean 
        ) : void;

        /**
         * generate default material
         * @method genMaterial
         */
        public genMaterial (
        ) : void;

        /**
         * remove attach node
         * @method removeAttachNode
         * @param {string} _boneName
         */
        public removeAttachNode (
            _boneName : string 
        ) : void;

        /**
         * 
         * @method getSkeleton
         * @return {cc.Skeleton3D}
         */
        public getSkeleton (
        ) : cc.Skeleton3D;

        /**
         * Force to write to depth buffer, this is useful if you want to achieve effects like fading.
         * @method setForceDepthWrite
         * @param {boolean} _value
         */
        public setForceDepthWrite (
            _value : boolean 
        ) : void;

        /**
         * get Mesh by Name, it returns the first one if there are more than one mesh with the same name 
         * @method getMeshByName
         * @param {string} _name
         * @return {cc.Mesh}
         */
        public getMeshByName (
            _name : string 
        ) : cc.Mesh;

        /**
         *  creates a Sprite3D
         * @method create
         * @param {(string)} _modelPath?
         * @param {(string)} _texturePath?
         * @return {cc.Sprite3D}
         */
        public static create (
            _modelPath? : (string), 
            _texturePath? : (string)
        ) : cc.Sprite3D;

        /**
         * 
         * @method Sprite3D
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class Sprite3DCache
     * @native
     */
    export abstract class Sprite3DCache 
    {

        /**
         * remove the SpriteData from Sprite3D by given the specified key
         * @method removeSprite3DData
         * @param {string} _key
         */
        public removeSprite3DData (
            _key : string 
        ) : void;

        /**
         * remove all the SpriteData from Sprite3D
         * @method removeAllSprite3DData
         */
        public removeAllSprite3DData (
        ) : void;

        /**
         * 
         * @method destroyInstance
         */
        public static destroyInstance (
        ) : void;

        /**
         * get & destroy
         * @method getInstance
         * @return {cc.Sprite3DCache}
         */
        public static getInstance (
        ) : cc.Sprite3DCache;

    }
    /**
     * @class Terrain
     * @native
     */
    export class Terrain 
        extends cc.Node
    {

        /**
         * initialize heightMap data 
         * @method initHeightMap
         * @param {string} _heightMap
         * @return {boolean}
         */
        public initHeightMap (
            _heightMap : string 
        ) : boolean;

        /**
         * set the MaxDetailAmount.
         * @method setMaxDetailMapAmount
         * @param {number} _maxValue
         */
        public setMaxDetailMapAmount (
            _maxValue : number 
        ) : void;

        /**
         * show the wireline instead of the surface,Debug Use only.<br>
         * -- Note only support desktop platform
         * @method setDrawWire
         * @param {boolean} _boolValue
         */
        public setDrawWire (
            _boolValue : boolean 
        ) : void;

        /**
         * set the Detail Map 
         * @method setDetailMap
         * @param {number} _index
         * @param {cc.Terrain::DetailMap} _detailMap
         */
        public setDetailMap (
            _index : number, 
            _detailMap : any 
        ) : void;

        /**
         * reset the heightmap data.
         * @method resetHeightMap
         * @param {string} _heightMap
         */
        public resetHeightMap (
            _heightMap : string 
        ) : void;

        /**
         * set directional light for the terrain<br>
         * -- param lightDir The direction of directional light, Note that lightDir is in the terrain's local space. Most of the time terrain is placed at (0,0,0) and without rotation, so lightDir is also in the world space.
         * @method setLightDir
         * @param {ctype.value_type<cc.Vec3>} _lightDir
         */
        public setLightDir (
            _lightDir : ctype.value_type<cc.Vec3> 
        ) : void;

        /**
         *  set the alpha map
         * @method setAlphaMap
         * @param {cc.Texture2D} _newAlphaMapTexture
         */
        public setAlphaMap (
            _newAlphaMapTexture : cc.Texture2D 
        ) : void;

        /**
         * set the skirt height ratio
         * @method setSkirtHeightRatio
         * @param {number} _ratio
         */
        public setSkirtHeightRatio (
            _ratio : number 
        ) : void;

        /**
         * Convert a world Space position (X,Z) to terrain space position (X,Z)
         * @method convertToTerrainSpace
         * @param {ctype.value_type<cc.Point>} _worldSpace
         * @return {ctype.value_type<cc.Point>}
         */
        public convertToTerrainSpace (
            _worldSpace : ctype.value_type<cc.Point> 
        ) : ctype.value_type<cc.Point>;

        /**
         * initialize alphaMap ,detailMaps textures
         * @method initTextures
         * @return {boolean}
         */
        public initTextures (
        ) : boolean;

        /**
         * initialize all Properties which terrain need 
         * @method initProperties
         * @return {boolean}
         */
        public initProperties (
        ) : boolean;

        /**
         * get specified position's height mapping to the terrain,use bi-linear interpolation method<br>
         * -- param pos the position (X,Z)<br>
         * -- param normal the specified position's normal vector in terrain . if this argument is NULL or nullptr,Normal calculation shall be skip.<br>
         * -- return the height value of the specified position of the terrain, if the (X,Z) position is out of the terrain bounds,it shall return 0;
         * @method getHeight
         * @param {(ctype.value_type<cc.Point>) | (number)} _pos | x
         * @param {(ctype.value_type<cc.Vec3>) | (number)} _Normal | z
         * @param {(ctype.value_type<cc.Vec3>)} _normal?
         * @return {number}
         */
        public getHeight (
            _pos_x : (ctype.value_type<cc.Point>) | (number), 
            _Normal_z : (ctype.value_type<cc.Vec3>) | (number), 
            _normal? : (ctype.value_type<cc.Vec3>)
        ) : number;

        /**
         * 
         * @method initWithTerrainData
         * @param {cc.Terrain::TerrainData} _parameter
         * @param {cc.Terrain::CrackFixedType} _fixedType
         * @return {boolean}
         */
        public initWithTerrainData (
            _parameter : any, 
            _fixedType : number 
        ) : boolean;

        /**
         * Set threshold distance of each LOD level,must equal or greater than the chunk size<br>
         * -- Note when invoke initHeightMap, the LOD distance will be automatic calculated.
         * @method setLODDistance
         * @param {number} _lod1
         * @param {number} _lod2
         * @param {number} _lod3
         */
        public setLODDistance (
            _lod1 : number, 
            _lod2 : number, 
            _lod3 : number 
        ) : void;

        /**
         * get the terrain's size
         * @method getTerrainSize
         * @return {ctype.value_type<cc.Size>}
         */
        public getTerrainSize (
        ) : ctype.value_type<cc.Size>;

        /**
         * Ray-Terrain intersection.<br>
         * -- param ray to hit the terrain<br>
         * -- param intersectionPoint hit point if hit<br>
         * -- return true if hit, false otherwise
         * @method getIntersectionPoint
         * @param {(cc.Ray)} _ray
         * @param {(ctype.value_type<cc.Vec3>)} _intersectionPoint?
         * @return {boolean | ctype.value_type<cc.Vec3>}
         */
        public getIntersectionPoint (
            _ray : (cc.Ray), 
            _intersectionPoint? : (ctype.value_type<cc.Vec3>)
        ) : boolean | ctype.value_type<cc.Vec3>;

        /**
         * get the normal of the specified position in terrain<br>
         * -- return the normal vector of the specified position of the terrain.<br>
         * -- note the fast normal calculation may not get precise normal vector.
         * @method getNormal
         * @param {number} _pixelX
         * @param {number} _pixelY
         * @return {ctype.value_type<cc.Vec3>}
         */
        public getNormal (
            _pixelX : number, 
            _pixelY : number 
        ) : ctype.value_type<cc.Vec3>;

        /**
         * 
         * @method reload
         */
        public reload (
        ) : void;

        /**
         * get height from the raw height filed
         * @method getImageHeight
         * @param {number} _pixelX
         * @param {number} _pixelY
         * @return {number}
         */
        public getImageHeight (
            _pixelX : number, 
            _pixelY : number 
        ) : number;

        /**
         *  set light map texture 
         * @method setLightMap
         * @param {string} _fileName
         */
        public setLightMap (
            _fileName : string 
        ) : void;

        /**
         * Switch frustum Culling Flag<br>
         * -- Note frustum culling will remarkable improve your terrain rendering performance. 
         * @method setIsEnableFrustumCull
         * @param {boolean} _boolValue
         */
        public setIsEnableFrustumCull (
            _boolValue : boolean 
        ) : void;

        /**
         * get the terrain's minimal height.
         * @method getMinHeight
         * @return {number}
         */
        public getMinHeight (
        ) : number;

        /**
         * get the terrain's maximal height.
         * @method getMaxHeight
         * @return {number}
         */
        public getMaxHeight (
        ) : number;

        /**
         * 
         * @method Terrain
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class Bundle3D
     * @native
     */
    export class Bundle3D 
    {

        /**
         * load a file. You must load a file first, then call loadMeshData, loadSkinData, and so on<br>
         * -- param path File to be loaded<br>
         * -- return result of load
         * @method load
         * @param {string} _path
         * @return {boolean}
         */
        public load (
            _path : string 
        ) : boolean;

        /**
         * load skin data from bundle<br>
         * -- param id The ID of the skin, load the first Skin in the bundle if it is empty
         * @method loadSkinData
         * @param {string} _id
         * @param {cc.SkinData} _skindata
         * @return {boolean}
         */
        public loadSkinData (
            _id : string, 
            _skindata : cc.SkinData 
        ) : boolean;

        /**
         * 
         * @method clear
         */
        public clear (
        ) : void;

        /**
         * 
         * @method loadMaterials
         * @param {cc.MaterialDatas} _materialdatas
         * @return {boolean}
         */
        public loadMaterials (
            _materialdatas : cc.MaterialDatas 
        ) : boolean;

        /**
         * 
         * @method loadMeshDatas
         * @param {cc.MeshDatas} _meshdatas
         * @return {boolean}
         */
        public loadMeshDatas (
            _meshdatas : cc.MeshDatas 
        ) : boolean;

        /**
         * 
         * @method loadNodes
         * @param {cc.NodeDatas} _nodedatas
         * @return {boolean}
         */
        public loadNodes (
            _nodedatas : cc.NodeDatas 
        ) : boolean;

        /**
         * load material data from bundle<br>
         * -- param id The ID of the animation, load the first animation in the bundle if it is empty
         * @method loadAnimationData
         * @param {string} _id
         * @param {cc.Animation3DData} _animationdata
         * @return {boolean}
         */
        public loadAnimationData (
            _id : string, 
            _animationdata : cc.Animation3DData 
        ) : boolean;

        /**
         * create a new bundle, destroy it when finish using it
         * @method createBundle
         * @return {cc.Bundle3D}
         */
        public static createBundle (
        ) : cc.Bundle3D;

        /**
         * 
         * @method destroyBundle
         * @param {cc.Bundle3D} _bundle
         */
        public static destroyBundle (
            _bundle : cc.Bundle3D 
        ) : void;

        /**
         * 
         * @method loadObj
         * @param {cc.MeshDatas} _meshdatas
         * @param {cc.MaterialDatas} _materialdatas
         * @param {cc.NodeDatas} _nodedatas
         * @param {string} _fullPath
         * @param {string} _mtl_basepath
         * @return {boolean}
         */
        public static loadObj (
            _meshdatas : cc.MeshDatas, 
            _materialdatas : cc.MaterialDatas, 
            _nodedatas : cc.NodeDatas, 
            _fullPath : string, 
            _mtl_basepath : string 
        ) : boolean;

        /**
         * 
         * @method Bundle3D
         * @constructor
         */
        public constructor (
        );

    }
}