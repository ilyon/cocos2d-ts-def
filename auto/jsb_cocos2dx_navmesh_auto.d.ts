/**
 * @module cocos2dx_navmesh
 */
declare namespace jsb {
    /**
     * @class NavMeshAgent
     * @native
     */
    export class NavMeshAgent 
        extends cc.Component
    {

        /**
         *  set maximal speed of agent 
         * @method setMaxSpeed
         * @param {number} _maxSpeed
         */
        public setMaxSpeed (
            _maxSpeed : number 
        ) : void;

        /**
         *  synchronize parameter to node. 
         * @method syncToNode
         */
        public syncToNode (
        ) : void;

        /**
         * Traverse OffMeshLink manually
         * @method completeOffMeshLink
         */
        public completeOffMeshLink (
        ) : void;

        /**
         *  get separation weight 
         * @method getSeparationWeight
         * @return {number}
         */
        public getSeparationWeight (
        ) : number;

        /**
         * Set automatic Traverse OffMeshLink 
         * @method setAutoTraverseOffMeshLink
         * @param {boolean} _isAuto
         */
        public setAutoTraverseOffMeshLink (
            _isAuto : boolean 
        ) : void;

        /**
         *  get current velocity 
         * @method getCurrentVelocity
         * @return {ctype.value_type<cc.Vec3>}
         */
        public getCurrentVelocity (
        ) : ctype.value_type<cc.Vec3>;

        /**
         *  synchronize parameter to agent. 
         * @method syncToAgent
         */
        public syncToAgent (
        ) : void;

        /**
         * Check agent arrived OffMeshLink 
         * @method isOnOffMeshLink
         * @return {boolean}
         */
        public isOnOffMeshLink (
        ) : boolean;

        /**
         *  set separation weight 
         * @method setSeparationWeight
         * @param {number} _weight
         */
        public setSeparationWeight (
            _weight : number 
        ) : void;

        /**
         *  pause movement 
         * @method pause
         */
        public pause (
        ) : void;

        /**
         * Set automatic Orientation 
         * @method setAutoOrientation
         * @param {boolean} _isAuto
         */
        public setAutoOrientation (
            _isAuto : boolean 
        ) : void;

        /**
         *  get agent height 
         * @method getHeight
         * @return {number}
         */
        public getHeight (
        ) : number;

        /**
         *  get maximal speed of agent 
         * @method getMaxSpeed
         * @return {number}
         */
        public getMaxSpeed (
        ) : number;

        /**
         * Get current OffMeshLink information
         * @method getCurrentOffMeshLinkData
         * @return {cc.OffMeshLinkData}
         */
        public getCurrentOffMeshLinkData (
        ) : cc.OffMeshLinkData;

        /**
         *  get agent radius 
         * @method getRadius
         * @return {number}
         */
        public getRadius (
        ) : number;

        /**
         * synchronization between node and agent is time consuming, you can skip some synchronization using this function
         * @method setSyncFlag
         * @param {cc.NavMeshAgent::NavMeshAgentSyncFlag} _flag
         */
        public setSyncFlag (
            _flag : number 
        ) : void;

        /**
         * 
         * @method getSyncFlag
         * @return {number}
         */
        public getSyncFlag (
        ) : number;

        /**
         *  resume movement 
         * @method resume
         */
        public resume (
        ) : void;

        /**
         *  stop movement 
         * @method stop
         */
        public stop (
        ) : void;

        /**
         *  set maximal acceleration of agent
         * @method setMaxAcceleration
         * @param {number} _maxAcceleration
         */
        public setMaxAcceleration (
            _maxAcceleration : number 
        ) : void;

        /**
         * Set the reference axes of agent's orientation<br>
         * -- param rotRefAxes The value of reference axes in local coordinate system.
         * @method setOrientationRefAxes
         * @param {ctype.value_type<cc.Vec3>} _rotRefAxes
         */
        public setOrientationRefAxes (
            _rotRefAxes : ctype.value_type<cc.Vec3> 
        ) : void;

        /**
         *  get maximal acceleration of agent
         * @method getMaxAcceleration
         * @return {number}
         */
        public getMaxAcceleration (
        ) : number;

        /**
         *  set agent height 
         * @method setHeight
         * @param {number} _height
         */
        public setHeight (
            _height : number 
        ) : void;

        /**
         *  get obstacle avoidance type 
         * @method getObstacleAvoidanceType
         * @return {number}
         */
        public getObstacleAvoidanceType (
        ) : number;

        /**
         *  get current velocity 
         * @method getVelocity
         * @return {ctype.value_type<cc.Vec3>}
         */
        public getVelocity (
        ) : ctype.value_type<cc.Vec3>;

        /**
         *  set agent radius 
         * @method setRadius
         * @param {number} _radius
         */
        public setRadius (
            _radius : number 
        ) : void;

        /**
         *  set obstacle avoidance type 
         * @method setObstacleAvoidanceType
         * @param {number} _type
         */
        public setObstacleAvoidanceType (
            _type : number 
        ) : void;

        /**
         * 
         * @method getNavMeshAgentComponentName
         * @return {string}
         */
        public static getNavMeshAgentComponentName (
        ) : string;

        /**
         * Create agent<br>
         * -- param param The parameters of agent.
         * @method create
         * @param {cc.NavMeshAgentParam} _param
         * @return {cc.NavMeshAgent}
         */
        public static create (
            _param : cc.NavMeshAgentParam 
        ) : cc.NavMeshAgent;

        /**
         * 
         * @method NavMeshAgent
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class NavMeshObstacle
     * @native
     */
    export class NavMeshObstacle 
        extends cc.Component
    {

        /**
         * 
         * @method getSyncFlag
         * @return {number}
         */
        public getSyncFlag (
        ) : number;

        /**
         * 
         * @method initWith
         * @param {number} _radius
         * @param {number} _height
         * @return {boolean}
         */
        public initWith (
            _radius : number, 
            _height : number 
        ) : boolean;

        /**
         *  synchronize parameter to obstacle. 
         * @method syncToObstacle
         */
        public syncToObstacle (
        ) : void;

        /**
         *  synchronize parameter to node. 
         * @method syncToNode
         */
        public syncToNode (
        ) : void;

        /**
         *  Get height of obstacle 
         * @method getHeight
         * @return {number}
         */
        public getHeight (
        ) : number;

        /**
         * synchronization between node and obstacle is time consuming, you can skip some synchronization using this function
         * @method setSyncFlag
         * @param {cc.NavMeshObstacle::NavMeshObstacleSyncFlag} _flag
         */
        public setSyncFlag (
            _flag : number 
        ) : void;

        /**
         *  Get radius of obstacle 
         * @method getRadius
         * @return {number}
         */
        public getRadius (
        ) : number;

        /**
         * Create obstacle, shape is cylinder<br>
         * -- param radius The radius of obstacle.<br>
         * -- param height The height of obstacle.
         * @method create
         * @param {number} _radius
         * @param {number} _height
         * @return {cc.NavMeshObstacle}
         */
        public static create (
            _radius : number, 
            _height : number 
        ) : cc.NavMeshObstacle;

        /**
         * 
         * @method getNavMeshObstacleComponentName
         * @return {string}
         */
        public static getNavMeshObstacleComponentName (
        ) : string;

        /**
         * 
         * @method NavMeshObstacle
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class NavMesh
     * @native
     */
    export class NavMesh 
    {

        /**
         *  remove a obstacle from navmesh. 
         * @method removeNavMeshObstacle
         * @param {cc.NavMeshObstacle} _obstacle
         */
        public removeNavMeshObstacle (
            _obstacle : cc.NavMeshObstacle 
        ) : void;

        /**
         *  remove a agent from navmesh. 
         * @method removeNavMeshAgent
         * @param {cc.NavMeshAgent} _agent
         */
        public removeNavMeshAgent (
            _agent : cc.NavMeshAgent 
        ) : void;

        /**
         *  update navmesh. 
         * @method update
         * @param {number} _dt
         */
        public update (
            _dt : number 
        ) : void;

        /**
         *  Check enabled debug draw. 
         * @method isDebugDrawEnabled
         * @return {boolean}
         */
        public isDebugDrawEnabled (
        ) : boolean;

        /**
         *  add a agent to navmesh. 
         * @method addNavMeshAgent
         * @param {cc.NavMeshAgent} _agent
         */
        public addNavMeshAgent (
            _agent : cc.NavMeshAgent 
        ) : void;

        /**
         *  add a obstacle to navmesh. 
         * @method addNavMeshObstacle
         * @param {cc.NavMeshObstacle} _obstacle
         */
        public addNavMeshObstacle (
            _obstacle : cc.NavMeshObstacle 
        ) : void;

        /**
         *  Enable debug draw or disable. 
         * @method setDebugDrawEnable
         * @param {boolean} _enable
         */
        public setDebugDrawEnable (
            _enable : boolean 
        ) : void;

        /**
         *  Internal method, the updater of debug drawing, need called each frame. 
         * @method debugDraw
         * @param {cc.Renderer} _renderer
         */
        public debugDraw (
            _renderer : any 
        ) : void;

        /**
         * Create navmesh<br>
         * -- param navFilePath The NavMesh File path.<br>
         * -- param geomFilePath The geometry File Path,include offmesh information,etc.
         * @method create
         * @param {string} _navFilePath
         * @param {string} _geomFilePath
         * @return {cc.NavMesh}
         */
        public static create (
            _navFilePath : string, 
            _geomFilePath : string 
        ) : cc.NavMesh;

        /**
         * 
         * @method NavMesh
         * @constructor
         */
        public constructor (
        );

    }
}