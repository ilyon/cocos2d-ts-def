
import sys
import os
import re
import json


prototype_pattern = re.compile(r'_proto\s?=\s?(\w+).(\w+).prototype;')
getter_setter_pattrern = re.compile(r'cc.defineGetterSetter\(_proto,\s?\"(\w+)\",\s?_proto.(\w+),\s?_proto.(\w+)\);')
getter_pattern = re.compile(r'cc.defineGetterSetter\(_proto,\s?\"(\w+)\",\s?_proto.(\w+)\);')

function_return_type_regex = re.compile(r'\) : (.*);')

static_property_number_definitions_pattern = re.compile(r'^(\w+).(\w+).(\w+)\s?=\s?([0-9]+);')

all_additions = { }
static_additions = { }

def search_class(content, namespace_name, class_name):
    lines = content.split('\n')
    
    SEARCH_NAMESPACE    = 0
    SEARCH_CLASS        = 1
    
    last_comment = ""
    state = SEARCH_NAMESPACE
    for line in lines:
        if state == SEARCH_NAMESPACE:
            if line.find("declare namespace {0}".format(namespace_name)) != -1:
                state = SEARCH_CLASS
            
        elif state == SEARCH_CLASS:
            if line.find(" class {0} ".format(class_name)) != -1:
                return True
        
    return False        
        

def parse_return_type(content, namespace_name, class_name, getter):
    lines = content.split('\n')
    
    SEARCH_NAMESPACE    = 0
    SEARCH_CLASS        = 1
    SEARCH_CLASS_START  = 2
    SEARCH_FUNCTION     = 3
    SEARCH_RETURN_TYPE  = 4
    
    last_comment = ""
    state = SEARCH_NAMESPACE
    for line in lines:
        if state == SEARCH_NAMESPACE:
            if line.find("declare namespace {0}".format(namespace_name)) != -1:
                state = SEARCH_CLASS
            
        elif state == SEARCH_CLASS:
            if line.find(" class {0} ".format(class_name)) != -1:
                state = SEARCH_CLASS_START
        
        elif state == SEARCH_CLASS_START:
            if line.find(r"{") != -1:
                state = SEARCH_FUNCTION
            
        elif state == SEARCH_FUNCTION:
            if line.find("/**") != -1:
                last_comment = line
            elif line.find("*") != -1:
                last_comment += "\n" + line
            elif line.find(r"}") != -1:
                return (" "*8 + "/* Can't find native property {0} implementation of {1}::{2}. using any type */".format(getter, namespace_name, class_name), "any")
                
            elif line.find(" {0} (".format(getter)) != -1:
                state = SEARCH_RETURN_TYPE
                
            
        elif state == SEARCH_RETURN_TYPE:
            return_type = function_return_type_regex.search(line)
            if len(line) == 0:
                state = SEARCH_FUNCTION
                
            elif return_type is not None:
                return (last_comment, return_type.group(1))
    
    return None
    

def parse_file_props(file):
    global all_additions
    
    f = open(file)
    lines = f.readlines()
    f.close()
    
    namespace = None
    class_name = None
    for line in lines:
        s = static_property_number_definitions_pattern.search(line)
        if s is not None:
            s_namespace = s.group(1)
            s_class_name = s.group(2)
            s_prop_name = s.group(3)
            s_value = s.group(4)
            print s_namespace, s_class_name, s_prop_name, s_value
            if not s_namespace in static_additions:
                static_additions[s_namespace] = {}
                
            if not s_class_name in static_additions[s_namespace]:
                static_additions[s_namespace][s_class_name] = []

            static_additions[s_namespace][s_class_name].append({
                "prop_name" : s_prop_name,
                "type" : "number"
            })

            continue

        m = prototype_pattern.search(line)
        if m is not None:
            namespace = m.group(1)
            class_name = m.group(2)
            
            if not namespace in all_additions:
                all_additions[namespace] = {}
                
            if not class_name in all_additions[namespace]:
                all_additions[namespace][class_name] = []
                
            continue
            
        gs = getter_setter_pattrern.search(line)
        if gs is not None:
            prop_name = gs.group(1)
            getter_name = gs.group(2)
            setter_name = gs.group(3)
            
            if namespace is not None:
                all_additions[namespace][class_name].append({
                    "readonly"  : False,
                    "prop_name" : prop_name,
                    "declaration_name" : getter_name
                })
            
            continue
            
        g = getter_pattern.search(line)
        if g is not None:
            prop_name = g.group(1)
            getter_name = g.group(2)
            
            if namespace is not None:
                all_additions[namespace][class_name].append({
                    "readonly"  : True,
                    "prop_name" : prop_name,
                    "declaration_name" : getter_name
                })
            
            continue
    
        
        
def walk_dir(dir, func):
    for root, subdirs, files in os.walk(dir):
        for file in files:
            func(os.path.join(root, file))

            

current_path = os.path.abspath(os.path.join(os.path.dirname(__file__)))            
if __name__ == '__main__':

    if len(sys.argv) != 2:
        print "cocos2d-x js script dir or file not specified"
        print "Example: python cocos2d_script_define_props_auto.py ../cocos2d-x/cocos/scripting/js-bindings/script/\n"
        sys.exit(1)
    
    root = sys.argv[1]
    if os.path.isfile(root):
        parse_file_props(root)
        
    elif os.path.isdir(root):
        walk_dir(root, parse_file_props)
        
    files_and_sources = { }
    def add_file_sources(file):
        relative_path = '/'.join(os.path.relpath(file, current_path).split(os.path.os.path.sep))
        f = open(file)
        files_and_sources[relative_path] = f.read()
        f.close()
    
    walk_dir(os.path.join(current_path, 'auto'), add_file_sources)
    
    content_mappings = { }
    for path in files_and_sources:
        content_mappings[path] = { }
    
    for namespace in static_additions:
        for class_name in static_additions[namespace]:
            should_add_props = False
            path_to_add = None

            for path in files_and_sources:
                if search_class(files_and_sources[path], namespace, class_name) == True:
                    should_add_props = True
                    path_to_add = path
                    break

            if should_add_props == False:
                continue

            class_content = content_mappings[path_to_add].get(class_name, "")
            
            for prop_def in static_additions[namespace][class_name]:
                prop_name = prop_def.get("prop_name")
                s_type = prop_def.get("type")
                class_content += (" "*8 + 'public static readonly {0}: {1};\n'.format(prop_name, s_type))

            content_mappings[path_to_add][class_name] = class_content


    for namespace in all_additions:
        for class_name in all_additions[namespace]:
            for prop_def in all_additions[namespace][class_name]:
                prop_name = prop_def.get("declaration_name")
                readonly = prop_def.get("readonly")
                getter_name = prop_def.get("prop_name")
                
                for path in files_and_sources:
                    ret = parse_return_type(files_and_sources[path], namespace, class_name, prop_name)
                    if ret is None:
                        continue
                    
                    commment, rettype = ret
                    
                    class_content = content_mappings[path].get(class_name, "")
                    field_declaration = " "*8 + "public{0}{1} : {2};\n".format(" readonly " if readonly else " ", getter_name, rettype)
                    class_content += '\n'.join([commment, field_declaration])
                    content_mappings[path][class_name] = class_content


    replacers = []
    for file_name in content_mappings:
        for class_name in content_mappings[file_name]:
            outfile_path = os.path.join(current_path, "auto_props_api", class_name + ".txt")
            f = open(outfile_path, 'w')
            f.write(content_mappings[file_name][class_name])
            f.close()
            
            replacers.append({
                "source"    : file_name,
                "class"     : class_name,
                "to"        : '/'.join(os.path.relpath(outfile_path, current_path).split(os.path.os.path.sep))
            })
    
    auto_props_api_json = { "replacers" : replacers }
    
    with open("auto_props_api.json", 'w') as f:
        json.dump(auto_props_api_json, f, indent=4)
    f.close()
    
    