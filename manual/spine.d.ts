
declare namespace sp {
    export enum ANIMATION_EVENT_TYPE {
        START,
        END,
        COMPLETE,
        EVENT
    }
}