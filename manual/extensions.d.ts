declare namespace cc {
    export interface ScrollViewDelegate {
        scrollViewDidScroll?(view: cc.ScrollView): void;
        scrollViewDidZoom?(view: cc.ScrollView): void;
    }

    export interface TableViewDelegate implements ScrollViewDelegate {
        /**
         * Delegate to respond touch event
         *
         * @param {cc.TableView} table table contains the given cell
         * @param {cc.TableViewCell} cell  cell that is touched
         */
        tableCellTouched?(table: cc.TableView, cell: cc.TableViewCell): void;

        /**
         * Delegate to respond a table cell press event.
         *
         * @param {cc.TableView} table table contains the given cell
         * @param {cc.TableViewCell} cell  cell that is pressed
         */
        tableCellHighlight?(table: cc.TableView, cell: cc.TableViewCell): void;

        /**
         * Delegate to respond a table cell release event
         *
         * @param {cc.TableView} table table contains the given cell
         * @param {cc.TableViewCell} cell  cell that is pressed
         */
        tableCellUnhighlight?(table: cc.TableView, cell: cc.TableViewCell): void;

        /**
         * <p>
         * Delegate called when the cell is about to be recycled. Immediately                     <br/>
         * after this call the cell will be removed from the scene graph and                      <br/>
         * recycled.
         * </p>
         * @param table table contains the given cell
         * @param cell  cell that is pressed
         */
        tableCellWillRecycle?(table: cc.TableView, cell: cc.TableViewCell): void;
    }

    export interface TableViewDataSource {
        /**
         * cell size for a given index
         * @param {cc.TableView} table table to hold the instances of Class
         * @param {Number} idx the index of a cell to get a size
         * @return {ctype.value_type<cc.Size>} size of a cell at given index
         */
        tableCellSizeForIndex?(table: cc.TableView, idx: number): ctype.value_type<cc.Size>;

        /**
         * a cell instance at a given index
         * @param {cc.TableView} table table to hold the instances of Class
         * @param idx index to search for a cell
         * @return {cc.TableViewCell} cell found at idx
         */
        tableCellAtIndex?(table: cc.TableView, idx: number): cc.TableViewCell;

        /**
         * Returns number of cells in a given table view.
         * @param {cc.TableView} table table to hold the instances of Class
         * @return {Number} number of cells
         */
        numberOfCellsInTableView?(table: cc.TableView): number;
    }
}