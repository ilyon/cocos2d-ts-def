
/**
 * The namespace for jsb exclusive APIs, all APIs in this namespace should never be used in Web engine.
 * So please check whether the running environment is native or not before any usage.
 * @namespace
 * @name jsb
 * @example
 *
 * if(cc.sys.isNative) {
 *     cc.log(cc.fileUtils.fullPathForFilename("test.js"));
 * }
 */
declare namespace jsb {

    export namespace AudioEngine {
        export const INVALID_AUDIO_ID: number;
        export const TIME_UNKNOWN: number;

        export enum AudioState {
            ERROR,
            INITIALZING,
            PLAYING,
            PAUSED
        }
    }

    export interface EventCodeMap {
        BEGAN:number;
        MOVED:number;
        ENDED:number;
        CANCELLED:number
    }
    
    // TODO: This is probably a bad idea to declare these as enums (since they clearly are not TS enums), but let's try it out and at least see if the values resolve properly
    export enum DiffType {
        ADDED,
        DELETED,
        MODIFIED
    }

    export enum DownloadState {
        UNSTARTED,
        DOWNLOADING,
        SUCCESSED
    }

    export namespace EventAssetsManager {
        export const ERROR_NO_LOCAL_MANIFEST: number;
        export const ERROR_DOWNLOAD_MANIFEST: number;
        export const ERROR_PARSE_MANIFEST: number;
        export const NEW_VERSION_FOUND: number;
        export const ALREADY_UP_TO_DATE: number;
        export const UPDATE_PROGRESSION: number;
        export const ASSET_UPDATED: number;
        export const ERROR_UPDATING: number;
        export const UPDATE_FINISHED: number;
        export const UPDATE_FAILED: number;
        export const ERROR_DECOMPRESS: number;
    }

    export enum ErrorCode {
        CREATE_FILE,
        NETWORK,
        NO_NEW_VERSION,
        UNCOMPRESS
    }

    export enum State {
        UNCHECKED,
        PREDOWNLOAD_VERSION,
        DOWNLOADING_VERSION,
        VERSION_LOADED,
        PREDOWNLOAD_MANIFEST,
        DOWNLOADING_MANIFEST,
        MANIFEST_LOADED,
        NEED_UPDATE,
        UPDATING,
        UP_TO_DATE,
        FAIL_TO_UPDATE
    }



    /**
     * ATTENTION: USE jsb.fileUtils INSTEAD OF jsb.FileUtils.
     * jsb.fileUtils is the native file utils' singleton object,
     * please refer to Cocos2d-x's API to know how to use it.
     * Only available in JSB
     * @class
     * @name jsb.fileUtils
     * @extend cc.Class
     */
    //jsb.fileUtils = /** @lends jsb.fileUtils# */{
    export const fileUtils:cc.FileUtils;

    // TODO: I don't know the best way to represent this, because I can't find a reference in the C++ docs. Just do this for now, fix it later on.
    /**
     * jsb.reflection is a bridge to let you invoke Java static functions.
     * please refer to this document to know how to use it: http://www.cocos2d-x.org/docs/manual/framework/html5/v3/reflection/en
     * Only available on iOS/Mac/Android platform
     * @class
     * @name jsb.reflection
     */
    export namespace reflection {
        /**
         * @function
         */
        export function callStaticMethod(...args : string[]):any;
    }
}