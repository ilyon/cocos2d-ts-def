declare type CallFuncCallback = (targetOrData?:any, data?:any)=>any;

declare namespace cc {
    // +--------------------------------------------------------------------------------
    // + File: cocos2d/core/base-nodes/CCAction.js
    // +--------------------------------------------------------------------------------

    /** Default Action tag
     * @constant
     * @type {Number}
     * @default
     */
    export const ACTION_TAG_INVALID:number;

    /**
     * Allocates and initializes the action.
     *
     * @function cc.action
     * @static
     * @return {cc.Action}
     *
     * @example
     * // return {cc.Action}
     * var action = cc.action();
     */
    export function action():Action;

    /**
     * creates the speed action.
     *
     * @function cc.speed
     * @param {cc.ActionInterval} action
     * @param {Number} speed
     * @return {cc.Speed}
     */
    export function speed(action:ActionInterval, speed:number):Speed;

    /**
     * creates the action with a set boundary. <br/>
     * creates the action with no boundary set.
     *
     * @function
     * @param {cc.Node} followedNode
     * @param {ctype.value_type<cc.Rect>} rect
     * @return {cc.Follow|Null} returns the cc.Follow object on success
     * @example
     * // example
     * // creates the action with a set boundary
     * var sprite = new cc.Sprite("spriteFileName");
     * var followAction = cc.follow(sprite, cc.rect(0, 0, s.width * 2 - 100, s.height));
     * this.runAction(followAction);
     *
     * // creates the action with no boundary set
     * var sprite = new cc.Sprite("spriteFileName");
     * var followAction = cc.follow(sprite);
     * this.runAction(followAction);
     */
    export function follow(followedNode:Node, rect:ctype.value_type<Rect>):Follow;

    // +--------------------------------------------------------------------------------
    // + File: cocos2d/core/base-nodes/CCActionCatmullRom.js
    // +--------------------------------------------------------------------------------
    /**
     * Returns the Cardinal Spline position for a given set of control points, tension and time. <br />
     * CatmullRom Spline formula. <br />
     * s(-ttt + 2tt - t)P1 + s(-ttt + tt)P2 + (2ttt - 3tt + 1)P2 + s(ttt - 2tt + t)P3 + (-2ttt + 3tt)P3 + s(ttt - tt)P4
     *
     * @function
     * @param {ctype.value_type<cc.Point>} p0
     * @param {ctype.value_type<cc.Point>} p1
     * @param {ctype.value_type<cc.Point>} p2
     * @param {ctype.value_type<cc.Point>} p3
     * @param {Number} tension
     * @param {Number} t
     * @return {ctype.value_type<cc.Point>}
     */
    export function cardinalSplineAt(p0:ctype.value_type<cc.Point>,
                                     p1:ctype.value_type<cc.Point>,
                                     p2:ctype.value_type<cc.Point>,
                                     p3:ctype.value_type<cc.Point>,
                                     tension:number,
                                     t:number):ctype.value_type<cc.Point>;

    /**
     * returns a new copy of the array reversed.
     *
     * @return {Array}
     */
    export function reverseControlPoints (controlPoints:ctype.value_type<cc.Point>[]):ctype.value_type<cc.Point>[];


    /**
     * returns a new clone of the controlPoints
     *
     * @param controlPoints
     * @returns {Array}
     */
    export function cloneControlPoints (controlPoints:ctype.value_type<cc.Point>[]):ctype.value_type<cc.Point>[];

    /**
     * returns a point from the array
     *
     * @param {Array} controlPoints
     * @param {Number} pos
     * @return {ctype.value_type<cc.Point>}
     */
    export function getControlPointAt(controlPoints:ctype.value_type<cc.Point>[], pos:number):ctype.value_type<cc.Point>;

    /**
     * reverse the current control point array inline, without generating a new one <br />
     *
     * @param controlPoints
     */
    export function reverseControlPointsInline(controlPoints:ctype.value_type<cc.Point>[]):void;

    /**
     * creates an action with a Cardinal Spline array of points and tension.
     *
     * @function
     * @param {Number} duration
     * @param {Array} points array of control points
     * @param {Number} tension
     * @return {cc.CardinalSplineTo}
     *
     * @example
     * //create a cc.CardinalSplineTo
     * var action1 = cc.cardinalSplineTo(3, array, 0);
     */
    export function cardinalSplineTo(duration:number, points:ctype.value_type<cc.Point>[], tension:number):CardinalSplineTo;

    /**
     * creates an action with a Cardinal Spline array of points and tension.
     *
     * @function
     * @param {Number} duration
     * @param {Array} points
     * @param {Number} tension
     *
     * @return {cc.CardinalSplineBy}
     */
    export function cardinalSplineBy(duration:number, points:ctype.value_type<cc.Point>[], tension:number):CardinalSplineBy;

    /**
     * creates an action with a Cardinal Spline array of points and tension.
     *
     * @function
     * @param {Number} dt
     * @param {Array} points
     * @param {number} [tension] Ignore, only here to suppress TypeScript compiler error for overloading method.
     * @return {cc.CatmullRomTo}
     *
     * @example
     * var action1 = cc.catmullRomTo(3, array);
     */
    export function catmullRomTo(dt:number, points:ctype.value_type<cc.Point>[], tension?:number):CatmullRomTo;

    /**
     * Creates an action with a Cardinal Spline array of points and tension
     * @function
     * @param {Number} dt
     * @param {Array} points
     * @return {cc.CatmullRomBy}
     * @example
     * var action1 = cc.catmullRomBy(3, array);
     */
    export function catmullRomBy(dt:number, points:ctype.value_type<cc.Point>[]):CatmullRomBy;

    /**
     * creates the action of ActionEase
     *
     * @param {cc.ActionInterval} action
     * @return {cc.ActionEase}
     * @example
     * // example
     * var moveEase = cc.actionEase(action);
     */
    export function actionEase(action:ActionInterval):ActionEase;

    /**
     * Creates the action with the inner action and the rate parameter.
     *
     * @param {cc.ActionInterval} action
     * @param {Number} rate
     * @return {cc.EaseRateAction}
     * @example
     * // example
     * var moveEaseRateAction = cc.easeRateAction(action, 3.0);
     */
    export function easeRateAction(action:ActionInterval, rate:number):EaseRateAction;

    /**
     * Creates the action easing object with the rate parameter. <br />
     * From slow to fast.
     *
     * @function
     * @param {Number} rate
     * @return {Object}
     * @example
     * // example
     * action.easing(cc.easeIn(3.0));
     */
    export function easeIn(rate:number):EaseIn;

    /**
     * Creates the action easing object with the rate parameter. <br />
     * From fast to slow.
     *
     * @function
     * @param {Number} rate
     * @return {Object}
     * @example
     * // example
     * action.easing(cc.easeOut(3.0));
     */
    export function easeOut(rate:number):EaseOut;

    /**
     * Creates the action easing object with the rate parameter. <br />
     * Slow to fast then to slow.
     * @function
     * @param {Number} rate
     * @return {Object}
     *
     * @example
     * //The new usage
     * action.easing(cc.easeInOut(3.0));
     */
    export function easeInOut(rate:number):EaseInOut;

    //// TODO: What's this for? Does it alter the inteface?
    //cc._easeExponentialInObj = {
    //    easing: function(dt){
    //        return dt === 0 ? 0 : Math.pow(2, 10 * (dt - 1));
    //    },
    //    reverse: function(){
    //        return cc._easeExponentialOutObj;
    //    }
    //};

    /**
     * Creates the action easing object with the rate parameter. <br />
     * Reference easeInExpo: <br />
     * {@link http://www.zhihu.com/question/21981571/answer/19925418}
     * @function
     * @return {Object}
     * @example
     * // example
     * action.easing(cc.easeExponentialIn());
     */
    export function easeExponentialIn():EaseExponentialIn;

    //cc._easeExponentialOutObj = {
    //    easing: function(dt){
    //        return dt === 1 ? 1 : (-(Math.pow(2, -10 * dt)) + 1);
    //    },
    //    reverse: function(){
    //        return cc._easeExponentialInObj;
    //    }
    //};

    /**
     * creates the action easing object. <br />
     * Reference easeOutExpo: <br />
     * {@link http://www.zhihu.com/question/21981571/answer/19925418}
     *
     * @return {Object}
     * @example
     * // example
     * action.easing(cc.easeExponentialOut());
     */
    export function easeExponentialOut():EaseExponentialOut;

    //cc._easeExponentialInOutObj = {
    //    easing: function(dt){
    //        if( dt !== 1 && dt !== 0) {
    //            dt *= 2;
    //            if (dt < 1)
    //                return 0.5 * Math.pow(2, 10 * (dt - 1));
    //            else
    //                return 0.5 * (-Math.pow(2, -10 * (dt - 1)) + 2);
    //        }
    //        return dt;
    //    },
    //    reverse: function(){
    //        return cc._easeExponentialInOutObj;
    //    }
    //};

    /**
     * creates an EaseExponentialInOut action easing object. <br />
     * Reference easeInOutExpo: <br />
     * {@link http://www.zhihu.com/question/21981571/answer/19925418}
     * @function
     * @return {Object}
     * @example
     * // example
     * action.easing(cc.easeExponentialInOut());
     */
    export function easeExponentialInOut():EaseExponentialInOut;

    //cc._easeSineInObj = {
    //    easing: function(dt){
    //        return (dt===0 || dt===1) ? dt : -1 * Math.cos(dt * Math.PI / 2) + 1;
    //    },
    //    reverse: function(){
    //        return cc._easeSineOutObj;
    //    }
    //};

    /**
     * creates an EaseSineIn action. <br />
     * Reference easeInSine: <br />
     * {@link http://www.zhihu.com/question/21981571/answer/19925418}
     * @function
     * @return {Object}
     * @example
     * // example
     * action.easing(cc.easeSineIn());
     */
    export function easeSineIn():EaseSineIn;

    //cc._easeSineOutObj = {
    //    easing: function(dt){
    //        return (dt===0 || dt===1) ? dt : Math.sin(dt * Math.PI / 2);
    //    },
    //    reverse: function(){
    //        return cc._easeSineInObj;
    //    }
    //};

    /**
     * Creates an EaseSineOut action easing object. <br />
     * Reference easeOutSine: <br />
     * {@link http://www.zhihu.com/question/21981571/answer/19925418}
     * @function
     * @return {Object}
     * @example
     * // example
     * action.easing(cc.easeSineOut());
     */
    export function easeSineOut():EaseSineOut;

    //cc._easeSineInOutObj = {
    //    easing: function(dt){
    //        return (dt === 0 || dt === 1) ? dt : -0.5 * (Math.cos(Math.PI * dt) - 1);
    //    },
    //    reverse: function(){
    //        return cc._easeSineInOutObj;
    //    }
    //};

    /**
     * creates the action easing object. <br />
     * Reference easeInOutSine: <br />
     * {@link http://www.zhihu.com/question/21981571/answer/19925418}
     * @return {Object}
     * @example
     * // example
     * action.easing(cc.easeSineInOut());
     */
    export function easeSineInOut():EaseSineInOut;

////default ease elastic in object (period = 0.3)
//    cc._easeElasticInObj = {
//        easing:function(dt){
//            if (dt === 0 || dt === 1)
//                return dt;
//            dt = dt - 1;
//            return -Math.pow(2, 10 * dt) * Math.sin((dt - (0.3 / 4)) * Math.PI * 2 / 0.3);
//        },
//        reverse:function(){
//            return cc._easeElasticOutObj;
//        }
//    };

    /**
     * Creates the action easing obejct with the period in radians (default is 0.3). <br />
     * Reference easeInElastic: <br />
     * {@link http://www.zhihu.com/question/21981571/answer/19925418}
     * @function
     * @param {Number} [period=0.3]
     * @return {Object}
     * @example
     * // example
     * action.easing(cc.easeElasticIn(3.0));
     */
    export function easeElasticIn(period?:number):EaseElasticIn;

////default ease elastic out object (period = 0.3)
//    cc._easeElasticOutObj = {
//        easing: function (dt) {
//            return (dt === 0 || dt === 1) ? dt : Math.pow(2, -10 * dt) * Math.sin((dt - (0.3 / 4)) * Math.PI * 2 / 0.3) + 1;
//        },
//        reverse:function(){
//            return cc._easeElasticInObj;
//        }
//    };
    /**
     * Creates the action easing object with the period in radians (default is 0.3). <br />
     * Reference easeOutElastic: <br />
     * {@link http://www.zhihu.com/question/21981571/answer/19925418}
     * @function
     * @param {Number} [period=0.3]
     * @return {Object}
     * @example
     * // example
     * action.easing(cc.easeElasticOut(3.0));
     */
    export function easeElasticOut(period?:number):EaseElasticOut;

    /**
     * Creates the action easing object with the period in radians (default is 0.3). <br />
     * Reference easeInOutElastic: <br />
     * {@link http://www.zhihu.com/question/21981571/answer/19925418}
     * @function
     * @param {Number} [period=0.3]
     * @return {Object}
     * @example
     * // example
     * action.easing(cc.easeElasticInOut(3.0));
     */
    export function easeElasticInOut(period?:number):EaseElasticInOut;

    //cc._easeBounceInObj = {
    //    easing: function(dt){
    //        return 1 - cc._bounceTime(1 - dt);
    //    },
    //    reverse: function(){
    //        return cc._easeBounceOutObj;
    //    }
    //};

    /**
     * Creates the action easing object. <br />
     * Eased bounce effect at the beginning.
     * @function
     * @return {Object}
     * @example
     * // example
     * action.easing(cc.easeBounceIn());
     */
    export function easeBounceIn():EaseBounceIn;

    //cc._easeBounceOutObj = {
    //    easing: function(dt){
    //        return cc._bounceTime(dt);
    //    },
    //    reverse:function () {
    //        return cc._easeBounceInObj;
    //    }
    //};

    /**
     * Creates the action easing object. <br />
     * Eased bounce effect at the ending.
     * @function
     * @return {Object}
     * @example
     * // example
     * action.easing(cc.easeBounceOut());
     */
    export function easeBounceOut():EaseBounceOut;

    //cc._easeBounceInOutObj = {
    //    easing: function (time1) {
    //        var newT;
    //        if (time1 < 0.5) {
    //            time1 = time1 * 2;
    //            newT = (1 - cc._bounceTime(1 - time1)) * 0.5;
    //        } else {
    //            newT = cc._bounceTime(time1 * 2 - 1) * 0.5 + 0.5;
    //        }
    //        return newT;
    //    },
    //    reverse: function(){
    //        return cc._easeBounceInOutObj;
    //    }
    //};

    /**
     * Creates the action easing object. <br />
     * Eased bounce effect at the begining and ending.
     * @function
     * @return {Object}
     * @example
     * // example
     * action.easing(cc.easeBounceInOut());
     */
    export function easeBounceInOut():EaseBounceInOut;

    //cc._easeBackInObj = {
    //    easing: function (time1) {
    //        var overshoot = 1.70158;
    //        return (time1===0 || time1===1) ? time1 : time1 * time1 * ((overshoot + 1) * time1 - overshoot);
    //    },
    //    reverse: function(){
    //        return cc._easeBackOutObj;
    //    }
    //};

    /**
     * Creates the action easing object. <br />
     * In the opposite direction to move slowly, and then accelerated to the right direction.
     * @function
     * @return {Object}
     * @example
     * // example
     * action.easing(cc.easeBackIn());
     */
    export function easeBackIn():EaseBackIn;

    //cc._easeBackOutObj = {
    //    easing: function (time1) {
    //        var overshoot = 1.70158;
    //        time1 = time1 - 1;
    //        return time1 * time1 * ((overshoot + 1) * time1 + overshoot) + 1;
    //    },
    //    reverse: function(){
    //        return cc._easeBackInObj;
    //    }
    //};

    /**
     * Creates the action easing object. <br />
     * Fast moving more than the finish, and then slowly back to the finish.
     * @function
     * @return {Object}
     * @example
     * // example
     * action.easing(cc.easeBackOut());
     */
    export function easeBackOut():EaseBackOut;

    //cc._easeBackInOutObj = {
    //    easing: function (time1) {
    //        var overshoot = 1.70158 * 1.525;
    //        time1 = time1 * 2;
    //        if (time1 < 1) {
    //            return (time1 * time1 * ((overshoot + 1) * time1 - overshoot)) / 2;
    //        } else {
    //            time1 = time1 - 2;
    //            return (time1 * time1 * ((overshoot + 1) * time1 + overshoot)) / 2 + 1;
    //        }
    //    },
    //    reverse: function(){
    //        return cc._easeBackInOutObj;
    //    }
    //};

    /**
     * Creates the action easing object. <br />
     * Begining of cc.EaseBackIn. Ending of cc.EaseBackOut.
     * @function
     * @return {Object}
     * @example
     * // example
     * action.easing(cc.easeBackInOut());
     */
    export function easeBackInOut():EaseBackInOut;

    /**
     * Creates the action easing object. <br />
     * Into the 4 reference point. <br />
     * To calculate the motion curve.
     * @param {Number} p0 The first bezier parameter
     * @param {Number} p1 The second bezier parameter
     * @param {Number} p2 The third bezier parameter
     * @param {Number} p3 The fourth bezier parameter
     * @returns {Object}
     * @example
     * // example
     * action.easing(cc.easeBezierAction(0.5, 0.5, 1.0, 1.0));
     */
    export function easeBezierAction(p0:number, p1:number, p2:number, p3:number):EaseBezierAction;

    //cc._easeQuadraticActionIn = {
    //    easing: cc.EaseQuadraticActionIn.prototype._updateTime,
    //    reverse: function(){
    //        return cc._easeQuadraticActionIn;
    //    }
    //};

    /**
     * Creates the action easing object. <br />
     * Reference easeInQuad: <br />
     * {@link http://www.zhihu.com/question/21981571/answer/19925418}
     * @returns {Object}
     * @example
     * //example
     * action.easing(cc.easeQuadraticActionIn());
     */
    export function easeQuadraticActionIn():EaseQuadraticActionIn;

    //cc._easeQuadraticActionOut = {
    //    easing: cc.EaseQuadraticActionOut.prototype._updateTime,
    //    reverse: function(){
    //        return cc._easeQuadraticActionOut;
    //    }
    //};

    /**
     * Creates the action easing object. <br />
     * Reference easeOutQuad: <br />
     * {@link http://www.zhihu.com/question/21981571/answer/19925418}
     * @function
     * @returns {Object}
     * @example
     * //example
     * action.easing(cc.easeQuadraticActionOut());
     */
    export function easeQuadraticActionOut():EaseQuadraticActionOut;

    //cc._easeQuadraticActionInOut = {
    //    easing: cc.EaseQuadraticActionInOut.prototype._updateTime,
    //    reverse: function(){
    //        return cc._easeQuadraticActionInOut;
    //    }
    //};

    /**
     * Creates the action easing object. <br />
     * Reference easeInOutQuad: <br />
     * {@link http://www.zhihu.com/question/21981571/answer/19925418}
     * @function
     * @returns {Object}
     * @example
     * //example
     * action.easing(cc.easeQuadraticActionInOut());
     */
    export function easeQuadraticActionInOut():EaseQuadraticActionInOut;

    //cc._easeQuarticActionIn = {
    //    easing: cc.EaseQuarticActionIn.prototype._updateTime,
    //    reverse: function(){
    //        return cc._easeQuarticActionIn;
    //    }
    //};

    /**
     * Creates the action easing object. <br />
     * Reference easeIntQuart: <br />
     * {@link http://www.zhihu.com/question/21981571/answer/19925418}
     * @function
     * @returns {Object}
     * @example
     * //example
     * action.easing(cc.easeQuarticActionIn());
     */
    export function easeQuarticActionIn():EaseQuarticActionIn;

    //cc._easeQuarticActionOut = {
    //    easing: cc.EaseQuarticActionOut.prototype._updateTime,
    //    reverse: function(){
    //        return cc._easeQuarticActionOut;
    //    }
    //};

    /**
     * Creates the action easing object. <br />
     * Reference easeOutQuart: <br />
     * {@link http://www.zhihu.com/question/21981571/answer/19925418}
     * @function
     * @returns {Object}
     * @example
     * //example
     * action.easing(cc.QuarticActionOut());
     */
    export function easeQuarticActionOut():EaseQuarticActionOut;

    //cc._easeQuarticActionInOut = {
    //    easing: cc.EaseQuarticActionInOut.prototype._updateTime,
    //    reverse: function(){
    //        return cc._easeQuarticActionInOut;
    //    }
    //};

    /**
     * Creates the action easing object.  <br />
     * Reference easeInOutQuart: <br />
     * {@link http://www.zhihu.com/question/21981571/answer/19925418}
     * @function
     * @returns {Object}
     */
    export function easeQuarticActionInOut():EaseQuarticActionInOut;

    //cc._easeQuinticActionIn = {
    //    easing: cc.EaseQuinticActionIn.prototype._updateTime,
    //    reverse: function(){
    //        return cc._easeQuinticActionIn;
    //    }
    //};

    /**
     * Creates the action easing object. <br />
     * Reference easeInQuint: <br />
     * {@link http://www.zhihu.com/question/21981571/answer/19925418}
     * @function
     * @returns {Object}
     * @example
     * //example
     * action.easing(cc.easeQuinticActionIn());
     */
    export function easeQuinticActionIn():EaseQuinticActionIn;

    //cc._easeQuinticActionOut = {
    //    easing: cc.EaseQuinticActionOut.prototype._updateTime,
    //    reverse: function(){
    //        return cc._easeQuinticActionOut;
    //    }
    //};

    /**
     * Creates the action easing object. <br />
     * Reference easeOutQuint: <br />
     * {@link http://www.zhihu.com/question/21981571/answer/19925418}
     * @function
     * @returns {Object}
     * @example
     * //example
     * action.easing(cc.easeQuadraticActionOut());
     */
    export function easeQuinticActionOut():EaseQuinticActionOut;

    //cc._easeQuinticActionInOut = {
    //    easing: cc.EaseQuinticActionInOut.prototype._updateTime,
    //    reverse: function(){
    //        return cc._easeQuinticActionInOut;
    //    }
    //};

    /**
     * Creates the action easing object. <br />
     * Reference easeInOutQuint: <br />
     * {@link http://www.zhihu.com/question/21981571/answer/19925418}
     * @function
     * @returns {Object}
     * @example
     * //example
     * action.easing(cc.easeQuinticActionInOut());
     */
    export function easeQuinticActionInOut():EaseQuinticActionInOut;

    //cc._easeCircleActionIn = {
    //    easing: cc.EaseCircleActionIn.prototype._updateTime,
    //    reverse: function(){
    //        return cc._easeCircleActionIn;
    //    }
    //};

    /**
     * Creates the action easing object. <br />
     * Reference easeInCirc: <br />
     * {@link http://www.zhihu.com/question/21981571/answer/19925418}
     * @function
     * @returns {Object}
     * @example
     * //example
     * action.easing(cc.easeCircleActionIn());
     */
    export function easeCircleActionIn():EaseCircleActionIn;

    //cc._easeCircleActionOut = {
    //    easing: cc.EaseCircleActionOut.prototype._updateTime,
    //    reverse: function(){
    //        return cc._easeCircleActionOut;
    //    }
    //};

    /**
     * Creates the action easing object. <br />
     * Reference easeOutCirc: <br />
     * {@link http://www.zhihu.com/question/21981571/answer/19925418}
     * @function
     * @returns {Object}
     * @exampple
     * //example
     * actioneasing(cc.easeCircleActionOut());
     */
    export function easeCircleActionOut():EaseCircleActionOut;

    //cc._easeCircleActionInOut = {
    //    easing: cc.EaseCircleActionInOut.prototype._updateTime,
    //    reverse: function(){
    //        return cc._easeCircleActionInOut;
    //    }
    //};

    /**
     * Creates the action easing object. <br />
     * Reference easeInOutCirc: <br />
     * {@link http://www.zhihu.com/question/21981571/answer/19925418}
     * @function
     * @returns {Object}
     * @example
     * //example
     * action.easing(cc.easeCircleActionInOut());
     */
    export function easeCircleActionInOut():EaseCircleActionInOut;

    //cc._easeCubicActionIn = {
    //    easing: cc.EaseCubicActionIn.prototype._updateTime,
    //    reverse: function(){
    //        return cc._easeCubicActionIn;
    //    }
    //};

    /**
     * Creates the action easing object. <br />
     * Reference easeInCubic: <br />
     * {@link http://www.zhihu.com/question/21981571/answer/19925418}
     * @function
     * @returns {Object}
     * @example
     * //example
     * action.easing(cc.easeCubicActionIn());
     */
    export function easeCubicActionIn():EaseCubicActionIn;

    //cc._easeCubicActionOut = {
    //    easing: cc.EaseCubicActionOut.prototype._updateTime,
    //    reverse: function(){
    //        return cc._easeCubicActionOut;
    //    }
    //};

    /**
     * Creates the action easing object. <br />
     * Reference easeOutCubic: <br />
     * {@link http://www.zhihu.com/question/21981571/answer/19925418}
     * @function
     * @returns {Object}
     * @example
     * //example
     * action.easing(cc.easeCubicActionOut());
     */
    export function easeCubicActionOut():EaseCubicActionOut;

    //cc._easeCubicActionInOut = {
    //    easing: cc.EaseCubicActionInOut.prototype._updateTime,
    //    reverse: function(){
    //        return cc._easeCubicActionInOut;
    //    }
    //};

    /**
     * Creates the action easing object. <br />
     * Reference easeInOutCubic: <br />
     * {@link http://www.zhihu.com/question/21981571/answer/19925418}
     * @function
     * @returns {Object}
     */
    export function easeCubicActionInOut():EaseCubicActionInOut;

    /**
     * Show the Node.
     * @function
     * @return {cc.Show}
     * @example
     * // example
     * var showAction = cc.show();
     */
    export function show():Show;

    /**
     * Hide the node.
     * @function
     * @return {cc.Hide}
     * @example
     * // example
     * var hideAction = cc.hide();
     */
    export function hide():Hide;

    /**
     * Toggles the visibility of a node.
     * @function
     * @return {cc.ToggleVisibility}
     * @example
     * // example
     * var toggleVisibilityAction = cc.toggleVisibility();
     */
    export function toggleVisibility():ToggleVisibility;

    /**
     * Create a RemoveSelf object with a flag indicate whether the target should be cleaned up while removing.
     *
     * @function
     * @param {Boolean} [isNeedCleanUp=true]
     * @return {cc.RemoveSelf}
     *
     * @example
     * // example
     * var removeSelfAction = cc.removeSelf();
     */
    export function removeSelf(isNeedCleanUp?:boolean):RemoveSelf;

    /**
     * Create a FlipX action to flip or unflip the target.
     *
     * @function
     * @param {Boolean} flip Indicate whether the target should be flipped or not
     * @return {cc.FlipX}
     * @example
     * var flipXAction = cc.flipX(true);
     */
    export function flipX(flip:boolean):FlipX;

    /**
     * Create a FlipY action to flip or unflip the target.
     *
     * @function
     * @param {Boolean} flip
     * @return {cc.FlipY}
     * @example
     * var flipYAction = cc.flipY(true);
     */
    export function flipY(flip:boolean):FlipY;

    /**
     * Creates a Place action with a position.
     * @function
     * @param {ctype.value_type<cc.Point>|Number} pos
     * @param {Number} [y]
     * @return {cc.Place}
     * @example
     * // example
     * var placeAction = cc.place(cc.p(200, 200));
     * var placeAction = cc.place(200, 200);
     */
    export function place(pos:ctype.value_type<cc.Point>|number, y?:number):Place;

    /**
     * Creates the action with the callback
     * @function
     * @param {function} selector
     * @param {object|null} [selectorTarget]
     * @param {*|null} [data] data for function, it accepts all data types.
     * @return {cc.CallFunc}
     * @example
     * // example
     * // CallFunc without data
     * var finish = cc.callFunc(this.removeSprite, this);
     *
     * // CallFunc with data
     * var finish = cc.callFunc(this.removeFromParentAndCleanup, this._grossini,  true);
     */
    export function callFunc(selector:CallFuncCallback, selectorTarget?:any, data?:any):CallFunc;

    /**
     * An interval action is an action that takes place within a certain period of time.
     * @function
     * @param {Number} d duration in seconds
     * @return {cc.ActionInterval}
     * @example
     * // example
     * var actionInterval = cc.actionInterval(3);
     */
    export function actionInterval(d:number):ActionInterval;

    /** helper constructor to create an array of sequenceable actions
     * @function
     * @param {Array|cc.FiniteTimeAction} tempArray
     * @return {cc.Sequence}
     * @example
     * // example
     * // create sequence with actions
     * var seq = cc.sequence(act1, act2);
     *
     * // create sequence with array
     * var seq = cc.sequence(actArray);
     * todo: It should be use new
     */
    export function sequence(...tempArray:FiniteTimeAction[]):Sequence;
    export function sequence(tempArray:FiniteTimeAction[]):Sequence;

    /**
     * Creates a Repeat action. Times is an unsigned integer between 1 and pow(2,30)
     * @function
     * @param {cc.FiniteTimeAction} action
     * @param {Number} times
     * @return {cc.Repeat}
     * @example
     * // example
     * var rep = cc.repeat(cc.sequence(jump2, jump1), 5);
     */
    export function repeat(action:FiniteTimeAction, times:number):Repeat;

    /**
     * Create a acton which repeat forever
     * @function
     * @param {cc.FiniteTimeAction} action
     * @return {cc.RepeatForever}
     * @example
     * // example
     * var repeat = cc.repeatForever(cc.rotateBy(1.0, 360));
     */
    export function repeatForever(action:FiniteTimeAction):RepeatForever;

    /**
     * Create a spawn action which runs several actions in parallel.
     * @function
     * @param {Array|cc.FiniteTimeAction}tempArray
     * @return {cc.FiniteTimeAction}
     * @example
     * // example
     * var action = cc.spawn(cc.jumpBy(2, cc.p(300, 0), 50, 4), cc.rotateBy(2, 720));
     * todo:It should be the direct use new
     */
    export function spawn(...tempArray:FiniteTimeAction[]):ActionInterval;
    export function spawn(tempArray:FiniteTimeAction[]):ActionInterval;

    /**
     * Creates a RotateTo action with separate rotation angles.
     * To specify the angle of rotation.
     * @function
     * @param {Number} duration duration in seconds
     * @param {Number} deltaAngleX deltaAngleX in degrees.
     * @param {Number} [deltaAngleY] deltaAngleY in degrees.
     * @return {cc.RotateTo}
     * @example
     * // example
     * var rotateTo = cc.rotateTo(2, 61.0);
     */
    export function rotateTo(duration:number, deltaAngleX:number, deltaAngleY?:number):RotateTo;

    /**
     * Rotates a cc.Node object clockwise a number of degrees by modifying it's rotation attribute.
     * Relative to its properties to modify.
     * @function
     * @param {Number} duration duration in seconds
     * @param {Number} deltaAngleX deltaAngleX in degrees
     * @param {Number} [deltaAngleY] deltaAngleY in degrees
     * @return {cc.RotateBy}
     * @example
     * // example
     * var actionBy = cc.rotateBy(2, 360);
     */
    export function rotateBy(duration:number, deltaAngleX:number, deltaAngleY?:number):RotateBy;

    /**
     * Create the action.
     * Relative to its coordinate moves a certain distance.
     * @function
     * @param {Number} duration duration in seconds
     * @param {ctype.value_type<cc.Point>|Number} deltaPos
     * @param {Number} deltaY
     * @return {cc.MoveBy}
     * @example
     * // example
     * var actionTo = cc.moveBy(2, cc.p(windowSize.width - 40, windowSize.height - 40));
     */
    export function moveBy(duration:number, deltaPos:number|ctype.value_type<cc.Point>, deltaY?:number):MoveBy;

    /**
     * Create new action.
     * Moving to the specified coordinates.
     * @function
     * @param {Number} duration duration in seconds
     * @param {ctype.value_type<cc.Point>} position
     * @param {Number} y
     * @return {cc.MoveBy}
     * @example
     * // example
     * var actionBy = cc.moveTo(2, cc.p(80, 80));
     */
    export function moveTo(duration:number, position:number|ctype.value_type<cc.Point>, y?:number):MoveTo;

    /**
     * Create new action.
     * Skews a cc.Node object to given angles by modifying it's skewX and skewY attributes.
     * Changes to the specified value.
     * @function
     * @param {Number} t time in seconds
     * @param {Number} sx
     * @param {Number} sy
     * @return {cc.SkewTo}
     * @example
     * // example
     * var actionTo = cc.skewTo(2, 37.2, -37.2);
     */
    export function skewTo(t:number, sx:number, sy:number):SkewTo;

    /**
     * Skews a cc.Node object by skewX and skewY degrees. <br />
     * Relative to its attribute modification.
     * @function
     * @param {Number} t time in seconds
     * @param {Number} sx sx skew in degrees for X axis
     * @param {Number} sy sy skew in degrees for Y axis
     * @return {cc.SkewBy}
     * @example
     * // example
     * var actionBy = cc.skewBy(2, 0, -90);
     */
    export function skewBy(t:number, sx:number, sy:number):SkewBy;

    /**
     * Moves a cc.Node object simulating a parabolic jump movement by modifying it's position attribute.
     * Relative to its movement.
     * @function
     * @param {Number} duration
     * @param {ctype.value_type<cc.Point>|Number} position
     * @param {Number} [y]
     * @param {Number} height
     * @param {Number} jumps
     * @return {cc.JumpBy}
     * @example
     * // example
     * var actionBy = cc.jumpBy(2, cc.p(300, 0), 50, 4);
     * var actionBy = cc.jumpBy(2, 300, 0, 50, 4);
     */
    export function jumpBy(duration:number, position:number|ctype.value_type<cc.Point>, y?:number, height?:number, jumps?:number):JumpBy;

    /**
     * Moves a cc.Node object to a parabolic position simulating a jump movement by modifying it's position attribute. <br />
     * Jump to the specified location.
     * @function
     * @param {Number} duration
     * @param {ctype.value_type<cc.Point>|Number} position
     * @param {Number} [y]
     * @param {Number} height
     * @param {Number} jumps
     * @return {cc.JumpTo}
     * @example
     * // example
     * var actionTo = cc.jumpTo(2, cc.p(300, 300), 50, 4);
     * var actionTo = cc.jumpTo(2, 300, 300, 50, 4);
     */
    export function jumpTo(duration:number, position:number|ctype.value_type<cc.Point>, y?:number, height?:number, jumps?:number):JumpTo;

    /**
     * @function
     * @param {Number} a
     * @param {Number} b
     * @param {Number} c
     * @param {Number} d
     * @param {Number} t
     * @return {Number}
     */
    export function bezierAt(a:number, b:number, c:number, d:number, t:number):number;

    /**
     * An action that moves the target with a cubic Bezier curve by a certain distance.
     * Relative to its movement.
     * @function
     * @param {Number} duration time in seconds
     * @param {Array} c Array of points
     * @return {cc.BezierBy}
     * @example
     * // example
     * var bezier = [cc.p(0, windowSize.height / 2), cc.p(300, -windowSize.height / 2), cc.p(300, 100)];
     * var bezierForward = cc.bezierBy(3, bezier);
     */
    export function bezierBy(duration:number, c:ctype.value_type<cc.Point>[]):BezierBy;

    /**
     * An action that moves the target with a cubic Bezier curve to a destination point.
     * @function
     * @param {Number} duration
     * @param {Array} c array of points
     * @return {cc.BezierTo}
     * @example
     * // example
     * var bezier = [cc.p(0, windowSize.height / 2), cc.p(300, -windowSize.height / 2), cc.p(300, 100)];
     * var bezierTo = cc.bezierTo(2, bezier);
     */
    export function bezierTo(duration:number, c:ctype.value_type<cc.Point>[]):BezierTo;

    /**
     * Scales a cc.Node object to a zoom factor by modifying it's scale attribute.
     * @function
     * @param {Number} duration
     * @param {Number} sx  scale parameter in X
     * @param {Number} [sy] scale parameter in Y, if Null equal to sx
     * @return {cc.ScaleTo}
     * @example
     * // example
     * // It scales to 0.5 in both X and Y.
     * var actionTo = cc.scaleTo(2, 0.5);
     *
     * // It scales to 0.5 in x and 2 in Y
     * var actionTo = cc.scaleTo(2, 0.5, 2);
     */
    export function scaleTo(duration:number, sx:number, sy?:number):ScaleTo;

    /**
     * Scales a cc.Node object a zoom factor by modifying it's scale attribute.
     * Relative to its changes.
     * @function
     * @param {Number} duration duration in seconds
     * @param {Number} sx sx  scale parameter in X
     * @param {Number|Null} [sy=] sy scale parameter in Y, if Null equal to sx
     * @return {cc.ScaleBy}
     * @example
     * // example without sy, it scales by 2 both in X and Y
     * var actionBy = cc.scaleBy(2, 2);
     *
     * //example with sy, it scales by 0.25 in X and 4.5 in Y
     * var actionBy2 = cc.scaleBy(2, 0.25, 4.5);
     */
    export function scaleBy(duration:number, sx:number, sy?:number):ScaleBy;

    /**
     * Blinks a cc.Node object by modifying it's visible attribute.
     * @function
     * @param {Number} duration  duration in seconds
     * @param blinks blinks in times
     * @return {cc.Blink}
     * @example
     * // example
     * var action = cc.blink(2, 10);
     */
    export function blink(duration:number, blinks:number):Blink;

    /**
     * Fades an object that implements the cc.RGBAProtocol protocol. It modifies the opacity from the current value to a custom one.
     * @function
     * @param {Number} duration
     * @param {Number} opacity 0-255, 0 is transparent
     * @return {cc.FadeTo}
     * @example
     * // example
     * var action = cc.fadeTo(1.0, 0);
     */
    export function fadeTo(duration:number, opacity:number):FadeTo;

    /**
     * Fades In an object that implements the cc.RGBAProtocol protocol. It modifies the opacity from 0 to 255.
     * @function
     * @param {Number} duration duration in seconds
     * @return {cc.FadeIn}
     * @example
     * //example
     * var action = cc.fadeIn(1.0);
     */
    export function fadeIn(duration:number):FadeIn;

    /**
     * Fades Out an object that implements the cc.RGBAProtocol protocol. It modifies the opacity from 255 to 0.
     * @function
     * @param {Number} duration  duration in seconds
     * @return {cc.FadeOut}
     * @example
     * // example
     * var action = cc.fadeOut(1.0);
     */
    export function fadeOut(duration:number):FadeOut;

    /**
     * Tints a cc.Node that implements the cc.NodeRGB protocol from current tint to a custom one.
     * @function
     * @param {Number} duration
     * @param {Number} red 0-255
     * @param {Number} green  0-255
     * @param {Number} blue 0-255
     * @return {cc.TintTo}
     * @example
     * // example
     * var action = cc.tintTo(2, 255, 0, 255);
     */
    export function tintTo(duration:number, red:number, green:number, blue:number):TintTo;

    /**
     * Tints a cc.Node that implements the cc.NodeRGB protocol from current tint to a custom one.
     * Relative to their own color change.
     * @function
     * @param {Number} duration  duration in seconds
     * @param {Number} deltaRed
     * @param {Number} deltaGreen
     * @param {Number} deltaBlue
     * @return {cc.TintBy}
     * @example
     * // example
     * var action = cc.tintBy(2, -127, -255, -127);
     */
    export function tintBy(duration:number, deltaRed:number, deltaGreen:number, deltaBlue:number):TintBy;

    /**
     * Delays the action a certain amount of seconds
     * @function
     * @param {Number} d duration in seconds
     * @return {cc.DelayTime}
     * @example
     * // example
     * var delay = cc.delayTime(1);
     */
    export function delayTime(d:number):DelayTime;

    /**
     * Executes an action in reverse order, from time=duration to time=0.
     * @function
     * @param {cc.FiniteTimeAction} action
     * @return {cc.ReverseTime}
     * @example
     * // example
     *  var reverse = cc.reverseTime(this);
     */
    export function reverseTime(action:FiniteTimeAction):ReverseTime;

    /**
     * create the animate with animation
     * @function
     * @param {cc.Animation} animation
     * @return {cc.Animate}
     * @example
     * // example
     * // create the animation with animation
     * var anim = cc.animate(dance_grey);
     */
    export function animate(animation:Animation):Animate;

    /**
     * Create an action with the specified action and forced target
     * @function
     * @param {cc.Node} target
     * @param {cc.FiniteTimeAction} action
     * @return {cc.TargetedAction}
     */
    export function targetedAction(target:Node, action:FiniteTimeAction):TargetedAction;

    /**
     * Creates an initializes the action with the property name (key), and the from and to parameters.
     * @function
     * @param {Number} duration
     * @param {String} key
     * @param {Number} from
     * @param {Number} to
     * @return {cc.ActionTween}
     */
    export function actionTween(duration:number, key:string, from:number, to:number):ActionTween;
}