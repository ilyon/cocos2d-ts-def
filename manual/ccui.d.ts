
declare namespace ccui {
    export namespace VideoPlayer {
        export enum EventType {
            ERROR,
            PLAYING,
            PAUSED,
            STOPPED,
            COMPLETED,
            SIZE_UPDATED,
        }
    }

    export namespace WebView {
        export enum EventType {
            LOADING,
            LOADED,
            ERROR,
            JS_EVALUATED,
        }
    }
}