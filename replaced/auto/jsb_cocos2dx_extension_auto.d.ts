// DONT CHANGE This File has been changed by another auto progress


/**
 * @module cocos2dx_extension
 */
declare namespace cc {
    /**
     * @class Control
     * @native
     */
    export class Control 
        extends cc.Layer
    {
        /* Can't find native property isOpacityModifyRGB implementation of cc::Control. using any type */
        public opacityModifyRGB : any;
        /**
         * 
         * @method getState
         * @return {number}
         */
        public readonly state : number;
        /**
         * 
         * @method isEnabled
         * @return {boolean}
         */
        public enabled : boolean;
        /**
         * 
         * @method isSelected
         * @return {boolean}
         */
        public selected : boolean;
        /**
         * 
         * @method isHighlighted
         * @return {boolean}
         */
        public highlighted : boolean;


        /**
         *  Tells whether the control is enabled. 
         * @method setEnabled
         * @param {boolean} _bEnabled
         */
        public setEnabled (
            _bEnabled : boolean 
        ) : void;

        /**
         * 
         * @method getState
         * @return {number}
         */
        public getState (
        ) : number;

        /**
         * Sends action messages for the given control events.<br>
         * -- param controlEvents A bitmask whose set flags specify the control events for<br>
         * -- which action messages are sent. See "CCControlEvent" for bitmask constants.
         * @method sendActionsForControlEvents
         * @param {cc.Control::EventType} _controlEvents
         */
        public sendActionsForControlEvents (
            _controlEvents : number 
        ) : void;

        /**
         *  A Boolean value that determines the control selected state. 
         * @method setSelected
         * @param {boolean} _bSelected
         */
        public setSelected (
            _bSelected : boolean 
        ) : void;

        /**
         * 
         * @method isEnabled
         * @return {boolean}
         */
        public isEnabled (
        ) : boolean;

        /**
         * Updates the control layout using its current internal state.
         * @method needsLayout
         */
        public needsLayout (
        ) : void;

        /**
         * 
         * @method hasVisibleParents
         * @return {boolean}
         */
        public hasVisibleParents (
        ) : boolean;

        /**
         * 
         * @method isSelected
         * @return {boolean}
         */
        public isSelected (
        ) : boolean;

        /**
         * Returns a boolean value that indicates whether a touch is inside the bounds<br>
         * -- of the receiver. The given touch must be relative to the world.<br>
         * -- param touch A Touch object that represents a touch.<br>
         * -- return Whether a touch is inside the receiver's rect.
         * @method isTouchInside
         * @param {cc.Touch} _touch
         * @return {boolean}
         */
        public isTouchInside (
            _touch : cc.Touch 
        ) : boolean;

        /**
         *  A Boolean value that determines whether the control is highlighted. 
         * @method setHighlighted
         * @param {boolean} _bHighlighted
         */
        public setHighlighted (
            _bHighlighted : boolean 
        ) : void;

        /**
         * Returns a point corresponding to the touch location converted into the<br>
         * -- control space coordinates.<br>
         * -- param touch A Touch object that represents a touch.
         * @method getTouchLocation
         * @param {cc.Touch} _touch
         * @return {ctype.value_type<cc.Point>}
         */
        public getTouchLocation (
            _touch : cc.Touch 
        ) : ctype.value_type<cc.Point>;

        /**
         * 
         * @method isHighlighted
         * @return {boolean}
         */
        public isHighlighted (
        ) : boolean;

        /**
         *  Creates a Control object 
         * @method create
         * @return {cc.Control}
         */
        public static create (
        ) : cc.Control;

        /**
         * js ctor
         * @method Control
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class ControlButton
     * @native
     */
    export class ControlButton 
        extends cc.Control
    {
        /* Can't find native property getColor implementation of cc::ControlButton. using any type */
        public color : any;
        /* Can't find native property getOpacity implementation of cc::ControlButton. using any type */
        public opacity : any;
        /* Can't find native property getAdjustBackgroundImage implementation of cc::ControlButton. using any type */
        public adjustBackgroundImage : any;
        /**
         * 
         * @method getZoomOnTouchDown
         * @return {boolean}
         */
        public zoomOnTouchDown : boolean;
        /**
         * 
         * @method getPreferredSize
         * @return {ctype.value_type<cc.Size>}
         */
        public preferredSize : ctype.value_type<cc.Size>;
        /**
         * 
         * @method getLabelAnchorPoint
         * @return {ctype.value_type<cc.Point>}
         */
        public labelAnchor : ctype.value_type<cc.Point>;


        /**
         * 
         * @method isPushed
         * @return {boolean}
         */
        public isPushed (
        ) : boolean;

        /**
         * Sets the title label to use for the specified state.<br>
         * -- If a property is not specified for a state, the default is to use<br>
         * -- the ButtonStateNormal value.<br>
         * -- param label The title label to use for the specified state.<br>
         * -- param state The state that uses the specified title. The values are described<br>
         * -- in "CCControlState".
         * @method setTitleLabelForState
         * @param {cc.Node} _label
         * @param {cc.Control::State} _state
         */
        public setTitleLabelForState (
            _label : cc.Node, 
            _state : number 
        ) : void;

        /**
         * 
         * @method setAdjustBackgroundImage
         * @param {boolean} _adjustBackgroundImage
         */
        public setAdjustBackgroundImage (
            _adjustBackgroundImage : boolean 
        ) : void;

        /**
         * Sets the title string to use for the specified state.<br>
         * -- If a property is not specified for a state, the default is to use<br>
         * -- the ButtonStateNormal value.<br>
         * -- param title The title string to use for the specified state.<br>
         * -- param state The state that uses the specified title. The values are described<br>
         * -- in "CCControlState".
         * @method setTitleForState
         * @param {string} _title
         * @param {cc.Control::State} _state
         */
        public setTitleForState (
            _title : string, 
            _state : number 
        ) : void;

        /**
         * 
         * @method setLabelAnchorPoint
         * @param {ctype.value_type<cc.Point>} _var
         */
        public setLabelAnchorPoint (
            _var : ctype.value_type<cc.Point> 
        ) : void;

        /**
         * 
         * @method getLabelAnchorPoint
         * @return {ctype.value_type<cc.Point>}
         */
        public getLabelAnchorPoint (
        ) : ctype.value_type<cc.Point>;

        /**
         * 
         * @method initWithBackgroundSprite
         * @param {ccui.Scale9Sprite} _sprite
         * @return {boolean}
         */
        public initWithBackgroundSprite (
            _sprite : ccui.Scale9Sprite 
        ) : boolean;

        /**
         * 
         * @method getTitleTTFSizeForState
         * @param {cc.Control::State} _state
         * @return {number}
         */
        public getTitleTTFSizeForState (
            _state : number 
        ) : number;

        /**
         * 
         * @method setTitleTTFForState
         * @param {string} _fntFile
         * @param {cc.Control::State} _state
         */
        public setTitleTTFForState (
            _fntFile : string, 
            _state : number 
        ) : void;

        /**
         * 
         * @method setTitleTTFSizeForState
         * @param {number} _size
         * @param {cc.Control::State} _state
         */
        public setTitleTTFSizeForState (
            _size : number, 
            _state : number 
        ) : void;

        /**
         * 
         * @method setTitleLabel
         * @param {cc.Node} _var
         */
        public setTitleLabel (
            _var : cc.Node 
        ) : void;

        /**
         * 
         * @method setPreferredSize
         * @param {ctype.value_type<cc.Size>} _var
         */
        public setPreferredSize (
            _var : ctype.value_type<cc.Size> 
        ) : void;

        /**
         * 
         * @method getCurrentTitleColor
         * @return {ctype.value_type<cc.Color>}
         */
        public getCurrentTitleColor (
        ) : ctype.value_type<cc.Color>;

        /**
         * 
         * @method setZoomOnTouchDown
         * @param {boolean} _var
         */
        public setZoomOnTouchDown (
            _var : boolean 
        ) : void;

        /**
         * 
         * @method setBackgroundSprite
         * @param {ccui.Scale9Sprite} _var
         */
        public setBackgroundSprite (
            _var : ccui.Scale9Sprite 
        ) : void;

        /**
         * Returns the background sprite used for a state.<br>
         * -- param state The state that uses the background sprite. Possible values are<br>
         * -- described in "CCControlState".
         * @method getBackgroundSpriteForState
         * @param {cc.Control::State} _state
         * @return {ccui.Scale9Sprite}
         */
        public getBackgroundSpriteForState (
            _state : number 
        ) : ccui.Scale9Sprite;

        /**
         * 
         * @method getHorizontalOrigin
         * @return {number}
         */
        public getHorizontalOrigin (
        ) : number;

        /**
         * 
         * @method initWithTitleAndFontNameAndFontSize
         * @param {string} _title
         * @param {string} _fontName
         * @param {number} _fontSize
         * @return {boolean}
         */
        public initWithTitleAndFontNameAndFontSize (
            _title : string, 
            _fontName : string, 
            _fontSize : number 
        ) : boolean;

        /**
         * Sets the font of the label, changes the label to a BMFont if necessary.<br>
         * -- param fntFile The name of the font to change to<br>
         * -- param state The state that uses the specified fntFile. The values are described<br>
         * -- in "CCControlState".
         * @method setTitleBMFontForState
         * @param {string} _fntFile
         * @param {cc.Control::State} _state
         */
        public setTitleBMFontForState (
            _fntFile : string, 
            _state : number 
        ) : void;

        /**
         * 
         * @method getScaleRatio
         * @return {number}
         */
        public getScaleRatio (
        ) : number;

        /**
         * 
         * @method getTitleTTFForState
         * @param {cc.Control::State} _state
         * @return {string}
         */
        public getTitleTTFForState (
            _state : number 
        ) : string;

        /**
         * 
         * @method getBackgroundSprite
         * @return {ccui.Scale9Sprite}
         */
        public getBackgroundSprite (
        ) : ccui.Scale9Sprite;

        /**
         * Returns the title color used for a state.<br>
         * -- param state The state that uses the specified color. The values are described<br>
         * -- in "CCControlState".<br>
         * -- return The color of the title for the specified state.
         * @method getTitleColorForState
         * @param {cc.Control::State} _state
         * @return {ctype.value_type<cc.Color>}
         */
        public getTitleColorForState (
            _state : number 
        ) : ctype.value_type<cc.Color>;

        /**
         * Sets the color of the title to use for the specified state.<br>
         * -- param color The color of the title to use for the specified state.<br>
         * -- param state The state that uses the specified color. The values are described<br>
         * -- in "CCControlState".
         * @method setTitleColorForState
         * @param {ctype.value_type<cc.Color>} _color
         * @param {cc.Control::State} _state
         */
        public setTitleColorForState (
            _color : ctype.value_type<cc.Color>, 
            _state : number 
        ) : void;

        /**
         *  Adjust the background image. YES by default. If the property is set to NO, the<br>
         * -- background will use the preferred size of the background image. 
         * @method doesAdjustBackgroundImage
         * @return {boolean}
         */
        public doesAdjustBackgroundImage (
        ) : boolean;

        /**
         * Sets the background spriteFrame to use for the specified button state.<br>
         * -- param spriteFrame The background spriteFrame to use for the specified state.<br>
         * -- param state The state that uses the specified image. The values are described<br>
         * -- in "CCControlState".
         * @method setBackgroundSpriteFrameForState
         * @param {cc.SpriteFrame} _spriteFrame
         * @param {cc.Control::State} _state
         */
        public setBackgroundSpriteFrameForState (
            _spriteFrame : cc.SpriteFrame, 
            _state : number 
        ) : void;

        /**
         * Sets the background sprite to use for the specified button state.<br>
         * -- param sprite The background sprite to use for the specified state.<br>
         * -- param state The state that uses the specified image. The values are described<br>
         * -- in "CCControlState".
         * @method setBackgroundSpriteForState
         * @param {ccui.Scale9Sprite} _sprite
         * @param {cc.Control::State} _state
         */
        public setBackgroundSpriteForState (
            _sprite : ccui.Scale9Sprite, 
            _state : number 
        ) : void;

        /**
         * 
         * @method setScaleRatio
         * @param {number} _var
         */
        public setScaleRatio (
            _var : number 
        ) : void;

        /**
         * 
         * @method getTitleBMFontForState
         * @param {cc.Control::State} _state
         * @return {string}
         */
        public getTitleBMFontForState (
            _state : number 
        ) : string;

        /**
         * 
         * @method getTitleLabel
         * @return {cc.Node}
         */
        public getTitleLabel (
        ) : cc.Node;

        /**
         * 
         * @method getPreferredSize
         * @return {ctype.value_type<cc.Size>}
         */
        public getPreferredSize (
        ) : ctype.value_type<cc.Size>;

        /**
         * 
         * @method getVerticalMargin
         * @return {number}
         */
        public getVerticalMargin (
        ) : number;

        /**
         * Returns the title label used for a state.<br>
         * -- param state The state that uses the title label. Possible values are described<br>
         * -- in "CCControlState".
         * @method getTitleLabelForState
         * @param {cc.Control::State} _state
         * @return {cc.Node}
         */
        public getTitleLabelForState (
            _state : number 
        ) : cc.Node;

        /**
         * 
         * @method setMargins
         * @param {number} _marginH
         * @param {number} _marginV
         */
        public setMargins (
            _marginH : number, 
            _marginV : number 
        ) : void;

        /**
         * 
         * @method getCurrentTitle
         * @return {string}
         */
        public getCurrentTitle (
        ) : string;

        /**
         * 
         * @method initWithLabelAndBackgroundSprite
         * @param {cc.Node} _label
         * @param {ccui.Scale9Sprite} _backgroundSprite
         * @param {boolean} _adjustBackGroundSize
         * @return {boolean}
         */
        public initWithLabelAndBackgroundSprite (
            _label : cc.Node, 
            _backgroundSprite : ccui.Scale9Sprite, 
            _adjustBackGroundSize : boolean 
        ) : boolean;

        /**
         * 
         * @method getZoomOnTouchDown
         * @return {boolean}
         */
        public getZoomOnTouchDown (
        ) : boolean;

        /**
         * Returns the title used for a state.<br>
         * -- param state The state that uses the title. Possible values are described in<br>
         * -- "CCControlState".<br>
         * -- return The title for the specified state.
         * @method getTitleForState
         * @param {cc.Control::State} _state
         * @return {string}
         */
        public getTitleForState (
            _state : number 
        ) : string;

        /**
         * 
         * @method create
         * @param {(ccui.Scale9Sprite) | (cc.Node) | (string)} _sprite | label | title?
         * @param {(ccui.Scale9Sprite) | (string)} _backgroundSprite | fontName?
         * @param {(number) | (boolean)} _fontSize | adjustBackGroundSize?
         * @return {cc.ControlButton}
         */
        public static create (
            _sprite_label_title? : (ccui.Scale9Sprite) | (cc.Node) | (string), 
            _backgroundSprite_fontName? : (ccui.Scale9Sprite) | (string), 
            _fontSize_adjustBackGroundSize? : (number) | (boolean)
        ) : cc.ControlButton;

        /**
         * js ctor
         * @method ControlButton
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class ControlHuePicker
     * @native
     */
    export class ControlHuePicker 
        extends cc.Control
    {
        /* Can't find native property isEnabled implementation of cc::ControlHuePicker. using any type */
        public enabled : any;
        /**
         * 
         * @method getHue
         * @return {number}
         */
        public hue : number;
        /**
         * 
         * @method getHuePercentage
         * @return {number}
         */
        public huePercent : number;
        /**
         * 
         * @method getBackground
         * @return {cc.Sprite}
         */
        public readonly background : cc.Sprite;
        /**
         * 
         * @method getSlider
         * @return {cc.Sprite}
         */
        public readonly slider : cc.Sprite;
        /**
         * 
         * @method getStartPos
         * @return {ctype.value_type<cc.Point>}
         */
        public readonly startPos : ctype.value_type<cc.Point>;


        /**
         * 
         * @method initWithTargetAndPos
         * @param {cc.Node} _target
         * @param {ctype.value_type<cc.Point>} _pos
         * @return {boolean}
         */
        public initWithTargetAndPos (
            _target : cc.Node, 
            _pos : ctype.value_type<cc.Point> 
        ) : boolean;

        /**
         * 
         * @method setHue
         * @param {number} _val
         */
        public setHue (
            _val : number 
        ) : void;

        /**
         * 
         * @method getStartPos
         * @return {ctype.value_type<cc.Point>}
         */
        public getStartPos (
        ) : ctype.value_type<cc.Point>;

        /**
         * 
         * @method getHue
         * @return {number}
         */
        public getHue (
        ) : number;

        /**
         * 
         * @method getSlider
         * @return {cc.Sprite}
         */
        public getSlider (
        ) : cc.Sprite;

        /**
         * 
         * @method setBackground
         * @param {cc.Sprite} _var
         */
        public setBackground (
            _var : cc.Sprite 
        ) : void;

        /**
         * 
         * @method setHuePercentage
         * @param {number} _val
         */
        public setHuePercentage (
            _val : number 
        ) : void;

        /**
         * 
         * @method getBackground
         * @return {cc.Sprite}
         */
        public getBackground (
        ) : cc.Sprite;

        /**
         * 
         * @method getHuePercentage
         * @return {number}
         */
        public getHuePercentage (
        ) : number;

        /**
         * 
         * @method setSlider
         * @param {cc.Sprite} _var
         */
        public setSlider (
            _var : cc.Sprite 
        ) : void;

        /**
         * 
         * @method create
         * @param {cc.Node} _target
         * @param {ctype.value_type<cc.Point>} _pos
         * @return {cc.ControlHuePicker}
         */
        public static create (
            _target : cc.Node, 
            _pos : ctype.value_type<cc.Point> 
        ) : cc.ControlHuePicker;

        /**
         * js ctor
         * @method ControlHuePicker
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class ControlSaturationBrightnessPicker
     * @native
     */
    export class ControlSaturationBrightnessPicker 
        extends cc.Control
    {
        /* Can't find native property isEnabled implementation of cc::ControlSaturationBrightnessPicker. using any type */
        public enabled : any;
        /**
         * 
         * @method getSaturation
         * @return {number}
         */
        public readonly saturation : number;
        /**
         * 
         * @method getBrightness
         * @return {number}
         */
        public readonly brightness : number;
        /**
         * 
         * @method getBackground
         * @return {cc.Sprite}
         */
        public readonly background : cc.Sprite;
        /**
         * 
         * @method getOverlay
         * @return {cc.Sprite}
         */
        public readonly overlay : cc.Sprite;
        /**
         * 
         * @method getShadow
         * @return {cc.Sprite}
         */
        public readonly shadow : cc.Sprite;
        /**
         * 
         * @method getSlider
         * @return {cc.Sprite}
         */
        public readonly slider : cc.Sprite;
        /**
         * 
         * @method getStartPos
         * @return {ctype.value_type<cc.Point>}
         */
        public readonly startPos : ctype.value_type<cc.Point>;


        /**
         * 
         * @method getShadow
         * @return {cc.Sprite}
         */
        public getShadow (
        ) : cc.Sprite;

        /**
         * 
         * @method initWithTargetAndPos
         * @param {cc.Node} _target
         * @param {ctype.value_type<cc.Point>} _pos
         * @return {boolean}
         */
        public initWithTargetAndPos (
            _target : cc.Node, 
            _pos : ctype.value_type<cc.Point> 
        ) : boolean;

        /**
         * 
         * @method getStartPos
         * @return {ctype.value_type<cc.Point>}
         */
        public getStartPos (
        ) : ctype.value_type<cc.Point>;

        /**
         * 
         * @method getOverlay
         * @return {cc.Sprite}
         */
        public getOverlay (
        ) : cc.Sprite;

        /**
         * 
         * @method getSlider
         * @return {cc.Sprite}
         */
        public getSlider (
        ) : cc.Sprite;

        /**
         * 
         * @method getBackground
         * @return {cc.Sprite}
         */
        public getBackground (
        ) : cc.Sprite;

        /**
         * 
         * @method getSaturation
         * @return {number}
         */
        public getSaturation (
        ) : number;

        /**
         * 
         * @method getBrightness
         * @return {number}
         */
        public getBrightness (
        ) : number;

        /**
         * 
         * @method create
         * @param {cc.Node} _target
         * @param {ctype.value_type<cc.Point>} _pos
         * @return {cc.ControlSaturationBrightnessPicker}
         */
        public static create (
            _target : cc.Node, 
            _pos : ctype.value_type<cc.Point> 
        ) : cc.ControlSaturationBrightnessPicker;

        /**
         * js ctor
         * @method ControlSaturationBrightnessPicker
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class ControlColourPicker
     * @native
     */
    export class ControlColourPicker 
        extends cc.Control
    {
        /* Can't find native property getColor implementation of cc::ControlColourPicker. using any type */
        public color : any;
        /* Can't find native property isEnabled implementation of cc::ControlColourPicker. using any type */
        public enabled : any;
        /**
         * 
         * @method getBackground
         * @return {cc.Sprite}
         */
        public readonly background : cc.Sprite;


        /**
         * 
         * @method hueSliderValueChanged
         * @param {cc.Ref} _sender
         * @param {cc.Control::EventType} _controlEvent
         */
        public hueSliderValueChanged (
            _sender : any, 
            _controlEvent : number 
        ) : void;

        /**
         * 
         * @method getHuePicker
         * @return {cc.ControlHuePicker}
         */
        public getHuePicker (
        ) : cc.ControlHuePicker;

        /**
         * 
         * @method getcolourPicker
         * @return {cc.ControlSaturationBrightnessPicker}
         */
        public getcolourPicker (
        ) : cc.ControlSaturationBrightnessPicker;

        /**
         * 
         * @method setBackground
         * @param {cc.Sprite} _var
         */
        public setBackground (
            _var : cc.Sprite 
        ) : void;

        /**
         * 
         * @method setcolourPicker
         * @param {cc.ControlSaturationBrightnessPicker} _var
         */
        public setcolourPicker (
            _var : cc.ControlSaturationBrightnessPicker 
        ) : void;

        /**
         * 
         * @method colourSliderValueChanged
         * @param {cc.Ref} _sender
         * @param {cc.Control::EventType} _controlEvent
         */
        public colourSliderValueChanged (
            _sender : any, 
            _controlEvent : number 
        ) : void;

        /**
         * 
         * @method setHuePicker
         * @param {cc.ControlHuePicker} _var
         */
        public setHuePicker (
            _var : cc.ControlHuePicker 
        ) : void;

        /**
         * 
         * @method getBackground
         * @return {cc.Sprite}
         */
        public getBackground (
        ) : cc.Sprite;

        /**
         * 
         * @method create
         * @return {cc.ControlColourPicker}
         */
        public static create (
        ) : cc.ControlColourPicker;

        /**
         * js ctor<br>
         * -- lua new
         * @method ControlColourPicker
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class ControlPotentiometer
     * @native
     */
    export class ControlPotentiometer 
        extends cc.Control
    {
        /* Can't find native property isEnabled implementation of cc::ControlPotentiometer. using any type */
        public enabled : any;
        /**
         * 
         * @method getValue
         * @return {number}
         */
        public value : number;
        /**
         * 
         * @method getMinimumValue
         * @return {number}
         */
        public minValue : number;
        /**
         * 
         * @method getMaximumValue
         * @return {number}
         */
        public maxValue : number;
        /**
         * 
         * @method getProgressTimer
         * @return {cc.ProgressTimer}
         */
        public progressTimer : cc.ProgressTimer;
        /**
         * 
         * @method getThumbSprite
         * @return {cc.Sprite}
         */
        public thumbSprite : cc.Sprite;
        /**
         * 
         * @method getPreviousLocation
         * @return {ctype.value_type<cc.Point>}
         */
        public prevLocation : ctype.value_type<cc.Point>;


        /**
         * 
         * @method setPreviousLocation
         * @param {ctype.value_type<cc.Point>} _var
         */
        public setPreviousLocation (
            _var : ctype.value_type<cc.Point> 
        ) : void;

        /**
         * 
         * @method setValue
         * @param {number} _value
         */
        public setValue (
            _value : number 
        ) : void;

        /**
         * 
         * @method getProgressTimer
         * @return {cc.ProgressTimer}
         */
        public getProgressTimer (
        ) : cc.ProgressTimer;

        /**
         * 
         * @method getMaximumValue
         * @return {number}
         */
        public getMaximumValue (
        ) : number;

        /**
         *  Returns the angle in degree between line1 and line2. 
         * @method angleInDegreesBetweenLineFromPoint_toPoint_toLineFromPoint_toPoint
         * @param {ctype.value_type<cc.Point>} _beginLineA
         * @param {ctype.value_type<cc.Point>} _endLineA
         * @param {ctype.value_type<cc.Point>} _beginLineB
         * @param {ctype.value_type<cc.Point>} _endLineB
         * @return {number}
         */
        public angleInDegreesBetweenLineFromPoint_toPoint_toLineFromPoint_toPoint (
            _beginLineA : ctype.value_type<cc.Point>, 
            _endLineA : ctype.value_type<cc.Point>, 
            _beginLineB : ctype.value_type<cc.Point>, 
            _endLineB : ctype.value_type<cc.Point> 
        ) : number;

        /**
         *  Factorize the event dispatch into these methods. 
         * @method potentiometerBegan
         * @param {ctype.value_type<cc.Point>} _location
         */
        public potentiometerBegan (
            _location : ctype.value_type<cc.Point> 
        ) : void;

        /**
         * 
         * @method setMaximumValue
         * @param {number} _maximumValue
         */
        public setMaximumValue (
            _maximumValue : number 
        ) : void;

        /**
         * 
         * @method getMinimumValue
         * @return {number}
         */
        public getMinimumValue (
        ) : number;

        /**
         * 
         * @method setThumbSprite
         * @param {cc.Sprite} _var
         */
        public setThumbSprite (
            _var : cc.Sprite 
        ) : void;

        /**
         * 
         * @method getValue
         * @return {number}
         */
        public getValue (
        ) : number;

        /**
         *  Returns the distance between the point1 and point2. 
         * @method distanceBetweenPointAndPoint
         * @param {ctype.value_type<cc.Point>} _point1
         * @param {ctype.value_type<cc.Point>} _point2
         * @return {number}
         */
        public distanceBetweenPointAndPoint (
            _point1 : ctype.value_type<cc.Point>, 
            _point2 : ctype.value_type<cc.Point> 
        ) : number;

        /**
         * 
         * @method potentiometerEnded
         * @param {ctype.value_type<cc.Point>} _location
         */
        public potentiometerEnded (
            _location : ctype.value_type<cc.Point> 
        ) : void;

        /**
         * 
         * @method getPreviousLocation
         * @return {ctype.value_type<cc.Point>}
         */
        public getPreviousLocation (
        ) : ctype.value_type<cc.Point>;

        /**
         * 
         * @method setProgressTimer
         * @param {cc.ProgressTimer} _var
         */
        public setProgressTimer (
            _var : cc.ProgressTimer 
        ) : void;

        /**
         * 
         * @method setMinimumValue
         * @param {number} _minimumValue
         */
        public setMinimumValue (
            _minimumValue : number 
        ) : void;

        /**
         * 
         * @method getThumbSprite
         * @return {cc.Sprite}
         */
        public getThumbSprite (
        ) : cc.Sprite;

        /**
         * Initializes a potentiometer with a track sprite and a progress bar.<br>
         * -- param trackSprite   Sprite, that is used as a background.<br>
         * -- param progressTimer ProgressTimer, that is used as a progress bar.
         * @method initWithTrackSprite_ProgressTimer_ThumbSprite
         * @param {cc.Sprite} _trackSprite
         * @param {cc.ProgressTimer} _progressTimer
         * @param {cc.Sprite} _thumbSprite
         * @return {boolean}
         */
        public initWithTrackSprite_ProgressTimer_ThumbSprite (
            _trackSprite : cc.Sprite, 
            _progressTimer : cc.ProgressTimer, 
            _thumbSprite : cc.Sprite 
        ) : boolean;

        /**
         * 
         * @method potentiometerMoved
         * @param {ctype.value_type<cc.Point>} _location
         */
        public potentiometerMoved (
            _location : ctype.value_type<cc.Point> 
        ) : void;

        /**
         * Creates potentiometer with a track filename and a progress filename.
         * @method create
         * @param {string} _backgroundFile
         * @param {string} _progressFile
         * @param {string} _thumbFile
         * @return {cc.ControlPotentiometer}
         */
        public static create (
            _backgroundFile : string, 
            _progressFile : string, 
            _thumbFile : string 
        ) : cc.ControlPotentiometer;

        /**
         * js ctor<br>
         * -- lua new
         * @method ControlPotentiometer
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class ControlSlider
     * @native
     */
    export class ControlSlider 
        extends cc.Control
    {
        /* Can't find native property isEnabled implementation of cc::ControlSlider. using any type */
        public enabled : any;
        /**
         * 
         * @method getValue
         * @return {number}
         */
        public value : number;
        /**
         * 
         * @method getMinimumValue
         * @return {number}
         */
        public minValue : number;
        /**
         * 
         * @method getMaximumValue
         * @return {number}
         */
        public maxValue : number;
        /**
         * 
         * @method getMinimumAllowedValue
         * @return {number}
         */
        public minAllowedValue : number;
        /**
         * 
         * @method getMaximumAllowedValue
         * @return {number}
         */
        public maxAllowedValue : number;
        /**
         * 
         * @method getThumbSprite
         * @return {cc.Sprite}
         */
        public readonly thumbSprite : cc.Sprite;
        /**
         * 
         * @method getProgressSprite
         * @return {cc.Sprite}
         */
        public readonly progressSprite : cc.Sprite;
        /**
         * 
         * @method getBackgroundSprite
         * @return {cc.Sprite}
         */
        public readonly backgroundSprite : cc.Sprite;


        /**
         * 
         * @method getMaximumAllowedValue
         * @return {number}
         */
        public getMaximumAllowedValue (
        ) : number;

        /**
         * Initializes a slider with a background sprite, a progress bar and a thumb<br>
         * -- item.<br>
         * -- param backgroundSprite          Sprite, that is used as a background.<br>
         * -- param progressSprite            Sprite, that is used as a progress bar.<br>
         * -- param thumbSprite               Sprite, that is used as a thumb.<br>
         * -- param selectedThumbSprite       Sprite, that is used as a selected thumb.
         * @method initWithSprites
         * @param {(cc.Sprite)} _backgroundSprite
         * @param {(cc.Sprite)} _progressSprite
         * @param {(cc.Sprite)} _thumbSprite
         * @param {(cc.Sprite)} _selectedThumbSprite?
         * @return {boolean}
         */
        public initWithSprites (
            _backgroundSprite : (cc.Sprite), 
            _progressSprite : (cc.Sprite), 
            _thumbSprite : (cc.Sprite), 
            _selectedThumbSprite? : (cc.Sprite)
        ) : boolean;

        /**
         * 
         * @method getMinimumAllowedValue
         * @return {number}
         */
        public getMinimumAllowedValue (
        ) : number;

        /**
         * 
         * @method getMaximumValue
         * @return {number}
         */
        public getMaximumValue (
        ) : number;

        /**
         * 
         * @method getSelectedThumbSprite
         * @return {cc.Sprite}
         */
        public getSelectedThumbSprite (
        ) : cc.Sprite;

        /**
         * 
         * @method setProgressSprite
         * @param {cc.Sprite} _var
         */
        public setProgressSprite (
            _var : cc.Sprite 
        ) : void;

        /**
         * 
         * @method setMaximumValue
         * @param {number} _val
         */
        public setMaximumValue (
            _val : number 
        ) : void;

        /**
         * 
         * @method getMinimumValue
         * @return {number}
         */
        public getMinimumValue (
        ) : number;

        /**
         * 
         * @method setThumbSprite
         * @param {cc.Sprite} _var
         */
        public setThumbSprite (
            _var : cc.Sprite 
        ) : void;

        /**
         * 
         * @method getValue
         * @return {number}
         */
        public getValue (
        ) : number;

        /**
         * 
         * @method getBackgroundSprite
         * @return {cc.Sprite}
         */
        public getBackgroundSprite (
        ) : cc.Sprite;

        /**
         * 
         * @method getThumbSprite
         * @return {cc.Sprite}
         */
        public getThumbSprite (
        ) : cc.Sprite;

        /**
         * 
         * @method setValue
         * @param {number} _val
         */
        public setValue (
            _val : number 
        ) : void;

        /**
         * 
         * @method locationFromTouch
         * @param {cc.Touch} _touch
         * @return {ctype.value_type<cc.Point>}
         */
        public locationFromTouch (
            _touch : cc.Touch 
        ) : ctype.value_type<cc.Point>;

        /**
         * 
         * @method setMinimumValue
         * @param {number} _val
         */
        public setMinimumValue (
            _val : number 
        ) : void;

        /**
         * 
         * @method setMinimumAllowedValue
         * @param {number} _var
         */
        public setMinimumAllowedValue (
            _var : number 
        ) : void;

        /**
         * 
         * @method getProgressSprite
         * @return {cc.Sprite}
         */
        public getProgressSprite (
        ) : cc.Sprite;

        /**
         * 
         * @method setSelectedThumbSprite
         * @param {cc.Sprite} _var
         */
        public setSelectedThumbSprite (
            _var : cc.Sprite 
        ) : void;

        /**
         * 
         * @method setBackgroundSprite
         * @param {cc.Sprite} _var
         */
        public setBackgroundSprite (
            _var : cc.Sprite 
        ) : void;

        /**
         * 
         * @method setMaximumAllowedValue
         * @param {number} _var
         */
        public setMaximumAllowedValue (
            _var : number 
        ) : void;

        /**
         * Creates a slider with a given background sprite and a progress bar and a<br>
         * -- thumb item.<br>
         * -- see initWithSprites
         * @method create
         * @param {(cc.Sprite) | (string)} _backgroundSprite | bgFile
         * @param {(cc.Sprite) | (string)} _pogressSprite | progressFile
         * @param {(cc.Sprite) | (string)} _thumbSprite | thumbFile
         * @param {(string) | (cc.Sprite)} _selectedThumbSpriteFile | selectedThumbSprite?
         * @return {cc.ControlSlider}
         */
        public static create (
            _backgroundSprite_bgFile : (cc.Sprite) | (string), 
            _pogressSprite_progressFile : (cc.Sprite) | (string), 
            _thumbSprite_thumbFile : (cc.Sprite) | (string), 
            _selectedThumbSpriteFile_selectedThumbSprite? : (string) | (cc.Sprite)
        ) : cc.ControlSlider;

        /**
         * js ctor<br>
         * -- lua new
         * @method ControlSlider
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class ControlStepper
     * @native
     */
    export class ControlStepper 
        extends cc.Control
    {
        /* Can't find native property getWraps implementation of cc::ControlStepper. using any type */
        public wraps : any;
        /**
         * 
         * @method getValue
         * @return {number}
         */
        public value : number;
        /* Can't find native property getMinimumValue implementation of cc::ControlStepper. using any type */
        public minValue : any;
        /* Can't find native property getMaximumValue implementation of cc::ControlStepper. using any type */
        public maxValue : any;
        /* Can't find native property getStepValue implementation of cc::ControlStepper. using any type */
        public stepValue : any;
        /**
         * 
         * @method isContinuous
         * @return {boolean}
         */
        public readonly continuous : boolean;
        /**
         * 
         * @method getMinusSprite
         * @return {cc.Sprite}
         */
        public minusSprite : cc.Sprite;
        /**
         * 
         * @method getPlusSprite
         * @return {cc.Sprite}
         */
        public plusSprite : cc.Sprite;
        /**
         * 
         * @method getMinusLabel
         * @return {cc.Label}
         */
        public minusLabel : cc.Label;
        /* Can't find native property getPlusSLabel implementation of cc::ControlStepper. using any type */
        public plusSLabel : any;


        /**
         * 
         * @method getMinusSprite
         * @return {cc.Sprite}
         */
        public getMinusSprite (
        ) : cc.Sprite;

        /**
         * 
         * @method setValue
         * @param {number} _value
         */
        public setValue (
            _value : number 
        ) : void;

        /**
         * 
         * @method setStepValue
         * @param {number} _stepValue
         */
        public setStepValue (
            _stepValue : number 
        ) : void;

        /**
         * 
         * @method initWithMinusSpriteAndPlusSprite
         * @param {cc.Sprite} _minusSprite
         * @param {cc.Sprite} _plusSprite
         * @return {boolean}
         */
        public initWithMinusSpriteAndPlusSprite (
            _minusSprite : cc.Sprite, 
            _plusSprite : cc.Sprite 
        ) : boolean;

        /**
         *  Set the numeric value of the stepper. If send is true, the Control::EventType::VALUE_CHANGED is sent. 
         * @method setValueWithSendingEvent
         * @param {number} _value
         * @param {boolean} _send
         */
        public setValueWithSendingEvent (
            _value : number, 
            _send : boolean 
        ) : void;

        /**
         * 
         * @method setMaximumValue
         * @param {number} _maximumValue
         */
        public setMaximumValue (
            _maximumValue : number 
        ) : void;

        /**
         * 
         * @method getMinusLabel
         * @return {cc.Label}
         */
        public getMinusLabel (
        ) : cc.Label;

        /**
         * 
         * @method getPlusLabel
         * @return {cc.Label}
         */
        public getPlusLabel (
        ) : cc.Label;

        /**
         * 
         * @method setWraps
         * @param {boolean} _wraps
         */
        public setWraps (
            _wraps : boolean 
        ) : void;

        /**
         * 
         * @method setMinusLabel
         * @param {cc.Label} _var
         */
        public setMinusLabel (
            _var : cc.Label 
        ) : void;

        /**
         *  Start the autorepeat increment/decrement. 
         * @method startAutorepeat
         */
        public startAutorepeat (
        ) : void;

        /**
         *  Update the layout of the stepper with the given touch location. 
         * @method updateLayoutUsingTouchLocation
         * @param {ctype.value_type<cc.Point>} _location
         */
        public updateLayoutUsingTouchLocation (
            _location : ctype.value_type<cc.Point> 
        ) : void;

        /**
         * 
         * @method isContinuous
         * @return {boolean}
         */
        public isContinuous (
        ) : boolean;

        /**
         *  Stop the autorepeat. 
         * @method stopAutorepeat
         */
        public stopAutorepeat (
        ) : void;

        /**
         * 
         * @method setMinimumValue
         * @param {number} _minimumValue
         */
        public setMinimumValue (
            _minimumValue : number 
        ) : void;

        /**
         * 
         * @method setPlusLabel
         * @param {cc.Label} _var
         */
        public setPlusLabel (
            _var : cc.Label 
        ) : void;

        /**
         * 
         * @method getValue
         * @return {number}
         */
        public getValue (
        ) : number;

        /**
         * 
         * @method getPlusSprite
         * @return {cc.Sprite}
         */
        public getPlusSprite (
        ) : cc.Sprite;

        /**
         * 
         * @method setPlusSprite
         * @param {cc.Sprite} _var
         */
        public setPlusSprite (
            _var : cc.Sprite 
        ) : void;

        /**
         * 
         * @method setMinusSprite
         * @param {cc.Sprite} _var
         */
        public setMinusSprite (
            _var : cc.Sprite 
        ) : void;

        /**
         * 
         * @method create
         * @param {cc.Sprite} _minusSprite
         * @param {cc.Sprite} _plusSprite
         * @return {cc.ControlStepper}
         */
        public static create (
            _minusSprite : cc.Sprite, 
            _plusSprite : cc.Sprite 
        ) : cc.ControlStepper;

        /**
         * js ctor<br>
         * -- lua new
         * @method ControlStepper
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class ControlSwitch
     * @native
     */
    export class ControlSwitch 
        extends cc.Control
    {
        /* Can't find native property isEnabled implementation of cc::ControlSwitch. using any type */
        public enabled : any;


        /**
         * 
         * @method setOn
         * @param {(boolean)} _isOn
         * @param {(boolean)} _animated?
         */
        public setOn (
            _isOn : (boolean), 
            _animated? : (boolean)
        ) : void;

        /**
         * 
         * @method locationFromTouch
         * @param {cc.Touch} _touch
         * @return {ctype.value_type<cc.Point>}
         */
        public locationFromTouch (
            _touch : cc.Touch 
        ) : ctype.value_type<cc.Point>;

        /**
         * 
         * @method isOn
         * @return {boolean}
         */
        public isOn (
        ) : boolean;

        /**
         *  Initializes a switch with a mask sprite, on/off sprites for on/off states, a thumb sprite and an on/off labels. 
         * @method initWithMaskSprite
         * @param {(cc.Sprite)} _maskSprite
         * @param {(cc.Sprite)} _onSprite
         * @param {(cc.Sprite)} _offSprite
         * @param {(cc.Sprite)} _thumbSprite
         * @param {(cc.Label)} _onLabel?
         * @param {(cc.Label)} _offLabel?
         * @return {boolean}
         */
        public initWithMaskSprite (
            _maskSprite : (cc.Sprite), 
            _onSprite : (cc.Sprite), 
            _offSprite : (cc.Sprite), 
            _thumbSprite : (cc.Sprite), 
            _onLabel? : (cc.Label), 
            _offLabel? : (cc.Label)
        ) : boolean;

        /**
         * 
         * @method hasMoved
         * @return {boolean}
         */
        public hasMoved (
        ) : boolean;

        /**
         *  Creates a switch with a mask sprite, on/off sprites for on/off states and a thumb sprite. 
         * @method create
         * @param {(cc.Sprite)} _maskSprite
         * @param {(cc.Sprite)} _onSprite
         * @param {(cc.Sprite)} _offSprite
         * @param {(cc.Sprite)} _thumbSprite
         * @param {(cc.Label)} _onLabel?
         * @param {(cc.Label)} _offLabel?
         * @return {cc.ControlSwitch}
         */
        public static create (
            _maskSprite : (cc.Sprite), 
            _onSprite : (cc.Sprite), 
            _offSprite : (cc.Sprite), 
            _thumbSprite : (cc.Sprite), 
            _onLabel? : (cc.Label), 
            _offLabel? : (cc.Label)
        ) : cc.ControlSwitch;

        /**
         * js ctor<br>
         * -- lua new
         * @method ControlSwitch
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class ScrollView
     * @native
     */
    export class ScrollView 
        extends cc.Layer
    {

        // See jsb_cocos2dx_extension_manual manual bindings for more information about this
        public setDelegate(delegate : cc.ScrollViewDelegate) : void;
        

        public static readonly DIR_NONE: number;
        public static readonly DIR_VERTICAL: number;
        public static readonly DIR_HORIZONTAL: number;
        public static readonly DIR_BOTH: number;
        public static readonly EVENT_SCROLL_TO_TOP: number;
        public static readonly EVENT_SCROLL_TO_BOTTOM: number;
        public static readonly EVENT_SCROLL_TO_LEFT: number;
        public static readonly EVENT_SCROLL_TO_RIGHT: number;
        public static readonly EVENT_SCROLLING: number;
        public static readonly EVENT_BOUNCE_TOP: number;
        public static readonly EVENT_BOUNCE_BOTTOM: number;
        public static readonly EVENT_BOUNCE_LEFT: number;
        public static readonly EVENT_BOUNCE_RIGHT: number;
        public static readonly AUTO_SCROLL_MAX_SPEED: number;
        /* Can't find native property _getWidth implementation of cc::ScrollView. using any type */
        public width : any;
        /* Can't find native property _getHeight implementation of cc::ScrollView. using any type */
        public height : any;
        /**
         * Query scroll direction of scrollview.
         * see `Direction`      Direction::VERTICAL means vertical scroll, Direction::HORIZONTAL means horizontal scroll
         * return Scrollview scroll direction.
         * @method getDirection
         * @return {number}
         */
        public direction : number;
        /* Can't find native property _getInnerWidth implementation of ccui::ScrollView. using any type */
        public innerWidth : any;
        /* Can't find native property _getInnerHeight implementation of ccui::ScrollView. using any type */
        public innerHeight : any;
        /* Can't find native property getBounceEnabled implementation of ccui::ScrollView. using any type */
        public bounceEnabled : any;
        /* Can't find native property getInertiaScrollEnabled implementation of ccui::ScrollView. using any type */
        public inertiaScrollEnabled : any;
        /* Can't find native property getChildren implementation of ccui::ScrollView. using any type */
        public readonly children : any;
        /* Can't find native property getChildrenCount implementation of ccui::ScrollView. using any type */
        public readonly childrenCount : any;
        /* Can't find native property getLayoutType implementation of ccui::ScrollView. using any type */
        public layoutType : any;


        /**
         * Determines whether it clips its children or not.
         * @method isClippingToBounds
         * @return {boolean}
         */
        public isClippingToBounds (
        ) : boolean;

        /**
         * 
         * @method setContainer
         * @param {cc.Node} _pContainer
         */
        public setContainer (
            _pContainer : cc.Node 
        ) : void;

        /**
         * Sets a new content offset. It ignores max/min offset. It just sets what's given. (just like UIKit's UIScrollView)<br>
         * -- You can override the animation duration with this method.<br>
         * -- param offset    The new offset.<br>
         * -- param dt        The animation duration.
         * @method setContentOffsetInDuration
         * @param {ctype.value_type<cc.Point>} _offset
         * @param {number} _dt
         */
        public setContentOffsetInDuration (
            _offset : ctype.value_type<cc.Point>, 
            _dt : number 
        ) : void;

        /**
         * Sets a new scale for container in a given duration.<br>
         * -- param s     The new scale value<br>
         * -- param dt    The animation duration
         * @method setZoomScaleInDuration
         * @param {number} _s
         * @param {number} _dt
         */
        public setZoomScaleInDuration (
            _s : number, 
            _dt : number 
        ) : void;

        /**
         * CCActionTweenDelegate
         * @method updateTweenAction
         * @param {number} _value
         * @param {string} _key
         */
        public updateTweenAction (
            _value : number, 
            _key : string 
        ) : void;

        /**
         * Set max scale<br>
         * -- param maxScale max scale
         * @method setMaxScale
         * @param {number} _maxScale
         */
        public setMaxScale (
            _maxScale : number 
        ) : void;

        /**
         * 
         * @method hasVisibleParents
         * @return {boolean}
         */
        public hasVisibleParents (
        ) : boolean;

        /**
         * direction allowed to scroll. ScrollViewDirectionBoth by default.
         * @method getDirection
         * @return {number}
         */
        public getDirection (
        ) : number;

        /**
         * 
         * @method getContainer
         * @return {cc.Node}
         */
        public getContainer (
        ) : cc.Node;

        /**
         * Set min scale<br>
         * -- param minScale min scale
         * @method setMinScale
         * @param {number} _minScale
         */
        public setMinScale (
            _minScale : number 
        ) : void;

        /**
         * 
         * @method getZoomScale
         * @return {number}
         */
        public getZoomScale (
        ) : number;

        /**
         * 
         * @method updateInset
         */
        public updateInset (
        ) : void;

        /**
         * Returns a scroll view object<br>
         * -- param size view size<br>
         * -- param container parent object<br>
         * -- return scroll view object
         * @method initWithViewSize
         * @param {ctype.value_type<cc.Size>} _size
         * @param {cc.Node} _container
         * @return {boolean}
         */
        public initWithViewSize (
            _size : ctype.value_type<cc.Size>, 
            _container : cc.Node 
        ) : boolean;

        /**
         * 
         * @method pause
         * @param {cc.Ref} _sender
         */
        public pause (
            _sender : any 
        ) : void;

        /**
         * 
         * @method setDirection
         * @param {cc.ScrollView::Direction} _eDirection
         */
        public setDirection (
            _eDirection : number 
        ) : void;

        /**
         * 
         * @method setBounceable
         * @param {boolean} _bBounceable
         */
        public setBounceable (
            _bBounceable : boolean 
        ) : void;

        /**
         * Sets a new content offset. It ignores max/min offset. It just sets what's given. (just like UIKit's UIScrollView)<br>
         * -- param offset    The new offset.<br>
         * -- param animated  If true, the view will scroll to the new offset.
         * @method setContentOffset
         * @param {ctype.value_type<cc.Point>} _offset
         * @param {boolean} _animated
         */
        public setContentOffset (
            _offset : ctype.value_type<cc.Point>, 
            _animated : boolean 
        ) : void;

        /**
         * 
         * @method isDragging
         * @return {boolean}
         */
        public isDragging (
        ) : boolean;

        /**
         * 
         * @method isTouchEnabled
         * @return {boolean}
         */
        public isTouchEnabled (
        ) : boolean;

        /**
         * 
         * @method isBounceable
         * @return {boolean}
         */
        public isBounceable (
        ) : boolean;

        /**
         * 
         * @method setTouchEnabled
         * @param {boolean} _enabled
         */
        public setTouchEnabled (
            _enabled : boolean 
        ) : void;

        /**
         * 
         * @method getContentOffset
         * @return {ctype.value_type<cc.Point>}
         */
        public getContentOffset (
        ) : ctype.value_type<cc.Point>;

        /**
         * 
         * @method resume
         * @param {cc.Ref} _sender
         */
        public resume (
            _sender : any 
        ) : void;

        /**
         * 
         * @method setClippingToBounds
         * @param {boolean} _bClippingToBounds
         */
        public setClippingToBounds (
            _bClippingToBounds : boolean 
        ) : void;

        /**
         * 
         * @method setViewSize
         * @param {ctype.value_type<cc.Size>} _size
         */
        public setViewSize (
            _size : ctype.value_type<cc.Size> 
        ) : void;

        /**
         * size to clip. Node boundingBox uses contentSize directly.<br>
         * -- It's semantically different what it actually means to common scroll views.<br>
         * -- Hence, this scroll view will use a separate size property.
         * @method getViewSize
         * @return {ctype.value_type<cc.Size>}
         */
        public getViewSize (
        ) : ctype.value_type<cc.Size>;

        /**
         * Returns the current container's maximum offset. You may want this while you animate scrolling by yourself
         * @method maxContainerOffset
         * @return {ctype.value_type<cc.Point>}
         */
        public maxContainerOffset (
        ) : ctype.value_type<cc.Point>;

        /**
         * 
         * @method isTouchMoved
         * @return {boolean}
         */
        public isTouchMoved (
        ) : boolean;

        /**
         * Determines if a given node's bounding box is in visible bounds<br>
         * -- returns true if it is in visible bounds
         * @method isNodeVisible
         * @param {cc.Node} _node
         * @return {boolean}
         */
        public isNodeVisible (
            _node : cc.Node 
        ) : boolean;

        /**
         * Returns the current container's minimum offset. You may want this while you animate scrolling by yourself
         * @method minContainerOffset
         * @return {ctype.value_type<cc.Point>}
         */
        public minContainerOffset (
        ) : ctype.value_type<cc.Point>;

        /**
         * Sets a new scale and does that for a predefined duration.<br>
         * -- param s         The new scale vale<br>
         * -- param animated  If true, scaling is animated
         * @method setZoomScale
         * @param {(number)} _s
         * @param {(boolean)} _animated?
         */
        public setZoomScale (
            _s : (number), 
            _animated? : (boolean)
        ) : void;

        /**
         * Returns an autoreleased scroll view object.<br>
         * -- return autoreleased scroll view object
         * @method create
         * @param {(ctype.value_type<cc.Size>)} _size?
         * @param {(cc.Node)} _container?
         * @return {cc.ScrollView}
         */
        public static create (
            _size? : (ctype.value_type<cc.Size>), 
            _container? : (cc.Node)
        ) : cc.ScrollView;

        /**
         * js ctor<br>
         * -- lua new
         * @method ScrollView
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class TableViewCell
     * @native
     */
    export class TableViewCell 
        extends cc.Node
    {
        /* Can't find native property getObjectID implementation of cc::TableViewCell. using any type */
        public objectId : any;


        /**
         * Cleans up any resources linked to this cell and resets <code>idx</code> property.
         * @method reset
         */
        public reset (
        ) : void;

        /**
         * The index used internally by SWTableView and its subclasses
         * @method getIdx
         * @return {number}
         */
        public getIdx (
        ) : number;

        /**
         * 
         * @method setIdx
         * @param {number} _uIdx
         */
        public setIdx (
            _uIdx : number 
        ) : void;

        /**
         * 
         * @method create
         * @return {cc.TableViewCell}
         */
        public static create (
        ) : cc.TableViewCell;

        /**
         * 
         * @method TableViewCell
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class TableView
     * @native
     */
    export class TableView 
        extends cc.ScrollView
    {

        // See jsb_cocos2dx_extension_manual manual bindings for more information about this
        public setDelegate(delegate : cc.TableViewDelegate) : void;
        public setDataSource(dataSource : cc.TableViewDataSource) : void;

        public static create(dataSource: cc.TableViewDataSource, size: ctype.value_type<cc.Size>, container?: cc.Node): cc.TableView;
        


        /**
         * Updates the content of the cell at a given index.<br>
         * -- param idx index to find a cell
         * @method updateCellAtIndex
         * @param {number} _idx
         */
        public updateCellAtIndex (
            _idx : number 
        ) : void;

        /**
         * determines how cell is ordered and filled in the view.
         * @method setVerticalFillOrder
         * @param {cc.TableView::VerticalFillOrder} _order
         */
        public setVerticalFillOrder (
            _order : number 
        ) : void;

        /**
         * 
         * @method _updateContentSize
         */
        public _updateContentSize (
        ) : void;

        /**
         * 
         * @method getVerticalFillOrder
         * @return {number}
         */
        public getVerticalFillOrder (
        ) : number;

        /**
         * Removes a cell at a given index<br>
         * -- param idx index to find a cell
         * @method removeCellAtIndex
         * @param {number} _idx
         */
        public removeCellAtIndex (
            _idx : number 
        ) : void;

        /**
         * 
         * @method initWithViewSize
         * @param {ctype.value_type<cc.Size>} _size
         * @param {cc.Node} _container
         * @return {boolean}
         */
        public initWithViewSize (
            _size : ctype.value_type<cc.Size>, 
            _container : cc.Node 
        ) : boolean;

        /**
         * 
         * @method scrollViewDidScroll
         * @param {cc.ScrollView} _view
         */
        public scrollViewDidScroll (
            _view : cc.ScrollView 
        ) : void;

        /**
         * reloads data from data source.  the view will be refreshed.
         * @method reloadData
         */
        public reloadData (
        ) : void;

        /**
         * 
         * @method scrollViewDidZoom
         * @param {cc.ScrollView} _view
         */
        public scrollViewDidZoom (
            _view : cc.ScrollView 
        ) : void;

        /**
         * Inserts a new cell at a given index<br>
         * -- param idx location to insert
         * @method insertCellAtIndex
         * @param {number} _idx
         */
        public insertCellAtIndex (
            _idx : number 
        ) : void;

        /**
         * Returns an existing cell at a given index. Returns nil if a cell is nonexistent at the moment of query.<br>
         * -- param idx index<br>
         * -- return a cell at a given index
         * @method cellAtIndex
         * @param {number} _idx
         * @return {cc.TableViewCell}
         */
        public cellAtIndex (
            _idx : number 
        ) : cc.TableViewCell;

        /**
         * Dequeues a free cell if available. nil if not.<br>
         * -- return free cell
         * @method dequeueCell
         * @return {cc.TableViewCell}
         */
        public dequeueCell (
        ) : cc.TableViewCell;

        /**
         * js ctor<br>
         * -- lua new
         * @method TableView
         * @constructor
         */
        public constructor (
        );

    }
}
namespace jsb 
{
    /**
     * @class EventAssetsManagerEx
     */
    export class EventAssetsManager 
        extends cc.EventCustom
    {

        /**
         * 
         * @method getAssetsManagerEx
         * @return {jsb.AssetsManager}
         */
        public getAssetsManagerEx (
        ) : jsb.AssetsManager;

        /**
         * 
         * @method getAssetId
         * @return {string}
         */
        public getAssetId (
        ) : string;

        /**
         * 
         * @method getCURLECode
         * @return {number}
         */
        public getCURLECode (
        ) : number;

        /**
         * 
         * @method getMessage
         * @return {string}
         */
        public getMessage (
        ) : string;

        /**
         * 
         * @method getCURLMCode
         * @return {number}
         */
        public getCURLMCode (
        ) : number;

        /**
         * 
         * @method getPercentByFile
         * @return {number}
         */
        public getPercentByFile (
        ) : number;

        /**
         * 
         * @method getEventCode
         * @return {number}
         */
        public getEventCode (
        ) : number;

        /**
         * 
         * @method getPercent
         * @return {number}
         */
        public getPercent (
        ) : number;

        /**
         *  Constructor 
         * @method EventAssetsManagerEx
         * @constructor
         * @param {string} _eventName
         * @param {jsb.AssetsManager} _manager
         * @param {jsb.EventAssetsManager::EventCode} _code
         * @param {number} _percent
         * @param {number} _percentByFile
         * @param {string} _assetId
         * @param {string} _message
         * @param {number} _curle_code
         * @param {number} _curlm_code
         */
        public constructor (
            _eventName : string, 
            _manager : jsb.AssetsManager, 
            _code : number, 
            _percent : number, 
            _percentByFile : number, 
            _assetId : string, 
            _message : string, 
            _curle_code : number, 
            _curlm_code : number 
        );

    }
    /**
     * @class Manifest
     * @native
     */
    export abstract class Manifest 
    {

        /**
         *  @brief Gets remote manifest file url.
         * @method getManifestFileUrl
         * @return {string}
         */
        public getManifestFileUrl (
        ) : string;

        /**
         *  @brief Check whether the version informations have been fully loaded
         * @method isVersionLoaded
         * @return {boolean}
         */
        public isVersionLoaded (
        ) : boolean;

        /**
         *  @brief Check whether the manifest have been fully loaded
         * @method isLoaded
         * @return {boolean}
         */
        public isLoaded (
        ) : boolean;

        /**
         *  @brief Gets remote package url.
         * @method getPackageUrl
         * @return {string}
         */
        public getPackageUrl (
        ) : string;

        /**
         *  @brief Gets manifest version.
         * @method getVersion
         * @return {string}
         */
        public getVersion (
        ) : string;

        /**
         *  @brief Gets remote version file url.
         * @method getVersionFileUrl
         * @return {string}
         */
        public getVersionFileUrl (
        ) : string;

        /**
         *  @brief Get the search paths list related to the Manifest.
         * @method getSearchPaths
         * @return {string[]}
         */
        public getSearchPaths (
        ) : string[];

    }
    /**
     * @class AssetsManagerEx
     * @native
     */
    export class AssetsManager 
    {
        /**
         * Currently JavaScript Bindigns (JSB), in some cases, needs to use retain and release. This is a bug in JSB, <br/>
         * and the ugly workaround is to use retain/release. So, these 2 methods were added to be compatible with JSB. <br/>
         * This is a hack, and should be removed once JSB fixes the retain/release bug.
         */
        public retain():void;

        /**
         * Currently JavaScript Bindigns (JSB), in some cases, needs to use retain and release. This is a bug in JSB, <br/>
         * and the ugly workaround is to use retain/release. So, these 2 methods were added to be compatible with JSB. <br/>
         * This is a hack, and should be removed once JSB fixes the retain/release bug.
         */
        public release():void;
        

        /**
         *  @brief Gets the current update state.
         * @method getState
         * @return {number}
         */
        public getState (
        ) : number;

        /**
         *  @brief  Check out if there is a new version of manifest.<br>
         * -- You may use this method before updating, then let user determine whether<br>
         * -- he wants to update resources.
         * @method checkUpdate
         */
        public checkUpdate (
        ) : void;

        /**
         *  @brief Gets storage path.
         * @method getStoragePath
         * @return {string}
         */
        public getStoragePath (
        ) : string;

        /**
         *  @brief Update with the current local manifest.
         * @method update
         */
        public update (
        ) : void;

        /**
         *  @brief Function for retrieve the local manifest object
         * @method getLocalManifest
         * @return {jsb.Manifest}
         */
        public getLocalManifest (
        ) : jsb.Manifest;

        /**
         *  @brief Function for retrieve the remote manifest object
         * @method getRemoteManifest
         * @return {jsb.Manifest}
         */
        public getRemoteManifest (
        ) : jsb.Manifest;

        /**
         *  @brief Reupdate all failed assets under the current AssetsManagerEx context
         * @method downloadFailedAssets
         */
        public downloadFailedAssets (
        ) : void;

        /**
         *  @brief Create function for creating a new AssetsManagerEx<br>
         * -- param manifestUrl   The url for the local manifest file<br>
         * -- param storagePath   The storage path for downloaded assetes<br>
         * -- warning   The cached manifest in your storage path have higher priority and will be searched first,<br>
         * -- only if it doesn't exist, AssetsManagerEx will use the given manifestUrl.
         * @method create
         * @param {string} _manifestUrl
         * @param {string} _storagePath
         * @return {jsb.AssetsManager}
         */
        public static create (
            _manifestUrl : string, 
            _storagePath : string 
        ) : jsb.AssetsManager;

        /**
         * 
         * @method AssetsManagerEx
         * @constructor
         * @param {string} _manifestUrl
         * @param {string} _storagePath
         */
        public constructor (
            _manifestUrl : string, 
            _storagePath : string 
        );

    }
    /**
     * @class EventListenerAssetsManagerEx
     * @native
     */
    export class EventListenerAssetsManager 
        extends cc.EventListenerCustom
    {

        /**
         *  Initializes event with type and callback function 
         * @method init
         * @param {jsb.AssetsManager} _AssetsManagerEx
         * @param {(arg0:jsb.EventAssetsManager) => void} _callback
         * @return {boolean}
         */
        public init (
            _AssetsManagerEx : jsb.AssetsManager, 
            _callback : (arg0:jsb.EventAssetsManager) => void 
        ) : boolean;

        /**
         *  Creates an event listener with type and callback.<br>
         * -- param eventType The type of the event.<br>
         * -- param callback The callback function when the specified event was emitted.
         * @method create
         * @param {jsb.AssetsManager} _AssetsManagerEx
         * @param {(arg0:jsb.EventAssetsManager) => void} _callback
         * @return {jsb.EventListenerAssetsManager}
         */
        public static create (
            _AssetsManagerEx : jsb.AssetsManager, 
            _callback : (arg0:jsb.EventAssetsManager) => void 
        ) : jsb.EventListenerAssetsManager;

        /**
         *  Constructor 
         * @method EventListenerAssetsManagerEx
         * @constructor
         */
        public constructor (
        );

    }
}