// DONT CHANGE This File has been changed by another auto progress


/**
 * @module cocos2dx_experimental_video
 */
declare namespace ccui {
    /**
     * @class VideoPlayer
     * @native
     */
    export class VideoPlayer 
        extends ccui.Widget
    {
        setEventListener(event: ccui.VideoPlayer.EventType, callback: (sender: ccui.VideoPlayer)=>void): void;
        

        /**
         * brief Get the local video file name.<br>
         * -- return The video file name.
         * @method getFileName
         * @return {string}
         */
        public getFileName (
        ) : string;

        /**
         * brief Get the URL of remoting video source.<br>
         * -- return A remoting URL address.
         * @method getURL
         * @return {string}
         */
        public getURL (
        ) : string;

        /**
         * Starts playback.
         * @method play
         */
        public play (
        ) : void;

        /**
         * Causes the video player to keep aspect ratio or no when displaying the video.<br>
         * -- param enable    Specify true to keep aspect ratio or false to scale the video until <br>
         * -- both dimensions fit the visible bounds of the view exactly.
         * @method setKeepAspectRatioEnabled
         * @param {boolean} _enable
         */
        public setKeepAspectRatioEnabled (
            _enable : boolean 
        ) : void;

        /**
         * Stops playback.
         * @method stop
         */
        public stop (
        ) : void;

        /**
         * Causes the video player to enter or exit full-screen mode.<br>
         * -- param fullscreen    Specify true to enter full-screen mode or false to exit full-screen mode.
         * @method setFullScreenEnabled
         * @param {boolean} _fullscreen
         */
        public setFullScreenEnabled (
            _fullscreen : boolean 
        ) : void;

        /**
         * Sets a file path as a video source for VideoPlayer.
         * @method setFileName
         * @param {string} _videoPath
         */
        public setFileName (
            _videoPath : string 
        ) : void;

        /**
         * Sets a URL as a video source for VideoPlayer.
         * @method setURL
         * @param {string} __videoURL
         */
        public setURL (
            __videoURL : string 
        ) : void;

        /**
         * Indicates whether the video player keep aspect ratio when displaying the video.
         * @method isKeepAspectRatioEnabled
         * @return {boolean}
         */
        public isKeepAspectRatioEnabled (
        ) : boolean;

        /**
         * 
         * @method getNaturalSize
         * @return {ctype.value_type<cc.Size>}
         */
        public getNaturalSize (
        ) : ctype.value_type<cc.Size>;

        /**
         * brief A function which will be called when video is playing.<br>
         * -- param event @see VideoPlayer::EventType.
         * @method onPlayEvent
         * @param {number} _event
         */
        public onPlayEvent (
            _event : number 
        ) : void;

        /**
         * Indicates whether the video player is in full-screen mode.<br>
         * -- return True if the video player is in full-screen mode, false otherwise.
         * @method isFullScreenEnabled
         * @return {boolean}
         */
        public isFullScreenEnabled (
        ) : boolean;

        /**
         * Checks whether the VideoPlayer is playing.<br>
         * -- return True if currently playing, false otherwise.
         * @method isPlaying
         * @return {boolean}
         */
        public isPlaying (
        ) : boolean;

        /**
         * Seeks to specified time position.<br>
         * -- param sec   The offset in seconds from the start to seek to.
         * @method seekTo
         * @param {number} _sec
         */
        public seekTo (
            _sec : number 
        ) : void;

        /**
         * 
         * @method create
         * @return {any}
         */
        public static create (
        ) : any;

        /**
         * 
         * @method VideoPlayer
         * @constructor
         */
        public constructor (
        );

    }
}