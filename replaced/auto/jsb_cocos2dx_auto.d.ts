// DONT CHANGE This File has been changed by another auto progress


/**
 * @module cocos2dx
 */
declare namespace cc {
    /**
     * @class Texture2D
     * @native
     */
    export class Texture2D 
    {


        initWithElement(element: HTMLImageElement | HTMLCanvasElement): void;
        handleLoadedTexture(): void;
        
        /**
         *  Gets the texture name. 
         * @method getName
         * @return {number}
         */
        public readonly name : number;
        /**
         *  Gets the pixel format of the texture. 
         * @method getPixelFormat
         * @return {number}
         */
        public readonly pixelFormat : number;
        /**
         *  Gets the width of the texture in pixels. 
         * @method getPixelsWide
         * @return {number}
         */
        public readonly pixelsWidth : number;
        /**
         *  Gets the height of the texture in pixels. 
         * @method getPixelsHigh
         * @return {number}
         */
        public readonly pixelsHeight : number;
        /* Can't find native property _getWidth implementation of cc::Texture2D. using any type */
        public readonly width : any;
        /* Can't find native property _getHeight implementation of cc::Texture2D. using any type */
        public readonly height : any;
        /**
         *  Get a shader program from the texture.
         * @method getGLProgram
         * @return {cc.GLProgram}
         */
        public shaderProgram : cc.GLProgram;
        /**
         *  Gets max S. 
         * @method getMaxS
         * @return {number}
         */
        public maxS : number;
        /**
         *  Gets max T. 
         * @method getMaxT
         * @return {number}
         */
        public maxT : number;


        /**
         *  Get a shader program from the texture.
         * @method getGLProgram
         * @return {cc.GLProgram}
         */
        public getShaderProgram (
        ) : cc.GLProgram;

        /**
         *  Gets max T. 
         * @method getMaxT
         * @return {number}
         */
        public getMaxT (
        ) : number;

        /**
         * 
         * @method setAlphaTexture
         * @param {cc.Texture2D} _alphaTexture
         */
        public setAlphaTexture (
            _alphaTexture : cc.Texture2D 
        ) : void;

        /**
         *  Returns the pixel format.<br>
         * -- since v2.0
         * @method getStringForFormat
         * @return {string}
         */
        public getStringForFormat (
        ) : string;

        /**
         * Initializes a texture from a UIImage object.<br>
         * -- We will use the format you passed to the function to convert the image format to the texture format.<br>
         * -- If you pass PixelFormat::Automatic, we will auto detect the image render type and use that type for texture to render.<br>
         * -- param image An UIImage object.<br>
         * -- param format Texture pixel formats.
         * @method initWithImage
         * @param {(cc.Image)} _image
         * @param {(cc.Texture2D::PixelFormat)} _format?
         * @return {boolean}
         */
        public initWithImage (
            _image : (cc.Image), 
            _format? : (number)
        ) : boolean;

        /**
         *  Set a shader program to the texture.<br>
         * -- It's used by drawAtPoint and drawInRect
         * @method setGLProgram
         * @param {cc.GLProgram} _program
         */
        public setShaderProgram (
            _program : cc.GLProgram 
        ) : void;

        /**
         *  Gets max S. 
         * @method getMaxS
         * @return {number}
         */
        public getMaxS (
        ) : number;

        /**
         *  Release only the gl texture.<br>
         * -- js NA<br>
         * -- lua NA
         * @method releaseGLTexture
         */
        public releaseGLTexture (
        ) : void;

        /**
         *  Whether or not the texture has their Alpha premultiplied. 
         * @method hasPremultipliedAlpha
         * @return {boolean}
         */
        public hasPremultipliedAlpha (
        ) : boolean;

        /**
         *  Initializes with mipmaps. <br>
         * -- param mipmaps Specifies a pointer to the image data in memory.<br>
         * -- param mipmapsNum The mipmaps number.<br>
         * -- param pixelFormat The image pixelFormat.<br>
         * -- param pixelsWide The image width.<br>
         * -- param pixelsHigh The image height.
         * @method initWithMipmaps
         * @param {cc._MipmapInfo} _mipmaps
         * @param {number} _mipmapsNum
         * @param {cc.Texture2D::PixelFormat} _pixelFormat
         * @param {number} _pixelsWide
         * @param {number} _pixelsHigh
         * @param {ctype.value_type<cc.Vec4>} _colors_palette
         * @return {boolean}
         */
        public initWithMipmaps (
            _mipmaps : cc._MipmapInfo, 
            _mipmapsNum : number, 
            _pixelFormat : number, 
            _pixelsWide : number, 
            _pixelsHigh : number, 
            _colors_palette : ctype.value_type<cc.Vec4> 
        ) : boolean;

        /**
         *  Gets the height of the texture in pixels. 
         * @method getPixelsHigh
         * @return {number}
         */
        public getPixelsHigh (
        ) : number;

        /**
         * 
         * @method getAlphaTextureName
         * @return {number}
         */
        public getAlphaTextureName (
        ) : number;

        /**
         *  Helper functions that returns bits per pixels for a given format.<br>
         * -- since v2.0
         * @method getBitsPerPixelForFormat
         * @param {(cc.Texture2D::PixelFormat)} _format?
         * @return {number}
         */
        public getBitsPerPixelForFormat (
            _format? : (number)
        ) : number;

        /**
         *  Gets the texture name. 
         * @method getName
         * @return {number}
         */
        public getName (
        ) : number;

        /**
         *  Initializes a texture from a string using a text definition.<br>
         * -- param text A null terminated string.<br>
         * -- param textDefinition A FontDefinition object contains font attributes.
         * @method initWithString
         * @param {(string)} _text
         * @param {(cc.FontDefinition) | (string)} _textDefinition | fontName
         * @param {(number)} _fontSize?
         * @param {(ctype.value_type<cc.Size>)} _dimensions?
         * @param {(cc.TextHAlignment)} _hAlignment?
         * @param {(cc.TextVAlignment)} _vAlignment?
         * @param {(boolean)} _enableWrap?
         * @param {(number)} _overflow?
         * @return {boolean}
         */
        public initWithString (
            _text : (string), 
            _textDefinition_fontName : (cc.FontDefinition) | (string), 
            _fontSize? : (number), 
            _dimensions? : (ctype.value_type<cc.Size>), 
            _hAlignment? : (number), 
            _vAlignment? : (number), 
            _enableWrap? : (boolean), 
            _overflow? : (number)
        ) : boolean;

        /**
         *  Sets max T. 
         * @method setMaxT
         * @param {number} _maxT
         */
        public setMaxT (
            _maxT : number 
        ) : void;

        /**
         * 
         * @method getPath
         * @return {string}
         */
        public getPath (
        ) : string;

        /**
         *  Draws a texture inside a rect.
         * @method drawInRect
         * @param {ctype.value_type<cc.Rect>} _rect
         */
        public drawInRect (
            _rect : ctype.value_type<cc.Rect> 
        ) : void;

        /**
         *  Get the texture content size.
         * @method getContentSize
         * @return {ctype.value_type<cc.Size>}
         */
        public getContentSize (
        ) : ctype.value_type<cc.Size>;

        /**
         *  Sets alias texture parameters:<br>
         * -- - GL_TEXTURE_MIN_FILTER = GL_NEAREST<br>
         * -- - GL_TEXTURE_MAG_FILTER = GL_NEAREST<br>
         * -- warning Calling this method could allocate additional texture memory.<br>
         * -- since v0.8
         * @method setAliasTexParameters
         */
        public setAliasTexParameters (
        ) : void;

        /**
         * 
         * @method getColorPalette
         * @return {ctype.value_type<cc.Vec4>}
         */
        public getColorPalette (
        ) : ctype.value_type<cc.Vec4>;

        /**
         *  Sets antialias texture parameters:<br>
         * -- - GL_TEXTURE_MIN_FILTER = GL_LINEAR<br>
         * -- - GL_TEXTURE_MAG_FILTER = GL_LINEAR<br>
         * -- warning Calling this method could allocate additional texture memory.<br>
         * -- since v0.8
         * @method setAntiAliasTexParameters
         */
        public setAntiAliasTexParameters (
        ) : void;

        /**
         *  Generates mipmap images for the texture.<br>
         * -- It only works if the texture size is POT (power of 2).<br>
         * -- since v0.99.0
         * @method generateMipmap
         */
        public generateMipmap (
        ) : void;

        /**
         * 
         * @method getAlphaTexture
         * @return {cc.Texture2D}
         */
        public getAlphaTexture (
        ) : cc.Texture2D;

        /**
         * Get texture name, dimensions and coordinates message by a string.<br>
         * -- js NA<br>
         * -- lua NA
         * @method getDescription
         * @return {string}
         */
        public getDescription (
        ) : string;

        /**
         *  Gets the pixel format of the texture. 
         * @method getPixelFormat
         * @return {number}
         */
        public getPixelFormat (
        ) : number;

        /**
         * 
         * @method getTexture2DInfo
         * @return {any}
         */
        public getTexture2DInfo (
        ) : any;

        /**
         *  Get content size. 
         * @method getContentSizeInPixels
         * @return {ctype.value_type<cc.Size>}
         */
        public getContentSizeInPixels (
        ) : ctype.value_type<cc.Size>;

        /**
         *  Gets the width of the texture in pixels. 
         * @method getPixelsWide
         * @return {number}
         */
        public getPixelsWide (
        ) : number;

        /**
         * Drawing extensions to make it easy to draw basic quads using a Texture2D object.<br>
         * -- These functions require GL_TEXTURE_2D and both GL_VERTEX_ARRAY and GL_TEXTURE_COORD_ARRAY client states to be enabled.<br>
         * -- Draws a texture at a given point. 
         * @method drawAtPoint
         * @param {ctype.value_type<cc.Point>} _point
         */
        public drawAtPoint (
            _point : ctype.value_type<cc.Point> 
        ) : void;

        /**
         *  Whether or not the texture has mip maps.
         * @method hasMipmaps
         * @return {boolean}
         */
        public hasMipmaps (
        ) : boolean;

        /**
         *  Sets max S. 
         * @method setMaxS
         * @param {number} _maxS
         */
        public setMaxS (
            _maxS : number 
        ) : void;

        /**
         *  sets the default pixel format for UIImagescontains alpha channel.<br>
         * -- param format<br>
         * -- If the UIImage contains alpha channel, then the options are:<br>
         * -- - generate 32-bit textures: Texture2D::PixelFormat::RGBA8888 (default one)<br>
         * -- - generate 24-bit textures: Texture2D::PixelFormat::RGB888<br>
         * -- - generate 16-bit textures: Texture2D::PixelFormat::RGBA4444<br>
         * -- - generate 16-bit textures: Texture2D::PixelFormat::RGB5A1<br>
         * -- - generate 16-bit textures: Texture2D::PixelFormat::RGB565<br>
         * -- - generate 8-bit textures: Texture2D::PixelFormat::A8 (only use it if you use just 1 color)<br>
         * -- How does it work ?<br>
         * -- - If the image is an RGBA (with Alpha) then the default pixel format will be used (it can be a 8-bit, 16-bit or 32-bit texture)<br>
         * -- - If the image is an RGB (without Alpha) then: If the default pixel format is RGBA8888 then a RGBA8888 (32-bit) will be used. Otherwise a RGB565 (16-bit texture) will be used.<br>
         * -- This parameter is not valid for PVR / PVR.CCZ images.<br>
         * -- since v0.8
         * @method setDefaultAlphaPixelFormat
         * @param {cc.Texture2D::PixelFormat} _format
         */
        public static setDefaultAlphaPixelFormat (
            _format : number 
        ) : void;

        /**
         *  Returns the alpha pixel format.<br>
         * -- since v0.8
         * @method getDefaultAlphaPixelFormat
         * @return {number}
         */
        public static getDefaultAlphaPixelFormat (
        ) : number;

        /**
         * js ctor
         * @method Texture2D
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class Touch
     * @native
     */
    export class Touch 
    {

        // *****************************************************************
        // *****************************************************************
        // ******IMPORTANT NOTE: *******************************************
        // This section is relay on 
        // cocos2d-x/cocos/scripting/js-bindings/manual/cocos2d_specifics.cpp
        // *****************************************************************
        // *****************************************************************
        
        /**
         * Currently JavaScript Bindings (JSB), in some cases, needs to use retain and release. This is a bug in JSB,
         * and the ugly workaround is to use retain/release. So, these 2 methods were added to be compatible with JSB.
         * This is a hack, and should be removed once JSB fixes the retain/release bug
         * You will need to retain an object if you created a listener and haven't added it any target node during the same frame.
         * Otherwise, JSB's native autorelease pool will consider this object a useless one and release it directly,
         * when you want to use it later, a "Invalid Native Object" error will be raised.
         * The retain function can increase a reference count for the native object to avoid it being released,
         * you need to manually invoke release function when you think this object is no longer needed, otherwise, there will be memory learks.
         * retain and release function call should be paired in developer's game code.
         * @function
         * @see cc.EventListener#release
         */
        retain():void;

        
        /**
         * Currently JavaScript Bindings (JSB), in some cases, needs to use retain and release. This is a bug in JSB,
         * and the ugly workaround is to use retain/release. So, these 2 methods were added to be compatible with JSB.
         * This is a hack, and should be removed once JSB fixes the retain/release bug
         * You will need to retain an object if you created a listener and haven't added it any target node during the same frame.
         * Otherwise, JSB's native autorelease pool will consider this object a useless one and release it directly,
         * when you want to use it later, a "Invalid Native Object" error will be raised.
         * The retain function can increase a reference count for the native object to avoid it being released,
         * you need to manually invoke release function when you think this object is no longer needed, otherwise, there will be memory learks.
         * retain and release function call should be paired in developer's game code.
         * @function
         * @see cc.EventListener#retain
         */
        public release():void;
        
        // *****************************************************************
        // *****************************************************************
        // *****************************************************************
        // *****************************************************************
        // *****************************************************************
        

        /**
         *  Returns the previous touch location in screen coordinates. <br>
         * -- return The previous touch location in screen coordinates.
         * @method getPreviousLocationInView
         * @return {ctype.value_type<cc.Point>}
         */
        public getPreviousLocationInView (
        ) : ctype.value_type<cc.Point>;

        /**
         *  Returns the current touch location in OpenGL coordinates.<br>
         * -- return The current touch location in OpenGL coordinates.
         * @method getLocation
         * @return {ctype.value_type<cc.Point>}
         */
        public getLocation (
        ) : ctype.value_type<cc.Point>;

        /**
         *  Returns the delta of 2 current touches locations in screen coordinates.<br>
         * -- return The delta of 2 current touches locations in screen coordinates.
         * @method getDelta
         * @return {ctype.value_type<cc.Point>}
         */
        public getDelta (
        ) : ctype.value_type<cc.Point>;

        /**
         *  Returns the start touch location in screen coordinates.<br>
         * -- return The start touch location in screen coordinates.
         * @method getStartLocationInView
         * @return {ctype.value_type<cc.Point>}
         */
        public getStartLocationInView (
        ) : ctype.value_type<cc.Point>;

        /**
         *  Returns the current touch force for 3d touch.<br>
         * -- return The current touch force for 3d touch.
         * @method getCurrentForce
         * @return {number}
         */
        public getCurrentForce (
        ) : number;

        /**
         *  Returns the start touch location in OpenGL coordinates.<br>
         * -- return The start touch location in OpenGL coordinates.
         * @method getStartLocation
         * @return {ctype.value_type<cc.Point>}
         */
        public getStartLocation (
        ) : ctype.value_type<cc.Point>;

        /**
         *  Get touch id.<br>
         * -- js getId<br>
         * -- lua getId<br>
         * -- return The id of touch.
         * @method getID
         * @return {number}
         */
        public getID (
        ) : number;

        /**
         *  Set the touch information. It always used to monitor touch event.<br>
         * -- param id A given id<br>
         * -- param x A given x coordinate.<br>
         * -- param y A given y coordinate.<br>
         * -- param force Current force for 3d touch.<br>
         * -- param maxForce maximum possible force for 3d touch.
         * @method setTouchInfo
         * @param {(number)} _id
         * @param {(number)} _x
         * @param {(number)} _y
         * @param {(number)} _force?
         * @param {(number)} _maxForce?
         */
        public setTouchInfo (
            _id : (number), 
            _x : (number), 
            _y : (number), 
            _force? : (number), 
            _maxForce? : (number)
        ) : void;

        /**
         *  Returns the maximum touch force for 3d touch.<br>
         * -- return The maximum touch force for 3d touch.
         * @method getMaxForce
         * @return {number}
         */
        public getMaxForce (
        ) : number;

        /**
         *  Returns the current touch location in screen coordinates.<br>
         * -- return The current touch location in screen coordinates.
         * @method getLocationInView
         * @return {ctype.value_type<cc.Point>}
         */
        public getLocationInView (
        ) : ctype.value_type<cc.Point>;

        /**
         *  Returns the previous touch location in OpenGL coordinates.<br>
         * -- return The previous touch location in OpenGL coordinates.
         * @method getPreviousLocation
         * @return {ctype.value_type<cc.Point>}
         */
        public getPreviousLocation (
        ) : ctype.value_type<cc.Point>;

        /**
         *  Constructor.<br>
         * -- js ctor
         * @method Touch
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class Event
     * @native
     */
    export class Event 
    {
        public static readonly TOUCH: number;
        public static readonly KEYBOARD: number;
        public static readonly ACCELERATION: number;
        public static readonly MOUSE: number;
        public static readonly CUSTOM: number;


        /**
         *  Checks whether the event has been stopped.<br>
         * -- return True if the event has been stopped.
         * @method isStopped
         * @return {boolean}
         */
        public isStopped (
        ) : boolean;

        /**
         *  Gets the event type.<br>
         * -- return The event type.
         * @method getType
         * @return {number}
         */
        public getType (
        ) : number;

        /**
         *  Gets current target of the event.<br>
         * -- return The target with which the event associates.<br>
         * -- note It's only available when the event listener is associated with node.<br>
         * -- It returns 0 when the listener is associated with fixed priority.
         * @method getCurrentTarget
         * @return {cc.Node}
         */
        public getCurrentTarget (
        ) : cc.Node;

        /**
         *  Stops propagation for current event.
         * @method stopPropagation
         */
        public stopPropagation (
        ) : void;

        /**
         *  Constructor 
         * @method Event
         * @constructor
         * @param {cc.Event::Type} _type
         */
        public constructor (
            _type : number 
        );

    }
    /**
     * @class EventTouch
     * @native
     */
    export class EventTouch 
        extends cc.Event
    {
        /**
         * The maximum touch numbers
         * @constant
         * @type {Number}
         */
        public static MAX_TOUCHES:number;

        public static EventCode:EventCodeMap;

        // *****************************************************************
        // *****************************************************************
        // ******IMPORTANT NOTE: *******************************************
        // This section is relay on 
        // cocos2d-x/cocos/scripting/js-bindings/manual/cocos2d_specifics.cpp
        // *****************************************************************
        // *****************************************************************
        
        /**
         * Returns touches of event
         * @returns {Array}
         */
        public getTouches():Touch[];
        
        // *****************************************************************
        // *****************************************************************
        // *****************************************************************
        // *****************************************************************
        // *****************************************************************
        
        public static readonly MAX_TOUCHES: number;


        /**
         *  Get event code.<br>
         * -- return The code of the event.
         * @method getEventCode
         * @return {number}
         */
        public getEventCode (
        ) : number;

        /**
         *  Set the event code.<br>
         * -- param eventCode A given EventCode.
         * @method setEventCode
         * @param {cc.EventTouch::EventCode} _eventCode
         */
        public setEventCode (
            _eventCode : number 
        ) : void;

        /**
         * Constructor.<br>
         * -- js NA
         * @method EventTouch
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class ComponentContainer
     * @native
     */
    export abstract class ComponentContainer 
    {

        /**
         * 
         * @method visit
         * @param {number} _delta
         */
        public visit (
            _delta : number 
        ) : void;

        /**
         * 
         * @method remove
         * @param {(cc.Component) | (string)} _com | name
         * @return {boolean}
         */
        public remove (
            _com_name : (cc.Component) | (string)
        ) : boolean;

        /**
         * 
         * @method removeAll
         */
        public removeAll (
        ) : void;

        /**
         * 
         * @method add
         * @param {cc.Component} _com
         * @return {boolean}
         */
        public add (
            _com : cc.Component 
        ) : boolean;

        /**
         * 
         * @method isEmpty
         * @return {boolean}
         */
        public isEmpty (
        ) : boolean;

        /**
         * js getComponent
         * @method get
         * @param {string} _name
         * @return {cc.Component}
         */
        public getComponent (
            _name : string 
        ) : cc.Component;

    }
    /**
     * @class Component
     * @native
     */
    export class Component 
    {

        /**
         * 
         * @method setEnabled
         * @param {boolean} _enabled
         */
        public setEnabled (
            _enabled : boolean 
        ) : void;

        /**
         * 
         * @method setName
         * @param {string} _name
         */
        public setName (
            _name : string 
        ) : void;

        /**
         * 
         * @method isEnabled
         * @return {boolean}
         */
        public isEnabled (
        ) : boolean;

        /**
         * 
         * @method getOwner
         * @return {cc.Node}
         */
        public getOwner (
        ) : cc.Node;

        /**
         * 
         * @method init
         * @return {boolean}
         */
        public init (
        ) : boolean;

        /**
         * 
         * @method setOwner
         * @param {cc.Node} _owner
         */
        public setOwner (
            _owner : cc.Node 
        ) : void;

        /**
         * 
         * @method getName
         * @return {string}
         */
        public getName (
        ) : string;

        /**
         * 
         * @method create
         * @return {cc.Component}
         */
        public static create (
        ) : cc.Component;

        /**
         * js ctor
         * @method Component
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class Node
     * @native
     */
    export class Node 
    {
        
        ///**
        // * THIS IS A HACK FOR SUPPORTING TS to JS inheritance
        // */
        protected ctor(...args : any[]) : void;
        
        ///**
        // * Properties configuration function
        // * All properties in attrs will be set to the node,
        // * when the setter of the node is available,
        // * the property will be set via setter function.
        // *
        // * @function
        // * @param {Object} attrs Properties to be set to node
        // */
        public attr(attrs : any) : void;
        
        // *****************************************************************
        // *****************************************************************
        // ******IMPORTANT NOTE: *******************************************
        // This section is relay on 
        // cocos2d-x/cocos/scripting/js-bindings/manual/cocos2d_specifics.cpp
        // *****************************************************************
        // *****************************************************************
        
        /**
         * Currently JavaScript Bindings (JSB), in some cases, needs to use retain and release. This is a bug in JSB,
         * and the ugly workaround is to use retain/release. So, these 2 methods were added to be compatible with JSB.
         * This is a hack, and should be removed once JSB fixes the retain/release bug
         * You will need to retain an object if you created an engine object and haven't added it into the scene graph during the same frame.
         * Otherwise, JSB's native autorelease pool will consider this object a useless one and release it directly,
         * when you want to use it later, a "Invalid Native Object" error will be raised.
         * The retain function can increase a reference count for the native object to avoid it being released,
         * you need to manually invoke release function when you think this object is no longer needed, otherwise, there will be memory learks.
         * retain and release function call should be paired in developer's game code.
         * @function
         * @see cc.Node#release
         */
        public retain():void;
    
        
        /**
         * Currently JavaScript Bindings (JSB), in some cases, needs to use retain and release. This is a bug in JSB,
         * and the ugly workaround is to use retain/release. So, these 2 methods were added to be compatible with JSB.
         * This is a hack, and should be removed once JSB fixes the retain/release bug
         * You will need to retain an object if you created an engine object and haven't added it into the scene graph during the same frame.
         * Otherwise, JSB's native autorelease pool will consider this object a useless one and release it directly,
         * when you want to use it later, a "Invalid Native Object" error will be raised.
         * The retain function can increase a reference count for the native object to avoid it being released,
         * you need to manually invoke release function when you think this object is no longer needed, otherwise, there will be memory learks.
         * retain and release function call should be paired in developer's game code.
         * @function
         * @see cc.Node#retain
         */
        public release():void;
        
        
        /**
         *
         *     Event callback that is invoked every time when CCNode enters the 'stage'.
         *     If the CCNode enters the 'stage' with a transition, this event is called when the transition starts.
         *     During onEnter you can't access a "sister/brother" node.
         *     If you override onEnter, you must call its parent's onEnter function with this._super().
         *
         * @function
         */
        public onEnter():void;

        
        /**
         *
         * callback that is called every time the cc.Node leaves the 'stage'.
         * If the cc.Node leaves the 'stage' with a transition, this callback is called when the transition finishes.
         * During onExit you can't access a sibling node.
         * If you override onExit, you shall call its parent's onExit with this._super().
         *
         * @function
         */
        public onExit():void;

        /**
         *
         *     Event callback that is invoked when the CCNode enters in the 'stage'.
         *     If the CCNode enters the 'stage' with a transition, this event is called when the transition finishes.
         *     If you override onEnterTransitionDidFinish, you shall call its parent's onEnterTransitionDidFinish with this._super()
         *
         * @function
         */
        public onEnterTransitionDidFinish():void;

        /**
         * callback that is called every time the cc.Node leaves the 'stage'.
         * If the cc.Node leaves the 'stage' with a transition, this callback is called when the transition starts.
         * If you override onExitTransitionDidStart, you shall call its parent's onExitTransitionDidStart with this._super()
         * @function
         */
        public onExitTransitionDidStart():void;

        /**
         * Stops all running actions and schedulers
         * @function
         */
        public cleanup():void;

        /**
         * Schedules a custom selector.
         * If the selector is already scheduled, then the interval parameter will be updated without scheduling it again.
         * @function
         * @param {function} callback A function wrapped as a selector
         * @param {Number} interval  Tick interval in seconds. 0 means tick every frame. If interval = 0, it's recommended to use scheduleUpdate() instead.
         * @param {Number} repeat    The selector will be executed (repeat + 1) times, you can use kCCRepeatForever for tick infinitely.
         * @param {Number} delay     The amount of time that the first tick will wait before execution.
         * @param {String} key The only string identifying the callback
         */
        public schedule(callback:(arg?:any) => void, interval?:number, repeat?:number, delay?:number):void;

        
        /**
         * Schedules a callback function that runs only once, with a delay of 0 or larger
         * @function
         * @see cc.Node#schedule
         * @param {function} callback  A function wrapped as a selector
         * @param {Number} delay  The amount of time that the first tick will wait before execution.
         * @param {String} key The only string identifying the callback
         */
        public scheduleOnce(callback:(arg?:any) => void, delay?:number, key?:string):void;

        /**
         *
         * schedules the "update" callback function with a custom priority.
         * This callback function will be called every frame.
         * Scheduled callback functions with a lower priority will be called before the ones that have a higher value.
         * Only one "update" callback function could be scheduled per node (You can't have 2 'update' callback functions).
         *
         * @function
         * @param {Number} priority
         */
        public scheduleUpdateWithPriority(priority:number):void;

        /**
         * Unschedules the "update" method.
         * @function
         * @see cc.Node#scheduleUpdate
         */
        public unscheduleUpdate():void;

        /**
         * schedules the "update" method.
         * It will use the order number 0. This method will be called every frame.
         * Scheduled methods with a lower order value will be called before the ones that have a higher order value.
         * Only one "update" method could be scheduled per node.
         * @function
         */
        public scheduleUpdate():void;

        /**
         * unschedules a custom callback function.
         * @function
         * @see cc.Node#schedule
         * @param {function} callback_fn  A function wrapped as a selector
         */
        public unschedule(callback_fn:(arg?:any) => void):void;

        /**
         * unschedule all scheduled callback functions: custom callback functions, and the 'update' callback function.
         * Actions are not affected by this method.
         * @function
         */
        public unscheduleAllCallbacks():void;

        /**
         *
         *     Changes the position (x,y) of the node in cocos2d coordinates.
         *     The original point (0,0) is at the left-bottom corner of screen.
         *     Usually we use cc.p(x,y) to compose CCPoint object.
         *     and Passing two numbers (x,y) is more efficient than passing CCPoint object.
         *
         * @function
         * @param {ctype.value_type<cc.Point>} point The position (x,y) of the node in coordinates or the X coordinate for position
         * @param {Number} [y] The Y coordinate for position
         * @example
         *    var size = cc.winSize;
         *    node.setPosition(size.width/2, size.height/2);
         */
        public setPosition(x:ctype.value_type<cc.Point>|number, y?:number):void;

        /**
         *
         *     Sets the untransformed size of the node.
         *
         *     The contentSize remains the same no matter the node is scaled or rotated.
         *     All nodes has a size. Layer and Scene has the same size of the screen.
         *
         * @function
         * @param {Number} size The untransformed size's width of the node.
         * @param {Number} [height] The untransformed size's height of the node.
         */
        public setContentSize(size:Size|number, height?:number):void;
    

        /**
         *
         *     Sets the anchor point in percent.
         *
         *     anchor point is the point around which all transformations and positioning manipulations take place.
         *     It's like a pin in the node where it is "attached" to its parent.
         *     The anchorPoint is normalized, like a percentage. (0,0) means the bottom-left corner and (1,1) means the top-right corner.
         *     But you can use values higher than (1,1) and lower than (0,0) too.
         *     The default anchor point is (0.5,0.5), so it starts at the center of the node.
         *
         * @function
         * @param {ctype.value_type<cc.Point>} point The anchor point of node or The x axis anchor of node.
         * @param {Number} [y] The y axis anchor of node.
         */
        public setAnchorPoint(point:Point|number, y?:number):void;

    
        /**
         * Sets the color of Node.
         * When color doesn't include opacity value like cc.color(128,128,128), this function only change the color.
         * When color include opacity like cc.color(128,128,128,100), then this function will change the color and the opacity.
         * @function
         * @param {ctype.value_type<cc.Color>} color The new color given
         */
        public setColor(color:ctype.value_type<cc.Color>):void;

        /**
         * Converts a Point to world space coordinates. The result is in Points.
         * @function
         * @param {ctype.value_type<cc.Point>} nodePoint
         * @return {ctype.value_type<cc.Point>}
         */
        public convertToWorldSpace(nodePoint?:ctype.value_type<cc.Point>):ctype.value_type<cc.Point>;

        
        /**
         * Converts a local Point to world space coordinates.The result is in Points.
         * treating the returned/received node point as anchor relative.
         * @function
         * @param {ctype.value_type<cc.Point>} nodePoint
         * @return {ctype.value_type<cc.Point>}
         */
        public convertToWorldSpaceAR(nodePoint?:ctype.value_type<cc.Point>):ctype.value_type<cc.Point>;

        /**
         *Sets the additional transform.
         *  The additional transform will be concatenated at the end of getNodeToParentTransform.
         *  It could be used to simulate `parent-child` relationship between two nodes (e.g. one is in BatchNode, another isn't).
         *
         *  @function
         *  @param {cc.AffineTransform} xform  The additional transform
         *  @example
         * // create a batchNode
         * var batch = new cc.SpriteBatchNode("Icon-114.png");
         * this.addChild(batch);
         *
         * // create two sprites, spriteA will be added to batchNode, they are using different textures.
         * var spriteA = new cc.Sprite(batch->getTexture());
         * var spriteB = new cc.Sprite("Icon-72.png");
         *
         * batch.addChild(spriteA);
         *
         * // We can't make spriteB as spriteA's child since they use different textures. So just add it to layer.
         * // But we want to simulate `parent-child` relationship for these two node.
         * this.addChild(spriteB);
         *
         * //position
         * spriteA.setPosition(ccp(200, 200));
         *
         * // Gets the spriteA's transform.
         * var t = spriteA.getNodeToParentTransform();
         *
         * // Sets the additional transform to spriteB, spriteB's position will based on its pseudo parent i.e. spriteA.
         * spriteB.setAdditionalTransform(t);
         *
         * //scale
         * spriteA.setScale(2);
         *
         * // Gets the spriteA's transform.
         * t = spriteA.getNodeToParentTransform();
         *
         * // Sets the additional transform to spriteB, spriteB's scale will based on its pseudo parent i.e. spriteA.
         * spriteB.setAdditionalTransform(t);
         *
         * //rotation
         * spriteA.setRotation(20);
         *
         * // Gets the spriteA's transform.
         * t = spriteA.getNodeToParentTransform();
         *
         * // Sets the additional transform to spriteB, spriteB's rotation will based on its pseudo parent i.e. spriteA.
         * spriteB.setAdditionalTransform(t);
         */
        public setAdditionalTransform(xform:AffineTransform):void;
        
        /**
         * 
         * @method addOnExitCallback
         * @param {() => void} _callback
         */
        public addOnExitCallback (
            _callback : () => void 
        ) : void;

        /**
         * 
         * @method addOnEnterCallback
         * @param {() => void} _callback
         */
        public addOnEnterCallback (
            _callback : () => void 
        ) : void;

        // *****************************************************************
        // *****************************************************************
        // *****************************************************************
        // *****************************************************************
        // *****************************************************************
        
        
        /**
         *  Gets the x coordinate of the node in its parent's coordinate system.
         * return The x coordinate of the node.
         * @method getPositionX
         * @return {number}
         */
        public x : number;
        /**
         *  Gets the y coordinate of the node in its parent's coordinate system.
         * return The y coordinate of the node.
         * @method getPositionY
         * @return {number}
         */
        public y : number;
        /* Can't find native property _getWidth implementation of cc::Node. using any type */
        public width : any;
        /* Can't find native property _getHeight implementation of cc::Node. using any type */
        public height : any;
        /* Can't find native property _getAnchorX implementation of cc::Node. using any type */
        public anchorX : any;
        /* Can't find native property _getAnchorY implementation of cc::Node. using any type */
        public anchorY : any;
        /**
         * Returns the X skew angle of the node in degrees.
         * see `setSkewX(float)`
         * return The X skew angle of the node in degrees.
         * @method getSkewX
         * @return {number}
         */
        public skewX : number;
        /**
         * Returns the Y skew angle of the node in degrees.
         * see `setSkewY(float)`
         * return The Y skew angle of the node in degrees.
         * @method getSkewY
         * @return {number}
         */
        public skewY : number;
        /**
         * Gets the local Z order of this node.
         * see `setLocalZOrder(int)`
         * return The local (relative to its siblings) Z order.
         * @method getLocalZOrder
         * @return {number}
         */
        public zIndex : number;
        /**
         * Gets position Z coordinate of this node.
         * see setPositionZ(float)
         * return The position Z coordinate of this node.
         * js getVertexZ
         * @method getPositionZ
         * @return {number}
         */
        public vertexZ : number;
        /**
         * Returns the rotation of the node in degrees.
         * see `setRotation(float)`
         * return The rotation of the node in degrees.
         * @method getRotation
         * @return {number}
         */
        public rotation : number;
        /**
         * Gets the X rotation (angle) of the node in degrees which performs a horizontal rotation skew.
         * see `setRotationSkewX(float)`
         * return The X rotation in degrees.
         * js getRotationX 
         * @method getRotationSkewX
         * @return {number}
         */
        public rotationX : number;
        /**
         * Gets the Y rotation (angle) of the node in degrees which performs a vertical rotational skew.
         * see `setRotationSkewY(float)`
         * return The Y rotation in degrees.
         * js getRotationY
         * @method getRotationSkewY
         * @return {number}
         */
        public rotationY : number;
        /**
         * Gets the scale factor of the node,  when X and Y have the same scale factor.
         * warning Assert when `_scaleX != _scaleY`
         * see setScale(float)
         * return The scale factor of the node.
         * @method getScale
         * @return {number}
         */
        public scale : number;
        /**
         * Returns the scale factor on X axis of this node
         * see setScaleX(float)
         * return The scale factor on X axis.
         * @method getScaleX
         * @return {number}
         */
        public scaleX : number;
        /**
         * Returns the scale factor on Y axis of this node
         * see `setScaleY(float)`
         * return The scale factor on Y axis.
         * @method getScaleY
         * @return {number}
         */
        public scaleY : number;
        /**
         * 
         * @method getChildren
         * @return {cc.Node[]}
         */
        public readonly children : cc.Node[];
        /**
         * Returns the amount of children.
         * return The amount of children.
         * @method getChildrenCount
         * @return {number}
         */
        public readonly childrenCount : number;
        /**
         * 
         * @method getParent
         * @return {cc.Node}
         */
        public parent : cc.Node;
        /**
         * Determines if the node is visible.
         * see `setVisible(bool)`
         * return true if the node is visible, false if the node is hidden.
         * @method isVisible
         * @return {boolean}
         */
        public visible : boolean;
        /**
         * Returns whether or not the node is "running".
         * If the node is running it will accept event callbacks like onEnter(), onExit(), update().
         * return Whether or not the node is running.
         * @method isRunning
         * @return {boolean}
         */
        public readonly running : boolean;
        /**
         * Gets whether the anchor point will be (0,0) when you position this node.
         * see `ignoreAnchorPointForPosition(bool)`
         * return true if the anchor point will be (0,0) when you position this node.
         * @method isIgnoreAnchorPointForPosition
         * @return {boolean}
         */
        public ignoreAnchor : boolean;
        /**
         * 
         * @method getActionManager
         * @return {cc.ActionManager}
         */
        public actionManager : cc.ActionManager;
        /**
         * 
         * @method getScheduler
         * @return {cc.Scheduler}
         */
        public scheduler : cc.Scheduler;
        /**
         * / @{/ @name GLProgram
         * Return the GLProgram (shader) currently used for this node.
         * return The GLProgram (shader) currently used for this node.
         * @method getGLProgram
         * @return {cc.GLProgram}
         */
        public shaderProgram : cc.GLProgram;
        /* Can't find native property getGLServerState implementation of cc::Node. using any type */
        public glServerState : any;
        /**
         * Returns a tag that is used to identify the node easily.
         * return An integer that identifies the node.
         * Please use `getTag()` instead.
         * @method getTag
         * @return {number}
         */
        public tag : number;
        /* Can't find native property getUserObject implementation of cc::Node. using any type */
        public userObject : any;
        /* Can't find native property getArrivalOrder implementation of cc::Node. using any type */
        public arrivalOrder : any;
        /**
         * / @} end of component functions
         * @method getOpacity
         * @return {number}
         */
        public opacity : number;
        /**
         * 
         * @method isOpacityModifyRGB
         * @return {boolean}
         */
        public opacityModifyRGB : boolean;
        /**
         * 
         * @method isCascadeOpacityEnabled
         * @return {boolean}
         */
        public cascadeOpacity : boolean;
        /**
         * 
         * @method getColor
         * @return {ctype.value_type<cc.Color>}
         */
        public color : ctype.value_type<cc.Color>;
        /**
         * 
         * @method isCascadeColorEnabled
         * @return {boolean}
         */
        public cascadeColor : boolean;


        /**
         * Adds a child to the container with a local z-order.<br>
         * -- If the child is added to a 'running' node, then 'onEnter' and 'onEnterTransitionDidFinish' will be called immediately.<br>
         * -- param child     A child node.<br>
         * -- param localZOrder    Z order for drawing priority. Please refer to `setLocalZOrder(int)`.
         * @method addChild
         * @param {(cc.Node)} _child
         * @param {(number)} _localZOrder?
         * @param {(number) | (string)} _tag | name?
         */
        public addChild (
            _child : (cc.Node), 
            _localZOrder? : (number), 
            _tag_name? : (number) | (string)
        ) : void;

        /**
         * Removes a component by its pointer.<br>
         * -- param component A given component.<br>
         * -- return True if removed success.
         * @method removeComponent
         * @param {(cc.Component) | (string)} _component | name
         * @return {boolean}
         */
        public removeComponent (
            _component_name : (cc.Component) | (string)
        ) : boolean;

        /**
         * 
         * @method setPhysicsBody
         * @param {cc.Component} _physicsBody
         */
        public setPhysicsBody (
            _physicsBody : cc.Component 
        ) : void;

        /**
         * / @{/ @name GLProgram<br>
         * -- Return the GLProgram (shader) currently used for this node.<br>
         * -- return The GLProgram (shader) currently used for this node.
         * @method getGLProgram
         * @return {cc.GLProgram}
         */
        public getShaderProgram (
        ) : cc.GLProgram;

        /**
         * Gets the description string. It makes debugging easier.<br>
         * -- return A string<br>
         * -- js NA<br>
         * -- lua NA
         * @method getDescription
         * @return {string}
         */
        public getDescription (
        ) : string;

        /**
         * 
         * @method setOpacityModifyRGB
         * @param {boolean} _value
         */
        public setOpacityModifyRGB (
            _value : boolean 
        ) : void;

        /**
         * 
         * @method setCascadeOpacityEnabled
         * @param {boolean} _cascadeOpacityEnabled
         */
        public setCascadeOpacityEnabled (
            _cascadeOpacityEnabled : boolean 
        ) : void;

        /**
         * 
         * @method getChildren
         * @return {cc.Node[]}
         */
        public getChildren (
        ) : cc.Node[];

        /**
         * 
         * @method setOnExitCallback
         * @param {() => void} _callback
         */
        public setOnExitCallback (
            _callback : () => void 
        ) : void;

        /**
         * Gets whether the anchor point will be (0,0) when you position this node.<br>
         * -- see `ignoreAnchorPointForPosition(bool)`<br>
         * -- return true if the anchor point will be (0,0) when you position this node.
         * @method isIgnoreAnchorPointForPosition
         * @return {boolean}
         */
        public isIgnoreAnchorPointForPosition (
        ) : boolean;

        /**
         * Gets a child from the container with its name.<br>
         * -- param name   An identifier to find the child node.<br>
         * -- return a Node object whose name equals to the input parameter.<br>
         * -- since v3.2
         * @method getChildByName
         * @param {string} _name
         * @return {cc.Node}
         */
        public getChildByName (
            _name : string 
        ) : cc.Node;

        /**
         * 
         * @method updateDisplayedOpacity
         * @param {number} _parentOpacity
         */
        public updateDisplayedOpacity (
            _parentOpacity : number 
        ) : void;

        /**
         * 
         * @method init
         * @return {boolean}
         */
        public init (
            // PURE MANUAL HACK
            ...args : any[]
            // PURE MANUAL HACK
        ) : boolean;

        /**
         *  get & set camera mask, the node is visible by the camera whose camera flag & node's camera mask is true 
         * @method getCameraMask
         * @return {number}
         */
        public getCameraMask (
        ) : number;

        /**
         * Sets the rotation (angle) of the node in degrees.<br>
         * -- 0 is the default rotation angle.<br>
         * -- Positive values rotate node clockwise, and negative values for anti-clockwise.<br>
         * -- param rotation     The rotation of the node in degrees.
         * @method setRotation
         * @param {number} _rotation
         */
        public setRotation (
            _rotation : number 
        ) : void;

        /**
         * Changes the scale factor on Z axis of this node<br>
         * -- The Default value is 1.0 if you haven't changed it before.<br>
         * -- param scaleZ   The scale factor on Z axis.<br>
         * -- warning The physics body doesn't support this.
         * @method setScaleZ
         * @param {number} _scaleZ
         */
        public setScaleZ (
            _scaleZ : number 
        ) : void;

        /**
         * Sets the scale (y) of the node.<br>
         * -- It is a scaling factor that multiplies the height of the node and its children.<br>
         * -- param scaleY   The scale factor on Y axis.<br>
         * -- warning The physics body doesn't support this.
         * @method setScaleY
         * @param {number} _scaleY
         */
        public setScaleY (
            _scaleY : number 
        ) : void;

        /**
         * Sets the scale (x) of the node.<br>
         * -- It is a scaling factor that multiplies the width of the node and its children.<br>
         * -- param scaleX   The scale factor on X axis.<br>
         * -- warning The physics body doesn't support this.
         * @method setScaleX
         * @param {number} _scaleX
         */
        public setScaleX (
            _scaleX : number 
        ) : void;

        /**
         * 
         * @method getColor
         * @return {ctype.value_type<cc.Color>}
         */
        public getColor (
        ) : ctype.value_type<cc.Color>;

        /**
         * 
         * @method setonEnterTransitionDidFinishCallback
         * @param {() => void} _callback
         */
        public setonEnterTransitionDidFinishCallback (
            _callback : () => void 
        ) : void;

        /**
         * Removes all components
         * @method removeAllComponents
         */
        public removeAllComponents (
        ) : void;

        /**
         * / @} end of component functions
         * @method getOpacity
         * @return {number}
         */
        public getOpacity (
        ) : number;

        /**
         * 
         * @method setCameraMask
         * @param {number} _mask
         * @param {boolean} _applyChildren
         */
        public setCameraMask (
            _mask : number, 
            _applyChildren : boolean 
        ) : void;

        /**
         * Returns a tag that is used to identify the node easily.<br>
         * -- return An integer that identifies the node.<br>
         * -- Please use `getTag()` instead.
         * @method getTag
         * @return {number}
         */
        public getTag (
        ) : number;

        /**
         * 
         * @method getonEnterTransitionDidFinishCallback
         * @return {() => void}
         */
        public getonEnterTransitionDidFinishCallback (
        ) : () => void;

        /**
         * 
         * @method getNodeToWorldAffineTransform
         * @return {cc.AffineTransform}
         */
        public getNodeToWorldTransform (
        ) : cc.AffineTransform;

        /**
         * Returns the position (X,Y,Z) in its parent's coordinate system.<br>
         * -- return The position (X, Y, and Z) in its parent's coordinate system.<br>
         * -- js NA
         * @method getPosition3D
         * @return {ctype.value_type<cc.Vec3>}
         */
        public getPosition3D (
        ) : ctype.value_type<cc.Vec3>;

        /**
         * Removes a child from the container. It will also cleanup all running actions depending on the cleanup parameter.<br>
         * -- param child     The child node which will be removed.<br>
         * -- param cleanup   True if all running actions and callbacks on the child node will be cleanup, false otherwise.
         * @method removeChild
         * @param {cc.Node} _child
         * @param {boolean} _cleanup
         */
        public removeChild (
            _child : cc.Node, 
            _cleanup : boolean 
        ) : void;

        /**
         *  Returns the Scene that contains the Node.<br>
         * -- It returns `nullptr` if the node doesn't belong to any Scene.<br>
         * -- This function recursively calls parent->getScene() until parent is a Scene object. The results are not cached. It is that the user caches the results in case this functions is being used inside a loop.<br>
         * -- return The Scene that contains the node.
         * @method getScene
         * @return {cc.Scene}
         */
        public getScene (
        ) : cc.Scene;

        /**
         *  Get the event dispatcher of scene.<br>
         * -- return The event dispatcher of scene.
         * @method getEventDispatcher
         * @return {cc.EventDispatcher}
         */
        public getEventDispatcher (
        ) : cc.EventDispatcher;

        /**
         * Changes the X skew angle of the node in degrees.<br>
         * -- The difference between `setRotationalSkew()` and `setSkew()` is that the first one simulate Flash's skew functionality<br>
         * -- while the second one uses the real skew function.<br>
         * -- This angle describes the shear distortion in the X direction.<br>
         * -- Thus, it is the angle between the Y coordinate and the left edge of the shape<br>
         * -- The default skewX angle is 0. Positive values distort the node in a CW direction.<br>
         * -- param skewX The X skew angle of the node in degrees.<br>
         * -- warning The physics body doesn't support this.
         * @method setSkewX
         * @param {number} _skewX
         */
        public setSkewX (
            _skewX : number 
        ) : void;

        /**
         * Set the GLProgramState for this node.<br>
         * -- param glProgramState The GLProgramState for this node.
         * @method setGLProgramState
         * @param {cc.GLProgramState} _glProgramState
         */
        public setGLProgramState (
            _glProgramState : cc.GLProgramState 
        ) : void;

        /**
         * 
         * @method setOnEnterCallback
         * @param {() => void} _callback
         */
        public setOnEnterCallback (
            _callback : () => void 
        ) : void;

        /**
         * Removes all actions from the running action list by its flags.<br>
         * -- param flags   A flag field that removes actions based on bitwise AND.
         * @method stopActionsByFlags
         * @param {number} _flags
         */
        public stopActionsByFlags (
            _flags : number 
        ) : void;

        /**
         *  Sets the position (x,y) using values between 0 and 1.<br>
         * -- The positions in pixels is calculated like the following:<br>
         * -- code pseudo code<br>
         * -- void setNormalizedPosition(Vec2 pos) {<br>
         * -- Size s = getParent()->getContentSize();<br>
         * -- _position = pos * s;<br>
         * -- }<br>
         * -- endcode<br>
         * -- param position The normalized position (x,y) of the node, using value between 0 and 1.
         * @method setNormalizedPosition
         * @param {ctype.value_type<cc.Point>} _position
         */
        public setNormalizedPosition (
            _position : ctype.value_type<cc.Point> 
        ) : void;

        /**
         * 
         * @method setonExitTransitionDidStartCallback
         * @param {() => void} _callback
         */
        public setonExitTransitionDidStartCallback (
            _callback : () => void 
        ) : void;

        /**
         * convenience methods which take a Touch instead of Vec2.<br>
         * -- param touch A given touch.<br>
         * -- return A point in world space coordinates.
         * @method convertTouchToNodeSpace
         * @param {cc.Touch} _touch
         * @return {ctype.value_type<cc.Point>}
         */
        public convertTouchToNodeSpace (
            _touch : cc.Touch 
        ) : ctype.value_type<cc.Point>;

        /**
         * Removes all children from the container, and do a cleanup to all running actions depending on the cleanup parameter.<br>
         * -- param cleanup   True if all running actions on all children nodes should be cleanup, false otherwise.<br>
         * -- js removeAllChildren<br>
         * -- lua removeAllChildren
         * @method removeAllChildrenWithCleanup
         * @param {(boolean)} _cleanup?
         */
        public removeAllChildren (
            _cleanup? : (boolean)
        ) : void;

        /**
         * Gets the X rotation (angle) of the node in degrees which performs a horizontal rotation skew.<br>
         * -- see `setRotationSkewX(float)`<br>
         * -- return The X rotation in degrees.<br>
         * -- js getRotationX 
         * @method getRotationSkewX
         * @return {number}
         */
        public getRotationX (
        ) : number;

        /**
         * Gets the Y rotation (angle) of the node in degrees which performs a vertical rotational skew.<br>
         * -- see `setRotationSkewY(float)`<br>
         * -- return The Y rotation in degrees.<br>
         * -- js getRotationY
         * @method getRotationSkewY
         * @return {number}
         */
        public getRotationY (
        ) : number;

        /**
         * Returns the world affine transform matrix. The matrix is in Pixels.<br>
         * -- return transformation matrix, in pixels.
         * @method getNodeToWorldTransform
         * @return {ctype.value_type<cc.Mat4>}
         */
        public getNodeToWorldTransform3D (
        ) : ctype.value_type<cc.Mat4>;

        /**
         * 
         * @method isCascadeOpacityEnabled
         * @return {boolean}
         */
        public isCascadeOpacityEnabled (
        ) : boolean;

        /**
         * Sets the parent node.<br>
         * -- param parent    A pointer to the parent node.
         * @method setParent
         * @param {cc.Node} _parent
         */
        public setParent (
            _parent : cc.Node 
        ) : void;

        /**
         *  Returns a string that is used to identify the node.<br>
         * -- return A string that identifies the node.<br>
         * -- since v3.2
         * @method getName
         * @return {string}
         */
        public getName (
        ) : string;

        /**
         * Returns the rotation (X,Y,Z) in degrees.<br>
         * -- return The rotation of the node in 3d.<br>
         * -- js NA
         * @method getRotation3D
         * @return {ctype.value_type<cc.Vec3>}
         */
        public getRotation3D (
        ) : ctype.value_type<cc.Vec3>;

        /**
         * Returns the affine transform matrix that transform the node's (local) space coordinates into the parent's space coordinates.<br>
         * -- The matrix is in Pixels.<br>
         * -- Note: If ancestor is not a valid ancestor of the node, the API would return the same value as @see getNodeToWorldAffineTransform<br>
         * -- param ancestor The parent's node pointer.<br>
         * -- since v3.7<br>
         * -- return The affine transformation matrix.
         * @method getNodeToParentAffineTransform
         * @param {(cc.Node)} _ancestor?
         * @return {cc.AffineTransform}
         */
        public getNodeToParentTransform (
            _ancestor? : (cc.Node)
        ) : cc.AffineTransform;

        /**
         * converts a Touch (world coordinates) into a local coordinate. This method is AR (Anchor Relative).<br>
         * -- param touch A given touch.<br>
         * -- return A point in world space coordinates, anchor relative.
         * @method convertTouchToNodeSpaceAR
         * @param {cc.Touch} _touch
         * @return {ctype.value_type<cc.Point>}
         */
        public convertTouchToNodeSpaceAR (
            _touch : cc.Touch 
        ) : ctype.value_type<cc.Point>;

        /**
         * 
         * @method getOnEnterCallback
         * @return {() => void}
         */
        public getOnEnterCallback (
        ) : () => void;

        /**
         * 
         * @method isOpacityModifyRGB
         * @return {boolean}
         */
        public isOpacityModifyRGB (
        ) : boolean;

        /**
         * Removes an action from the running action list by its tag.<br>
         * -- param tag   A tag that indicates the action to be removed.
         * @method stopActionByTag
         * @param {number} _tag
         */
        public stopActionByTag (
            _tag : number 
        ) : void;

        /**
         * Reorders a child according to a new z value.<br>
         * -- param child     An already added child node. It MUST be already added.<br>
         * -- param localZOrder Z order for drawing priority. Please refer to setLocalZOrder(int).
         * @method reorderChild
         * @param {cc.Node} _child
         * @param {number} _localZOrder
         */
        public reorderChild (
            _child : cc.Node, 
            _localZOrder : number 
        ) : void;

        /**
         * Sets whether the anchor point will be (0,0) when you position this node.<br>
         * -- This is an internal method, only used by Layer and Scene. Don't call it outside framework.<br>
         * -- The default value is false, while in Layer and Scene are true.<br>
         * -- param ignore    true if anchor point will be (0,0) when you position this node.<br>
         * -- todo This method should be renamed as setIgnoreAnchorPointForPosition(bool) or something with "set".
         * @method ignoreAnchorPointForPosition
         * @param {boolean} _ignore
         */
        public ignoreAnchorPointForPosition (
            _ignore : boolean 
        ) : void;

        /**
         * 
         * @method addOnExitCallback
         * @param {() => void} _callback
         */
        public addOnExitCallback (
            _callback : () => void 
        ) : void;

        /**
         * Changes the Y skew angle of the node in degrees.<br>
         * -- The difference between `setRotationalSkew()` and `setSkew()` is that the first one simulate Flash's skew functionality<br>
         * -- while the second one uses the real skew function.<br>
         * -- This angle describes the shear distortion in the Y direction.<br>
         * -- Thus, it is the angle between the X coordinate and the bottom edge of the shape.<br>
         * -- The default skewY angle is 0. Positive values distort the node in a CCW direction.<br>
         * -- param skewY    The Y skew angle of the node in degrees.<br>
         * -- warning The physics body doesn't support this.
         * @method setSkewY
         * @param {number} _skewY
         */
        public setSkewY (
            _skewY : number 
        ) : void;

        /**
         * Sets the rotation (X,Y,Z) in degrees.<br>
         * -- Useful for 3d rotations.<br>
         * -- warning The physics body doesn't support this.<br>
         * -- param rotation The rotation of the node in 3d.<br>
         * -- js NA
         * @method setRotation3D
         * @param {ctype.value_type<cc.Vec3>} _rotation
         */
        public setRotation3D (
            _rotation : ctype.value_type<cc.Vec3> 
        ) : void;

        /**
         * Gets/Sets x or y coordinate individually for position.<br>
         * -- These methods are used in Lua and Javascript Bindings<br>
         * -- Sets the x coordinate of the node in its parent's coordinate system.<br>
         * -- param x The x coordinate of the node.
         * @method setPositionX
         * @param {number} _x
         */
        public setPositionX (
            _x : number 
        ) : void;

        /**
         * Sets the transformation matrix manually.<br>
         * -- param transform A given transformation matrix.
         * @method setNodeToParentTransform
         * @param {ctype.value_type<cc.Mat4>} _transform
         */
        public setNodeToParentTransform (
            _transform : ctype.value_type<cc.Mat4> 
        ) : void;

        /**
         * Returns the anchor point in percent.<br>
         * -- see `setAnchorPoint(const Vec2&)`<br>
         * -- return The anchor point of node.
         * @method getAnchorPoint
         * @return {ctype.value_type<cc.Point>}
         */
        public getAnchorPoint (
        ) : ctype.value_type<cc.Point>;

        /**
         * Returns the numbers of actions that are running plus the ones that are schedule to run (actions in actionsToAdd and actions arrays).<br>
         * -- Composable actions are counted as 1 action. Example:<br>
         * -- If you are running 1 Sequence of 7 actions, it will return 1.<br>
         * -- If you are running 7 Sequences of 2 actions, it will return 7.<br>
         * -- todo Rename to getNumberOfRunningActions()<br>
         * -- return The number of actions that are running plus the ones that are schedule to run.
         * @method getNumberOfRunningActions
         * @return {number}
         */
        public getNumberOfRunningActions (
        ) : number;

        /**
         * Calls children's updateTransform() method recursively.<br>
         * -- This method is moved from Sprite, so it's no longer specific to Sprite.<br>
         * -- As the result, you apply SpriteBatchNode's optimization on your customed Node.<br>
         * -- e.g., `batchNode->addChild(myCustomNode)`, while you can only addChild(sprite) before.
         * @method updateTransform
         */
        public updateTransform (
        ) : void;

        /**
         * Determines if the node is visible.<br>
         * -- see `setVisible(bool)`<br>
         * -- return true if the node is visible, false if the node is hidden.
         * @method isVisible
         * @return {boolean}
         */
        public isVisible (
        ) : boolean;

        /**
         * param discardDraw output `true` if element is too small or too glassy to be visible<br>
         * -- param discardVisit output `true` if we can ignore rendering all children elements
         * @method discard
         * @param {boolean} _discardDraw
         * @param {boolean} _discardVisit
         */
        public discard (
            _discardDraw : boolean, 
            _discardVisit : boolean 
        ) : void;

        /**
         * Returns the amount of children.<br>
         * -- return The amount of children.
         * @method getChildrenCount
         * @return {number}
         */
        public getChildrenCount (
        ) : number;

        /**
         * Returns the matrix that transform the node's (local) space coordinates into the parent's space coordinates.<br>
         * -- The matrix is in Pixels.<br>
         * -- Note: If ancestor is not a valid ancestor of the node, the API would return the same value as @see getNodeToWorldTransform<br>
         * -- param ancestor The parent's node pointer.<br>
         * -- since v3.7<br>
         * -- return The transformation matrix.
         * @method getNodeToParentTransform
         * @param {(cc.Node)} _ancestor?
         * @return {ctype.value_type<cc.Mat4>}
         */
        public getNodeToParentTransform3D (
            _ancestor? : (cc.Node)
        ) : ctype.value_type<cc.Mat4>;

        /**
         * Converts a Vec2 to node (local) space coordinates. The result is in Points.<br>
         * -- treating the returned/received node point as anchor relative.<br>
         * -- param worldPoint A given coordinate.<br>
         * -- return A point in node (local) space coordinates, anchor relative.
         * @method convertToNodeSpaceAR
         * @param {ctype.value_type<cc.Point>} _worldPoint
         * @return {ctype.value_type<cc.Point>}
         */
        public convertToNodeSpaceAR (
            _worldPoint : ctype.value_type<cc.Point> 
        ) : ctype.value_type<cc.Point>;

        /**
         * Adds a component.<br>
         * -- param component A given component.<br>
         * -- return True if added success.
         * @method addComponent
         * @param {cc.Component} _component
         * @return {boolean}
         */
        public addComponent (
            _component : cc.Component 
        ) : boolean;

        /**
         * Executes an action, and returns the action that is executed.<br>
         * -- This node becomes the action's target. Refer to Action::getTarget().<br>
         * -- warning Actions don't retain their target.<br>
         * -- param action An Action pointer.
         * @method runAction
         * @param {cc.Action} _action
         * @return {cc.Action}
         */
        public runAction (
            _action : cc.Action 
        ) : cc.Action;

        /**
         * 
         * @method visit
         * @param {(cc.Renderer)} _renderer?
         * @param {(ctype.value_type<cc.Mat4>)} _parentTransform?
         * @param {(number)} _parentFlags?
         */
        public visit (
            _renderer? : (any), 
            _parentTransform? : (ctype.value_type<cc.Mat4>), 
            _parentFlags? : (number)
        ) : void;

        /**
         * Sets the shader program for this node<br>
         * -- Since v2.0, each rendering node must set its shader program.<br>
         * -- It should be set in initialize phase.<br>
         * -- code<br>
         * -- node->setGLrProgram(GLProgramCache::getInstance()->getProgram(GLProgram::SHADER_NAME_POSITION_TEXTURE_COLOR));<br>
         * -- endcode<br>
         * -- param glprogram The shader program.
         * @method setGLProgram
         * @param {cc.GLProgram} _glprogram
         */
        public setShaderProgram (
            _glprogram : cc.GLProgram 
        ) : void;

        /**
         * Returns the rotation of the node in degrees.<br>
         * -- see `setRotation(float)`<br>
         * -- return The rotation of the node in degrees.
         * @method getRotation
         * @return {number}
         */
        public getRotation (
        ) : number;

        /**
         * 
         * @method getPhysicsBody
         * @return {cc.PhysicsBody}
         */
        public getPhysicsBody (
        ) : cc.PhysicsBody;

        /**
         * Returns the anchorPoint in absolute pixels.<br>
         * -- warning You can only read it. If you wish to modify it, use anchorPoint instead.<br>
         * -- see `getAnchorPoint()`<br>
         * -- return The anchor point in absolute pixels.
         * @method getAnchorPointInPoints
         * @return {ctype.value_type<cc.Point>}
         */
        public getAnchorPointInPoints (
        ) : ctype.value_type<cc.Point>;

        /**
         * Return the rotation by quaternion, Note that when _rotationZ_X == _rotationZ_Y, the returned quaternion equals to RotationZ_X * RotationY * RotationX,<br>
         * -- it equals to RotationY * RotationX otherwise.<br>
         * -- return The rotation in quaternion.<br>
         * -- js NA
         * @method getRotationQuat
         * @return {cc.Quaternion}
         */
        public getRotationQuat (
        ) : cc.Quaternion;

        /**
         * Removes a child from the container by tag value. It will also cleanup all running actions depending on the cleanup parameter.<br>
         * -- param name       A string that identifies a child node.<br>
         * -- param cleanup   True if all running actions and callbacks on the child node will be cleanup, false otherwise.
         * @method removeChildByName
         * @param {string} _name
         * @param {boolean} _cleanup
         */
        public removeChildByName (
            _name : string, 
            _cleanup : boolean 
        ) : void;

        /**
         * Sets the 'z' coordinate in the position. It is the OpenGL Z vertex value.<br>
         * -- The OpenGL depth buffer and depth testing are disabled by default. You need to turn them on.<br>
         * -- In order to use this property correctly.<br>
         * -- `setPositionZ()` also sets the `setGlobalZValue()` with the positionZ as value.<br>
         * -- see `setGlobalZValue()`<br>
         * -- param positionZ  OpenGL Z vertex of this node.<br>
         * -- js setVertexZ
         * @method setPositionZ
         * @param {number} _positionZ
         */
        public setVertexZ (
            _positionZ : number 
        ) : void;

        /**
         * Return the GLProgramState currently used for this node.<br>
         * -- return The GLProgramState currently used for this node.
         * @method getGLProgramState
         * @return {cc.GLProgramState}
         */
        public getGLProgramState (
        ) : cc.GLProgramState;

        /**
         * Sets a Scheduler object that is used to schedule all "updates" and timers.<br>
         * -- warning If you set a new Scheduler, then previously created timers/update are going to be removed.<br>
         * -- param scheduler     A Scheduler object that is used to schedule all "update" and timers.
         * @method setScheduler
         * @param {cc.Scheduler} _scheduler
         */
        public setScheduler (
            _scheduler : cc.Scheduler 
        ) : void;

        /**
         * Stops and removes all actions from the running action list .
         * @method stopAllActions
         */
        public stopAllActions (
        ) : void;

        /**
         * Returns the X skew angle of the node in degrees.<br>
         * -- see `setSkewX(float)`<br>
         * -- return The X skew angle of the node in degrees.
         * @method getSkewX
         * @return {number}
         */
        public getSkewX (
        ) : number;

        /**
         * Returns the Y skew angle of the node in degrees.<br>
         * -- see `setSkewY(float)`<br>
         * -- return The Y skew angle of the node in degrees.
         * @method getSkewY
         * @return {number}
         */
        public getSkewY (
        ) : number;

        /**
         * Checks whether a lambda function is scheduled.<br>
         * -- param key      key of the callback<br>
         * -- return Whether the lambda function selector is scheduled.<br>
         * -- js NA<br>
         * -- lua NA
         * @method isScheduled
         * @param {string} _key
         * @return {boolean}
         */
        public isScheduled (
            _key : string 
        ) : boolean;

        /**
         * 
         * @method getDisplayedColor
         * @return {ctype.value_type<cc.Color>}
         */
        public getDisplayedColor (
        ) : ctype.value_type<cc.Color>;

        /**
         * Gets an action from the running action list by its tag.<br>
         * -- see `setTag(int)`, `getTag()`.<br>
         * -- return The action object with the given tag.
         * @method getActionByTag
         * @param {number} _tag
         * @return {cc.Action}
         */
        public getActionByTag (
            _tag : number 
        ) : cc.Action;

        /**
         * Sets the X rotation (angle) of the node in degrees which performs a horizontal rotational skew.<br>
         * -- The difference between `setRotationalSkew()` and `setSkew()` is that the first one simulate Flash's skew functionality,<br>
         * -- while the second one uses the real skew function.<br>
         * -- 0 is the default rotation angle.<br>
         * -- Positive values rotate node clockwise, and negative values for anti-clockwise.<br>
         * -- param rotationX    The X rotation in degrees which performs a horizontal rotational skew.<br>
         * -- warning The physics body doesn't support this.<br>
         * -- js setRotationX
         * @method setRotationSkewX
         * @param {number} _rotationX
         */
        public setRotationX (
            _rotationX : number 
        ) : void;

        /**
         * Sets the Y rotation (angle) of the node in degrees which performs a vertical rotational skew.<br>
         * -- The difference between `setRotationalSkew()` and `setSkew()` is that the first one simulate Flash's skew functionality,<br>
         * -- while the second one uses the real skew function.<br>
         * -- 0 is the default rotation angle.<br>
         * -- Positive values rotate node clockwise, and negative values for anti-clockwise.<br>
         * -- param rotationY    The Y rotation in degrees.<br>
         * -- warning The physics body doesn't support this.<br>
         * -- js setRotationY
         * @method setRotationSkewY
         * @param {number} _rotationY
         */
        public setRotationY (
            _rotationY : number 
        ) : void;

        /**
         *  Changes the name that is used to identify the node easily.<br>
         * -- param name A string that identifies the node.<br>
         * -- since v3.2
         * @method setName
         * @param {string} _name
         */
        public setName (
            _name : string 
        ) : void;

        /**
         * 
         * @method getDisplayedOpacity
         * @return {number}
         */
        public getDisplayedOpacity (
        ) : number;

        /**
         * Gets the local Z order of this node.<br>
         * -- see `setLocalZOrder(int)`<br>
         * -- return The local (relative to its siblings) Z order.
         * @method getLocalZOrder
         * @return {number}
         */
        public getLocalZOrder (
        ) : number;

        /**
         * 
         * @method getScheduler
         * @return {cc.Scheduler}
         */
        public getScheduler (
        ) : cc.Scheduler;

        /**
         * Returns the arrival order, indicates which children is added previously.<br>
         * -- see `setOrderOfArrival(unsigned int)`<br>
         * -- return The arrival order.
         * @method getOrderOfArrival
         * @return {number}
         */
        public getOrderOfArrival (
        ) : number;

        /**
         * Sets the ActionManager object that is used by all actions.<br>
         * -- warning If you set a new ActionManager, then previously created actions will be removed.<br>
         * -- param actionManager     A ActionManager object that is used by all actions.
         * @method setActionManager
         * @param {cc.ActionManager} _actionManager
         */
        public setActionManager (
            _actionManager : cc.ActionManager 
        ) : void;

        /**
         * Gets position in a more efficient way, returns two number instead of a Vec2 object.<br>
         * -- see `setPosition(float, float)`<br>
         * -- In js,out value not return.<br>
         * -- param x To receive x coordinate for position.<br>
         * -- param y To receive y coordinate for position.
         * @method getPosition
         * @param {(number)} _x?
         * @param {(number)} _y?
         * @return {void | ctype.value_type<cc.Point>}
         */
        // PURE MANUAL HACK
        public getPosition (
            /*_x? : (number), 
            _y? : (number)*/
        ) : /*void |*/ ctype.value_type<cc.Point>;
        // PURE MANUAL HACK

        /**
         * Returns whether or not the node is "running".<br>
         * -- If the node is running it will accept event callbacks like onEnter(), onExit(), update().<br>
         * -- return Whether or not the node is running.
         * @method isRunning
         * @return {boolean}
         */
        public isRunning (
        ) : boolean;

        /**
         * 
         * @method getParent
         * @return {cc.Node}
         */
        public getParent (
        ) : cc.Node;

        /**
         * Returns the inverse world affine transform matrix. The matrix is in Pixels.<br>
         * -- return The transformation matrix.
         * @method getWorldToNodeTransform
         * @return {ctype.value_type<cc.Mat4>}
         */
        public getWorldToNodeTransform3D (
        ) : ctype.value_type<cc.Mat4>;

        /**
         *  Gets the y coordinate of the node in its parent's coordinate system.<br>
         * -- return The y coordinate of the node.
         * @method getPositionY
         * @return {number}
         */
        public getPositionY (
        ) : number;

        /**
         *  Gets the x coordinate of the node in its parent's coordinate system.<br>
         * -- return The x coordinate of the node.
         * @method getPositionX
         * @return {number}
         */
        public getPositionX (
        ) : number;

        /**
         * Removes a child from the container by tag value. It will also cleanup all running actions depending on the cleanup parameter.<br>
         * -- param tag       An integer number that identifies a child node.<br>
         * -- param cleanup   True if all running actions and callbacks on the child node will be cleanup, false otherwise.<br>
         * -- Please use `removeChildByName` instead.
         * @method removeChildByTag
         * @param {number} _tag
         * @param {boolean} _cleanup
         */
        public removeChildByTag (
            _tag : number, 
            _cleanup : boolean 
        ) : void;

        /**
         *  Sets the y coordinate of the node in its parent's coordinate system.<br>
         * -- param y The y coordinate of the node.
         * @method setPositionY
         * @param {number} _y
         */
        public setPositionY (
            _y : number 
        ) : void;

        /**
         * 
         * @method addOnEnterCallback
         * @param {() => void} _callback
         */
        public addOnEnterCallback (
            _callback : () => void 
        ) : void;

        /**
         * 
         * @method updateDisplayedColor
         * @param {ctype.value_type<cc.Color>} _parentColor
         */
        public updateDisplayedColor (
            _parentColor : ctype.value_type<cc.Color> 
        ) : void;

        /**
         * Sets whether the node is visible.<br>
         * -- The default value is true, a node is default to visible.<br>
         * -- param visible   true if the node is visible, false if the node is hidden.
         * @method setVisible
         * @param {boolean} _visible
         */
        public setVisible (
            _visible : boolean 
        ) : void;

        /**
         * 
         * @method getParentToNodeAffineTransform
         * @return {cc.AffineTransform}
         */
        public getParentToNodeTransform (
        ) : cc.AffineTransform;

        /**
         * Gets position Z coordinate of this node.<br>
         * -- see setPositionZ(float)<br>
         * -- return The position Z coordinate of this node.<br>
         * -- js getVertexZ
         * @method getPositionZ
         * @return {number}
         */
        public getVertexZ (
        ) : number;

        /**
         * Defines the oder in which the nodes are renderer.<br>
         * -- Nodes that have a Global Z Order lower, are renderer first.<br>
         * -- In case two or more nodes have the same Global Z Order, the oder is not guaranteed.<br>
         * -- The only exception if the Nodes have a Global Z Order == 0. In that case, the Scene Graph order is used.<br>
         * -- By default, all nodes have a Global Z Order = 0. That means that by default, the Scene Graph order is used to render the nodes.<br>
         * -- Global Z Order is useful when you need to render nodes in an order different than the Scene Graph order.<br>
         * -- Limitations: Global Z Order can't be used by Nodes that have SpriteBatchNode as one of their ancestors.<br>
         * -- And if ClippingNode is one of the ancestors, then "global Z order" will be relative to the ClippingNode.<br>
         * -- see `setLocalZOrder()`<br>
         * -- see `setVertexZ()`<br>
         * -- since v3.0<br>
         * -- param globalZOrder The global Z order value.
         * @method setGlobalZOrder
         * @param {number} _globalZOrder
         */
        public setGlobalZOrder (
            _globalZOrder : number 
        ) : void;

        /**
         * Sets the scale (x,y) of the node.<br>
         * -- It is a scaling factor that multiplies the width and height of the node and its children.<br>
         * -- param scaleX     The scale factor on X axis.<br>
         * -- param scaleY     The scale factor on Y axis.<br>
         * -- warning The physics body doesn't support this.
         * @method setScale
         * @param {(number)} _scaleX | scale
         * @param {(number)} _scaleY?
         */
        public setScale (
            _scaleX_scale : (number), 
            _scaleY? : (number)
        ) : void;

        /**
         * 
         * @method getOnExitCallback
         * @return {() => void}
         */
        public getOnExitCallback (
        ) : () => void;

        /**
         * Gets a child from the container with its tag.<br>
         * -- param tag   An identifier to find the child node.<br>
         * -- return a Node object whose tag equals to the input parameter.<br>
         * -- Please use `getChildByName()` instead.
         * @method getChildByTag
         * @param {number} _tag
         * @return {cc.Node}
         */
        public getChildByTag (
            _tag : number 
        ) : cc.Node;

        /**
         * Sets the arrival order when this node has a same ZOrder with other children.<br>
         * -- A node which called addChild subsequently will take a larger arrival order,<br>
         * -- If two children have the same Z order, the child with larger arrival order will be drawn later.<br>
         * -- warning This method is used internally for localZOrder sorting, don't change this manually<br>
         * -- param orderOfArrival   The arrival order.
         * @method setOrderOfArrival
         * @param {number} _orderOfArrival
         */
        public setOrderOfArrival (
            _orderOfArrival : number 
        ) : void;

        /**
         * Returns the scale factor on Z axis of this node<br>
         * -- see `setScaleZ(float)`<br>
         * -- return The scale factor on Z axis.
         * @method getScaleZ
         * @return {number}
         */
        public getScaleZ (
        ) : number;

        /**
         * Returns the scale factor on Y axis of this node<br>
         * -- see `setScaleY(float)`<br>
         * -- return The scale factor on Y axis.
         * @method getScaleY
         * @return {number}
         */
        public getScaleY (
        ) : number;

        /**
         * Returns the scale factor on X axis of this node<br>
         * -- see setScaleX(float)<br>
         * -- return The scale factor on X axis.
         * @method getScaleX
         * @return {number}
         */
        public getScaleX (
        ) : number;

        /**
         * LocalZOrder is the 'key' used to sort the node relative to its siblings.<br>
         * -- The Node's parent will sort all its children based ont the LocalZOrder value.<br>
         * -- If two nodes have the same LocalZOrder, then the node that was added first to the children's array will be in front of the other node in the array.<br>
         * -- Also, the Scene Graph is traversed using the "In-Order" tree traversal algorithm ( http:en.wikipedia.org/wiki/Tree_traversal#In-order )<br>
         * -- And Nodes that have LocalZOder values < 0 are the "left" subtree<br>
         * -- While Nodes with LocalZOder >=0 are the "right" subtree.<br>
         * -- see `setGlobalZOrder`<br>
         * -- see `setVertexZ`<br>
         * -- param localZOrder The local Z order value.
         * @method setLocalZOrder
         * @param {number} _localZOrder
         */
        public setLocalZOrder (
            _localZOrder : number 
        ) : void;

        /**
         * 
         * @method setCascadeColorEnabled
         * @param {boolean} _cascadeColorEnabled
         */
        public setCascadeColorEnabled (
            _cascadeColorEnabled : boolean 
        ) : void;

        /**
         * 
         * @method setOpacity
         * @param {number} _opacity
         */
        public setOpacity (
            _opacity : number 
        ) : void;

        /**
         * / @{/ @name component functions<br>
         * -- Gets a component by its name.<br>
         * -- param name A given name of component.<br>
         * -- return The Component by name.
         * @method getComponent
         * @param {string} _name
         * @return {cc.Component}
         */
        public getComponent (
            _name : string 
        ) : cc.Component;

        /**
         * Returns the untransformed size of the node.<br>
         * -- see `setContentSize(const Size&)`<br>
         * -- return The untransformed size of the node.
         * @method getContentSize
         * @return {ctype.value_type<cc.Size>}
         */
        public getContentSize (
        ) : ctype.value_type<cc.Size>;

        /**
         * Removes all actions from the running action list by its tag.<br>
         * -- param tag   A tag that indicates the action to be removed.
         * @method stopAllActionsByTag
         * @param {number} _tag
         */
        public stopAllActionsByTag (
            _tag : number 
        ) : void;

        /**
         * Returns an AABB (axis-aligned bounding-box) in its parent's coordinate system.<br>
         * -- return An AABB (axis-aligned bounding-box) in its parent's coordinate system
         * @method getBoundingBox
         * @return {ctype.value_type<cc.Rect>}
         */
        public getBoundingBox (
        ) : ctype.value_type<cc.Rect>;

        /**
         *  Set event dispatcher for scene.<br>
         * -- param dispatcher The event dispatcher of scene.
         * @method setEventDispatcher
         * @param {cc.EventDispatcher} _dispatcher
         */
        public setEventDispatcher (
            _dispatcher : cc.EventDispatcher 
        ) : void;

        /**
         * Returns the Node's Global Z Order.<br>
         * -- see `setGlobalZOrder(int)`<br>
         * -- return The node's global Z order
         * @method getGlobalZOrder
         * @return {number}
         */
        public getGlobalZOrder (
        ) : number;

        /**
         * 
         * @method draw
         * @param {(cc.Renderer)} _renderer?
         * @param {(ctype.value_type<cc.Mat4>)} _transform?
         * @param {(number)} _flags?
         */
        public draw (
            _renderer? : (any), 
            _transform? : (ctype.value_type<cc.Mat4>), 
            _flags? : (number)
        ) : void;

        /**
         * Returns a user assigned Object.<br>
         * -- Similar to UserData, but instead of holding a void* it holds an object.<br>
         * -- The UserObject will be retained once in this method,<br>
         * -- and the previous UserObject (if existed) will be released.<br>
         * -- The UserObject will be released in Node's destructor.<br>
         * -- param userObject    A user assigned Object.
         * @method setUserObject
         * @param {cc.Ref} _userObject
         */
        public setUserObject (
            _userObject : any 
        ) : void;

        /**
         *  Search the children of the receiving node to perform processing for nodes which share a name.<br>
         * -- param name The name to search for, supports c++11 regular expression.<br>
         * -- Search syntax options:<br>
         * -- ``: Can only be placed at the begin of the search string. This indicates that it will search recursively.<br>
         * -- `..`: The search should move up to the node's parent. Can only be placed at the end of string.<br>
         * -- `/` : When placed anywhere but the start of the search string, this indicates that the search should move to the node's children.<br>
         * -- code<br>
         * -- enumerateChildren("MyName", ...): This searches the children recursively and matches any node with the name `MyName`.<br>
         * -- enumerateChildren("[[:alnum:]]+", ...): This search string matches every node of its children.<br>
         * -- enumerateChildren("A[[:digit:]]", ...): This searches the node's children and returns any child named `A0`, `A1`, ..., `A9`.<br>
         * -- enumerateChildren("Abby/Normal", ...): This searches the node's grandchildren and returns any node whose name is `Normal`<br>
         * -- and whose parent is named `Abby`.<br>
         * -- enumerateChildren("Abby/Normal", ...): This searches recursively and returns any node whose name is `Normal` and whose<br>
         * -- parent is named `Abby`.<br>
         * -- endcode<br>
         * -- warning Only support alpha or number for name, and not support unicode.<br>
         * -- param callback A callback function to execute on nodes that match the `name` parameter. The function takes the following arguments:<br>
         * -- `node` <br>
         * -- A node that matches the name<br>
         * -- And returns a boolean result. Your callback can return `true` to terminate the enumeration.<br>
         * -- since v3.2
         * @method enumerateChildren
         * @param {string} _name
         * @param {(arg0:cc.Node) => boolean} _callback
         */
        public enumerateChildren (
            _name : string, 
            _callback : (arg0:cc.Node) => boolean 
        ) : void;

        /**
         * 
         * @method getonExitTransitionDidStartCallback
         * @return {() => void}
         */
        public getonExitTransitionDidStartCallback (
        ) : () => void;

        /**
         * Removes this node itself from its parent node.<br>
         * -- If the node orphan, then nothing happens.<br>
         * -- param cleanup   true if all actions and callbacks on this node should be removed, false otherwise.<br>
         * -- js removeFromParent<br>
         * -- lua removeFromParent
         * @method removeFromParentAndCleanup
         * @param {(boolean)} _cleanup?
         */
        public removeFromParent (
            _cleanup? : (boolean)
        ) : void;

        /**
         * Sets the position (X, Y, and Z) in its parent's coordinate system.<br>
         * -- param position The position (X, Y, and Z) in its parent's coordinate system.<br>
         * -- js NA
         * @method setPosition3D
         * @param {ctype.value_type<cc.Vec3>} _position
         */
        public setPosition3D (
            _position : ctype.value_type<cc.Vec3> 
        ) : void;

        /**
         * Update method will be called automatically every frame if "scheduleUpdate" is called, and the node is "live".<br>
         * -- param delta In seconds.
         * @method update
         * @param {number} _delta
         */
        public update (
            _delta : number 
        ) : void;

        /**
         * Sorts the children array once before drawing, instead of every time when a child is added or reordered.<br>
         * -- This approach can improves the performance massively.<br>
         * -- note Don't call this manually unless a child added needs to be removed in the same frame.
         * @method sortAllChildren
         */
        public sortAllChildren (
        ) : void;

        /**
         * 
         * @method getWorldToNodeAffineTransform
         * @return {cc.AffineTransform}
         */
        public getWorldToNodeTransform (
        ) : cc.AffineTransform;

        /**
         * Gets the scale factor of the node,  when X and Y have the same scale factor.<br>
         * -- warning Assert when `_scaleX != _scaleY`<br>
         * -- see setScale(float)<br>
         * -- return The scale factor of the node.
         * @method getScale
         * @return {number}
         */
        public getScale (
        ) : number;

        /**
         *  Returns the normalized position.<br>
         * -- return The normalized position.
         * @method getNormalizedPosition
         * @return {ctype.value_type<cc.Point>}
         */
        public getNormalizedPosition (
        ) : ctype.value_type<cc.Point>;

        /**
         * Returns the matrix that transform parent's space coordinates to the node's (local) space coordinates.<br>
         * -- The matrix is in Pixels.<br>
         * -- return The transformation matrix.
         * @method getParentToNodeTransform
         * @return {ctype.value_type<cc.Mat4>}
         */
        public getParentToNodeTransform3D (
        ) : ctype.value_type<cc.Mat4>;

        /**
         * Converts a Vec2 to node (local) space coordinates. The result is in Points.<br>
         * -- param worldPoint A given coordinate.<br>
         * -- return A point in node (local) space coordinates.
         * @method convertToNodeSpace
         * @param {ctype.value_type<cc.Point>} _worldPoint
         * @return {ctype.value_type<cc.Point>}
         */
        public convertToNodeSpace (
            _worldPoint : ctype.value_type<cc.Point> 
        ) : ctype.value_type<cc.Point>;

        /**
         * Changes the tag that is used to identify the node easily.<br>
         * -- Please refer to getTag for the sample code.<br>
         * -- param tag   A integer that identifies the node.<br>
         * -- Please use `setName()` instead.
         * @method setTag
         * @param {number} _tag
         */
        public setTag (
            _tag : number 
        ) : void;

        /**
         * 
         * @method isCascadeColorEnabled
         * @return {boolean}
         */
        public isCascadeColorEnabled (
        ) : boolean;

        /**
         * Set rotation by quaternion. You should make sure the quaternion is normalized.<br>
         * -- param quat The rotation in quaternion, note that the quat must be normalized.<br>
         * -- js NA
         * @method setRotationQuat
         * @param {cc.Quaternion} _quat
         */
        public setRotationQuat (
            _quat : cc.Quaternion 
        ) : void;

        /**
         * Stops and removes an action from the running action list.<br>
         * -- param action    The action object to be removed.
         * @method stopAction
         * @param {cc.Action} _action
         */
        public stopAction (
            _action : cc.Action 
        ) : void;

        /**
         * 
         * @method getActionManager
         * @return {cc.ActionManager}
         */
        public getActionManager (
        ) : cc.ActionManager;

        /**
         * Allocates and initializes a node.<br>
         * -- return A initialized node which is marked as "autorelease".
         * @method create
         * @return {cc.Node}
         */
        public static create (
        ) : cc.Node;

        /**
         * 
         * @method Node
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class __NodeRGBA
     * @native
     */
    export class NodeRGBA 
        extends cc.Node
    {

        /**
         * 
         * @method __NodeRGBA
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class Scene
     * @native
     */
    export class Scene 
        extends cc.Node
    {

        /**
         * 
         * @method setCameraOrderDirty
         */
        public setCameraOrderDirty (
        ) : void;

        /**
         *  Render the scene.<br>
         * -- param renderer The renderer use to render the scene.<br>
         * -- js NA
         * @method render
         * @param {cc.Renderer} _renderer
         */
        public render (
            _renderer : any 
        ) : void;

        /**
         * 
         * @method stepPhysicsAndNavigation
         * @param {number} _deltaTime
         */
        public stepPhysicsAndNavigation (
            _deltaTime : number 
        ) : void;

        /**
         * 
         * @method onProjectionChanged
         * @param {cc.EventCustom} _event
         */
        public onProjectionChanged (
            _event : cc.EventCustom 
        ) : void;

        /**
         * 
         * @method initWithSize
         * @param {ctype.value_type<cc.Size>} _size
         * @return {boolean}
         */
        public initWithSize (
            _size : ctype.value_type<cc.Size> 
        ) : boolean;

        /**
         *  Get the default camera.<br>
         * -- js NA<br>
         * -- return The default camera of scene.
         * @method getDefaultCamera
         * @return {cc.Camera}
         */
        public getDefaultCamera (
        ) : cc.Camera;

        /**
         *  Creates a new Scene object with a predefined Size. <br>
         * -- param size The predefined size of scene.<br>
         * -- return An autoreleased Scene object.<br>
         * -- js NA
         * @method createWithSize
         * @param {ctype.value_type<cc.Size>} _size
         * @return {cc.Scene}
         */
        public static createWithSize (
            _size : ctype.value_type<cc.Size> 
        ) : cc.Scene;

        /**
         *  Creates a new Scene object. <br>
         * -- return An autoreleased Scene object.
         * @method create
         * @return {cc.Scene}
         */
        public static create (
        ) : cc.Scene;

        /**
         * 
         * @method Scene
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class GLView
     * @native
     */
    export abstract class GLView 
    {

        /**
         * Set the frame size of EGL view.<br>
         * -- param width The width of the fram size.<br>
         * -- param height The height of the fram size.
         * @method setFrameSize
         * @param {number} _width
         * @param {number} _height
         */
        public setFrameSize (
            _width : number, 
            _height : number 
        ) : void;

        /**
         * Get the opengl view port rectangle.<br>
         * -- return Return the opengl view port rectangle.
         * @method getViewPortRect
         * @return {ctype.value_type<cc.Rect>}
         */
        public getViewPortRect (
        ) : ctype.value_type<cc.Rect>;

        /**
         *  Only works on ios platform. Set Content Scale of the Factor. 
         * @method setContentScaleFactor
         * @param {number} _scaleFactor
         * @return {boolean}
         */
        public setContentScaleFactor (
            _scaleFactor : number 
        ) : boolean;

        /**
         *  Only works on ios platform. Get Content Scale of the Factor. 
         * @method getContentScaleFactor
         * @return {number}
         */
        public getContentScaleFactor (
        ) : number;

        /**
         *  Open or close IME keyboard , subclass must implement this method. <br>
         * -- param open Open or close IME keyboard.
         * @method setIMEKeyboardState
         * @param {boolean} _open
         */
        public setIMEKeyboardState (
            _open : boolean 
        ) : void;

        /**
         * Gets safe area rectangle
         * @method getSafeAreaRect
         * @return {ctype.value_type<cc.Rect>}
         */
        public getSafeAreaRect (
        ) : ctype.value_type<cc.Rect>;

        /**
         * Set Scissor rectangle with points.<br>
         * -- param x Set the points of x.<br>
         * -- param y Set the points of y.<br>
         * -- param w Set the width of  the view port<br>
         * -- param h Set the Height of the view port.
         * @method setScissorInPoints
         * @param {number} _x
         * @param {number} _y
         * @param {number} _w
         * @param {number} _h
         */
        public setScissorInPoints (
            _x : number, 
            _y : number, 
            _w : number, 
            _h : number 
        ) : void;

        /**
         *  Get the view name.<br>
         * -- return The view name.
         * @method getViewName
         * @return {string}
         */
        public getViewName (
        ) : string;

        /**
         *  Get whether opengl render system is ready, subclass must implement this method. 
         * @method isOpenGLReady
         * @return {boolean}
         */
        public isOpenGLReady (
        ) : boolean;

        /**
         * Hide or Show the mouse cursor if there is one.<br>
         * -- param isVisible Hide or Show the mouse cursor if there is one.
         * @method setCursorVisible
         * @param {boolean} _isVisible
         */
        public setCursorVisible (
            _isVisible : boolean 
        ) : void;

        /**
         * Get scale factor of the vertical direction.<br>
         * -- return Scale factor of the vertical direction.
         * @method getScaleY
         * @return {number}
         */
        public getScaleY (
        ) : number;

        /**
         * Get scale factor of the horizontal direction.<br>
         * -- return Scale factor of the horizontal direction.
         * @method getScaleX
         * @return {number}
         */
        public getScaleX (
        ) : number;

        /**
         * Get the visible origin point of opengl viewport.<br>
         * -- return The visible origin point of opengl viewport.
         * @method getVisibleOrigin
         * @return {ctype.value_type<cc.Point>}
         */
        public getVisibleOrigin (
        ) : ctype.value_type<cc.Point>;

        /**
         * Get the frame size of EGL view.<br>
         * -- In general, it returns the screen size since the EGL view is a fullscreen view.<br>
         * -- return The frame size of EGL view.
         * @method getFrameSize
         * @return {ctype.value_type<cc.Size>}
         */
        public getFrameSize (
        ) : ctype.value_type<cc.Size>;

        /**
         *  Set zoom factor for frame. This methods are for<br>
         * -- debugging big resolution (e.g.new ipad) app on desktop.<br>
         * -- param zoomFactor The zoom factor for frame.
         * @method setFrameZoomFactor
         * @param {number} _zoomFactor
         */
        public setFrameZoomFactor (
            _zoomFactor : number 
        ) : void;

        /**
         *  Get zoom factor for frame. This methods are for<br>
         * -- debugging big resolution (e.g.new ipad) app on desktop.<br>
         * -- return The zoom factor for frame.
         * @method getFrameZoomFactor
         * @return {number}
         */
        public getFrameZoomFactor (
        ) : number;

        /**
         *  Get design resolution size.<br>
         * -- Default resolution size is the same as 'getFrameSize'.<br>
         * -- return The design resolution size.
         * @method getDesignResolutionSize
         * @return {ctype.value_type<cc.Size>}
         */
        public getDesignResolutionSize (
        ) : ctype.value_type<cc.Size>;

        /**
         *  When the window is closed, it will return false if the platforms is Ios or Android.<br>
         * -- If the platforms is windows or Mac,it will return true.<br>
         * -- return In ios and android it will return false,if in windows or Mac it will return true.
         * @method windowShouldClose
         * @return {boolean}
         */
        public windowShouldClose (
        ) : boolean;

        /**
         * Set the design resolution size.<br>
         * -- param width Design resolution width.<br>
         * -- param height Design resolution height.<br>
         * -- param resolutionPolicy The resolution policy desired, you may choose:<br>
         * -- [1] EXACT_FIT Fill screen by stretch-to-fit: if the design resolution ratio of width to height is different from the screen resolution ratio, your game view will be stretched.<br>
         * -- [2] NO_BORDER Full screen without black border: if the design resolution ratio of width to height is different from the screen resolution ratio, two areas of your game view will be cut.<br>
         * -- [3] SHOW_ALL  Full screen with black border: if the design resolution ratio of width to height is different from the screen resolution ratio, two black borders will be shown.
         * @method setDesignResolutionSize
         * @param {number} _width
         * @param {number} _height
         * @param {ResolutionPolicy} _resolutionPolicy
         */
        public setDesignResolutionSize (
            _width : number, 
            _height : number, 
            _resolutionPolicy : number 
        ) : void;

        /**
         *  Returns the current Resolution policy.<br>
         * -- return The current Resolution policy.
         * @method getResolutionPolicy
         * @return {number}
         */
        public getResolutionPolicy (
        ) : number;

        /**
         *  Returns whether or not the view is in Retina Display mode.<br>
         * -- return Returns whether or not the view is in Retina Display mode.
         * @method isRetinaDisplay
         * @return {boolean}
         */
        public isRetinaDisplay (
        ) : boolean;

        /**
         * Set opengl view port rectangle with points.<br>
         * -- param x Set the points of x.<br>
         * -- param y Set the points of y.<br>
         * -- param w Set the width of  the view port<br>
         * -- param h Set the Height of the view port.
         * @method setViewPortInPoints
         * @param {number} _x
         * @param {number} _y
         * @param {number} _w
         * @param {number} _h
         */
        public setViewPortInPoints (
            _x : number, 
            _y : number, 
            _w : number, 
            _h : number 
        ) : void;

        /**
         * Get the current scissor rectangle.<br>
         * -- return The current scissor rectangle.
         * @method getScissorRect
         * @return {ctype.value_type<cc.Rect>}
         */
        public getScissorRect (
        ) : ctype.value_type<cc.Rect>;

        /**
         *  Get retina factor.<br>
         * -- return The retina factor.
         * @method getRetinaFactor
         * @return {number}
         */
        public getRetinaFactor (
        ) : number;

        /**
         *  Set the view name. <br>
         * -- param viewname A string will be set to the view as name.
         * @method setViewName
         * @param {string} _viewname
         */
        public setViewName (
            _viewname : string 
        ) : void;

        /**
         * Get the visible rectangle of opengl viewport.<br>
         * -- return The visible rectangle of opengl viewport.
         * @method getVisibleRect
         * @return {ctype.value_type<cc.Rect>}
         */
        public getVisibleRect (
        ) : ctype.value_type<cc.Rect>;

        /**
         * Get the visible area size of opengl viewport.<br>
         * -- return The visible area size of opengl viewport.
         * @method getVisibleSize
         * @return {ctype.value_type<cc.Size>}
         */
        public getVisibleSize (
        ) : ctype.value_type<cc.Size>;

        /**
         * Get whether GL_SCISSOR_TEST is enable.<br>
         * -- return Whether GL_SCISSOR_TEST is enable.
         * @method isScissorEnabled
         * @return {boolean}
         */
        public isScissorEnabled (
        ) : boolean;

        /**
         *  Polls the events. 
         * @method pollEvents
         */
        public pollEvents (
        ) : void;

        /**
         *  Static method and member so that we can modify it on all platforms before create OpenGL context. <br>
         * -- param glContextAttrs The OpenGL context attrs.
         * @method setGLContextAttrs
         * @param {[WARNING] NATIVE CODE NOT SUPPORTING: GLContextAttrs} _glContextAttrs
         */
        public static setGLContextAttrs (
            _glContextAttrs : any 
        ) : void;

        /**
         *  Return the OpenGL context attrs. <br>
         * -- return Return the OpenGL context attrs.
         * @method getGLContextAttrs
         * @return {any}
         */
        public static getGLContextAttrs (
        ) : any;

    }
    /**
     * @class Director
     * @native
     */
    export abstract class Director 
    {
        /**
         * The event after visit of cc.Director
         * @constant
         * @type {string}
         * @example
         *   cc.eventManager.addCustomListener(cc.Director.EVENT_AFTER_VISIT, function(event) {
         *           cc.log("after visit event.");
         *       });
         */
        public static EVENT_AFTER_VISIT: string;

        /**
         * The event projection changed of cc.Director
         * @constant
         * @type {string}
         * @example
         *   cc.eventManager.addCustomListener(cc.Director.EVENT_PROJECTION_CHANGED, function(event) {
         *           cc.log("Projection changed.");
         *       });
         */
        public static EVENT_PROJECTION_CHANGED: string;

        //Possible OpenGL projections used by director
        /**
         * Constant for 2D projection (orthogonal projection)
         * @constant
         * @type {Number}
         */
        public static PROJECTION_2D: number;

        /**
         * Constant for 3D projection with a fovy=60, znear=0.5f and zfar=1500.
         * @constant
         * @type {Number}
         */
        public static PROJECTION_3D: number;

        /**
         * Constant for custom projection, if cc.Director's projection set to it, it calls "updateProjection" on the projection delegate.
         * @constant
         * @type {Number}
         */
        public static PROJECTION_CUSTOM: number;

        /**
         * Constant for default projection of cc.Director, default projection is 3D projection
         * @constant
         * @type {Number}
         */
        public static PROJECTION_DEFAULT: number;

        public sharedDirector: Director;
        public firstUseDirector: boolean;
        
        /**
         * Run a scene. Replaces the running scene with a new one or enter the first scene.
         * @param {cc.Scene} scene
         */
        runScene(scene: cc.Scene): void;

        public static readonly PROJECTION_2D: number;
        public static readonly PROJECTION_3D: number;
        public static readonly PROJECTION_CUSTOM: number;


        /**
         *  Pauses the running scene.<br>
         * -- The running scene will be _drawed_ but all scheduled timers will be paused.<br>
         * -- While paused, the draw rate will be 4 FPS to reduce CPU consumption.
         * @method pause
         */
        public pause (
        ) : void;

        /**
         *  Sets the EventDispatcher associated with this director.<br>
         * -- since v3.0<br>
         * -- js NA
         * @method setEventDispatcher
         * @param {cc.EventDispatcher} _dispatcher
         */
        public setEventDispatcher (
            _dispatcher : cc.EventDispatcher 
        ) : void;

        /**
         *  The size in pixels of the surface. It could be different than the screen size.<br>
         * -- High-res devices might have a higher surface size than the screen size.<br>
         * -- Only available when compiled using SDK >= 4.0.<br>
         * -- since v0.99.4
         * @method setContentScaleFactor
         * @param {number} _scaleFactor
         */
        public setContentScaleFactor (
            _scaleFactor : number 
        ) : void;

        /**
         * 
         * @method getDeltaTime
         * @return {number}
         */
        public getDeltaTime (
        ) : number;

        /**
         * Gets content scale factor.<br>
         * -- see Director::setContentScaleFactor()
         * @method getContentScaleFactor
         * @return {number}
         */
        public getContentScaleFactor (
        ) : number;

        /**
         *  Returns the size of the OpenGL view in pixels. 
         * @method getWinSizeInPixels
         * @return {ctype.value_type<cc.Size>}
         */
        public getWinSizeInPixels (
        ) : ctype.value_type<cc.Size>;

        /**
         * Returns safe area rectangle of the OpenGL view in points.
         * @method getSafeAreaRect
         * @return {ctype.value_type<cc.Rect>}
         */
        public getSafeAreaRect (
        ) : ctype.value_type<cc.Rect>;

        /**
         *  Sets the OpenGL default values.<br>
         * -- It will enable alpha blending, disable depth test.<br>
         * -- js NA
         * @method setGLDefaultValues
         */
        public setGLDefaultValues (
        ) : void;

        /**
         *  Sets the ActionManager associated with this director.<br>
         * -- since v2.0
         * @method setActionManager
         * @param {cc.ActionManager} _actionManager
         */
        public setActionManager (
            _actionManager : cc.ActionManager 
        ) : void;

        /**
         *  Enables/disables OpenGL alpha blending. 
         * @method setAlphaBlending
         * @param {boolean} _on
         */
        public setAlphaBlending (
            _on : boolean 
        ) : void;

        /**
         * Pops out all scenes from the stack until the root scene in the queue.<br>
         * -- This scene will replace the running one.<br>
         * -- Internally it will call `popToSceneStackLevel(1)`.
         * @method popToRootScene
         */
        public popToRootScene (
        ) : void;

        /**
         * Adds a matrix to the top of specified type of matrix stack.<br>
         * -- param type Matrix type.<br>
         * -- param mat The matrix that to be added.<br>
         * -- js NA
         * @method loadMatrix
         * @param {cc.MATRIX_STACK_TYPE} _type
         * @param {ctype.value_type<cc.Mat4>} _mat
         */
        public loadMatrix (
            _type : number, 
            _mat : ctype.value_type<cc.Mat4> 
        ) : void;

        /**
         *  This object will be visited after the main scene is visited.<br>
         * -- This object MUST implement the "visit" function.<br>
         * -- Useful to hook a notification object, like Notifications (http:github.com/manucorporat/CCNotifications)<br>
         * -- since v0.99.5
         * @method getNotificationNode
         * @return {cc.Node}
         */
        public getNotificationNode (
        ) : cc.Node;

        /**
         *  Returns the size of the OpenGL view in points. 
         * @method getWinSize
         * @return {ctype.value_type<cc.Size>}
         */
        public getWinSize (
        ) : ctype.value_type<cc.Size>;

        /**
         *  Ends the execution, releases the running scene.<br>
         * -- lua endToLua
         * @method end
         */
        public end (
        ) : void;

        /**
         * 
         * @method getTextureCache
         * @return {cc.TextureCache}
         */
        public getTextureCache (
        ) : cc.TextureCache;

        /**
         *  Whether or not the replaced scene will receive the cleanup message.<br>
         * -- If the new scene is pushed, then the old scene won't receive the "cleanup" message.<br>
         * -- If the new scene replaces the old one, the it will receive the "cleanup" message.<br>
         * -- since v0.99.0
         * @method isSendCleanupToScene
         * @return {boolean}
         */
        public isSendCleanupToScene (
        ) : boolean;

        /**
         *  Returns visible origin coordinate of the OpenGL view in points. 
         * @method getVisibleOrigin
         * @return {ctype.value_type<cc.Point>}
         */
        public getVisibleOrigin (
        ) : ctype.value_type<cc.Point>;

        /**
         *  Invoke main loop with delta time. Then `calculateDeltaTime` can just use the delta time directly.<br>
         * -- The delta time paseed may include vsync time. See issue #17806<br>
         * -- since 3.16
         * @method mainLoop
         * @param {(number)} _dt?
         */
        public mainLoop (
            _dt? : (number)
        ) : void;

        /**
         *  Enables/disables OpenGL depth test. 
         * @method setDepthTest
         * @param {boolean} _on
         */
        public setDepthTest (
            _on : boolean 
        ) : void;

        /**
         * Gets Frame Rate.<br>
         * -- js NA
         * @method getFrameRate
         * @return {number}
         */
        public getFrameRate (
        ) : number;

        /**
         *  Get seconds per frame. 
         * @method getSecondsPerFrame
         * @return {number}
         */
        public getSecondsPerFrame (
        ) : number;

        /**
         * Clear all types of matrix stack, and add identity matrix to these matrix stacks.<br>
         * -- js NA
         * @method resetMatrixStack
         */
        public resetMatrixStack (
        ) : void;

        /**
         * Converts an OpenGL coordinate to a screen coordinate.<br>
         * -- Useful to convert node points to window points for calls such as glScissor.
         * @method convertToUI
         * @param {ctype.value_type<cc.Point>} _point
         * @return {ctype.value_type<cc.Point>}
         */
        public convertToUI (
            _point : ctype.value_type<cc.Point> 
        ) : ctype.value_type<cc.Point>;

        /**
         * Clones a specified type matrix and put it to the top of specified type of matrix stack.<br>
         * -- js NA
         * @method pushMatrix
         * @param {cc.MATRIX_STACK_TYPE} _type
         */
        public pushMatrix (
            _type : number 
        ) : void;

        /**
         *  Sets the default values based on the Configuration info. 
         * @method setDefaultValues
         */
        public setDefaultValues (
        ) : void;

        /**
         * 
         * @method init
         * @return {boolean}
         */
        public init (
        ) : boolean;

        /**
         *  Sets the Scheduler associated with this director.<br>
         * -- since v2.0
         * @method setScheduler
         * @param {cc.Scheduler} _scheduler
         */
        public setScheduler (
            _scheduler : cc.Scheduler 
        ) : void;

        /**
         * Gets the top matrix of specified type of matrix stack.<br>
         * -- js NA
         * @method getMatrix
         * @param {cc.MATRIX_STACK_TYPE} _type
         * @return {ctype.value_type<cc.Mat4>}
         */
        public getMatrix (
            _type : number 
        ) : ctype.value_type<cc.Mat4>;

        /**
         * returns whether or not the Director is in a valid state
         * @method isValid
         * @return {boolean}
         */
        public isValid (
        ) : boolean;

        /**
         *  The main loop is triggered again.<br>
         * -- Call this function only if [stopAnimation] was called earlier.<br>
         * -- warning Don't call this function to start the main loop. To run the main loop call runWithScene.
         * @method startAnimation
         */
        public startAnimation (
        ) : void;

        /**
         * 
         * @method printUnusedTexturesHoldsBySpriteFrameCache
         */
        public printUnusedTexturesHoldsBySpriteFrameCache (
        ) : void;

        /**
         * Get the GLView.<br>
         * -- lua NA
         * @method getOpenGLView
         * @return {cc.GLView}
         */
        public getOpenGLView (
        ) : cc.GLView;

        /**
         *  Gets current running Scene. Director can only run one Scene at a time. 
         * @method getRunningScene
         * @return {cc.Scene}
         */
        public getRunningScene (
        ) : cc.Scene;

        /**
         *  Sets the glViewport.
         * @method setViewport
         */
        public setViewport (
        ) : void;

        /**
         *  Stops the animation. Nothing will be drawn. The main loop won't be triggered anymore.<br>
         * -- If you don't want to pause your animation call [pause] instead.
         * @method stopAnimation
         */
        public stopAnimation (
        ) : void;

        /**
         *  Pops out all scenes from the stack until it reaches `level`.<br>
         * -- If level is 0, it will end the director.<br>
         * -- If level is 1, it will pop all scenes until it reaches to root scene.<br>
         * -- If level is <= than the current stack level, it won't do anything.
         * @method popToSceneStackLevel
         * @param {number} _level
         */
        public popToSceneStackLevel (
            _level : number 
        ) : void;

        /**
         *  Resumes the paused scene.<br>
         * -- The scheduled timers will be activated again.<br>
         * -- The "delta time" will be 0 (as if the game wasn't paused).
         * @method resume
         */
        public resume (
        ) : void;

        /**
         *  Whether or not `_nextDeltaTimeZero` is set to 0. 
         * @method isNextDeltaTimeZero
         * @return {boolean}
         */
        public isNextDeltaTimeZero (
        ) : boolean;

        /**
         *  Sets clear values for the color buffers,<br>
         * -- value range of each element is [0.0, 1.0].<br>
         * -- js NA
         * @method setClearColor
         * @param {ctype.value_type<cc.Color>} _clearColor
         */
        public setClearColor (
            _clearColor : ctype.value_type<cc.Color> 
        ) : void;

        /**
         * Sets the GLView. <br>
         * -- lua NA
         * @method setOpenGLView
         * @param {cc.GLView} _openGLView
         */
        public setOpenGLView (
            _openGLView : cc.GLView 
        ) : void;

        /**
         * Converts a screen coordinate to an OpenGL coordinate.<br>
         * -- Useful to convert (multi) touch coordinates to the current layout (portrait or landscape).
         * @method convertToGL
         * @param {ctype.value_type<cc.Point>} _point
         * @return {ctype.value_type<cc.Point>}
         */
        public convertToGL (
            _point : ctype.value_type<cc.Point> 
        ) : ctype.value_type<cc.Point>;

        /**
         *  Removes all cocos2d cached data.<br>
         * -- It will purge the TextureCache, SpriteFrameCache, LabelBMFont cache<br>
         * -- since v0.99.3
         * @method purgeCachedData
         */
        public purgeCachedData (
        ) : void;

        /**
         *  How many frames were called since the director started 
         * @method getTotalFrames
         * @return {number}
         */
        public getTotalFrames (
        ) : number;

        /**
         * Enters the Director's main loop with the given Scene.<br>
         * -- Call it to run only your FIRST scene.<br>
         * -- Don't call it if there is already a running scene.<br>
         * -- It will call pushScene: and then it will call startAnimation<br>
         * -- js NA
         * @method runWithScene
         * @param {cc.Scene} _scene
         */
        public runWithScene (
            _scene : cc.Scene 
        ) : void;

        /**
         * Sets the notification node.<br>
         * -- see Director::getNotificationNode()
         * @method setNotificationNode
         * @param {cc.Node} _node
         */
        public setNotificationNode (
            _node : cc.Node 
        ) : void;

        /**
         *  Draw the scene.<br>
         * -- This method is called every frame. Don't call it manually.
         * @method drawScene
         */
        public drawScene (
        ) : void;

        /**
         * 
         * @method restart
         */
        public restart (
        ) : void;

        /**
         * Pops out a scene from the stack.<br>
         * -- This scene will replace the running one.<br>
         * -- The running scene will be deleted. If there are no more scenes in the stack the execution is terminated.<br>
         * -- ONLY call it if there is a running scene.
         * @method popScene
         */
        public popScene (
        ) : void;

        /**
         *  Adds an identity matrix to the top of specified type of matrix stack.<br>
         * -- js NA
         * @method loadIdentityMatrix
         * @param {cc.MATRIX_STACK_TYPE} _type
         */
        public loadIdentityMatrix (
            _type : number 
        ) : void;

        /**
         *  Whether or not displaying the FPS on the bottom-left corner of the screen. 
         * @method isDisplayStats
         * @return {boolean}
         */
        public isDisplayStats (
        ) : boolean;

        /**
         *  Sets OpenGL projection. 
         * @method setProjection
         * @param {cc.Director::Projection} _projection
         */
        public setProjection (
            _projection : number 
        ) : void;

        /**
         * Multiplies a matrix to the top of specified type of matrix stack.<br>
         * -- param type Matrix type.<br>
         * -- param mat The matrix that to be multiplied.<br>
         * -- js NA
         * @method multiplyMatrix
         * @param {cc.MATRIX_STACK_TYPE} _type
         * @param {ctype.value_type<cc.Mat4>} _mat
         */
        public multiplyMatrix (
            _type : number, 
            _mat : ctype.value_type<cc.Mat4> 
        ) : void;

        /**
         * Gets the distance between camera and near clipping frame.<br>
         * -- It is correct for default camera that near clipping frame is same as the screen.
         * @method getZEye
         * @return {number}
         */
        public getZEye (
        ) : number;

        /**
         * Sets the delta time between current frame and next frame is 0.<br>
         * -- This value will be used in Schedule, and will affect all functions that are using frame delta time, such as Actions.<br>
         * -- This value will take effect only one time.
         * @method setNextDeltaTimeZero
         * @param {boolean} _nextDeltaTimeZero
         */
        public setNextDeltaTimeZero (
            _nextDeltaTimeZero : boolean 
        ) : void;

        /**
         *  Pops the top matrix of the specified type of matrix stack.<br>
         * -- js NA
         * @method popMatrix
         * @param {cc.MATRIX_STACK_TYPE} _type
         */
        public popMatrix (
            _type : number 
        ) : void;

        /**
         * Returns visible size of the OpenGL view in points.<br>
         * -- The value is equal to `Director::getWinSize()` if don't invoke `GLView::setDesignResolutionSize()`.
         * @method getVisibleSize
         * @return {ctype.value_type<cc.Size>}
         */
        public getVisibleSize (
        ) : ctype.value_type<cc.Size>;

        /**
         *  Gets the Scheduler associated with this director.<br>
         * -- since v2.0
         * @method getScheduler
         * @return {cc.Scheduler}
         */
        public getScheduler (
        ) : cc.Scheduler;

        /**
         * Suspends the execution of the running scene, pushing it on the stack of suspended scenes.<br>
         * -- The new scene will be executed.<br>
         * -- Try to avoid big stacks of pushed scenes to reduce memory allocation. <br>
         * -- ONLY call it if there is a running scene.
         * @method pushScene
         * @param {cc.Scene} _scene
         */
        public pushScene (
            _scene : cc.Scene 
        ) : void;

        /**
         *  Gets the FPS value. 
         * @method getAnimationInterval
         * @return {number}
         */
        public getAnimationInterval (
        ) : number;

        /**
         *  Whether or not the Director is paused. 
         * @method isPaused
         * @return {boolean}
         */
        public isPaused (
        ) : boolean;

        /**
         *  Display the FPS on the bottom-left corner of the screen. 
         * @method setDisplayStats
         * @param {boolean} _displayStats
         */
        public setDisplayStats (
            _displayStats : boolean 
        ) : void;

        /**
         *  Gets the EventDispatcher associated with this director.<br>
         * -- since v3.0<br>
         * -- js NA
         * @method getEventDispatcher
         * @return {cc.EventDispatcher}
         */
        public getEventDispatcher (
        ) : cc.EventDispatcher;

        /**
         *  Replaces the running scene with a new one. The running scene is terminated.<br>
         * -- ONLY call it if there is a running scene.<br>
         * -- js NA
         * @method replaceScene
         * @param {cc.Scene} _scene
         */
        public replaceScene (
            _scene : cc.Scene 
        ) : void;

        /**
         *  Sets the FPS value. FPS = 1/interval. 
         * @method setAnimationInterval
         * @param {number} _interval
         */
        public setAnimationInterval (
            _interval : number 
        ) : void;

        /**
         *  Gets the ActionManager associated with this director.<br>
         * -- since v2.0
         * @method getActionManager
         * @return {cc.ActionManager}
         */
        public getActionManager (
        ) : cc.ActionManager;

        /**
         * Returns a shared instance of the director. <br>
         * -- js _getInstance
         * @method getInstance
         * @return {cc.Director}
         */
        public static getInstance (
        ) : cc.Director;

    }
    /**
     * @class Scheduler
     * @native
     */
    export class Scheduler 
    {
        /**
         * Priority level reserved for system services.
         * @constant
         * @type Number
         */
        public static PRIORITY_SYSTEM:number;

        
        // *****************************************************************
        // *****************************************************************
        // ******IMPORTANT NOTE: *******************************************
        // This section is relay on 
        // cocos2d-x/cocos/scripting/js-bindings/manual/cocos2d_specifics.cpp
        // *****************************************************************
        // *****************************************************************
        
        /**
         * Resumes the target.<br/>
         * The 'target' will be unpaused, so all schedule selectors/update will be 'ticked' again.<br/>
         * If the target is not present, nothing happens.
         * @param {cc.Class} target
         */
        public resumeTarget(target:any):void;

        
        /**
         * <p>
         *    Pauses the target.<br/>
         *    All scheduled selectors/update for a given target won't be 'ticked' until the target is resumed.<br/>
         *    If the target is not present, nothing happens.
         * </p>
         * @param {cc.Class} target
         */
        public pauseTarget(target:any):void;

        /**
         * <p>
         *    Schedules the 'update' callback_fn for a given target with a given priority.<br/>
         *    The 'update' callback_fn will be called every frame.<br/>
         *    The lower the priority, the earlier it is called.
         * </p>
         * @deprecated since v3.4 please use .scheduleUpdate
         * @param {cc.Class} target
         * @param {Number} priority
         * @param {Boolean} paused
         * @example
         * //register this object to scheduler
         * cc.director.getScheduler().scheduleUpdateForTarget(this, priority, !this._isRunning );
         */
        public scheduleUpdateForTarget(target:Class, priority?:number, paused?:boolean):void;

        /**
         * TODO: Put an explanation here for this method's purpose/functionality.
         * @param {Class} target
         */
        public unscheduleUpdate(target:any):void;

        /**
         * TODO: Put an explanation here for this method's purpose/functionality.
         *       Also, again we have a key parameter, and WTF is the proper type?!?
         *
         * @param {cc.Class} target
         * @param {function} callback
         * @param {Number} interval
         * @param {Number} repeat
         * @param {Number} delay
         * @param {Boolean} paused
         * @param {any} key
         * @example
         */
        public schedule(callback:(arg?:any) => void, target:any,
                 interval?:number, repeat?:number, delay?:number, paused?:boolean, key?:any):void;

        /**
         * <p>
         *   The scheduled method will be called every 'interval' seconds.</br>
         *   If paused is YES, then it won't be called until it is resumed.<br/>
         *   If 'interval' is 0, it will be called every frame, but if so, it recommended to use 'scheduleUpdateForTarget:' instead.<br/>
         *   If the callback function is already scheduled, then only the interval parameter will be updated without re-scheduling it again.<br/>
         *   repeat let the action be repeated repeat + 1 times, use cc.REPEAT_FOREVER to let the action run continuously<br/>
         *   delay is the amount of time the action will wait before it'll start<br/>
         * </p>
         * @deprecated since v3.4 please use .schedule
         * @param {cc.Class} target
         * @param {function} callback_fn
         * @param {Number} interval
         * @param {Number} repeat
         * @param {Number} delay
         * @param {Boolean} paused
         * @example
         * //register a schedule to scheduler
         * cc.director.getScheduler().scheduleCallbackForTarget(this, function, interval, repeat, delay, !this._isRunning );
         */
        public scheduleCallbackForTarget(target:any, callback_fn:(arg?:any) => void,
                          interval?:number, repeat?:number, delay?:number, paused?:boolean):void;

        
        /**
         * TODO: Put an explanation here for this method's purpose/functionality.
         * @param {Class} target
         */
        public unscheduleAllForTarget(target:any):void;

        /**
         *  <p>
         *      Unschedules all function callbacks from all targets. <br/>
         *      You should NEVER call this method, unless you know what you are doing.
         *  </p>
         * @deprecated since v3.4 please use .unscheduleAllWithMinPriority
         */
        public unscheduleAllCallbacks():void;

        
        /**
         * <p>
         *    Unschedules all function callbacks from all targets with a minimum priority.<br/>
         *    You should only call this with kCCPriorityNonSystemMin or higher.
         * </p>
         * @deprecated since v3.4 please use .unscheduleAllWithMinPriority
         * @param {Number} minPriority
         */
        public unscheduleAllCallbacksWithMinPriority(minPriority:number):void;

        /**
         * Returns whether or not the target is paused
         * @param {cc.Class} target
         * @return {Boolean}
         */
        public isTargetPaused(target:any):boolean;

        // *****************************************************************
        // *****************************************************************
        // *****************************************************************
        // *****************************************************************
        // *****************************************************************
        

        /**
         *  Modifies the time of all scheduled callbacks.<br>
         * -- You can use this property to create a 'slow motion' or 'fast forward' effect.<br>
         * -- Default is 1.0. To create a 'slow motion' effect, use values below 1.0.<br>
         * -- To create a 'fast forward' effect, use values higher than 1.0.<br>
         * -- since v0.8<br>
         * -- warning It will affect EVERY scheduled selector / action.
         * @method setTimeScale
         * @param {number} _timeScale
         */
        public setTimeScale (
            _timeScale : number 
        ) : void;

        /**
         *  Unschedules all selectors from all targets with a minimum priority.<br>
         * -- You should only call this with `PRIORITY_NON_SYSTEM_MIN` or higher.<br>
         * -- param minPriority The minimum priority of selector to be unscheduled. Which means, all selectors which<br>
         * -- priority is higher than minPriority will be unscheduled.<br>
         * -- since v2.0.0
         * @method unscheduleAllWithMinPriority
         * @param {number} _minPriority
         */
        public unscheduleAllWithMinPriority (
            _minPriority : number 
        ) : void;

        /**
         *  'update' the scheduler.<br>
         * -- You should NEVER call this method, unless you know what you are doing.<br>
         * -- lua NA
         * @method update
         * @param {number} _dt
         */
        public update (
            _dt : number 
        ) : void;

        /**
         *  Unschedule a script entry. <br>
         * -- warning Don't invoke this function unless you know what you are doing.<br>
         * -- js NA<br>
         * -- lua NA
         * @method unscheduleScriptEntry
         * @param {number} _scheduleScriptEntryID
         */
        public unscheduleScriptEntry (
            _scheduleScriptEntryID : number 
        ) : void;

        /**
         *  Calls a function on the cocos2d thread. Useful when you need to call a cocos2d function from another thread.<br>
         * -- This function is thread safe.<br>
         * -- param function The function to be run in cocos2d thread.<br>
         * -- since v3.0<br>
         * -- js NA
         * @method performFunctionInCocosThread
         * @param {() => void} _function
         */
        public performFunctionInCocosThread (
            _function : () => void 
        ) : void;

        /**
         *  Unschedules all selectors from all targets.<br>
         * -- You should NEVER call this method, unless you know what you are doing.<br>
         * -- since v0.99.3
         * @method unscheduleAll
         */
        public unscheduleAll (
        ) : void;

        /**
         * Gets the time scale of schedule callbacks.<br>
         * -- see Scheduler::setTimeScale()
         * @method getTimeScale
         * @return {number}
         */
        public getTimeScale (
        ) : number;

        /**
         * Constructor<br>
         * -- js ctor
         * @method Scheduler
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class AsyncTaskPool
     * @native
     */
    export abstract class AsyncTaskPool 
    {

        /**
         * Stop tasks.<br>
         * -- param type Task type you want to stop.
         * @method stopTasks
         * @param {cc.AsyncTaskPool::TaskType} _type
         */
        public stopTasks (
            _type : number 
        ) : void;

        /**
         * Destroys the async task pool.
         * @method destoryInstance
         */
        public static destoryInstance (
        ) : void;

        /**
         * Returns the shared instance of the async task pool.
         * @method getInstance
         * @return {cc.AsyncTaskPool}
         */
        public static getInstance (
        ) : cc.AsyncTaskPool;

    }
    /**
     * @class Action
     * @native
     */
    export abstract class Action 
    {
        /**
         * Currently JavaScript Bindigns (JSB), in some cases, needs to use retain and release. This is a bug in JSB, <br/>
         * and the ugly workaround is to use retain/release. So, these 2 methods were added to be compatible with JSB. <br/>
         * This is a hack, and should be removed once JSB fixes the retain/release bug.
         */
        public retain():void;

        /**
         * Currently JavaScript Bindigns (JSB), in some cases, needs to use retain and release. This is a bug in JSB, <br/>
         * and the ugly workaround is to use retain/release. So, these 2 methods were added to be compatible with JSB. <br/>
         * This is a hack, and should be removed once JSB fixes the retain/release bug.
         */
        public release():void;
        
        public originalTarget:Node;
        public target:Node;
        
        /**
         *  Returns a tag that is used to identify the action easily. 
         * return A tag.
         * @method getTag
         * @return {number}
         */
        public tag : number;


        /**
         *  Called before the action start. It will also set the target. <br>
         * -- param target A certain target.
         * @method startWithTarget
         * @param {cc.Node} _target
         */
        public startWithTarget (
            _target : cc.Node 
        ) : void;

        /**
         * Set the original target, since target can be nil.<br>
         * -- Is the target that were used to run the action. Unless you are doing something complex, like ActionManager, you should NOT call this method.<br>
         * -- The target is 'assigned', it is not 'retained'.<br>
         * -- since v0.8.2<br>
         * -- param originalTarget Is 'assigned', it is not 'retained'.
         * @method setOriginalTarget
         * @param {cc.Node} _originalTarget
         */
        public setOriginalTarget (
            _originalTarget : cc.Node 
        ) : void;

        /**
         *  Returns a clone of action.<br>
         * -- return A clone action.
         * @method clone
         * @return {cc.Action}
         */
        public clone (
        ) : cc.Action;

        /**
         *  Return a original Target. <br>
         * -- return A original Target.
         * @method getOriginalTarget
         * @return {cc.Node}
         */
        public getOriginalTarget (
        ) : cc.Node;

        /**
         * Called after the action has finished. It will set the 'target' to nil.<br>
         * -- IMPORTANT: You should never call "Action::stop()" manually. Instead, use: "target->stopAction(action);".
         * @method stop
         */
        public stop (
        ) : void;

        /**
         * Called once per frame. time a value between 0 and 1.<br>
         * -- For example:<br>
         * -- - 0 Means that the action just started.<br>
         * -- - 0.5 Means that the action is in the middle.<br>
         * -- - 1 Means that the action is over.<br>
         * -- param time A value between 0 and 1.
         * @method update
         * @param {number} _time
         */
        public update (
            _time : number 
        ) : void;

        /**
         *  Return certain target.<br>
         * -- return A certain target.
         * @method getTarget
         * @return {cc.Node}
         */
        public getTarget (
        ) : cc.Node;

        /**
         *  Returns a flag field that is used to group the actions easily.<br>
         * -- return A tag.
         * @method getFlags
         * @return {number}
         */
        public getFlags (
        ) : number;

        /**
         *  Called every frame with it's delta time, dt in seconds. DON'T override unless you know what you are doing. <br>
         * -- param dt In seconds.
         * @method step
         * @param {number} _dt
         */
        public step (
            _dt : number 
        ) : void;

        /**
         *  Changes the tag that is used to identify the action easily. <br>
         * -- param tag Used to identify the action easily.
         * @method setTag
         * @param {number} _tag
         */
        public setTag (
            _tag : number 
        ) : void;

        /**
         *  Changes the flag field that is used to group the actions easily.<br>
         * -- param tag Used to identify the action easily.
         * @method setFlags
         * @param {number} _flags
         */
        public setFlags (
            _flags : number 
        ) : void;

        /**
         *  Returns a tag that is used to identify the action easily. <br>
         * -- return A tag.
         * @method getTag
         * @return {number}
         */
        public getTag (
        ) : number;

        /**
         *  The action will modify the target properties. <br>
         * -- param target A certain target.
         * @method setTarget
         * @param {cc.Node} _target
         */
        public setTarget (
            _target : cc.Node 
        ) : void;

        /**
         *  Return true if the action has finished. <br>
         * -- return Is true if the action has finished.
         * @method isDone
         * @return {boolean}
         */
        public isDone (
        ) : boolean;

        /**
         *  Returns a new action that performs the exactly the reverse action. <br>
         * -- return A new action that performs the exactly the reverse action.<br>
         * -- js NA
         * @method reverse
         * @return {cc.Action}
         */
        public reverse (
        ) : cc.Action;

    }
    /**
     * @class FiniteTimeAction
     * @native
     */
    export abstract class FiniteTimeAction 
        extends cc.Action
    {

        /**
         *  Set duration in seconds of the action. <br>
         * -- param duration In seconds of the action.
         * @method setDuration
         * @param {number} _duration
         */
        public setDuration (
            _duration : number 
        ) : void;

        /**
         *  Get duration in seconds of the action. <br>
         * -- return The duration in seconds of the action.
         * @method getDuration
         * @return {number}
         */
        public getDuration (
        ) : number;

    }
    /**
     * @class Speed
     * @native
     */
    export class Speed 
        extends cc.Action
    {

        /**
         *  Replace the interior action.<br>
         * -- param action The new action, it will replace the running action.
         * @method setInnerAction
         * @param {cc.ActionInterval} _action
         */
        public setInnerAction (
            _action : cc.ActionInterval 
        ) : void;

        /**
         *  Return the speed.<br>
         * -- return The action speed.
         * @method getSpeed
         * @return {number}
         */
        public _getSpeed (
        ) : number;

        /**
         *  Alter the speed of the inner function in runtime. <br>
         * -- param speed Alter the speed of the inner function in runtime.
         * @method setSpeed
         * @param {number} _speed
         */
        public _setSpeed (
            _speed : number 
        ) : void;

        /**
         *  Initializes the action. 
         * @method initWithAction
         * @param {cc.ActionInterval} _action
         * @param {number} _speed
         * @return {boolean}
         */
        public initWithAction (
            _action : cc.ActionInterval, 
            _speed : number 
        ) : boolean;

        /**
         *  Return the interior action.<br>
         * -- return The interior action.
         * @method getInnerAction
         * @return {cc.ActionInterval}
         */
        public getInnerAction (
        ) : cc.ActionInterval;

        /**
         *  Create the action and set the speed.<br>
         * -- param action An action.<br>
         * -- param speed The action speed.
         * @method create
         * @param {cc.ActionInterval} _action
         * @param {number} _speed
         * @return {cc.Speed}
         */
        public static create (
            _action : cc.ActionInterval, 
            _speed : number 
        ) : cc.Speed;

        /**
         * 
         * @method Speed
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class Follow
     * @native
     */
    export class Follow 
        extends cc.Action
    {

        /**
         *  Alter behavior - turn on/off boundary. <br>
         * -- param value Turn on/off boundary.
         * @method setBoundarySet
         * @param {boolean} _value
         */
        public setBoundarySet (
            _value : boolean 
        ) : void;

        /**
         * Initializes the action with a set boundary or with no boundary.<br>
         * -- param followedNode  The node to be followed.<br>
         * -- param rect  The boundary. If \p rect is equal to Rect::ZERO, it'll work<br>
         * -- with no boundary.
         * @method initWithTarget
         * @param {cc.Node} _followedNode
         * @param {ctype.value_type<cc.Rect>} _rect
         * @return {boolean}
         */
        public initWithTarget (
            _followedNode : cc.Node, 
            _rect : ctype.value_type<cc.Rect> 
        ) : boolean;

        /**
         *  Return boundarySet.<br>
         * -- return Return boundarySet.
         * @method isBoundarySet
         * @return {boolean}
         */
        public isBoundarySet (
        ) : boolean;

        /**
         * Creates the action with a set boundary or with no boundary.<br>
         * -- param followedNode  The node to be followed.<br>
         * -- param rect  The boundary. If \p rect is equal to Rect::ZERO, it'll work<br>
         * -- with no boundary.
         * @method create
         * @param {cc.Node} _followedNode
         * @param {ctype.value_type<cc.Rect>} _rect
         * @return {cc.Follow}
         */
        public static create (
            _followedNode : cc.Node, 
            _rect : ctype.value_type<cc.Rect> 
        ) : cc.Follow;

        /**
         * js ctor
         * @method Follow
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class Image
     * @native
     */
    export class Image 
    {
        /**
         * This function is ignored by default inside cocos, for some unknown reason.
         * In our branch we added this function manually so, here it is.
         */
        public initWithImageData(data : any[]) : boolean;

        /**
         * 
         * @method hasPremultipliedAlpha
         * @return {boolean}
         */
        public hasPremultipliedAlpha (
        ) : boolean;

        /**
         * 
         * @method getDataLen
         * @return {number}
         */
        public getDataLen (
        ) : number;

        /**
         * brief    Save Image data to the specified file, with specified format.<br>
         * -- param    filePath        the file's absolute path, including file suffix.<br>
         * -- param    isToRGB        whether the image is saved as RGB format.
         * @method saveToFile
         * @param {string} _filename
         * @param {boolean} _isToRGB
         * @return {boolean}
         */
        public saveToFile (
            _filename : string, 
            _isToRGB : boolean 
        ) : boolean;

        /**
         * 
         * @method hasAlpha
         * @return {boolean}
         */
        public hasAlpha (
        ) : boolean;

        /**
         * 
         * @method isCompressed
         * @return {boolean}
         */
        public isCompressed (
        ) : boolean;

        /**
         * 
         * @method getHeight
         * @return {number}
         */
        public getHeight (
        ) : number;

        /**
         * brief Load the image from the specified path.<br>
         * -- param path   the absolute file path.<br>
         * -- return true if loaded correctly.
         * @method initWithImageFile
         * @param {string} _path
         * @return {boolean}
         */
        public initWithImageFile (
            _path : string 
        ) : boolean;

        /**
         * 
         * @method getWidth
         * @return {number}
         */
        public getWidth (
        ) : number;

        /**
         * 
         * @method getBitPerPixel
         * @return {number}
         */
        public getBitPerPixel (
        ) : number;

        /**
         * 
         * @method getPalette
         * @return {ctype.value_type<cc.Vec4>}
         */
        public getPalette (
        ) : ctype.value_type<cc.Vec4>;

        /**
         * 
         * @method getFileType
         * @return {number}
         */
        public getFileType (
        ) : number;

        /**
         * 
         * @method getFilePath
         * @return {string}
         */
        public getFilePath (
        ) : string;

        /**
         * 
         * @method getNumberOfMipmaps
         * @return {number}
         */
        public getNumberOfMipmaps (
        ) : number;

        /**
         * 
         * @method getRenderFormat
         * @return {number}
         */
        public getRenderFormat (
        ) : number;

        /**
         * 
         * @method getData
         * @return {string}
         */
        public getData (
        ) : string;

        /**
         * 
         * @method getMipmaps
         * @return {cc._MipmapInfo}
         */
        public getMipmaps (
        ) : cc._MipmapInfo;

        /**
         * 
         * @method initWithRawData
         * @param {string} _data
         * @param {number} _dataLen
         * @param {number} _width
         * @param {number} _height
         * @param {number} _bitsPerComponent
         * @param {boolean} _preMulti
         * @return {boolean}
         */
        public initWithRawData (
            _data : string, 
            _dataLen : number, 
            _width : number, 
            _height : number, 
            _bitsPerComponent : number, 
            _preMulti : boolean 
        ) : boolean;

        /**
         *  treats (or not) PVR files as if they have alpha premultiplied.<br>
         * -- Since it is impossible to know at runtime if the PVR images have the alpha channel premultiplied, it is<br>
         * -- possible load them as if they have (or not) the alpha channel premultiplied.<br>
         * -- By default it is disabled.
         * @method setPVRImagesHavePremultipliedAlpha
         * @param {boolean} _haveAlphaPremultiplied
         */
        public static setPVRImagesHavePremultipliedAlpha (
            _haveAlphaPremultiplied : boolean 
        ) : void;

        /**
         * js ctor
         * @method Image
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class GLProgramState
     * @native
     */
    export class GLProgramState 
    {

        /**
         * 
         * @method setUniformCallback
         * @param {(number) | (string)} _uniformLocation | uniformName
         * @param {((arg0:cc.GLProgram, arg1:cc.Uniform) => void)} _callback
         */
        public setUniformCallback (
            _uniformLocation_uniformName : (number) | (string), 
            _callback : ((arg0:cc.GLProgram, arg1:cc.Uniform) => void)
        ) : void;

        /**
         *  Get the flag of vertex attribs used by OR operation.
         * @method getVertexAttribsFlags
         * @return {number}
         */
        public getVertexAttribsFlags (
        ) : number;

        /**
         * Applies the specified custom auto-binding.<br>
         * -- param uniformName Name of the shader uniform.<br>
         * -- param autoBinding Name of the auto binding.
         * @method applyAutoBinding
         * @param {string} _uniformName
         * @param {string} _autoBinding
         */
        public applyAutoBinding (
            _uniformName : string, 
            _autoBinding : string 
        ) : void;

        /**
         * 
         * @method setUniformVec2
         * @param {(number) | (string)} _uniformLocation | uniformName
         * @param {(ctype.value_type<cc.Point>)} _value
         */
        public setUniformVec2 (
            _uniformLocation_uniformName : (number) | (string), 
            _value : (ctype.value_type<cc.Point>)
        ) : void;

        /**
         * 
         * @method setUniformVec3
         * @param {(number) | (string)} _uniformLocation | uniformName
         * @param {(ctype.value_type<cc.Vec3>)} _value
         */
        public setUniformVec3 (
            _uniformLocation_uniformName : (number) | (string), 
            _value : (ctype.value_type<cc.Vec3>)
        ) : void;

        /**
         * @{<br>
         * -- Set the vertex attribute value.
         * @method setVertexAttribCallback
         * @param {string} _name
         * @param {(arg0:cc.VertexAttrib) => void} _callback
         */
        public setVertexAttribCallback (
            _name : string, 
            _callback : (arg0:cc.VertexAttrib) => void 
        ) : void;

        /**
         * Apply GLProgram, attributes and uniforms.<br>
         * -- param modelView The applied modelView matrix to shader.
         * @method apply
         * @param {ctype.value_type<cc.Mat4>} _modelView
         */
        public apply (
            _modelView : ctype.value_type<cc.Mat4> 
        ) : void;

        /**
         * Returns the Node bound to the GLProgramState
         * @method getNodeBinding
         * @return {cc.Node}
         */
        public getNodeBinding (
        ) : cc.Node;

        /**
         * Apply GLProgram, and built in uniforms.<br>
         * -- param modelView The applied modelView matrix to shader.
         * @method applyGLProgram
         * @param {ctype.value_type<cc.Mat4>} _modelView
         */
        public applyGLProgram (
            _modelView : ctype.value_type<cc.Mat4> 
        ) : void;

        /**
         * Sets the node that this render state is bound to.<br>
         * -- The specified node is used to apply auto-bindings for the render state.<br>
         * -- This is typically set to the node of the model that a material is<br>
         * -- applied to.<br>
         * -- param node The node to use for applying auto-bindings.
         * @method setNodeBinding
         * @param {cc.Node} _node
         */
        public setNodeBinding (
            _node : cc.Node 
        ) : void;

        /**
         *  @{<br>
         * -- Setting user defined uniforms by uniform location in the shader.
         * @method setUniformInt
         * @param {(number) | (string)} _uniformLocation | uniformName
         * @param {(number)} _value
         */
        public setUniformInt (
            _uniformLocation_uniformName : (number) | (string), 
            _value : (number)
        ) : void;

        /**
         * Sets a uniform auto-binding.<br>
         * -- This method parses the passed in autoBinding string and attempts to convert it<br>
         * -- to an enumeration value. If it matches to one of the predefined strings, it will create a<br>
         * -- callback to get the correct value at runtime.<br>
         * -- param uniformName The name of the material parameter to store an auto-binding for.<br>
         * -- param autoBinding A string matching one of the built-in AutoBinding enum constants.
         * @method setParameterAutoBinding
         * @param {string} _uniformName
         * @param {string} _autoBinding
         */
        public setParameterAutoBinding (
            _uniformName : string, 
            _autoBinding : string 
        ) : void;

        /**
         * 
         * @method setUniformVec2v
         * @param {(number) | (string)} _uniformLocation | uniformName
         * @param {(number)} _size
         * @param {(ctype.value_type<cc.Point[]>)} _pointer
         */
        public setUniformVec2v (
            _uniformLocation_uniformName : (number) | (string), 
            _size : (number), 
            _pointer : (ctype.value_type<cc.Point[]>)
        ) : void;

        /**
         * Get the number of user defined uniform count.
         * @method getUniformCount
         * @return {number}
         */
        public getUniformCount (
        ) : number;

        /**
         * Apply attributes.<br>
         * -- param applyAttribFlags Call GL::enableVertexAttribs(_vertexAttribsFlags) or not.
         * @method applyAttributes
         */
        public applyAttributes (
        ) : void;

        /**
         *  Returns a new copy of the GLProgramState. The GLProgram is reused 
         * @method clone
         * @return {cc.GLProgramState}
         */
        public clone (
        ) : cc.GLProgramState;

        /**
         * @{ <br>
         * -- Setter and Getter of the owner GLProgram binded in this program state.
         * @method setGLProgram
         * @param {cc.GLProgram} _glprogram
         */
        public setGLProgram (
            _glprogram : cc.GLProgram 
        ) : void;

        /**
         * 
         * @method setUniformFloatv
         * @param {(number) | (string)} _uniformLocation | uniformName
         * @param {(number)} _size
         * @param {(number)} _pointer
         */
        public setUniformFloatv (
            _uniformLocation_uniformName : (number) | (string), 
            _size : (number), 
            _pointer : (number)
        ) : void;

        /**
         * 
         * @method getGLProgram
         * @return {cc.GLProgram}
         */
        public getGLProgram (
        ) : cc.GLProgram;

        /**
         * 
         * @method setUniformTexture
         * @param {(number) | (string)} _uniformLocation | uniformName
         * @param {(cc.Texture2D)} _texture
         */
        public setUniformTexture (
            _uniformLocation_uniformName : (number) | (string), 
            _texture : (cc.Texture2D)
        ) : void;

        /**
         * Apply user defined uniforms.
         * @method applyUniforms
         */
        public applyUniforms (
        ) : void;

        /**
         * 
         * @method setUniformFloat
         * @param {(number) | (string)} _uniformLocation | uniformName
         * @param {(number)} _value
         */
        public setUniformFloat (
            _uniformLocation_uniformName : (number) | (string), 
            _value : (number)
        ) : void;

        /**
         * 
         * @method setUniformMat4
         * @param {(number) | (string)} _uniformLocation | uniformName
         * @param {(ctype.value_type<cc.Mat4>)} _value
         */
        public setUniformMat4 (
            _uniformLocation_uniformName : (number) | (string), 
            _value : (ctype.value_type<cc.Mat4>)
        ) : void;

        /**
         * 
         * @method setUniformVec3v
         * @param {(number) | (string)} _uniformLocation | uniformName
         * @param {(number)} _size
         * @param {(ctype.value_type<cc.Vec3>)} _pointer
         */
        public setUniformVec3v (
            _uniformLocation_uniformName : (number) | (string), 
            _size : (number), 
            _pointer : (ctype.value_type<cc.Vec3>)
        ) : void;

        /**
         * Get the number of vertex attributes.
         * @method getVertexAttribCount
         * @return {number}
         */
        public getVertexAttribCount (
        ) : number;

        /**
         *  returns a new instance of GLProgramState for a given GLProgram 
         * @method create
         * @param {cc.GLProgram} _glprogram
         * @return {cc.GLProgramState}
         */
        public static create (
            _glprogram : cc.GLProgram 
        ) : cc.GLProgramState;

        /**
         *  gets-or-creates an instance of GLProgramState for the given GLProgramName & texture 
         * @method getOrCreateWithGLProgramName
         * @param {(string)} _glProgramName
         * @param {(cc.Texture2D)} _texture?
         * @return {cc.GLProgramState}
         */
        public static getOrCreateWithGLProgramName (
            _glProgramName : (string), 
            _texture? : (cc.Texture2D)
        ) : cc.GLProgramState;

        /**
         *  gets-or-creates an instance of GLProgramState for a given GLProgram 
         * @method getOrCreateWithGLProgram
         * @param {cc.GLProgram} _glprogram
         * @return {cc.GLProgramState}
         */
        public static getOrCreateWithGLProgram (
            _glprogram : cc.GLProgram 
        ) : cc.GLProgramState;

        /**
         *  gets-or-creates an instance of GLProgramState for given shaders 
         * @method getOrCreateWithShaders
         * @param {string} _vertexShader
         * @param {string} _fragShader
         * @param {string} _compileTimeDefines
         * @return {cc.GLProgramState}
         */
        public static getOrCreateWithShaders (
            _vertexShader : string, 
            _fragShader : string, 
            _compileTimeDefines : string 
        ) : cc.GLProgramState;

    }
    /**
     * @class SpriteFrame
     * @native
     */
    export class SpriteFrame 
    {

        // *****************************************************************
        // *****************************************************************
        // *****************************************************************
        // *****************************************************************
        // *****************************************************************
        
        public retain():void;
        public release():void;

        // *****************************************************************
        // *****************************************************************
        // *****************************************************************
        // *****************************************************************
        // *****************************************************************
        
        

        /**
         *  Set texture of the frame, the texture is retained.<br>
         * -- param pobTexture The texture of the sprite frame.
         * @method setTexture
         * @param {cc.Texture2D} _pobTexture
         */
        public setTexture (
            _pobTexture : cc.Texture2D 
        ) : void;

        /**
         *  Get texture of the frame.<br>
         * -- return The texture of the sprite frame.
         * @method getTexture
         * @return {cc.Texture2D}
         */
        public getTexture (
        ) : cc.Texture2D;

        /**
         *  Set offset of the frame.<br>
         * -- param offsetInPixels The offset of the sprite frame, in pixels.
         * @method setOffsetInPixels
         * @param {ctype.value_type<cc.Point>} _offsetInPixels
         */
        public setOffsetInPixels (
            _offsetInPixels : ctype.value_type<cc.Point> 
        ) : void;

        /**
         *  Get original size of the trimmed image.<br>
         * -- return The original size of the trimmed image, in pixels.
         * @method getOriginalSizeInPixels
         * @return {ctype.value_type<cc.Size>}
         */
        public getOriginalSizeInPixels (
        ) : ctype.value_type<cc.Size>;

        /**
         *  Set original size of the trimmed image.<br>
         * -- param sizeInPixels The original size of the trimmed image.
         * @method setOriginalSize
         * @param {ctype.value_type<cc.Size>} _sizeInPixels
         */
        public setOriginalSize (
            _sizeInPixels : ctype.value_type<cc.Size> 
        ) : void;

        /**
         *  Set rect of the sprite frame.<br>
         * -- param rectInPixels The rect of the sprite frame, in pixels.
         * @method setRectInPixels
         * @param {ctype.value_type<cc.Rect>} _rectInPixels
         */
        public setRectInPixels (
            _rectInPixels : ctype.value_type<cc.Rect> 
        ) : void;

        /**
         *  Get rect of the frame.<br>
         * -- return The rect of the sprite frame.
         * @method getRect
         * @return {ctype.value_type<cc.Rect>}
         */
        public getRect (
        ) : ctype.value_type<cc.Rect>;

        /**
         *  Set offset of the frame.<br>
         * -- param offsets The offset of the sprite frame.
         * @method setOffset
         * @param {ctype.value_type<cc.Point>} _offsets
         */
        public setOffset (
            _offsets : ctype.value_type<cc.Point> 
        ) : void;

        /**
         *  Initializes a SpriteFrame with a texture, rect, rotated, offset and originalSize in pixels.<br>
         * -- The originalSize is the size in pixels of the frame before being trimmed.<br>
         * -- since v1.1
         * @method initWithTextureFilename
         * @param {(string)} _filename
         * @param {(ctype.value_type<cc.Rect>)} _rect
         * @param {(boolean)} _rotated?
         * @param {(ctype.value_type<cc.Point>)} _offset?
         * @param {(ctype.value_type<cc.Size>)} _originalSize?
         * @return {boolean}
         */
        public initWithTextureFilename (
            _filename : (string), 
            _rect : (ctype.value_type<cc.Rect>), 
            _rotated? : (boolean), 
            _offset? : (ctype.value_type<cc.Point>), 
            _originalSize? : (ctype.value_type<cc.Size>)
        ) : boolean;

        /**
         *  Set rect of the frame.<br>
         * -- param rect The rect of the sprite.
         * @method setRect
         * @param {ctype.value_type<cc.Rect>} _rect
         */
        public setRect (
            _rect : ctype.value_type<cc.Rect> 
        ) : void;

        /**
         *  Initializes a SpriteFrame with a texture, rect, rotated, offset and originalSize in pixels.<br>
         * -- The originalSize is the size in points of the frame before being trimmed.
         * @method initWithTexture
         * @param {(cc.Texture2D)} _pobTexture
         * @param {(ctype.value_type<cc.Rect>)} _rect
         * @param {(boolean)} _rotated?
         * @param {(ctype.value_type<cc.Point>)} _offset?
         * @param {(ctype.value_type<cc.Size>)} _originalSize?
         * @return {boolean}
         */
        public initWithTexture (
            _pobTexture : (cc.Texture2D), 
            _rect : (ctype.value_type<cc.Rect>), 
            _rotated? : (boolean), 
            _offset? : (ctype.value_type<cc.Point>), 
            _originalSize? : (ctype.value_type<cc.Size>)
        ) : boolean;

        /**
         *  Get original size of the trimmed image.<br>
         * -- return The original size of the trimmed image.
         * @method getOriginalSize
         * @return {ctype.value_type<cc.Size>}
         */
        public getOriginalSize (
        ) : ctype.value_type<cc.Size>;

        /**
         * 
         * @method clone
         * @return {cc.SpriteFrame}
         */
        public clone (
        ) : cc.SpriteFrame;

        /**
         *  Get rect of the sprite frame.<br>
         * -- return The rect of the sprite frame, in pixels.
         * @method getRectInPixels
         * @return {ctype.value_type<cc.Rect>}
         */
        public getRectInPixels (
        ) : ctype.value_type<cc.Rect>;

        /**
         * Is the sprite frame rotated or not.<br>
         * -- return Is rotated if true.
         * @method isRotated
         * @return {boolean}
         */
        public isRotated (
        ) : boolean;

        /**
         *  Set rotated of the sprite frame.<br>
         * -- param rotated Rotated the sprite frame if true.
         * @method setRotated
         * @param {boolean} _rotated
         */
        public setRotated (
            _rotated : boolean 
        ) : void;

        /**
         *  Get offset of the frame.<br>
         * -- return The offset of the sprite frame.
         * @method getOffset
         * @return {ctype.value_type<cc.Point>}
         */
        public getOffset (
        ) : ctype.value_type<cc.Point>;

        /**
         *  Set original size of the trimmed image.<br>
         * -- param sizeInPixels The original size of the trimmed image, in pixels.
         * @method setOriginalSizeInPixels
         * @param {ctype.value_type<cc.Size>} _sizeInPixels
         */
        public setOriginalSizeInPixels (
            _sizeInPixels : ctype.value_type<cc.Size> 
        ) : void;

        /**
         *  Get offset of the frame.<br>
         * -- return The offset of the sprite frame, in pixels.
         * @method getOffsetInPixels
         * @return {ctype.value_type<cc.Point>}
         */
        public getOffsetInPixels (
        ) : ctype.value_type<cc.Point>;

        /**
         *  Create a SpriteFrame with a texture filename, rect, rotated, offset and originalSize in pixels.<br>
         * -- The originalSize is the size in pixels of the frame before being trimmed.<br>
         * -- param filename Texture filename<br>
         * -- param rect A specified rect.<br>
         * -- param rotated Is rotated if true.<br>
         * -- param offset A specified offset.<br>
         * -- param originalSize A specified original size.<br>
         * -- return An autoreleased SpriteFrame object.
         * @method create
         * @param {(string)} _filename
         * @param {(ctype.value_type<cc.Rect>)} _rect
         * @param {(boolean)} _rotated?
         * @param {(ctype.value_type<cc.Point>)} _offset?
         * @param {(ctype.value_type<cc.Size>)} _originalSize?
         * @return {cc.SpriteFrame}
         */
        public static create (
            _filename : (string), 
            _rect : (ctype.value_type<cc.Rect>), 
            _rotated? : (boolean), 
            _offset? : (ctype.value_type<cc.Point>), 
            _originalSize? : (ctype.value_type<cc.Size>)
        ) : cc.SpriteFrame;

        /**
         *  Create a SpriteFrame with a texture, rect, rotated, offset and originalSize in pixels.<br>
         * -- The originalSize is the size in points of the frame before being trimmed.<br>
         * -- param pobTexture The texture pointer.<br>
         * -- param rect A specified rect.<br>
         * -- param rotated Is rotated if true.<br>
         * -- param offset A specified offset.<br>
         * -- param originalSize A specified original size.<br>
         * -- return An autoreleased SpriteFrame object.
         * @method createWithTexture
         * @param {(cc.Texture2D)} _pobTexture
         * @param {(ctype.value_type<cc.Rect>)} _rect
         * @param {(boolean)} _rotated?
         * @param {(ctype.value_type<cc.Point>)} _offset?
         * @param {(ctype.value_type<cc.Size>)} _originalSize?
         * @return {cc.SpriteFrame}
         */
        public static createWithTexture (
            _pobTexture : (cc.Texture2D), 
            _rect : (ctype.value_type<cc.Rect>), 
            _rotated? : (boolean), 
            _offset? : (ctype.value_type<cc.Point>), 
            _originalSize? : (ctype.value_type<cc.Size>)
        ) : cc.SpriteFrame;

        /**
         * lua NA
         * @method SpriteFrame
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class AnimationFrame
     * @native
     */
    export class AnimationFrame 
    {

        /**
         *  Set the SpriteFrame.<br>
         * -- param frame A SpriteFrame will be used.
         * @method setSpriteFrame
         * @param {cc.SpriteFrame} _frame
         */
        public setSpriteFrame (
            _frame : cc.SpriteFrame 
        ) : void;

        /**
         * 
         * @method getUserInfo
         * @return {any}
         */
        public getUserInfo (
        ) : any;

        /**
         *  Sets the units of time the frame takes.<br>
         * -- param delayUnits The units of time the frame takes.
         * @method setDelayUnits
         * @param {number} _delayUnits
         */
        public setDelayUnits (
            _delayUnits : number 
        ) : void;

        /**
         * 
         * @method clone
         * @return {cc.AnimationFrame}
         */
        public clone (
        ) : cc.AnimationFrame;

        /**
         *  Return a SpriteFrameName to be used.<br>
         * -- return a SpriteFrameName to be used.
         * @method getSpriteFrame
         * @return {cc.SpriteFrame}
         */
        public getSpriteFrame (
        ) : cc.SpriteFrame;

        /**
         *  Gets the units of time the frame takes.<br>
         * -- return The units of time the frame takes.
         * @method getDelayUnits
         * @return {number}
         */
        public getDelayUnits (
        ) : number;

        /**
         *  Sets user information.<br>
         * -- param userInfo A dictionary as UserInfo.
         * @method setUserInfo
         * @param {any} _userInfo
         */
        public setUserInfo (
            _userInfo : any 
        ) : void;

        /**
         *  initializes the animation frame with a spriteframe, number of delay units and a notification user info 
         * @method initWithSpriteFrame
         * @param {cc.SpriteFrame} _spriteFrame
         * @param {number} _delayUnits
         * @param {any} _userInfo
         * @return {boolean}
         */
        public initWithSpriteFrame (
            _spriteFrame : cc.SpriteFrame, 
            _delayUnits : number, 
            _userInfo : any 
        ) : boolean;

        /**
         * Creates the animation frame with a spriteframe, number of delay units and a notification user info.<br>
         * -- param spriteFrame The animation frame with a spriteframe.<br>
         * -- param delayUnits Number of delay units.<br>
         * -- param userInfo A notification user info.<br>
         * -- since 3.0
         * @method create
         * @param {cc.SpriteFrame} _spriteFrame
         * @param {number} _delayUnits
         * @param {any} _userInfo
         * @return {cc.AnimationFrame}
         */
        public static create (
            _spriteFrame : cc.SpriteFrame, 
            _delayUnits : number, 
            _userInfo : any 
        ) : cc.AnimationFrame;

        /**
         * js ctor
         * @method AnimationFrame
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class Animation
     * @native
     */
    export class Animation 
    {

        /**
         *  Gets the times the animation is going to loop. 0 means animation is not animated. 1, animation is executed one time, ... <br>
         * -- return The times the animation is going to loop.
         * @method getLoops
         * @return {number}
         */
        public getLoops (
        ) : number;

        /**
         *  Adds a SpriteFrame to a Animation.<br>
         * -- param frame The frame will be added with one "delay unit".
         * @method addSpriteFrame
         * @param {cc.SpriteFrame} _frame
         */
        public addSpriteFrame (
            _frame : cc.SpriteFrame 
        ) : void;

        /**
         *  Sets whether to restore the original frame when animation finishes. <br>
         * -- param restoreOriginalFrame Whether to restore the original frame when animation finishes.
         * @method setRestoreOriginalFrame
         * @param {boolean} _restoreOriginalFrame
         */
        public setRestoreOriginalFrame (
            _restoreOriginalFrame : boolean 
        ) : void;

        /**
         * 
         * @method clone
         * @return {cc.Animation}
         */
        public clone (
        ) : cc.Animation;

        /**
         *  Gets the duration in seconds of the whole animation. It is the result of totalDelayUnits * delayPerUnit.<br>
         * -- return Result of totalDelayUnits * delayPerUnit.
         * @method getDuration
         * @return {number}
         */
        public getDuration (
        ) : number;

        /**
         *  Initializes a Animation with AnimationFrame.<br>
         * -- since v2.0
         * @method initWithAnimationFrames
         * @param {cc.AnimationFrame[]} _arrayOfAnimationFrameNames
         * @param {number} _delayPerUnit
         * @param {number} _loops
         * @return {boolean}
         */
        public initWithAnimationFrames (
            _arrayOfAnimationFrameNames : cc.AnimationFrame[], 
            _delayPerUnit : number, 
            _loops : number 
        ) : boolean;

        /**
         *  Initializes a Animation. 
         * @method init
         * @return {boolean}
         */
        public init (
        ) : boolean;

        /**
         *  Sets the array of AnimationFrames. <br>
         * -- param frames The array of AnimationFrames.
         * @method setFrames
         * @param {cc.AnimationFrame[]} _frames
         */
        public setFrames (
            _frames : cc.AnimationFrame[] 
        ) : void;

        /**
         *  Gets the array of AnimationFrames.<br>
         * -- return The array of AnimationFrames.
         * @method getFrames
         * @return {cc.AnimationFrame[]}
         */
        public getFrames (
        ) : cc.AnimationFrame[];

        /**
         *  Sets the times the animation is going to loop. 0 means animation is not animated. 1, animation is executed one time, ... <br>
         * -- param loops The times the animation is going to loop.
         * @method setLoops
         * @param {number} _loops
         */
        public setLoops (
            _loops : number 
        ) : void;

        /**
         *  Sets the delay in seconds of the "delay unit".<br>
         * -- param delayPerUnit The delay in seconds of the "delay unit".
         * @method setDelayPerUnit
         * @param {number} _delayPerUnit
         */
        public setDelayPerUnit (
            _delayPerUnit : number 
        ) : void;

        /**
         *  Adds a frame with an image filename. Internally it will create a SpriteFrame and it will add it.<br>
         * -- The frame will be added with one "delay unit".<br>
         * -- Added to facilitate the migration from v0.8 to v0.9.<br>
         * -- param filename The path of SpriteFrame.
         * @method addSpriteFrameWithFile
         * @param {string} _filename
         */
        public addSpriteFrameWithFile (
            _filename : string 
        ) : void;

        /**
         *  Gets the total Delay units of the Animation. <br>
         * -- return The total Delay units of the Animation.
         * @method getTotalDelayUnits
         * @return {number}
         */
        public getTotalDelayUnits (
        ) : number;

        /**
         *  Gets the delay in seconds of the "delay unit".<br>
         * -- return The delay in seconds of the "delay unit".
         * @method getDelayPerUnit
         * @return {number}
         */
        public getDelayPerUnit (
        ) : number;

        /**
         *  Initializes a Animation with frames and a delay between frames.<br>
         * -- since v0.99.5
         * @method initWithSpriteFrames
         * @param {cc.SpriteFrame[]} _arrayOfSpriteFrameNames
         * @param {number} _delay
         * @param {number} _loops
         * @return {boolean}
         */
        public initWithSpriteFrames (
            _arrayOfSpriteFrameNames : cc.SpriteFrame[], 
            _delay : number, 
            _loops : number 
        ) : boolean;

        /**
         *  Checks whether to restore the original frame when animation finishes. <br>
         * -- return Restore the original frame when animation finishes.
         * @method getRestoreOriginalFrame
         * @return {boolean}
         */
        public getRestoreOriginalFrame (
        ) : boolean;

        /**
         *  Adds a frame with a texture and a rect. Internally it will create a SpriteFrame and it will add it.<br>
         * -- The frame will be added with one "delay unit".<br>
         * -- Added to facilitate the migration from v0.8 to v0.9.<br>
         * -- param pobTexture A frame with a texture.<br>
         * -- param rect The Texture of rect.
         * @method addSpriteFrameWithTexture
         * @param {cc.Texture2D} _pobTexture
         * @param {ctype.value_type<cc.Rect>} _rect
         */
        public addSpriteFrameWithTexture (
            _pobTexture : cc.Texture2D, 
            _rect : ctype.value_type<cc.Rect> 
        ) : void;

        /**
         * 
         * @method create
         * @param {(cc.AnimationFrame[])} _arrayOfAnimationFrameNames?
         * @param {(number)} _delayPerUnit?
         * @param {(number)} _loops?
         * @return {cc.Animation}
         */
        public static createWithAnimationFrames (
            _arrayOfAnimationFrameNames? : (cc.AnimationFrame[]), 
            _delayPerUnit? : (number), 
            _loops? : (number)
        ) : cc.Animation;

        /**
         * 
         * @method createWithSpriteFrames
         * @param {cc.SpriteFrame[]} _arrayOfSpriteFrameNames
         * @param {number} _delay
         * @param {number} _loops
         * @return {cc.Animation}
         */
        public static createWithSpriteFrames (
            _arrayOfSpriteFrameNames : cc.SpriteFrame[], 
            _delay : number, 
            _loops : number 
        ) : cc.Animation;

        /**
         * 
         * @method Animation
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class ActionInterval
     * @native
     */
    export abstract class ActionInterval 
        extends cc.FiniteTimeAction
    {
        /**
         * Get this action speed.
         * @return {Number}
         */
        public getSpeed():number;

        /**
         * Set this action speed.
         * @param {Number} speed
         * @returns {cc.ActionInterval}
         */
        public setSpeed(speed:number):ActionInterval;
        
        
        // *****************************************************************
        // *****************************************************************
        // ******IMPORTANT NOTE: *******************************************
        // This section is relay on 
        // cocos2d-x/cocos/scripting/js-bindings/manual/cocos2d_specifics.cpp
        // *****************************************************************
        // *****************************************************************
        
        
        /**
         * Repeats an action a number of times.
         * To repeat an action forever use the CCRepeatForever action.
         * @param times
         * @returns {cc.ActionInterval}
         */
        public repeat(times:number):ActionInterval;

        /**
         * Repeats an action for ever.  <br/>
         * To repeat the an action for a limited number of times use the Repeat action. <br/>
         * @returns {cc.ActionInterval}
         */
        public repeatForever():ActionInterval;
        
        /**
         * Implementation of ease motion.
         *
         * @example
         * //example
         * action.easeing(cc.easeIn(3.0));
         * @param {Object} easeObj
         * @returns {cc.ActionInterval}
         */
        // TODO: Shouldn't this parameter type be ActionEase instead of any?
        public easing(easeObj:any):ActionInterval;
        
        /**
         * Changes the speed of an action, making it take longer (speed>1)
         * or less (speed<1) time. <br/>
         * Useful to simulate 'slow motion' or 'fast forward' effect.
         *
         * @param speed
         * @returns {cc.Action}
         */
        public speed(speed:number):ActionInterval;

        // *****************************************************************
        // *****************************************************************
        // *****************************************************************
        // *****************************************************************
        // *****************************************************************
        // *****************************************************************
        
        

        /**
         *  Gets the amplitude rate, extension in GridAction<br>
         * -- return  The amplitude rate.
         * @method getAmplitudeRate
         * @return {number}
         */
        public getAmplitudeRate (
        ) : number;

        /**
         *  initializes the action 
         * @method initWithDuration
         * @param {number} _d
         * @return {boolean}
         */
        public initWithDuration (
            _d : number,

            // PURE MANUAL HACK
            ...args : any[]
            // PURE MANUAL HACK
        ) : boolean;

        /**
         *  Sets the amplitude rate, extension in GridAction<br>
         * -- param amp   The amplitude rate.
         * @method setAmplitudeRate
         * @param {number} _amp
         */
        public setAmplitudeRate (
            _amp : number 
        ) : void;

        /**
         *  How many seconds had elapsed since the actions started to run.<br>
         * -- return The seconds had elapsed since the actions started to run.
         * @method getElapsed
         * @return {number}
         */
        public getElapsed (
        ) : number;

    }
    /**
     * @class Sequence
     * @native
     */
    export class Sequence 
        extends cc.ActionInterval
    {

        /**
         * 
         * @method init
         * @param {cc.FiniteTimeAction[]} _arrayOfActions
         * @return {boolean}
         */
        public init (
            _arrayOfActions : cc.FiniteTimeAction[] 
        ) : boolean;

        /**
         *  initializes the action 
         * @method initWithTwoActions
         * @param {cc.FiniteTimeAction} _pActionOne
         * @param {cc.FiniteTimeAction} _pActionTwo
         * @return {boolean}
         */
        public initWithTwoActions (
            _pActionOne : cc.FiniteTimeAction, 
            _pActionTwo : cc.FiniteTimeAction 
        ) : boolean;

        /**
         * 
         * @method Sequence
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class Repeat
     * @native
     */
    export class Repeat 
        extends cc.ActionInterval
    {

        /**
         *  Sets the inner action.<br>
         * -- param action The inner action.
         * @method setInnerAction
         * @param {cc.FiniteTimeAction} _action
         */
        public setInnerAction (
            _action : cc.FiniteTimeAction 
        ) : void;

        /**
         *  initializes a Repeat action. Times is an unsigned integer between 1 and pow(2,30) 
         * @method initWithAction
         * @param {cc.FiniteTimeAction} _pAction
         * @param {number} _times
         * @return {boolean}
         */
        public initWithAction (
            _pAction : cc.FiniteTimeAction, 
            _times : number 
        ) : boolean;

        /**
         *  Gets the inner action.<br>
         * -- return The inner action.
         * @method getInnerAction
         * @return {cc.FiniteTimeAction}
         */
        public getInnerAction (
        ) : cc.FiniteTimeAction;

        /**
         *  Creates a Repeat action. Times is an unsigned integer between 1 and pow(2,30).<br>
         * -- param action The action needs to repeat.<br>
         * -- param times The repeat times.<br>
         * -- return An autoreleased Repeat object.
         * @method create
         * @param {cc.FiniteTimeAction} _action
         * @param {number} _times
         * @return {cc.Repeat}
         */
        public static create (
            _action : cc.FiniteTimeAction, 
            _times : number 
        ) : cc.Repeat;

        /**
         * 
         * @method Repeat
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class RepeatForever
     * @native
     */
    export class RepeatForever 
        extends cc.ActionInterval
    {

        /**
         *  Sets the inner action.<br>
         * -- param action The inner action.
         * @method setInnerAction
         * @param {cc.ActionInterval} _action
         */
        public setInnerAction (
            _action : cc.ActionInterval 
        ) : void;

        /**
         *  initializes the action 
         * @method initWithAction
         * @param {cc.ActionInterval} _action
         * @return {boolean}
         */
        public initWithAction (
            _action : cc.ActionInterval 
        ) : boolean;

        /**
         *  Gets the inner action.<br>
         * -- return The inner action.
         * @method getInnerAction
         * @return {cc.ActionInterval}
         */
        public getInnerAction (
        ) : cc.ActionInterval;

        /**
         *  Creates the action.<br>
         * -- param action The action need to repeat forever.<br>
         * -- return An autoreleased RepeatForever object.
         * @method create
         * @param {cc.ActionInterval} _action
         * @return {cc.RepeatForever}
         */
        public static create (
            _action : cc.ActionInterval 
        ) : cc.RepeatForever;

        /**
         * 
         * @method RepeatForever
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class Spawn
     * @native
     */
    export class Spawn 
        extends cc.ActionInterval
    {

        /**
         * 
         * @method init
         * @param {cc.FiniteTimeAction[]} _arrayOfActions
         * @return {boolean}
         */
        public init (
            _arrayOfActions : cc.FiniteTimeAction[] 
        ) : boolean;

        /**
         *  initializes the Spawn action with the 2 actions to spawn 
         * @method initWithTwoActions
         * @param {cc.FiniteTimeAction} _action1
         * @param {cc.FiniteTimeAction} _action2
         * @return {boolean}
         */
        public initWithTwoActions (
            _action1 : cc.FiniteTimeAction, 
            _action2 : cc.FiniteTimeAction 
        ) : boolean;

        /**
         * 
         * @method Spawn
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class RotateTo
     * @native
     */
    export class RotateTo 
        extends cc.ActionInterval
    {

        /**
         * initializes the action<br>
         * -- param duration in seconds
         * @method initWithDuration
         * @param {(number)} _duration
         * @param {(ctype.value_type<cc.Vec3>) | (number)} _dstAngle3D | dstAngleX
         * @param {(number)} _dstAngleY?
         * @return {boolean}
         */
        public initWithDuration (
            _duration : (number), 
            _dstAngle3D_dstAngleX : (ctype.value_type<cc.Vec3>) | (number), 
            _dstAngleY? : (number)
        ) : boolean;

        /**
         * Creates the action.<br>
         * -- param duration Duration time, in seconds.<br>
         * -- param dstAngle In degreesCW.<br>
         * -- return An autoreleased RotateTo object.
         * @method create
         * @param {(number)} _duration
         * @param {(number) | (ctype.value_type<cc.Vec3>)} _dstAngle | dstAngleX | dstAngle3D
         * @param {(number)} _dstAngleY?
         * @return {cc.RotateTo}
         */
        public static create (
            _duration : (number), 
            _dstAngle_dstAngleX_dstAngle3D : (number) | (ctype.value_type<cc.Vec3>), 
            _dstAngleY? : (number)
        ) : cc.RotateTo;

        /**
         * 
         * @method RotateTo
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class RotateBy
     * @native
     */
    export class RotateBy 
        extends cc.ActionInterval
    {

        /**
         * warning The physics body contained in Node doesn't support rotate with different x and y angle.<br>
         * -- param deltaAngleZ_X in degreesCW<br>
         * -- param deltaAngleZ_Y in degreesCW
         * @method initWithDuration
         * @param {(number)} _duration
         * @param {(number) | (ctype.value_type<cc.Vec3>)} _deltaAngleZ_X | deltaAngle | deltaAngle3D
         * @param {(number)} _deltaAngleZ_Y?
         * @return {boolean}
         */
        public initWithDuration (
            _duration : (number), 
            _deltaAngleZ_X_deltaAngle_deltaAngle3D : (number) | (ctype.value_type<cc.Vec3>), 
            _deltaAngleZ_Y? : (number)
        ) : boolean;

        /**
         * Creates the action with separate rotation angles.<br>
         * -- param duration Duration time, in seconds.<br>
         * -- param deltaAngleZ_X In degreesCW.<br>
         * -- param deltaAngleZ_Y In degreesCW.<br>
         * -- return An autoreleased RotateBy object.<br>
         * -- warning The physics body contained in Node doesn't support rotate with different x and y angle.
         * @method create
         * @param {(number)} _duration
         * @param {(number) | (ctype.value_type<cc.Vec3>)} _deltaAngleZ_X | deltaAngle | deltaAngle3D
         * @param {(number)} _deltaAngleZ_Y?
         * @return {cc.RotateBy}
         */
        public static create (
            _duration : (number), 
            _deltaAngleZ_X_deltaAngle_deltaAngle3D : (number) | (ctype.value_type<cc.Vec3>), 
            _deltaAngleZ_Y? : (number)
        ) : cc.RotateBy;

        /**
         * 
         * @method RotateBy
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class MoveBy
     * @native
     */
    export class MoveBy 
        extends cc.ActionInterval
    {

        /**
         * 
         * @method initWithDuration
         * @param {(number)} _duration
         * @param {(ctype.value_type<cc.Vec3>) | (ctype.value_type<cc.Point>)} _deltaPosition
         * @return {boolean}
         */
        public initWithDuration (
            _duration : (number), 
            _deltaPosition : (ctype.value_type<cc.Vec3>) | (ctype.value_type<cc.Point>)
        ) : boolean;

        /**
         * Creates the action.<br>
         * -- param duration Duration time, in seconds.<br>
         * -- param deltaPosition The delta distance in 3d, it's a Vec3 type.<br>
         * -- return An autoreleased MoveBy object.
         * @method create
         * @param {(number)} _duration
         * @param {(ctype.value_type<cc.Vec3>) | (ctype.value_type<cc.Point>)} _deltaPosition
         * @return {cc.MoveBy}
         */
        public static create (
            _duration : (number), 
            _deltaPosition : (ctype.value_type<cc.Vec3>) | (ctype.value_type<cc.Point>)
        ) : cc.MoveBy;

        /**
         * 
         * @method MoveBy
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class MoveTo
     * @native
     */
    export class MoveTo 
        extends cc.MoveBy
    {

        /**
         * initializes the action<br>
         * -- param duration in seconds
         * @method initWithDuration
         * @param {(number)} _duration
         * @param {(ctype.value_type<cc.Vec3>) | (ctype.value_type<cc.Point>)} _position
         * @return {boolean}
         */
        public initWithDuration (
            _duration : (number), 
            _position : (ctype.value_type<cc.Vec3>) | (ctype.value_type<cc.Point>)
        ) : boolean;

        /**
         * Creates the action.<br>
         * -- param duration Duration time, in seconds.<br>
         * -- param position The destination position in 3d.<br>
         * -- return An autoreleased MoveTo object.
         * @method create
         * @param {(number)} _duration
         * @param {(ctype.value_type<cc.Vec3>) | (ctype.value_type<cc.Point>)} _position
         * @return {cc.MoveTo}
         */
        public static create (
            _duration : (number), 
            _position : (ctype.value_type<cc.Vec3>) | (ctype.value_type<cc.Point>)
        ) : cc.MoveTo;

        /**
         * 
         * @method MoveTo
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class SkewTo
     * @native
     */
    export class SkewTo 
        extends cc.ActionInterval
    {

        /**
         * param t In seconds.
         * @method initWithDuration
         * @param {number} _t
         * @param {number} _sx
         * @param {number} _sy
         * @return {boolean}
         */
        public initWithDuration (
            _t : number, 
            _sx : number, 
            _sy : number 
        ) : boolean;

        /**
         * Creates the action.<br>
         * -- param t Duration time, in seconds.<br>
         * -- param sx Skew x angle.<br>
         * -- param sy Skew y angle.<br>
         * -- return An autoreleased SkewTo object.
         * @method create
         * @param {number} _t
         * @param {number} _sx
         * @param {number} _sy
         * @return {cc.SkewTo}
         */
        public static create (
            _t : number, 
            _sx : number, 
            _sy : number 
        ) : cc.SkewTo;

        /**
         * 
         * @method SkewTo
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class SkewBy
     * @native
     */
    export class SkewBy 
        extends cc.SkewTo
    {

        /**
         * param t In seconds.
         * @method initWithDuration
         * @param {number} _t
         * @param {number} _sx
         * @param {number} _sy
         * @return {boolean}
         */
        public initWithDuration (
            _t : number, 
            _sx : number, 
            _sy : number 
        ) : boolean;

        /**
         * Creates the action.<br>
         * -- param t Duration time, in seconds.<br>
         * -- param deltaSkewX Skew x delta angle.<br>
         * -- param deltaSkewY Skew y delta angle.<br>
         * -- return An autoreleased SkewBy object.
         * @method create
         * @param {number} _t
         * @param {number} _deltaSkewX
         * @param {number} _deltaSkewY
         * @return {cc.SkewBy}
         */
        public static create (
            _t : number, 
            _deltaSkewX : number, 
            _deltaSkewY : number 
        ) : cc.SkewBy;

        /**
         * 
         * @method SkewBy
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class JumpBy
     * @native
     */
    export class JumpBy 
        extends cc.ActionInterval
    {

        /**
         * initializes the action<br>
         * -- param duration in seconds
         * @method initWithDuration
         * @param {number} _duration
         * @param {ctype.value_type<cc.Point>} _position
         * @param {number} _height
         * @param {number} _jumps
         * @return {boolean}
         */
        public initWithDuration (
            _duration : number, 
            _position : ctype.value_type<cc.Point>, 
            _height : number, 
            _jumps : number 
        ) : boolean;

        /**
         * Creates the action.<br>
         * -- param duration Duration time, in seconds.<br>
         * -- param position The jumping distance.<br>
         * -- param height The jumping height.<br>
         * -- param jumps The jumping times.<br>
         * -- return An autoreleased JumpBy object.
         * @method create
         * @param {number} _duration
         * @param {ctype.value_type<cc.Point>} _position
         * @param {number} _height
         * @param {number} _jumps
         * @return {cc.JumpBy}
         */
        public static create (
            _duration : number, 
            _position : ctype.value_type<cc.Point>, 
            _height : number, 
            _jumps : number 
        ) : cc.JumpBy;

        /**
         * 
         * @method JumpBy
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class JumpTo
     * @native
     */
    export class JumpTo 
        extends cc.JumpBy
    {

        /**
         * initializes the action<br>
         * -- param duration In seconds.
         * @method initWithDuration
         * @param {number} _duration
         * @param {ctype.value_type<cc.Point>} _position
         * @param {number} _height
         * @param {number} _jumps
         * @return {boolean}
         */
        public initWithDuration (
            _duration : number, 
            _position : ctype.value_type<cc.Point>, 
            _height : number, 
            _jumps : number 
        ) : boolean;

        /**
         * Creates the action.<br>
         * -- param duration Duration time, in seconds.<br>
         * -- param position The jumping destination position.<br>
         * -- param height The jumping height.<br>
         * -- param jumps The jumping times.<br>
         * -- return An autoreleased JumpTo object.
         * @method create
         * @param {number} _duration
         * @param {ctype.value_type<cc.Point>} _position
         * @param {number} _height
         * @param {number} _jumps
         * @return {cc.JumpTo}
         */
        public static create (
            _duration : number, 
            _position : ctype.value_type<cc.Point>, 
            _height : number, 
            _jumps : number 
        ) : cc.JumpTo;

        /**
         * 
         * @method JumpTo
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class BezierBy
     * @native
     */
    export class BezierBy 
        extends cc.ActionInterval
    {

        /**
         * 
         * @method BezierBy
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class BezierTo
     * @native
     */
    export class BezierTo 
        extends cc.BezierBy
    {

        /**
         * 
         * @method BezierTo
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class ScaleTo
     * @native
     */
    export class ScaleTo 
        extends cc.ActionInterval
    {

        /**
         * initializes the action with and X factor and a Y factor <br>
         * -- param duration in seconds
         * @method initWithDuration
         * @param {(number)} _duration
         * @param {(number)} _sx | s
         * @param {(number)} _sy?
         * @param {(number)} _sz?
         * @return {boolean}
         */
        public initWithDuration (
            _duration : (number), 
            _sx_s : (number), 
            _sy? : (number), 
            _sz? : (number)
        ) : boolean;

        /**
         * Creates the action with and X factor and a Y factor.<br>
         * -- param duration Duration time, in seconds.<br>
         * -- param sx Scale factor of x.<br>
         * -- param sy Scale factor of y.<br>
         * -- return An autoreleased ScaleTo object.
         * @method create
         * @param {(number)} _duration
         * @param {(number)} _sx | s
         * @param {(number)} _sy?
         * @param {(number)} _sz?
         * @return {cc.ScaleTo}
         */
        public static create (
            _duration : (number), 
            _sx_s : (number), 
            _sy? : (number), 
            _sz? : (number)
        ) : cc.ScaleTo;

        /**
         * 
         * @method ScaleTo
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class ScaleBy
     * @native
     */
    export class ScaleBy 
        extends cc.ScaleTo
    {

        /**
         * Creates the action with and X factor and a Y factor.<br>
         * -- param duration Duration time, in seconds.<br>
         * -- param sx Scale factor of x.<br>
         * -- param sy Scale factor of y.<br>
         * -- return An autoreleased ScaleBy object.
         * @method create
         * @param {(number)} _duration
         * @param {(number)} _sx | s
         * @param {(number)} _sy?
         * @param {(number)} _sz?
         * @return {cc.ScaleBy}
         */
        public static create (
            _duration : (number), 
            _sx_s : (number), 
            _sy? : (number), 
            _sz? : (number)
        ) : cc.ScaleBy;

        /**
         * 
         * @method ScaleBy
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class Blink
     * @native
     */
    export class Blink 
        extends cc.ActionInterval
    {

        /**
         * initializes the action <br>
         * -- param duration in seconds
         * @method initWithDuration
         * @param {number} _duration
         * @param {number} _blinks
         * @return {boolean}
         */
        public initWithDuration (
            _duration : number, 
            _blinks : number 
        ) : boolean;

        /**
         * Creates the action.<br>
         * -- param duration Duration time, in seconds.<br>
         * -- param blinks Blink times.<br>
         * -- return An autoreleased Blink object.
         * @method create
         * @param {number} _duration
         * @param {number} _blinks
         * @return {cc.Blink}
         */
        public static create (
            _duration : number, 
            _blinks : number 
        ) : cc.Blink;

        /**
         * 
         * @method Blink
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class FadeTo
     * @native
     */
    export class FadeTo 
        extends cc.ActionInterval
    {

        /**
         * initializes the action with duration and opacity <br>
         * -- param duration in seconds
         * @method initWithDuration
         * @param {number} _duration
         * @param {number} _opacity
         * @return {boolean}
         */
        public initWithDuration (
            _duration : number, 
            _opacity : number 
        ) : boolean;

        /**
         * Creates an action with duration and opacity.<br>
         * -- param duration Duration time, in seconds.<br>
         * -- param opacity A certain opacity, the range is from 0 to 255.<br>
         * -- return An autoreleased FadeTo object.
         * @method create
         * @param {number} _duration
         * @param {number} _opacity
         * @return {cc.FadeTo}
         */
        public static create (
            _duration : number, 
            _opacity : number 
        ) : cc.FadeTo;

        /**
         * 
         * @method FadeTo
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class FadeIn
     * @native
     */
    export class FadeIn 
        extends cc.FadeTo
    {

        /**
         * js NA
         * @method setReverseAction
         * @param {cc.FadeTo} _ac
         */
        public setReverseAction (
            _ac : cc.FadeTo 
        ) : void;

        /**
         * Creates the action.<br>
         * -- param d Duration time, in seconds.<br>
         * -- return An autoreleased FadeIn object.
         * @method create
         * @param {number} _d
         * @return {cc.FadeIn}
         */
        public static create (
            _d : number 
        ) : cc.FadeIn;

        /**
         * 
         * @method FadeIn
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class FadeOut
     * @native
     */
    export class FadeOut 
        extends cc.FadeTo
    {

        /**
         * js NA
         * @method setReverseAction
         * @param {cc.FadeTo} _ac
         */
        public setReverseAction (
            _ac : cc.FadeTo 
        ) : void;

        /**
         * Creates the action.<br>
         * -- param d Duration time, in seconds.
         * @method create
         * @param {number} _d
         * @return {cc.FadeOut}
         */
        public static create (
            _d : number 
        ) : cc.FadeOut;

        /**
         * 
         * @method FadeOut
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class TintTo
     * @native
     */
    export class TintTo 
        extends cc.ActionInterval
    {

        /**
         *  initializes the action with duration and color 
         * @method initWithDuration
         * @param {number} _duration
         * @param {number} _red
         * @param {number} _green
         * @param {number} _blue
         * @return {boolean}
         */
        public initWithDuration (
            _duration : number, 
            _red : number, 
            _green : number, 
            _blue : number 
        ) : boolean;

        /**
         * Creates an action with duration and color.<br>
         * -- param duration Duration time, in seconds.<br>
         * -- param color It's a Color3B type.<br>
         * -- return An autoreleased TintTo object.
         * @method create
         * @param {(number)} _duration
         * @param {(ctype.value_type<cc.Color>) | (number)} _color | red
         * @param {(number)} _green?
         * @param {(number)} _blue?
         * @return {cc.TintTo}
         */
        public static create (
            _duration : (number), 
            _color_red : (ctype.value_type<cc.Color>) | (number), 
            _green? : (number), 
            _blue? : (number)
        ) : cc.TintTo;

        /**
         * 
         * @method TintTo
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class TintBy
     * @native
     */
    export class TintBy 
        extends cc.ActionInterval
    {

        /**
         *  initializes the action with duration and color 
         * @method initWithDuration
         * @param {number} _duration
         * @param {number} _deltaRed
         * @param {number} _deltaGreen
         * @param {number} _deltaBlue
         * @return {boolean}
         */
        public initWithDuration (
            _duration : number, 
            _deltaRed : number, 
            _deltaGreen : number, 
            _deltaBlue : number 
        ) : boolean;

        /**
         * Creates an action with duration and color.<br>
         * -- param duration Duration time, in seconds.<br>
         * -- param deltaRed Delta red color.<br>
         * -- param deltaGreen Delta green color.<br>
         * -- param deltaBlue Delta blue color.<br>
         * -- return An autoreleased TintBy object.
         * @method create
         * @param {number} _duration
         * @param {number} _deltaRed
         * @param {number} _deltaGreen
         * @param {number} _deltaBlue
         * @return {cc.TintBy}
         */
        public static create (
            _duration : number, 
            _deltaRed : number, 
            _deltaGreen : number, 
            _deltaBlue : number 
        ) : cc.TintBy;

        /**
         * 
         * @method TintBy
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class DelayTime
     * @native
     */
    export class DelayTime 
        extends cc.ActionInterval
    {

        /**
         * Creates the action.<br>
         * -- param d Duration time, in seconds.<br>
         * -- return An autoreleased DelayTime object.
         * @method create
         * @param {number} _d
         * @return {cc.DelayTime}
         */
        public static create (
            _d : number 
        ) : cc.DelayTime;

        /**
         * 
         * @method DelayTime
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class ReverseTime
     * @native
     */
    export class ReverseTime 
        extends cc.ActionInterval
    {

        /**
         *  initializes the action 
         * @method initWithAction
         * @param {cc.FiniteTimeAction} _action
         * @return {boolean}
         */
        public initWithAction (
            _action : cc.FiniteTimeAction 
        ) : boolean;

        /**
         *  Creates the action.<br>
         * -- param action a certain action.<br>
         * -- return An autoreleased ReverseTime object.
         * @method create
         * @param {cc.FiniteTimeAction} _action
         * @return {cc.ReverseTime}
         */
        public static create (
            _action : cc.FiniteTimeAction 
        ) : cc.ReverseTime;

        /**
         * 
         * @method ReverseTime
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class Animate
     * @native
     */
    export class Animate 
        extends cc.ActionInterval
    {

        /**
         *  initializes the action with an Animation and will restore the original frame when the animation is over 
         * @method initWithAnimation
         * @param {cc.Animation} _animation
         * @return {boolean}
         */
        public initWithAnimation (
            _animation : cc.Animation 
        ) : boolean;

        /**
         * 
         * @method getAnimation
         * @return {cc.Animation}
         */
        public getAnimation (
        ) : cc.Animation;

        /**
         * Gets the index of sprite frame currently displayed.<br>
         * -- return int  the index of sprite frame currently displayed.
         * @method getCurrentFrameIndex
         * @return {number}
         */
        public getCurrentFrameIndex (
        ) : number;

        /**
         *  Sets the Animation object to be animated <br>
         * -- param animation certain animation.
         * @method setAnimation
         * @param {cc.Animation} _animation
         */
        public setAnimation (
            _animation : cc.Animation 
        ) : void;

        /**
         *  Creates the action with an Animation and will restore the original frame when the animation is over.<br>
         * -- param animation A certain animation.<br>
         * -- return An autoreleased Animate object.
         * @method create
         * @param {cc.Animation} _animation
         * @return {cc.Animate}
         */
        public static create (
            _animation : cc.Animation 
        ) : cc.Animate;

        /**
         * 
         * @method Animate
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class TargetedAction
     * @native
     */
    export class TargetedAction 
        extends cc.ActionInterval
    {

        /**
         * 
         * @method getForcedTarget
         * @return {cc.Node}
         */
        public getForcedTarget (
        ) : cc.Node;

        /**
         *  Init an action with the specified action and forced target 
         * @method initWithTarget
         * @param {cc.Node} _target
         * @param {cc.FiniteTimeAction} _action
         * @return {boolean}
         */
        public initWithTarget (
            _target : cc.Node, 
            _action : cc.FiniteTimeAction 
        ) : boolean;

        /**
         *  Sets the target that the action will be forced to run with.<br>
         * -- param forcedTarget The target that the action will be forced to run with.
         * @method setForcedTarget
         * @param {cc.Node} _forcedTarget
         */
        public setForcedTarget (
            _forcedTarget : cc.Node 
        ) : void;

        /**
         *  Create an action with the specified action and forced target.<br>
         * -- param target The target needs to override.<br>
         * -- param action The action needs to override.<br>
         * -- return An autoreleased TargetedAction object.
         * @method create
         * @param {cc.Node} _target
         * @param {cc.FiniteTimeAction} _action
         * @return {cc.TargetedAction}
         */
        public static create (
            _target : cc.Node, 
            _action : cc.FiniteTimeAction 
        ) : cc.TargetedAction;

        /**
         * 
         * @method TargetedAction
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class ActionFloat
     * @native
     */
    export class ActionFloat 
        extends cc.ActionInterval
    {

        /**
         * 
         * @method initWithDuration
         * @param {number} _duration
         * @param {number} _from
         * @param {number} _to
         * @param {(arg0:float) => void} _callback
         * @return {boolean}
         */
        public initWithDuration (
            _duration : number, 
            _from : number, 
            _to : number, 
            _callback : (arg0:float) => void 
        ) : boolean;

        /**
         * Creates FloatAction with specified duration, from value, to value and callback to report back<br>
         * -- results<br>
         * -- param duration of the action<br>
         * -- param from value to start from<br>
         * -- param to value to be at the end of the action<br>
         * -- param callback to report back result<br>
         * -- return An autoreleased ActionFloat object
         * @method create
         * @param {number} _duration
         * @param {number} _from
         * @param {number} _to
         * @param {(arg0:float) => void} _callback
         * @return {cc.ActionFloat}
         */
        public static create (
            _duration : number, 
            _from : number, 
            _to : number, 
            _callback : (arg0:float) => void 
        ) : cc.ActionFloat;

        /**
         * 
         * @method ActionFloat
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class Configuration
     * @native
     */
    export abstract class Configuration 
    {

        /**
         *  Whether or not PVR Texture Compressed is supported.<br>
         * -- return Is true if supports PVR Texture Compressed.
         * @method supportsPVRTC
         * @return {boolean}
         */
        public supportsPVRTC (
        ) : boolean;

        /**
         *  OpenGL Max Modelview Stack Depth.<br>
         * -- return The OpenGL Max Modelview Stack Depth.
         * @method getMaxModelviewStackDepth
         * @return {number}
         */
        public getMaxModelviewStackDepth (
        ) : number;

        /**
         *  Whether or not shareable VAOs are supported.<br>
         * -- return Is true if supports shareable VAOs.<br>
         * -- since v2.0.0
         * @method supportsShareableVAO
         * @return {boolean}
         */
        public supportsShareableVAO (
        ) : boolean;

        /**
         *  Whether or not BGRA8888 textures are supported.<br>
         * -- return Is true if supports BGRA8888 textures.<br>
         * -- since v0.99.2
         * @method supportsBGRA8888
         * @return {boolean}
         */
        public supportsBGRA8888 (
        ) : boolean;

        /**
         *  Returns whether or not an OpenGL is supported. <br>
         * -- param searchName A given search name.<br>
         * -- return Is true if an OpenGL is supported.
         * @method checkForGLExtension
         * @param {string} _searchName
         * @return {boolean}
         */
        public checkForGLExtension (
            _searchName : string 
        ) : boolean;

        /**
         *  Whether or not ATITC Texture Compressed is supported.<br>
         * -- return Is true if supports ATITC Texture Compressed.
         * @method supportsATITC
         * @return {boolean}
         */
        public supportsATITC (
        ) : boolean;

        /**
         *  Whether or not the GPU supports NPOT (Non Power Of Two) textures.<br>
         * -- OpenGL ES 2.0 already supports NPOT (iOS).<br>
         * -- return Is true if supports NPOT.<br>
         * -- since v0.99.2
         * @method supportsNPOT
         * @return {boolean}
         */
        public supportsNPOT (
        ) : boolean;

        /**
         *  Initialize method.<br>
         * -- return Is true if initialize success.
         * @method init
         * @return {boolean}
         */
        public init (
        ) : boolean;

        /**
         *  get 3d animate quality
         * @method getAnimate3DQuality
         * @return {number}
         */
        public getAnimate3DQuality (
        ) : number;

        /**
         *  Max support point light in shader, for Sprite3D.<br>
         * -- return Maximum supports point light in shader.<br>
         * -- since v3.3
         * @method getMaxSupportPointLightInShader
         * @return {number}
         */
        public getMaxSupportPointLightInShader (
        ) : number;

        /**
         *  OpenGL Max texture size.<br>
         * -- return The OpenGL Max texture size.
         * @method getMaxTextureSize
         * @return {number}
         */
        public getMaxTextureSize (
        ) : number;

        /**
         *  Sets a new key/value pair  in the configuration dictionary.<br>
         * -- param key A given key.<br>
         * -- param value A given value.
         * @method setValue
         * @param {string} _key
         * @param {cc.Value} _value
         */
        public setValue (
            _key : string, 
            _value : cc.Value 
        ) : void;

        /**
         *  Max support spot light in shader, for Sprite3D.<br>
         * -- return Maximum supports spot light in shader.<br>
         * -- since v3.3
         * @method getMaxSupportSpotLightInShader
         * @return {number}
         */
        public getMaxSupportSpotLightInShader (
        ) : number;

        /**
         *  Whether or not ETC Texture Compressed is supported.<br>
         * -- return Is true if supports ETC Texture Compressed.
         * @method supportsETC
         * @return {boolean}
         */
        public supportsETC (
        ) : boolean;

        /**
         *  Max support directional light in shader, for Sprite3D.<br>
         * -- return Maximum supports directional light in shader.<br>
         * -- since v3.3
         * @method getMaxSupportDirLightInShader
         * @return {number}
         */
        public getMaxSupportDirLightInShader (
        ) : number;

        /**
         *  Loads a config file. If the keys are already present, then they are going to be replaced. Otherwise the new keys are added.<br>
         * -- param filename Config file name.
         * @method loadConfigFile
         * @param {string} _filename
         */
        public loadConfigFile (
            _filename : string 
        ) : void;

        /**
         *  Whether or not glDiscardFramebufferEXT is supported.<br>
         * -- return Is true if supports glDiscardFramebufferEXT.<br>
         * -- since v0.99.2
         * @method supportsDiscardFramebuffer
         * @return {boolean}
         */
        public supportsDiscardFramebuffer (
        ) : boolean;

        /**
         *  Whether or not S3TC Texture Compressed is supported.<br>
         * -- return Is true if supports S3TC Texture Compressed.
         * @method supportsS3TC
         * @return {boolean}
         */
        public supportsS3TC (
        ) : boolean;

        /**
         *  Returns the Configuration info.<br>
         * -- return The Configuration info.
         * @method getInfo
         * @return {string}
         */
        public dumpInfo (
        ) : string;

        /**
         *  Returns the maximum texture units.<br>
         * -- return The maximum texture units.<br>
         * -- since v2.0.0
         * @method getMaxTextureUnits
         * @return {number}
         */
        public getMaxTextureUnits (
        ) : number;

        /**
         *  Returns the value of a given key as a double.<br>
         * -- param key A given key.<br>
         * -- param defaultValue if not find the value, return the defaultValue.<br>
         * -- return 
         * @method getValue
         * @param {string} _key
         * @param {cc.Value} _defaultValue
         * @return {cc.Value}
         */
        public getValue (
            _key : string, 
            _defaultValue : cc.Value 
        ) : cc.Value;

        /**
         *  Gathers OpenGL / GPU information.
         * @method gatherGPUInfo
         */
        public gatherGPUInfo (
        ) : void;

        /**
         *  Purge the shared instance of Configuration.
         * @method destroyInstance
         */
        public static destroyInstance (
        ) : void;

        /**
         *  Returns a shared instance of Configuration.<br>
         * -- return An autoreleased Configuration object.
         * @method getInstance
         * @return {cc.Configuration}
         */
        public static getInstance (
        ) : cc.Configuration;

    }
    /**
     * @class Properties
     * @native
     */
    export class Properties 
    {

        /**
         * Returns the value of a variable that is set in this Properties object.<br>
         * -- Variables take on the format ${name} and are inherited from parent Property objects.<br>
         * -- param name Name of the variable to get.<br>
         * -- param defaultValue Value to return if the variable is not found.<br>
         * -- return The value of the specified variable, or defaultValue if not found.
         * @method getVariable
         * @param {string} _name
         * @param {string} _defaultValue
         * @return {string}
         */
        public getVariable (
            _name : string, 
            _defaultValue : string 
        ) : string;

        /**
         * Get the value of the given property as a string. This can always be retrieved,<br>
         * -- whatever the intended type of the property.<br>
         * -- param name The name of the property to interpret, or NULL to return the current property's value.<br>
         * -- param defaultValue The default value to return if the specified property does not exist.<br>
         * -- return The value of the given property as a string, or the empty string if no property with that name exists.
         * @method getString
         * @return {string}
         */
        public getString (
        ) : string;

        /**
         * Interpret the value of the given property as a long integer.<br>
         * -- If the property does not exist, zero will be returned.<br>
         * -- If the property exists but could not be scanned, an error will be logged and zero will be returned.<br>
         * -- param name The name of the property to interpret, or NULL to return the current property's value.<br>
         * -- return The value of the given property interpreted as a long.<br>
         * -- Zero if the property does not exist or could not be scanned.
         * @method getLong
         * @return {number}
         */
        public getLong (
        ) : number;

        /**
         * Get the name of this Property's namespace.<br>
         * -- return The name of this Property's namespace.
         * @method getNamespace
         * @param {(string)} _id?
         * @param {(boolean)} _searchNames?
         * @param {(boolean)} _recurse?
         * @return {string | cc.Properties}
         */
        public getNamespace (
            _id? : (string), 
            _searchNames? : (boolean), 
            _recurse? : (boolean)
        ) : string | cc.Properties;

        /**
         * Gets the file path for the given property if the file exists.<br>
         * -- This method will first search for the file relative to the working directory.<br>
         * -- If the file is not found then it will search relative to the directory the bundle file is in.<br>
         * -- param name The name of the property.<br>
         * -- param path The string to copy the path to if the file exists.<br>
         * -- return True if the property exists and the file exists, false otherwise.<br>
         * -- script{ignore}
         * @method getPath
         * @param {string} _name
         * @param {string} _path
         * @return {boolean}
         */
        public getPath (
            _name : string, 
            _path : string 
        ) : boolean;

        /**
         * Interpret the value of the given property as a Matrix.<br>
         * -- If the property does not exist, out will be set to the identity matrix.<br>
         * -- If the property exists but could not be scanned, an error will be logged and out will be set<br>
         * -- to the identity matrix.<br>
         * -- param name The name of the property to interpret, or NULL to return the current property's value.<br>
         * -- param out The matrix to set to this property's interpreted value.<br>
         * -- return True on success, false if the property does not exist or could not be scanned.
         * @method getMat4
         * @param {string} _name
         * @param {ctype.value_type<cc.Mat4>} _out
         * @return {boolean}
         */
        public getMat4 (
            _name : string, 
            _out : ctype.value_type<cc.Mat4> 
        ) : boolean;

        /**
         * Check if a property with the given name is specified in this Properties object.<br>
         * -- param name The name of the property to query.<br>
         * -- return True if the property exists, false otherwise.
         * @method exists
         * @param {string} _name
         * @return {boolean}
         */
        public exists (
            _name : string 
        ) : boolean;

        /**
         * Sets the value of the property with the specified name.<br>
         * -- If there is no property in this namespace with the current name,<br>
         * -- one is added. Otherwise, the value of the first property with the<br>
         * -- specified name is updated.<br>
         * -- If name is NULL, the value current property (see getNextProperty) is<br>
         * -- set, unless there is no current property, in which case false<br>
         * -- is returned.<br>
         * -- param name The name of the property to set.<br>
         * -- param value The property value.<br>
         * -- return True if the property was set, false otherwise.
         * @method setString
         * @param {string} _name
         * @param {string} _value
         * @return {boolean}
         */
        public setString (
            _name : string, 
            _value : string 
        ) : boolean;

        /**
         * Get the ID of this Property's namespace. The ID should be a unique identifier,<br>
         * -- but its uniqueness is not enforced.<br>
         * -- return The ID of this Property's namespace.
         * @method getId
         * @return {string}
         */
        public getId (
        ) : string;

        /**
         * Rewind the getNextProperty() and getNextNamespace() iterators<br>
         * -- to the beginning of the file.
         * @method rewind
         */
        public rewind (
        ) : void;

        /**
         * Sets the value of the specified variable.<br>
         * -- param name Name of the variable to set.<br>
         * -- param value The value to set.
         * @method setVariable
         * @param {string} _name
         * @param {string} _value
         */
        public setVariable (
            _name : string, 
            _value : string 
        ) : void;

        /**
         * Interpret the value of the given property as a boolean.<br>
         * -- param name The name of the property to interpret, or NULL to return the current property's value.<br>
         * -- param defaultValue the default value to return if the specified property does not exist within the properties file.<br>
         * -- return true if the property exists and its value is "true", otherwise false.
         * @method getBool
         * @return {boolean}
         */
        public getBool (
        ) : boolean;

        /**
         * Interpret the value of the given property as an RGBA color in hex and write this color to a Vector4.<br>
         * -- E.g. 0xff0000ff represents opaque red and sets the vector to (1, 0, 0, 1).<br>
         * -- If the property does not exist, out will be set to Vector4(0.0f, 0.0f, 0.0f, 0.0f).<br>
         * -- If the property exists but could not be scanned, an error will be logged and out will be set<br>
         * -- to Vector4(0.0f, 0.0f, 0.0f, 0.0f).<br>
         * -- param name The name of the property to interpret, or NULL to return the current property's value.<br>
         * -- param out The vector to set to this property's interpreted value.<br>
         * -- return True on success, false if the property does not exist or could not be scanned.
         * @method getColor
         * @param {(string)} _name
         * @param {(ctype.value_type<cc.Vec4>) | (ctype.value_type<cc.Vec3>)} _out
         * @return {boolean}
         */
        public getColor (
            _name : (string), 
            _out : (ctype.value_type<cc.Vec4>) | (ctype.value_type<cc.Vec3>)
        ) : boolean;

        /**
         * Returns the type of a property.<br>
         * -- param name The name of the property to interpret, or NULL to return the current property's type.<br>
         * -- return The type of the property.
         * @method getType
         * @return {number}
         */
        public getType (
        ) : number;

        /**
         * Get the next namespace.
         * @method getNextNamespace
         * @return {cc.Properties}
         */
        public getNextNamespace (
        ) : cc.Properties;

        /**
         * Interpret the value of the given property as an integer.<br>
         * -- If the property does not exist, zero will be returned.<br>
         * -- If the property exists but could not be scanned, an error will be logged and zero will be returned.<br>
         * -- param name The name of the property to interpret, or NULL to return the current property's value.<br>
         * -- return The value of the given property interpreted as an integer.<br>
         * -- Zero if the property does not exist or could not be scanned.
         * @method getInt
         * @return {number}
         */
        public getInt (
        ) : number;

        /**
         * Interpret the value of the given property as a Vector3.<br>
         * -- If the property does not exist, out will be set to Vector3(0.0f, 0.0f, 0.0f).<br>
         * -- If the property exists but could not be scanned, an error will be logged and out will be set<br>
         * -- to Vector3(0.0f, 0.0f, 0.0f).<br>
         * -- param name The name of the property to interpret, or NULL to return the current property's value.<br>
         * -- param out The vector to set to this property's interpreted value.<br>
         * -- return True on success, false if the property does not exist or could not be scanned.
         * @method getVec3
         * @param {string} _name
         * @param {ctype.value_type<cc.Vec3>} _out
         * @return {boolean}
         */
        public getVec3 (
            _name : string, 
            _out : ctype.value_type<cc.Vec3> 
        ) : boolean;

        /**
         * Interpret the value of the given property as a Vector2.<br>
         * -- If the property does not exist, out will be set to Vector2(0.0f, 0.0f).<br>
         * -- If the property exists but could not be scanned, an error will be logged and out will be set<br>
         * -- to Vector2(0.0f, 0.0f).<br>
         * -- param name The name of the property to interpret, or NULL to return the current property's value.<br>
         * -- param out The vector to set to this property's interpreted value.<br>
         * -- return True on success, false if the property does not exist or could not be scanned.
         * @method getVec2
         * @param {string} _name
         * @param {ctype.value_type<cc.Point[]>} _out
         * @return {boolean}
         */
        public getVec2 (
            _name : string, 
            _out : ctype.value_type<cc.Point[]> 
        ) : boolean;

        /**
         * Interpret the value of the given property as a Vector4.<br>
         * -- If the property does not exist, out will be set to Vector4(0.0f, 0.0f, 0.0f, 0.0f).<br>
         * -- If the property exists but could not be scanned, an error will be logged and out will be set<br>
         * -- to Vector4(0.0f, 0.0f, 0.0f, 0.0f).<br>
         * -- param name The name of the property to interpret, or NULL to return the current property's value.<br>
         * -- param out The vector to set to this property's interpreted value.<br>
         * -- return True on success, false if the property does not exist or could not be scanned.
         * @method getVec4
         * @param {string} _name
         * @param {ctype.value_type<cc.Vec4>} _out
         * @return {boolean}
         */
        public getVec4 (
            _name : string, 
            _out : ctype.value_type<cc.Vec4> 
        ) : boolean;

        /**
         * Get the name of the next property.<br>
         * -- If a valid next property is returned, the value of the property can be<br>
         * -- retrieved using any of the get methods in this class, passing NULL for the property name.<br>
         * -- return The name of the next property, or NULL if there are no properties remaining.
         * @method getNextProperty
         * @return {string}
         */
        public getNextProperty (
        ) : string;

        /**
         * Interpret the value of the given property as a floating-point number.<br>
         * -- If the property does not exist, zero will be returned.<br>
         * -- If the property exists but could not be scanned, an error will be logged and zero will be returned.<br>
         * -- param name The name of the property to interpret, or NULL to return the current property's value.<br>
         * -- return The value of the given property interpreted as a float.<br>
         * -- Zero if the property does not exist or could not be scanned.
         * @method getFloat
         * @return {number}
         */
        public getFloat (
        ) : number;

        /**
         * Interpret the value of the given property as a Quaternion specified as an axis angle.<br>
         * -- If the property does not exist, out will be set to Quaternion().<br>
         * -- If the property exists but could not be scanned, an error will be logged and out will be set<br>
         * -- to Quaternion().<br>
         * -- param name The name of the property to interpret, or NULL to return the current property's value.<br>
         * -- param out The quaternion to set to this property's interpreted value.<br>
         * -- return True on success, false if the property does not exist or could not be scanned.
         * @method getQuaternionFromAxisAngle
         * @param {string} _name
         * @param {cc.Quaternion} _out
         * @return {boolean}
         */
        public getQuaternionFromAxisAngle (
            _name : string, 
            _out : cc.Quaternion 
        ) : boolean;

        /**
         * Attempts to parse the specified string as an RGBA color value.<br>
         * -- param str The string to parse.<br>
         * -- param out The value to populate if successful.<br>
         * -- return True if a valid RGBA color was parsed, false otherwise.
         * @method parseColor
         * @param {(string)} _str
         * @param {(ctype.value_type<cc.Vec4>) | (ctype.value_type<cc.Vec3>)} _out
         * @return {boolean}
         */
        public static parseColor (
            _str : (string), 
            _out : (ctype.value_type<cc.Vec4>) | (ctype.value_type<cc.Vec3>)
        ) : boolean;

        /**
         * Attempts to parse the specified string as a Vector3 value.<br>
         * -- On error, false is returned and the output is set to all zero values.<br>
         * -- param str The string to parse.<br>
         * -- param out The value to populate if successful.<br>
         * -- return True if a valid Vector3 was parsed, false otherwise.
         * @method parseVec3
         * @param {string} _str
         * @param {ctype.value_type<cc.Vec3>} _out
         * @return {boolean}
         */
        public static parseVec3 (
            _str : string, 
            _out : ctype.value_type<cc.Vec3> 
        ) : boolean;

        /**
         * Attempts to parse the specified string as an axis-angle value.<br>
         * -- The specified string is expected to contain four comma-separated<br>
         * -- values, where the first three values represents the axis and the<br>
         * -- fourth value represents the angle, in degrees.<br>
         * -- On error, false is returned and the output is set to all zero values.<br>
         * -- param str The string to parse.<br>
         * -- param out A Quaternion populated with the orientation of the axis-angle, if successful.<br>
         * -- return True if a valid axis-angle was parsed, false otherwise.
         * @method parseAxisAngle
         * @param {string} _str
         * @param {cc.Quaternion} _out
         * @return {boolean}
         */
        public static parseAxisAngle (
            _str : string, 
            _out : cc.Quaternion 
        ) : boolean;

        /**
         * Attempts to parse the specified string as a Vector2 value.<br>
         * -- On error, false is returned and the output is set to all zero values.<br>
         * -- param str The string to parse.<br>
         * -- param out The value to populate if successful.<br>
         * -- return True if a valid Vector2 was parsed, false otherwise.
         * @method parseVec2
         * @param {string} _str
         * @param {ctype.value_type<cc.Point[]>} _out
         * @return {boolean}
         */
        public static parseVec2 (
            _str : string, 
            _out : ctype.value_type<cc.Point[]> 
        ) : boolean;

        /**
         * Creates a Properties runtime settings from the specified URL, where the URL is of<br>
         * -- the format "<file-path>.<extension>#<namespace-id>/<namespace-id>/.../<namespace-id>"<br>
         * -- (and "#<namespace-id>/<namespace-id>/.../<namespace-id>" is optional).<br>
         * -- param url The URL to create the properties from.<br>
         * -- return The created Properties or NULL if there was an error.<br>
         * -- script{create}
         * @method createNonRefCounted
         * @param {string} _url
         * @return {cc.Properties}
         */
        public static createNonRefCounted (
            _url : string 
        ) : cc.Properties;

        /**
         * Attempts to parse the specified string as a Vector4 value.<br>
         * -- On error, false is returned and the output is set to all zero values.<br>
         * -- param str The string to parse.<br>
         * -- param out The value to populate if successful.<br>
         * -- return True if a valid Vector4 was parsed, false otherwise.
         * @method parseVec4
         * @param {string} _str
         * @param {ctype.value_type<cc.Vec4>} _out
         * @return {boolean}
         */
        public static parseVec4 (
            _str : string, 
            _out : ctype.value_type<cc.Vec4> 
        ) : boolean;

    }
    /**
     * @class FileUtils
     * @native
     */
    export abstract class FileUtils 
    {
        // *****************************************************************
        // *****************************************************************
        // ******IMPORTANT NOTE: *******************************************
        // This section is relay on 
        // cocos2d-x/cocos/scripting/js-bindings/manual/cocos2d_specifics.cpp
        // *****************************************************************
        // *****************************************************************

        /**
         * @function setSearchResolutionsOrder
         * @param {Array} searchResolutionsOrder
         */
        public setSearchResolutionsOrder(searchResolutionsOrder:string[]):void;

        /**
         * @function setSearchPaths
         * @param {Array} searchPaths
         */
        public setSearchPaths(searchPaths:string[]):void;

        /**
         * @function getSearchPaths
         * @return {Array}
         */
        public getSearchPaths():string[];

        /**
         * @function getSearchResolutionsOrder
         * @return {Array}
         */
        public getSearchResolutionsOrder():string[];

        public writeDataToFile(data: any[], path: string): boolean;
        public getDataFromFile(path: string): any[];

        // *****************************************************************
        // *****************************************************************
        // *****************************************************************
        // *****************************************************************
        // *****************************************************************
        

        /**
         *  Returns the fullpath for a given filename.<br>
         * -- First it will try to get a new filename from the "filenameLookup" dictionary.<br>
         * -- If a new filename can't be found on the dictionary, it will use the original filename.<br>
         * -- Then it will try to obtain the full path of the filename using the FileUtils search rules: resolutions, and search paths.<br>
         * -- The file search is based on the array element order of search paths and resolution directories.<br>
         * -- For instance:<br>
         * -- We set two elements("/mnt/sdcard/", "internal_dir/") to search paths vector by setSearchPaths,<br>
         * -- and set three elements("resources-ipadhd/", "resources-ipad/", "resources-iphonehd")<br>
         * -- to resolutions vector by setSearchResolutionsOrder. The "internal_dir" is relative to "Resources/".<br>
         * -- If we have a file named 'sprite.png', the mapping in fileLookup dictionary contains `key: sprite.png -> value: sprite.pvr.gz`.<br>
         * -- Firstly, it will replace 'sprite.png' with 'sprite.pvr.gz', then searching the file sprite.pvr.gz as follows:<br>
         * -- /mnt/sdcard/resources-ipadhd/sprite.pvr.gz      (if not found, search next)<br>
         * -- /mnt/sdcard/resources-ipad/sprite.pvr.gz        (if not found, search next)<br>
         * -- /mnt/sdcard/resources-iphonehd/sprite.pvr.gz    (if not found, search next)<br>
         * -- /mnt/sdcard/sprite.pvr.gz                       (if not found, search next)<br>
         * -- internal_dir/resources-ipadhd/sprite.pvr.gz     (if not found, search next)<br>
         * -- internal_dir/resources-ipad/sprite.pvr.gz       (if not found, search next)<br>
         * -- internal_dir/resources-iphonehd/sprite.pvr.gz   (if not found, search next)<br>
         * -- internal_dir/sprite.pvr.gz                      (if not found, return "sprite.png")<br>
         * -- If the filename contains relative path like "gamescene/uilayer/sprite.png",<br>
         * -- and the mapping in fileLookup dictionary contains `key: gamescene/uilayer/sprite.png -> value: gamescene/uilayer/sprite.pvr.gz`.<br>
         * -- The file search order will be:<br>
         * -- /mnt/sdcard/gamescene/uilayer/resources-ipadhd/sprite.pvr.gz      (if not found, search next)<br>
         * -- /mnt/sdcard/gamescene/uilayer/resources-ipad/sprite.pvr.gz        (if not found, search next)<br>
         * -- /mnt/sdcard/gamescene/uilayer/resources-iphonehd/sprite.pvr.gz    (if not found, search next)<br>
         * -- /mnt/sdcard/gamescene/uilayer/sprite.pvr.gz                       (if not found, search next)<br>
         * -- internal_dir/gamescene/uilayer/resources-ipadhd/sprite.pvr.gz     (if not found, search next)<br>
         * -- internal_dir/gamescene/uilayer/resources-ipad/sprite.pvr.gz       (if not found, search next)<br>
         * -- internal_dir/gamescene/uilayer/resources-iphonehd/sprite.pvr.gz   (if not found, search next)<br>
         * -- internal_dir/gamescene/uilayer/sprite.pvr.gz                      (if not found, return "gamescene/uilayer/sprite.png")<br>
         * -- If the new file can't be found on the file system, it will return the parameter filename directly.<br>
         * -- This method was added to simplify multiplatform support. Whether you are using cocos2d-js or any cross-compilation toolchain like StellaSDK or Apportable,<br>
         * -- you might need to load different resources for a given file in the different platforms.<br>
         * -- since v2.1
         * @method fullPathForFilename
         * @param {string} _filename
         * @return {string}
         */
        public fullPathForFilename (
            _filename : string 
        ) : string;

        /**
         * Gets string from a file, async off the main cocos thread<br>
         * -- param path filepath for the string to be read. Can be relative or absolute path<br>
         * -- param callback Function that will be called when file is read. Will be called <br>
         * -- on the main cocos thread.
         * @method getStringFromFile
         * @param {(string)} _path | filename
         * @param {((arg0:string) => void)} _callback?
         * @return {void | string}
         */
        public getStringFromFile (
            _path_filename : (string), 
            _callback? : ((arg0:string) => void)
        ) : void | string;

        /**
         * Removes a file, async off the main cocos thread.<br>
         * -- param filepath the path of the file to remove, it must be an absolute path<br>
         * -- param callback The function that will be called when the operation is complete. Will have one boolean<br>
         * -- argument, true if the file was successfully removed, false otherwise.
         * @method removeFile
         * @param {(string)} _filepath
         * @param {((arg0:boolean) => void)} _callback?
         * @return {void | boolean}
         */
        public removeFile (
            _filepath : (string), 
            _callback? : ((arg0:boolean) => void)
        ) : void | boolean;

        /**
         * Checks whether the path is an absolute path.<br>
         * -- note On Android, if the parameter passed in is relative to "assets/", this method will treat it as an absolute path.<br>
         * -- Also on Blackberry, path starts with "app/native/Resources/" is treated as an absolute path.<br>
         * -- param path The path that needs to be checked.<br>
         * -- return True if it's an absolute path, false if not.
         * @method isAbsolutePath
         * @param {string} _path
         * @return {boolean}
         */
        public isAbsolutePath (
            _path : string 
        ) : boolean;

        /**
         * Renames a file under the given directory, async off the main cocos thread.<br>
         * -- param path     The parent directory path of the file, it must be an absolute path.<br>
         * -- param oldname  The current name of the file.<br>
         * -- param name     The new name of the file.<br>
         * -- param callback The function that will be called when the operation is complete. Will have one boolean<br>
         * -- argument, true if the file was successfully renamed, false otherwise.
         * @method renameFile
         * @param {(string)} _path | oldfullpath
         * @param {(string)} _oldname | newfullpath
         * @param {(string) | ((arg0:boolean) => void)} _name | callback?
         * @param {((arg0:boolean) => void)} _callback?
         * @return {void | boolean}
         */
        public renameFile (
            _path_oldfullpath : (string), 
            _oldname_newfullpath : (string), 
            _name_callback? : (string) | ((arg0:boolean) => void), 
            _callback? : ((arg0:boolean) => void)
        ) : void | boolean;

        /**
         * Get default resource root path.
         * @method getDefaultResourceRootPath
         * @return {string}
         */
        public getDefaultResourceRootPath (
        ) : string;

        /**
         * Loads the filenameLookup dictionary from the contents of a filename.<br>
         * -- note The plist file name should follow the format below:<br>
         * -- code<br>
         * -- <?xml version="1.0" encoding="UTF-8"?><br>
         * -- <!DOCTYPE plist PUBLIC "-AppleDTD PLIST 1.0EN" "http:www.apple.com/DTDs/PropertyList-1.0.dtd"><br>
         * -- <plist version="1.0"><br>
         * -- <dict><br>
         * -- <key>filenames</key><br>
         * -- <dict><br>
         * -- <key>sounds/click.wav</key><br>
         * -- <string>sounds/click.caf</string><br>
         * -- <key>sounds/endgame.wav</key><br>
         * -- <string>sounds/endgame.caf</string><br>
         * -- <key>sounds/gem-0.wav</key><br>
         * -- <string>sounds/gem-0.caf</string><br>
         * -- </dict><br>
         * -- <key>metadata</key><br>
         * -- <dict><br>
         * -- <key>version</key><br>
         * -- <integer>1</integer><br>
         * -- </dict><br>
         * -- </dict><br>
         * -- </plist><br>
         * -- endcode<br>
         * -- param filename The plist file name.<br>
         * -- since v2.1<br>
         * -- js loadFilenameLookup<br>
         * -- lua loadFilenameLookup
         * @method loadFilenameLookupDictionaryFromFile
         * @param {string} _filename
         */
        public loadFilenameLookup (
            _filename : string 
        ) : void;

        /**
         *  Checks whether to pop up a message box when failed to load an image.<br>
         * -- return True if pop up a message box when failed to load an image, false if not.
         * @method isPopupNotify
         * @return {boolean}
         */
        public isPopupNotify (
        ) : boolean;

        /**
         * 
         * @method getContents
         * @param {string} _filename
         * @param {cc.ResizableBuffer} _buffer
         * @return {number}
         */
        public getContents (
            _filename : string, 
            _buffer : cc.ResizableBuffer 
        ) : number;

        /**
         * 
         * @method getValueVectorFromFile
         * @param {string} _filename
         * @return {cc.Value[]}
         */
        public getValueVectorFromFile (
            _filename : string 
        ) : cc.Value[];

        /**
         * Gets the array of search paths.<br>
         * -- return The array of search paths which may contain the prefix of default resource root path. <br>
         * -- note In best practise, getter function should return the value of setter function passes in.<br>
         * -- But since we should not break the compatibility, we keep using the old logic. <br>
         * -- Therefore, If you want to get the original search paths, please call 'getOriginalSearchPaths()' instead.<br>
         * -- see fullPathForFilename(const char*).<br>
         * -- lua NA
         * @method getSearchPaths
         * @return {string[]}
         */
        public getSearchPaths (
        ) : string[];

        /**
         * write a ValueMap into a plist file<br>
         * -- param dict the ValueMap want to save<br>
         * -- param fullPath The full path to the file you want to save a string<br>
         * -- return bool
         * @method writeToFile
         * @param {any} _dict
         * @param {string} _fullPath
         * @return {boolean}
         */
        public writeToFile (
            _dict : any, 
            _fullPath : string 
        ) : boolean;

        /**
         * Gets the original search path array set by 'setSearchPaths' or 'addSearchPath'.<br>
         * -- return The array of the original search paths
         * @method getOriginalSearchPaths
         * @return {string[]}
         */
        public getOriginalSearchPaths (
        ) : string[];

        /**
         * Gets the new filename from the filename lookup dictionary.<br>
         * -- It is possible to have a override names.<br>
         * -- param filename The original filename.<br>
         * -- return The new filename after searching in the filename lookup dictionary.<br>
         * -- If the original filename wasn't in the dictionary, it will return the original filename.
         * @method getNewFilename
         * @param {string} _filename
         * @return {string}
         */
        public getNewFilename (
            _filename : string 
        ) : string;

        /**
         * Converts the contents of a file to a ValueMap.<br>
         * -- param filename The filename of the file to gets content.<br>
         * -- return ValueMap of the file contents.<br>
         * -- note This method is used internally.
         * @method getValueMapFromFile
         * @param {string} _filename
         * @return {any}
         */
        public getValueMapFromFile (
            _filename : string 
        ) : any;

        /**
         * Retrieve the file size, async off the main cocos thread.<br>
         * -- note If a relative path was passed in, it will be inserted a default root path at the beginning.<br>
         * -- param filepath The path of the file, it could be a relative or absolute path.<br>
         * -- param callback The function that will be called when the operation is complete. Will have one long<br>
         * -- argument, the file size.
         * @method getFileSize
         * @param {(string)} _filepath
         * @param {((arg0:long) => void)} _callback?
         * @return {void | number}
         */
        public getFileSize (
            _filepath : (string), 
            _callback? : ((arg0:long) => void)
        ) : void | number;

        /**
         *  Converts the contents of a file to a ValueMap.<br>
         * -- This method is used internally.
         * @method getValueMapFromData
         * @param {string} _filedata
         * @param {number} _filesize
         * @return {any}
         */
        public getValueMapFromData (
            _filedata : string, 
            _filesize : number 
        ) : any;

        /**
         * Removes a directory, async off the main cocos thread.<br>
         * -- param dirPath the path of the directory, it must be an absolute path<br>
         * -- param callback The function that will be called when the operation is complete. Will have one boolean<br>
         * -- argument, true if the directory was successfully removed, false otherwise.
         * @method removeDirectory
         * @param {(string)} _dirPath
         * @param {((arg0:boolean) => void)} _callback?
         * @return {void | boolean}
         */
        public removeDirectory (
            _dirPath : (string), 
            _callback? : ((arg0:boolean) => void)
        ) : void | boolean;

        /**
         * Sets the array of search paths.<br>
         * -- You can use this array to modify the search path of the resources.<br>
         * -- If you want to use "themes" or search resources in the "cache", you can do it easily by adding new entries in this array.<br>
         * -- note This method could access relative path and absolute path.<br>
         * -- If the relative path was passed to the vector, FileUtils will add the default resource directory before the relative path.<br>
         * -- For instance:<br>
         * -- On Android, the default resource root path is "assets/".<br>
         * -- If "/mnt/sdcard/" and "resources-large" were set to the search paths vector,<br>
         * -- "resources-large" will be converted to "assets/resources-large" since it was a relative path.<br>
         * -- param searchPaths The array contains search paths.<br>
         * -- see fullPathForFilename(const char*)<br>
         * -- since v2.1<br>
         * -- In js:var setSearchPaths(var jsval);<br>
         * -- lua NA
         * @method setSearchPaths
         * @param {string[]} _searchPaths
         */
        public setSearchPaths (
            _searchPaths : string[] 
        ) : void;

        /**
         * Write a string to a file, done async off the main cocos thread<br>
         * -- Use this function if you need file access without blocking the main thread.<br>
         * -- This function takes a std::string by value on purpose, to leverage move sematics.<br>
         * -- If you want to avoid a copy of your datastr, use std::move/std::forward if appropriate<br>
         * -- param dataStr the string want to save<br>
         * -- param fullPath The full path to the file you want to save a string<br>
         * -- param callback The function called once the string has been written to a file. This<br>
         * -- function will be executed on the main cocos thread. It will have on boolean argument <br>
         * -- signifying if the write was successful.
         * @method writeStringToFile
         * @param {(string)} _dataStr
         * @param {(string)} _fullPath
         * @param {((arg0:boolean) => void)} _callback?
         * @return {void | boolean}
         */
        public writeStringToFile (
            _dataStr : (string), 
            _fullPath : (string), 
            _callback? : ((arg0:boolean) => void)
        ) : void | boolean;

        /**
         * Sets the array that contains the search order of the resources.<br>
         * -- param searchResolutionsOrder The source array that contains the search order of the resources.<br>
         * -- see getSearchResolutionsOrder(), fullPathForFilename(const char*).<br>
         * -- since v2.1<br>
         * -- In js:var setSearchResolutionsOrder(var jsval)<br>
         * -- lua NA
         * @method setSearchResolutionsOrder
         * @param {string[]} _searchResolutionsOrder
         */
        public setSearchResolutionsOrder (
            _searchResolutionsOrder : string[] 
        ) : void;

        /**
         * Append search order of the resources.<br>
         * -- see setSearchResolutionsOrder(), fullPathForFilename().<br>
         * -- since v2.1
         * @method addSearchResolutionsOrder
         * @param {string} _order
         * @param {boolean} _front
         */
        public addSearchResolutionsOrder (
            _order : string, 
            _front : boolean 
        ) : void;

        /**
         * Add search path.<br>
         * -- since v2.1
         * @method addSearchPath
         * @param {string} _path
         * @param {boolean} _front
         */
        public addSearchPath (
            _path : string, 
            _front : boolean 
        ) : void;

        /**
         * Write a ValueVector into a file, done async off the main cocos thread.<br>
         * -- Use this function if you need to write a ValueVector while not blocking the main cocos thread.<br>
         * -- This function takes ValueVector by value on purpose, to leverage move sematics.<br>
         * -- If you want to avoid a copy of your dict, use std::move/std::forward if appropriate<br>
         * -- param vecData The ValueVector that will be written to disk<br>
         * -- param fullPath The absolute file path that the data will be written to<br>
         * -- param callback The function that will be called when vecData is written to disk. This<br>
         * -- function will be executed on the main cocos thread. It will have on boolean argument <br>
         * -- signifying if the write was successful.
         * @method writeValueVectorToFile
         * @param {(cc.Value[])} _vecData
         * @param {(string)} _fullPath
         * @param {((arg0:boolean) => void)} _callback?
         * @return {void | boolean}
         */
        public writeValueVectorToFile (
            _vecData : (cc.Value[]), 
            _fullPath : (string), 
            _callback? : ((arg0:boolean) => void)
        ) : void | boolean;

        /**
         * Checks if a file exists, done async off the main cocos thread.<br>
         * -- Use this function if you need to check if a file exists while not blocking the main cocos thread.<br>
         * -- note If a relative path was passed in, it will be inserted a default root path at the beginning.<br>
         * -- param filename The path of the file, it could be a relative or absolute path.<br>
         * -- param callback The function that will be called when the operation is complete. Will have one boolean<br>
         * -- argument, true if the file exists, false otherwise.
         * @method isFileExist
         * @param {(string)} _filename
         * @param {((arg0:boolean) => void)} _callback?
         * @return {void | boolean}
         */
        public isFileExist (
            _filename : (string), 
            _callback? : ((arg0:boolean) => void)
        ) : void | boolean;

        /**
         * Purges full path caches.
         * @method purgeCachedEntries
         */
        public purgeCachedEntries (
        ) : void;

        /**
         * Gets full path from a file name and the path of the relative file.<br>
         * -- param filename The file name.<br>
         * -- param relativeFile The path of the relative file.<br>
         * -- return The full path.<br>
         * -- e.g. filename: hello.png, pszRelativeFile: /User/path1/path2/hello.plist<br>
         * -- Return: /User/path1/path2/hello.pvr (If there a a key(hello.png)-value(hello.pvr) in FilenameLookup dictionary. )
         * @method fullPathFromRelativeFile
         * @param {string} _filename
         * @param {string} _relativeFile
         * @return {string}
         */
        public fullPathFromRelativeFile (
            _filename : string, 
            _relativeFile : string 
        ) : string;

        /**
         * Windows fopen can't support UTF-8 filename<br>
         * -- Need convert all parameters fopen and other 3rd-party libs<br>
         * -- param filenameUtf8 std::string name file for conversion from utf-8<br>
         * -- return std::string ansi filename in current locale
         * @method getSuitableFOpen
         * @param {string} _filenameUtf8
         * @return {string}
         */
        public getSuitableFOpen (
            _filenameUtf8 : string 
        ) : string;

        /**
         * Write a ValueMap into a file, done async off the main cocos thread.<br>
         * -- Use this function if you need to write a ValueMap while not blocking the main cocos thread.<br>
         * -- This function takes ValueMap by value on purpose, to leverage move sematics.<br>
         * -- If you want to avoid a copy of your dict, use std::move/std::forward if appropriate<br>
         * -- param dict The ValueMap that will be written to disk<br>
         * -- param fullPath The absolute file path that the data will be written to<br>
         * -- param callback The function that will be called when dict is written to disk. This<br>
         * -- function will be executed on the main cocos thread. It will have on boolean argument <br>
         * -- signifying if the write was successful.
         * @method writeValueMapToFile
         * @param {(any)} _dict
         * @param {(string)} _fullPath
         * @param {((arg0:boolean) => void)} _callback?
         * @return {void | boolean}
         */
        public writeValueMapToFile (
            _dict : (any), 
            _fullPath : (string), 
            _callback? : ((arg0:boolean) => void)
        ) : void | boolean;

        /**
         * Gets filename extension is a suffix (separated from the base filename by a dot) in lower case.<br>
         * -- Examples of filename extensions are .png, .jpeg, .exe, .dmg and .txt.<br>
         * -- param filePath The path of the file, it could be a relative or absolute path.<br>
         * -- return suffix for filename in lower case or empty if a dot not found.
         * @method getFileExtension
         * @param {string} _filePath
         * @return {string}
         */
        public getFileExtension (
            _filePath : string 
        ) : string;

        /**
         * Sets writable path.
         * @method setWritablePath
         * @param {string} _writablePath
         */
        public setWritablePath (
            _writablePath : string 
        ) : void;

        /**
         * Sets whether to pop-up a message box when failed to load an image.
         * @method setPopupNotify
         * @param {boolean} _notify
         */
        public setPopupNotify (
            _notify : boolean 
        ) : void;

        /**
         * Checks whether the absoulate path is a directory, async off of the main cocos thread.<br>
         * -- param dirPath The path of the directory, it must be an absolute path<br>
         * -- param callback that will accept a boolean, true if the file exists, false otherwise. <br>
         * -- Callback will happen on the main cocos thread.
         * @method isDirectoryExist
         * @param {(string)} _fullPath | dirPath
         * @param {((arg0:boolean) => void)} _callback?
         * @return {void | boolean}
         */
        public isDirectoryExist (
            _fullPath_dirPath : (string), 
            _callback? : ((arg0:boolean) => void)
        ) : void | boolean;

        /**
         * Set default resource root path.
         * @method setDefaultResourceRootPath
         * @param {string} _path
         */
        public setDefaultResourceRootPath (
            _path : string 
        ) : void;

        /**
         * Gets the array that contains the search order of the resources.<br>
         * -- see setSearchResolutionsOrder(const std::vector<std::string>&), fullPathForFilename(const char*).<br>
         * -- since v2.1<br>
         * -- lua NA
         * @method getSearchResolutionsOrder
         * @return {string[]}
         */
        public getSearchResolutionsOrder (
        ) : string[];

        /**
         * Create a directory, async off the main cocos thread.<br>
         * -- param dirPath the path of the directory, it must be an absolute path<br>
         * -- param callback The function that will be called when the operation is complete. Will have one boolean<br>
         * -- argument, true if the directory was successfully, false otherwise.
         * @method createDirectory
         * @param {(string)} _dirPath
         * @param {((arg0:boolean) => void)} _callback?
         * @return {void | boolean}
         */
        public createDirectory (
            _dirPath : (string), 
            _callback? : ((arg0:boolean) => void)
        ) : void | boolean;

        /**
         * Gets the writable path.<br>
         * -- return  The path that can be write/read a file in
         * @method getWritablePath
         * @return {string}
         */
        public getWritablePath (
        ) : string;

        /**
         * You can inherit from platform dependent implementation of FileUtils, such as FileUtilsAndroid,<br>
         * -- and use this function to set delegate, then FileUtils will invoke delegate's implementation.<br>
         * -- For example, your resources are encrypted, so you need to decrypt it after reading data from<br>
         * -- resources, then you can implement all getXXX functions, and engine will invoke your own getXX<br>
         * -- functions when reading data of resources.<br>
         * -- If you don't want to system default implementation after setting delegate, you can just pass nullptr<br>
         * -- to this function.<br>
         * -- warning It will delete previous delegate<br>
         * -- lua NA
         * @method setDelegate
         * @param {cc.FileUtils} _delegate
         */
        public static setDelegate (
            _delegate : cc.FileUtils 
        ) : void;

        /**
         * Gets the instance of FileUtils.
         * @method getInstance
         * @return {cc.FileUtils}
         */
        public static getInstance (
        ) : cc.FileUtils;

    }
    /**
     * @class EventAcceleration
     * @native
     */
    export class EventAcceleration 
        extends cc.Event
    {

        /**
         *  Constructor.<br>
         * -- param acc A given Acceleration.
         * @method EventAcceleration
         * @constructor
         * @param {cc.Acceleration} _acc
         */
        public constructor (
            _acc : cc.Acceleration 
        );

    }
    /**
     * @class EventCustom
     * @native
     */
    export class EventCustom 
        extends cc.Event
    {

        /**
         *  Gets event name.<br>
         * -- return The name of the event.
         * @method getEventName
         * @return {string}
         */
        public getEventName (
        ) : string;

        /**
         *  Constructor.<br>
         * -- param eventName A given name of the custom event.<br>
         * -- js ctor
         * @method EventCustom
         * @constructor
         * @param {string} _eventName
         */
        public constructor (
            _eventName : string 
        );

    }
    /**
     * @class EventListener
     * @native
     */
    export abstract class EventListener 
    {
        // event listener type
        /**
         * The type code of unknown event listener.
         * @constant
         * @type {number}
         */
        public static UNKNOWN:number;

        /**
         * The type code of one by one touch event listener.
         * @constant
         * @type {number}
         */
        public static TOUCH_ONE_BY_ONE:number;

        /**
         * The type code of all at once touch event listener.
         * @constant
         * @type {number}
         */
        public static TOUCH_ALL_AT_ONCE:number;

        /**
         * The type code of keyboard event listener.
         * @constant
         * @type {number}
         */
        public static KEYBOARD:number;

        /**
         * The type code of mouse event listener.
         * @constant
         * @type {number}
         */
        public static MOUSE:number;

        /**
         * The type code of acceleration event listener.
         * @constant
         * @type {number}
         */
        public static ACCELERATION:number;

        ///**
        // * The type code of focus event listener.
        // * @constant
        // * @type {number}
        // */
        //public static ACCELERATION:number;

        /**
         * The type code of custom event listener.
         * @constant
         * @type {number}
         */
        public static CUSTOM:number;

        /**
         * The type code of Focus change event listener.
         * @constant
         * @type {number}
         */
        public static FOCUS:number;

        // *****************************************************************
        // *****************************************************************
        // ******IMPORTANT NOTE: *******************************************
        // This section is relay on 
        // cocos2d-x/cocos/scripting/js-bindings/manual/cocos2d_specifics.cpp
        // *****************************************************************
        // *****************************************************************
        
        /**
         * Currently JavaScript Bindings (JSB), in some cases, needs to use retain and release. This is a bug in JSB,
         * and the ugly workaround is to use retain/release. So, these 2 methods were added to be compatible with JSB.
         * This is a hack, and should be removed once JSB fixes the retain/release bug
         * You will need to retain an object if you created a listener and haven't added it any target node during the same frame.
         * Otherwise, JSB's native autorelease pool will consider this object a useless one and release it directly,
         * when you want to use it later, a "Invalid Native Object" error will be raised.
         * The retain function can increase a reference count for the native object to avoid it being released,
         * you need to manually invoke release function when you think this object is no longer needed, otherwise, there will be memory learks.
         * retain and release function call should be paired in developer's game code.
         * @function
         * @see cc.EventListener#release
         */
        retain():void;

        
        /**
         * Currently JavaScript Bindings (JSB), in some cases, needs to use retain and release. This is a bug in JSB,
         * and the ugly workaround is to use retain/release. So, these 2 methods were added to be compatible with JSB.
         * This is a hack, and should be removed once JSB fixes the retain/release bug
         * You will need to retain an object if you created a listener and haven't added it any target node during the same frame.
         * Otherwise, JSB's native autorelease pool will consider this object a useless one and release it directly,
         * when you want to use it later, a "Invalid Native Object" error will be raised.
         * The retain function can increase a reference count for the native object to avoid it being released,
         * you need to manually invoke release function when you think this object is no longer needed, otherwise, there will be memory learks.
         * retain and release function call should be paired in developer's game code.
         * @function
         * @see cc.EventListener#retain
         */
        public release():void;
        
        // *****************************************************************
        // *****************************************************************
        // *****************************************************************
        // *****************************************************************
        // *****************************************************************
        
        public static readonly UNKNOWN: number;
        public static readonly TOUCH_ONE_BY_ONE: number;
        public static readonly TOUCH_ALL_AT_ONCE: number;
        public static readonly KEYBOARD: number;
        public static readonly MOUSE: number;
        public static readonly ACCELERATION: number;
        public static readonly FOCUS: number;
        public static readonly CUSTOM: number;


        /**
         *  Enables or disables the listener.<br>
         * -- note Only listeners with `enabled` state will be able to receive events.<br>
         * -- When an listener was initialized, it's enabled by default.<br>
         * -- An event listener can receive events when it is enabled and is not paused.<br>
         * -- paused state is always false when it is a fixed priority listener.<br>
         * -- param enabled True if enables the listener.
         * @method setEnabled
         * @param {boolean} _enabled
         */
        public setEnabled (
            _enabled : boolean 
        ) : void;

        /**
         *  Checks whether the listener is enabled.<br>
         * -- return True if the listener is enabled.
         * @method isEnabled
         * @return {boolean}
         */
        public isEnabled (
        ) : boolean;

        /**
         *  Clones the listener, its subclasses have to override this method.
         * @method clone
         * @return {cc.EventListener}
         */
        public clone (
        ) : cc.EventListener;

        /**
         *  Checks whether the listener is available.<br>
         * -- return True if the listener is available.
         * @method checkAvailable
         * @return {boolean}
         */
        public checkAvailable (
        ) : boolean;

    }
    /**
     * @class EventDispatcher
     * @native
     */
    export class EventDispatcher 
    {

        /**
         *  Whether to enable dispatching events.<br>
         * -- param isEnabled  True if enable dispatching events.
         * @method setEnabled
         * @param {boolean} _isEnabled
         */
        public setEnabled (
            _isEnabled : boolean 
        ) : void;

        /**
         *  Removes all listeners.
         * @method removeAllEventListeners
         */
        public removeAllListeners (
        ) : void;

        /**
         *  Adds a event listener for a specified event with the priority of scene graph.<br>
         * -- param listener The listener of a specified event.<br>
         * -- param node The priority of the listener is based on the draw order of this node.<br>
         * -- note  The priority of scene graph will be fixed value 0. So the order of listener item<br>
         * -- in the vector will be ' <0, scene graph (0 priority), >0'.
         * @method addEventListenerWithSceneGraphPriority
         * @param {cc.EventListener} _listener
         * @param {cc.Node} _node
         */
        public addEventListenerWithSceneGraphPriority (
            _listener : cc.EventListener, 
            _node : cc.Node 
        ) : void;

        /**
         *  Adds a Custom event listener.<br>
         * -- It will use a fixed priority of 1.<br>
         * -- param eventName A given name of the event.<br>
         * -- param callback A given callback method that associated the event name.<br>
         * -- return the generated event. Needed in order to remove the event from the dispatcher
         * @method addCustomEventListener
         * @param {string} _eventName
         * @param {(arg0:cc.EventCustom) => void} _callback
         * @return {cc.EventListenerCustom}
         */
        public addCustomListener (
            _eventName : string, 
            _callback : (arg0:cc.EventCustom) => void 
        ) : cc.EventListenerCustom;

        /**
         *  Adds a event listener for a specified event with the fixed priority.<br>
         * -- param listener The listener of a specified event.<br>
         * -- param fixedPriority The fixed priority of the listener.<br>
         * -- note A lower priority will be called before the ones that have a higher value.<br>
         * -- 0 priority is forbidden for fixed priority since it's used for scene graph based priority.
         * @method addEventListenerWithFixedPriority
         * @param {cc.EventListener} _listener
         * @param {number} _fixedPriority
         */
        public addEventListenerWithFixedPriority (
            _listener : cc.EventListener, 
            _fixedPriority : number 
        ) : void;

        /**
         *  Removes all listeners which are associated with the specified target.<br>
         * -- param target A given target node.<br>
         * -- param recursive True if remove recursively, the default value is false.
         * @method removeEventListenersForTarget
         * @param {(cc.Node) | (cc.EventListener::Type)} _target | listenerType
         * @param {(boolean)} _recursive?
         */
        public removeListeners (
            _target_listenerType : (cc.Node) | (number), 
            _recursive? : (boolean)
        ) : void;

        /**
         *  Resumes all listeners which are associated the specified target.<br>
         * -- param target A given target node.<br>
         * -- param recursive True if resume recursively, the default value is false.
         * @method resumeEventListenersForTarget
         * @param {cc.Node} _target
         * @param {boolean} _recursive
         */
        public resumeTarget (
            _target : cc.Node, 
            _recursive : boolean 
        ) : void;

        /**
         *  Sets listener's priority with fixed value.<br>
         * -- param listener A given listener.<br>
         * -- param fixedPriority The fixed priority value.
         * @method setPriority
         * @param {cc.EventListener} _listener
         * @param {number} _fixedPriority
         */
        public setPriority (
            _listener : cc.EventListener, 
            _fixedPriority : number 
        ) : void;

        /**
         *  Dispatches the event.<br>
         * -- Also removes all EventListeners marked for deletion from the<br>
         * -- event dispatcher list.<br>
         * -- param event The event needs to be dispatched.
         * @method dispatchEvent
         * @param {cc.Event} _event
         */
        public dispatchEvent (
            _event : cc.Event 
        ) : void;

        /**
         *  Pauses all listeners which are associated the specified target.<br>
         * -- param target A given target node.<br>
         * -- param recursive True if pause recursively, the default value is false.
         * @method pauseEventListenersForTarget
         * @param {cc.Node} _target
         * @param {boolean} _recursive
         */
        public pauseTarget (
            _target : cc.Node, 
            _recursive : boolean 
        ) : void;

        /**
         *  Removes all custom listeners with the same event name.<br>
         * -- param customEventName A given event listener name which needs to be removed.
         * @method removeCustomEventListeners
         * @param {string} _customEventName
         */
        public removeCustomListeners (
            _customEventName : string 
        ) : void;

        /**
         *  Remove a listener.<br>
         * -- param listener The specified event listener which needs to be removed.
         * @method removeEventListener
         * @param {cc.EventListener} _listener
         */
        public removeListener (
            _listener : cc.EventListener 
        ) : void;

        /**
         *  Checks whether dispatching events is enabled.<br>
         * -- return True if dispatching events is enabled.
         * @method isEnabled
         * @return {boolean}
         */
        public isEnabled (
        ) : boolean;

        /**
         *  Constructor of EventDispatcher.
         * @method EventDispatcher
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class EventFocus
     * @native
     */
    export class EventFocus 
        extends cc.Event
    {

        /**
         *  Constructor.<br>
         * -- param widgetLoseFocus The widget which lose focus.<br>
         * -- param widgetGetFocus The widget which get focus.<br>
         * -- js ctor
         * @method EventFocus
         * @constructor
         * @param {ccui.Widget} _widgetLoseFocus
         * @param {ccui.Widget} _widgetGetFocus
         */
        public constructor (
            _widgetLoseFocus : ccui.Widget, 
            _widgetGetFocus : ccui.Widget 
        );

    }
    /**
     * @class EventListenerAcceleration
     * @native
     */
    export class EventListenerAcceleration 
        extends cc.EventListener
    {

        /**
         * 
         * @method init
         * @param {(arg0:cc.Acceleration, arg1:cc.Event) => void} _callback
         * @return {boolean}
         */
        public init (
            _callback : (arg0:cc.Acceleration, arg1:cc.Event) => void 
        ) : boolean;

        /**
         *  Create a acceleration EventListener.<br>
         * -- param callback The acceleration callback method.<br>
         * -- return An autoreleased EventListenerAcceleration object.
         * @method create
         * @param {(arg0:cc.Acceleration, arg1:cc.Event) => void} _callback
         * @return {cc.EventListenerAcceleration}
         */
        public static create (
            _callback : (arg0:cc.Acceleration, arg1:cc.Event) => void 
        ) : cc.EventListenerAcceleration;

        /**
         * 
         * @method EventListenerAcceleration
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class EventListenerCustom
     * @native
     */
    export class EventListenerCustom 
        extends cc.EventListener
    {

        /**
         *  Creates an event listener with type and callback.<br>
         * -- param eventName The type of the event.<br>
         * -- param callback The callback function when the specified event was emitted.<br>
         * -- return An autoreleased EventListenerCustom object.
         * @method create
         * @param {string} _eventName
         * @param {(arg0:cc.EventCustom) => void} _callback
         * @return {cc.EventListenerCustom}
         */
        public static create (
            _eventName : string, 
            _callback : (arg0:cc.EventCustom) => void 
        ) : cc.EventListenerCustom;

        /**
         *  Constructor 
         * @method EventListenerCustom
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class EventListenerFocus
     * @native
     */
    export class EventListenerFocus 
        extends cc.EventListener
    {

        /**
         * 
         * @method init
         * @return {boolean}
         */
        public init (
        ) : boolean;

        /**
         * 
         * @method EventListenerFocus
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class EventListenerKeyboard
     * @native
     */
    export class EventListenerKeyboard 
        extends cc.EventListener
    {

        /**
         * 
         * @method init
         * @return {boolean}
         */
        public init (
        ) : boolean;

        /**
         * 
         * @method EventListenerKeyboard
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class EventMouse
     * @native
     */
    export class EventMouse 
        extends cc.Event
    {
        public static readonly NONE: number;
        public static readonly DOWN: number;
        public static readonly UP: number;
        public static readonly MOVE: number;
        public static readonly SCROLL: number;
        public static readonly BUTTON_LEFT: number;
        public static readonly BUTTON_RIGHT: number;
        public static readonly BUTTON_MIDDLE: number;
        public static readonly BUTTON_4: number;
        public static readonly BUTTON_5: number;
        public static readonly BUTTON_6: number;
        public static readonly BUTTON_7: number;
        public static readonly BUTTON_8: number;


        /**
         *  Get mouse button.<br>
         * -- return The mouse button.<br>
         * -- js getButton
         * @method getMouseButton
         * @return {number}
         */
        public getButton (
        ) : number;

        /**
         *  Returns the current touch location in OpenGL coordinates.<br>
         * -- return The current touch location in OpenGL coordinates.
         * @method getLocation
         * @return {ctype.value_type<cc.Point>}
         */
        public getLocation (
        ) : ctype.value_type<cc.Point>;

        /**
         *  Set mouse button.<br>
         * -- param button a given mouse button.<br>
         * -- js setButton
         * @method setMouseButton
         * @param {number} _button
         */
        public setButton (
            _button : number 
        ) : void;

        /**
         *  Set mouse scroll data.<br>
         * -- param scrollX The scroll data of x axis.<br>
         * -- param scrollY The scroll data of y axis.
         * @method setScrollData
         * @param {number} _scrollX
         * @param {number} _scrollY
         */
        public setScrollData (
            _scrollX : number, 
            _scrollY : number 
        ) : void;

        /**
         *  Returns the previous touch location in screen coordinates.<br>
         * -- return The previous touch location in screen coordinates.<br>
         * -- js NA
         * @method getPreviousLocationInView
         * @return {ctype.value_type<cc.Point>}
         */
        public getPreviousLocationInView (
        ) : ctype.value_type<cc.Point>;

        /**
         *  Returns the delta of 2 current touches locations in screen coordinates.<br>
         * -- return The delta of 2 current touches locations in screen coordinates.
         * @method getDelta
         * @return {ctype.value_type<cc.Point>}
         */
        public getDelta (
        ) : ctype.value_type<cc.Point>;

        /**
         *  Returns the start touch location in OpenGL coordinates.<br>
         * -- return The start touch location in OpenGL coordinates.<br>
         * -- js NA
         * @method getStartLocation
         * @return {ctype.value_type<cc.Point>}
         */
        public getStartLocation (
        ) : ctype.value_type<cc.Point>;

        /**
         *  Get the cursor position of y axis.<br>
         * -- return The y coordinate of cursor position.<br>
         * -- js getLocationY
         * @method getCursorY
         * @return {number}
         */
        public getLocationY (
        ) : number;

        /**
         *  Get the cursor position of x axis.<br>
         * -- return The x coordinate of cursor position.<br>
         * -- js getLocationX
         * @method getCursorX
         * @return {number}
         */
        public getLocationX (
        ) : number;

        /**
         *  Returns the current touch location in screen coordinates.<br>
         * -- return The current touch location in screen coordinates.
         * @method getLocationInView
         * @return {ctype.value_type<cc.Point>}
         */
        public getLocationInView (
        ) : ctype.value_type<cc.Point>;

        /**
         *  Get mouse scroll data of y axis.<br>
         * -- return The scroll data of y axis.
         * @method getScrollY
         * @return {number}
         */
        public getScrollY (
        ) : number;

        /**
         *  Set the cursor position.<br>
         * -- param x The x coordinate of cursor position.<br>
         * -- param y The y coordinate of cursor position.<br>
         * -- js setLocation
         * @method setCursorPosition
         * @param {number} _x
         * @param {number} _y
         */
        public setLocation (
            _x : number, 
            _y : number 
        ) : void;

        /**
         *  Get mouse scroll data of x axis.<br>
         * -- return The scroll data of x axis.
         * @method getScrollX
         * @return {number}
         */
        public getScrollX (
        ) : number;

        /**
         *  Returns the previous touch location in OpenGL coordinates.<br>
         * -- return The previous touch location in OpenGL coordinates.<br>
         * -- js NA
         * @method getPreviousLocation
         * @return {ctype.value_type<cc.Point>}
         */
        public getPreviousLocation (
        ) : ctype.value_type<cc.Point>;

        /**
         *  Returns the start touch location in screen coordinates.<br>
         * -- return The start touch location in screen coordinates.<br>
         * -- js NA
         * @method getStartLocationInView
         * @return {ctype.value_type<cc.Point>}
         */
        public getStartLocationInView (
        ) : ctype.value_type<cc.Point>;

        /**
         *  Constructor.<br>
         * -- param mouseEventCode A given mouse event type.<br>
         * -- js ctor
         * @method EventMouse
         * @constructor
         * @param {cc.EventMouse::MouseEventType} _mouseEventCode
         */
        public constructor (
            _mouseEventCode : number 
        );

    }
    /**
     * @class EventListenerMouse
     * @native
     */
    export class EventListenerMouse 
        extends cc.EventListener
    {

        /**
         * 
         * @method init
         * @return {boolean}
         */
        public init (
        ) : boolean;

        /**
         * 
         * @method EventListenerMouse
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class EventListenerTouchOneByOne
     * @native
     */
    export class EventListenerTouchOneByOne 
        extends cc.EventListener
    {

        /**
         *  Is swall touches or not.<br>
         * -- return True if needs to swall touches.
         * @method isSwallowTouches
         * @return {boolean}
         */
        public isSwallowTouches (
        ) : boolean;

        /**
         *  Whether or not to swall touches.<br>
         * -- param needSwallow True if needs to swall touches.
         * @method setSwallowTouches
         * @param {boolean} _needSwallow
         */
        public setSwallowTouches (
            _needSwallow : boolean 
        ) : void;

        /**
         * 
         * @method init
         * @return {boolean}
         */
        public init (
        ) : boolean;

        /**
         * 
         * @method EventListenerTouchOneByOne
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class EventListenerTouchAllAtOnce
     * @native
     */
    export class EventListenerTouchAllAtOnce 
        extends cc.EventListener
    {

        /**
         * 
         * @method init
         * @return {boolean}
         */
        public init (
        ) : boolean;

        /**
         * 
         * @method EventListenerTouchAllAtOnce
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class ActionCamera
     * @native
     */
    export class ActionCamera 
        extends cc.ActionInterval
    {

        /**
         * 
         * @method setEye
         * @param {(number) | (ctype.value_type<cc.Vec3>)} _x | eye
         * @param {(number)} _y?
         * @param {(number)} _z?
         */
        public setEye (
            _x_eye : (number) | (ctype.value_type<cc.Vec3>), 
            _y? : (number), 
            _z? : (number)
        ) : void;

        /**
         * 
         * @method getEye
         * @return {ctype.value_type<cc.Vec3>}
         */
        public getEye (
        ) : ctype.value_type<cc.Vec3>;

        /**
         * 
         * @method setUp
         * @param {ctype.value_type<cc.Vec3>} _up
         */
        public setUp (
            _up : ctype.value_type<cc.Vec3> 
        ) : void;

        /**
         * 
         * @method getCenter
         * @return {ctype.value_type<cc.Vec3>}
         */
        public getCenter (
        ) : ctype.value_type<cc.Vec3>;

        /**
         * 
         * @method setCenter
         * @param {ctype.value_type<cc.Vec3>} _center
         */
        public setCenter (
            _center : ctype.value_type<cc.Vec3> 
        ) : void;

        /**
         * 
         * @method getUp
         * @return {ctype.value_type<cc.Vec3>}
         */
        public getUp (
        ) : ctype.value_type<cc.Vec3>;

        /**
         * js ctor<br>
         * -- lua new
         * @method ActionCamera
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class OrbitCamera
     * @native
     */
    export class OrbitCamera 
        extends cc.ActionCamera
    {

        /**
         *  Positions the camera according to spherical coordinates. <br>
         * -- param r The spherical radius.<br>
         * -- param zenith The spherical zenith.<br>
         * -- param azimuth The spherical azimuth.
         * @method sphericalRadius
         * @param {number} _r
         * @param {number} _zenith
         * @param {number} _azimuth
         */
        public sphericalRadius (
            _r : number, 
            _zenith : number, 
            _azimuth : number 
        ) : void;

        /**
         *  Initializes a OrbitCamera action with radius, delta-radius,  z, deltaZ, x, deltaX. 
         * @method initWithDuration
         * @param {number} _t
         * @param {number} _radius
         * @param {number} _deltaRadius
         * @param {number} _angleZ
         * @param {number} _deltaAngleZ
         * @param {number} _angleX
         * @param {number} _deltaAngleX
         * @return {boolean}
         */
        public initWithDuration (
            _t : number, 
            _radius : number, 
            _deltaRadius : number, 
            _angleZ : number, 
            _deltaAngleZ : number, 
            _angleX : number, 
            _deltaAngleX : number 
        ) : boolean;

        /**
         *  Creates a OrbitCamera action with radius, delta-radius,  z, deltaZ, x, deltaX. <br>
         * -- param t Duration in seconds.<br>
         * -- param radius The start radius.<br>
         * -- param deltaRadius The delta radius.<br>
         * -- param angleZ The start angle in Z.<br>
         * -- param deltaAngleZ The delta angle in Z.<br>
         * -- param angleX The start angle in X.<br>
         * -- param deltaAngleX The delta angle in X.<br>
         * -- return An OrbitCamera.
         * @method create
         * @param {number} _t
         * @param {number} _radius
         * @param {number} _deltaRadius
         * @param {number} _angleZ
         * @param {number} _deltaAngleZ
         * @param {number} _angleX
         * @param {number} _deltaAngleX
         * @return {cc.OrbitCamera}
         */
        public static create (
            _t : number, 
            _radius : number, 
            _deltaRadius : number, 
            _angleZ : number, 
            _deltaAngleZ : number, 
            _angleX : number, 
            _deltaAngleX : number 
        ) : cc.OrbitCamera;

        /**
         * js ctor
         * @method OrbitCamera
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class CardinalSplineTo
     * @native
     */
    export class CardinalSplineTo 
        extends cc.ActionInterval
    {

        /**
         *  Return a PointArray.<br>
         * -- return A PointArray.
         * @method getPoints
         * @return {ctype.value_type<cc.Point>}
         */
        public getPoints (
        ) : ctype.value_type<cc.Point>;

        /**
         *  It will update the target position and change the _previousPosition to newPos<br>
         * -- param newPos The new position.
         * @method updatePosition
         * @param {ctype.value_type<cc.Point>} _newPos
         */
        public updatePosition (
            _newPos : ctype.value_type<cc.Point> 
        ) : void;

        /**
         * js ctor<br>
         * -- lua NA
         * @method CardinalSplineTo
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class CardinalSplineBy
     * @native
     */
    export class CardinalSplineBy 
        extends cc.CardinalSplineTo
    {

        /**
         * 
         * @method CardinalSplineBy
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class CatmullRomTo
     * @native
     */
    export class CatmullRomTo 
        extends cc.CardinalSplineTo
    {

    }
    /**
     * @class CatmullRomBy
     * @native
     */
    export class CatmullRomBy 
        extends cc.CardinalSplineBy
    {

    }
    /**
     * @class ActionEase
     * @native
     */
    export abstract class ActionEase 
        extends cc.ActionInterval
    {

        /**
         * brief Initializes the action.<br>
         * -- return Return true when the initialization success, otherwise return false.
         * @method initWithAction
         * @param {cc.ActionInterval} _action
         * @return {boolean}
         */
        public initWithAction (
            _action : cc.ActionInterval 
        ) : boolean;

        /**
         * brief Get the pointer of the inner action.<br>
         * -- return The pointer of the inner action.
         * @method getInnerAction
         * @return {cc.ActionInterval}
         */
        public getInnerAction (
        ) : cc.ActionInterval;

    }
    /**
     * @class EaseRateAction
     * @native
     */
    export abstract class EaseRateAction 
        extends cc.ActionEase
    {

        /**
         * brief Set the rate value for the ease rate action.<br>
         * -- param rate The value will be set.
         * @method setRate
         * @param {number} _rate
         */
        public setRate (
            _rate : number 
        ) : void;

        /**
         * brief Initializes the action with the inner action and the rate parameter.<br>
         * -- param pAction The pointer of the inner action.<br>
         * -- param fRate The value of the rate parameter.<br>
         * -- return Return true when the initialization success, otherwise return false.
         * @method initWithAction
         * @param {cc.ActionInterval} _pAction
         * @param {number} _fRate
         * @return {boolean}
         */
        public initWithAction (
            _pAction : cc.ActionInterval, 
            _fRate : number 
        ) : boolean;

        /**
         * brief Get the rate value of the ease rate action.<br>
         * -- return Return the rate value of the ease rate action.
         * @method getRate
         * @return {number}
         */
        public getRate (
        ) : number;

        /**
         * brief Creates the action with the inner action and the rate parameter.<br>
         * -- param action A given ActionInterval<br>
         * -- param rate A given rate<br>
         * -- return An autoreleased EaseRateAction object.
         * @method create
         * @param {cc.ActionInterval} _action
         * @param {number} _rate
         * @return {cc.EaseRateAction}
         */
        public static create (
            _action : cc.ActionInterval, 
            _rate : number 
        ) : cc.EaseRateAction;

    }
    /**
     * @class EaseIn
     * @native
     */
    export class EaseIn 
        extends cc.EaseRateAction
    {

        /**
         * brief Create the action with the inner action and the rate parameter.<br>
         * -- param action The pointer of the inner action.<br>
         * -- param rate The value of the rate parameter.<br>
         * -- return A pointer of EaseIn action. If creation failed, return nil.
         * @method create
         * @param {cc.ActionInterval} _action
         * @param {number} _rate
         * @return {cc.EaseIn}
         */
        public static create (
            _action : cc.ActionInterval, 
            _rate : number 
        ) : cc.EaseIn;

        /**
         * 
         * @method EaseIn
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class EaseOut
     * @native
     */
    export class EaseOut 
        extends cc.EaseRateAction
    {

        /**
         * brief Create the action with the inner action and the rate parameter.<br>
         * -- param action The pointer of the inner action.<br>
         * -- param rate The value of the rate parameter.<br>
         * -- return A pointer of EaseOut action. If creation failed, return nil.
         * @method create
         * @param {cc.ActionInterval} _action
         * @param {number} _rate
         * @return {cc.EaseOut}
         */
        public static create (
            _action : cc.ActionInterval, 
            _rate : number 
        ) : cc.EaseOut;

        /**
         * 
         * @method EaseOut
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class EaseInOut
     * @native
     */
    export class EaseInOut 
        extends cc.EaseRateAction
    {

        /**
         * brief Create the action with the inner action and the rate parameter.<br>
         * -- param action The pointer of the inner action.<br>
         * -- param rate The value of the rate parameter.<br>
         * -- return A pointer of EaseInOut action. If creation failed, return nil.
         * @method create
         * @param {cc.ActionInterval} _action
         * @param {number} _rate
         * @return {cc.EaseInOut}
         */
        public static create (
            _action : cc.ActionInterval, 
            _rate : number 
        ) : cc.EaseInOut;

        /**
         * 
         * @method EaseInOut
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class EaseExponentialIn
     * @native
     */
    export class EaseExponentialIn 
        extends cc.ActionEase
    {

        /**
         * brief Create the action with the inner action.<br>
         * -- param action The pointer of the inner action.<br>
         * -- return A pointer of EaseExponentialIn action. If creation failed, return nil.
         * @method create
         * @param {cc.ActionInterval} _action
         * @return {cc.EaseExponentialIn}
         */
        public static create (
            _action : cc.ActionInterval 
        ) : cc.EaseExponentialIn;

        /**
         * 
         * @method EaseExponentialIn
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class EaseExponentialOut
     * @native
     */
    export class EaseExponentialOut 
        extends cc.ActionEase
    {

        /**
         * brief Create the action with the inner action.<br>
         * -- param action The pointer of the inner action.<br>
         * -- return A pointer of EaseExponentialOut action. If creation failed, return nil.
         * @method create
         * @param {cc.ActionInterval} _action
         * @return {cc.EaseExponentialOut}
         */
        public static create (
            _action : cc.ActionInterval 
        ) : cc.EaseExponentialOut;

        /**
         * 
         * @method EaseExponentialOut
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class EaseExponentialInOut
     * @native
     */
    export class EaseExponentialInOut 
        extends cc.ActionEase
    {

        /**
         * brief Create the action with the inner action.<br>
         * -- param action The pointer of the inner action.<br>
         * -- return A pointer of EaseExponentialInOut action. If creation failed, return nil.
         * @method create
         * @param {cc.ActionInterval} _action
         * @return {cc.EaseExponentialInOut}
         */
        public static create (
            _action : cc.ActionInterval 
        ) : cc.EaseExponentialInOut;

        /**
         * 
         * @method EaseExponentialInOut
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class EaseSineIn
     * @native
     */
    export class EaseSineIn 
        extends cc.ActionEase
    {

        /**
         * brief Create the action with the inner action.<br>
         * -- param action The pointer of the inner action.<br>
         * -- return A pointer of EaseSineIn action. If creation failed, return nil.
         * @method create
         * @param {cc.ActionInterval} _action
         * @return {cc.EaseSineIn}
         */
        public static create (
            _action : cc.ActionInterval 
        ) : cc.EaseSineIn;

        /**
         * 
         * @method EaseSineIn
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class EaseSineOut
     * @native
     */
    export class EaseSineOut 
        extends cc.ActionEase
    {

        /**
         * brief Create the action with the inner action.<br>
         * -- param action The pointer of the inner action.<br>
         * -- return A pointer of EaseSineOut action. If creation failed, return nil.
         * @method create
         * @param {cc.ActionInterval} _action
         * @return {cc.EaseSineOut}
         */
        public static create (
            _action : cc.ActionInterval 
        ) : cc.EaseSineOut;

        /**
         * 
         * @method EaseSineOut
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class EaseSineInOut
     * @native
     */
    export class EaseSineInOut 
        extends cc.ActionEase
    {

        /**
         * brief Create the action with the inner action.<br>
         * -- param action The pointer of the inner action.<br>
         * -- return A pointer of EaseSineInOut action. If creation failed, return nil.
         * @method create
         * @param {cc.ActionInterval} _action
         * @return {cc.EaseSineInOut}
         */
        public static create (
            _action : cc.ActionInterval 
        ) : cc.EaseSineInOut;

        /**
         * 
         * @method EaseSineInOut
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class EaseElastic
     * @native
     */
    export abstract class EaseElastic 
        extends cc.ActionEase
    {

        /**
         * brief Set period of the wave in radians.<br>
         * -- param fPeriod The value will be set.
         * @method setPeriod
         * @param {number} _fPeriod
         */
        public setPeriod (
            _fPeriod : number 
        ) : void;

        /**
         * brief Initializes the action with the inner action and the period in radians.<br>
         * -- param action The pointer of the inner action.<br>
         * -- param period Period of the wave in radians. Default is 0.3.<br>
         * -- return Return true when the initialization success, otherwise return false.
         * @method initWithAction
         * @param {cc.ActionInterval} _action
         * @param {number} _period
         * @return {boolean}
         */
        public initWithAction (
            _action : cc.ActionInterval, 
            _period : number 
        ) : boolean;

        /**
         * brief Get period of the wave in radians. Default value is 0.3.<br>
         * -- return Return the period of the wave in radians.
         * @method getPeriod
         * @return {number}
         */
        public getPeriod (
        ) : number;

    }
    /**
     * @class EaseElasticIn
     * @native
     */
    export class EaseElasticIn 
        extends cc.EaseElastic
    {

        /**
         * brief Create the EaseElasticIn action with the inner action and period value is 0.3.<br>
         * -- param action The pointer of the inner action.<br>
         * -- return A pointer of EaseElasticIn action. If creation failed, return nil.
         * @method create
         * @param {(cc.ActionInterval)} _action
         * @param {(number)} _period?
         * @return {cc.EaseElasticIn}
         */
        public static create (
            _action : (cc.ActionInterval), 
            _period? : (number)
        ) : cc.EaseElasticIn;

        /**
         * 
         * @method EaseElasticIn
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class EaseElasticOut
     * @native
     */
    export class EaseElasticOut 
        extends cc.EaseElastic
    {

        /**
         * brief Create the EaseElasticOut action with the inner action and period value is 0.3.<br>
         * -- param action The pointer of the inner action.<br>
         * -- return A pointer of EaseElasticOut action. If creation failed, return nil.
         * @method create
         * @param {(cc.ActionInterval)} _action
         * @param {(number)} _period?
         * @return {cc.EaseElasticOut}
         */
        public static create (
            _action : (cc.ActionInterval), 
            _period? : (number)
        ) : cc.EaseElasticOut;

        /**
         * 
         * @method EaseElasticOut
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class EaseElasticInOut
     * @native
     */
    export class EaseElasticInOut 
        extends cc.EaseElastic
    {

        /**
         * brief Create the EaseElasticInOut action with the inner action and period value is 0.3.<br>
         * -- param action The pointer of the inner action.<br>
         * -- return A pointer of EaseElasticInOut action. If creation failed, return nil.
         * @method create
         * @param {(cc.ActionInterval)} _action
         * @param {(number)} _period?
         * @return {cc.EaseElasticInOut}
         */
        public static create (
            _action : (cc.ActionInterval), 
            _period? : (number)
        ) : cc.EaseElasticInOut;

        /**
         * 
         * @method EaseElasticInOut
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class EaseBounce
     * @native
     */
    export abstract class EaseBounce 
        extends cc.ActionEase
    {

    }
    /**
     * @class EaseBounceIn
     * @native
     */
    export class EaseBounceIn 
        extends cc.EaseBounce
    {

        /**
         * brief Create the action with the inner action.<br>
         * -- param action The pointer of the inner action.<br>
         * -- return A pointer of EaseBounceIn action. If creation failed, return nil.
         * @method create
         * @param {cc.ActionInterval} _action
         * @return {cc.EaseBounceIn}
         */
        public static create (
            _action : cc.ActionInterval 
        ) : cc.EaseBounceIn;

        /**
         * 
         * @method EaseBounceIn
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class EaseBounceOut
     * @native
     */
    export class EaseBounceOut 
        extends cc.EaseBounce
    {

        /**
         * brief Create the action with the inner action.<br>
         * -- param action The pointer of the inner action.<br>
         * -- return A pointer of EaseBounceOut action. If creation failed, return nil.
         * @method create
         * @param {cc.ActionInterval} _action
         * @return {cc.EaseBounceOut}
         */
        public static create (
            _action : cc.ActionInterval 
        ) : cc.EaseBounceOut;

        /**
         * 
         * @method EaseBounceOut
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class EaseBounceInOut
     * @native
     */
    export class EaseBounceInOut 
        extends cc.EaseBounce
    {

        /**
         * brief Create the action with the inner action.<br>
         * -- param action The pointer of the inner action.<br>
         * -- return A pointer of EaseBounceInOut action. If creation failed, return nil.
         * @method create
         * @param {cc.ActionInterval} _action
         * @return {cc.EaseBounceInOut}
         */
        public static create (
            _action : cc.ActionInterval 
        ) : cc.EaseBounceInOut;

        /**
         * 
         * @method EaseBounceInOut
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class EaseBackIn
     * @native
     */
    export class EaseBackIn 
        extends cc.ActionEase
    {

        /**
         * brief Create the action with the inner action.<br>
         * -- param action The pointer of the inner action.<br>
         * -- return A pointer of EaseBackIn action. If creation failed, return nil.
         * @method create
         * @param {cc.ActionInterval} _action
         * @return {cc.EaseBackIn}
         */
        public static create (
            _action : cc.ActionInterval 
        ) : cc.EaseBackIn;

        /**
         * 
         * @method EaseBackIn
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class EaseBackOut
     * @native
     */
    export class EaseBackOut 
        extends cc.ActionEase
    {

        /**
         * brief Create the action with the inner action.<br>
         * -- param action The pointer of the inner action.<br>
         * -- return A pointer of EaseBackOut action. If creation failed, return nil.
         * @method create
         * @param {cc.ActionInterval} _action
         * @return {cc.EaseBackOut}
         */
        public static create (
            _action : cc.ActionInterval 
        ) : cc.EaseBackOut;

        /**
         * 
         * @method EaseBackOut
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class EaseBackInOut
     * @native
     */
    export class EaseBackInOut 
        extends cc.ActionEase
    {

        /**
         * brief Create the action with the inner action.<br>
         * -- param action The pointer of the inner action.<br>
         * -- return A pointer of EaseBackInOut action. If creation failed, return nil.
         * @method create
         * @param {cc.ActionInterval} _action
         * @return {cc.EaseBackInOut}
         */
        public static create (
            _action : cc.ActionInterval 
        ) : cc.EaseBackInOut;

        /**
         * 
         * @method EaseBackInOut
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class EaseBezierAction
     * @native
     */
    export class EaseBezierAction 
        extends cc.ActionEase
    {

        /**
         * brief Set the bezier parameters.
         * @method setBezierParamer
         * @param {number} _p0
         * @param {number} _p1
         * @param {number} _p2
         * @param {number} _p3
         */
        public setBezierParamer (
            _p0 : number, 
            _p1 : number, 
            _p2 : number, 
            _p3 : number 
        ) : void;

        /**
         * brief Create the action with the inner action.<br>
         * -- param action The pointer of the inner action.<br>
         * -- return A pointer of EaseBezierAction action. If creation failed, return nil.
         * @method create
         * @param {cc.ActionInterval} _action
         * @return {cc.EaseBezierAction}
         */
        public static create (
            _action : cc.ActionInterval 
        ) : cc.EaseBezierAction;

        /**
         * 
         * @method EaseBezierAction
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class EaseQuadraticActionIn
     * @native
     */
    export class EaseQuadraticActionIn 
        extends cc.ActionEase
    {

        /**
         * brief Create the action with the inner action.<br>
         * -- param action The pointer of the inner action.<br>
         * -- return A pointer of EaseQuadraticActionIn action. If creation failed, return nil.
         * @method create
         * @param {cc.ActionInterval} _action
         * @return {cc.EaseQuadraticActionIn}
         */
        public static create (
            _action : cc.ActionInterval 
        ) : cc.EaseQuadraticActionIn;

        /**
         * 
         * @method EaseQuadraticActionIn
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class EaseQuadraticActionOut
     * @native
     */
    export class EaseQuadraticActionOut 
        extends cc.ActionEase
    {

        /**
         * brief Create the action with the inner action.<br>
         * -- param action The pointer of the inner action.<br>
         * -- return A pointer of EaseQuadraticActionOut action. If creation failed, return nil.
         * @method create
         * @param {cc.ActionInterval} _action
         * @return {cc.EaseQuadraticActionOut}
         */
        public static create (
            _action : cc.ActionInterval 
        ) : cc.EaseQuadraticActionOut;

        /**
         * 
         * @method EaseQuadraticActionOut
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class EaseQuadraticActionInOut
     * @native
     */
    export class EaseQuadraticActionInOut 
        extends cc.ActionEase
    {

        /**
         * brief Create the action with the inner action.<br>
         * -- param action The pointer of the inner action.<br>
         * -- return A pointer of EaseQuadraticActionInOut action. If creation failed, return nil.
         * @method create
         * @param {cc.ActionInterval} _action
         * @return {cc.EaseQuadraticActionInOut}
         */
        public static create (
            _action : cc.ActionInterval 
        ) : cc.EaseQuadraticActionInOut;

        /**
         * 
         * @method EaseQuadraticActionInOut
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class EaseQuarticActionIn
     * @native
     */
    export class EaseQuarticActionIn 
        extends cc.ActionEase
    {

        /**
         * brief Create the action with the inner action.<br>
         * -- param action The pointer of the inner action.<br>
         * -- return A pointer of EaseQuarticActionIn action. If creation failed, return nil.
         * @method create
         * @param {cc.ActionInterval} _action
         * @return {cc.EaseQuarticActionIn}
         */
        public static create (
            _action : cc.ActionInterval 
        ) : cc.EaseQuarticActionIn;

        /**
         * 
         * @method EaseQuarticActionIn
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class EaseQuarticActionOut
     * @native
     */
    export class EaseQuarticActionOut 
        extends cc.ActionEase
    {

        /**
         * brief Create the action with the inner action.<br>
         * -- param action The pointer of the inner action.<br>
         * -- return A pointer of EaseQuarticActionOut action. If creation failed, return nil.
         * @method create
         * @param {cc.ActionInterval} _action
         * @return {cc.EaseQuarticActionOut}
         */
        public static create (
            _action : cc.ActionInterval 
        ) : cc.EaseQuarticActionOut;

        /**
         * 
         * @method EaseQuarticActionOut
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class EaseQuarticActionInOut
     * @native
     */
    export class EaseQuarticActionInOut 
        extends cc.ActionEase
    {

        /**
         * brief Create the action with the inner action.<br>
         * -- param action The pointer of the inner action.<br>
         * -- return A pointer of EaseQuarticActionInOut action. If creation failed, return nil.
         * @method create
         * @param {cc.ActionInterval} _action
         * @return {cc.EaseQuarticActionInOut}
         */
        public static create (
            _action : cc.ActionInterval 
        ) : cc.EaseQuarticActionInOut;

        /**
         * 
         * @method EaseQuarticActionInOut
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class EaseQuinticActionIn
     * @native
     */
    export class EaseQuinticActionIn 
        extends cc.ActionEase
    {

        /**
         * brief Create the action with the inner action.<br>
         * -- param action The pointer of the inner action.<br>
         * -- return A pointer of EaseQuinticActionIn action. If creation failed, return nil.
         * @method create
         * @param {cc.ActionInterval} _action
         * @return {cc.EaseQuinticActionIn}
         */
        public static create (
            _action : cc.ActionInterval 
        ) : cc.EaseQuinticActionIn;

        /**
         * 
         * @method EaseQuinticActionIn
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class EaseQuinticActionOut
     * @native
     */
    export class EaseQuinticActionOut 
        extends cc.ActionEase
    {

        /**
         * brief Create the action with the inner action.<br>
         * -- param action The pointer of the inner action.<br>
         * -- return A pointer of EaseQuinticActionOut action. If creation failed, return nil.
         * @method create
         * @param {cc.ActionInterval} _action
         * @return {cc.EaseQuinticActionOut}
         */
        public static create (
            _action : cc.ActionInterval 
        ) : cc.EaseQuinticActionOut;

        /**
         * 
         * @method EaseQuinticActionOut
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class EaseQuinticActionInOut
     * @native
     */
    export class EaseQuinticActionInOut 
        extends cc.ActionEase
    {

        /**
         * brief Create the action with the inner action.<br>
         * -- param action The pointer of the inner action.<br>
         * -- return A pointer of EaseQuinticActionInOut action. If creation failed, return nil.
         * @method create
         * @param {cc.ActionInterval} _action
         * @return {cc.EaseQuinticActionInOut}
         */
        public static create (
            _action : cc.ActionInterval 
        ) : cc.EaseQuinticActionInOut;

        /**
         * 
         * @method EaseQuinticActionInOut
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class EaseCircleActionIn
     * @native
     */
    export class EaseCircleActionIn 
        extends cc.ActionEase
    {

        /**
         * brief Create the action with the inner action.<br>
         * -- param action The pointer of the inner action.<br>
         * -- return A pointer of EaseCircleActionIn action. If creation failed, return nil.
         * @method create
         * @param {cc.ActionInterval} _action
         * @return {cc.EaseCircleActionIn}
         */
        public static create (
            _action : cc.ActionInterval 
        ) : cc.EaseCircleActionIn;

        /**
         * 
         * @method EaseCircleActionIn
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class EaseCircleActionOut
     * @native
     */
    export class EaseCircleActionOut 
        extends cc.ActionEase
    {

        /**
         * brief Create the action with the inner action.<br>
         * -- param action The pointer of the inner action.<br>
         * -- return A pointer of EaseCircleActionOut action. If creation failed, return nil.
         * @method create
         * @param {cc.ActionInterval} _action
         * @return {cc.EaseCircleActionOut}
         */
        public static create (
            _action : cc.ActionInterval 
        ) : cc.EaseCircleActionOut;

        /**
         * 
         * @method EaseCircleActionOut
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class EaseCircleActionInOut
     * @native
     */
    export class EaseCircleActionInOut 
        extends cc.ActionEase
    {

        /**
         * brief Create the action with the inner action.<br>
         * -- param action The pointer of the inner action.<br>
         * -- return A pointer of EaseCircleActionInOut action. If creation failed, return nil.
         * @method create
         * @param {cc.ActionInterval} _action
         * @return {cc.EaseCircleActionInOut}
         */
        public static create (
            _action : cc.ActionInterval 
        ) : cc.EaseCircleActionInOut;

        /**
         * 
         * @method EaseCircleActionInOut
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class EaseCubicActionIn
     * @native
     */
    export class EaseCubicActionIn 
        extends cc.ActionEase
    {

        /**
         * brief Create the action with the inner action.<br>
         * -- param action The pointer of the inner action.<br>
         * -- return A pointer of EaseCubicActionIn action. If creation failed, return nil.
         * @method create
         * @param {cc.ActionInterval} _action
         * @return {cc.EaseCubicActionIn}
         */
        public static create (
            _action : cc.ActionInterval 
        ) : cc.EaseCubicActionIn;

        /**
         * 
         * @method EaseCubicActionIn
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class EaseCubicActionOut
     * @native
     */
    export class EaseCubicActionOut 
        extends cc.ActionEase
    {

        /**
         * brief Create the action with the inner action.<br>
         * -- param action The pointer of the inner action.<br>
         * -- return A pointer of EaseCubicActionOut action. If creation failed, return nil.
         * @method create
         * @param {cc.ActionInterval} _action
         * @return {cc.EaseCubicActionOut}
         */
        public static create (
            _action : cc.ActionInterval 
        ) : cc.EaseCubicActionOut;

        /**
         * 
         * @method EaseCubicActionOut
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class EaseCubicActionInOut
     * @native
     */
    export class EaseCubicActionInOut 
        extends cc.ActionEase
    {

        /**
         * brief Create the action with the inner action.<br>
         * -- param action The pointer of the inner action.<br>
         * -- return A pointer of EaseCubicActionInOut action. If creation failed, return nil.
         * @method create
         * @param {cc.ActionInterval} _action
         * @return {cc.EaseCubicActionInOut}
         */
        public static create (
            _action : cc.ActionInterval 
        ) : cc.EaseCubicActionInOut;

        /**
         * 
         * @method EaseCubicActionInOut
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class ActionInstant
     * @native
     */
    export abstract class ActionInstant 
        extends cc.FiniteTimeAction
    {

    }
    /**
     * @class Show
     * @native
     */
    export class Show 
        extends cc.ActionInstant
    {

        /**
         *  Allocates and initializes the action.<br>
         * -- return  An autoreleased Show object.
         * @method create
         * @return {cc.Show}
         */
        public static create (
        ) : cc.Show;

        /**
         * 
         * @method Show
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class Hide
     * @native
     */
    export class Hide 
        extends cc.ActionInstant
    {

        /**
         *  Allocates and initializes the action.<br>
         * -- return An autoreleased Hide object.
         * @method create
         * @return {cc.Hide}
         */
        public static create (
        ) : cc.Hide;

        /**
         * 
         * @method Hide
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class ToggleVisibility
     * @native
     */
    export class ToggleVisibility 
        extends cc.ActionInstant
    {

        /**
         *  Allocates and initializes the action.<br>
         * -- return An autoreleased ToggleVisibility object.
         * @method create
         * @return {cc.ToggleVisibility}
         */
        public static create (
        ) : cc.ToggleVisibility;

        /**
         * 
         * @method ToggleVisibility
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class RemoveSelf
     * @native
     */
    export class RemoveSelf 
        extends cc.ActionInstant
    {

        /**
         *  init the action 
         * @method init
         * @param {boolean} _isNeedCleanUp
         * @return {boolean}
         */
        public init (
            _isNeedCleanUp : boolean 
        ) : boolean;

        /**
         *  Create the action.<br>
         * -- param isNeedCleanUp Is need to clean up, the default value is true.<br>
         * -- return An autoreleased RemoveSelf object.
         * @method create
         * @return {cc.RemoveSelf}
         */
        public static create (
        ) : cc.RemoveSelf;

        /**
         * 
         * @method RemoveSelf
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class FlipX
     * @native
     */
    export class FlipX 
        extends cc.ActionInstant
    {

        /**
         *  init the action 
         * @method initWithFlipX
         * @param {boolean} _x
         * @return {boolean}
         */
        public initWithFlipX (
            _x : boolean 
        ) : boolean;

        /**
         *  Create the action.<br>
         * -- param x Flips the sprite horizontally if true.<br>
         * -- return  An autoreleased FlipX object.
         * @method create
         * @param {boolean} _x
         * @return {cc.FlipX}
         */
        public static create (
            _x : boolean 
        ) : cc.FlipX;

        /**
         * 
         * @method FlipX
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class FlipY
     * @native
     */
    export class FlipY 
        extends cc.ActionInstant
    {

        /**
         *  init the action 
         * @method initWithFlipY
         * @param {boolean} _y
         * @return {boolean}
         */
        public initWithFlipY (
            _y : boolean 
        ) : boolean;

        /**
         *  Create the action.<br>
         * -- param y Flips the sprite vertically if true.<br>
         * -- return An autoreleased FlipY object.
         * @method create
         * @param {boolean} _y
         * @return {cc.FlipY}
         */
        public static create (
            _y : boolean 
        ) : cc.FlipY;

        /**
         * 
         * @method FlipY
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class Place
     * @native
     */
    export class Place 
        extends cc.ActionInstant
    {

        /**
         *  Initializes a Place action with a position 
         * @method initWithPosition
         * @param {ctype.value_type<cc.Point>} _pos
         * @return {boolean}
         */
        public initWithPosition (
            _pos : ctype.value_type<cc.Point> 
        ) : boolean;

        /**
         *  Creates a Place action with a position.<br>
         * -- param pos  A certain position.<br>
         * -- return  An autoreleased Place object.
         * @method create
         * @param {ctype.value_type<cc.Point>} _pos
         * @return {cc.Place}
         */
        public static create (
            _pos : ctype.value_type<cc.Point> 
        ) : cc.Place;

        /**
         * 
         * @method Place
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class CallFunc
     * @native
     */
    export class _CallFunc 
        extends cc.ActionInstant
    {

        /**
         *  Executes the callback.
         * @method execute
         */
        public execute (
        ) : void;

        /**
         * 
         * @method CallFunc
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class CallFuncN
     * @native
     */
    export class CallFunc 
        extends cc._CallFunc
    {

        /**
         * 
         * @method CallFuncN
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class GridAction
     * @native
     */
    export abstract class GridAction 
        extends cc.ActionInterval
    {

        /**
         * brief Get the pointer of GridBase.<br>
         * -- return The pointer of GridBase.
         * @method getGrid
         * @return {cc.GridBase}
         */
        public getGrid (
        ) : cc.GridBase;

        /**
         * brief Initializes the action with size and duration.<br>
         * -- param duration The duration of the GridAction. It's a value in seconds.<br>
         * -- param gridSize The size of the GridAction should be.<br>
         * -- return Return true when the initialization success, otherwise return false.
         * @method initWithDuration
         * @param {number} _duration
         * @param {ctype.value_type<cc.Size>} _gridSize
         * @return {boolean}
         */
        public initWithDuration (
            _duration : number, 
            _gridSize : ctype.value_type<cc.Size> 
        ) : boolean;

    }
    /**
     * @class Grid3DAction
     * @native
     */
    export abstract class Grid3DAction 
        extends cc.GridAction
    {

        /**
         * brief Get the effect grid rect.<br>
         * -- return Return the effect grid rect.
         * @method getGridRect
         * @return {ctype.value_type<cc.Rect>}
         */
        public getGridRect (
        ) : ctype.value_type<cc.Rect>;

    }
    /**
     * @class TiledGrid3DAction
     * @native
     */
    export abstract class TiledGrid3DAction 
        extends cc.GridAction
    {

    }
    /**
     * @class StopGrid
     * @native
     */
    export class StopGrid 
        extends cc.ActionInstant
    {

        /**
         * brief Create a StopGrid Action.<br>
         * -- return Return a pointer of StopGrid. When the creation failed, return nil.
         * @method create
         * @return {cc.StopGrid}
         */
        public static create (
        ) : cc.StopGrid;

        /**
         * 
         * @method StopGrid
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class ReuseGrid
     * @native
     */
    export class ReuseGrid 
        extends cc.ActionInstant
    {

        /**
         * brief Initializes an action with the number of times that the current grid will be reused.<br>
         * -- param times Specify times the grid will be reused.<br>
         * -- return If the initialization success, return true; otherwise, return false.
         * @method initWithTimes
         * @param {number} _times
         * @return {boolean}
         */
        public initWithTimes (
            _times : number 
        ) : boolean;

        /**
         * brief Create an action with the number of times that the current grid will be reused.<br>
         * -- param times Specify times the grid will be reused.<br>
         * -- return Return a pointer of ReuseGrid. When the creation failed, return nil.
         * @method create
         * @param {number} _times
         * @return {cc.ReuseGrid}
         */
        public static create (
            _times : number 
        ) : cc.ReuseGrid;

        /**
         * 
         * @method ReuseGrid
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class Waves3D
     * @native
     */
    export class Waves3D 
        extends cc.Grid3DAction
    {

        /**
         * brief Set the amplitude rate of the effect.<br>
         * -- param amplitudeRate The value of amplitude rate will be set.
         * @method setAmplitudeRate
         * @param {number} _amplitudeRate
         */
        public setAmplitudeRate (
            _amplitudeRate : number 
        ) : void;

        /**
         * brief Initializes an action with duration, grid size, waves and amplitude.<br>
         * -- param duration Specify the duration of the Waves3D action. It's a value in seconds.<br>
         * -- param gridSize Specify the size of the grid.<br>
         * -- param waves Specify the waves count of the Waves3D action.<br>
         * -- param amplitude Specify the amplitude of the Waves3D action.<br>
         * -- return If the initialization success, return true; otherwise, return false.
         * @method initWithDuration
         * @param {number} _duration
         * @param {ctype.value_type<cc.Size>} _gridSize
         * @param {number} _waves
         * @param {number} _amplitude
         * @return {boolean}
         */
        public initWithDuration (
            _duration : number, 
            _gridSize : ctype.value_type<cc.Size>, 
            _waves : number, 
            _amplitude : number 
        ) : boolean;

        /**
         * brief Get the amplitude of the effect.<br>
         * -- return Return the amplitude of the effect.
         * @method getAmplitude
         * @return {number}
         */
        public getAmplitude (
        ) : number;

        /**
         * brief Get the amplitude rate of the effect.<br>
         * -- return Return the amplitude rate of the effect.
         * @method getAmplitudeRate
         * @return {number}
         */
        public getAmplitudeRate (
        ) : number;

        /**
         * brief Set the amplitude to the effect.<br>
         * -- param amplitude The value of amplitude will be set.
         * @method setAmplitude
         * @param {number} _amplitude
         */
        public setAmplitude (
            _amplitude : number 
        ) : void;

        /**
         * brief Create an action with duration, grid size, waves and amplitude.<br>
         * -- param duration Specify the duration of the Waves3D action. It's a value in seconds.<br>
         * -- param gridSize Specify the size of the grid.<br>
         * -- param waves Specify the waves count of the Waves3D action.<br>
         * -- param amplitude Specify the amplitude of the Waves3D action.<br>
         * -- return If the creation success, return a pointer of Waves3D action; otherwise, return nil.
         * @method create
         * @param {number} _duration
         * @param {ctype.value_type<cc.Size>} _gridSize
         * @param {number} _waves
         * @param {number} _amplitude
         * @return {cc.Waves3D}
         */
        public static create (
            _duration : number, 
            _gridSize : ctype.value_type<cc.Size>, 
            _waves : number, 
            _amplitude : number 
        ) : cc.Waves3D;

        /**
         * 
         * @method Waves3D
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class FlipX3D
     * @native
     */
    export class FlipX3D 
        extends cc.Grid3DAction
    {

        /**
         * brief Initializes an action with duration and grid size.<br>
         * -- param gridSize Specify the grid size of the FlipX3D action.<br>
         * -- param duration Specify the duration of the FlipX3D action. It's a value in seconds.<br>
         * -- return If the initialization success, return true; otherwise, return false.
         * @method initWithSize
         * @param {ctype.value_type<cc.Size>} _gridSize
         * @param {number} _duration
         * @return {boolean}
         */
        public initWithSize (
            _gridSize : ctype.value_type<cc.Size>, 
            _duration : number 
        ) : boolean;

        /**
         * brief Initializes an action with duration.<br>
         * -- param duration Specify the duration of the FlipX3D action. It's a value in seconds.<br>
         * -- return If the initialization success, return true; otherwise, return false.
         * @method initWithDuration
         * @param {number} _duration
         * @return {boolean}
         */
        public initWithDuration (
            _duration : number 
        ) : boolean;

        /**
         * brief Create the action with duration.<br>
         * -- param duration Specify the duration of the FilpX3D action. It's a value in seconds.<br>
         * -- return If the creation success, return a pointer of FilpX3D action; otherwise, return nil.
         * @method create
         * @param {number} _duration
         * @return {cc.FlipX3D}
         */
        public static create (
            _duration : number 
        ) : cc.FlipX3D;

        /**
         * 
         * @method FlipX3D
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class FlipY3D
     * @native
     */
    export class FlipY3D 
        extends cc.FlipX3D
    {

        /**
         * brief Create the action with duration.<br>
         * -- param duration Specify the duration of the FlipY3D action. It's a value in seconds.<br>
         * -- return If the creation success, return a pointer of FlipY3D action; otherwise, return nil.
         * @method create
         * @param {number} _duration
         * @return {cc.FlipY3D}
         */
        public static create (
            _duration : number 
        ) : cc.FlipY3D;

        /**
         * 
         * @method FlipY3D
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class Lens3D
     * @native
     */
    export class Lens3D 
        extends cc.Grid3DAction
    {

        /**
         * brief Set whether lens is concave.<br>
         * -- param concave Whether lens is concave.
         * @method setConcave
         * @param {boolean} _concave
         */
        public setConcave (
            _concave : boolean 
        ) : void;

        /**
         * brief Initializes the action with center position, radius, grid size and duration.<br>
         * -- param duration Specify the duration of the Lens3D action. It's a value in seconds.<br>
         * -- param gridSize Specify the size of the grid.<br>
         * -- param position Specify the center position of the lens effect.<br>
         * -- param radius Specify the radius of the lens effect.<br>
         * -- return If the initialization success, return true; otherwise, return false.
         * @method initWithDuration
         * @param {number} _duration
         * @param {ctype.value_type<cc.Size>} _gridSize
         * @param {ctype.value_type<cc.Point>} _position
         * @param {number} _radius
         * @return {boolean}
         */
        public initWithDuration (
            _duration : number, 
            _gridSize : ctype.value_type<cc.Size>, 
            _position : ctype.value_type<cc.Point>, 
            _radius : number 
        ) : boolean;

        /**
         * brief Set the value of lens effect.<br>
         * -- param lensEffect The value of lens effect will be set.
         * @method setLensEffect
         * @param {number} _lensEffect
         */
        public setLensEffect (
            _lensEffect : number 
        ) : void;

        /**
         * brief Get the value of lens effect. Default value is 0.7.<br>
         * -- return The value of lens effect.
         * @method getLensEffect
         * @return {number}
         */
        public getLensEffect (
        ) : number;

        /**
         * brief Set the center position of lens effect.<br>
         * -- param position The center position will be set.
         * @method setPosition
         * @param {ctype.value_type<cc.Point>} _position
         */
        public setPosition (
            _position : ctype.value_type<cc.Point> 
        ) : void;

        /**
         * brief Get the center position of lens effect.<br>
         * -- return The center position of lens effect.
         * @method getPosition
         * @return {ctype.value_type<cc.Point>}
         */
        public getPosition (
        ) : ctype.value_type<cc.Point>;

        /**
         * brief Create the action with center position, radius, a grid size and duration.<br>
         * -- param duration Specify the duration of the Lens3D action. It's a value in seconds.<br>
         * -- param gridSize Specify the size of the grid.<br>
         * -- param position Specify the center position of the lens.<br>
         * -- param radius Specify the radius of the lens.<br>
         * -- return If the creation success, return a pointer of Lens3D action; otherwise, return nil.
         * @method create
         * @param {number} _duration
         * @param {ctype.value_type<cc.Size>} _gridSize
         * @param {ctype.value_type<cc.Point>} _position
         * @param {number} _radius
         * @return {cc.Lens3D}
         */
        public static create (
            _duration : number, 
            _gridSize : ctype.value_type<cc.Size>, 
            _position : ctype.value_type<cc.Point>, 
            _radius : number 
        ) : cc.Lens3D;

        /**
         * 
         * @method Lens3D
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class Ripple3D
     * @native
     */
    export class Ripple3D 
        extends cc.Grid3DAction
    {

        /**
         * brief Set the amplitude rate of ripple effect.<br>
         * -- param fAmplitudeRate The amplitude rate of ripple effect.
         * @method setAmplitudeRate
         * @param {number} _fAmplitudeRate
         */
        public setAmplitudeRate (
            _fAmplitudeRate : number 
        ) : void;

        /**
         * brief Initializes the action with center position, radius, number of waves, amplitude, a grid size and duration.<br>
         * -- param duration Specify the duration of the Ripple3D action. It's a value in seconds.<br>
         * -- param gridSize Specify the size of the grid.<br>
         * -- param position Specify the center position of the ripple effect.<br>
         * -- param radius Specify the radius of the ripple effect.<br>
         * -- param waves Specify the waves count of the ripple effect.<br>
         * -- param amplitude Specify the amplitude of the ripple effect.<br>
         * -- return If the initialization success, return true; otherwise, return false.
         * @method initWithDuration
         * @param {number} _duration
         * @param {ctype.value_type<cc.Size>} _gridSize
         * @param {ctype.value_type<cc.Point>} _position
         * @param {number} _radius
         * @param {number} _waves
         * @param {number} _amplitude
         * @return {boolean}
         */
        public initWithDuration (
            _duration : number, 
            _gridSize : ctype.value_type<cc.Size>, 
            _position : ctype.value_type<cc.Point>, 
            _radius : number, 
            _waves : number, 
            _amplitude : number 
        ) : boolean;

        /**
         * brief Get the amplitude rate of ripple effect.<br>
         * -- return The amplitude rate of ripple effect.
         * @method getAmplitudeRate
         * @return {number}
         */
        public getAmplitudeRate (
        ) : number;

        /**
         * brief Set the amplitude of ripple effect.<br>
         * -- param fAmplitude The amplitude of ripple effect.
         * @method setAmplitude
         * @param {number} _fAmplitude
         */
        public setAmplitude (
            _fAmplitude : number 
        ) : void;

        /**
         * brief Get the amplitude of ripple effect.<br>
         * -- return The amplitude of ripple effect.
         * @method getAmplitude
         * @return {number}
         */
        public getAmplitude (
        ) : number;

        /**
         * brief Set the center position of ripple effect.<br>
         * -- param position The center position of ripple effect will be set.
         * @method setPosition
         * @param {ctype.value_type<cc.Point>} _position
         */
        public setPosition (
            _position : ctype.value_type<cc.Point> 
        ) : void;

        /**
         * brief Get the center position of ripple effect.<br>
         * -- return The center position of ripple effect.
         * @method getPosition
         * @return {ctype.value_type<cc.Point>}
         */
        public getPosition (
        ) : ctype.value_type<cc.Point>;

        /**
         * brief Create the action with center position, radius, number of waves, amplitude, a grid size and duration.<br>
         * -- param duration Specify the duration of the Ripple3D action. It's a value in seconds.<br>
         * -- param gridSize Specify the size of the grid.<br>
         * -- param position Specify the center position of the ripple effect.<br>
         * -- param radius Specify the radius of the ripple effect.<br>
         * -- param waves Specify the waves count of the ripple effect.<br>
         * -- param amplitude Specify the amplitude of the ripple effect.<br>
         * -- return If the creation success, return a pointer of Ripple3D action; otherwise, return nil.
         * @method create
         * @param {number} _duration
         * @param {ctype.value_type<cc.Size>} _gridSize
         * @param {ctype.value_type<cc.Point>} _position
         * @param {number} _radius
         * @param {number} _waves
         * @param {number} _amplitude
         * @return {cc.Ripple3D}
         */
        public static create (
            _duration : number, 
            _gridSize : ctype.value_type<cc.Size>, 
            _position : ctype.value_type<cc.Point>, 
            _radius : number, 
            _waves : number, 
            _amplitude : number 
        ) : cc.Ripple3D;

        /**
         * 
         * @method Ripple3D
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class Shaky3D
     * @native
     */
    export class Shaky3D 
        extends cc.Grid3DAction
    {

        /**
         * brief Initializes the action with a range, shake Z vertices, grid size and duration.<br>
         * -- param duration Specify the duration of the Shaky3D action. It's a value in seconds.<br>
         * -- param gridSize Specify the size of the grid.<br>
         * -- param range Specify the range of the shaky effect.<br>
         * -- param shakeZ Specify whether shake on the z axis.<br>
         * -- return If the Initialization success, return true; otherwise, return false.
         * @method initWithDuration
         * @param {number} _duration
         * @param {ctype.value_type<cc.Size>} _gridSize
         * @param {number} _range
         * @param {boolean} _shakeZ
         * @return {boolean}
         */
        public initWithDuration (
            _duration : number, 
            _gridSize : ctype.value_type<cc.Size>, 
            _range : number, 
            _shakeZ : boolean 
        ) : boolean;

        /**
         * brief Create the action with a range, shake Z vertices, a grid and duration.<br>
         * -- param initWithDuration Specify the duration of the Shaky3D action. It's a value in seconds.<br>
         * -- param gridSize Specify the size of the grid.<br>
         * -- param range Specify the range of the shaky effect.<br>
         * -- param shakeZ Specify whether shake on the z axis.<br>
         * -- return If the creation success, return a pointer of Shaky3D action; otherwise, return nil.
         * @method create
         * @param {number} _initWithDuration
         * @param {ctype.value_type<cc.Size>} _gridSize
         * @param {number} _range
         * @param {boolean} _shakeZ
         * @return {cc.Shaky3D}
         */
        public static create (
            _initWithDuration : number, 
            _gridSize : ctype.value_type<cc.Size>, 
            _range : number, 
            _shakeZ : boolean 
        ) : cc.Shaky3D;

        /**
         * 
         * @method Shaky3D
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class Liquid
     * @native
     */
    export class Liquid 
        extends cc.Grid3DAction
    {

        /**
         * brief Set the amplitude rate of the effect.<br>
         * -- param amplitudeRate The value of amplitude rate will be set.
         * @method setAmplitudeRate
         * @param {number} _amplitudeRate
         */
        public setAmplitudeRate (
            _amplitudeRate : number 
        ) : void;

        /**
         * brief Initializes the action with amplitude, grid size, waves count and duration.<br>
         * -- param duration Specify the duration of the Liquid action. It's a value in seconds.<br>
         * -- param gridSize Specify the size of the grid.<br>
         * -- param waves Specify the waves count of the Liquid action.<br>
         * -- param amplitude Specify the amplitude of the Liquid action.<br>
         * -- return If the initialization success, return true; otherwise, return false.
         * @method initWithDuration
         * @param {number} _duration
         * @param {ctype.value_type<cc.Size>} _gridSize
         * @param {number} _waves
         * @param {number} _amplitude
         * @return {boolean}
         */
        public initWithDuration (
            _duration : number, 
            _gridSize : ctype.value_type<cc.Size>, 
            _waves : number, 
            _amplitude : number 
        ) : boolean;

        /**
         * brief Get the amplitude of the effect.<br>
         * -- return Return the amplitude of the effect.
         * @method getAmplitude
         * @return {number}
         */
        public getAmplitude (
        ) : number;

        /**
         * brief Get the amplitude rate of the effect.<br>
         * -- return Return the amplitude rate of the effect.
         * @method getAmplitudeRate
         * @return {number}
         */
        public getAmplitudeRate (
        ) : number;

        /**
         * brief Set the amplitude to the effect.<br>
         * -- param amplitude The value of amplitude will be set.
         * @method setAmplitude
         * @param {number} _amplitude
         */
        public setAmplitude (
            _amplitude : number 
        ) : void;

        /**
         * brief Create the action with amplitude, grid size, waves count and duration.<br>
         * -- param duration Specify the duration of the Liquid action. It's a value in seconds.<br>
         * -- param gridSize Specify the size of the grid.<br>
         * -- param waves Specify the waves count of the Liquid action.<br>
         * -- param amplitude Specify the amplitude of the Liquid action.<br>
         * -- return If the creation success, return a pointer of Liquid action; otherwise, return nil.
         * @method create
         * @param {number} _duration
         * @param {ctype.value_type<cc.Size>} _gridSize
         * @param {number} _waves
         * @param {number} _amplitude
         * @return {cc.Liquid}
         */
        public static create (
            _duration : number, 
            _gridSize : ctype.value_type<cc.Size>, 
            _waves : number, 
            _amplitude : number 
        ) : cc.Liquid;

        /**
         * 
         * @method Liquid
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class Waves
     * @native
     */
    export class Waves 
        extends cc.Grid3DAction
    {

        /**
         * brief Set the amplitude rate of the effect.<br>
         * -- param amplitudeRate The value of amplitude rate will be set.
         * @method setAmplitudeRate
         * @param {number} _amplitudeRate
         */
        public setAmplitudeRate (
            _amplitudeRate : number 
        ) : void;

        /**
         * brief Initializes the action with amplitude, horizontal sin, vertical sin, grid size, waves count and duration.<br>
         * -- param duration Specify the duration of the Waves action. It's a value in seconds.<br>
         * -- param gridSize Specify the size of the grid.<br>
         * -- param waves Specify the waves count of the Waves action.<br>
         * -- param amplitude Specify the amplitude of the Waves action.<br>
         * -- param horizontal Specify whether waves on horizontal.<br>
         * -- param vertical Specify whether waves on vertical.<br>
         * -- return If the initialization success, return true; otherwise, return false.
         * @method initWithDuration
         * @param {number} _duration
         * @param {ctype.value_type<cc.Size>} _gridSize
         * @param {number} _waves
         * @param {number} _amplitude
         * @param {boolean} _horizontal
         * @param {boolean} _vertical
         * @return {boolean}
         */
        public initWithDuration (
            _duration : number, 
            _gridSize : ctype.value_type<cc.Size>, 
            _waves : number, 
            _amplitude : number, 
            _horizontal : boolean, 
            _vertical : boolean 
        ) : boolean;

        /**
         * brief Get the amplitude of the effect.<br>
         * -- return Return the amplitude of the effect.
         * @method getAmplitude
         * @return {number}
         */
        public getAmplitude (
        ) : number;

        /**
         * brief Get the amplitude rate of the effect.<br>
         * -- return Return the amplitude rate of the effect.
         * @method getAmplitudeRate
         * @return {number}
         */
        public getAmplitudeRate (
        ) : number;

        /**
         * brief Set the amplitude to the effect.<br>
         * -- param amplitude The value of amplitude will be set.
         * @method setAmplitude
         * @param {number} _amplitude
         */
        public setAmplitude (
            _amplitude : number 
        ) : void;

        /**
         * brief Create the action with amplitude, horizontal sin, vertical sin, grid size, waves count and duration.<br>
         * -- param duration Specify the duration of the Waves action. It's a value in seconds.<br>
         * -- param gridSize Specify the size of the grid.<br>
         * -- param waves Specify the waves count of the Waves action.<br>
         * -- param amplitude Specify the amplitude of the Waves action.<br>
         * -- param horizontal Specify whether waves on horizontal.<br>
         * -- param vertical Specify whether waves on vertical.<br>
         * -- return If the creation success, return a pointer of Waves action; otherwise, return nil.
         * @method create
         * @param {number} _duration
         * @param {ctype.value_type<cc.Size>} _gridSize
         * @param {number} _waves
         * @param {number} _amplitude
         * @param {boolean} _horizontal
         * @param {boolean} _vertical
         * @return {cc.Waves}
         */
        public static create (
            _duration : number, 
            _gridSize : ctype.value_type<cc.Size>, 
            _waves : number, 
            _amplitude : number, 
            _horizontal : boolean, 
            _vertical : boolean 
        ) : cc.Waves;

        /**
         * 
         * @method Waves
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class Twirl
     * @native
     */
    export class Twirl 
        extends cc.Grid3DAction
    {

        /**
         * brief Set the amplitude rate of the effect.<br>
         * -- param amplitudeRate The value of amplitude rate will be set.
         * @method setAmplitudeRate
         * @param {number} _amplitudeRate
         */
        public setAmplitudeRate (
            _amplitudeRate : number 
        ) : void;

        /**
         * brief Initializes the action with center position, number of twirls, amplitude, a grid size and duration.<br>
         * -- param duration Specify the duration of the Twirl action. It's a value in seconds.<br>
         * -- param gridSize Specify the size of the grid.<br>
         * -- param position Specify the center position of the twirl action.<br>
         * -- param twirls Specify the twirls count of the Twirl action.<br>
         * -- param amplitude Specify the amplitude of the Twirl action.<br>
         * -- return If the initialization success, return true; otherwise, return false.
         * @method initWithDuration
         * @param {number} _duration
         * @param {ctype.value_type<cc.Size>} _gridSize
         * @param {ctype.value_type<cc.Point>} _position
         * @param {number} _twirls
         * @param {number} _amplitude
         * @return {boolean}
         */
        public initWithDuration (
            _duration : number, 
            _gridSize : ctype.value_type<cc.Size>, 
            _position : ctype.value_type<cc.Point>, 
            _twirls : number, 
            _amplitude : number 
        ) : boolean;

        /**
         * brief Get the amplitude rate of the effect.<br>
         * -- return Return the amplitude rate of the effect.
         * @method getAmplitudeRate
         * @return {number}
         */
        public getAmplitudeRate (
        ) : number;

        /**
         * brief Set the amplitude to the effect.<br>
         * -- param amplitude The value of amplitude will be set.
         * @method setAmplitude
         * @param {number} _amplitude
         */
        public setAmplitude (
            _amplitude : number 
        ) : void;

        /**
         * brief Get the amplitude of the effect.<br>
         * -- return Return the amplitude of the effect.
         * @method getAmplitude
         * @return {number}
         */
        public getAmplitude (
        ) : number;

        /**
         * brief Set the center position of twirl action.<br>
         * -- param position The center position of twirl action will be set.
         * @method setPosition
         * @param {ctype.value_type<cc.Point>} _position
         */
        public setPosition (
            _position : ctype.value_type<cc.Point> 
        ) : void;

        /**
         * brief Get the center position of twirl action.<br>
         * -- return The center position of twirl action.
         * @method getPosition
         * @return {ctype.value_type<cc.Point>}
         */
        public getPosition (
        ) : ctype.value_type<cc.Point>;

        /**
         * brief Create the action with center position, number of twirls, amplitude, a grid size and duration.<br>
         * -- param duration Specify the duration of the Twirl action. It's a value in seconds.<br>
         * -- param gridSize Specify the size of the grid.<br>
         * -- param position Specify the center position of the twirl action.<br>
         * -- param twirls Specify the twirls count of the Twirl action.<br>
         * -- param amplitude Specify the amplitude of the Twirl action.<br>
         * -- return If the creation success, return a pointer of Twirl action; otherwise, return nil.
         * @method create
         * @param {number} _duration
         * @param {ctype.value_type<cc.Size>} _gridSize
         * @param {ctype.value_type<cc.Point>} _position
         * @param {number} _twirls
         * @param {number} _amplitude
         * @return {cc.Twirl}
         */
        public static create (
            _duration : number, 
            _gridSize : ctype.value_type<cc.Size>, 
            _position : ctype.value_type<cc.Point>, 
            _twirls : number, 
            _amplitude : number 
        ) : cc.Twirl;

        /**
         * 
         * @method Twirl
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class ActionManager
     * @native
     */
    export class ActionManager 
    {

        /**
         *  Gets an action given its tag an a target.<br>
         * -- param tag       The action's tag.<br>
         * -- param target    A certain target.<br>
         * -- return  The Action the with the given tag.
         * @method getActionByTag
         * @param {number} _tag
         * @param {cc.Node} _target
         * @return {cc.Action}
         */
        public getActionByTag (
            _tag : number, 
            _target : cc.Node 
        ) : cc.Action;

        /**
         *  Removes an action given its tag and the target.<br>
         * -- param tag       The action's tag.<br>
         * -- param target    A certain target.
         * @method removeActionByTag
         * @param {number} _tag
         * @param {cc.Node} _target
         */
        public removeActionByTag (
            _tag : number, 
            _target : cc.Node 
        ) : void;

        /**
         *  Removes all actions matching at least one bit in flags and the target.<br>
         * -- param flags     The flag field to match the actions' flags based on bitwise AND.<br>
         * -- param target    A certain target.<br>
         * -- js NA
         * @method removeActionsByFlags
         * @param {number} _flags
         * @param {cc.Node} _target
         */
        public removeActionsByFlags (
            _flags : number, 
            _target : cc.Node 
        ) : void;

        /**
         *  Removes all actions from all the targets.
         * @method removeAllActions
         */
        public removeAllActions (
        ) : void;

        /**
         *  Adds an action with a target. <br>
         * -- If the target is already present, then the action will be added to the existing target.<br>
         * -- If the target is not present, a new instance of this target will be created either paused or not, and the action will be added to the newly created target.<br>
         * -- When the target is paused, the queued actions won't be 'ticked'.<br>
         * -- param action    A certain action.<br>
         * -- param target    The target which need to be added an action.<br>
         * -- param paused    Is the target paused or not.
         * @method addAction
         * @param {cc.Action} _action
         * @param {cc.Node} _target
         * @param {boolean} _paused
         */
        public addAction (
            _action : cc.Action, 
            _target : cc.Node, 
            _paused : boolean 
        ) : void;

        /**
         *  Resumes the target. All queued actions will be resumed.<br>
         * -- param target    A certain target.
         * @method resumeTarget
         * @param {cc.Node} _target
         */
        public resumeTarget (
            _target : cc.Node 
        ) : void;

        /**
         *  Main loop of ActionManager.<br>
         * -- param dt    In seconds.
         * @method update
         * @param {number} _dt
         */
        public update (
            _dt : number 
        ) : void;

        /**
         *  Pauses the target: all running actions and newly added actions will be paused.<br>
         * -- param target    A certain target.
         * @method pauseTarget
         * @param {cc.Node} _target
         */
        public pauseTarget (
            _target : cc.Node 
        ) : void;

        /**
         *  Returns the numbers of actions that are running in a certain target. <br>
         * -- Composable actions are counted as 1 action. Example:<br>
         * -- - If you are running 1 Sequence of 7 actions, it will return 1.<br>
         * -- - If you are running 7 Sequences of 2 actions, it will return 7.<br>
         * -- param target    A certain target.<br>
         * -- return  The numbers of actions that are running in a certain target.<br>
         * -- js NA
         * @method getNumberOfRunningActionsInTarget
         * @param {cc.Node} _target
         * @return {number}
         */
        public getNumberOfRunningActionsInTarget (
            _target : cc.Node 
        ) : number;

        /**
         *  Removes all actions from a certain target.<br>
         * -- All the actions that belongs to the target will be removed.<br>
         * -- param target    A certain target.
         * @method removeAllActionsFromTarget
         * @param {cc.Node} _target
         */
        public removeAllActionsFromTarget (
            _target : cc.Node 
        ) : void;

        /**
         *  Resume a set of targets (convenience function to reverse a pauseAllRunningActions call).<br>
         * -- param targetsToResume   A set of targets need to be resumed.
         * @method resumeTargets
         * @param {cc.Node[]} _targetsToResume
         */
        public resumeTargets (
            _targetsToResume : cc.Node[] 
        ) : void;

        /**
         *  Removes an action given an action reference.<br>
         * -- param action    A certain target.
         * @method removeAction
         * @param {cc.Action} _action
         */
        public removeAction (
            _action : cc.Action 
        ) : void;

        /**
         *  Removes all actions given its tag and the target.<br>
         * -- param tag       The actions' tag.<br>
         * -- param target    A certain target.<br>
         * -- js NA
         * @method removeAllActionsByTag
         * @param {number} _tag
         * @param {cc.Node} _target
         */
        public removeAllActionsByTag (
            _tag : number, 
            _target : cc.Node 
        ) : void;

        /**
         *  Pauses all running actions, returning a list of targets whose actions were paused.<br>
         * -- return  A list of targets whose actions were paused.
         * @method pauseAllRunningActions
         * @return {cc.Node[]}
         */
        public pauseAllRunningActions (
        ) : cc.Node[];

        /**
         * js ctor
         * @method ActionManager
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class PageTurn3D
     * @native
     */
    export class PageTurn3D 
        extends cc.Grid3DAction
    {

        /**
         * brief Create an action with duration, grid size.<br>
         * -- param duration Specify the duration of the PageTurn3D action. It's a value in seconds.<br>
         * -- param gridSize Specify the size of the grid.<br>
         * -- return If the creation success, return a pointer of PageTurn3D action; otherwise, return nil.
         * @method create
         * @param {number} _duration
         * @param {ctype.value_type<cc.Size>} _gridSize
         * @return {cc.PageTurn3D}
         */
        public static create (
            _duration : number, 
            _gridSize : ctype.value_type<cc.Size> 
        ) : cc.PageTurn3D;

    }
    /**
     * @class ProgressTo
     * @native
     */
    export class ProgressTo 
        extends cc.ActionInterval
    {

        /**
         * brief Initializes with a duration and destination percentage. <br>
         * -- param duration Specify the duration of the ProgressTo action. It's a value in seconds.<br>
         * -- param percent Specify the destination percentage.<br>
         * -- return If the creation success, return true; otherwise, return false.
         * @method initWithDuration
         * @param {number} _duration
         * @param {number} _percent
         * @return {boolean}
         */
        public initWithDuration (
            _duration : number, 
            _percent : number 
        ) : boolean;

        /**
         * brief Create and initializes with a duration and a destination percentage.<br>
         * -- param duration Specify the duration of the ProgressTo action. It's a value in seconds.<br>
         * -- param percent Specify the destination percentage.<br>
         * -- return If the creation success, return a pointer of ProgressTo action; otherwise, return nil.
         * @method create
         * @param {number} _duration
         * @param {number} _percent
         * @return {cc.ProgressTo}
         */
        public static create (
            _duration : number, 
            _percent : number 
        ) : cc.ProgressTo;

        /**
         * 
         * @method ProgressTo
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class ProgressFromTo
     * @native
     */
    export class ProgressFromTo 
        extends cc.ActionInterval
    {

        /**
         * brief Initializes the action with a duration, a "from" percentage and a "to" percentage.<br>
         * -- param duration Specify the duration of the ProgressFromTo action. It's a value in seconds.<br>
         * -- param fromPercentage Specify the source percentage.<br>
         * -- param toPercentage Specify the destination percentage.<br>
         * -- return If the creation success, return true; otherwise, return false.
         * @method initWithDuration
         * @param {number} _duration
         * @param {number} _fromPercentage
         * @param {number} _toPercentage
         * @return {boolean}
         */
        public initWithDuration (
            _duration : number, 
            _fromPercentage : number, 
            _toPercentage : number 
        ) : boolean;

        /**
         * brief Create and initializes the action with a duration, a "from" percentage and a "to" percentage.<br>
         * -- param duration Specify the duration of the ProgressFromTo action. It's a value in seconds.<br>
         * -- param fromPercentage Specify the source percentage.<br>
         * -- param toPercentage Specify the destination percentage.<br>
         * -- return If the creation success, return a pointer of ProgressFromTo action; otherwise, return nil.
         * @method create
         * @param {number} _duration
         * @param {number} _fromPercentage
         * @param {number} _toPercentage
         * @return {cc.ProgressFromTo}
         */
        public static create (
            _duration : number, 
            _fromPercentage : number, 
            _toPercentage : number 
        ) : cc.ProgressFromTo;

        /**
         * 
         * @method ProgressFromTo
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class ShakyTiles3D
     * @native
     */
    export class ShakyTiles3D 
        extends cc.TiledGrid3DAction
    {

        /**
         * brief Initializes the action with a range, shake Z vertices, grid size and duration.<br>
         * -- param duration Specify the duration of the ShakyTiles3D action. It's a value in seconds.<br>
         * -- param gridSize Specify the size of the grid.<br>
         * -- param range Specify the range of the shaky effect.<br>
         * -- param shakeZ Specify whether shake on the z axis.<br>
         * -- return If the Initialization success, return true; otherwise, return false.
         * @method initWithDuration
         * @param {number} _duration
         * @param {ctype.value_type<cc.Size>} _gridSize
         * @param {number} _range
         * @param {boolean} _shakeZ
         * @return {boolean}
         */
        public initWithDuration (
            _duration : number, 
            _gridSize : ctype.value_type<cc.Size>, 
            _range : number, 
            _shakeZ : boolean 
        ) : boolean;

        /**
         * brief Create the action with a range, shake Z vertices, a grid and duration.<br>
         * -- param duration Specify the duration of the ShakyTiles3D action. It's a value in seconds.<br>
         * -- param gridSize Specify the size of the grid.<br>
         * -- param range Specify the range of the shaky effect.<br>
         * -- param shakeZ Specify whether shake on the z axis.<br>
         * -- return If the creation success, return a pointer of ShakyTiles3D action; otherwise, return nil.
         * @method create
         * @param {number} _duration
         * @param {ctype.value_type<cc.Size>} _gridSize
         * @param {number} _range
         * @param {boolean} _shakeZ
         * @return {cc.ShakyTiles3D}
         */
        public static create (
            _duration : number, 
            _gridSize : ctype.value_type<cc.Size>, 
            _range : number, 
            _shakeZ : boolean 
        ) : cc.ShakyTiles3D;

        /**
         * 
         * @method ShakyTiles3D
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class ShatteredTiles3D
     * @native
     */
    export class ShatteredTiles3D 
        extends cc.TiledGrid3DAction
    {

        /**
         * brief Initializes the action with a range, shatter Z vertices, grid size and duration.<br>
         * -- param duration Specify the duration of the ShatteredTiles3D action. It's a value in seconds.<br>
         * -- param gridSize Specify the size of the grid.<br>
         * -- param range Specify the range of the shatter effect.<br>
         * -- param shatterZ Specify whether shake on the z axis.<br>
         * -- return If the Initialization success, return true; otherwise, return false.
         * @method initWithDuration
         * @param {number} _duration
         * @param {ctype.value_type<cc.Size>} _gridSize
         * @param {number} _range
         * @param {boolean} _shatterZ
         * @return {boolean}
         */
        public initWithDuration (
            _duration : number, 
            _gridSize : ctype.value_type<cc.Size>, 
            _range : number, 
            _shatterZ : boolean 
        ) : boolean;

        /**
         * brief Create the action with a range, whether of not to shatter Z vertices, grid size and duration.<br>
         * -- param duration Specify the duration of the ShatteredTiles3D action. It's a value in seconds.<br>
         * -- param gridSize Specify the size of the grid.<br>
         * -- param range Specify the range of the shatter effect.<br>
         * -- param shatterZ Specify whether shatter on the z axis.<br>
         * -- return If the creation success, return a pointer of ShatteredTiles3D action; otherwise, return nil.
         * @method create
         * @param {number} _duration
         * @param {ctype.value_type<cc.Size>} _gridSize
         * @param {number} _range
         * @param {boolean} _shatterZ
         * @return {cc.ShatteredTiles3D}
         */
        public static create (
            _duration : number, 
            _gridSize : ctype.value_type<cc.Size>, 
            _range : number, 
            _shatterZ : boolean 
        ) : cc.ShatteredTiles3D;

        /**
         * 
         * @method ShatteredTiles3D
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class ShuffleTiles
     * @native
     */
    export class ShuffleTiles 
        extends cc.TiledGrid3DAction
    {

        /**
         * 
         * @method placeTile
         * @param {ctype.value_type<cc.Point>} _pos
         * @param {cc.Tile} _t
         */
        public placeTile (
            _pos : ctype.value_type<cc.Point>, 
            _t : cc.Tile 
        ) : void;

        /**
         * 
         * @method shuffle
         * @param {number} _array
         * @param {number} _len
         */
        public shuffle (
            _array : number, 
            _len : number 
        ) : void;

        /**
         * brief Initializes the action with grid size, random seed and duration.<br>
         * -- param duration Specify the duration of the ShuffleTiles action. It's a value in seconds.<br>
         * -- param gridSize Specify the size of the grid.<br>
         * -- param seed Specify the random seed.<br>
         * -- return If the Initialization success, return true; otherwise, return false.
         * @method initWithDuration
         * @param {number} _duration
         * @param {ctype.value_type<cc.Size>} _gridSize
         * @param {number} _seed
         * @return {boolean}
         */
        public initWithDuration (
            _duration : number, 
            _gridSize : ctype.value_type<cc.Size>, 
            _seed : number 
        ) : boolean;

        /**
         * 
         * @method getDelta
         * @param {ctype.value_type<cc.Size>} _pos
         * @return {ctype.value_type<cc.Size>}
         */
        public getDelta (
            _pos : ctype.value_type<cc.Size> 
        ) : ctype.value_type<cc.Size>;

        /**
         * brief Create the action with grid size, random seed and duration.<br>
         * -- param duration Specify the duration of the ShuffleTiles action. It's a value in seconds.<br>
         * -- param gridSize Specify the size of the grid.<br>
         * -- param seed Specify the random seed.<br>
         * -- return If the creation success, return a pointer of ShuffleTiles action; otherwise, return nil.
         * @method create
         * @param {number} _duration
         * @param {ctype.value_type<cc.Size>} _gridSize
         * @param {number} _seed
         * @return {cc.ShuffleTiles}
         */
        public static create (
            _duration : number, 
            _gridSize : ctype.value_type<cc.Size>, 
            _seed : number 
        ) : cc.ShuffleTiles;

        /**
         * 
         * @method ShuffleTiles
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class FadeOutTRTiles
     * @native
     */
    export class FadeOutTRTiles 
        extends cc.TiledGrid3DAction
    {

        /**
         * brief Show the tile at specified position.<br>
         * -- param pos The position index of the tile should be shown.
         * @method turnOnTile
         * @param {ctype.value_type<cc.Point>} _pos
         */
        public turnOnTile (
            _pos : ctype.value_type<cc.Point> 
        ) : void;

        /**
         * brief Hide the tile at specified position.<br>
         * -- param pos The position index of the tile should be hide.
         * @method turnOffTile
         * @param {ctype.value_type<cc.Point>} _pos
         */
        public turnOffTile (
            _pos : ctype.value_type<cc.Point> 
        ) : void;

        /**
         * brief Show part of the tile.<br>
         * -- param pos The position index of the tile should be shown.<br>
         * -- param distance The percentage that the tile should be shown.
         * @method transformTile
         * @param {ctype.value_type<cc.Point>} _pos
         * @param {number} _distance
         */
        public transformTile (
            _pos : ctype.value_type<cc.Point>, 
            _distance : number 
        ) : void;

        /**
         * brief Calculate the percentage a tile should be shown.<br>
         * -- param pos The position index of the tile.<br>
         * -- param time The current percentage of the action.<br>
         * -- return Return the percentage the tile should be shown.
         * @method testFunc
         * @param {ctype.value_type<cc.Size>} _pos
         * @param {number} _time
         * @return {number}
         */
        public testFunc (
            _pos : ctype.value_type<cc.Size>, 
            _time : number 
        ) : number;

        /**
         * brief Create the action with the grid size and the duration.<br>
         * -- param duration Specify the duration of the FadeOutTRTiles action. It's a value in seconds.<br>
         * -- param gridSize Specify the size of the grid.<br>
         * -- return If the creation success, return a pointer of FadeOutTRTiles action; otherwise, return nil.
         * @method create
         * @param {number} _duration
         * @param {ctype.value_type<cc.Size>} _gridSize
         * @return {cc.FadeOutTRTiles}
         */
        public static create (
            _duration : number, 
            _gridSize : ctype.value_type<cc.Size> 
        ) : cc.FadeOutTRTiles;

        /**
         * 
         * @method FadeOutTRTiles
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class FadeOutBLTiles
     * @native
     */
    export class FadeOutBLTiles 
        extends cc.FadeOutTRTiles
    {

        /**
         * brief Create the action with the grid size and the duration.<br>
         * -- param duration Specify the duration of the FadeOutBLTiles action. It's a value in seconds.<br>
         * -- param gridSize Specify the size of the grid.<br>
         * -- return If the creation success, return a pointer of FadeOutBLTiles action; otherwise, return nil.
         * @method create
         * @param {number} _duration
         * @param {ctype.value_type<cc.Size>} _gridSize
         * @return {cc.FadeOutBLTiles}
         */
        public static create (
            _duration : number, 
            _gridSize : ctype.value_type<cc.Size> 
        ) : cc.FadeOutBLTiles;

        /**
         * 
         * @method FadeOutBLTiles
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class FadeOutUpTiles
     * @native
     */
    export class FadeOutUpTiles 
        extends cc.FadeOutTRTiles
    {

        /**
         * brief Create the action with the grid size and the duration.<br>
         * -- param duration Specify the duration of the FadeOutUpTiles action. It's a value in seconds.<br>
         * -- param gridSize Specify the size of the grid.<br>
         * -- return If the creation success, return a pointer of FadeOutUpTiles action; otherwise, return nil.
         * @method create
         * @param {number} _duration
         * @param {ctype.value_type<cc.Size>} _gridSize
         * @return {cc.FadeOutUpTiles}
         */
        public static create (
            _duration : number, 
            _gridSize : ctype.value_type<cc.Size> 
        ) : cc.FadeOutUpTiles;

        /**
         * 
         * @method FadeOutUpTiles
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class FadeOutDownTiles
     * @native
     */
    export class FadeOutDownTiles 
        extends cc.FadeOutUpTiles
    {

        /**
         * brief Create the action with the grid size and the duration.<br>
         * -- param duration Specify the duration of the FadeOutDownTiles action. It's a value in seconds.<br>
         * -- param gridSize Specify the size of the grid.<br>
         * -- return If the creation success, return a pointer of FadeOutDownTiles action; otherwise, return nil.
         * @method create
         * @param {number} _duration
         * @param {ctype.value_type<cc.Size>} _gridSize
         * @return {cc.FadeOutDownTiles}
         */
        public static create (
            _duration : number, 
            _gridSize : ctype.value_type<cc.Size> 
        ) : cc.FadeOutDownTiles;

        /**
         * 
         * @method FadeOutDownTiles
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class TurnOffTiles
     * @native
     */
    export class TurnOffTiles 
        extends cc.TiledGrid3DAction
    {

        /**
         * brief Show the tile at specified position.<br>
         * -- param pos The position index of the tile should be shown.
         * @method turnOnTile
         * @param {ctype.value_type<cc.Point>} _pos
         */
        public turnOnTile (
            _pos : ctype.value_type<cc.Point> 
        ) : void;

        /**
         * brief Hide the tile at specified position.<br>
         * -- param pos The position index of the tile should be hide.
         * @method turnOffTile
         * @param {ctype.value_type<cc.Point>} _pos
         */
        public turnOffTile (
            _pos : ctype.value_type<cc.Point> 
        ) : void;

        /**
         * brief Shuffle the array specified.<br>
         * -- param array The array will be shuffled.<br>
         * -- param len The size of the array.
         * @method shuffle
         * @param {number} _array
         * @param {number} _len
         */
        public shuffle (
            _array : number, 
            _len : number 
        ) : void;

        /**
         * brief Initializes the action with grid size, random seed and duration.<br>
         * -- param duration Specify the duration of the TurnOffTiles action. It's a value in seconds.<br>
         * -- param gridSize Specify the size of the grid.<br>
         * -- param seed Specify the random seed.<br>
         * -- return If the Initialization success, return true; otherwise, return false.
         * @method initWithDuration
         * @param {number} _duration
         * @param {ctype.value_type<cc.Size>} _gridSize
         * @param {number} _seed
         * @return {boolean}
         */
        public initWithDuration (
            _duration : number, 
            _gridSize : ctype.value_type<cc.Size>, 
            _seed : number 
        ) : boolean;

        /**
         * brief Create the action with the grid size and the duration.<br>
         * -- param duration Specify the duration of the TurnOffTiles action. It's a value in seconds.<br>
         * -- param gridSize Specify the size of the grid.<br>
         * -- param seed Specify the random seed.<br>
         * -- return If the creation success, return a pointer of TurnOffTiles action; otherwise, return nil.
         * @method create
         * @param {(number)} _duration
         * @param {(ctype.value_type<cc.Size>)} _gridSize
         * @param {(number)} _seed?
         * @return {cc.TurnOffTiles}
         */
        public static create (
            _duration : (number), 
            _gridSize : (ctype.value_type<cc.Size>), 
            _seed? : (number)
        ) : cc.TurnOffTiles;

        /**
         * 
         * @method TurnOffTiles
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class WavesTiles3D
     * @native
     */
    export class WavesTiles3D 
        extends cc.TiledGrid3DAction
    {

        /**
         * brief Set the amplitude rate of the effect.<br>
         * -- param amplitudeRate The value of amplitude rate will be set.
         * @method setAmplitudeRate
         * @param {number} _amplitudeRate
         */
        public setAmplitudeRate (
            _amplitudeRate : number 
        ) : void;

        /**
         * brief Initializes an action with duration, grid size, waves count and amplitude.<br>
         * -- param duration Specify the duration of the WavesTiles3D action. It's a value in seconds.<br>
         * -- param gridSize Specify the size of the grid.<br>
         * -- param waves Specify the waves count of the WavesTiles3D action.<br>
         * -- param amplitude Specify the amplitude of the WavesTiles3D action.<br>
         * -- return If the initialization success, return true; otherwise, return false.
         * @method initWithDuration
         * @param {number} _duration
         * @param {ctype.value_type<cc.Size>} _gridSize
         * @param {number} _waves
         * @param {number} _amplitude
         * @return {boolean}
         */
        public initWithDuration (
            _duration : number, 
            _gridSize : ctype.value_type<cc.Size>, 
            _waves : number, 
            _amplitude : number 
        ) : boolean;

        /**
         * brief Get the amplitude of the effect.<br>
         * -- return Return the amplitude of the effect.
         * @method getAmplitude
         * @return {number}
         */
        public getAmplitude (
        ) : number;

        /**
         * brief Get the amplitude rate of the effect.<br>
         * -- return Return the amplitude rate of the effect.
         * @method getAmplitudeRate
         * @return {number}
         */
        public getAmplitudeRate (
        ) : number;

        /**
         * brief Set the amplitude to the effect.<br>
         * -- param amplitude The value of amplitude will be set.
         * @method setAmplitude
         * @param {number} _amplitude
         */
        public setAmplitude (
            _amplitude : number 
        ) : void;

        /**
         * brief Create the action with a number of waves, the waves amplitude, the grid size and the duration.<br>
         * -- param duration Specify the duration of the WavesTiles3D action. It's a value in seconds.<br>
         * -- param gridSize Specify the size of the grid.<br>
         * -- param waves Specify the waves count of the WavesTiles3D action.<br>
         * -- param amplitude Specify the amplitude of the WavesTiles3D action.<br>
         * -- return If the creation success, return a pointer of WavesTiles3D action; otherwise, return nil.
         * @method create
         * @param {number} _duration
         * @param {ctype.value_type<cc.Size>} _gridSize
         * @param {number} _waves
         * @param {number} _amplitude
         * @return {cc.WavesTiles3D}
         */
        public static create (
            _duration : number, 
            _gridSize : ctype.value_type<cc.Size>, 
            _waves : number, 
            _amplitude : number 
        ) : cc.WavesTiles3D;

        /**
         * 
         * @method WavesTiles3D
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class JumpTiles3D
     * @native
     */
    export class JumpTiles3D 
        extends cc.TiledGrid3DAction
    {

        /**
         * brief Set the amplitude rate of the effect.<br>
         * -- param amplitudeRate The value of amplitude rate will be set.
         * @method setAmplitudeRate
         * @param {number} _amplitudeRate
         */
        public setAmplitudeRate (
            _amplitudeRate : number 
        ) : void;

        /**
         * brief Initializes the action with the number of jumps, the sin amplitude, the grid size and the duration.<br>
         * -- param duration Specify the duration of the JumpTiles3D action. It's a value in seconds.<br>
         * -- param gridSize Specify the size of the grid.<br>
         * -- param numberOfJumps Specify the jump tiles count.<br>
         * -- param amplitude Specify the amplitude of the JumpTiles3D action.<br>
         * -- return If the initialization success, return true; otherwise, return false.
         * @method initWithDuration
         * @param {number} _duration
         * @param {ctype.value_type<cc.Size>} _gridSize
         * @param {number} _numberOfJumps
         * @param {number} _amplitude
         * @return {boolean}
         */
        public initWithDuration (
            _duration : number, 
            _gridSize : ctype.value_type<cc.Size>, 
            _numberOfJumps : number, 
            _amplitude : number 
        ) : boolean;

        /**
         * brief Get the amplitude of the effect.<br>
         * -- return Return the amplitude of the effect.
         * @method getAmplitude
         * @return {number}
         */
        public getAmplitude (
        ) : number;

        /**
         * brief Get the amplitude rate of the effect.<br>
         * -- return Return the amplitude rate of the effect.
         * @method getAmplitudeRate
         * @return {number}
         */
        public getAmplitudeRate (
        ) : number;

        /**
         * brief Set the amplitude to the effect.<br>
         * -- param amplitude The value of amplitude will be set.
         * @method setAmplitude
         * @param {number} _amplitude
         */
        public setAmplitude (
            _amplitude : number 
        ) : void;

        /**
         * brief Create the action with the number of jumps, the sin amplitude, the grid size and the duration.<br>
         * -- param duration Specify the duration of the JumpTiles3D action. It's a value in seconds.<br>
         * -- param gridSize Specify the size of the grid.<br>
         * -- param numberOfJumps Specify the jump tiles count.<br>
         * -- param amplitude Specify the amplitude of the JumpTiles3D action.<br>
         * -- return If the creation success, return a pointer of JumpTiles3D action; otherwise, return nil.
         * @method create
         * @param {number} _duration
         * @param {ctype.value_type<cc.Size>} _gridSize
         * @param {number} _numberOfJumps
         * @param {number} _amplitude
         * @return {cc.JumpTiles3D}
         */
        public static create (
            _duration : number, 
            _gridSize : ctype.value_type<cc.Size>, 
            _numberOfJumps : number, 
            _amplitude : number 
        ) : cc.JumpTiles3D;

        /**
         * 
         * @method JumpTiles3D
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class SplitRows
     * @native
     */
    export class SplitRows 
        extends cc.TiledGrid3DAction
    {

        /**
         * brief Initializes the action with the number rows and the duration.<br>
         * -- param duration Specify the duration of the SplitRows action. It's a value in seconds.<br>
         * -- param rows Specify the rows count should be split.<br>
         * -- return If the creation success, return true; otherwise, return false.
         * @method initWithDuration
         * @param {number} _duration
         * @param {number} _rows
         * @return {boolean}
         */
        public initWithDuration (
            _duration : number, 
            _rows : number 
        ) : boolean;

        /**
         * brief Create the action with the number of rows and the duration.<br>
         * -- param duration Specify the duration of the SplitRows action. It's a value in seconds.<br>
         * -- param rows Specify the rows count should be split.<br>
         * -- return If the creation success, return a pointer of SplitRows action; otherwise, return nil.
         * @method create
         * @param {number} _duration
         * @param {number} _rows
         * @return {cc.SplitRows}
         */
        public static create (
            _duration : number, 
            _rows : number 
        ) : cc.SplitRows;

        /**
         * 
         * @method SplitRows
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class SplitCols
     * @native
     */
    export class SplitCols 
        extends cc.TiledGrid3DAction
    {

        /**
         * brief Initializes the action with the number columns and the duration.<br>
         * -- param duration Specify the duration of the SplitCols action. It's a value in seconds.<br>
         * -- param cols Specify the columns count should be split.<br>
         * -- return If the creation success, return true; otherwise, return false.
         * @method initWithDuration
         * @param {number} _duration
         * @param {number} _cols
         * @return {boolean}
         */
        public initWithDuration (
            _duration : number, 
            _cols : number 
        ) : boolean;

        /**
         * brief Create the action with the number of columns and the duration.<br>
         * -- param duration Specify the duration of the SplitCols action. It's a value in seconds.<br>
         * -- param cols Specify the columns count should be split.<br>
         * -- return If the creation success, return a pointer of SplitCols action; otherwise, return nil.
         * @method create
         * @param {number} _duration
         * @param {number} _cols
         * @return {cc.SplitCols}
         */
        public static create (
            _duration : number, 
            _cols : number 
        ) : cc.SplitCols;

        /**
         * 
         * @method SplitCols
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class ActionTween
     * @native
     */
    export class ActionTween 
        extends cc.ActionInterval
    {

        /**
         * brief Initializes the action with the property name (key), and the from and to parameters.<br>
         * -- param duration The duration of the ActionTween. It's a value in seconds.<br>
         * -- param key The key of property which should be updated.<br>
         * -- param from The value of the specified property when the action begin.<br>
         * -- param to The value of the specified property when the action end.<br>
         * -- return If the initialization success, return true; otherwise, return false.
         * @method initWithDuration
         * @param {number} _duration
         * @param {string} _key
         * @param {number} _from
         * @param {number} _to
         * @return {boolean}
         */
        public initWithDuration (
            _duration : number, 
            _key : string, 
            _from : number, 
            _to : number 
        ) : boolean;

        /**
         * brief Create and initializes the action with the property name (key), and the from and to parameters.<br>
         * -- param duration The duration of the ActionTween. It's a value in seconds.<br>
         * -- param key The key of property which should be updated.<br>
         * -- param from The value of the specified property when the action begin.<br>
         * -- param to The value of the specified property when the action end.<br>
         * -- return If the creation success, return a pointer of ActionTween; otherwise, return nil.
         * @method create
         * @param {number} _duration
         * @param {string} _key
         * @param {number} _from
         * @param {number} _to
         * @return {cc.ActionTween}
         */
        public static create (
            _duration : number, 
            _key : string, 
            _from : number, 
            _to : number 
        ) : cc.ActionTween;

    }
    /**
     * @class AtlasNode
     * @native
     */
    export class AtlasNode 
        extends cc.Node
    {
        /* Can't find native property getOpacity implementation of cc::AtlasNode. using any type */
        public opacity : any;
        /* Can't find native property isOpacityModifyRGB implementation of cc::AtlasNode. using any type */
        public opacityModifyRGB : any;
        /* Can't find native property getColor implementation of cc::AtlasNode. using any type */
        public color : any;
        /**
         * 
         * @method getTexture
         * @return {cc.Texture2D}
         */
        public texture : cc.Texture2D;
        /**
         *  Return the buffer manager of the texture vertex. 
         * return Return A TextureAtlas.
         * @method getTextureAtlas
         * @return {cc.TextureAtlas}
         */
        public textureAtlas : cc.TextureAtlas;
        /**
         * 
         * @method getQuadsToDraw
         * @return {number}
         */
        public quadsToDraw : number;


        /**
         *  updates the Atlas (indexed vertex array).<br>
         * -- Shall be overridden in subclasses.
         * @method updateAtlasValues
         */
        public updateAtlasValues (
        ) : void;

        /**
         *  Initializes an AtlasNode  with an Atlas file the width and height of each item and the quantity of items to render
         * @method initWithTileFile
         * @param {string} _tile
         * @param {number} _tileWidth
         * @param {number} _tileHeight
         * @param {number} _itemsToRender
         * @return {boolean}
         */
        public initWithTileFile (
            _tile : string, 
            _tileWidth : number, 
            _tileHeight : number, 
            _itemsToRender : number 
        ) : boolean;

        /**
         * 
         * @method getTexture
         * @return {cc.Texture2D}
         */
        public getTexture (
        ) : cc.Texture2D;

        /**
         *  Set an buffer manager of the texture vertex. 
         * @method setTextureAtlas
         * @param {cc.TextureAtlas} _textureAtlas
         */
        public setTextureAtlas (
            _textureAtlas : cc.TextureAtlas 
        ) : void;

        /**
         * code<br>
         * -- When this function bound into js or lua,the parameter will be changed<br>
         * -- In js: var setBlendFunc(var src, var dst)<br>
         * -- endcode<br>
         * -- lua NA
         * @method setBlendFunc
         * @param {cc.BlendFunc} _blendFunc
         */
        public setBlendFunc (
            _blendFunc : cc.BlendFunc 
        ) : void;

        /**
         *  Return the buffer manager of the texture vertex. <br>
         * -- return Return A TextureAtlas.
         * @method getTextureAtlas
         * @return {cc.TextureAtlas}
         */
        public getTextureAtlas (
        ) : cc.TextureAtlas;

        /**
         * lua NA
         * @method getBlendFunc
         * @return {cc.BlendFunc}
         */
        public getBlendFunc (
        ) : cc.BlendFunc;

        /**
         * 
         * @method getQuadsToDraw
         * @return {number}
         */
        public getQuadsToDraw (
        ) : number;

        /**
         * 
         * @method setTexture
         * @param {cc.Texture2D} _texture
         */
        public setTexture (
            _texture : cc.Texture2D 
        ) : void;

        /**
         *  Initializes an AtlasNode  with a texture the width and height of each item measured in points and the quantity of items to render
         * @method initWithTexture
         * @param {cc.Texture2D} _texture
         * @param {number} _tileWidth
         * @param {number} _tileHeight
         * @param {number} _itemsToRender
         * @return {boolean}
         */
        public initWithTexture (
            _texture : cc.Texture2D, 
            _tileWidth : number, 
            _tileHeight : number, 
            _itemsToRender : number 
        ) : boolean;

        /**
         * 
         * @method setQuadsToDraw
         * @param {number} _quadsToDraw
         */
        public setQuadsToDraw (
            _quadsToDraw : number 
        ) : void;

        /**
         *  creates a AtlasNode  with an Atlas file the width and height of each item and the quantity of items to render.<br>
         * -- param filename The path of Atlas file.<br>
         * -- param tileWidth The width of the item.<br>
         * -- param tileHeight The height of the item.<br>
         * -- param itemsToRender The quantity of items to render.
         * @method create
         * @param {string} _filename
         * @param {number} _tileWidth
         * @param {number} _tileHeight
         * @param {number} _itemsToRender
         * @return {cc.AtlasNode}
         */
        public static create (
            _filename : string, 
            _tileWidth : number, 
            _tileHeight : number, 
            _itemsToRender : number 
        ) : cc.AtlasNode;

        /**
         * 
         * @method AtlasNode
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class ClippingNode
     * @native
     */
    export class ClippingNode 
        extends cc.Node
    {
        /**
         *  The alpha threshold.
         * The content is drawn only where the stencil have pixel with alpha greater than the alphaThreshold.
         * Should be a float between 0 and 1.
         * This default to 1 (so alpha test is disabled).
         * return The alpha threshold value,Should be a float between 0 and 1.
         * @method getAlphaThreshold
         * @return {number}
         */
        public alphaThreshold : number;
        /* Can't find native property getInverted implementation of cc::ClippingNode. using any type */
        public inverted : any;
        /**
         *  The Node to use as a stencil to do the clipping.
         * The stencil node will be retained.
         * This default to nil.
         * return The stencil node.
         * @method getStencil
         * @return {cc.Node}
         */
        public stencil : cc.Node;


        /**
         *  If stencil has no children it will not be drawn.<br>
         * -- If you have custom stencil-based node with stencil drawing mechanics other then children-based,<br>
         * -- then this method should return true every time you wish stencil to be visited.<br>
         * -- By default returns true if has any children attached.<br>
         * -- return If you have custom stencil-based node with stencil drawing mechanics other then children-based,<br>
         * -- then this method should return true every time you wish stencil to be visited.<br>
         * -- By default returns true if has any children attached.<br>
         * -- js NA
         * @method hasContent
         * @return {boolean}
         */
        public hasContent (
        ) : boolean;

        /**
         *  Set the ClippingNode whether or not invert.<br>
         * -- param inverted A bool Type,to set the ClippingNode whether or not invert.
         * @method setInverted
         * @param {boolean} _inverted
         */
        public setInverted (
            _inverted : boolean 
        ) : void;

        /**
         *  Set the Node to use as a stencil to do the clipping.<br>
         * -- param stencil The Node to use as a stencil to do the clipping.
         * @method setStencil
         * @param {cc.Node} _stencil
         */
        public setStencil (
            _stencil : cc.Node 
        ) : void;

        /**
         *  The alpha threshold.<br>
         * -- The content is drawn only where the stencil have pixel with alpha greater than the alphaThreshold.<br>
         * -- Should be a float between 0 and 1.<br>
         * -- This default to 1 (so alpha test is disabled).<br>
         * -- return The alpha threshold value,Should be a float between 0 and 1.
         * @method getAlphaThreshold
         * @return {number}
         */
        public getAlphaThreshold (
        ) : number;

        /**
         *  The Node to use as a stencil to do the clipping.<br>
         * -- The stencil node will be retained.<br>
         * -- This default to nil.<br>
         * -- return The stencil node.
         * @method getStencil
         * @return {cc.Node}
         */
        public getStencil (
        ) : cc.Node;

        /**
         *  Set the alpha threshold. <br>
         * -- param alphaThreshold The alpha threshold.
         * @method setAlphaThreshold
         * @param {number} _alphaThreshold
         */
        public setAlphaThreshold (
            _alphaThreshold : number 
        ) : void;

        /**
         *  Inverted. If this is set to true,<br>
         * -- the stencil is inverted, so the content is drawn where the stencil is NOT drawn.<br>
         * -- This default to false.<br>
         * -- return If the clippingNode is Inverted, it will be return true.
         * @method isInverted
         * @return {boolean}
         */
        public isInverted (
        ) : boolean;

        /**
         *  Creates and initializes a clipping node with an other node as its stencil.<br>
         * -- The stencil node will be retained.<br>
         * -- param stencil The stencil node.
         * @method create
         * @param {(cc.Node)} _stencil?
         * @return {cc.ClippingNode}
         */
        public static create (
            _stencil? : (cc.Node)
        ) : cc.ClippingNode;

        /**
         * 
         * @method ClippingNode
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class DrawNode
     * @native
     */
    export class DrawNode 
        extends cc.Node
    {
        // This function manually added.
        // see cocos2d-x/cocos/scripting/js-bindings/script/jsb_cocos2d.js
        public drawPoly(verts : ctype.value_type<cc.Point[]>, fillColor : ctype.value_type<cc.Color>, borderWidth : number, borderColor? : ctype.value_type<cc.Color>) : void;

        

        /**
         *  Draw an line from origin to destination with color. <br>
         * -- param origin The line origin.<br>
         * -- param destination The line destination.<br>
         * -- param color The line color.<br>
         * -- js NA
         * @method drawLine
         * @param {ctype.value_type<cc.Point>} _origin
         * @param {ctype.value_type<cc.Point>} _destination
         * @param {ctype.value_type<cc.Color>} _color
         */
        public drawLine (
            _origin : ctype.value_type<cc.Point>, 
            _destination : ctype.value_type<cc.Point>, 
            _color : ctype.value_type<cc.Color> 
        ) : void;

        /**
         *  Draw a group point.<br>
         * -- param position A Vec2 pointer.<br>
         * -- param numberOfPoints The number of points.<br>
         * -- param pointSize The point size.<br>
         * -- param color The point color.<br>
         * -- js NA
         * @method drawPoints
         * @param {(ctype.value_type<cc.Point[]>)} _position
         * @param {(number)} _numberOfPoints
         * @param {(number) | (ctype.value_type<cc.Color>)} _pointSize | color
         * @param {(ctype.value_type<cc.Color>)} _color?
         */
        public drawPoints (
            _position : (ctype.value_type<cc.Point[]>), 
            _numberOfPoints : (number), 
            _pointSize_color : (number) | (ctype.value_type<cc.Color>), 
            _color? : (ctype.value_type<cc.Color>)
        ) : void;

        /**
         *  Draws a rectangle with 4 points.<br>
         * -- param p1 The rectangle vertex point.<br>
         * -- param p2 The rectangle vertex point.<br>
         * -- param p3 The rectangle vertex point.<br>
         * -- param p4 The rectangle vertex point.<br>
         * -- param color The rectangle color.
         * @method drawRect
         * @param {(ctype.value_type<cc.Point>)} _p1 | origin
         * @param {(ctype.value_type<cc.Point>)} _p2 | destination
         * @param {(ctype.value_type<cc.Point>) | (ctype.value_type<cc.Color>)} _p3 | color
         * @param {(ctype.value_type<cc.Point>)} _p4?
         * @param {(ctype.value_type<cc.Color>)} _color?
         */
        public drawRect (
            _p1_origin : (ctype.value_type<cc.Point>), 
            _p2_destination : (ctype.value_type<cc.Point>), 
            _p3_color : (ctype.value_type<cc.Point>) | (ctype.value_type<cc.Color>), 
            _p4? : (ctype.value_type<cc.Point>), 
            _color? : (ctype.value_type<cc.Color>)
        ) : void;

        /**
         *  Draws a solid circle given the center, radius and number of segments.<br>
         * -- param center The circle center point.<br>
         * -- param radius The circle rotate of radius.<br>
         * -- param angle  The circle angle.<br>
         * -- param segments The number of segments.<br>
         * -- param color The solid circle color.<br>
         * -- js NA
         * @method drawSolidCircle
         * @param {(ctype.value_type<cc.Point>)} _center
         * @param {(number)} _radius
         * @param {(number)} _angle
         * @param {(number)} _segments
         * @param {(ctype.value_type<cc.Color>) | (number)} _color | scaleX
         * @param {(number)} _scaleY?
         * @param {(ctype.value_type<cc.Color>)} _color?
         */
        public drawSolidCircle (
            _center : (ctype.value_type<cc.Point>), 
            _radius : (number), 
            _angle : (number), 
            _segments : (number), 
            _color_scaleX : (ctype.value_type<cc.Color>) | (number), 
            _scaleY? : (number), 
            _color? : (ctype.value_type<cc.Color>)
        ) : void;

        /**
         * 
         * @method setLineWidth
         * @param {number} _lineWidth
         */
        public setLineWidth (
            _lineWidth : number 
        ) : void;

        /**
         * js NA
         * @method onDrawGLPoint
         * @param {ctype.value_type<cc.Mat4>} _transform
         * @param {number} _flags
         */
        public onDrawGLPoint (
            _transform : ctype.value_type<cc.Mat4>, 
            _flags : number 
        ) : void;

        /**
         *  draw a polygon with a fill color and line color<br>
         * -- code<br>
         * -- When this function bound into js or lua,the parameter will be changed<br>
         * -- In js: var drawPolygon(var Arrayofpoints, var fillColor, var width, var borderColor)<br>
         * -- In lua:local drawPolygon(local pointTable,local tableCount,local fillColor,local width,local borderColor)<br>
         * -- endcode<br>
         * -- param verts A pointer to point coordinates.<br>
         * -- param count The number of verts measured in points.<br>
         * -- param fillColor The color will fill in polygon.<br>
         * -- param borderWidth The border of line width.<br>
         * -- param borderColor The border of line color.<br>
         * -- js NA
         * @method drawPolygon
         * @param {ctype.value_type<cc.Point[]>} _verts
         * @param {number} _count
         * @param {ctype.value_type<cc.Color>} _fillColor
         * @param {number} _borderWidth
         * @param {ctype.value_type<cc.Color>} _borderColor
         */
        public drawPolygon (
            _verts : ctype.value_type<cc.Point[]>, 
            _count : number, 
            _fillColor : ctype.value_type<cc.Color>, 
            _borderWidth : number, 
            _borderColor : ctype.value_type<cc.Color> 
        ) : void;

        /**
         *  draw a dot at a position, with a given radius and color. <br>
         * -- param pos The dot center.<br>
         * -- param radius The dot radius.<br>
         * -- param color The dot color.
         * @method drawDot
         * @param {ctype.value_type<cc.Point>} _pos
         * @param {number} _radius
         * @param {ctype.value_type<cc.Color>} _color
         */
        public drawDot (
            _pos : ctype.value_type<cc.Point>, 
            _radius : number, 
            _color : ctype.value_type<cc.Color> 
        ) : void;

        /**
         *  Draws a Catmull Rom path.<br>
         * -- param points A point array  of control point.<br>
         * -- param segments The number of segments.<br>
         * -- param color The Catmull Rom color.
         * @method drawCatmullRom
         * @param {ctype.value_type<cc.Point>} _points
         * @param {number} _segments
         * @param {ctype.value_type<cc.Color>} _color
         */
        public drawCatmullRom (
            _points : ctype.value_type<cc.Point>, 
            _segments : number, 
            _color : ctype.value_type<cc.Color> 
        ) : void;

        /**
         *  draw a segment with a radius and color. <br>
         * -- param from The segment origin.<br>
         * -- param to The segment destination.<br>
         * -- param radius The segment radius.<br>
         * -- param color The segment color.
         * @method drawSegment
         * @param {ctype.value_type<cc.Point>} _from
         * @param {ctype.value_type<cc.Point>} _to
         * @param {number} _radius
         * @param {ctype.value_type<cc.Color>} _color
         */
        public drawSegment (
            _from : ctype.value_type<cc.Point>, 
            _to : ctype.value_type<cc.Point>, 
            _radius : number, 
            _color : ctype.value_type<cc.Color> 
        ) : void;

        /**
         *  Get the color mixed mode.<br>
         * -- lua NA
         * @method getBlendFunc
         * @return {cc.BlendFunc}
         */
        public getBlendFunc (
        ) : cc.BlendFunc;

        /**
         * js NA
         * @method onDraw
         * @param {ctype.value_type<cc.Mat4>} _transform
         * @param {number} _flags
         */
        public onDraw (
            _transform : ctype.value_type<cc.Mat4>, 
            _flags : number 
        ) : void;

        /**
         *  Draws a circle given the center, radius and number of segments.<br>
         * -- param center The circle center point.<br>
         * -- param radius The circle rotate of radius.<br>
         * -- param angle  The circle angle.<br>
         * -- param segments The number of segments.<br>
         * -- param drawLineToCenter Whether or not draw the line from the origin to center.<br>
         * -- param color Set the circle color.
         * @method drawCircle
         * @param {(ctype.value_type<cc.Point>)} _center
         * @param {(number)} _radius
         * @param {(number)} _angle
         * @param {(number)} _segments
         * @param {(boolean)} _drawLineToCenter
         * @param {(ctype.value_type<cc.Color>) | (number)} _color | scaleX
         * @param {(number)} _scaleY?
         * @param {(ctype.value_type<cc.Color>)} _color?
         */
        public drawCircle (
            _center : (ctype.value_type<cc.Point>), 
            _radius : (number), 
            _angle : (number), 
            _segments : (number), 
            _drawLineToCenter : (boolean), 
            _color_scaleX : (ctype.value_type<cc.Color>) | (number), 
            _scaleY? : (number), 
            _color? : (ctype.value_type<cc.Color>)
        ) : void;

        /**
         *  Draws a quad bezier path.<br>
         * -- param origin The origin of the bezier path.<br>
         * -- param control The control of the bezier path.<br>
         * -- param destination The destination of the bezier path.<br>
         * -- param segments The number of segments.<br>
         * -- param color Set the quad bezier color.
         * @method drawQuadBezier
         * @param {ctype.value_type<cc.Point>} _origin
         * @param {ctype.value_type<cc.Point>} _control
         * @param {ctype.value_type<cc.Point>} _destination
         * @param {number} _segments
         * @param {ctype.value_type<cc.Color>} _color
         */
        public drawQuadBezier (
            _origin : ctype.value_type<cc.Point>, 
            _control : ctype.value_type<cc.Point>, 
            _destination : ctype.value_type<cc.Point>, 
            _segments : number, 
            _color : ctype.value_type<cc.Color> 
        ) : void;

        /**
         * js NA
         * @method onDrawGLLine
         * @param {ctype.value_type<cc.Mat4>} _transform
         * @param {number} _flags
         */
        public onDrawGLLine (
            _transform : ctype.value_type<cc.Mat4>, 
            _flags : number 
        ) : void;

        /**
         *  Draws a solid polygon given a pointer to CGPoint coordinates, the number of vertices measured in points, and a color.<br>
         * -- param poli A solid polygon given a pointer to CGPoint coordinates.<br>
         * -- param numberOfPoints The number of vertices measured in points.<br>
         * -- param color The solid polygon color.<br>
         * -- js NA
         * @method drawSolidPoly
         * @param {ctype.value_type<cc.Point[]>} _poli
         * @param {number} _numberOfPoints
         * @param {ctype.value_type<cc.Color>} _color
         */
        public drawSolidPoly (
            _poli : ctype.value_type<cc.Point[]>, 
            _numberOfPoints : number, 
            _color : ctype.value_type<cc.Color> 
        ) : void;

        /**
         *  draw a triangle with color. <br>
         * -- param p1 The triangle vertex point.<br>
         * -- param p2 The triangle vertex point.<br>
         * -- param p3 The triangle vertex point.<br>
         * -- param color The triangle color.<br>
         * -- js NA
         * @method drawTriangle
         * @param {ctype.value_type<cc.Point>} _p1
         * @param {ctype.value_type<cc.Point>} _p2
         * @param {ctype.value_type<cc.Point>} _p3
         * @param {ctype.value_type<cc.Color>} _color
         */
        public drawTriangle (
            _p1 : ctype.value_type<cc.Point>, 
            _p2 : ctype.value_type<cc.Point>, 
            _p3 : ctype.value_type<cc.Point>, 
            _color : ctype.value_type<cc.Color> 
        ) : void;

        /**
         *  Set the color mixed mode.<br>
         * -- code<br>
         * -- When this function bound into js or lua,the parameter will be changed<br>
         * -- In js: var setBlendFunc(var src, var dst)<br>
         * -- endcode<br>
         * -- lua NA
         * @method setBlendFunc
         * @param {cc.BlendFunc} _blendFunc
         */
        public setBlendFunc (
            _blendFunc : cc.BlendFunc 
        ) : void;

        /**
         *  Clear the geometry in the node's buffer. 
         * @method clear
         */
        public clear (
        ) : void;

        /**
         *  Draws a Cardinal Spline path.<br>
         * -- param config A array point.<br>
         * -- param tension The tension of the spline.<br>
         * -- param segments The number of segments.<br>
         * -- param color Set the Spline color.
         * @method drawCardinalSpline
         * @param {ctype.value_type<cc.Point>} _config
         * @param {number} _tension
         * @param {number} _segments
         * @param {ctype.value_type<cc.Color>} _color
         */
        public drawCardinalSpline (
            _config : ctype.value_type<cc.Point>, 
            _tension : number, 
            _segments : number, 
            _color : ctype.value_type<cc.Color> 
        ) : void;

        /**
         *  Draws a solid rectangle given the origin and destination point measured in points.<br>
         * -- The origin and the destination can not have the same x and y coordinate.<br>
         * -- param origin The rectangle origin.<br>
         * -- param destination The rectangle destination.<br>
         * -- param color The rectangle color.<br>
         * -- js NA
         * @method drawSolidRect
         * @param {ctype.value_type<cc.Point>} _origin
         * @param {ctype.value_type<cc.Point>} _destination
         * @param {ctype.value_type<cc.Color>} _color
         */
        public drawSolidRect (
            _origin : ctype.value_type<cc.Point>, 
            _destination : ctype.value_type<cc.Point>, 
            _color : ctype.value_type<cc.Color> 
        ) : void;

        /**
         * 
         * @method getLineWidth
         * @return {number}
         */
        public getLineWidth (
        ) : number;

        /**
         *  Draw a point.<br>
         * -- param point A Vec2 used to point.<br>
         * -- param pointSize The point size.<br>
         * -- param color The point color.<br>
         * -- js NA
         * @method drawPoint
         * @param {ctype.value_type<cc.Point>} _point
         * @param {number} _pointSize
         * @param {ctype.value_type<cc.Color>} _color
         */
        public drawPoint (
            _point : ctype.value_type<cc.Point>, 
            _pointSize : number, 
            _color : ctype.value_type<cc.Color> 
        ) : void;

        /**
         *  Draw a cubic bezier curve with color and number of segments<br>
         * -- param origin The origin of the bezier path.<br>
         * -- param control1 The first control of the bezier path.<br>
         * -- param control2 The second control of the bezier path.<br>
         * -- param destination The destination of the bezier path.<br>
         * -- param segments The number of segments.<br>
         * -- param color Set the cubic bezier color.
         * @method drawCubicBezier
         * @param {ctype.value_type<cc.Point>} _origin
         * @param {ctype.value_type<cc.Point>} _control1
         * @param {ctype.value_type<cc.Point>} _control2
         * @param {ctype.value_type<cc.Point>} _destination
         * @param {number} _segments
         * @param {ctype.value_type<cc.Color>} _color
         */
        public drawCubicBezier (
            _origin : ctype.value_type<cc.Point>, 
            _control1 : ctype.value_type<cc.Point>, 
            _control2 : ctype.value_type<cc.Point>, 
            _destination : ctype.value_type<cc.Point>, 
            _segments : number, 
            _color : ctype.value_type<cc.Color> 
        ) : void;

        /**
         *  creates and initialize a DrawNode node.<br>
         * -- return Return an autorelease object.
         * @method create
         * @return {cc.DrawNode}
         */
        public static create (
        ) : cc.DrawNode;

        /**
         * 
         * @method DrawNode
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class Label
     * @native
     */
    export class Label 
        extends cc.Node
    {
        /* Can't find native property getContentSize implementation of cc::Label. using any type */
        public size : any;
        /* Can't find native property _getWidth implementation of cc::Label. using any type */
        public width : any;
        /* Can't find native property _getHeight implementation of cc::Label. using any type */
        public height : any;
        /**
         * 
         * @method getWidth
         * @return {number}
         */
        public boundingWidth : number;
        /**
         * 
         * @method getHeight
         * @return {number}
         */
        public boundingHeight : number;


        /**
         * 
         * @method isClipMarginEnabled
         * @return {boolean}
         */
        public isClipMarginEnabled (
        ) : boolean;

        /**
         * Enable shadow effect to Label.<br>
         * -- todo Support blur for shadow effect.
         * @method enableShadow
         */
        public enableShadow (
        ) : void;

        /**
         *  Sets the untransformed size of the Label in a more efficient way. 
         * @method setDimensions
         * @param {number} _width
         * @param {number} _height
         */
        public setDimensions (
            _width : number, 
            _height : number 
        ) : void;

        /**
         * 
         * @method getWidth
         * @return {number}
         */
        public getWidth (
        ) : number;

        /**
         *  Return the text the Label is currently displaying.
         * @method getString
         * @return {string}
         */
        public getString (
        ) : string;

        /**
         * 
         * @method getHeight
         * @return {number}
         */
        public getHeight (
        ) : number;

        /**
         * Disable effect to Label.<br>
         * -- see `LabelEffect`
         * @method disableEffect
         * @param {(cc.LabelEffect)} _effect?
         */
        public disableEffect (
            _effect? : (number)
        ) : void;

        /**
         *  Returns the text color of the Label.
         * @method getTextColor
         * @return {ctype.value_type<cc.Color>}
         */
        public getTextColor (
        ) : ctype.value_type<cc.Color>;

        /**
         * 
         * @method getBlendFunc
         * @return {cc.BlendFunc}
         */
        public getBlendFunc (
        ) : cc.BlendFunc;

        /**
         * Toggle wrap option of the label.<br>
         * -- Note: System font doesn't support manually toggle wrap.<br>
         * -- param enable Set true to enable wrap and false to disable wrap.
         * @method enableWrap
         * @param {boolean} _enable
         */
        public enableWrap (
            _enable : boolean 
        ) : void;

        /**
         * Makes the Label exactly this untransformed width.<br>
         * -- The Label's width be used for text align if the value not equal zero.
         * @method setWidth
         * @param {number} _width
         */
        public setWidth (
            _width : number 
        ) : void;

        /**
         * Returns the additional kerning of the Label.<br>
         * -- warning Not support system font.<br>
         * -- since v3.2.0
         * @method getAdditionalKerning
         * @return {number}
         */
        public getAdditionalKerning (
        ) : number;

        /**
         * Return the user define BMFont size.<br>
         * -- return The BMFont size in float value.
         * @method getBMFontSize
         * @return {number}
         */
        public getBMFontSize (
        ) : number;

        /**
         * 
         * @method getMaxLineWidth
         * @return {number}
         */
        public getMaxLineWidth (
        ) : number;

        /**
         *  Returns the Label's text horizontal alignment.
         * @method getHorizontalAlignment
         * @return {number}
         */
        public getHorizontalAlignment (
        ) : number;

        /**
         * Return shadow effect offset value.
         * @method getShadowOffset
         * @return {ctype.value_type<cc.Size>}
         */
        public getShadowOffset (
        ) : ctype.value_type<cc.Size>;

        /**
         * 
         * @method getLineSpacing
         * @return {number}
         */
        public getLineSpacing (
        ) : number;

        /**
         *  Clips upper and lower margin to reduce height of Label.
         * @method setClipMarginEnabled
         * @param {boolean} _clipEnabled
         */
        public setClipMarginEnabled (
            _clipEnabled : boolean 
        ) : void;

        /**
         *  Sets the text that this Label is to display.
         * @method setString
         * @param {string} _text
         */
        public setString (
            _text : string 
        ) : void;

        /**
         * Sets a new system font to Label.<br>
         * -- param font A font file or a font family name.<br>
         * -- warning
         * @method setSystemFontName
         * @param {string} _font
         */
        public setSystemFontName (
            _font : string 
        ) : void;

        /**
         * Query the wrap is enabled or not.<br>
         * -- Note: System font will always return true.
         * @method isWrapEnabled
         * @return {boolean}
         */
        public isWrapEnabled (
        ) : boolean;

        /**
         * Return the outline effect size value.
         * @method getOutlineSize
         * @return {number}
         */
        public getOutlineSize (
        ) : number;

        /**
         *  Sets a new bitmap font to Label 
         * @method setBMFontFilePath
         * @param {string} _bmfontFilePath
         * @param {ctype.value_type<cc.Point>} _imageOffset
         * @param {number} _fontSize
         * @return {boolean}
         */
        public setBMFontFilePath (
            _bmfontFilePath : string, 
            _imageOffset : ctype.value_type<cc.Point>, 
            _fontSize : number 
        ) : boolean;

        /**
         * 
         * @method initWithTTF
         * @param {(cc._ttfConfig) | (string)} _ttfConfig | text
         * @param {(string)} _text | fontFilePath
         * @param {(cc.TextHAlignment) | (number)} _hAlignment | fontSize
         * @param {(number) | (ctype.value_type<cc.Size>)} _maxLineWidth | dimensions
         * @param {(cc.TextHAlignment)} _hAlignment?
         * @param {(cc.TextVAlignment)} _vAlignment?
         * @return {boolean}
         */
        public initWithTTF (
            _ttfConfig_text : (cc._ttfConfig) | (string), 
            _text_fontFilePath : (string), 
            _hAlignment_fontSize : (number), 
            _maxLineWidth_dimensions : (number) | (ctype.value_type<cc.Size>), 
            _hAlignment? : (number), 
            _vAlignment? : (number)
        ) : boolean;

        /**
         *  Sets the line height of the Label.<br>
         * -- warning Not support system font.<br>
         * -- since v3.2.0
         * @method setLineHeight
         * @param {number} _height
         */
        public setLineHeight (
            _height : number 
        ) : void;

        /**
         * 
         * @method setSystemFontSize
         * @param {number} _fontSize
         */
        public setSystemFontSize (
            _fontSize : number 
        ) : void;

        /**
         * Change the label's Overflow type, currently only TTF and BMFont support all the valid Overflow type.<br>
         * -- Char Map font supports all the Overflow type except for SHRINK, because we can't measure it's font size.<br>
         * -- System font only support Overflow::Normal and Overflow::RESIZE_HEIGHT.<br>
         * -- param overflow   see `Overflow`
         * @method setOverflow
         * @param {cc.Label::Overflow} _overflow
         */
        public setOverflow (
            _overflow : number 
        ) : void;

        /**
         * 
         * @method setForceWrap
         */
        public setForceWrap (
        ) : void;

        /**
         *  Update content immediately.
         * @method updateContent
         */
        public updateContent (
        ) : void;

        /**
         * Return length of string.
         * @method getStringLength
         * @return {number}
         */
        public getStringLength (
        ) : number;

        /**
         * Specify what happens when a line is too long for Label.<br>
         * -- param breakWithoutSpace Lines are automatically broken between words if this value is false.
         * @method setLineBreakWithoutSpace
         * @param {boolean} _breakWithoutSpace
         */
        public setLineBreakWithoutSpace (
            _breakWithoutSpace : boolean 
        ) : void;

        /**
         * Return the number of lines of text.
         * @method getStringNumLines
         * @return {number}
         */
        public getStringNumLines (
        ) : number;

        /**
         * Enable outline effect to Label.<br>
         * -- warning Limiting use to only when the Label created with true type font or system font.
         * @method enableOutline
         * @param {ctype.value_type<cc.Color>} _outlineColor
         * @param {number} _outlineSize
         */
        public enableOutline (
            _outlineColor : ctype.value_type<cc.Color>, 
            _outlineSize : number 
        ) : void;

        /**
         * Return the shadow effect blur radius.
         * @method getShadowBlurRadius
         * @return {number}
         */
        public getShadowBlurRadius (
        ) : number;

        /**
         * Return current effect color value.
         * @method getEffectColor
         * @return {ctype.value_type<cc.Color>}
         */
        public getEffectColor (
        ) : ctype.value_type<cc.Color>;

        /**
         * 
         * @method removeAllChildrenWithCleanup
         * @param {boolean} _cleanup
         */
        public removeAllChildrenWithCleanup (
            _cleanup : boolean 
        ) : void;

        /**
         * Sets a new char map configuration to Label.<br>
         * -- see `createWithCharMap(Texture2D*,int,int,int)`
         * @method setCharMap
         * @param {(cc.Texture2D) | (string)} _texture | charMapFile | plistFile
         * @param {(number)} _itemWidth?
         * @param {(number)} _itemHeight?
         * @param {(number)} _startCharMap?
         * @return {boolean}
         */
        public setCharMap (
            _texture_charMapFile_plistFile : (cc.Texture2D) | (string), 
            _itemWidth? : (number), 
            _itemHeight? : (number), 
            _startCharMap? : (number)
        ) : boolean;

        /**
         * 
         * @method getDimensions
         * @return {ctype.value_type<cc.Size>}
         */
        public getDimensions (
        ) : ctype.value_type<cc.Size>;

        /**
         * Makes the Label at most this line untransformed width.<br>
         * -- The Label's max line width be used for force line breaks if the value not equal zero.
         * @method setMaxLineWidth
         * @param {number} _maxLineWidth
         */
        public setMaxLineWidth (
            _maxLineWidth : number 
        ) : void;

        /**
         *  Returns the system font used by the Label.
         * @method getSystemFontName
         * @return {string}
         */
        public getSystemFontName (
        ) : string;

        /**
         *  Sets the Label's text vertical alignment.
         * @method setVerticalAlignment
         * @param {cc.TextVAlignment} _vAlignment
         */
        public setVerticalAlignment (
            _vAlignment : number 
        ) : void;

        /**
         * 
         * @method setLineSpacing
         * @param {number} _height
         */
        public setLineSpacing (
            _height : number 
        ) : void;

        /**
         * Returns the line height of this Label.<br>
         * -- warning Not support system font.<br>
         * -- since v3.2.0
         * @method getLineHeight
         * @return {number}
         */
        public getLineHeight (
        ) : number;

        /**
         * Return the shadow effect color value.
         * @method getShadowColor
         * @return {ctype.value_type<cc.Color>}
         */
        public getShadowColor (
        ) : ctype.value_type<cc.Color>;

        /**
         * Returns the TTF configuration object used by the Label.<br>
         * -- see `TTFConfig`
         * @method getTTFConfig
         * @return {cc._ttfConfig}
         */
        public getTTFConfig (
        ) : cc._ttfConfig;

        /**
         * Enable italics rendering
         * @method enableItalics
         */
        public enableItalics (
        ) : void;

        /**
         * Sets the text color of Label.<br>
         * -- The text color is different from the color of Node.<br>
         * -- warning Limiting use to only when the Label created with true type font or system font.
         * @method setTextColor
         * @param {ctype.value_type<cc.Color>} _color
         */
        public setTextColor (
            _color : ctype.value_type<cc.Color> 
        ) : void;

        /**
         * Provides a way to treat each character like a Sprite.<br>
         * -- warning No support system font.
         * @method getLetter
         * @param {number} _lettetIndex
         * @return {cc.Sprite}
         */
        public getLetter (
            _lettetIndex : number 
        ) : cc.Sprite;

        /**
         * Makes the Label exactly this untransformed height.<br>
         * -- The Label's height be used for text align if the value not equal zero.<br>
         * -- The text will display incomplete if the size of Label is not large enough to display all text.
         * @method setHeight
         * @param {number} _height
         */
        public setHeight (
            _height : number 
        ) : void;

        /**
         * Return whether the shadow effect is enabled.
         * @method isShadowEnabled
         * @return {boolean}
         */
        public isShadowEnabled (
        ) : boolean;

        /**
         * Enable glow effect to Label.<br>
         * -- warning Limiting use to only when the Label created with true type font.
         * @method enableGlow
         * @param {ctype.value_type<cc.Color>} _glowColor
         */
        public enableGlow (
            _glowColor : ctype.value_type<cc.Color> 
        ) : void;

        /**
         * Query the label's Overflow type.<br>
         * -- return see `Overflow`
         * @method getOverflow
         * @return {number}
         */
        public getOverflow (
        ) : number;

        /**
         *  Returns the Label's text vertical alignment.
         * @method getVerticalAlignment
         * @return {number}
         */
        public getVerticalAlignment (
        ) : number;

        /**
         * Sets the additional kerning of the Label.<br>
         * -- warning Not support system font.<br>
         * -- since v3.2.0
         * @method setAdditionalKerning
         * @param {number} _space
         */
        public setAdditionalKerning (
            _space : number 
        ) : void;

        /**
         *  Returns the bitmap font path used by the Label.
         * @method getSystemFontSize
         * @return {number}
         */
        public getSystemFontSize (
        ) : number;

        /**
         * 
         * @method setBlendFunc
         * @param {cc.BlendFunc} _blendFunc
         */
        public setBlendFunc (
            _blendFunc : cc.BlendFunc 
        ) : void;

        /**
         *  Returns the Label's text horizontal alignment.
         * @method getTextAlignment
         * @return {number}
         */
        public getTextAlignment (
        ) : number;

        /**
         *  Returns the bitmap font used by the Label.
         * @method getBMFontFilePath
         * @return {string}
         */
        public getBMFontFilePath (
        ) : string;

        /**
         * Enables strikethrough.<br>
         * -- Underline and Strikethrough cannot be enabled at the same time.<br>
         * -- Strikethrough is like an underline but at the middle of the glyph
         * @method enableStrikethrough
         */
        public enableStrikethrough (
        ) : void;

        /**
         *  Sets the Label's text horizontal alignment.
         * @method setHorizontalAlignment
         * @param {cc.TextHAlignment} _hAlignment
         */
        public setHorizontalAlignment (
            _hAlignment : number 
        ) : void;

        /**
         * Enable bold rendering
         * @method enableBold
         */
        public enableBold (
        ) : void;

        /**
         * Enable underline
         * @method enableUnderline
         */
        public enableUnderline (
        ) : void;

        /**
         * Return current effect type.
         * @method getLabelEffectType
         * @return {number}
         */
        public getLabelEffectType (
        ) : number;

        /**
         *  Sets the Label's text alignment.
         * @method setAlignment
         * @param {(cc.TextHAlignment)} _hAlignment
         * @param {(cc.TextVAlignment)} _vAlignment?
         */
        public setAlignment (
            _hAlignment : (number), 
            _vAlignment? : (number)
        ) : void;

        /**
         * warning This method is not recommended for game developers.
         * @method requestSystemFontRefresh
         */
        public requestSystemFontRefresh (
        ) : void;

        /**
         * Change font size of label type BMFONT<br>
         * -- Note: This function only scale the BMFONT letter to mimic the font size change effect.<br>
         * -- param fontSize The desired font size in float.
         * @method setBMFontSize
         * @param {number} _fontSize
         */
        public setBMFontSize (
            _fontSize : number 
        ) : void;

        /**
         * Allocates and initializes a Label, with a bitmap font file.<br>
         * -- param bmfontPath A bitmap font file, it's a FNT format.<br>
         * -- param text The initial text.<br>
         * -- param hAlignment Text horizontal alignment.<br>
         * -- param maxLineWidth The max line width.<br>
         * -- param imageOffset<br>
         * -- return An automatically released Label object.<br>
         * -- see setBMFontFilePath setMaxLineWidth
         * @method createWithBMFont
         * @param {string} _bmfontPath
         * @param {string} _text
         * @param {cc.TextHAlignment} _hAlignment
         * @param {number} _maxLineWidth
         * @param {ctype.value_type<cc.Point>} _imageOffset
         * @return {cc.Label}
         */
        public static createWithBMFont (
            _bmfontPath : string, 
            _text : string, 
            _hAlignment : number, 
            _maxLineWidth : number, 
            _imageOffset : ctype.value_type<cc.Point> 
        ) : cc.Label;

        /**
         * Allocates and initializes a Label, with default settings.<br>
         * -- return An automatically released Label object.
         * @method create
         * @return {cc.Label}
         */
        public static create (
        ) : cc.Label;

        /**
         * Allocates and initializes a Label, with char map configuration.<br>
         * -- param texture A pointer to an existing Texture2D object.<br>
         * -- param itemWidth The width in points of each element.<br>
         * -- param itemHeight The height in points of each element.<br>
         * -- param startCharMap The starting char of the char map.<br>
         * -- return An automatically released Label object.
         * @method createWithCharMap
         * @param {(cc.Texture2D) | (string)} _texture | charMapFile | plistFile
         * @param {(number)} _itemWidth?
         * @param {(number)} _itemHeight?
         * @param {(number)} _startCharMap?
         * @return {cc.Label}
         */
        public static createWithCharMap (
            _texture_charMapFile_plistFile : (cc.Texture2D) | (string), 
            _itemWidth? : (number), 
            _itemHeight? : (number), 
            _startCharMap? : (number)
        ) : cc.Label;

        /**
         * Allocates and initializes a Label, base on platform-dependent API.<br>
         * -- param text The initial text.<br>
         * -- param font A font file or a font family name.<br>
         * -- param fontSize The font size. This value must be > 0.<br>
         * -- param dimensions<br>
         * -- param hAlignment The text horizontal alignment.<br>
         * -- param vAlignment The text vertical alignment.<br>
         * -- warning It will generate texture by the platform-dependent code.<br>
         * -- return An automatically released Label object.
         * @method createWithSystemFont
         * @param {string} _text
         * @param {string} _font
         * @param {number} _fontSize
         * @param {ctype.value_type<cc.Size>} _dimensions
         * @param {cc.TextHAlignment} _hAlignment
         * @param {cc.TextVAlignment} _vAlignment
         * @return {cc.Label}
         */
        public static createWithSystemFont (
            _text : string, 
            _font : string, 
            _fontSize : number, 
            _dimensions : ctype.value_type<cc.Size>, 
            _hAlignment : number, 
            _vAlignment : number 
        ) : cc.Label;

        /**
         * Constructor of Label.<br>
         * -- js NA
         * @method Label
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class LabelAtlas
     * @native
     */
    export class LabelAtlas 
        extends cc.AtlasNode
    {
        /* Can't find native property getColor implementation of cc::LabelAtlas. using any type */
        public color : any;
        /**
         * 
         * @method getString
         * @return {string}
         */
        public string : string;
        /* Can't find native property getOpacity implementation of cc::LabelAtlas. using any type */
        public opacity : any;


        /**
         * 
         * @method setString
         * @param {string} _label
         */
        public setString (
            _label : string 
        ) : void;

        /**
         * Initializes the LabelAtlas with a string and a configuration file.<br>
         * -- since v2.0
         * @method initWithString
         * @param {(string)} _string
         * @param {(string) | (cc.Texture2D)} _fntFile | charMapFile | texture
         * @param {(number)} _itemWidth?
         * @param {(number)} _itemHeight?
         * @param {(number)} _startCharMap?
         * @return {boolean}
         */
        public initWithString (
            _string : (string), 
            _fntFile_charMapFile_texture : (string) | (cc.Texture2D), 
            _itemWidth? : (number), 
            _itemHeight? : (number), 
            _startCharMap? : (number)
        ) : boolean;

        /**
         * 
         * @method getString
         * @return {string}
         */
        public getString (
        ) : string;

        /**
         *  Creates the LabelAtlas with a string, a char map file(the atlas), the width and height of each element and the starting char of the atlas. 
         * @method create
         * @param {(string)} _string?
         * @param {(string)} _charMapFile | fntFile?
         * @param {(number)} _itemWidth?
         * @param {(number)} _itemHeight?
         * @param {(number)} _startCharMap?
         * @return {cc.LabelAtlas}
         */
        public static _create (
            _string? : (string), 
            _charMapFile_fntFile? : (string), 
            _itemWidth? : (number), 
            _itemHeight? : (number), 
            _startCharMap? : (number)
        ) : cc.LabelAtlas;

        /**
         * 
         * @method LabelAtlas
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class LabelBMFont
     * @native
     */
    export class LabelBMFont 
        extends cc.Node
    {
        /* Can't find native property _getAnchorX implementation of cc::LabelBMFont. using any type */
        public anchorX : any;
        /* Can't find native property _getAnchorY implementation of cc::LabelBMFont. using any type */
        public anchorY : any;
        /* Can't find native property getScale implementation of cc::LabelBMFont. using any type */
        public scale : any;
        /* Can't find native property getScaleX implementation of cc::LabelBMFont. using any type */
        public scaleX : any;
        /* Can't find native property getScaleY implementation of cc::LabelBMFont. using any type */
        public scaleY : any;
        /* Can't find native property isOpacityModifyRGB implementation of cc::LabelBMFont. using any type */
        public opacityModifyRGB : any;
        /* Can't find native property getOpacity implementation of cc::LabelBMFont. using any type */
        public opacity : any;
        /* Can't find native property isCascadeOpacityEnabled implementation of cc::LabelBMFont. using any type */
        public cascadeOpacity : any;
        /* Can't find native property getColor implementation of cc::LabelBMFont. using any type */
        public color : any;
        /* Can't find native property isCascadeColorEnabled implementation of cc::LabelBMFont. using any type */
        public cascadeColor : any;
        /**
         * 
         * @method getString
         * @return {string}
         */
        public string : string;
        /* Can't find native property _getAlignment implementation of cc::LabelBMFont. using any type */
        public textAlign : any;
        /* Can't find native property _getBoundingWidth implementation of cc::LabelBMFont. using any type */
        public boundingWidth : any;
        /* Can't find native property _getBoundingHeight implementation of cc::LabelBMFont. using any type */
        public boundingHeight : any;


        /**
         * 
         * @method setLineBreakWithoutSpace
         * @param {boolean} _breakWithoutSpace
         */
        public setLineBreakWithoutSpace (
            _breakWithoutSpace : boolean 
        ) : void;

        /**
         * 
         * @method getBlendFunc
         * @return {cc.BlendFunc}
         */
        public getBlendFunc (
        ) : cc.BlendFunc;

        /**
         * 
         * @method getLetter
         * @param {number} _ID
         * @return {cc.Sprite}
         */
        public getLetter (
            _ID : number 
        ) : cc.Sprite;

        /**
         * 
         * @method getString
         * @return {string}
         */
        public getString (
        ) : string;

        /**
         * 
         * @method setBlendFunc
         * @param {cc.BlendFunc} _blendFunc
         */
        public setBlendFunc (
            _blendFunc : cc.BlendFunc 
        ) : void;

        /**
         * 
         * @method setString
         * @param {string} _newString
         */
        public setString (
            _newString : string 
        ) : void;

        /**
         *  init a bitmap font atlas with an initial string and the FNT file 
         * @method initWithString
         * @param {string} _str
         * @param {string} _fntFile
         * @param {number} _width
         * @param {cc.TextHAlignment} _alignment
         * @param {ctype.value_type<cc.Point>} _imageOffset
         * @return {boolean}
         */
        public initWithString (
            _str : string, 
            _fntFile : string, 
            _width : number, 
            _alignment : number, 
            _imageOffset : ctype.value_type<cc.Point> 
        ) : boolean;

        /**
         * 
         * @method getFntFile
         * @return {string}
         */
        public getFntFile (
        ) : string;

        /**
         * 
         * @method setFntFile
         * @param {string} _fntFile
         * @param {ctype.value_type<cc.Point>} _imageOffset
         */
        public setFntFile (
            _fntFile : string, 
            _imageOffset : ctype.value_type<cc.Point> 
        ) : void;

        /**
         * 
         * @method setAlignment
         * @param {cc.TextHAlignment} _alignment
         */
        public setAlignment (
            _alignment : number 
        ) : void;

        /**
         * 
         * @method setWidth
         * @param {number} _width
         */
        public setWidth (
            _width : number 
        ) : void;

        /**
         *  Creates an label.
         * @method create
         * @param {(string)} _str?
         * @param {(string)} _fntFile?
         * @param {(number)} _width?
         * @param {(cc.TextHAlignment)} _alignment?
         * @param {(ctype.value_type<cc.Point>)} _imageOffset?
         * @return {cc.LabelBMFont}
         */
        public static create (
            _str? : (string), 
            _fntFile? : (string), 
            _width? : (number), 
            _alignment? : (number), 
            _imageOffset? : (ctype.value_type<cc.Point>)
        ) : cc.LabelBMFont;

        /**
         * js ctor
         * @method LabelBMFont
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class LabelTTF
     * @native
     */
    export class LabelTTF 
        extends cc.Node
    {
        /* Can't find native property getContentSize implementation of cc::LabelTTF. using any type */
        public size : any;
        /* Can't find native property _getWidth implementation of cc::LabelTTF. using any type */
        public width : any;
        /* Can't find native property _getHeight implementation of cc::LabelTTF. using any type */
        public height : any;
        /**
         * 
         * @method getString
         * @return {string}
         */
        public string : string;
        /**
         * 
         * @method getHorizontalAlignment
         * @return {number}
         */
        public textAlign : number;
        /**
         * 
         * @method getVerticalAlignment
         * @return {number}
         */
        public verticalAlign : number;
        /**
         * 
         * @method getFontSize
         * @return {number}
         */
        public fontSize : number;
        /**
         * 
         * @method getFontName
         * @return {string}
         */
        public fontName : string;
        /* Can't find native property _getFont implementation of cc::LabelTTF. using any type */
        public font : any;
        /* Can't find native property _getBoundingWidth implementation of cc::LabelTTF. using any type */
        public boundingWidth : any;
        /* Can't find native property _getBoundingHeight implementation of cc::LabelTTF. using any type */
        public boundingHeight : any;
        /* Can't find native property _getFillStyle implementation of cc::LabelTTF. using any type */
        public fillStyle : any;
        /* Can't find native property _getStrokeStyle implementation of cc::LabelTTF. using any type */
        public strokeStyle : any;
        /* Can't find native property _getLineWidth implementation of cc::LabelTTF. using any type */
        public lineWidth : any;
        /* Can't find native property _getShadowOffsetX implementation of cc::LabelTTF. using any type */
        public shadowOffsetX : any;
        /* Can't find native property _getShadowOffsetY implementation of cc::LabelTTF. using any type */
        public shadowOffsetY : any;
        /* Can't find native property _getShadowOpacity implementation of cc::LabelTTF. using any type */
        public shadowOpacity : any;
        /* Can't find native property _getShadowBlur implementation of cc::LabelTTF. using any type */
        public shadowBlur : any;
        /* Can't find native property getOpacity implementation of cc::LabelTTF. using any type */
        public opacity : any;
        /* Can't find native property isOpacityModifyRGB implementation of cc::LabelTTF. using any type */
        public opacityModifyRGB : any;
        /* Can't find native property isCascadeOpacityEnabled implementation of cc::LabelTTF. using any type */
        public cascadeOpacity : any;
        /* Can't find native property getColor implementation of cc::LabelTTF. using any type */
        public color : any;
        /* Can't find native property isCascadeColorEnabled implementation of cc::LabelTTF. using any type */
        public cascadeColor : any;


        /**
         *  enable or disable shadow for the label 
         * @method enableShadow
         * @param {ctype.value_type<cc.Size>} _shadowOffset
         * @param {number} _shadowOpacity
         * @param {number} _shadowBlur
         * @param {boolean} _mustUpdateTexture
         */
        public enableShadow (
            _shadowOffset : ctype.value_type<cc.Size>, 
            _shadowOpacity : number, 
            _shadowBlur : number, 
            _mustUpdateTexture : boolean 
        ) : void;

        /**
         * 
         * @method setDimensions
         * @param {ctype.value_type<cc.Size>} _dim
         */
        public setDimensions (
            _dim : ctype.value_type<cc.Size> 
        ) : void;

        /**
         * 
         * @method getFontSize
         * @return {number}
         */
        public getFontSize (
        ) : number;

        /**
         * 
         * @method getString
         * @return {string}
         */
        public getString (
        ) : string;

        /**
         * 
         * @method setFlippedY
         * @param {boolean} _flippedY
         */
        public setFlippedY (
            _flippedY : boolean 
        ) : void;

        /**
         * 
         * @method setFlippedX
         * @param {boolean} _flippedX
         */
        public setFlippedX (
            _flippedX : boolean 
        ) : void;

        /**
         *  set the text definition used by this label 
         * @method setTextDefinition
         * @param {cc.FontDefinition} _theDefinition
         */
        public setTextDefinition (
            _theDefinition : cc.FontDefinition 
        ) : void;

        /**
         * 
         * @method setFontName
         * @param {string} _fontName
         */
        public setFontName (
            _fontName : string 
        ) : void;

        /**
         * 
         * @method getHorizontalAlignment
         * @return {number}
         */
        public getHorizontalAlignment (
        ) : number;

        /**
         *  initializes the LabelTTF with a font name, alignment, dimension and font size 
         * @method initWithStringAndTextDefinition
         * @param {string} _string
         * @param {cc.FontDefinition} _textDefinition
         * @return {boolean}
         */
        public initWithStringAndTextDefinition (
            _string : string, 
            _textDefinition : cc.FontDefinition 
        ) : boolean;

        /**
         *  changes the string to render<br>
         * -- warning Changing the string is as expensive as creating a new LabelTTF. To obtain better performance use LabelAtlas
         * @method setString
         * @param {string} _label
         */
        public setString (
            _label : string 
        ) : void;

        /**
         *  initializes the LabelTTF with a font name, alignment, dimension and font size 
         * @method initWithString
         * @param {string} _string
         * @param {string} _fontName
         * @param {number} _fontSize
         * @param {ctype.value_type<cc.Size>} _dimensions
         * @param {cc.TextHAlignment} _hAlignment
         * @param {cc.TextVAlignment} _vAlignment
         * @return {boolean}
         */
        public initWithString (
            _string : string, 
            _fontName : string, 
            _fontSize : number, 
            _dimensions : ctype.value_type<cc.Size>, 
            _hAlignment : number, 
            _vAlignment : number 
        ) : boolean;

        /**
         *  set text tinting 
         * @method setFontFillColor
         * @param {ctype.value_type<cc.Color>} _tintColor
         * @param {boolean} _mustUpdateTexture
         */
        public setFontFillColor (
            _tintColor : ctype.value_type<cc.Color>, 
            _mustUpdateTexture : boolean 
        ) : void;

        /**
         * 
         * @method getBlendFunc
         * @return {cc.BlendFunc}
         */
        public getBlendFunc (
        ) : cc.BlendFunc;

        /**
         *  enable or disable stroke 
         * @method enableStroke
         * @param {ctype.value_type<cc.Color>} _strokeColor
         * @param {number} _strokeSize
         * @param {boolean} _mustUpdateTexture
         */
        public enableStroke (
            _strokeColor : ctype.value_type<cc.Color>, 
            _strokeSize : number, 
            _mustUpdateTexture : boolean 
        ) : void;

        /**
         * 
         * @method getDimensions
         * @return {ctype.value_type<cc.Size>}
         */
        public getDimensions (
        ) : ctype.value_type<cc.Size>;

        /**
         * 
         * @method setVerticalAlignment
         * @param {cc.TextVAlignment} _verticalAlignment
         */
        public setVerticalAlignment (
            _verticalAlignment : number 
        ) : void;

        /**
         * 
         * @method setFontSize
         * @param {number} _fontSize
         */
        public setFontSize (
            _fontSize : number 
        ) : void;

        /**
         * 
         * @method getVerticalAlignment
         * @return {number}
         */
        public getVerticalAlignment (
        ) : number;

        /**
         *  get the text definition used by this label 
         * @method getTextDefinition
         * @return {cc.FontDefinition}
         */
        public getTextDefinition (
        ) : cc.FontDefinition;

        /**
         * 
         * @method setBlendFunc
         * @param {cc.BlendFunc} _blendFunc
         */
        public setBlendFunc (
            _blendFunc : cc.BlendFunc 
        ) : void;

        /**
         * 
         * @method getFontName
         * @return {string}
         */
        public getFontName (
        ) : string;

        /**
         * 
         * @method setHorizontalAlignment
         * @param {cc.TextHAlignment} _alignment
         */
        public setHorizontalAlignment (
            _alignment : number 
        ) : void;

        /**
         *  disable shadow rendering 
         * @method disableShadow
         */
        public disableShadow (
        ) : void;

        /**
         *  disable stroke 
         * @method disableStroke
         */
        public disableStroke (
        ) : void;

        /**
         *  Creates an label.
         * @method create
         * @param {(string)} _string?
         * @param {(string)} _fontName?
         * @param {(number)} _fontSize?
         * @param {(ctype.value_type<cc.Size>)} _dimensions?
         * @param {(cc.TextHAlignment)} _hAlignment?
         * @param {(cc.TextVAlignment)} _vAlignment?
         * @return {cc.LabelTTF}
         */
        public static create (
            _string? : (string), 
            _fontName? : (string), 
            _fontSize? : (number), 
            _dimensions? : (ctype.value_type<cc.Size>), 
            _hAlignment? : (number), 
            _vAlignment? : (number)
        ) : cc.LabelTTF;

        /**
         *  Create a label with string and a font definition
         * @method createWithFontDefinition
         * @param {string} _string
         * @param {cc.FontDefinition} _textDefinition
         * @return {cc.LabelTTF}
         */
        public static createWithFontDefinition (
            _string : string, 
            _textDefinition : cc.FontDefinition 
        ) : cc.LabelTTF;

        /**
         * js ctor
         * @method LabelTTF
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class Layer
     * @native
     */
    export class Layer 
        extends cc.Node
    {

        /**
         *  Creates a fullscreen black layer.<br>
         * -- return An autoreleased Layer object.
         * @method create
         * @return {cc.Layer}
         */
        public static create (
        ) : cc.Layer;

        /**
         * 
         * @method Layer
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class __LayerRGBA
     * @native
     */
    export class LayerRGBA 
        extends cc.Layer
    {
        /* Can't find native property isOpacityModifyRGB implementation of cc::LayerRGBA. using any type */
        public opacityModifyRGB : any;
        /* Can't find native property getOpacity implementation of cc::LayerRGBA. using any type */
        public opacity : any;
        /* Can't find native property isCascadeOpacityEnabled implementation of cc::LayerRGBA. using any type */
        public cascadeOpacity : any;
        /* Can't find native property getColor implementation of cc::LayerRGBA. using any type */
        public color : any;
        /* Can't find native property isCascadeColorEnabled implementation of cc::LayerRGBA. using any type */
        public cascadeColor : any;


        /**
         * 
         * @method create
         * @return {cc.__LayerRGBA}
         */
        public static create (
        ) : cc.__LayerRGBA;

        /**
         * 
         * @method __LayerRGBA
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class LayerColor
     * @native
     */
    export class LayerColor 
        extends cc.Layer
    {
        /* Can't find native property _getWidth implementation of cc::LayerColor. using any type */
        public width : any;
        /* Can't find native property _getHeight implementation of cc::LayerColor. using any type */
        public height : any;
        /* Can't find native property getOpacity implementation of cc::LayerColor. using any type */
        public opacity : any;
        /* Can't find native property getColor implementation of cc::LayerColor. using any type */
        public color : any;
        /* Can't find native property isOpacityModifyRGB implementation of cc::LayerColor. using any type */
        public opacityModifyRGB : any;
        /* Can't find native property _getWidth implementation of cc::LayerColor. using any type */
        public width : any;
        /* Can't find native property _getHeight implementation of cc::LayerColor. using any type */
        public height : any;
        /* Can't find native property getOpacity implementation of cc::LayerColor. using any type */
        public opacity : any;
        /* Can't find native property isOpacityModifyRGB implementation of cc::LayerColor. using any type */
        public opacityModifyRGB : any;
        /* Can't find native property isCascadeOpacityEnabled implementation of cc::LayerColor. using any type */
        public cascadeOpacity : any;
        /* Can't find native property getColor implementation of cc::LayerColor. using any type */
        public color : any;
        /* Can't find native property isCascadeColorEnabled implementation of cc::LayerColor. using any type */
        public cascadeColor : any;


        /**
         *  Change width and height in Points.<br>
         * -- param w The width of layer.<br>
         * -- param h The Height of layer.<br>
         * -- since v0.8
         * @method changeWidthAndHeight
         * @param {number} _w
         * @param {number} _h
         */
        public changeWidthAndHeight (
            _w : number, 
            _h : number 
        ) : void;

        /**
         *  BlendFunction. Conforms to BlendProtocol protocol <br>
         * -- lua NA
         * @method getBlendFunc
         * @return {cc.BlendFunc}
         */
        public getBlendFunc (
        ) : cc.BlendFunc;

        /**
         * code<br>
         * -- When this function bound into js or lua,the parameter will be changed<br>
         * -- In js: var setBlendFunc(var src, var dst)<br>
         * -- In lua: local setBlendFunc(local src, local dst)<br>
         * -- endcode
         * @method setBlendFunc
         * @param {cc.BlendFunc} _blendFunc
         */
        public setBlendFunc (
            _blendFunc : cc.BlendFunc 
        ) : void;

        /**
         *  Change width in Points.<br>
         * -- param w The width of layer.
         * @method changeWidth
         * @param {number} _w
         */
        public changeWidth (
            _w : number 
        ) : void;

        /**
         * 
         * @method initWithColor
         * @param {(ctype.value_type<cc.Color>)} _color
         * @param {(number)} _width?
         * @param {(number)} _height?
         * @return {boolean}
         */
        public init (
            _color : (ctype.value_type<cc.Color>), 
            _width? : (number), 
            _height? : (number)
        ) : boolean;

        /**
         *  Change height in Points.<br>
         * -- param h The height of layer.
         * @method changeHeight
         * @param {number} _h
         */
        public changeHeight (
            _h : number 
        ) : void;

        /**
         *  Creates a Layer with color, width and height in Points.<br>
         * -- param color The color of layer.<br>
         * -- param width The width of layer.<br>
         * -- param height The height of layer.<br>
         * -- return An autoreleased LayerColor object.
         * @method create
         * @param {(ctype.value_type<cc.Color>)} _color?
         * @param {(number)} _width?
         * @param {(number)} _height?
         * @return {cc.LayerColor}
         */
        public static create (
            _color? : (ctype.value_type<cc.Color>), 
            _width? : (number), 
            _height? : (number)
        ) : cc.LayerColor;

        /**
         * 
         * @method LayerColor
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class LayerGradient
     * @native
     */
    export class LayerGradient 
        extends cc.LayerColor
    {
        /* Can't find native property getContentSize implementation of cc::LayerGradient. using any type */
        public size : any;
        /* Can't find native property _getWidth implementation of cc::LayerGradient. using any type */
        public width : any;
        /* Can't find native property _getHeight implementation of cc::LayerGradient. using any type */
        public height : any;
        /**
         *  Returns the start color of the gradient.
         * return The start color.
         * @method getStartColor
         * @return {ctype.value_type<cc.Color>}
         */
        public startColor : ctype.value_type<cc.Color>;
        /**
         *  Returns the end color of the gradient.
         * return The end color.
         * @method getEndColor
         * @return {ctype.value_type<cc.Color>}
         */
        public endColor : ctype.value_type<cc.Color>;
        /**
         *  Returns the start opacity of the gradient.
         * return The start opacity.
         * @method getStartOpacity
         * @return {number}
         */
        public startOpacity : number;
        /**
         *  Returns the end opacity of the gradient.
         * return The end opacity.
         * @method getEndOpacity
         * @return {number}
         */
        public endOpacity : number;
        /**
         *  Returns the directional vector used for the gradient.
         * return The direction of gradient.
         * @method getVector
         * @return {ctype.value_type<cc.Point>}
         */
        public vector : ctype.value_type<cc.Point>;
        /**
         *  Get the compressedInterpolation
         * return The interpolation will be compressed if true.
         * @method isCompressedInterpolation
         * @return {boolean}
         */
        public compresseInterpolation : boolean;
        /* Can't find native property getOpacity implementation of cc::LayerGradient. using any type */
        public opacity : any;
        /* Can't find native property isOpacityModifyRGB implementation of cc::LayerGradient. using any type */
        public opacityModifyRGB : any;
        /* Can't find native property isCascadeOpacityEnabled implementation of cc::LayerGradient. using any type */
        public cascadeOpacity : any;
        /* Can't find native property getColor implementation of cc::LayerGradient. using any type */
        public color : any;
        /* Can't find native property isCascadeColorEnabled implementation of cc::LayerGradient. using any type */
        public cascadeColor : any;


        /**
         *  Returns the start color of the gradient.<br>
         * -- return The start color.
         * @method getStartColor
         * @return {ctype.value_type<cc.Color>}
         */
        public getStartColor (
        ) : ctype.value_type<cc.Color>;

        /**
         *  Get the compressedInterpolation<br>
         * -- return The interpolation will be compressed if true.
         * @method isCompressedInterpolation
         * @return {boolean}
         */
        public isCompressedInterpolation (
        ) : boolean;

        /**
         *  Returns the start opacity of the gradient.<br>
         * -- return The start opacity.
         * @method getStartOpacity
         * @return {number}
         */
        public getStartOpacity (
        ) : number;

        /**
         *  Sets the directional vector that will be used for the gradient.<br>
         * -- The default value is vertical direction (0,-1). <br>
         * -- param alongVector The direction of gradient.
         * @method setVector
         * @param {ctype.value_type<cc.Point>} _alongVector
         */
        public setVector (
            _alongVector : ctype.value_type<cc.Point> 
        ) : void;

        /**
         *  Returns the start opacity of the gradient.<br>
         * -- param startOpacity The start opacity, from 0 to 255.
         * @method setStartOpacity
         * @param {number} _startOpacity
         */
        public setStartOpacity (
            _startOpacity : number 
        ) : void;

        /**
         *  Whether or not the interpolation will be compressed in order to display all the colors of the gradient both in canonical and non canonical vectors.<br>
         * -- Default: true.<br>
         * -- param compressedInterpolation The interpolation will be compressed if true.
         * @method setCompressedInterpolation
         * @param {boolean} _compressedInterpolation
         */
        public setCompressedInterpolation (
            _compressedInterpolation : boolean 
        ) : void;

        /**
         *  Returns the end opacity of the gradient.<br>
         * -- param endOpacity The end opacity, from 0 to 255.
         * @method setEndOpacity
         * @param {number} _endOpacity
         */
        public setEndOpacity (
            _endOpacity : number 
        ) : void;

        /**
         *  Returns the directional vector used for the gradient.<br>
         * -- return The direction of gradient.
         * @method getVector
         * @return {ctype.value_type<cc.Point>}
         */
        public getVector (
        ) : ctype.value_type<cc.Point>;

        /**
         *  Sets the end color of the gradient.<br>
         * -- param endColor The end color.
         * @method setEndColor
         * @param {ctype.value_type<cc.Color>} _endColor
         */
        public setEndColor (
            _endColor : ctype.value_type<cc.Color> 
        ) : void;

        /**
         *  Initializes the Layer with a gradient between start and end in the direction of v.<br>
         * -- js init<br>
         * -- lua init
         * @method initWithColor
         * @param {(ctype.value_type<cc.Color>)} _start
         * @param {(ctype.value_type<cc.Color>)} _end
         * @param {(ctype.value_type<cc.Point>)} _v?
         * @return {boolean}
         */
        public initWithColor (
            _start : (ctype.value_type<cc.Color>), 
            _end : (ctype.value_type<cc.Color>), 
            _v? : (ctype.value_type<cc.Point>)
        ) : boolean;

        /**
         *  Returns the end color of the gradient.<br>
         * -- return The end color.
         * @method getEndColor
         * @return {ctype.value_type<cc.Color>}
         */
        public getEndColor (
        ) : ctype.value_type<cc.Color>;

        /**
         *  Returns the end opacity of the gradient.<br>
         * -- return The end opacity.
         * @method getEndOpacity
         * @return {number}
         */
        public getEndOpacity (
        ) : number;

        /**
         *  Sets the start color of the gradient.<br>
         * -- param startColor The start color.
         * @method setStartColor
         * @param {ctype.value_type<cc.Color>} _startColor
         */
        public setStartColor (
            _startColor : ctype.value_type<cc.Color> 
        ) : void;

        /**
         *  Creates a full-screen Layer with a gradient between start and end.<br>
         * -- param start The start color.<br>
         * -- param end The end color.<br>
         * -- return An autoreleased LayerGradient object.
         * @method create
         * @param {(ctype.value_type<cc.Color>)} _start?
         * @param {(ctype.value_type<cc.Color>)} _end?
         * @param {(ctype.value_type<cc.Point>)} _v?
         * @return {cc.LayerGradient}
         */
        public static create (
            _start? : (ctype.value_type<cc.Color>), 
            _end? : (ctype.value_type<cc.Color>), 
            _v? : (ctype.value_type<cc.Point>)
        ) : cc.LayerGradient;

        /**
         * 
         * @method LayerGradient
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class LayerMultiplex
     * @native
     */
    export class LayerMultiplex 
        extends cc.Layer
    {

        /**
         *  initializes a MultiplexLayer with an array of layers<br>
         * -- since v2.1
         * @method initWithArray
         * @param {cc.Layer[]} _arrayOfLayers
         * @return {boolean}
         */
        public initWithArray (
            _arrayOfLayers : cc.Layer[] 
        ) : boolean;

        /**
         *  release the current layer and switches to another layer indexed by n.<br>
         * -- The current (old) layer will be removed from it's parent with 'cleanup=true'.<br>
         * -- param n The layer indexed by n will display.
         * @method switchToAndReleaseMe
         * @param {number} _n
         */
        public switchToAndReleaseMe (
            _n : number 
        ) : void;

        /**
         *  Add a certain layer to LayerMultiplex.<br>
         * -- param layer A layer need to be added to the LayerMultiplex.
         * @method addLayer
         * @param {cc.Layer} _layer
         */
        public addLayer (
            _layer : cc.Layer 
        ) : void;

        /**
         *  Switches to a certain layer indexed by n.<br>
         * -- The current (old) layer will be removed from it's parent with 'cleanup=true'.<br>
         * -- param n The layer indexed by n will display.
         * @method switchTo
         * @param {number} _n
         */
        public switchTo (
            _n : number 
        ) : void;

        /**
         * js ctor
         * @method LayerMultiplex
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class MenuItem
     * @native
     */
    export class MenuItem 
        extends cc.Node
    {
        /* Can't find native property isOpacityModifyRGB implementation of cc::MenuItem. using any type */
        public opacityModifyRGB : any;
        /* Can't find native property getOpacity implementation of cc::MenuItem. using any type */
        public opacity : any;
        /* Can't find native property isCascadeOpacityEnabled implementation of cc::MenuItem. using any type */
        public cascadeOpacity : any;
        /* Can't find native property getColor implementation of cc::MenuItem. using any type */
        public color : any;
        /* Can't find native property isCascadeColorEnabled implementation of cc::MenuItem. using any type */
        public cascadeColor : any;
        /**
         *  Returns whether or not the item is enabled. 
         * @method isEnabled
         * @return {boolean}
         */
        public enabled : boolean;


        /**
         *  Enables or disables the item. 
         * @method setEnabled
         * @param {boolean} _value
         */
        public setEnabled (
            _value : boolean 
        ) : void;

        /**
         *  Activate the item. 
         * @method activate
         */
        public activate (
        ) : void;

        /**
         *  Initializes a MenuItem with a target/selector.<br>
         * -- lua NA
         * @method initWithCallback
         * @param {(arg0:cc.Ref) => void} _callback
         * @return {boolean}
         */
        public initWithCallback (
            _callback : (arg0:any) => void 
        ) : boolean;

        /**
         *  Returns whether or not the item is enabled. 
         * @method isEnabled
         * @return {boolean}
         */
        public isEnabled (
        ) : boolean;

        /**
         *  The item was selected (not activated), similar to "mouse-over". 
         * @method selected
         */
        public selected (
        ) : void;

        /**
         *  Returns whether or not the item is selected. 
         * @method isSelected
         * @return {boolean}
         */
        public isSelected (
        ) : boolean;

        /**
         *  Set the callback to the menu item.<br>
         * -- code<br>
         * -- In js,can contain two params,the second param is jsptr.<br>
         * -- endcode<br>
         * -- lua NA
         * @method setCallback
         * @param {(arg0:cc.Ref) => void} _callback
         */
        public setCallback (
            _callback : (arg0:any) => void 
        ) : void;

        /**
         *  The item was unselected. 
         * @method unselected
         */
        public unselected (
        ) : void;

        /**
         *  Returns the outside box. 
         * @method rect
         * @return {ctype.value_type<cc.Rect>}
         */
        public rect (
        ) : ctype.value_type<cc.Rect>;

        /**
         * js ctor
         * @method MenuItem
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class MenuItemLabel
     * @native
     */
    export class MenuItemLabel 
        extends cc.MenuItem
    {
        /* Can't find native property getString implementation of cc::MenuItemLabel. using any type */
        public string : any;
        /**
         *  Gets the label that is rendered. 
         * @method getLabel
         * @return {cc.Node}
         */
        public label : cc.Node;
        /**
         *  Gets the color that will be used to disable the item. 
         * @method getDisabledColor
         * @return {ctype.value_type<cc.Color>}
         */
        public disabledColor : ctype.value_type<cc.Color>;


        /**
         *  Sets the label that is rendered. 
         * @method setLabel
         * @param {cc.Node} _node
         */
        public setLabel (
            _node : cc.Node 
        ) : void;

        /**
         *  Gets the color that will be used to disable the item. 
         * @method getDisabledColor
         * @return {ctype.value_type<cc.Color>}
         */
        public getDisabledColor (
        ) : ctype.value_type<cc.Color>;

        /**
         *  Sets a new string to the inner label. 
         * @method setString
         * @param {string} _label
         */
        public setString (
            _label : string 
        ) : void;

        /**
         *  Initializes a MenuItemLabel with a Label, target and selector. 
         * @method initWithLabel
         * @param {cc.Node} _label
         * @param {(arg0:cc.Ref) => void} _callback
         * @return {boolean}
         */
        public initWithLabel (
            _label : cc.Node, 
            _callback : (arg0:any) => void 
        ) : boolean;

        /**
         *  Sets the color that will be used to disable the item. 
         * @method setDisabledColor
         * @param {ctype.value_type<cc.Color>} _color
         */
        public setDisabledColor (
            _color : ctype.value_type<cc.Color> 
        ) : void;

        /**
         *  Gets the label that is rendered. 
         * @method getLabel
         * @return {cc.Node}
         */
        public getLabel (
        ) : cc.Node;

        /**
         * js ctor
         * @method MenuItemLabel
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class MenuItemAtlasFont
     * @native
     */
    export class MenuItemAtlasFont 
        extends cc.MenuItemLabel
    {

        /**
         *  Initializes a menu item from a string and atlas with a target/selector. 
         * @method initWithString
         * @param {string} _value
         * @param {string} _charMapFile
         * @param {number} _itemWidth
         * @param {number} _itemHeight
         * @param {number} _startCharMap
         * @param {(arg0:cc.Ref) => void} _callback
         * @return {boolean}
         */
        public initWithString (
            _value : string, 
            _charMapFile : string, 
            _itemWidth : number, 
            _itemHeight : number, 
            _startCharMap : number, 
            _callback : (arg0:any) => void 
        ) : boolean;

        /**
         * js ctor
         * @method MenuItemAtlasFont
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class MenuItemFont
     * @native
     */
    export class MenuItemFont 
        extends cc.MenuItemLabel
    {
        /* Can't find native property fontSize implementation of cc::MenuItemFont. using any type */
        public fontSize : any;
        /* Can't find native property fontName implementation of cc::MenuItemFont. using any type */
        public fontName : any;


        /**
         * Set the font name .<br>
         * -- c++ can not overload static and non-static member functions with the same parameter types.<br>
         * -- so change the name to setFontNameObj.<br>
         * -- js setFontName<br>
         * -- js NA
         * @method setFontNameObj
         * @param {string} _name
         */
        public setFontName (
            _name : string 
        ) : void;

        /**
         *  get font size .<br>
         * -- js getFontSize<br>
         * -- js NA
         * @method getFontSizeObj
         * @return {number}
         */
        public getFontSize (
        ) : number;

        /**
         *  Set font size.<br>
         * -- c++ can not overload static and non-static member functions with the same parameter types.<br>
         * -- so change the name to setFontSizeObj.<br>
         * -- js setFontSize<br>
         * -- js NA
         * @method setFontSizeObj
         * @param {number} _size
         */
        public setFontSize (
            _size : number 
        ) : void;

        /**
         *  Initializes a menu item from a string with a target/selector. 
         * @method initWithString
         * @param {string} _value
         * @param {(arg0:cc.Ref) => void} _callback
         * @return {boolean}
         */
        public initWithString (
            _value : string, 
            _callback : (arg0:any) => void 
        ) : boolean;

        /**
         *  Returns the name of the Font.<br>
         * -- js getFontNameObj<br>
         * -- js NA
         * @method getFontNameObj
         * @return {string}
         */
        public getFontName (
        ) : string;

        /**
         *  Set the default font name. 
         * @method setFontName
         * @param {string} _name
         */
        public static setFontName (
            _name : string 
        ) : void;

        /**
         *  Get default font size. 
         * @method getFontSize
         * @return {number}
         */
        public static getFontSize (
        ) : number;

        /**
         *  Get the default font name. 
         * @method getFontName
         * @return {string}
         */
        public static getFontName (
        ) : string;

        /**
         *  Set default font size. 
         * @method setFontSize
         * @param {number} _size
         */
        public static setFontSize (
            _size : number 
        ) : void;

        /**
         * js ctor
         * @method MenuItemFont
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class MenuItemSprite
     * @native
     */
    export class MenuItemSprite 
        extends cc.MenuItem
    {
        /**
         *  Gets the image used when the item is not selected. 
         * @method getNormalImage
         * @return {cc.Node}
         */
        public normalImage : cc.Node;
        /**
         *  Gets the image used when the item is selected. 
         * @method getSelectedImage
         * @return {cc.Node}
         */
        public selectedImage : cc.Node;
        /**
         *  Gets the image used when the item is disabled. 
         * @method getDisabledImage
         * @return {cc.Node}
         */
        public disabledImage : cc.Node;


        /**
         *  Enables or disables the item. 
         * @method setEnabled
         * @param {boolean} _bEnabled
         */
        public setEnabled (
            _bEnabled : boolean 
        ) : void;

        /**
         * The item was selected (not activated), similar to "mouse-over".<br>
         * -- since v0.99.5
         * @method selected
         */
        public selected (
        ) : void;

        /**
         *  Sets the image used when the item is not selected. 
         * @method setNormalImage
         * @param {cc.Node} _image
         */
        public setNormalImage (
            _image : cc.Node 
        ) : void;

        /**
         *  Sets the image used when the item is disabled. 
         * @method setDisabledImage
         * @param {cc.Node} _image
         */
        public setDisabledImage (
            _image : cc.Node 
        ) : void;

        /**
         *  Initializes a menu item with a normal, selected  and disabled image with a callable object. 
         * @method initWithNormalSprite
         * @param {cc.Node} _normalSprite
         * @param {cc.Node} _selectedSprite
         * @param {cc.Node} _disabledSprite
         * @param {(arg0:cc.Ref) => void} _callback
         * @return {boolean}
         */
        public initWithNormalSprite (
            _normalSprite : cc.Node, 
            _selectedSprite : cc.Node, 
            _disabledSprite : cc.Node, 
            _callback : (arg0:any) => void 
        ) : boolean;

        /**
         *  Sets the image used when the item is selected. 
         * @method setSelectedImage
         * @param {cc.Node} _image
         */
        public setSelectedImage (
            _image : cc.Node 
        ) : void;

        /**
         *  Gets the image used when the item is disabled. 
         * @method getDisabledImage
         * @return {cc.Node}
         */
        public getDisabledImage (
        ) : cc.Node;

        /**
         *  Gets the image used when the item is selected. 
         * @method getSelectedImage
         * @return {cc.Node}
         */
        public getSelectedImage (
        ) : cc.Node;

        /**
         *  Gets the image used when the item is not selected. 
         * @method getNormalImage
         * @return {cc.Node}
         */
        public getNormalImage (
        ) : cc.Node;

        /**
         *  The item was unselected. 
         * @method unselected
         */
        public unselected (
        ) : void;

        /**
         * 
         * @method MenuItemSprite
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class MenuItemImage
     * @native
     */
    export class MenuItemImage 
        extends cc.MenuItemSprite
    {

        /**
         *  Sets the sprite frame for the disabled image. 
         * @method setDisabledSpriteFrame
         * @param {cc.SpriteFrame} _frame
         */
        public setDisabledSpriteFrame (
            _frame : cc.SpriteFrame 
        ) : void;

        /**
         *  Sets the sprite frame for the selected image. 
         * @method setSelectedSpriteFrame
         * @param {cc.SpriteFrame} _frame
         */
        public setSelectedSpriteFrame (
            _frame : cc.SpriteFrame 
        ) : void;

        /**
         *  Sets the sprite frame for the normal image. 
         * @method setNormalSpriteFrame
         * @param {cc.SpriteFrame} _frame
         */
        public setNormalSpriteFrame (
            _frame : cc.SpriteFrame 
        ) : void;

        /**
         * 
         * @method init
         * @return {boolean}
         */
        public init (
        ) : boolean;

        /**
         *  Initializes a menu item with a normal, selected  and disabled image with a callable object. 
         * @method initWithNormalImage
         * @param {string} _normalImage
         * @param {string} _selectedImage
         * @param {string} _disabledImage
         * @param {(arg0:cc.Ref) => void} _callback
         * @return {boolean}
         */
        public initWithNormalImage (
            _normalImage : string, 
            _selectedImage : string, 
            _disabledImage : string, 
            _callback : (arg0:any) => void 
        ) : boolean;

        /**
         * js ctor
         * @method MenuItemImage
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class MenuItemToggle
     * @native
     */
    export class MenuItemToggle 
        extends cc.MenuItem
    {

        /**
         *  Sets the array that contains the subitems. 
         * @method setSubItems
         * @param {cc.MenuItem[]} _items
         */
        public setSubItems (
            _items : cc.MenuItem[] 
        ) : void;

        /**
         *  Initializes a menu item with a item. 
         * @method initWithItem
         * @param {cc.MenuItem} _item
         * @return {boolean}
         */
        public initWithItem (
            _item : cc.MenuItem 
        ) : boolean;

        /**
         *  Gets the index of the selected item. 
         * @method getSelectedIndex
         * @return {number}
         */
        public getSelectedIndex (
        ) : number;

        /**
         *  Add more menu item. 
         * @method addSubItem
         * @param {cc.MenuItem} _item
         */
        public addSubItem (
            _item : cc.MenuItem 
        ) : void;

        /**
         *  Return the selected item. 
         * @method getSelectedItem
         * @return {cc.MenuItem}
         */
        public getSelectedItem (
        ) : cc.MenuItem;

        /**
         *  Sets the index of the selected item. 
         * @method setSelectedIndex
         * @param {number} _index
         */
        public setSelectedIndex (
            _index : number 
        ) : void;

        /**
         * js ctor
         * @method MenuItemToggle
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class Menu
     * @native
     */
    export class Menu 
        extends cc.Layer
    {
        /* Can't find native property isOpacityModifyRGB implementation of cc::Menu. using any type */
        public opacityModifyRGB : any;
        /* Can't find native property getOpacity implementation of cc::Menu. using any type */
        public opacity : any;
        /* Can't find native property isCascadeOpacityEnabled implementation of cc::Menu. using any type */
        public cascadeOpacity : any;
        /* Can't find native property getColor implementation of cc::Menu. using any type */
        public color : any;
        /* Can't find native property isCascadeColorEnabled implementation of cc::Menu. using any type */
        public cascadeColor : any;
        /* Can't find native property getEnabled implementation of cc::Menu. using any type */
        public enabled : any;


        /**
         *  initializes a Menu with a NSArray of MenuItem objects 
         * @method initWithArray
         * @param {cc.MenuItem[]} _arrayOfItems
         * @return {boolean}
         */
        public initWithArray (
            _arrayOfItems : cc.MenuItem[] 
        ) : boolean;

        /**
         * Set whether the menu is visible.<br>
         * -- The default value is true, a menu is default to visible.<br>
         * -- param value true if menu is enable, false if menu is disable.
         * @method setEnabled
         * @param {boolean} _value
         */
        public setEnabled (
            _value : boolean 
        ) : void;

        /**
         *  Align items vertically. 
         * @method alignItemsVertically
         */
        public alignItemsVertically (
        ) : void;

        /**
         * Determines if the menu is enable.<br>
         * -- see `setEnabled(bool)`.<br>
         * -- return whether the menu is enabled or not.
         * @method isEnabled
         * @return {boolean}
         */
        public isEnabled (
        ) : boolean;

        /**
         *  Align items horizontally. 
         * @method alignItemsHorizontally
         */
        public alignItemsHorizontally (
        ) : void;

        /**
         *  Align items horizontally with padding.<br>
         * -- since v0.7.2
         * @method alignItemsHorizontallyWithPadding
         * @param {number} _padding
         */
        public alignItemsHorizontallyWithPadding (
            _padding : number 
        ) : void;

        /**
         *  Align items vertically with padding.<br>
         * -- since v0.7.2
         * @method alignItemsVerticallyWithPadding
         * @param {number} _padding
         */
        public alignItemsVerticallyWithPadding (
            _padding : number 
        ) : void;

        /**
         * js ctor
         * @method Menu
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class MotionStreak
     * @native
     */
    export class MotionStreak 
        extends cc.Node
    {
        /* Can't find native property getPositionX implementation of cc::MotionStreak. using any type */
        public x : any;
        /* Can't find native property getPositionY implementation of cc::MotionStreak. using any type */
        public y : any;
        /* Can't find native property isOpacityModifyRGB implementation of cc::MotionStreak. using any type */
        public opacityModifyRGB : any;
        /* Can't find native property getOpacity implementation of cc::MotionStreak. using any type */
        public opacity : any;
        /* Can't find native property isOpacityModifyRGB implementation of cc::MotionStreak. using any type */
        public opacityModifyRGB : any;
        /* Can't find native property getColor implementation of cc::MotionStreak. using any type */
        public color : any;
        /* Can't find native property isCascadeColorEnabled implementation of cc::MotionStreak. using any type */
        public cascadeColor : any;


        /**
         *  Remove all living segments of the ribbon.
         * @method reset
         */
        public reset (
        ) : void;

        /**
         * 
         * @method setTexture
         * @param {cc.Texture2D} _texture
         */
        public setTexture (
            _texture : cc.Texture2D 
        ) : void;

        /**
         * 
         * @method getTexture
         * @return {cc.Texture2D}
         */
        public getTexture (
        ) : cc.Texture2D;

        /**
         *  Color used for the tint.<br>
         * -- param colors The color used for the tint.
         * @method tintWithColor
         * @param {ctype.value_type<cc.Color>} _colors
         */
        public tintWithColor (
            _colors : ctype.value_type<cc.Color> 
        ) : void;

        /**
         * js NA<br>
         * -- lua NA
         * @method setBlendFunc
         * @param {cc.BlendFunc} _blendFunc
         */
        public setBlendFunc (
            _blendFunc : cc.BlendFunc 
        ) : void;

        /**
         *  Sets the starting position initialized or not.<br>
         * -- param bStartingPositionInitialized True if initialized the starting position.
         * @method setStartingPositionInitialized
         * @param {boolean} _bStartingPositionInitialized
         */
        public setStartingPositionInitialized (
            _bStartingPositionInitialized : boolean 
        ) : void;

        /**
         * js NA<br>
         * -- lua NA
         * @method getBlendFunc
         * @return {cc.BlendFunc}
         */
        public getBlendFunc (
        ) : cc.BlendFunc;

        /**
         *  Is the starting position initialized or not.<br>
         * -- return True if the starting position is initialized.
         * @method isStartingPositionInitialized
         * @return {boolean}
         */
        public isStartingPositionInitialized (
        ) : boolean;

        /**
         *  When fast mode is enabled, new points are added faster but with lower precision. <br>
         * -- return True if fast mode is enabled.
         * @method isFastMode
         * @return {boolean}
         */
        public isFastMode (
        ) : boolean;

        /**
         *  Get stroke.<br>
         * -- return float stroke.
         * @method getStroke
         * @return {number}
         */
        public getStroke (
        ) : number;

        /**
         *  initializes a motion streak with fade in seconds, minimum segments, stroke's width, color and texture  
         * @method initWithFade
         * @param {(number)} _fade
         * @param {(number)} _minSeg
         * @param {(number)} _stroke
         * @param {(ctype.value_type<cc.Color>)} _color
         * @param {(cc.Texture2D) | (string)} _texture | path
         * @return {boolean}
         */
        public initWithFade (
            _fade : (number), 
            _minSeg : (number), 
            _stroke : (number), 
            _color : (ctype.value_type<cc.Color>), 
            _texture_path : (cc.Texture2D) | (string)
        ) : boolean;

        /**
         *  Sets fast mode or not.<br>
         * -- param bFastMode True if enabled fast mode.
         * @method setFastMode
         * @param {boolean} _bFastMode
         */
        public setFastMode (
            _bFastMode : boolean 
        ) : void;

        /**
         *  Set stroke.<br>
         * -- param stroke The width of stroke.
         * @method setStroke
         * @param {number} _stroke
         */
        public setStroke (
            _stroke : number 
        ) : void;

        /**
         *  Creates and initializes a motion streak with fade in seconds, minimum segments, stroke's width, color, texture.<br>
         * -- param fade The fade time, in seconds.<br>
         * -- param minSeg The minimum segments.<br>
         * -- param stroke The width of stroke.<br>
         * -- param color The color of stroke.<br>
         * -- param texture The texture name of stoke.<br>
         * -- return An autoreleased MotionStreak object.
         * @method create
         * @param {(number)} _fade
         * @param {(number)} _minSeg
         * @param {(number)} _stroke
         * @param {(ctype.value_type<cc.Color>)} _color
         * @param {(cc.Texture2D) | (string)} _texture | path
         * @return {cc.MotionStreak}
         */
        public static create (
            _fade : (number), 
            _minSeg : (number), 
            _stroke : (number), 
            _color : (ctype.value_type<cc.Color>), 
            _texture_path : (cc.Texture2D) | (string)
        ) : cc.MotionStreak;

        /**
         * 
         * @method MotionStreak
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class NodeGrid
     * @native
     */
    export class NodeGrid 
        extends cc.Node
    {
        /**
         * js NA
         * @method getGrid
         * @return {cc.GridBase}
         */
        public grid : cc.GridBase;


        /**
         *  Set the Grid Target. <br>
         * -- param target A Node is used to set the Grid Target.
         * @method setTarget
         * @param {cc.Node} _target
         */
        public setTarget (
            _target : cc.Node 
        ) : void;

        /**
         * js NA
         * @method getGrid
         * @return {cc.GridBase}
         */
        public getGrid (
        ) : cc.GridBase;

        /**
         * brief Get the effect grid rect.<br>
         * -- return Return the effect grid rect.
         * @method getGridRect
         * @return {ctype.value_type<cc.Rect>}
         */
        public getGridRect (
        ) : ctype.value_type<cc.Rect>;

        /**
         * 
         * @method create
         * @param {(ctype.value_type<cc.Rect>)} _rect?
         * @return {cc.NodeGrid}
         */
        public static create (
            _rect? : (ctype.value_type<cc.Rect>)
        ) : cc.NodeGrid;

        /**
         * 
         * @method NodeGrid
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class ParticleBatchNode
     * @native
     */
    export class ParticleBatchNode 
        extends cc.Node
    {
        /**
         *  Gets the texture atlas used for drawing the quads.
         * return The texture atlas used for drawing the quads.
         * @method getTextureAtlas
         * @return {cc.TextureAtlas}
         */
        public textureAtlas : cc.TextureAtlas;
        /**
         * 
         * @method getTexture
         * @return {cc.Texture2D}
         */
        public texture : cc.Texture2D;


        /**
         * 
         * @method setTexture
         * @param {cc.Texture2D} _texture
         */
        public setTexture (
            _texture : cc.Texture2D 
        ) : void;

        /**
         *  initializes the particle system with Texture2D, a capacity of particles 
         * @method initWithTexture
         * @param {cc.Texture2D} _tex
         * @param {number} _capacity
         * @return {boolean}
         */
        public initWithTexture (
            _tex : cc.Texture2D, 
            _capacity : number 
        ) : boolean;

        /**
         *  Disables a particle by inserting a 0'd quad into the texture atlas.<br>
         * -- param particleIndex The index of the particle.
         * @method disableParticle
         * @param {number} _particleIndex
         */
        public disableParticle (
            _particleIndex : number 
        ) : void;

        /**
         * 
         * @method getTexture
         * @return {cc.Texture2D}
         */
        public getTexture (
        ) : cc.Texture2D;

        /**
         *  Sets the texture atlas used for drawing the quads.<br>
         * -- param atlas The texture atlas used for drawing the quads.
         * @method setTextureAtlas
         * @param {cc.TextureAtlas} _atlas
         */
        public setTextureAtlas (
            _atlas : cc.TextureAtlas 
        ) : void;

        /**
         *  initializes the particle system with the name of a file on disk (for a list of supported formats look at the Texture2D class), a capacity of particles 
         * @method initWithFile
         * @param {string} _fileImage
         * @param {number} _capacity
         * @return {boolean}
         */
        public initWithFile (
            _fileImage : string, 
            _capacity : number 
        ) : boolean;

        /**
         * code<br>
         * -- When this function bound into js or lua,the parameter will be changed<br>
         * -- In js: var setBlendFunc(var src, var dst)<br>
         * -- endcode<br>
         * -- lua NA
         * @method setBlendFunc
         * @param {cc.BlendFunc} _blendFunc
         */
        public setBlendFunc (
            _blendFunc : cc.BlendFunc 
        ) : void;

        /**
         * 
         * @method removeAllChildrenWithCleanup
         * @param {boolean} _doCleanup
         */
        public removeAllChildrenWithCleanup (
            _doCleanup : boolean 
        ) : void;

        /**
         *  Gets the texture atlas used for drawing the quads.<br>
         * -- return The texture atlas used for drawing the quads.
         * @method getTextureAtlas
         * @return {cc.TextureAtlas}
         */
        public getTextureAtlas (
        ) : cc.TextureAtlas;

        /**
         * js NA<br>
         * -- lua NA
         * @method getBlendFunc
         * @return {cc.BlendFunc}
         */
        public getBlendFunc (
        ) : cc.BlendFunc;

        /**
         *  Inserts a child into the ParticleBatchNode.<br>
         * -- param system A given particle system.<br>
         * -- param index The insert index.
         * @method insertChild
         * @param {cc.ParticleSystem} _system
         * @param {number} _index
         */
        public insertChild (
            _system : cc.ParticleSystem, 
            _index : number 
        ) : void;

        /**
         *  Remove a child of the ParticleBatchNode.<br>
         * -- param index The index of the child.<br>
         * -- param doCleanup True if all actions and callbacks on this node should be removed, false otherwise.
         * @method removeChildAtIndex
         * @param {number} _index
         * @param {boolean} _doCleanup
         */
        public removeChildAtIndex (
            _index : number, 
            _doCleanup : boolean 
        ) : void;

        /**
         *  Create the particle system with the name of a file on disk (for a list of supported formats look at the Texture2D class), a capacity of particles.<br>
         * -- param fileImage A given file name.<br>
         * -- param capacity A capacity of particles.<br>
         * -- return An autoreleased ParticleBatchNode object.
         * @method create
         * @param {string} _fileImage
         * @param {number} _capacity
         * @return {cc.ParticleBatchNode}
         */
        public static create (
            _fileImage : string, 
            _capacity : number 
        ) : cc.ParticleBatchNode;

        /**
         *  Create the particle system with Texture2D, a capacity of particles, which particle system to use.<br>
         * -- param tex A given texture.<br>
         * -- param capacity A capacity of particles.<br>
         * -- return An autoreleased ParticleBatchNode object.<br>
         * -- js NA
         * @method createWithTexture
         * @param {cc.Texture2D} _tex
         * @param {number} _capacity
         * @return {cc.ParticleBatchNode}
         */
        public static createWithTexture (
            _tex : cc.Texture2D, 
            _capacity : number 
        ) : cc.ParticleBatchNode;

        /**
         * js ctor
         * @method ParticleBatchNode
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class ParticleData
     * @native
     */
    export class ParticleData 
    {

        /**
         * 
         * @method release
         */
        public release (
        ) : void;

        /**
         * 
         * @method getMaxCount
         * @return {number}
         */
        public getMaxCount (
        ) : number;

        /**
         * 
         * @method init
         * @param {number} _count
         * @return {boolean}
         */
        public init (
            _count : number 
        ) : boolean;

        /**
         * 
         * @method copyParticle
         * @param {number} _p1
         * @param {number} _p2
         */
        public copyParticle (
            _p1 : number, 
            _p2 : number 
        ) : void;

        /**
         * 
         * @method ParticleData
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class ParticleSystem
     * @native
     */
    export class ParticleSystem__ 
        extends cc.Node
    {

        /*
        * Consts taken from jsb_cocos2d_constants manually
        */
        public static TYPE_FREE : number;
        public static TYPE_RELATIVE : number;
        public static TYPE_GROUPED : number;
        public static DURATION_INFINITY : number;
        public static MODE_GRAVITY : number;
        public static MODE_RADIUS : number;
        public static START_SIZE_EQUAL_TO_END_SIZE : number;
        public static START_RADIUS_EQUAL_TO_END_RADIUS : number;
        public static readonly SHAPE_MODE: number;
        public static readonly TEXTURE_MODE: number;
        public static readonly STAR_SHAPE: number;
        public static readonly BALL_SHAPE: number;
        public static readonly TYPE_FREE: number;
        public static readonly TYPE_RELATIVE: number;
        public static readonly TYPE_GROUPED: number;
        public static readonly MODE_GRAVITY: number;
        public static readonly MODE_RADIUS: number;
        /* Can't find native property getRotation implementation of cc::ParticleSystem. using any type */
        public rotation : any;
        /* Can't find native property getScale implementation of cc::ParticleSystem. using any type */
        public scale : any;
        /* Can't find native property getScaleX implementation of cc::ParticleSystem. using any type */
        public scaleX : any;
        /* Can't find native property getScaleY implementation of cc::ParticleSystem. using any type */
        public scaleY : any;
        /* Can't find native property isOpacityModifyRGB implementation of cc::ParticleSystem. using any type */
        public opacityModifyRGB : any;
        /**
         *  Gets the batch node.
         * return The batch node.
         * @method getBatchNode
         * @return {cc.ParticleBatchNode}
         */
        public batchNode : cc.ParticleBatchNode;
        /**
         *  Whether or not the particle system is active.
         * return True if the particle system is active.
         * @method isActive
         * @return {boolean}
         */
        public readonly active : boolean;
        /* Can't find native property getShapeType implementation of cc::ParticleSystem. using any type */
        public shapeType : any;
        /**
         *  Gets the index of system in batch node array.
         * return The index of system in batch node array.
         * @method getAtlasIndex
         * @return {number}
         */
        public atlasIndex : number;
        /**
         *  Gets the Quantity of particles that are being simulated at the moment.
         * return The Quantity of particles that are being simulated at the moment.
         * @method getParticleCount
         * @return {number}
         */
        public particleCount : number;
        /**
         *  Gets how many seconds the emitter will run. -1 means 'forever'.
         * return The seconds that the emitter will run. -1 means 'forever'.
         * @method getDuration
         * @return {number}
         */
        public duration : number;
        /**
         *  Gets the source position of the emitter.
         * return The source position of the emitter.
         * @method getSourcePosition
         * @return {ctype.value_type<cc.Point>}
         */
        public sourcePos : ctype.value_type<cc.Point>;
        /**
         *  Gets the position variance of the emitter.
         * return The position variance of the emitter.
         * @method getPosVar
         * @return {ctype.value_type<cc.Point>}
         */
        public posVar : ctype.value_type<cc.Point>;
        /**
         *  Gets the life of each particle.
         * return The life of each particle.
         * @method getLife
         * @return {number}
         */
        public life : number;
        /**
         *  Gets the life variance of each particle.
         * return The life variance of each particle.
         * @method getLifeVar
         * @return {number}
         */
        public lifeVar : number;
        /**
         *  Gets the angle of each particle. 
         * return The angle of each particle.
         * @method getAngle
         * @return {number}
         */
        public angle : number;
        /**
         *  Gets the angle variance of each particle.
         * return The angle variance of each particle.
         * @method getAngleVar
         * @return {number}
         */
        public angleVar : number;
        /**
         *  Gets the start size in pixels of each particle.
         * return The start size in pixels of each particle.
         * @method getStartSize
         * @return {number}
         */
        public startSize : number;
        /**
         *  Gets the start size variance in pixels of each particle.
         * return The start size variance in pixels of each particle.
         * @method getStartSizeVar
         * @return {number}
         */
        public startSizeVar : number;
        /**
         *  Gets the end size in pixels of each particle.
         * return The end size in pixels of each particle.
         * @method getEndSize
         * @return {number}
         */
        public endSize : number;
        /**
         *  Gets the end size variance in pixels of each particle.
         * return The end size variance in pixels of each particle.
         * @method getEndSizeVar
         * @return {number}
         */
        public endSizeVar : number;
        /**
         *  Gets the start spin of each particle.
         * return The start spin of each particle.
         * @method getStartSpin
         * @return {number}
         */
        public startSpin : number;
        /**
         *  Gets the start spin variance of each particle.
         * return The start spin variance of each particle.
         * @method getStartSpinVar
         * @return {number}
         */
        public startSpinVar : number;
        /**
         *  Gets the end spin of each particle.
         * return The end spin of each particle.
         * @method getEndSpin
         * @return {number}
         */
        public endSpin : number;
        /**
         *  Gets the end spin variance of each particle.
         * return The end spin variance of each particle.
         * @method getEndSpinVar
         * @return {number}
         */
        public endSpinVar : number;
        /**
         *  Gets the gravity.
         * return The gravity.
         * @method getGravity
         * @return {cc.Point}
         */
        public gravity : cc.Point;
        /**
         *  Gets the speed.
         * return The speed.
         * @method getSpeed
         * @return {number}
         */
        public speed : number;
        /**
         *  Gets the speed variance.
         * return The speed variance.
         * @method getSpeedVar
         * @return {number}
         */
        public speedVar : number;
        /**
         *  Gets the tangential acceleration.
         * return The tangential acceleration.
         * @method getTangentialAccel
         * @return {number}
         */
        public tangentialAccel : number;
        /**
         *  Gets the tangential acceleration variance.
         * return The tangential acceleration variance.
         * @method getTangentialAccelVar
         * @return {number}
         */
        public tangentialAccelVar : number;
        /**
         *  Gets the tangential acceleration.
         * return The tangential acceleration.
         * @method getTangentialAccel
         * @return {number}
         */
        public tangentialAccel : number;
        /**
         *  Gets the tangential acceleration variance.
         * return The tangential acceleration variance.
         * @method getTangentialAccelVar
         * @return {number}
         */
        public tangentialAccelVar : number;
        /**
         *  Whether or not the rotation of each particle to its direction.
         * return True if the rotation is the direction.
         * @method getRotationIsDir
         * @return {boolean}
         */
        public rotationIsDir : boolean;
        /**
         *  Gets the start radius.
         * return The start radius.
         * @method getStartRadius
         * @return {number}
         */
        public startRadius : number;
        /**
         *  Gets the start radius variance.
         * return The start radius variance.
         * @method getStartRadiusVar
         * @return {number}
         */
        public startRadiusVar : number;
        /**
         *  Gets the end radius.
         * return The end radius.
         * @method getEndRadius
         * @return {number}
         */
        public endRadius : number;
        /**
         *  Gets the end radius variance.
         * return The end radius variance.
         * @method getEndRadiusVar
         * @return {number}
         */
        public endRadiusVar : number;
        /**
         *  Gets the number of degrees to rotate a particle around the source pos per second.
         * return The number of degrees to rotate a particle around the source pos per second.
         * @method getRotatePerSecond
         * @return {number}
         */
        public rotatePerS : number;
        /**
         *  Gets the rotate per second variance.
         * return The rotate per second variance.
         * @method getRotatePerSecondVar
         * @return {number}
         */
        public rotatePerSVar : number;
        /**
         *  Gets the start color of each particle.
         * return The start color of each particle.
         * @method getStartColor
         * @return {ctype.value_type<cc.Color>}
         */
        public startColor : ctype.value_type<cc.Color>;
        /**
         *  Gets the start color variance of each particle.
         * return The start color variance of each particle.
         * @method getStartColorVar
         * @return {ctype.value_type<cc.Color>}
         */
        public startColorVar : ctype.value_type<cc.Color>;
        /**
         *  Gets the end color and end color variation of each particle.
         * return The end color and end color variation of each particle.
         * @method getEndColor
         * @return {ctype.value_type<cc.Color>}
         */
        public endColor : ctype.value_type<cc.Color>;
        /**
         *  Gets the end color variance of each particle.
         * return The end color variance of each particle.
         * @method getEndColorVar
         * @return {ctype.value_type<cc.Color>}
         */
        public endColorVar : ctype.value_type<cc.Color>;
        /**
         *  Gets the emission rate of the particles.
         * return The emission rate of the particles.
         * @method getEmissionRate
         * @return {number}
         */
        public emissionRate : number;
        /**
         *  Switch between different kind of emitter modes:
         * - kParticleModeGravity: uses gravity, speed, radial and tangential acceleration.
         * - kParticleModeRadius: uses radius movement + rotation.
         * return The mode of the emitter.
         * @method getEmitterMode
         * @return {number}
         */
        public emitterMode : number;
        /**
         *  Gets the particles movement type: Free or Grouped.
         * since v0.8
         * return The particles movement type.
         * @method getPositionType
         * @return {number}
         */
        public positionType : number;
        /**
         *  Gets the maximum particles of the system.
         * return The maximum particles of the system.
         * @method getTotalParticles
         * @return {number}
         */
        public totalParticles : number;
        /* Can't find native property getAutoRemoveOnFinish implementation of cc::ParticleSystem. using any type */
        public autoRemoveOnFinish : any;
        /**
         * 
         * @method getTexture
         * @return {cc.Texture2D}
         */
        public texture : cc.Texture2D;


        /**
         *  Gets the start size variance in pixels of each particle.<br>
         * -- return The start size variance in pixels of each particle.
         * @method getStartSizeVar
         * @return {number}
         */
        public getStartSizeVar (
        ) : number;

        /**
         * 
         * @method getTexture
         * @return {cc.Texture2D}
         */
        public getTexture (
        ) : cc.Texture2D;

        /**
         *  Whether or not the system is full.<br>
         * -- return True if the system is full.
         * @method isFull
         * @return {boolean}
         */
        public isFull (
        ) : boolean;

        /**
         *  Gets the batch node.<br>
         * -- return The batch node.
         * @method getBatchNode
         * @return {cc.ParticleBatchNode}
         */
        public getBatchNode (
        ) : cc.ParticleBatchNode;

        /**
         *  Gets the start color of each particle.<br>
         * -- return The start color of each particle.
         * @method getStartColor
         * @return {ctype.value_type<cc.Color>}
         */
        public getStartColor (
        ) : ctype.value_type<cc.Color>;

        /**
         *  Gets the particles movement type: Free or Grouped.<br>
         * -- since v0.8<br>
         * -- return The particles movement type.
         * @method getPositionType
         * @return {number}
         */
        public getPositionType (
        ) : number;

        /**
         *  Sets the position variance of the emitter.<br>
         * -- param pos The position variance of the emitter.
         * @method setPosVar
         * @param {ctype.value_type<cc.Point>} _pos
         */
        public setPosVar (
            _pos : ctype.value_type<cc.Point> 
        ) : void;

        /**
         *  Gets the end spin of each particle.<br>
         * -- return The end spin of each particle.
         * @method getEndSpin
         * @return {number}
         */
        public getEndSpin (
        ) : number;

        /**
         *  Sets the rotate per second variance.<br>
         * -- param degrees The rotate per second variance.
         * @method setRotatePerSecondVar
         * @param {number} _degrees
         */
        public setRotatePerSecondVar (
            _degrees : number 
        ) : void;

        /**
         *  Gets the start spin variance of each particle.<br>
         * -- return The start spin variance of each particle.
         * @method getStartSpinVar
         * @return {number}
         */
        public getStartSpinVar (
        ) : number;

        /**
         *  Gets the radial acceleration variance.<br>
         * -- return The radial acceleration variance.
         * @method getRadialAccelVar
         * @return {number}
         */
        public getRadialAccelVar (
        ) : number;

        /**
         *  Gets the end size variance in pixels of each particle.<br>
         * -- return The end size variance in pixels of each particle.
         * @method getEndSizeVar
         * @return {number}
         */
        public getEndSizeVar (
        ) : number;

        /**
         *  Sets the tangential acceleration.<br>
         * -- param t The tangential acceleration.
         * @method setTangentialAccel
         * @param {number} _t
         */
        public setTangentialAccel (
            _t : number 
        ) : void;

        /**
         *  Gets the radial acceleration.<br>
         * -- return The radial acceleration.
         * @method getRadialAccel
         * @return {number}
         */
        public getRadialAccel (
        ) : number;

        /**
         *  Sets the start radius.<br>
         * -- param startRadius The start radius.
         * @method setStartRadius
         * @param {number} _startRadius
         */
        public setStartRadius (
            _startRadius : number 
        ) : void;

        /**
         *  Sets the number of degrees to rotate a particle around the source pos per second.<br>
         * -- param degrees The number of degrees to rotate a particle around the source pos per second.
         * @method setRotatePerSecond
         * @param {number} _degrees
         */
        public setRotatePerSecond (
            _degrees : number 
        ) : void;

        /**
         *  Sets the end size in pixels of each particle.<br>
         * -- param endSize The end size in pixels of each particle.
         * @method setEndSize
         * @param {number} _endSize
         */
        public setEndSize (
            _endSize : number 
        ) : void;

        /**
         *  Gets the gravity.<br>
         * -- return The gravity.
         * @method getGravity
         * @return {ctype.value_type<cc.Point>}
         */
        public getGravity (
        ) : ctype.value_type<cc.Point>;

        /**
         *  Gets the tangential acceleration.<br>
         * -- return The tangential acceleration.
         * @method getTangentialAccel
         * @return {number}
         */
        public getTangentialAccel (
        ) : number;

        /**
         *  Sets the end radius.<br>
         * -- param endRadius The end radius.
         * @method setEndRadius
         * @param {number} _endRadius
         */
        public setEndRadius (
            _endRadius : number 
        ) : void;

        /**
         *  Gets the speed.<br>
         * -- return The speed.
         * @method getSpeed
         * @return {number}
         */
        public getSpeed (
        ) : number;

        /**
         *  Gets the angle of each particle. <br>
         * -- return The angle of each particle.
         * @method getAngle
         * @return {number}
         */
        public getAngle (
        ) : number;

        /**
         *  Sets the end color and end color variation of each particle.<br>
         * -- param color The end color and end color variation of each particle.
         * @method setEndColor
         * @param {ctype.value_type<cc.Color>} _color
         */
        public setEndColor (
            _color : ctype.value_type<cc.Color> 
        ) : void;

        /**
         *  Sets the start spin of each particle.<br>
         * -- param spin The start spin of each particle.
         * @method setStartSpin
         * @param {number} _spin
         */
        public setStartSpin (
            _spin : number 
        ) : void;

        /**
         *  Sets how many seconds the emitter will run. -1 means 'forever'.<br>
         * -- param duration The seconds that the emitter will run. -1 means 'forever'.
         * @method setDuration
         * @param {number} _duration
         */
        public setDuration (
            _duration : number 
        ) : void;

        /**
         *  Initializes a system with a fixed number of particles
         * @method initWithTotalParticles
         * @param {number} _numberOfParticles
         * @return {boolean}
         */
        public initWithTotalParticles (
            _numberOfParticles : number 
        ) : boolean;

        /**
         * 
         * @method addParticles
         * @param {number} _count
         */
        public addParticles (
            _count : number 
        ) : void;

        /**
         * 
         * @method setTexture
         * @param {cc.Texture2D} _texture
         */
        public setTexture (
            _texture : cc.Texture2D 
        ) : void;

        /**
         *  Gets the position variance of the emitter.<br>
         * -- return The position variance of the emitter.
         * @method getPosVar
         * @return {ctype.value_type<cc.Point>}
         */
        public getPosVar (
        ) : ctype.value_type<cc.Point>;

        /**
         *  Call the update method with no time..
         * @method updateWithNoTime
         */
        public updateWithNoTime (
        ) : void;

        /**
         *  Whether or not the particle system is blend additive.<br>
         * -- return True if the particle system is blend additive.
         * @method isBlendAdditive
         * @return {boolean}
         */
        public isBlendAdditive (
        ) : boolean;

        /**
         *  Gets the speed variance.<br>
         * -- return The speed variance.
         * @method getSpeedVar
         * @return {number}
         */
        public getSpeedVar (
        ) : number;

        /**
         *  Sets the particles movement type: Free or Grouped.<br>
         * -- since v0.8<br>
         * -- param type The particles movement type.
         * @method setPositionType
         * @param {cc.ParticleSystem::PositionType} _type
         */
        public setPositionType (
            _type : number 
        ) : void;

        /**
         * 
         * @method stopSystem
         */
        public stopSystem (
        ) : void;

        /**
         *  Gets the source position of the emitter.<br>
         * -- return The source position of the emitter.
         * @method getSourcePosition
         * @return {ctype.value_type<cc.Point>}
         */
        public getSourcePosition (
        ) : ctype.value_type<cc.Point>;

        /**
         *  Sets the life variance of each particle.<br>
         * -- param lifeVar The life variance of each particle.
         * @method setLifeVar
         * @param {number} _lifeVar
         */
        public setLifeVar (
            _lifeVar : number 
        ) : void;

        /**
         *  Sets the maximum particles of the system.<br>
         * -- param totalParticles The maximum particles of the system.
         * @method setTotalParticles
         * @param {number} _totalParticles
         */
        public setTotalParticles (
            _totalParticles : number 
        ) : void;

        /**
         *  Sets the end color variance of each particle.<br>
         * -- param color The end color variance of each particle.
         * @method setEndColorVar
         * @param {ctype.value_type<cc.Color>} _color
         */
        public setEndColorVar (
            _color : ctype.value_type<cc.Color> 
        ) : void;

        /**
         *  Gets the index of system in batch node array.<br>
         * -- return The index of system in batch node array.
         * @method getAtlasIndex
         * @return {number}
         */
        public getAtlasIndex (
        ) : number;

        /**
         *  Gets the start size in pixels of each particle.<br>
         * -- return The start size in pixels of each particle.
         * @method getStartSize
         * @return {number}
         */
        public getStartSize (
        ) : number;

        /**
         *  Sets the start spin variance of each particle.<br>
         * -- param pinVar The start spin variance of each particle.
         * @method setStartSpinVar
         * @param {number} _pinVar
         */
        public setStartSpinVar (
            _pinVar : number 
        ) : void;

        /**
         *  Kill all living particles.
         * @method resetSystem
         */
        public resetSystem (
        ) : void;

        /**
         *  Sets the index of system in batch node array.<br>
         * -- param index The index of system in batch node array.
         * @method setAtlasIndex
         * @param {number} _index
         */
        public setAtlasIndex (
            _index : number 
        ) : void;

        /**
         *  Sets the tangential acceleration variance.<br>
         * -- param t The tangential acceleration variance.
         * @method setTangentialAccelVar
         * @param {number} _t
         */
        public setTangentialAccelVar (
            _t : number 
        ) : void;

        /**
         *  Sets the end radius variance.<br>
         * -- param endRadiusVar The end radius variance.
         * @method setEndRadiusVar
         * @param {number} _endRadiusVar
         */
        public setEndRadiusVar (
            _endRadiusVar : number 
        ) : void;

        /**
         *  Gets the end radius.<br>
         * -- return The end radius.
         * @method getEndRadius
         * @return {number}
         */
        public getEndRadius (
        ) : number;

        /**
         *  Whether or not the particle system is active.<br>
         * -- return True if the particle system is active.
         * @method isActive
         * @return {boolean}
         */
        public isActive (
        ) : boolean;

        /**
         *  Sets the radial acceleration variance.<br>
         * -- param t The radial acceleration variance.
         * @method setRadialAccelVar
         * @param {number} _t
         */
        public setRadialAccelVar (
            _t : number 
        ) : void;

        /**
         *  Sets the start size in pixels of each particle.<br>
         * -- param startSize The start size in pixels of each particle.
         * @method setStartSize
         * @param {number} _startSize
         */
        public setStartSize (
            _startSize : number 
        ) : void;

        /**
         *  Sets the speed.<br>
         * -- param speed The speed.
         * @method setSpeed
         * @param {number} _speed
         */
        public setSpeed (
            _speed : number 
        ) : void;

        /**
         *  Gets the start spin of each particle.<br>
         * -- return The start spin of each particle.
         * @method getStartSpin
         * @return {number}
         */
        public getStartSpin (
        ) : number;

        /**
         * 
         * @method getResourceFile
         * @return {string}
         */
        public getResourceFile (
        ) : string;

        /**
         *  Gets the number of degrees to rotate a particle around the source pos per second.<br>
         * -- return The number of degrees to rotate a particle around the source pos per second.
         * @method getRotatePerSecond
         * @return {number}
         */
        public getRotatePerSecond (
        ) : number;

        /**
         *  Sets the mode of the emitter.<br>
         * -- param mode The mode of the emitter.
         * @method setEmitterMode
         * @param {cc.ParticleSystem::Mode} _mode
         */
        public setEmitterMode (
            _mode : number 
        ) : void;

        /**
         *  Gets how many seconds the emitter will run. -1 means 'forever'.<br>
         * -- return The seconds that the emitter will run. -1 means 'forever'.
         * @method getDuration
         * @return {number}
         */
        public getDuration (
        ) : number;

        /**
         *  Sets the source position of the emitter.<br>
         * -- param pos The source position of the emitter.
         * @method setSourcePosition
         * @param {ctype.value_type<cc.Point>} _pos
         */
        public setSourcePosition (
            _pos : ctype.value_type<cc.Point> 
        ) : void;

        /**
         *  Update the verts position data of particle,<br>
         * -- should be overridden by subclasses. 
         * @method updateParticleQuads
         */
        public updateParticleQuads (
        ) : void;

        /**
         *  Gets the end spin variance of each particle.<br>
         * -- return The end spin variance of each particle.
         * @method getEndSpinVar
         * @return {number}
         */
        public getEndSpinVar (
        ) : number;

        /**
         *  Sets the particle system blend additive.<br>
         * -- param value True if the particle system is blend additive.
         * @method setBlendAdditive
         * @param {boolean} _value
         */
        public setBlendAdditive (
            _value : boolean 
        ) : void;

        /**
         *  Sets the life of each particle.<br>
         * -- param life The life of each particle.
         * @method setLife
         * @param {number} _life
         */
        public setLife (
            _life : number 
        ) : void;

        /**
         *  Sets the angle variance of each particle.<br>
         * -- param angleVar The angle variance of each particle.
         * @method setAngleVar
         * @param {number} _angleVar
         */
        public setAngleVar (
            _angleVar : number 
        ) : void;

        /**
         *  Sets the rotation of each particle to its direction.<br>
         * -- param t True if the rotation is the direction.
         * @method setRotationIsDir
         * @param {boolean} _t
         */
        public setRotationIsDir (
            _t : boolean 
        ) : void;

        /**
         *  Sets the end size variance in pixels of each particle.<br>
         * -- param sizeVar The end size variance in pixels of each particle.
         * @method setEndSizeVar
         * @param {number} _sizeVar
         */
        public setEndSizeVar (
            _sizeVar : number 
        ) : void;

        /**
         *  Sets the angle of each particle.<br>
         * -- param angle The angle of each particle.
         * @method setAngle
         * @param {number} _angle
         */
        public setAngle (
            _angle : number 
        ) : void;

        /**
         *  Sets the batch node.<br>
         * -- param batchNode The batch node.
         * @method setBatchNode
         * @param {cc.ParticleBatchNode} _batchNode
         */
        public setBatchNode (
            _batchNode : cc.ParticleBatchNode 
        ) : void;

        /**
         *  Gets the tangential acceleration variance.<br>
         * -- return The tangential acceleration variance.
         * @method getTangentialAccelVar
         * @return {number}
         */
        public getTangentialAccelVar (
        ) : number;

        /**
         *  Switch between different kind of emitter modes:<br>
         * -- - kParticleModeGravity: uses gravity, speed, radial and tangential acceleration.<br>
         * -- - kParticleModeRadius: uses radius movement + rotation.<br>
         * -- return The mode of the emitter.
         * @method getEmitterMode
         * @return {number}
         */
        public getEmitterMode (
        ) : number;

        /**
         *  Sets the end spin variance of each particle.<br>
         * -- param endSpinVar The end spin variance of each particle.
         * @method setEndSpinVar
         * @param {number} _endSpinVar
         */
        public setEndSpinVar (
            _endSpinVar : number 
        ) : void;

        /**
         *  initializes a ParticleSystem from a plist file.<br>
         * -- This plist files can be created manually or with Particle Designer:<br>
         * -- http:particledesigner.71squared.com/<br>
         * -- since v0.99.3
         * @method initWithFile
         * @param {string} _plistFile
         * @return {boolean}
         */
        public initWithFile (
            _plistFile : string 
        ) : boolean;

        /**
         *  Gets the angle variance of each particle.<br>
         * -- return The angle variance of each particle.
         * @method getAngleVar
         * @return {number}
         */
        public getAngleVar (
        ) : number;

        /**
         *  Sets the start color of each particle.<br>
         * -- param color The start color of each particle.
         * @method setStartColor
         * @param {ctype.value_type<cc.Color>} _color
         */
        public setStartColor (
            _color : ctype.value_type<cc.Color> 
        ) : void;

        /**
         *  Gets the rotate per second variance.<br>
         * -- return The rotate per second variance.
         * @method getRotatePerSecondVar
         * @return {number}
         */
        public getRotatePerSecondVar (
        ) : number;

        /**
         *  Gets the end size in pixels of each particle.<br>
         * -- return The end size in pixels of each particle.
         * @method getEndSize
         * @return {number}
         */
        public getEndSize (
        ) : number;

        /**
         *  Gets the life of each particle.<br>
         * -- return The life of each particle.
         * @method getLife
         * @return {number}
         */
        public getLife (
        ) : number;

        /**
         *  Sets the speed variance.<br>
         * -- param speed The speed variance.
         * @method setSpeedVar
         * @param {number} _speed
         */
        public setSpeedVar (
            _speed : number 
        ) : void;

        /**
         *  Set the particle system auto removed it self on finish.<br>
         * -- param var True if the particle system removed self on finish.
         * @method setAutoRemoveOnFinish
         * @param {boolean} _var
         */
        public setAutoRemoveOnFinish (
            _var : boolean 
        ) : void;

        /**
         *  Sets the gravity.<br>
         * -- param g The gravity.
         * @method setGravity
         * @param {ctype.value_type<cc.Point>} _g
         */
        public setGravity (
            _g : ctype.value_type<cc.Point> 
        ) : void;

        /**
         *  Update the VBO verts buffer which does not use batch node,<br>
         * -- should be overridden by subclasses. 
         * @method postStep
         */
        public postStep (
        ) : void;

        /**
         *  Sets the emission rate of the particles.<br>
         * -- param rate The emission rate of the particles.
         * @method setEmissionRate
         * @param {number} _rate
         */
        public setEmissionRate (
            _rate : number 
        ) : void;

        /**
         *  Gets the end color variance of each particle.<br>
         * -- return The end color variance of each particle.
         * @method getEndColorVar
         * @return {ctype.value_type<cc.Color>}
         */
        public getEndColorVar (
        ) : ctype.value_type<cc.Color>;

        /**
         *  Whether or not the rotation of each particle to its direction.<br>
         * -- return True if the rotation is the direction.
         * @method getRotationIsDir
         * @return {boolean}
         */
        public getRotationIsDir (
        ) : boolean;

        /**
         *  Gets the emission rate of the particles.<br>
         * -- return The emission rate of the particles.
         * @method getEmissionRate
         * @return {number}
         */
        public getEmissionRate (
        ) : number;

        /**
         *  Gets the end color and end color variation of each particle.<br>
         * -- return The end color and end color variation of each particle.
         * @method getEndColor
         * @return {ctype.value_type<cc.Color>}
         */
        public getEndColor (
        ) : ctype.value_type<cc.Color>;

        /**
         *  Gets the life variance of each particle.<br>
         * -- return The life variance of each particle.
         * @method getLifeVar
         * @return {number}
         */
        public getLifeVar (
        ) : number;

        /**
         *  Sets the start size variance in pixels of each particle.<br>
         * -- param sizeVar The start size variance in pixels of each particle.
         * @method setStartSizeVar
         * @param {number} _sizeVar
         */
        public setStartSizeVar (
            _sizeVar : number 
        ) : void;

        /**
         *  Gets the start radius.<br>
         * -- return The start radius.
         * @method getStartRadius
         * @return {number}
         */
        public getStartRadius (
        ) : number;

        /**
         *  Gets the Quantity of particles that are being simulated at the moment.<br>
         * -- return The Quantity of particles that are being simulated at the moment.
         * @method getParticleCount
         * @return {number}
         */
        public getParticleCount (
        ) : number;

        /**
         *  Gets the start radius variance.<br>
         * -- return The start radius variance.
         * @method getStartRadiusVar
         * @return {number}
         */
        public getStartRadiusVar (
        ) : number;

        /**
         * js NA<br>
         * -- lua NA
         * @method getBlendFunc
         * @return {cc.BlendFunc}
         */
        public getBlendFunc (
        ) : cc.BlendFunc;

        /**
         *  Sets the start color variance of each particle.<br>
         * -- param color The start color variance of each particle.
         * @method setStartColorVar
         * @param {ctype.value_type<cc.Color>} _color
         */
        public setStartColorVar (
            _color : ctype.value_type<cc.Color> 
        ) : void;

        /**
         *  Sets the end spin of each particle.<br>
         * -- param endSpin The end spin of each particle.
         * @method setEndSpin
         * @param {number} _endSpin
         */
        public setEndSpin (
            _endSpin : number 
        ) : void;

        /**
         *  Sets the radial acceleration.<br>
         * -- param t The radial acceleration.
         * @method setRadialAccel
         * @param {number} _t
         */
        public setRadialAccel (
            _t : number 
        ) : void;

        /**
         *  initializes a particle system from a NSDictionary and the path from where to load the png<br>
         * -- since v2.1
         * @method initWithDictionary
         * @param {(any)} _dictionary
         * @param {(string)} _dirname?
         * @return {boolean}
         */
        public initWithDictionary (
            _dictionary : (any), 
            _dirname? : (string)
        ) : boolean;

        /**
         *  Whether or not the particle system removed self on finish.<br>
         * -- return True if the particle system removed self on finish.
         * @method isAutoRemoveOnFinish
         * @return {boolean}
         */
        public isAutoRemoveOnFinish (
        ) : boolean;

        /**
         *  Gets the maximum particles of the system.<br>
         * -- return The maximum particles of the system.
         * @method getTotalParticles
         * @return {number}
         */
        public getTotalParticles (
        ) : number;

        /**
         *  Sets the start radius variance.<br>
         * -- param startRadiusVar The start radius variance.
         * @method setStartRadiusVar
         * @param {number} _startRadiusVar
         */
        public setStartRadiusVar (
            _startRadiusVar : number 
        ) : void;

        /**
         * code<br>
         * -- When this function bound into js or lua,the parameter will be changed<br>
         * -- In js: var setBlendFunc(var src, var dst)<br>
         * -- In lua: local setBlendFunc(local src, local dst)<br>
         * -- endcode
         * @method setBlendFunc
         * @param {cc.BlendFunc} _blendFunc
         */
        public setBlendFunc (
            _blendFunc : cc.BlendFunc 
        ) : void;

        /**
         *  Gets the end radius variance.<br>
         * -- return The end radius variance.
         * @method getEndRadiusVar
         * @return {number}
         */
        public getEndRadiusVar (
        ) : number;

        /**
         *  Gets the start color variance of each particle.<br>
         * -- return The start color variance of each particle.
         * @method getStartColorVar
         * @return {ctype.value_type<cc.Color>}
         */
        public getStartColorVar (
        ) : ctype.value_type<cc.Color>;

        /**
         *  Creates an initializes a ParticleSystem from a plist file.<br>
         * -- This plist files can be created manually or with Particle Designer:<br>
         * -- http:particledesigner.71squared.com/<br>
         * -- since v2.0<br>
         * -- param plistFile Particle plist file name.<br>
         * -- return An autoreleased ParticleSystem object.
         * @method create
         * @param {string} _plistFile
         * @return {cc.ParticleSystem}
         */
        public static create (
            _plistFile : string 
        ) : cc.ParticleSystem;

        /**
         *  Create a system with a fixed number of particles.<br>
         * -- param numberOfParticles A given number of particles.<br>
         * -- return An autoreleased ParticleSystemQuad object.<br>
         * -- js NA
         * @method createWithTotalParticles
         * @param {number} _numberOfParticles
         * @return {cc.ParticleSystem}
         */
        public static createWithTotalParticles (
            _numberOfParticles : number 
        ) : cc.ParticleSystem;

        /**
         * js ctor
         * @method ParticleSystem
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class ParticleSystemQuad
     * @native
     */
    export class ParticleSystem 
        extends cc.ParticleSystem__
    {

        /**
         *  Sets a new SpriteFrame as particle.<br>
         * -- WARNING: this method is experimental. Use setTextureWithRect instead.<br>
         * -- param spriteFrame A given sprite frame as particle texture.<br>
         * -- since v0.99.4
         * @method setDisplayFrame
         * @param {cc.SpriteFrame} _spriteFrame
         */
        public setDisplayFrame (
            _spriteFrame : cc.SpriteFrame 
        ) : void;

        /**
         *  Sets a new texture with a rect. The rect is in Points.<br>
         * -- since v0.99.4<br>
         * -- js NA<br>
         * -- lua NA<br>
         * -- param texture A given texture.<br>
         * -- 8 @param rect A given rect, in points.
         * @method setTextureWithRect
         * @param {cc.Texture2D} _texture
         * @param {ctype.value_type<cc.Rect>} _rect
         */
        public setTextureWithRect (
            _texture : cc.Texture2D, 
            _rect : ctype.value_type<cc.Rect> 
        ) : void;

        /**
         *  Listen the event that renderer was recreated on Android/WP8.<br>
         * -- js NA<br>
         * -- lua NA<br>
         * -- param event the event that renderer was recreated on Android/WP8.
         * @method listenRendererRecreated
         * @param {cc.EventCustom} _event
         */
        public listenRendererRecreated (
            _event : cc.EventCustom 
        ) : void;

        /**
         *  Creates an initializes a ParticleSystemQuad from a plist file.<br>
         * -- This plist files can be created manually or with Particle Designer.<br>
         * -- param filename Particle plist file name.<br>
         * -- return An autoreleased ParticleSystemQuad object.
         * @method create
         * @param {(string) | (any)} _filename | dictionary?
         * @return {cc.ParticleSystemQuad}
         */
        public static create (
            _filename_dictionary? : (string) | (any)
        ) : cc.ParticleSystem;

        /**
         *  Creates a Particle Emitter with a number of particles.<br>
         * -- param numberOfParticles A given number of particles.<br>
         * -- return An autoreleased ParticleSystemQuad object.
         * @method createWithTotalParticles
         * @param {number} _numberOfParticles
         * @return {cc.ParticleSystem}
         */
        public static createWithTotalParticles (
            _numberOfParticles : number 
        ) : cc.ParticleSystem;

        /**
         * js ctor
         * @method ParticleSystemQuad
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class ParticleFire
     * @native
     */
    export class ParticleFire 
        extends cc.ParticleSystem
    {

        /**
         *  Create a fire particle system.<br>
         * -- return An autoreleased ParticleFire object.
         * @method create
         * @return {cc.ParticleFire}
         */
        public static create (
        ) : cc.ParticleFire;

        /**
         *  Create a fire particle system withe a fixed number of particles.<br>
         * -- param numberOfParticles A given number of particles.<br>
         * -- return An autoreleased ParticleFire object.<br>
         * -- js NA
         * @method createWithTotalParticles
         * @param {number} _numberOfParticles
         * @return {cc.ParticleFire}
         */
        public static createWithTotalParticles (
            _numberOfParticles : number 
        ) : cc.ParticleFire;

        /**
         * js ctor
         * @method ParticleFire
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class ParticleFireworks
     * @native
     */
    export class ParticleFireworks 
        extends cc.ParticleSystem
    {

        /**
         * 
         * @method init
         * @return {boolean}
         */
        public init (
        ) : boolean;

        /**
         * 
         * @method initWithTotalParticles
         * @param {number} _numberOfParticles
         * @return {boolean}
         */
        public initWithTotalParticles (
            _numberOfParticles : number 
        ) : boolean;

        /**
         *  Create a fireworks particle system.<br>
         * -- return An autoreleased ParticleFireworks object.
         * @method create
         * @return {cc.ParticleFireworks}
         */
        public static create (
        ) : cc.ParticleFireworks;

        /**
         *  Create a fireworks particle system withe a fixed number of particles.<br>
         * -- param numberOfParticles A given number of particles.<br>
         * -- return An autoreleased ParticleFireworks object.<br>
         * -- js NA
         * @method createWithTotalParticles
         * @param {number} _numberOfParticles
         * @return {cc.ParticleFireworks}
         */
        public static createWithTotalParticles (
            _numberOfParticles : number 
        ) : cc.ParticleFireworks;

        /**
         * js ctor
         * @method ParticleFireworks
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class ParticleSun
     * @native
     */
    export class ParticleSun 
        extends cc.ParticleSystem
    {

        /**
         * 
         * @method init
         * @return {boolean}
         */
        public init (
        ) : boolean;

        /**
         * 
         * @method initWithTotalParticles
         * @param {number} _numberOfParticles
         * @return {boolean}
         */
        public initWithTotalParticles (
            _numberOfParticles : number 
        ) : boolean;

        /**
         *  Create a sun particle system.<br>
         * -- return An autoreleased ParticleSun object.
         * @method create
         * @return {cc.ParticleSun}
         */
        public static create (
        ) : cc.ParticleSun;

        /**
         *  Create a sun particle system withe a fixed number of particles.<br>
         * -- param numberOfParticles A given number of particles.<br>
         * -- return An autoreleased ParticleSun object.<br>
         * -- js NA
         * @method createWithTotalParticles
         * @param {number} _numberOfParticles
         * @return {cc.ParticleSun}
         */
        public static createWithTotalParticles (
            _numberOfParticles : number 
        ) : cc.ParticleSun;

        /**
         * js ctor
         * @method ParticleSun
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class ParticleGalaxy
     * @native
     */
    export class ParticleGalaxy 
        extends cc.ParticleSystem
    {

        /**
         * 
         * @method init
         * @return {boolean}
         */
        public init (
        ) : boolean;

        /**
         * 
         * @method initWithTotalParticles
         * @param {number} _numberOfParticles
         * @return {boolean}
         */
        public initWithTotalParticles (
            _numberOfParticles : number 
        ) : boolean;

        /**
         *  Create a galaxy particle system.<br>
         * -- return An autoreleased ParticleGalaxy object.
         * @method create
         * @return {cc.ParticleGalaxy}
         */
        public static create (
        ) : cc.ParticleGalaxy;

        /**
         *  Create a galaxy particle system withe a fixed number of particles.<br>
         * -- param numberOfParticles A given number of particles.<br>
         * -- return An autoreleased ParticleGalaxy object.<br>
         * -- js NA
         * @method createWithTotalParticles
         * @param {number} _numberOfParticles
         * @return {cc.ParticleGalaxy}
         */
        public static createWithTotalParticles (
            _numberOfParticles : number 
        ) : cc.ParticleGalaxy;

        /**
         * js ctor
         * @method ParticleGalaxy
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class ParticleFlower
     * @native
     */
    export class ParticleFlower 
        extends cc.ParticleSystem
    {

        /**
         * 
         * @method init
         * @return {boolean}
         */
        public init (
        ) : boolean;

        /**
         * 
         * @method initWithTotalParticles
         * @param {number} _numberOfParticles
         * @return {boolean}
         */
        public initWithTotalParticles (
            _numberOfParticles : number 
        ) : boolean;

        /**
         *  Create a flower particle system.<br>
         * -- return An autoreleased ParticleFlower object.
         * @method create
         * @return {cc.ParticleFlower}
         */
        public static create (
        ) : cc.ParticleFlower;

        /**
         *  Create a flower particle system withe a fixed number of particles.<br>
         * -- param numberOfParticles A given number of particles.<br>
         * -- return An autoreleased ParticleFlower object.<br>
         * -- js NA
         * @method createWithTotalParticles
         * @param {number} _numberOfParticles
         * @return {cc.ParticleFlower}
         */
        public static createWithTotalParticles (
            _numberOfParticles : number 
        ) : cc.ParticleFlower;

        /**
         * js ctor
         * @method ParticleFlower
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class ParticleMeteor
     * @native
     */
    export class ParticleMeteor 
        extends cc.ParticleSystem
    {

        /**
         * 
         * @method init
         * @return {boolean}
         */
        public init (
        ) : boolean;

        /**
         * 
         * @method initWithTotalParticles
         * @param {number} _numberOfParticles
         * @return {boolean}
         */
        public initWithTotalParticles (
            _numberOfParticles : number 
        ) : boolean;

        /**
         *  Create a meteor particle system.<br>
         * -- return An autoreleased ParticleMeteor object.
         * @method create
         * @return {cc.ParticleMeteor}
         */
        public static create (
        ) : cc.ParticleMeteor;

        /**
         *  Create a meteor particle system withe a fixed number of particles.<br>
         * -- param numberOfParticles A given number of particles.<br>
         * -- return An autoreleased ParticleMeteor object.<br>
         * -- js NA
         * @method createWithTotalParticles
         * @param {number} _numberOfParticles
         * @return {cc.ParticleMeteor}
         */
        public static createWithTotalParticles (
            _numberOfParticles : number 
        ) : cc.ParticleMeteor;

        /**
         * js ctor
         * @method ParticleMeteor
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class ParticleSpiral
     * @native
     */
    export class ParticleSpiral 
        extends cc.ParticleSystem
    {

        /**
         * 
         * @method init
         * @return {boolean}
         */
        public init (
        ) : boolean;

        /**
         * 
         * @method initWithTotalParticles
         * @param {number} _numberOfParticles
         * @return {boolean}
         */
        public initWithTotalParticles (
            _numberOfParticles : number 
        ) : boolean;

        /**
         *  Create a spiral particle system.<br>
         * -- return An autoreleased ParticleSpiral object.
         * @method create
         * @return {cc.ParticleSpiral}
         */
        public static create (
        ) : cc.ParticleSpiral;

        /**
         *  Create a spiral particle system withe a fixed number of particles.<br>
         * -- param numberOfParticles A given number of particles.<br>
         * -- return An autoreleased ParticleSpiral object.<br>
         * -- js NA
         * @method createWithTotalParticles
         * @param {number} _numberOfParticles
         * @return {cc.ParticleSpiral}
         */
        public static createWithTotalParticles (
            _numberOfParticles : number 
        ) : cc.ParticleSpiral;

        /**
         * js ctor
         * @method ParticleSpiral
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class ParticleExplosion
     * @native
     */
    export class ParticleExplosion 
        extends cc.ParticleSystem
    {

        /**
         * 
         * @method init
         * @return {boolean}
         */
        public init (
        ) : boolean;

        /**
         * 
         * @method initWithTotalParticles
         * @param {number} _numberOfParticles
         * @return {boolean}
         */
        public initWithTotalParticles (
            _numberOfParticles : number 
        ) : boolean;

        /**
         *  Create a explosion particle system.<br>
         * -- return An autoreleased ParticleExplosion object.
         * @method create
         * @return {cc.ParticleExplosion}
         */
        public static create (
        ) : cc.ParticleExplosion;

        /**
         *  Create a explosion particle system withe a fixed number of particles.<br>
         * -- param numberOfParticles A given number of particles.<br>
         * -- return An autoreleased ParticleExplosion object.<br>
         * -- js NA
         * @method createWithTotalParticles
         * @param {number} _numberOfParticles
         * @return {cc.ParticleExplosion}
         */
        public static createWithTotalParticles (
            _numberOfParticles : number 
        ) : cc.ParticleExplosion;

        /**
         * js ctor
         * @method ParticleExplosion
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class ParticleSmoke
     * @native
     */
    export class ParticleSmoke 
        extends cc.ParticleSystem
    {

        /**
         * 
         * @method init
         * @return {boolean}
         */
        public init (
        ) : boolean;

        /**
         * 
         * @method initWithTotalParticles
         * @param {number} _numberOfParticles
         * @return {boolean}
         */
        public initWithTotalParticles (
            _numberOfParticles : number 
        ) : boolean;

        /**
         *  Create a smoke particle system.<br>
         * -- return An autoreleased ParticleSmoke object.
         * @method create
         * @return {cc.ParticleSmoke}
         */
        public static create (
        ) : cc.ParticleSmoke;

        /**
         *  Create a smoke particle system withe a fixed number of particles.<br>
         * -- param numberOfParticles A given number of particles.<br>
         * -- return An autoreleased ParticleSmoke object.<br>
         * -- js NA
         * @method createWithTotalParticles
         * @param {number} _numberOfParticles
         * @return {cc.ParticleSmoke}
         */
        public static createWithTotalParticles (
            _numberOfParticles : number 
        ) : cc.ParticleSmoke;

        /**
         * js ctor
         * @method ParticleSmoke
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class ParticleSnow
     * @native
     */
    export class ParticleSnow 
        extends cc.ParticleSystem
    {

        /**
         * 
         * @method init
         * @return {boolean}
         */
        public init (
        ) : boolean;

        /**
         * 
         * @method initWithTotalParticles
         * @param {number} _numberOfParticles
         * @return {boolean}
         */
        public initWithTotalParticles (
            _numberOfParticles : number 
        ) : boolean;

        /**
         *  Create a snow particle system.<br>
         * -- return An autoreleased ParticleSnow object.
         * @method create
         * @return {cc.ParticleSnow}
         */
        public static create (
        ) : cc.ParticleSnow;

        /**
         *  Create a snow particle system withe a fixed number of particles.<br>
         * -- param numberOfParticles A given number of particles.<br>
         * -- return An autoreleased ParticleSnow object.<br>
         * -- js NA
         * @method createWithTotalParticles
         * @param {number} _numberOfParticles
         * @return {cc.ParticleSnow}
         */
        public static createWithTotalParticles (
            _numberOfParticles : number 
        ) : cc.ParticleSnow;

        /**
         * js ctor
         * @method ParticleSnow
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class ParticleRain
     * @native
     */
    export class ParticleRain 
        extends cc.ParticleSystem
    {

        /**
         * 
         * @method init
         * @return {boolean}
         */
        public init (
        ) : boolean;

        /**
         * 
         * @method initWithTotalParticles
         * @param {number} _numberOfParticles
         * @return {boolean}
         */
        public initWithTotalParticles (
            _numberOfParticles : number 
        ) : boolean;

        /**
         *  Create a rain particle system.<br>
         * -- return An autoreleased ParticleRain object.
         * @method create
         * @return {cc.ParticleRain}
         */
        public static create (
        ) : cc.ParticleRain;

        /**
         *  Create a rain particle system withe a fixed number of particles.<br>
         * -- param numberOfParticles A given number of particles.<br>
         * -- return An autoreleased ParticleRain object.<br>
         * -- js NA
         * @method createWithTotalParticles
         * @param {number} _numberOfParticles
         * @return {cc.ParticleRain}
         */
        public static createWithTotalParticles (
            _numberOfParticles : number 
        ) : cc.ParticleRain;

        /**
         * js ctor
         * @method ParticleRain
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class ProgressTimer
     * @native
     */
    export class ProgressTimer 
        extends cc.Node
    {
        public static TYPE_BAR: number;
        public static TYPE_RADIAL: number;
        
        public static readonly TEXTURE_COORDS_COUNT: number;
        /* Can't find native property getOpacity implementation of cc::ProgressTimer. using any type */
        public opacity : any;
        /* Can't find native property isOpacityModifyRGB implementation of cc::ProgressTimer. using any type */
        public opacityModifyRGB : any;
        /* Can't find native property getColor implementation of cc::ProgressTimer. using any type */
        public color : any;
        /**
         *  Returns the Midpoint. 
         * return A Vec2.
         * @method getMidpoint
         * @return {cc.Point}
         */
        public midPoint : cc.Point;
        /**
         *  Returns the BarChangeRate.
         * return A barChangeRate.
         * @method getBarChangeRate
         * @return {cc.Point}
         */
        public barChangeRate : cc.Point;
        /**
         *  Change the percentage to change progress. 
         * return A Type
         * @method getType
         * @return {number}
         */
        public type : number;
        /**
         *  Percentages are from 0 to 100.
         * return Percentages.
         * @method getPercentage
         * @return {number}
         */
        public percentage : number;
        /**
         *  The image to show the progress percentage, retain. 
         * return A sprite.
         * @method getSprite
         * @return {cc.Sprite}
         */
        public sprite : cc.Sprite;
        /**
         *  Return the Reverse direction. 
         * return If the direction is Anti-clockwise,it will return true.
         * @method isReverseDirection
         * @return {boolean}
         */
        public reverseDir : boolean;


        /**
         *  Initializes a progress timer with the sprite as the shape the timer goes through 
         * @method initWithSprite
         * @param {cc.Sprite} _sp
         * @return {boolean}
         */
        public initWithSprite (
            _sp : cc.Sprite 
        ) : boolean;

        /**
         *  Return the Reverse direction. <br>
         * -- return If the direction is Anti-clockwise,it will return true.
         * @method isReverseDirection
         * @return {boolean}
         */
        public isReverseDirection (
        ) : boolean;

        /**
         * This allows the bar type to move the component at a specific rate.<br>
         * -- Set the component to 0 to make sure it stays at 100%.<br>
         * -- For example you want a left to right bar but not have the height stay 100%.<br>
         * -- Set the rate to be Vec2(0,1); and set the midpoint to = Vec2(0,.5f).<br>
         * -- param barChangeRate A Vec2.
         * @method setBarChangeRate
         * @param {ctype.value_type<cc.Point>} _barChangeRate
         */
        public setBarChangeRate (
            _barChangeRate : ctype.value_type<cc.Point> 
        ) : void;

        /**
         *  Percentages are from 0 to 100.<br>
         * -- return Percentages.
         * @method getPercentage
         * @return {number}
         */
        public getPercentage (
        ) : number;

        /**
         *  Set the sprite as the shape. <br>
         * -- param sprite The sprite as the shape.
         * @method setSprite
         * @param {cc.Sprite} _sprite
         */
        public setSprite (
            _sprite : cc.Sprite 
        ) : void;

        /**
         *  Change the percentage to change progress. <br>
         * -- return A Type
         * @method getType
         * @return {number}
         */
        public getType (
        ) : number;

        /**
         *  The image to show the progress percentage, retain. <br>
         * -- return A sprite.
         * @method getSprite
         * @return {cc.Sprite}
         */
        public getSprite (
        ) : cc.Sprite;

        /**
         * Midpoint is used to modify the progress start position.<br>
         * -- If you're using radials type then the midpoint changes the center point.<br>
         * -- If you're using bar type the the midpoint changes the bar growth.<br>
         * -- it expands from the center but clamps to the sprites edge so:<br>
         * -- you want a left to right then set the midpoint all the way to Vec2(0,y).<br>
         * -- you want a right to left then set the midpoint all the way to Vec2(1,y).<br>
         * -- you want a bottom to top then set the midpoint all the way to Vec2(x,0).<br>
         * -- you want a top to bottom then set the midpoint all the way to Vec2(x,1).<br>
         * -- param point A Vec2 point.
         * @method setMidpoint
         * @param {ctype.value_type<cc.Point>} _point
         */
        public setMidpoint (
            _point : ctype.value_type<cc.Point> 
        ) : void;

        /**
         *  Returns the BarChangeRate.<br>
         * -- return A barChangeRate.
         * @method getBarChangeRate
         * @return {ctype.value_type<cc.Point>}
         */
        public getBarChangeRate (
        ) : ctype.value_type<cc.Point>;

        /**
         *  Set the Reverse direction.<br>
         * -- param value If value is false it will clockwise,if is true it will Anti-clockwise.
         * @method setReverseDirection
         * @param {(boolean)} _value | reverse
         */
        public setReverseDirection (
            _value_reverse : (boolean)
        ) : void;

        /**
         *  Returns the Midpoint. <br>
         * -- return A Vec2.
         * @method getMidpoint
         * @return {ctype.value_type<cc.Point>}
         */
        public getMidpoint (
        ) : ctype.value_type<cc.Point>;

        /**
         *  Set the initial percentage values. <br>
         * -- param percentage The initial percentage values.
         * @method setPercentage
         * @param {number} _percentage
         */
        public setPercentage (
            _percentage : number 
        ) : void;

        /**
         *  Set the ProgressTimer type. <br>
         * -- param type Is an Type.
         * @method setType
         * @param {cc.ProgressTimer::Type} _type
         */
        public setType (
            _type : number 
        ) : void;

        /**
         *  Creates a progress timer with the sprite as the shape the timer goes through.<br>
         * -- param sp The sprite as the shape the timer goes through.<br>
         * -- return A ProgressTimer.
         * @method create
         * @param {cc.Sprite} _sp
         * @return {cc.ProgressTimer}
         */
        public static create (
            _sp : cc.Sprite 
        ) : cc.ProgressTimer;

        /**
         * js ctor
         * @method ProgressTimer
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class ProtectedNode
     * @native
     */
    export class ProtectedNode 
        extends cc.Node
    {

        /**
         * Adds a child to the container with a local z-order.<br>
         * -- If the child is added to a 'running' node, then 'onEnter' and 'onEnterTransitionDidFinish' will be called immediately.<br>
         * -- param child     A child node<br>
         * -- param localZOrder    Z order for drawing priority. Please refer to `setLocalZOrder(int)`
         * @method addProtectedChild
         * @param {(cc.Node)} _child
         * @param {(number)} _localZOrder?
         * @param {(number)} _tag?
         */
        public addProtectedChild (
            _child : (cc.Node), 
            _localZOrder? : (number), 
            _tag? : (number)
        ) : void;

        /**
         * 
         * @method disableCascadeColor
         */
        public disableCascadeColor (
        ) : void;

        /**
         * Removes a child from the container by tag value. It will also cleanup all running actions depending on the cleanup parameter.<br>
         * -- param tag       An integer number that identifies a child node.<br>
         * -- param cleanup   true if all running actions and callbacks on the child node will be cleanup, false otherwise.
         * @method removeProtectedChildByTag
         * @param {number} _tag
         * @param {boolean} _cleanup
         */
        public removeProtectedChildByTag (
            _tag : number, 
            _cleanup : boolean 
        ) : void;

        /**
         * Reorders a child according to a new z value.<br>
         * -- param child     An already added child node. It MUST be already added.<br>
         * -- param localZOrder Z order for drawing priority. Please refer to setLocalZOrder(int)
         * @method reorderProtectedChild
         * @param {cc.Node} _child
         * @param {number} _localZOrder
         */
        public reorderProtectedChild (
            _child : cc.Node, 
            _localZOrder : number 
        ) : void;

        /**
         * Removes all children from the container, and do a cleanup to all running actions depending on the cleanup parameter.<br>
         * -- param cleanup   true if all running actions on all children nodes should be cleanup, false otherwise.<br>
         * -- js removeAllChildren<br>
         * -- lua removeAllChildren
         * @method removeAllProtectedChildrenWithCleanup
         * @param {boolean} _cleanup
         */
        public removeAllProtectedChildrenWithCleanup (
            _cleanup : boolean 
        ) : void;

        /**
         * 
         * @method disableCascadeOpacity
         */
        public disableCascadeOpacity (
        ) : void;

        /**
         * Sorts the children array once before drawing, instead of every time when a child is added or reordered.<br>
         * -- This approach can improves the performance massively.<br>
         * -- note Don't call this manually unless a child added needs to be removed in the same frame
         * @method sortAllProtectedChildren
         */
        public sortAllProtectedChildren (
        ) : void;

        /**
         * Gets a child from the container with its tag.<br>
         * -- param tag   An identifier to find the child node.<br>
         * -- return a Node object whose tag equals to the input parameter.
         * @method getProtectedChildByTag
         * @param {number} _tag
         * @return {cc.Node}
         */
        public getProtectedChildByTag (
            _tag : number 
        ) : cc.Node;

        /**
         * Removes a child from the container. It will also cleanup all running actions depending on the cleanup parameter.<br>
         * -- param child     The child node which will be removed.<br>
         * -- param cleanup   true if all running actions and callbacks on the child node will be cleanup, false otherwise.
         * @method removeProtectedChild
         * @param {cc.Node} _child
         * @param {boolean} _cleanup
         */
        public removeProtectedChild (
            _child : cc.Node, 
            _cleanup : boolean 
        ) : void;

        /**
         * Removes all children from the container with a cleanup.<br>
         * -- see `removeAllChildrenWithCleanup(bool)`.
         * @method removeAllProtectedChildren
         */
        public removeAllProtectedChildren (
        ) : void;

        /**
         * Creates a ProtectedNode with no argument.<br>
         * -- return A instance of ProtectedNode.
         * @method create
         * @return {cc.ProtectedNode}
         */
        public static create (
        ) : cc.ProtectedNode;

        /**
         * 
         * @method ProtectedNode
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class Sprite
     * @native
     */
    export class Sprite 
        extends cc.Node
    {
        /* Can't find native property isIgnoreAnchorPointForPosition implementation of cc::Sprite. using any type */
        public ignoreAnchor : any;
        /* Can't find native property getDirty implementation of cc::Sprite. using any type */
        public dirty : any;
        /**
         * Returns the flag which indicates whether the sprite is flipped horizontally or not.
         * It only flips the texture of the sprite, and not the texture of the sprite's children.
         * Also, flipping the texture doesn't alter the anchorPoint.
         * If you want to flip the anchorPoint too, and/or to flip the children too use:
         * sprite->setScaleX(sprite->getScaleX() * -1);
         * return true if the sprite is flipped horizontally, false otherwise.
         * @method isFlippedX
         * @return {boolean}
         */
        public flippedX : boolean;
        /**
         * Return the flag which indicates whether the sprite is flipped vertically or not.
         * It only flips the texture of the sprite, and not the texture of the sprite's children.
         * Also, flipping the texture doesn't alter the anchorPoint.
         * If you want to flip the anchorPoint too, and/or to flip the children too use:
         * sprite->setScaleY(sprite->getScaleY() * -1);
         * return true if the sprite is flipped vertically, false otherwise.
         * @method isFlippedY
         * @return {boolean}
         */
        public flippedY : boolean;
        /* Can't find native property _getOffsetX implementation of cc::Sprite. using any type */
        public readonly offsetX : any;
        /* Can't find native property _getOffsetY implementation of cc::Sprite. using any type */
        public readonly offsetY : any;
        /**
         * Returns the index used on the TextureAtlas.
         * @method getAtlasIndex
         * @return {number}
         */
        public atlasIndex : number;
        /**
         *  Returns the Texture2D object used by the sprite. 
         * @method getTexture
         * @return {cc.Texture2D}
         */
        public texture : cc.Texture2D;
        /**
         * Returns whether or not the texture rectangle is rotated.
         * @method isTextureRectRotated
         * @return {boolean}
         */
        public readonly textureRectRotated : boolean;
        /**
         * Gets the weak reference of the TextureAtlas when the sprite is rendered using via SpriteBatchNode.
         * @method getTextureAtlas
         * @return {cc.TextureAtlas}
         */
        public textureAtlas : cc.TextureAtlas;
        /**
         * Returns the batch node object if this sprite is rendered by SpriteBatchNode.
         * return The SpriteBatchNode object if this sprite is rendered by SpriteBatchNode,
         * nullptr if the sprite isn't used batch node.
         * @method getBatchNode
         * @return {cc.SpriteBatchNode}
         */
        public batchNode : cc.SpriteBatchNode;
        /* Can't find native property getQuad implementation of cc::Sprite. using any type */
        public readonly quad : any;
        /* Can't find native property getOpacity implementation of cc::Sprite. using any type */
        public opacity : any;
        /* Can't find native property isOpacityModifyRGB implementation of cc::Sprite. using any type */
        public opacityModifyRGB : any;
        /* Can't find native property isCascadeOpacityEnabled implementation of cc::Sprite. using any type */
        public cascadeOpacity : any;
        /* Can't find native property getColor implementation of cc::Sprite. using any type */
        public color : any;
        /* Can't find native property isCascadeColorEnabled implementation of cc::Sprite. using any type */
        public cascadeColor : any;


        /**
         * 
         * @method setSpriteFrame
         * @param {(cc.SpriteFrame) | (string)} _newFrame | spriteFrameName
         */
        public setSpriteFrame (
            _newFrame_spriteFrameName : (cc.SpriteFrame) | (string)
        ) : void;

        /**
         *  @overload<br>
         * -- The Texture's rect is not changed.
         * @method setTexture
         * @param {(cc.Texture2D) | (string)} _texture | filename
         */
        public setTexture (
            _texture_filename : (cc.Texture2D) | (string)
        ) : void;

        /**
         *  Returns the Texture2D object used by the sprite. 
         * @method getTexture
         * @return {cc.Texture2D}
         */
        public getTexture (
        ) : cc.Texture2D;

        /**
         * Sets whether the sprite should be flipped vertically or not.<br>
         * -- param flippedY true if the sprite should be flipped vertically, false otherwise.
         * @method setFlippedY
         * @param {boolean} _flippedY
         */
        public setFlippedY (
            _flippedY : boolean 
        ) : void;

        /**
         * Sets whether the sprite should be flipped horizontally or not.<br>
         * -- param flippedX true if the sprite should be flipped horizontally, false otherwise.
         * @method setFlippedX
         * @param {boolean} _flippedX
         */
        public setFlippedX (
            _flippedX : boolean 
        ) : void;

        /**
         * 
         * @method setRotationSkewX
         * @param {number} _rotationX
         */
        public setRotationSkewX (
            _rotationX : number 
        ) : void;

        /**
         * 
         * @method setRotationSkewY
         * @param {number} _rotationY
         */
        public setRotationSkewY (
            _rotationY : number 
        ) : void;

        /**
         * / @}
         * @method getResourceType
         * @return {number}
         */
        public getResourceType (
        ) : number;

        /**
         * Initializes a sprite with a texture and a rect.<br>
         * -- After initialization, the offset will be (0,0).<br>
         * -- param   texture    A pointer to an existing Texture2D object.<br>
         * -- You can use a Texture2D object for many sprites.<br>
         * -- param   rect        Only the contents inside rect of this texture will be applied for this sprite.<br>
         * -- return  True if the sprite is initialized properly, false otherwise.
         * @method initWithTexture
         * @param {(cc.Texture2D)} _texture
         * @param {(ctype.value_type<cc.Rect>)} _rect?
         * @param {(boolean)} _rotated?
         * @return {boolean}
         */
        public initWithTexture (
            _texture : (cc.Texture2D), 
            _rect? : (ctype.value_type<cc.Rect>), 
            _rotated? : (boolean)
        ) : boolean;

        /**
         * Returns the batch node object if this sprite is rendered by SpriteBatchNode.<br>
         * -- return The SpriteBatchNode object if this sprite is rendered by SpriteBatchNode,<br>
         * -- nullptr if the sprite isn't used batch node.
         * @method getBatchNode
         * @return {cc.SpriteBatchNode}
         */
        public getBatchNode (
        ) : cc.SpriteBatchNode;

        /**
         * Gets the offset position of the sprite. Calculated automatically by editors like Zwoptex.
         * @method getOffsetPosition
         * @return {ctype.value_type<cc.Point>}
         */
        public getOffsetPosition (
        ) : ctype.value_type<cc.Point>;

        /**
         * 
         * @method removeAllChildrenWithCleanup
         * @param {boolean} _cleanup
         */
        public removeAllChildrenWithCleanup (
            _cleanup : boolean 
        ) : void;

        /**
         *  @overload<br>
         * -- It will update the texture coordinates and the vertex rectangle.
         * @method setTextureRect
         * @param {(ctype.value_type<cc.Rect>)} _rect
         * @param {(boolean)} _rotated?
         * @param {(ctype.value_type<cc.Size>)} _untrimmedSize?
         */
        public setTextureRect (
            _rect : (ctype.value_type<cc.Rect>), 
            _rotated? : (boolean), 
            _untrimmedSize? : (ctype.value_type<cc.Size>)
        ) : void;

        /**
         * Initializes a sprite with an sprite frame name.<br>
         * -- A SpriteFrame will be fetched from the SpriteFrameCache by name.<br>
         * -- If the SpriteFrame doesn't exist it will raise an exception.<br>
         * -- param   spriteFrameName  A key string that can fected a valid SpriteFrame from SpriteFrameCache.<br>
         * -- return  True if the sprite is initialized properly, false otherwise.
         * @method initWithSpriteFrameName
         * @param {string} _spriteFrameName
         * @return {boolean}
         */
        public initWithSpriteFrameName (
            _spriteFrameName : string 
        ) : boolean;

        /**
         * Returns whether or not a SpriteFrame is being displayed.
         * @method isFrameDisplayed
         * @param {cc.SpriteFrame} _frame
         * @return {boolean}
         */
        public isFrameDisplayed (
            _frame : cc.SpriteFrame 
        ) : boolean;

        /**
         * Returns the index used on the TextureAtlas.
         * @method getAtlasIndex
         * @return {number}
         */
        public getAtlasIndex (
        ) : number;

        /**
         * Sets the batch node to sprite.<br>
         * -- warning This method is not recommended for game developers. Sample code for using batch node<br>
         * -- code<br>
         * -- SpriteBatchNode *batch = SpriteBatchNode::create("Images/grossini_dance_atlas.png", 15);<br>
         * -- Sprite *sprite = Sprite::createWithTexture(batch->getTexture(), Rect(0, 0, 57, 57));<br>
         * -- batch->addChild(sprite);<br>
         * -- layer->addChild(batch);<br>
         * -- endcode
         * @method setBatchNode
         * @param {cc.SpriteBatchNode} _spriteBatchNode
         */
        public setBatchNode (
            _spriteBatchNode : cc.SpriteBatchNode 
        ) : void;

        /**
         * js  NA<br>
         * -- lua NA
         * @method getBlendFunc
         * @return {cc.BlendFunc}
         */
        public getBlendFunc (
        ) : cc.BlendFunc;

        /**
         * / @{/ @name Animation methods<br>
         * -- Changes the display frame with animation name and index.<br>
         * -- The animation name will be get from the AnimationCache.
         * @method setDisplayFrameWithAnimationName
         * @param {string} _animationName
         * @param {number} _frameIndex
         */
        public setDisplayFrameWithAnimationName (
            _animationName : string, 
            _frameIndex : number 
        ) : void;

        /**
         * Sets the weak reference of the TextureAtlas when the sprite is rendered using via SpriteBatchNode.
         * @method setTextureAtlas
         * @param {cc.TextureAtlas} _textureAtlas
         */
        public setTextureAtlas (
            _textureAtlas : cc.TextureAtlas 
        ) : void;

        /**
         * Returns the current displayed frame.
         * @method getSpriteFrame
         * @return {cc.SpriteFrame}
         */
        public getSpriteFrame (
        ) : cc.SpriteFrame;

        /**
         * 
         * @method getResourceName
         * @return {string}
         */
        public getResourceName (
        ) : string;

        /**
         * Whether or not the Sprite needs to be updated in the Atlas.<br>
         * -- return True if the sprite needs to be updated in the Atlas, false otherwise.
         * @method isDirty
         * @return {boolean}
         */
        public isDirty (
        ) : boolean;

        /**
         * Sets the index used on the TextureAtlas.<br>
         * -- warning Don't modify this value unless you know what you are doing.
         * @method setAtlasIndex
         * @param {number} _atlasIndex
         */
        public setAtlasIndex (
            _atlasIndex : number 
        ) : void;

        /**
         * Makes the Sprite to be updated in the Atlas.
         * @method setDirty
         * @param {boolean} _dirty
         */
        public setDirty (
            _dirty : boolean 
        ) : void;

        /**
         * Returns whether or not the texture rectangle is rotated.
         * @method isTextureRectRotated
         * @return {boolean}
         */
        public isTextureRectRotated (
        ) : boolean;

        /**
         * Returns the rect of the Sprite in points.
         * @method getTextureRect
         * @return {ctype.value_type<cc.Rect>}
         */
        public getTextureRect (
        ) : ctype.value_type<cc.Rect>;

        /**
         * Initializes a sprite with an image filename, and a rect.<br>
         * -- This method will find filename from local file system, load its content to Texture2D,<br>
         * -- then use Texture2D to create a sprite.<br>
         * -- After initialization, the offset will be (0,0).<br>
         * -- param   filename The path to an image file in local file system.<br>
         * -- param   rect        The rectangle assigned the content area from texture.<br>
         * -- return  True if the sprite is initialized properly, false otherwise.<br>
         * -- lua     init
         * @method initWithFile
         * @param {(string)} _filename
         * @param {(ctype.value_type<cc.Rect>)} _rect?
         * @return {boolean}
         */
        public initWithFile (
            _filename : (string), 
            _rect? : (ctype.value_type<cc.Rect>)
        ) : boolean;

        /**
         * / @{/ @name Functions inherited from TextureProtocol.<br>
         * -- code<br>
         * -- When this function bound into js or lua,the parameter will be changed.<br>
         * -- In js: var setBlendFunc(var src, var dst).<br>
         * -- In lua: local setBlendFunc(local src, local dst).<br>
         * -- endcode
         * @method setBlendFunc
         * @param {cc.BlendFunc} _blendFunc
         */
        public setBlendFunc (
            _blendFunc : cc.BlendFunc 
        ) : void;

        /**
         * Gets the weak reference of the TextureAtlas when the sprite is rendered using via SpriteBatchNode.
         * @method getTextureAtlas
         * @return {cc.TextureAtlas}
         */
        public getTextureAtlas (
        ) : cc.TextureAtlas;

        /**
         * Initializes a sprite with an SpriteFrame. The texture and rect in SpriteFrame will be applied on this sprite.<br>
         * -- param   spriteFrame  A SpriteFrame object. It should includes a valid texture and a rect.<br>
         * -- return  True if the sprite is initialized properly, false otherwise.
         * @method initWithSpriteFrame
         * @param {cc.SpriteFrame} _spriteFrame
         * @return {boolean}
         */
        public initWithSpriteFrame (
            _spriteFrame : cc.SpriteFrame 
        ) : boolean;

        /**
         * Returns the flag which indicates whether the sprite is flipped horizontally or not.<br>
         * -- It only flips the texture of the sprite, and not the texture of the sprite's children.<br>
         * -- Also, flipping the texture doesn't alter the anchorPoint.<br>
         * -- If you want to flip the anchorPoint too, and/or to flip the children too use:<br>
         * -- sprite->setScaleX(sprite->getScaleX() * -1);<br>
         * -- return true if the sprite is flipped horizontally, false otherwise.
         * @method isFlippedX
         * @return {boolean}
         */
        public isFlippedX (
        ) : boolean;

        /**
         * Return the flag which indicates whether the sprite is flipped vertically or not.<br>
         * -- It only flips the texture of the sprite, and not the texture of the sprite's children.<br>
         * -- Also, flipping the texture doesn't alter the anchorPoint.<br>
         * -- If you want to flip the anchorPoint too, and/or to flip the children too use:<br>
         * -- sprite->setScaleY(sprite->getScaleY() * -1);<br>
         * -- return true if the sprite is flipped vertically, false otherwise.
         * @method isFlippedY
         * @return {boolean}
         */
        public isFlippedY (
        ) : boolean;

        /**
         * Sets the vertex rect.<br>
         * -- It will be called internally by setTextureRect.<br>
         * -- Useful if you want to create 2x images from SD images in Retina Display.<br>
         * -- Do not call it manually. Use setTextureRect instead.
         * @method setVertexRect
         * @param {ctype.value_type<cc.Rect>} _rect
         */
        public setVertexRect (
            _rect : ctype.value_type<cc.Rect> 
        ) : void;

        /**
         * js ctor
         * @method Sprite
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class RenderTexture
     * @native
     */
    export class RenderTexture 
        extends cc.Node
    {
        /**
         *  Gets the Sprite being used. 
         * return A Sprite.
         * @method getSprite
         * @return {cc.Sprite}
         */
        public sprite : cc.Sprite;
        /**
         *  Valid flags: GL_COLOR_BUFFER_BIT, GL_DEPTH_BUFFER_BIT, GL_STENCIL_BUFFER_BIT. They can be OR'ed. Valid when "autoDraw" is true. 
         * return Clear flags.
         * @method getClearFlags
         * @return {number}
         */
        public clearFlags : number;
        /* Can't find native property getClearDepthVal implementation of cc::RenderTexture. using any type */
        public clearDepthVal : any;
        /* Can't find native property getClearStencilVal implementation of cc::RenderTexture. using any type */
        public clearStencilVal : any;
        /**
         *  Clear color value. Valid only when "autoDraw" is true. 
         * return Color value.
         * @method getClearColor
         * @return {ctype.value_type<cc.Color>}
         */
        public clearColorVal : ctype.value_type<cc.Color>;
        /* Can't find native property getAutoDraw implementation of cc::RenderTexture. using any type */
        public autoDraw : any;


        /**
         * Used for grab part of screen to a texture. <br>
         * -- param rtBegin The position of renderTexture on the fullRect.<br>
         * -- param fullRect The total size of screen.<br>
         * -- param fullViewport The total viewportSize.
         * @method setVirtualViewport
         * @param {ctype.value_type<cc.Point>} _rtBegin
         * @param {ctype.value_type<cc.Rect>} _fullRect
         * @param {ctype.value_type<cc.Rect>} _fullViewport
         */
        public setVirtualViewport (
            _rtBegin : ctype.value_type<cc.Point>, 
            _fullRect : ctype.value_type<cc.Rect>, 
            _fullViewport : ctype.value_type<cc.Rect> 
        ) : void;

        /**
         *  Clears the texture with a specified stencil value.<br>
         * -- param stencilValue A specified stencil value.
         * @method clearStencil
         * @param {number} _stencilValue
         */
        public clearStencil (
            _stencilValue : number 
        ) : void;

        /**
         *  Value for clearDepth. Valid only when "autoDraw" is true. <br>
         * -- return Value for clearDepth.
         * @method getClearDepth
         * @return {number}
         */
        public getClearDepth (
        ) : number;

        /**
         *  Value for clear Stencil. Valid only when "autoDraw" is true.<br>
         * -- return Value for clear Stencil.
         * @method getClearStencil
         * @return {number}
         */
        public getClearStencil (
        ) : number;

        /**
         *  Ends grabbing. 
         * @method end
         */
        public end (
        ) : void;

        /**
         *  Set Value for clear Stencil.<br>
         * -- param clearStencil Value for clear Stencil.
         * @method setClearStencil
         * @param {number} _clearStencil
         */
        public setClearStencil (
            _clearStencil : number 
        ) : void;

        /**
         *  Sets the Sprite being used. <br>
         * -- param sprite A Sprite.
         * @method setSprite
         * @param {cc.Sprite} _sprite
         */
        public setSprite (
            _sprite : cc.Sprite 
        ) : void;

        /**
         *  Gets the Sprite being used. <br>
         * -- return A Sprite.
         * @method getSprite
         * @return {cc.Sprite}
         */
        public getSprite (
        ) : cc.Sprite;

        /**
         *  When enabled, it will render its children into the texture automatically. Disabled by default for compatibility reasons.<br>
         * -- Will be enabled in the future.<br>
         * -- return Return the autoDraw value.
         * @method isAutoDraw
         * @return {boolean}
         */
        public isAutoDraw (
        ) : boolean;

        /**
         *  Flag: Use stack matrix computed from scene hierarchy or generate new modelView and projection matrix.<br>
         * -- param keepMatrix Whether or not use stack matrix computed from scene hierarchy or generate new modelView and projection matrix.<br>
         * -- js NA
         * @method setKeepMatrix
         * @param {boolean} _keepMatrix
         */
        public setKeepMatrix (
            _keepMatrix : boolean 
        ) : void;

        /**
         *  Set flags.<br>
         * -- param clearFlags Valid flags: GL_COLOR_BUFFER_BIT, GL_DEPTH_BUFFER_BIT, GL_STENCIL_BUFFER_BIT.
         * @method setClearFlags
         * @param {number} _clearFlags
         */
        public setClearFlags (
            _clearFlags : number 
        ) : void;

        /**
         *  Starts grabbing. 
         * @method begin
         */
        public begin (
        ) : void;

        /**
         *  Set a valve to control whether or not render its children into the texture automatically. <br>
         * -- param isAutoDraw Whether or not render its children into the texture automatically.
         * @method setAutoDraw
         * @param {boolean} _isAutoDraw
         */
        public setAutoDraw (
            _isAutoDraw : boolean 
        ) : void;

        /**
         *  Set color value. <br>
         * -- param clearColor Color value.
         * @method setClearColor
         * @param {ctype.value_type<cc.Color>} _clearColor
         */
        public setClearColor (
            _clearColor : ctype.value_type<cc.Color> 
        ) : void;

        /**
         *  End is key word of lua, use other name to export to lua.<br>
         * -- js NA
         * @method endToLua
         */
        public endToLua (
        ) : void;

        /**
         *  Starts rendering to the texture while clearing the texture first.<br>
         * -- This is more efficient then calling -clear first and then -begin.<br>
         * -- param r Red.<br>
         * -- param g Green.<br>
         * -- param b Blue.<br>
         * -- param a Alpha.<br>
         * -- param depthValue The depth Value.
         * @method beginWithClear
         * @param {(number)} _r
         * @param {(number)} _g
         * @param {(number)} _b
         * @param {(number)} _a
         * @param {(number)} _depthValue?
         * @param {(number)} _stencilValue?
         */
        public beginWithClear (
            _r : (number), 
            _g : (number), 
            _b : (number), 
            _a : (number), 
            _depthValue? : (number), 
            _stencilValue? : (number)
        ) : void;

        /**
         *  Clears the texture with a specified depth value. <br>
         * -- param depthValue A specified depth value.
         * @method clearDepth
         * @param {number} _depthValue
         */
        public clearDepth (
            _depthValue : number 
        ) : void;

        /**
         *  Clear color value. Valid only when "autoDraw" is true. <br>
         * -- return Color value.
         * @method getClearColor
         * @return {ctype.value_type<cc.Color>}
         */
        public getClearColor (
        ) : ctype.value_type<cc.Color>;

        /**
         *  Clears the texture with a color. <br>
         * -- param r Red.<br>
         * -- param g Green.<br>
         * -- param b Blue.<br>
         * -- param a Alpha.
         * @method clear
         * @param {number} _r
         * @param {number} _g
         * @param {number} _b
         * @param {number} _a
         */
        public clear (
            _r : number, 
            _g : number, 
            _b : number, 
            _a : number 
        ) : void;

        /**
         *  Valid flags: GL_COLOR_BUFFER_BIT, GL_DEPTH_BUFFER_BIT, GL_STENCIL_BUFFER_BIT. They can be OR'ed. Valid when "autoDraw" is true. <br>
         * -- return Clear flags.
         * @method getClearFlags
         * @return {number}
         */
        public getClearFlags (
        ) : number;

        /**
         * 
         * @method newImage
         * @return {cc.Image}
         */
        public newImage (
        ) : cc.Image;

        /**
         *  Set Value for clearDepth.<br>
         * -- param clearDepth Value for clearDepth.
         * @method setClearDepth
         * @param {number} _clearDepth
         */
        public setClearDepth (
            _clearDepth : number 
        ) : void;

        /**
         *  Initializes a RenderTexture object with width and height in Points and a pixel format( only RGB and RGBA formats are valid ) and depthStencil format. <br>
         * -- param w The RenderTexture object width.<br>
         * -- param h The RenderTexture object height.<br>
         * -- param format In Points and a pixel format( only RGB and RGBA formats are valid ).<br>
         * -- param depthStencilFormat The depthStencil format.<br>
         * -- return If succeed, it will return true.
         * @method initWithWidthAndHeight
         * @param {(number)} _w
         * @param {(number)} _h
         * @param {(cc.Texture2D::PixelFormat)} _format
         * @param {(number)} _depthStencilFormat?
         * @return {boolean}
         */
        public initWithWidthAndHeight (
            _w : (number), 
            _h : (number), 
            _format : (number), 
            _depthStencilFormat? : (number)
        ) : boolean;

        /**
         *  Creates a RenderTexture object with width and height in Points and a pixel format, only RGB and RGBA formats are valid. <br>
         * -- param w The RenderTexture object width.<br>
         * -- param h The RenderTexture object height.<br>
         * -- param format In Points and a pixel format( only RGB and RGBA formats are valid ).
         * @method create
         * @param {(number)} _w
         * @param {(number)} _h
         * @param {(cc.Texture2D::PixelFormat)} _format?
         * @param {(number)} _depthStencilFormat?
         * @return {cc.RenderTexture}
         */
        public static create (
            _w : (number), 
            _h : (number), 
            _format? : (number), 
            _depthStencilFormat? : (number)
        ) : cc.RenderTexture;

        /**
         *  FIXME: should be protected.<br>
         * -- but due to a bug in PowerVR + Android,<br>
         * -- the constructor is public again.<br>
         * -- js ctor
         * @method RenderTexture
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class TransitionEaseScene
     * @native
     */
    export abstract class TransitionEaseScene 
    {

        /**
         *  Returns the Ease action that will be performed on a linear action.<br>
         * -- since v0.8.2<br>
         * -- param action A given interval action.<br>
         * -- return The Ease action that will be performed on a linear action.
         * @method easeActionWithAction
         * @param {cc.ActionInterval} _action
         * @return {cc.ActionInterval}
         */
        public easeActionWithAction (
            _action : cc.ActionInterval 
        ) : cc.ActionInterval;

    }
    /**
     * @class TransitionScene
     * @native
     */
    export class TransitionScene 
        extends cc.Scene
    {

        /**
         * 
         * @method getInScene
         * @return {cc.Scene}
         */
        public getInScene (
        ) : cc.Scene;

        /**
         *  Called after the transition finishes.
         * @method finish
         */
        public finish (
        ) : void;

        /**
         *  initializes a transition with duration and incoming scene 
         * @method initWithDuration
         * @param {number} _t
         * @param {cc.Scene} _scene
         * @return {boolean}
         */
        public initWithDuration (
            _t : number, 
            _scene : cc.Scene 
        ) : boolean;

        /**
         * 
         * @method getDuration
         * @return {number}
         */
        public getDuration (
        ) : number;

        /**
         *  Used by some transitions to hide the outer scene.
         * @method hideOutShowIn
         */
        public hideOutShowIn (
        ) : void;

        /**
         *  Creates a base transition with duration and incoming scene.<br>
         * -- param t Duration time, in seconds.<br>
         * -- param scene A given scene.<br>
         * -- return A autoreleased TransitionScene object.
         * @method create
         * @param {number} _t
         * @param {cc.Scene} _scene
         * @return {cc.TransitionScene}
         */
        public static create (
            _t : number, 
            _scene : cc.Scene 
        ) : cc.TransitionScene;

        /**
         * 
         * @method TransitionScene
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class TransitionSceneOriented
     * @native
     */
    export class TransitionSceneOriented 
        extends cc.TransitionScene
    {

        /**
         *  initializes a transition with duration and incoming scene 
         * @method initWithDuration
         * @param {number} _t
         * @param {cc.Scene} _scene
         * @param {cc.TransitionScene::Orientation} _orientation
         * @return {boolean}
         */
        public initWithDuration (
            _t : number, 
            _scene : cc.Scene, 
            _orientation : number 
        ) : boolean;

        /**
         *  Creates a transition with duration, incoming scene and orientation.<br>
         * -- param t Duration time, in seconds.<br>
         * -- param scene A given scene.<br>
         * -- param orientation A given orientation: LeftOver, RightOver, UpOver, DownOver.<br>
         * -- return A autoreleased TransitionSceneOriented object.
         * @method create
         * @param {number} _t
         * @param {cc.Scene} _scene
         * @param {cc.TransitionScene::Orientation} _orientation
         * @return {cc.TransitionSceneOriented}
         */
        public static create (
            _t : number, 
            _scene : cc.Scene, 
            _orientation : number 
        ) : cc.TransitionSceneOriented;

        /**
         * 
         * @method TransitionSceneOriented
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class TransitionRotoZoom
     * @native
     */
    export class TransitionRotoZoom 
        extends cc.TransitionScene
    {

        /**
         *  Creates a transition with duration and incoming scene.<br>
         * -- param t Duration time, in seconds.<br>
         * -- param scene A given scene.<br>
         * -- return A autoreleased TransitionRotoZoom object.
         * @method create
         * @param {number} _t
         * @param {cc.Scene} _scene
         * @return {cc.TransitionRotoZoom}
         */
        public static create (
            _t : number, 
            _scene : cc.Scene 
        ) : cc.TransitionRotoZoom;

        /**
         * 
         * @method TransitionRotoZoom
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class TransitionJumpZoom
     * @native
     */
    export class TransitionJumpZoom 
        extends cc.TransitionScene
    {

        /**
         *  Creates a transition with duration and incoming scene.<br>
         * -- param t Duration time, in seconds.<br>
         * -- param scene A given scene.<br>
         * -- return A autoreleased TransitionJumpZoom object.
         * @method create
         * @param {number} _t
         * @param {cc.Scene} _scene
         * @return {cc.TransitionJumpZoom}
         */
        public static create (
            _t : number, 
            _scene : cc.Scene 
        ) : cc.TransitionJumpZoom;

        /**
         * 
         * @method TransitionJumpZoom
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class TransitionMoveInL
     * @native
     */
    export class TransitionMoveInL 
        extends cc.TransitionScene
    {

        /**
         *  Returns the action that will be performed. <br>
         * -- return The action that will be performed.
         * @method action
         * @return {cc.ActionInterval}
         */
        public action (
        ) : cc.ActionInterval;

        /**
         * 
         * @method easeActionWithAction
         * @param {cc.ActionInterval} _action
         * @return {cc.ActionInterval}
         */
        public easeActionWithAction (
            _action : cc.ActionInterval 
        ) : cc.ActionInterval;

        /**
         *  Creates a transition with duration and incoming scene.<br>
         * -- param t Duration time, in seconds.<br>
         * -- param scene A given scene.<br>
         * -- return A autoreleased TransitionMoveInL object.
         * @method create
         * @param {number} _t
         * @param {cc.Scene} _scene
         * @return {cc.TransitionMoveInL}
         */
        public static create (
            _t : number, 
            _scene : cc.Scene 
        ) : cc.TransitionMoveInL;

        /**
         * 
         * @method TransitionMoveInL
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class TransitionMoveInR
     * @native
     */
    export class TransitionMoveInR 
        extends cc.TransitionMoveInL
    {

        /**
         *  Creates a transition with duration and incoming scene.<br>
         * -- param t Duration time, in seconds.<br>
         * -- param scene A given scene.<br>
         * -- return A autoreleased TransitionMoveInR object.
         * @method create
         * @param {number} _t
         * @param {cc.Scene} _scene
         * @return {cc.TransitionMoveInR}
         */
        public static create (
            _t : number, 
            _scene : cc.Scene 
        ) : cc.TransitionMoveInR;

        /**
         * 
         * @method TransitionMoveInR
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class TransitionMoveInT
     * @native
     */
    export class TransitionMoveInT 
        extends cc.TransitionMoveInL
    {

        /**
         *  Creates a transition with duration and incoming scene.<br>
         * -- param t Duration time, in seconds.<br>
         * -- param scene A given scene.<br>
         * -- return A autoreleased TransitionMoveInT object.
         * @method create
         * @param {number} _t
         * @param {cc.Scene} _scene
         * @return {cc.TransitionMoveInT}
         */
        public static create (
            _t : number, 
            _scene : cc.Scene 
        ) : cc.TransitionMoveInT;

        /**
         * 
         * @method TransitionMoveInT
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class TransitionMoveInB
     * @native
     */
    export class TransitionMoveInB 
        extends cc.TransitionMoveInL
    {

        /**
         *  Creates a transition with duration and incoming scene.<br>
         * -- param t Duration time, in seconds.<br>
         * -- param scene A given scene.<br>
         * -- return A autoreleased TransitionMoveInB object.
         * @method create
         * @param {number} _t
         * @param {cc.Scene} _scene
         * @return {cc.TransitionMoveInB}
         */
        public static create (
            _t : number, 
            _scene : cc.Scene 
        ) : cc.TransitionMoveInB;

        /**
         * 
         * @method TransitionMoveInB
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class TransitionSlideInL
     * @native
     */
    export class TransitionSlideInL 
        extends cc.TransitionScene
    {

        /**
         *  Returns the action that will be performed by the incoming and outgoing scene.<br>
         * -- return The action that will be performed by the incoming and outgoing scene.
         * @method action
         * @return {cc.ActionInterval}
         */
        public action (
        ) : cc.ActionInterval;

        /**
         * 
         * @method easeActionWithAction
         * @param {cc.ActionInterval} _action
         * @return {cc.ActionInterval}
         */
        public easeActionWithAction (
            _action : cc.ActionInterval 
        ) : cc.ActionInterval;

        /**
         *  Creates a transition with duration and incoming scene.<br>
         * -- param t Duration time, in seconds.<br>
         * -- param scene A given scene.<br>
         * -- return A autoreleased TransitionSlideInL object.
         * @method create
         * @param {number} _t
         * @param {cc.Scene} _scene
         * @return {cc.TransitionSlideInL}
         */
        public static create (
            _t : number, 
            _scene : cc.Scene 
        ) : cc.TransitionSlideInL;

        /**
         * 
         * @method TransitionSlideInL
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class TransitionSlideInR
     * @native
     */
    export class TransitionSlideInR 
        extends cc.TransitionSlideInL
    {

        /**
         *  Creates a transition with duration and incoming scene.<br>
         * -- param t Duration time, in seconds.<br>
         * -- param scene A given scene.<br>
         * -- return A autoreleased TransitionSlideInR object.
         * @method create
         * @param {number} _t
         * @param {cc.Scene} _scene
         * @return {cc.TransitionSlideInR}
         */
        public static create (
            _t : number, 
            _scene : cc.Scene 
        ) : cc.TransitionSlideInR;

        /**
         * 
         * @method TransitionSlideInR
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class TransitionSlideInB
     * @native
     */
    export class TransitionSlideInB 
        extends cc.TransitionSlideInL
    {

        /**
         *  Creates a transition with duration and incoming scene.<br>
         * -- param t Duration time, in seconds.<br>
         * -- param scene A given scene.<br>
         * -- return A autoreleased TransitionSlideInB object.
         * @method create
         * @param {number} _t
         * @param {cc.Scene} _scene
         * @return {cc.TransitionSlideInB}
         */
        public static create (
            _t : number, 
            _scene : cc.Scene 
        ) : cc.TransitionSlideInB;

        /**
         * 
         * @method TransitionSlideInB
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class TransitionSlideInT
     * @native
     */
    export class TransitionSlideInT 
        extends cc.TransitionSlideInL
    {

        /**
         *  Creates a transition with duration and incoming scene.<br>
         * -- param t Duration time, in seconds.<br>
         * -- param scene A given scene.<br>
         * -- return A autoreleased TransitionSlideInT object.
         * @method create
         * @param {number} _t
         * @param {cc.Scene} _scene
         * @return {cc.TransitionSlideInT}
         */
        public static create (
            _t : number, 
            _scene : cc.Scene 
        ) : cc.TransitionSlideInT;

        /**
         * 
         * @method TransitionSlideInT
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class TransitionShrinkGrow
     * @native
     */
    export class TransitionShrinkGrow 
        extends cc.TransitionScene
    {

        /**
         * 
         * @method easeActionWithAction
         * @param {cc.ActionInterval} _action
         * @return {cc.ActionInterval}
         */
        public easeActionWithAction (
            _action : cc.ActionInterval 
        ) : cc.ActionInterval;

        /**
         *  Creates a transition with duration and incoming scene.<br>
         * -- param t Duration time, in seconds.<br>
         * -- param scene A given scene.<br>
         * -- return A autoreleased TransitionShrinkGrow object.
         * @method create
         * @param {number} _t
         * @param {cc.Scene} _scene
         * @return {cc.TransitionShrinkGrow}
         */
        public static create (
            _t : number, 
            _scene : cc.Scene 
        ) : cc.TransitionShrinkGrow;

        /**
         * 
         * @method TransitionShrinkGrow
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class TransitionFlipX
     * @native
     */
    export class TransitionFlipX 
        extends cc.TransitionSceneOriented
    {

        /**
         *  Creates a transition with duration and incoming scene.<br>
         * -- param t Duration time, in seconds.<br>
         * -- param s A given scene.<br>
         * -- return A autoreleased TransitionFlipX object.
         * @method create
         * @param {(number)} _t
         * @param {(cc.Scene)} _s
         * @param {(cc.TransitionScene::Orientation)} _o?
         * @return {cc.TransitionFlipX}
         */
        public static create (
            _t : (number), 
            _s : (cc.Scene), 
            _o? : (number)
        ) : cc.TransitionFlipX;

        /**
         * 
         * @method TransitionFlipX
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class TransitionFlipY
     * @native
     */
    export class TransitionFlipY 
        extends cc.TransitionSceneOriented
    {

        /**
         *  Creates a transition with duration and incoming scene.<br>
         * -- param t Duration time, in seconds.<br>
         * -- param s A given scene.<br>
         * -- return A autoreleased TransitionFlipY object.
         * @method create
         * @param {(number)} _t
         * @param {(cc.Scene)} _s
         * @param {(cc.TransitionScene::Orientation)} _o?
         * @return {cc.TransitionFlipY}
         */
        public static create (
            _t : (number), 
            _s : (cc.Scene), 
            _o? : (number)
        ) : cc.TransitionFlipY;

        /**
         * 
         * @method TransitionFlipY
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class TransitionFlipAngular
     * @native
     */
    export class TransitionFlipAngular 
        extends cc.TransitionSceneOriented
    {

        /**
         *  Creates a transition with duration and incoming scene.<br>
         * -- param t Duration time, in seconds.<br>
         * -- param s A given scene.<br>
         * -- return A autoreleased TransitionFlipAngular object.
         * @method create
         * @param {(number)} _t
         * @param {(cc.Scene)} _s
         * @param {(cc.TransitionScene::Orientation)} _o?
         * @return {cc.TransitionFlipAngular}
         */
        public static create (
            _t : (number), 
            _s : (cc.Scene), 
            _o? : (number)
        ) : cc.TransitionFlipAngular;

        /**
         * 
         * @method TransitionFlipAngular
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class TransitionZoomFlipX
     * @native
     */
    export class TransitionZoomFlipX 
        extends cc.TransitionSceneOriented
    {

        /**
         *  Creates a transition with duration and incoming scene.<br>
         * -- param t Duration time, in seconds.<br>
         * -- param s A given scene.<br>
         * -- return A autoreleased TransitionZoomFlipX object.
         * @method create
         * @param {(number)} _t
         * @param {(cc.Scene)} _s
         * @param {(cc.TransitionScene::Orientation)} _o?
         * @return {cc.TransitionZoomFlipX}
         */
        public static create (
            _t : (number), 
            _s : (cc.Scene), 
            _o? : (number)
        ) : cc.TransitionZoomFlipX;

        /**
         * 
         * @method TransitionZoomFlipX
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class TransitionZoomFlipY
     * @native
     */
    export class TransitionZoomFlipY 
        extends cc.TransitionSceneOriented
    {

        /**
         *  Creates a transition with duration and incoming scene.<br>
         * -- param t Duration time, in seconds.<br>
         * -- param s A given scene.<br>
         * -- return A autoreleased TransitionZoomFlipY object.
         * @method create
         * @param {(number)} _t
         * @param {(cc.Scene)} _s
         * @param {(cc.TransitionScene::Orientation)} _o?
         * @return {cc.TransitionZoomFlipY}
         */
        public static create (
            _t : (number), 
            _s : (cc.Scene), 
            _o? : (number)
        ) : cc.TransitionZoomFlipY;

        /**
         * 
         * @method TransitionZoomFlipY
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class TransitionZoomFlipAngular
     * @native
     */
    export class TransitionZoomFlipAngular 
        extends cc.TransitionSceneOriented
    {

        /**
         *  Creates a transition with duration and incoming scene.<br>
         * -- param t Duration time, in seconds.<br>
         * -- param s A given scene.<br>
         * -- return A autoreleased TransitionZoomFlipAngular object.
         * @method create
         * @param {(number)} _t
         * @param {(cc.Scene)} _s
         * @param {(cc.TransitionScene::Orientation)} _o?
         * @return {cc.TransitionZoomFlipAngular}
         */
        public static create (
            _t : (number), 
            _s : (cc.Scene), 
            _o? : (number)
        ) : cc.TransitionZoomFlipAngular;

        /**
         * 
         * @method TransitionZoomFlipAngular
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class TransitionFade
     * @native
     */
    export class TransitionFade 
        extends cc.TransitionScene
    {

        /**
         * 
         * @method initWithDuration
         * @param {(number)} _t
         * @param {(cc.Scene)} _scene
         * @param {(ctype.value_type<cc.Color>)} _color?
         * @return {boolean}
         */
        public initWithDuration (
            _t : (number), 
            _scene : (cc.Scene), 
            _color? : (ctype.value_type<cc.Color>)
        ) : boolean;

        /**
         *  Creates the transition with a duration.<br>
         * -- param duration Duration time, in seconds.<br>
         * -- param scene A given scene.<br>
         * -- return A autoreleased TransitionFade object.
         * @method create
         * @param {(number)} _duration
         * @param {(cc.Scene)} _scene
         * @param {(ctype.value_type<cc.Color>)} _color?
         * @return {cc.TransitionFade}
         */
        public static create (
            _duration : (number), 
            _scene : (cc.Scene), 
            _color? : (ctype.value_type<cc.Color>)
        ) : cc.TransitionFade;

        /**
         * 
         * @method TransitionFade
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class TransitionCrossFade
     * @native
     */
    export class TransitionCrossFade 
        extends cc.TransitionScene
    {

        /**
         *  Creates a transition with duration and incoming scene.<br>
         * -- param t Duration time, in seconds.<br>
         * -- param scene A given scene.<br>
         * -- return A autoreleased TransitionCrossFade object.
         * @method create
         * @param {number} _t
         * @param {cc.Scene} _scene
         * @return {cc.TransitionCrossFade}
         */
        public static create (
            _t : number, 
            _scene : cc.Scene 
        ) : cc.TransitionCrossFade;

        /**
         * 
         * @method TransitionCrossFade
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class TransitionTurnOffTiles
     * @native
     */
    export class TransitionTurnOffTiles 
        extends cc.TransitionScene
    {

        /**
         * 
         * @method easeActionWithAction
         * @param {cc.ActionInterval} _action
         * @return {cc.ActionInterval}
         */
        public easeActionWithAction (
            _action : cc.ActionInterval 
        ) : cc.ActionInterval;

        /**
         *  Creates a transition with duration and incoming scene.<br>
         * -- param t Duration time, in seconds.<br>
         * -- param scene A given scene.<br>
         * -- return A autoreleased TransitionTurnOffTiles object.
         * @method create
         * @param {number} _t
         * @param {cc.Scene} _scene
         * @return {cc.TransitionTurnOffTiles}
         */
        public static create (
            _t : number, 
            _scene : cc.Scene 
        ) : cc.TransitionTurnOffTiles;

        /**
         * 
         * @method TransitionTurnOffTiles
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class TransitionSplitCols
     * @native
     */
    export class TransitionSplitCols 
        extends cc.TransitionScene
    {

        /**
         *  Returns the action that will be performed.<br>
         * -- return The action that will be performed.
         * @method action
         * @return {cc.ActionInterval}
         */
        public action (
        ) : cc.ActionInterval;

        /**
         * 
         * @method easeActionWithAction
         * @param {cc.ActionInterval} _action
         * @return {cc.ActionInterval}
         */
        public easeActionWithAction (
            _action : cc.ActionInterval 
        ) : cc.ActionInterval;

        /**
         *  Creates a transition with duration and incoming scene.<br>
         * -- param t Duration time, in seconds.<br>
         * -- param scene A given scene.<br>
         * -- return A autoreleased TransitionSplitCols object.
         * @method create
         * @param {number} _t
         * @param {cc.Scene} _scene
         * @return {cc.TransitionSplitCols}
         */
        public static create (
            _t : number, 
            _scene : cc.Scene 
        ) : cc.TransitionSplitCols;

        /**
         * 
         * @method TransitionSplitCols
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class TransitionSplitRows
     * @native
     */
    export class TransitionSplitRows 
        extends cc.TransitionSplitCols
    {

        /**
         *  Creates a transition with duration and incoming scene.<br>
         * -- param t Duration time, in seconds.<br>
         * -- param scene A given scene.<br>
         * -- return A autoreleased TransitionSplitRows object.
         * @method create
         * @param {number} _t
         * @param {cc.Scene} _scene
         * @return {cc.TransitionSplitRows}
         */
        public static create (
            _t : number, 
            _scene : cc.Scene 
        ) : cc.TransitionSplitRows;

        /**
         * 
         * @method TransitionSplitRows
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class TransitionFadeTR
     * @native
     */
    export class TransitionFadeTR 
        extends cc.TransitionScene
    {

        /**
         * 
         * @method easeActionWithAction
         * @param {cc.ActionInterval} _action
         * @return {cc.ActionInterval}
         */
        public easeActionWithAction (
            _action : cc.ActionInterval 
        ) : cc.ActionInterval;

        /**
         *  Returns the action that will be performed with size.<br>
         * -- param size A given size.<br>
         * -- return The action that will be performed.
         * @method actionWithSize
         * @param {ctype.value_type<cc.Size>} _size
         * @return {cc.ActionInterval}
         */
        public actionWithSize (
            _size : ctype.value_type<cc.Size> 
        ) : cc.ActionInterval;

        /**
         *  Creates a transition with duration and incoming scene.<br>
         * -- param t Duration time, in seconds.<br>
         * -- param scene A given scene.<br>
         * -- return A autoreleased TransitionFadeTR object.
         * @method create
         * @param {number} _t
         * @param {cc.Scene} _scene
         * @return {cc.TransitionFadeTR}
         */
        public static create (
            _t : number, 
            _scene : cc.Scene 
        ) : cc.TransitionFadeTR;

        /**
         * 
         * @method TransitionFadeTR
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class TransitionFadeBL
     * @native
     */
    export class TransitionFadeBL 
        extends cc.TransitionFadeTR
    {

        /**
         *  Creates a transition with duration and incoming scene.<br>
         * -- param t Duration time, in seconds.<br>
         * -- param scene A given scene.<br>
         * -- return A autoreleased TransitionFadeBL object.
         * @method create
         * @param {number} _t
         * @param {cc.Scene} _scene
         * @return {cc.TransitionFadeBL}
         */
        public static create (
            _t : number, 
            _scene : cc.Scene 
        ) : cc.TransitionFadeBL;

        /**
         * 
         * @method TransitionFadeBL
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class TransitionFadeUp
     * @native
     */
    export class TransitionFadeUp 
        extends cc.TransitionFadeTR
    {

        /**
         *  Creates a transition with duration and incoming scene.<br>
         * -- param t Duration time, in seconds.<br>
         * -- param scene A given scene.<br>
         * -- return A autoreleased TransitionFadeUp object.
         * @method create
         * @param {number} _t
         * @param {cc.Scene} _scene
         * @return {cc.TransitionFadeUp}
         */
        public static create (
            _t : number, 
            _scene : cc.Scene 
        ) : cc.TransitionFadeUp;

        /**
         * 
         * @method TransitionFadeUp
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class TransitionFadeDown
     * @native
     */
    export class TransitionFadeDown 
        extends cc.TransitionFadeTR
    {

        /**
         *  Creates a transition with duration and incoming scene.<br>
         * -- param t Duration time, in seconds.<br>
         * -- param scene A given scene.<br>
         * -- return A autoreleased TransitionFadeDown object.
         * @method create
         * @param {number} _t
         * @param {cc.Scene} _scene
         * @return {cc.TransitionFadeDown}
         */
        public static create (
            _t : number, 
            _scene : cc.Scene 
        ) : cc.TransitionFadeDown;

        /**
         * 
         * @method TransitionFadeDown
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class TransitionPageTurn
     * @native
     */
    export class TransitionPageTurn 
        extends cc.TransitionScene
    {

        /**
         *  Returns the action that will be performed with size.<br>
         * -- param vector A given size.<br>
         * -- return The action that will be performed.
         * @method actionWithSize
         * @param {ctype.value_type<cc.Size>} _vector
         * @return {cc.ActionInterval}
         */
        public actionWithSize (
            _vector : ctype.value_type<cc.Size> 
        ) : cc.ActionInterval;

        /**
         * Creates a base transition with duration and incoming scene.<br>
         * -- If back is true then the effect is reversed to appear as if the incoming<br>
         * -- scene is being turned from left over the outgoing scene.<br>
         * -- param t Duration time, in seconds.<br>
         * -- param scene A given scene.<br>
         * -- param backwards If back is true then the effect is reversed to appear as if the incoming scene is being turned from left over the outgoing scene.<br>
         * -- return True if initialize success.
         * @method initWithDuration
         * @param {number} _t
         * @param {cc.Scene} _scene
         * @param {boolean} _backwards
         * @return {boolean}
         */
        public initWithDuration (
            _t : number, 
            _scene : cc.Scene, 
            _backwards : boolean 
        ) : boolean;

        /**
         * Creates a base transition with duration and incoming scene.<br>
         * -- If back is true then the effect is reversed to appear as if the incoming<br>
         * -- scene is being turned from left over the outgoing scene.<br>
         * -- param t Duration time, in seconds.<br>
         * -- param scene A given scene.<br>
         * -- param backwards If back is true then the effect is reversed to appear as if the incoming scene is being turned from left over the outgoing scene.<br>
         * -- return An autoreleased TransitionPageTurn object.
         * @method create
         * @param {number} _t
         * @param {cc.Scene} _scene
         * @param {boolean} _backwards
         * @return {cc.TransitionPageTurn}
         */
        public static create (
            _t : number, 
            _scene : cc.Scene, 
            _backwards : boolean 
        ) : cc.TransitionPageTurn;

        /**
         * js ctor
         * @method TransitionPageTurn
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class TransitionProgress
     * @native
     */
    export class TransitionProgress 
        extends cc.TransitionScene
    {

        /**
         *  Creates a transition with duration and incoming scene.<br>
         * -- param t Duration time, in seconds.<br>
         * -- param scene A given scene.<br>
         * -- return An autoreleased TransitionProgress object.
         * @method create
         * @param {number} _t
         * @param {cc.Scene} _scene
         * @return {cc.TransitionProgress}
         */
        public static create (
            _t : number, 
            _scene : cc.Scene 
        ) : cc.TransitionProgress;

        /**
         * 
         * @method TransitionProgress
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class TransitionProgressRadialCCW
     * @native
     */
    export class TransitionProgressRadialCCW 
        extends cc.TransitionProgress
    {

        /**
         *  Creates a transition with duration and incoming scene.<br>
         * -- param t Duration time, in seconds.<br>
         * -- param scene A given scene.<br>
         * -- return An autoreleased TransitionProgressRadialCCW object.
         * @method create
         * @param {number} _t
         * @param {cc.Scene} _scene
         * @return {cc.TransitionProgressRadialCCW}
         */
        public static create (
            _t : number, 
            _scene : cc.Scene 
        ) : cc.TransitionProgressRadialCCW;

        /**
         * js ctor
         * @method TransitionProgressRadialCCW
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class TransitionProgressRadialCW
     * @native
     */
    export class TransitionProgressRadialCW 
        extends cc.TransitionProgress
    {

        /**
         *  Creates a transition with duration and incoming scene.<br>
         * -- param t Duration time, in seconds.<br>
         * -- param scene A given scene.<br>
         * -- return An autoreleased TransitionProgressRadialCW object.
         * @method create
         * @param {number} _t
         * @param {cc.Scene} _scene
         * @return {cc.TransitionProgressRadialCW}
         */
        public static create (
            _t : number, 
            _scene : cc.Scene 
        ) : cc.TransitionProgressRadialCW;

        /**
         * js ctor
         * @method TransitionProgressRadialCW
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class TransitionProgressHorizontal
     * @native
     */
    export class TransitionProgressHorizontal 
        extends cc.TransitionProgress
    {

        /**
         *  Creates a transition with duration and incoming scene.<br>
         * -- param t Duration time, in seconds.<br>
         * -- param scene A given scene.<br>
         * -- return An autoreleased TransitionProgressHorizontal object.
         * @method create
         * @param {number} _t
         * @param {cc.Scene} _scene
         * @return {cc.TransitionProgressHorizontal}
         */
        public static create (
            _t : number, 
            _scene : cc.Scene 
        ) : cc.TransitionProgressHorizontal;

        /**
         * js ctor
         * @method TransitionProgressHorizontal
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class TransitionProgressVertical
     * @native
     */
    export class TransitionProgressVertical 
        extends cc.TransitionProgress
    {

        /**
         *  Creates a transition with duration and incoming scene.<br>
         * -- param t Duration time, in seconds.<br>
         * -- param scene A given scene.<br>
         * -- return An autoreleased TransitionProgressVertical object.
         * @method create
         * @param {number} _t
         * @param {cc.Scene} _scene
         * @return {cc.TransitionProgressVertical}
         */
        public static create (
            _t : number, 
            _scene : cc.Scene 
        ) : cc.TransitionProgressVertical;

        /**
         * js ctor
         * @method TransitionProgressVertical
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class TransitionProgressInOut
     * @native
     */
    export class TransitionProgressInOut 
        extends cc.TransitionProgress
    {

        /**
         *  Creates a transition with duration and incoming scene.<br>
         * -- param t Duration time, in seconds.<br>
         * -- param scene A given scene.<br>
         * -- return An autoreleased TransitionProgressInOut object.
         * @method create
         * @param {number} _t
         * @param {cc.Scene} _scene
         * @return {cc.TransitionProgressInOut}
         */
        public static create (
            _t : number, 
            _scene : cc.Scene 
        ) : cc.TransitionProgressInOut;

        /**
         * js ctor
         * @method TransitionProgressInOut
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class TransitionProgressOutIn
     * @native
     */
    export class TransitionProgressOutIn 
        extends cc.TransitionProgress
    {

        /**
         *  Creates a transition with duration and incoming scene.<br>
         * -- param t Duration time, in seconds.<br>
         * -- param scene A given scene.<br>
         * -- return An autoreleased TransitionProgressOutIn object.
         * @method create
         * @param {number} _t
         * @param {cc.Scene} _scene
         * @return {cc.TransitionProgressOutIn}
         */
        public static create (
            _t : number, 
            _scene : cc.Scene 
        ) : cc.TransitionProgressOutIn;

        /**
         * js ctor
         * @method TransitionProgressOutIn
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class Camera
     * @native
     */
    export class Camera 
        extends cc.Node
    {

        /**
         * get depth, camera with larger depth is drawn on top of camera with smaller depth, the depth of camera with CameraFlag::DEFAULT is 0, user defined camera is -1 by default
         * @method getDepth
         * @return {number}
         */
        public getDepth (
        ) : number;

        /**
         * get view projection matrix
         * @method getViewProjectionMatrix
         * @return {ctype.value_type<cc.Mat4>}
         */
        public getViewProjectionMatrix (
        ) : ctype.value_type<cc.Mat4>;

        /**
         * 
         * @method applyViewport
         */
        public applyViewport (
        ) : void;

        /**
         * set the background brush. See CameraBackgroundBrush for more information.<br>
         * -- param clearBrush Brush used to clear the background
         * @method setBackgroundBrush
         * @param {cc.CameraBackgroundBrush} _clearBrush
         */
        public setBackgroundBrush (
            _clearBrush : cc.CameraBackgroundBrush 
        ) : void;

        /**
         * Make Camera looks at target<br>
         * -- param target The target camera is point at<br>
         * -- param up The up vector, usually it's Y axis
         * @method lookAt
         * @param {ctype.value_type<cc.Vec3>} _target
         * @param {ctype.value_type<cc.Vec3>} _up
         */
        public lookAt (
            _target : ctype.value_type<cc.Vec3>, 
            _up : ctype.value_type<cc.Vec3> 
        ) : void;

        /**
         * Apply the FBO, RenderTargets and viewport.
         * @method apply
         */
        public apply (
        ) : void;

        /**
         * Get clear brush
         * @method getBackgroundBrush
         * @return {cc.CameraBackgroundBrush}
         */
        public getBackgroundBrush (
        ) : cc.CameraBackgroundBrush;

        /**
         * Gets the camera's projection matrix.<br>
         * -- return The camera projection matrix.
         * @method getProjectionMatrix
         * @return {ctype.value_type<cc.Mat4>}
         */
        public getProjectionMatrix (
        ) : ctype.value_type<cc.Mat4>;

        /**
         * 
         * @method isBrushValid
         * @return {boolean}
         */
        public isBrushValid (
        ) : boolean;

        /**
         * Get object depth towards camera
         * @method getDepthInView
         * @param {ctype.value_type<cc.Mat4>} _transform
         * @return {number}
         */
        public getDepthInView (
            _transform : ctype.value_type<cc.Mat4> 
        ) : number;

        /**
         * Before rendering scene with this camera, the background need to be cleared. It clears the depth buffer with max depth by default. Use setBackgroundBrush to modify the default behavior
         * @method clearBackground
         */
        public clearBackground (
        ) : void;

        /**
         * set additional matrix for the projection matrix, it multiplies mat to projection matrix when called, used by WP8
         * @method setAdditionalProjection
         * @param {ctype.value_type<cc.Mat4>} _mat
         */
        public setAdditionalProjection (
            _mat : ctype.value_type<cc.Mat4> 
        ) : void;

        /**
         * Set Viewport for camera.
         * @method setViewport
         * @param {cc.experimental::Viewport} _vp
         */
        public setViewport (
            _vp : any 
        ) : void;

        /**
         *  init camera 
         * @method initDefault
         * @return {boolean}
         */
        public initDefault (
        ) : boolean;

        /**
         * get & set Camera flag
         * @method getCameraFlag
         * @return {number}
         */
        public getCameraFlag (
        ) : number;

        /**
         * Gets the type of camera.<br>
         * -- return The camera type.
         * @method getType
         * @return {number}
         */
        public getType (
        ) : number;

        /**
         * 
         * @method initOrthographic
         * @param {number} _zoomX
         * @param {number} _zoomY
         * @param {number} _nearPlane
         * @param {number} _farPlane
         * @return {boolean}
         */
        public initOrthographic (
            _zoomX : number, 
            _zoomY : number, 
            _nearPlane : number, 
            _farPlane : number 
        ) : boolean;

        /**
         * get rendered order
         * @method getRenderOrder
         * @return {number}
         */
        public getRenderOrder (
        ) : number;

        /**
         * set depth, camera with larger depth is drawn on top of camera with smaller depth, the depth of camera with CameraFlag::DEFAULT is 0, user defined camera is -1 by default
         * @method setDepth
         * @param {number} _depth
         */
        public setDepth (
            _depth : number 
        ) : void;

        /**
         * Set the scene,this method shall not be invoke manually
         * @method setScene
         * @param {cc.Scene} _scene
         */
        public setScene (
            _scene : cc.Scene 
        ) : void;

        /**
         * 
         * @method projectGL
         * @param {ctype.value_type<cc.Vec3>} _src
         * @return {ctype.value_type<cc.Point>}
         */
        public projectGL (
            _src : ctype.value_type<cc.Vec3> 
        ) : ctype.value_type<cc.Point>;

        /**
         * Gets the camera's view matrix.<br>
         * -- return The camera view matrix.
         * @method getViewMatrix
         * @return {ctype.value_type<cc.Mat4>}
         */
        public getViewMatrix (
        ) : ctype.value_type<cc.Mat4>;

        /**
         * Get the frustum's near plane.
         * @method getNearPlane
         * @return {number}
         */
        public getNearPlane (
        ) : number;

        /**
         * 
         * @method project
         * @param {ctype.value_type<cc.Vec3>} _src
         * @return {ctype.value_type<cc.Point>}
         */
        public project (
            _src : ctype.value_type<cc.Vec3> 
        ) : ctype.value_type<cc.Point>;

        /**
         * 
         * @method setCameraFlag
         * @param {cc.CameraFlag} _flag
         */
        public setCameraFlag (
            _flag : number 
        ) : void;

        /**
         * Get the frustum's far plane.
         * @method getFarPlane
         * @return {number}
         */
        public getFarPlane (
        ) : number;

        /**
         * 
         * @method applyFrameBufferObject
         */
        public applyFrameBufferObject (
        ) : void;

        /**
         * Set FBO, which will attach several render target for the rendered result.
         * @method setFrameBufferObject
         * @param {cc.experimental::FrameBuffer} _fbo
         */
        public setFrameBufferObject (
            _fbo : any 
        ) : void;

        /**
         * Whether or not the viewprojection matrix was updated since the last frame.<br>
         * -- return True if the viewprojection matrix was updated since the last frame.
         * @method isViewProjectionUpdated
         * @return {boolean}
         */
        public isViewProjectionUpdated (
        ) : boolean;

        /**
         * 
         * @method initPerspective
         * @param {number} _fieldOfView
         * @param {number} _aspectRatio
         * @param {number} _nearPlane
         * @param {number} _farPlane
         * @return {boolean}
         */
        public initPerspective (
            _fieldOfView : number, 
            _aspectRatio : number, 
            _nearPlane : number, 
            _farPlane : number 
        ) : boolean;

        /**
         * Creates an orthographic camera.<br>
         * -- param zoomX The zoom factor along the X-axis of the orthographic projection (the width of the ortho projection).<br>
         * -- param zoomY The zoom factor along the Y-axis of the orthographic projection (the height of the ortho projection).<br>
         * -- param nearPlane The near plane distance.<br>
         * -- param farPlane The far plane distance.
         * @method createOrthographic
         * @param {number} _zoomX
         * @param {number} _zoomY
         * @param {number} _nearPlane
         * @param {number} _farPlane
         * @return {cc.Camera}
         */
        public static createOrthographic (
            _zoomX : number, 
            _zoomY : number, 
            _nearPlane : number, 
            _farPlane : number 
        ) : cc.Camera;

        /**
         * Get the visiting camera , the visiting camera shall be set on Scene::render
         * @method getVisitingCamera
         * @return {cc.Camera}
         */
        public static getVisitingCamera (
        ) : cc.Camera;

        /**
         *  create default camera, the camera type depends on Director::getProjection, the depth of the default camera is 0 
         * @method create
         * @return {cc.Camera}
         */
        public static create (
        ) : cc.Camera;

        /**
         * Creates a perspective camera.<br>
         * -- param fieldOfView The field of view for the perspective camera (normally in the range of 40-60 degrees).<br>
         * -- param aspectRatio The aspect ratio of the camera (normally the width of the viewport divided by the height of the viewport).<br>
         * -- param nearPlane The near plane distance.<br>
         * -- param farPlane The far plane distance.
         * @method createPerspective
         * @param {number} _fieldOfView
         * @param {number} _aspectRatio
         * @param {number} _nearPlane
         * @param {number} _farPlane
         * @return {cc.Camera}
         */
        public static createPerspective (
            _fieldOfView : number, 
            _aspectRatio : number, 
            _nearPlane : number, 
            _farPlane : number 
        ) : cc.Camera;

        /**
         * 
         * @method getDefaultViewport
         * @return {any}
         */
        public static getDefaultViewport (
        ) : any;

        /**
         * 
         * @method setDefaultViewport
         * @param {cc.experimental::Viewport} _vp
         */
        public static setDefaultViewport (
            _vp : any 
        ) : void;

        /**
         * Get the default camera of the current running scene.
         * @method getDefaultCamera
         * @return {cc.Camera}
         */
        public static getDefaultCamera (
        ) : cc.Camera;

        /**
         * 
         * @method Camera
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class CameraBackgroundBrush
     * @native
     */
    export class CameraBackgroundBrush 
    {

        /**
         * get brush type<br>
         * -- return BrushType
         * @method getBrushType
         * @return {number}
         */
        public getBrushType (
        ) : number;

        /**
         * draw the background
         * @method drawBackground
         * @param {cc.Camera} _camera
         */
        public drawBackground (
            _camera : cc.Camera 
        ) : void;

        /**
         * 
         * @method init
         * @return {boolean}
         */
        public init (
        ) : boolean;

        /**
         * 
         * @method isValid
         * @return {boolean}
         */
        public isValid (
        ) : boolean;

        /**
         *  Creates a Skybox brush with 6 textures.<br>
         * -- param positive_x texture for the right side of the texture cube face.<br>
         * -- param negative_x texture for the up side of the texture cube face.<br>
         * -- param positive_y texture for the top side of the texture cube face<br>
         * -- param negative_y texture for the bottom side of the texture cube face<br>
         * -- param positive_z texture for the forward side of the texture cube face.<br>
         * -- param negative_z texture for the rear side of the texture cube face.<br>
         * -- return  A new brush inited with given parameters.
         * @method createSkyboxBrush
         * @param {string} _positive_x
         * @param {string} _negative_x
         * @param {string} _positive_y
         * @param {string} _negative_y
         * @param {string} _positive_z
         * @param {string} _negative_z
         * @return {cc.CameraBackgroundSkyBoxBrush}
         */
        public static createSkyboxBrush (
            _positive_x : string, 
            _negative_x : string, 
            _positive_y : string, 
            _negative_y : string, 
            _positive_z : string, 
            _negative_z : string 
        ) : cc.CameraBackgroundSkyBoxBrush;

        /**
         * Creates a color brush<br>
         * -- param color Color of brush<br>
         * -- param depth Depth used to clear depth buffer<br>
         * -- return Created brush
         * @method createColorBrush
         * @param {ctype.value_type<cc.Color>} _color
         * @param {number} _depth
         * @return {cc.CameraBackgroundColorBrush}
         */
        public static createColorBrush (
            _color : ctype.value_type<cc.Color>, 
            _depth : number 
        ) : cc.CameraBackgroundColorBrush;

        /**
         * Creates a none brush, it does nothing when clear the background<br>
         * -- return Created brush.
         * @method createNoneBrush
         * @return {cc.CameraBackgroundBrush}
         */
        public static createNoneBrush (
        ) : cc.CameraBackgroundBrush;

        /**
         * Creates a depth brush, which clears depth buffer with a given depth.<br>
         * -- param depth Depth used to clear depth buffer<br>
         * -- return Created brush
         * @method createDepthBrush
         * @return {cc.CameraBackgroundDepthBrush}
         */
        public static createDepthBrush (
        ) : cc.CameraBackgroundDepthBrush;

        /**
         * 
         * @method CameraBackgroundBrush
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class CameraBackgroundDepthBrush
     * @native
     */
    export class CameraBackgroundDepthBrush 
        extends cc.CameraBackgroundBrush
    {

        /**
         * Set depth<br>
         * -- param depth Depth used to clear depth buffer
         * @method setDepth
         * @param {number} _depth
         */
        public setDepth (
            _depth : number 
        ) : void;

        /**
         * Create a depth brush<br>
         * -- param depth Depth used to clear the depth buffer<br>
         * -- return Created brush
         * @method create
         * @param {number} _depth
         * @return {cc.CameraBackgroundDepthBrush}
         */
        public static create (
            _depth : number 
        ) : cc.CameraBackgroundDepthBrush;

        /**
         * 
         * @method CameraBackgroundDepthBrush
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class CameraBackgroundColorBrush
     * @native
     */
    export class CameraBackgroundColorBrush 
        extends cc.CameraBackgroundDepthBrush
    {

        /**
         * Set clear color<br>
         * -- param color Color used to clear the color buffer
         * @method setColor
         * @param {ctype.value_type<cc.Color>} _color
         */
        public setColor (
            _color : ctype.value_type<cc.Color> 
        ) : void;

        /**
         * Create a color brush<br>
         * -- param color Color used to clear the color buffer<br>
         * -- param depth Depth used to clear the depth buffer<br>
         * -- return Created brush
         * @method create
         * @param {ctype.value_type<cc.Color>} _color
         * @param {number} _depth
         * @return {cc.CameraBackgroundColorBrush}
         */
        public static create (
            _color : ctype.value_type<cc.Color>, 
            _depth : number 
        ) : cc.CameraBackgroundColorBrush;

        /**
         * 
         * @method CameraBackgroundColorBrush
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class CameraBackgroundSkyBoxBrush
     * @native
     */
    export class CameraBackgroundSkyBoxBrush 
        extends cc.CameraBackgroundBrush
    {

        /**
         * 
         * @method setTextureValid
         * @param {boolean} _valid
         */
        public setTextureValid (
            _valid : boolean 
        ) : void;

        /**
         * Set skybox texture <br>
         * -- param texture Skybox texture
         * @method setTexture
         * @param {cc.TextureCube} _texture
         */
        public setTexture (
            _texture : cc.TextureCube 
        ) : void;

        /**
         * 
         * @method setActived
         * @param {boolean} _actived
         */
        public setActived (
            _actived : boolean 
        ) : void;

        /**
         * 
         * @method isActived
         * @return {boolean}
         */
        public isActived (
        ) : boolean;

        /**
         *  Creates a Skybox brush with 6 textures.
         * @method create
         * @param {(string)} _positive_x?
         * @param {(string)} _negative_x?
         * @param {(string)} _positive_y?
         * @param {(string)} _negative_y?
         * @param {(string)} _positive_z?
         * @param {(string)} _negative_z?
         * @return {cc.CameraBackgroundSkyBoxBrush}
         */
        public static create (
            _positive_x? : (string), 
            _negative_x? : (string), 
            _positive_y? : (string), 
            _negative_y? : (string), 
            _positive_z? : (string), 
            _negative_z? : (string)
        ) : cc.CameraBackgroundSkyBoxBrush;

        /**
         * 
         * @method CameraBackgroundSkyBoxBrush
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class GridBase
     * @native
     */
    export class GridBase 
    {

        /**
         * Set the size of the grid.
         * @method setGridSize
         * @param {ctype.value_type<cc.Size>} _gridSize
         */
        public setGridSize (
            _gridSize : ctype.value_type<cc.Size> 
        ) : void;

        /**
         * brief Set the effect grid rect.<br>
         * -- param rect The effect grid rect.
         * @method setGridRect
         * @param {ctype.value_type<cc.Rect>} _rect
         */
        public setGridRect (
            _rect : ctype.value_type<cc.Rect> 
        ) : void;

        /**
         * 
         * @method afterBlit
         */
        public afterBlit (
        ) : void;

        /**
         * brief Get the effect grid rect.<br>
         * -- return Return the effect grid rect.
         * @method getGridRect
         * @return {ctype.value_type<cc.Rect>}
         */
        public getGridRect (
        ) : ctype.value_type<cc.Rect>;

        /**
         * 
         * @method afterDraw
         * @param {cc.Node} _target
         */
        public afterDraw (
            _target : cc.Node 
        ) : void;

        /**
         * @{<br>
         * -- Init and reset the status when render effects by using the grid.
         * @method beforeDraw
         */
        public beforeDraw (
        ) : void;

        /**
         * Interface, Calculate the vertices used for the blit.
         * @method calculateVertexPoints
         */
        public calculateVertexPoints (
        ) : void;

        /**
         *  is texture flipped. 
         * @method isTextureFlipped
         * @return {boolean}
         */
        public isTextureFlipped (
        ) : boolean;

        /**
         *  Size of the grid. 
         * @method getGridSize
         * @return {ctype.value_type<cc.Size>}
         */
        public getGridSize (
        ) : ctype.value_type<cc.Size>;

        /**
         *  Pixels between the grids. 
         * @method getStep
         * @return {ctype.value_type<cc.Point>}
         */
        public getStep (
        ) : ctype.value_type<cc.Point>;

        /**
         * Change projection to 2D for grabbing.
         * @method set2DProjection
         */
        public set2DProjection (
        ) : void;

        /**
         * Get the pixels between the grids.
         * @method setStep
         * @param {ctype.value_type<cc.Point>} _step
         */
        public setStep (
            _step : ctype.value_type<cc.Point> 
        ) : void;

        /**
         * Set the texture flipped or not.
         * @method setTextureFlipped
         * @param {boolean} _flipped
         */
        public setTextureFlipped (
            _flipped : boolean 
        ) : void;

        /**
         * Interface used to blit the texture with grid to screen.
         * @method blit
         */
        public blit (
        ) : void;

        /**
         * 
         * @method setActive
         * @param {boolean} _active
         */
        public setActive (
            _active : boolean 
        ) : void;

        /**
         *  Get number of times that the grid will be reused. 
         * @method getReuseGrid
         * @return {number}
         */
        public getReuseGrid (
        ) : number;

        /**
         * 
         * @method initWithSize
         * @param {(ctype.value_type<cc.Size>)} _gridSize
         * @param {(ctype.value_type<cc.Rect>) | (cc.Texture2D)} _rect | texture?
         * @param {(boolean)} _flipped?
         * @param {(ctype.value_type<cc.Rect>)} _rect?
         * @return {boolean}
         */
        public initWithSize (
            _gridSize : (ctype.value_type<cc.Size>), 
            _rect_texture? : (ctype.value_type<cc.Rect>) | (cc.Texture2D), 
            _flipped? : (boolean), 
            _rect? : (ctype.value_type<cc.Rect>)
        ) : boolean;

        /**
         * @{<br>
         * -- Interface for custom action when before or after draw.<br>
         * -- js NA
         * @method beforeBlit
         */
        public beforeBlit (
        ) : void;

        /**
         *  Set number of times that the grid will be reused. 
         * @method setReuseGrid
         * @param {number} _reuseGrid
         */
        public setReuseGrid (
            _reuseGrid : number 
        ) : void;

        /**
         * @} @{<br>
         * -- Getter and setter of the active state of the grid.
         * @method isActive
         * @return {boolean}
         */
        public isActive (
        ) : boolean;

        /**
         * Interface, Reuse the grid vertices.
         * @method reuse
         */
        public reuse (
        ) : void;

        /**
         *  create one Grid 
         * @method create
         * @param {(ctype.value_type<cc.Size>)} _gridSize
         * @param {(cc.Texture2D)} _texture?
         * @param {(boolean)} _flipped?
         * @return {cc.GridBase}
         */
        public static create (
            _gridSize : (ctype.value_type<cc.Size>), 
            _texture? : (cc.Texture2D), 
            _flipped? : (boolean)
        ) : cc.GridBase;

    }
    /**
     * @class Grid3D
     * @native
     */
    export class Grid3D 
        extends cc.GridBase
    {

        /**
         * 
         * @method getNeedDepthTestForBlit
         * @return {boolean}
         */
        public getNeedDepthTestForBlit (
        ) : boolean;

        /**
         * @{<br>
         * -- Getter and Setter for depth test state when blit.<br>
         * -- js NA
         * @method setNeedDepthTestForBlit
         * @param {boolean} _neededDepthTest
         */
        public setNeedDepthTestForBlit (
            _neededDepthTest : boolean 
        ) : void;

        /**
         *  create one Grid. 
         * @method create
         * @param {(ctype.value_type<cc.Size>)} _gridSize
         * @param {(ctype.value_type<cc.Rect>) | (cc.Texture2D)} _rect | texture?
         * @param {(boolean)} _flipped?
         * @param {(ctype.value_type<cc.Rect>)} _rect?
         * @return {cc.Grid3D}
         */
        public static create (
            _gridSize : (ctype.value_type<cc.Size>), 
            _rect_texture? : (ctype.value_type<cc.Rect>) | (cc.Texture2D), 
            _flipped? : (boolean), 
            _rect? : (ctype.value_type<cc.Rect>)
        ) : cc.Grid3D;

        /**
         * Constructor.<br>
         * -- js ctor
         * @method Grid3D
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class TiledGrid3D
     * @native
     */
    export class TiledGrid3D 
        extends cc.GridBase
    {

        /**
         *  Create one Grid. 
         * @method create
         * @param {(ctype.value_type<cc.Size>)} _gridSize
         * @param {(ctype.value_type<cc.Rect>) | (cc.Texture2D)} _rect | texture?
         * @param {(boolean)} _flipped?
         * @param {(ctype.value_type<cc.Rect>)} _rect?
         * @return {cc.TiledGrid3D}
         */
        public static create (
            _gridSize : (ctype.value_type<cc.Size>), 
            _rect_texture? : (ctype.value_type<cc.Rect>) | (cc.Texture2D), 
            _flipped? : (boolean), 
            _rect? : (ctype.value_type<cc.Rect>)
        ) : cc.TiledGrid3D;

        /**
         * Constructor.<br>
         * -- js ctor
         * @method TiledGrid3D
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class BaseLight
     * @native
     */
    export abstract class BaseLight 
        extends cc.Node
    {

        /**
         * light enabled getter and setter.
         * @method setEnabled
         * @param {boolean} _enabled
         */
        public setEnabled (
            _enabled : boolean 
        ) : void;

        /**
         *  intensity getter and setter 
         * @method getIntensity
         * @return {number}
         */
        public getIntensity (
        ) : number;

        /**
         * 
         * @method isEnabled
         * @return {boolean}
         */
        public isEnabled (
        ) : boolean;

        /**
         * Get the light type,light type MUST be one of LightType::DIRECTIONAL ,<br>
         * -- LightType::POINT, LightType::SPOT, LightType::AMBIENT.
         * @method getLightType
         * @return {number}
         */
        public getLightType (
        ) : number;

        /**
         * 
         * @method setLightFlag
         * @param {cc.LightFlag} _flag
         */
        public setLightFlag (
            _flag : number 
        ) : void;

        /**
         * 
         * @method setIntensity
         * @param {number} _intensity
         */
        public setIntensity (
            _intensity : number 
        ) : void;

        /**
         * light flag getter and setter
         * @method getLightFlag
         * @return {number}
         */
        public getLightFlag (
        ) : number;

    }
    /**
     * @class DirectionLight
     * @native
     */
    export class DirectionLight 
        extends cc.BaseLight
    {

        /**
         * Returns the Direction in parent.
         * @method getDirection
         * @return {ctype.value_type<cc.Vec3>}
         */
        public getDirection (
        ) : ctype.value_type<cc.Vec3>;

        /**
         * Returns direction in world.
         * @method getDirectionInWorld
         * @return {ctype.value_type<cc.Vec3>}
         */
        public getDirectionInWorld (
        ) : ctype.value_type<cc.Vec3>;

        /**
         * Sets the Direction in parent.<br>
         * -- param dir The Direction in parent.
         * @method setDirection
         * @param {ctype.value_type<cc.Vec3>} _dir
         */
        public setDirection (
            _dir : ctype.value_type<cc.Vec3> 
        ) : void;

        /**
         * Creates a direction light.<br>
         * -- param direction The light's direction<br>
         * -- param color The light's color.<br>
         * -- return The new direction light.
         * @method create
         * @param {ctype.value_type<cc.Vec3>} _direction
         * @param {ctype.value_type<cc.Color>} _color
         * @return {cc.DirectionLight}
         */
        public static create (
            _direction : ctype.value_type<cc.Vec3>, 
            _color : ctype.value_type<cc.Color> 
        ) : cc.DirectionLight;

        /**
         * 
         * @method DirectionLight
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class PointLight
     * @native
     */
    export class PointLight 
        extends cc.BaseLight
    {

        /**
         *  get or set range 
         * @method getRange
         * @return {number}
         */
        public getRange (
        ) : number;

        /**
         * 
         * @method setRange
         * @param {number} _range
         */
        public setRange (
            _range : number 
        ) : void;

        /**
         * Creates a point light.<br>
         * -- param position The light's position<br>
         * -- param color The light's color.<br>
         * -- param range The light's range.<br>
         * -- return The new point light.
         * @method create
         * @param {ctype.value_type<cc.Vec3>} _position
         * @param {ctype.value_type<cc.Color>} _color
         * @param {number} _range
         * @return {ctype.value_type<cc.Point>}
         */
        public static create (
            _position : ctype.value_type<cc.Vec3>, 
            _color : ctype.value_type<cc.Color>, 
            _range : number 
        ) : ctype.value_type<cc.Point>;

        /**
         * 
         * @method PointLight
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class SpotLight
     * @native
     */
    export class SpotLight 
        extends cc.BaseLight
    {

        /**
         * Returns the range of point or spot light.<br>
         * -- return The range of the point or spot light.
         * @method getRange
         * @return {number}
         */
        public getRange (
        ) : number;

        /**
         * Sets the Direction in parent.<br>
         * -- param dir The Direction in parent.
         * @method setDirection
         * @param {ctype.value_type<cc.Vec3>} _dir
         */
        public setDirection (
            _dir : ctype.value_type<cc.Vec3> 
        ) : void;

        /**
         *  get cos innerAngle 
         * @method getCosInnerAngle
         * @return {number}
         */
        public getCosInnerAngle (
        ) : number;

        /**
         * Returns the outer angle of the spot light (in radians).
         * @method getOuterAngle
         * @return {number}
         */
        public getOuterAngle (
        ) : number;

        /**
         * Returns the inner angle the spot light (in radians).
         * @method getInnerAngle
         * @return {number}
         */
        public getInnerAngle (
        ) : number;

        /**
         * Returns the Direction in parent.
         * @method getDirection
         * @return {ctype.value_type<cc.Vec3>}
         */
        public getDirection (
        ) : ctype.value_type<cc.Vec3>;

        /**
         *  get cos outAngle 
         * @method getCosOuterAngle
         * @return {number}
         */
        public getCosOuterAngle (
        ) : number;

        /**
         * Sets the outer angle of a spot light (in radians).<br>
         * -- param outerAngle The angle of spot light (in radians).
         * @method setOuterAngle
         * @param {number} _angle
         */
        public setOuterAngle (
            _angle : number 
        ) : void;

        /**
         * Sets the inner angle of a spot light (in radians).<br>
         * -- param angle The angle of spot light (in radians).
         * @method setInnerAngle
         * @param {number} _angle
         */
        public setInnerAngle (
            _angle : number 
        ) : void;

        /**
         * Returns direction in world.
         * @method getDirectionInWorld
         * @return {ctype.value_type<cc.Vec3>}
         */
        public getDirectionInWorld (
        ) : ctype.value_type<cc.Vec3>;

        /**
         * Sets the range of point or spot light.<br>
         * -- param range The range of point or spot light.
         * @method setRange
         * @param {number} _range
         */
        public setRange (
            _range : number 
        ) : void;

        /**
         * Creates a spot light.<br>
         * -- param direction The light's direction<br>
         * -- param position The light's position<br>
         * -- param color The light's color.<br>
         * -- param innerAngle The light's inner angle (in radians).<br>
         * -- param outerAngle The light's outer angle (in radians).<br>
         * -- param range The light's range.<br>
         * -- return The new spot light.
         * @method create
         * @param {ctype.value_type<cc.Vec3>} _direction
         * @param {ctype.value_type<cc.Vec3>} _position
         * @param {ctype.value_type<cc.Color>} _color
         * @param {number} _innerAngle
         * @param {number} _outerAngle
         * @param {number} _range
         * @return {cc.SpotLight}
         */
        public static create (
            _direction : ctype.value_type<cc.Vec3>, 
            _position : ctype.value_type<cc.Vec3>, 
            _color : ctype.value_type<cc.Color>, 
            _innerAngle : number, 
            _outerAngle : number, 
            _range : number 
        ) : cc.SpotLight;

        /**
         * 
         * @method SpotLight
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class AmbientLight
     * @native
     */
    export class AmbientLight 
        extends cc.BaseLight
    {

        /**
         * Creates a ambient light.<br>
         * -- param color The light's color.<br>
         * -- return The new ambient light.
         * @method create
         * @param {ctype.value_type<cc.Color>} _color
         * @return {cc.AmbientLight}
         */
        public static create (
            _color : ctype.value_type<cc.Color> 
        ) : cc.AmbientLight;

        /**
         * 
         * @method AmbientLight
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class GLProgram
     * @native
     */
    export class GLProgram 
    {
        
        // *****************************************************************
        // *****************************************************************
        // ******IMPORTANT NOTE: *******************************************
        // This section is relay on 
        // cocos2d-x/cocos/scripting/js-bindings/manual/cocos2d_specifics.cpp
        // *****************************************************************
        // *****************************************************************
        
        /**
         * Currently JavaScript Bindings (JSB), in some cases, needs to use retain and release. This is a bug in JSB,
         * and the ugly workaround is to use retain/release. So, these 2 methods were added to be compatible with JSB.
         * This is a hack, and should be removed once JSB fixes the retain/release bug
         * You will need to retain an object if you created an engine object and haven't added it into the scene graph during the same frame.
         * Otherwise, JSB's native autorelease pool will consider this object a useless one and release it directly,
         * when you want to use it later, a "Invalid Native Object" error will be raised.
         * The retain function can increase a reference count for the native object to avoid it being released,
         * you need to manually invoke release function when you think this object is no longer needed, otherwise, there will be memory learks.
         * retain and release function call should be paired in developer's game code.
         * @function
         * @see cc.Node#release
         */
        public retain():void;

        /**
         * Currently JavaScript Bindings (JSB), in some cases, needs to use retain and release. This is a bug in JSB,
         * and the ugly workaround is to use retain/release. So, these 2 methods were added to be compatible with JSB.
         * This is a hack, and should be removed once JSB fixes the retain/release bug
         * You will need to retain an object if you created an engine object and haven't added it into the scene graph during the same frame.
         * Otherwise, JSB's native autorelease pool will consider this object a useless one and release it directly,
         * when you want to use it later, a "Invalid Native Object" error will be raised.
         * The retain function can increase a reference count for the native object to avoid it being released,
         * you need to manually invoke release function when you think this object is no longer needed, otherwise, there will be memory learks.
         * retain and release function call should be paired in developer's game code.
         * @function
         * @see cc.Node#retain
         */
        public release():void;
        
        public getProgram():number;
        
        /**
         * calls glUniform4fv only if the values are different than the previous call for this same shader program.
         */
        public setUniformLocationWithMatrix2fv(location : number, floats : number[], numberOfArrays : number) : void;
        
        /**
         * calls glUniform4fv only if the values are different than the previous call for this same shader program.
         */
        public setUniformLocationWithMatrix3fv(location : number, floats : number[], numberOfArrays : number) : void;
        
        /**
         * calls glUniform4fv only if the values are different than the previous call for this same shader program.
         */
        public setUniformLocationWithMatrix4fv(location : number, floats : number[], numberOfArrays : number) : void;
        
        // *****************************************************************
        // *****************************************************************
        // *****************************************************************
        // *****************************************************************
        // *****************************************************************
        

        /**
         *  returns the fragmentShader error log 
         * @method getFragmentShaderLog
         * @return {string}
         */
        public getFragmentShaderLog (
        ) : string;

        /**
         *   It will add a new attribute to the shader by calling glBindAttribLocation. 
         * @method bindAttribLocation
         * @param {string} _attributeName
         * @param {number} _index
         */
        public addAttribute (
            _attributeName : string, 
            _index : number 
        ) : void;

        /**
         *  returns the Uniform flags 
         * @method getUniformFlags
         * @return {any}
         */
        public getUniformFlags (
        ) : any;

        /**
         *  calls retrieves the named uniform location for this shader program. 
         * @method getUniformLocationForName
         * @param {string} _name
         * @return {number}
         */
        public getUniformLocationForName (
            _name : string 
        ) : number;

        /**
         *  it will call glUseProgram() 
         * @method use
         */
        public use (
        ) : void;

        /**
         *  returns the vertexShader error log 
         * @method getVertexShaderLog
         * @return {string}
         */
        public getVertexShaderLog (
        ) : string;

        /**
         * @{ Get the uniform or vertex attribute by string name in shader, return null if it does not exist.
         * @method getUniform
         * @param {string} _name
         * @return {cc.Uniform}
         */
        public getUniform (
            _name : string 
        ) : cc.Uniform;

        /**
         * 
         * @method initWithByteArrays
         * @param {(string)} _vShaderByteArray
         * @param {(string)} _fShaderByteArray
         * @param {(string)} _compileTimeDefines | compileTimeHeaders?
         * @param {(string)} _compileTimeDefines?
         * @return {boolean}
         */
        public initWithString (
            _vShaderByteArray : (string), 
            _fShaderByteArray : (string), 
            _compileTimeDefines_compileTimeHeaders? : (string), 
            _compileTimeDefines? : (string)
        ) : boolean;

        /**
         *  calls glUniform1f only if the values are different than the previous call for this same shader program.<br>
         * -- In js or lua,please use setUniformLocationF32<br>
         * -- js NA
         * @method setUniformLocationWith1f
         * @param {number} _location
         * @param {number} _f1
         */
        public setUniformLocationWith1f (
            _location : number, 
            _f1 : number 
        ) : void;

        /**
         * 
         * @method initWithFilenames
         * @param {(string)} _vShaderFilename
         * @param {(string)} _fShaderFilename
         * @param {(string)} _compileTimeDefines | compileTimeHeaders?
         * @param {(string)} _compileTimeDefines?
         * @return {boolean}
         */
        public init (
            _vShaderFilename : (string), 
            _fShaderFilename : (string), 
            _compileTimeDefines_compileTimeHeaders? : (string), 
            _compileTimeDefines? : (string)
        ) : boolean;

        /**
         *  calls glUniform3f only if the values are different than the previous call for this same shader program.<br>
         * -- In js or lua,please use setUniformLocationF32<br>
         * -- js NA
         * @method setUniformLocationWith3f
         * @param {number} _location
         * @param {number} _f1
         * @param {number} _f2
         * @param {number} _f3
         */
        public setUniformLocationWith3f (
            _location : number, 
            _f1 : number, 
            _f2 : number, 
            _f3 : number 
        ) : void;

        /**
         * Update the builtin uniforms if they are different than the previous call for this same shader program.
         * @method setUniformsForBuiltins
         * @param {(ctype.value_type<cc.Mat4>)} _modelView?
         */
        public setUniformsForBuiltins (
            _modelView? : (ctype.value_type<cc.Mat4>)
        ) : void;

        /**
         *  calls glUniform3i only if the values are different than the previous call for this same shader program. 
         * @method setUniformLocationWith3i
         * @param {number} _location
         * @param {number} _i1
         * @param {number} _i2
         * @param {number} _i3
         */
        public setUniformLocationWith3i (
            _location : number, 
            _i1 : number, 
            _i2 : number, 
            _i3 : number 
        ) : void;

        /**
         *  calls glUniform4f only if the values are different than the previous call for this same shader program.<br>
         * -- In js or lua,please use setUniformLocationF32<br>
         * -- js NA
         * @method setUniformLocationWith4f
         * @param {number} _location
         * @param {number} _f1
         * @param {number} _f2
         * @param {number} _f3
         * @param {number} _f4
         */
        public setUniformLocationWith4f (
            _location : number, 
            _f1 : number, 
            _f2 : number, 
            _f3 : number, 
            _f4 : number 
        ) : void;

        /**
         *  It will create 4 uniforms:<br>
         * -- - kUniformPMatrix<br>
         * -- - kUniformMVMatrix<br>
         * -- - kUniformMVPMatrix<br>
         * -- - GLProgram::UNIFORM_SAMPLER<br>
         * -- And it will bind "GLProgram::UNIFORM_SAMPLER" to 0
         * @method updateUniforms
         */
        public updateUniforms (
        ) : void;

        /**
         *  Calls glGetUniformLocation(). 
         * @method getUniformLocation
         * @param {string} _attributeName
         * @return {number}
         */
        public getUniformLocation (
            _attributeName : string 
        ) : number;

        /**
         *  links the glProgram 
         * @method link
         * @return {boolean}
         */
        public link (
        ) : boolean;

        /**
         *  Reload all shaders, this function is designed for android<br>
         * -- when opengl context lost, so don't call it.
         * @method reset
         */
        public reset (
        ) : void;

        /**
         *  Calls glGetAttribLocation. 
         * @method getAttribLocation
         * @param {string} _attributeName
         * @return {number}
         */
        public getAttribLocation (
            _attributeName : string 
        ) : number;

        /**
         * 
         * @method getVertexAttrib
         * @param {string} _name
         * @return {cc.VertexAttrib}
         */
        public getVertexAttrib (
            _name : string 
        ) : cc.VertexAttrib;

        /**
         *  calls glUniform2f only if the values are different than the previous call for this same shader program.<br>
         * -- In js or lua,please use setUniformLocationF32<br>
         * -- js NA
         * @method setUniformLocationWith2f
         * @param {number} _location
         * @param {number} _f1
         * @param {number} _f2
         */
        public setUniformLocationWith2f (
            _location : number, 
            _f1 : number, 
            _f2 : number 
        ) : void;

        /**
         *  calls glUniform4i only if the values are different than the previous call for this same shader program. 
         * @method setUniformLocationWith4i
         * @param {number} _location
         * @param {number} _i1
         * @param {number} _i2
         * @param {number} _i3
         * @param {number} _i4
         */
        public setUniformLocationWith4i (
            _location : number, 
            _i1 : number, 
            _i2 : number, 
            _i3 : number, 
            _i4 : number 
        ) : void;

        /**
         *  calls glUniform1i only if the values are different than the previous call for this same shader program.<br>
         * -- js setUniformLocationI32<br>
         * -- lua setUniformLocationI32
         * @method setUniformLocationWith1i
         * @param {number} _location
         * @param {number} _i1
         */
        public setUniformLocationI32 (
            _location : number, 
            _i1 : number 
        ) : void;

        /**
         *  calls glUniform2i only if the values are different than the previous call for this same shader program. 
         * @method setUniformLocationWith2i
         * @param {number} _location
         * @param {number} _i1
         * @param {number} _i2
         */
        public setUniformLocationWith2i (
            _location : number, 
            _i1 : number, 
            _i2 : number 
        ) : void;

        /**
         * 
         * @method createWithByteArrays
         * @param {(string)} _vShaderByteArray
         * @param {(string)} _fShaderByteArray
         * @param {(string)} _compileTimeDefines | compileTimeHeaders?
         * @param {(string)} _compileTimeDefines?
         * @return {cc.GLProgram}
         */
        public static createWithByteArrays (
            _vShaderByteArray : (string), 
            _fShaderByteArray : (string), 
            _compileTimeDefines_compileTimeHeaders? : (string), 
            _compileTimeDefines? : (string)
        ) : cc.GLProgram;

        /**
         * 
         * @method createWithFilenames
         * @param {(string)} _vShaderFilename
         * @param {(string)} _fShaderFilename
         * @param {(string)} _compileTimeDefines | compileTimeHeaders?
         * @param {(string)} _compileTimeDefines?
         * @return {cc.GLProgram}
         */
        public static createWithFilenames (
            _vShaderFilename : (string), 
            _fShaderFilename : (string), 
            _compileTimeDefines_compileTimeHeaders? : (string), 
            _compileTimeDefines? : (string)
        ) : cc.GLProgram;

        /**
         * Constructor.
         * @method GLProgram
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class GLProgramCache
     * @native
     */
    export class ShaderCache 
    {

        /**
         *  loads the default shaders 
         * @method loadDefaultGLPrograms
         */
        public loadDefaultShaders (
        ) : void;

        /**
         *  reload default programs these are relative to light 
         * @method reloadDefaultGLProgramsRelativeToLights
         */
        public reloadDefaultGLProgramsRelativeToLights (
        ) : void;

        /**
         *  adds a GLProgram to the cache for a given name 
         * @method addGLProgram
         * @param {cc.GLProgram} _program
         * @param {string} _key
         */
        public addProgram (
            _program : cc.GLProgram, 
            _key : string 
        ) : void;

        /**
         *  reload the default shaders 
         * @method reloadDefaultGLPrograms
         */
        public reloadDefaultShaders (
        ) : void;

        /**
         *  returns a GL program for a given key 
         * @method getGLProgram
         * @param {string} _key
         * @return {cc.GLProgram}
         */
        public getProgram (
            _key : string 
        ) : cc.GLProgram;

        /**
         *  purges the cache. It releases the retained instance. 
         * @method destroyInstance
         */
        public static destroyInstance (
        ) : void;

        /**
         *  returns the shared instance 
         * @method getInstance
         * @return {cc.GLProgramCache}
         */
        public static getInstance (
        ) : cc.GLProgramCache;

        /**
         * Constructor.<br>
         * -- js ctor
         * @method GLProgramCache
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class RenderState
     * @native
     */
    export class RenderState 
    {

        /**
         *  Texture that will use in the CC_Texture0 uniform.<br>
         * -- Added to be backwards compatible. Use Samplers from .material instead.
         * @method setTexture
         * @param {cc.Texture2D} _texture
         */
        public setTexture (
            _texture : cc.Texture2D 
        ) : void;

        /**
         * Returns the topmost RenderState in the hierarchy below the given RenderState.
         * @method getTopmost
         * @param {cc.RenderState} _below
         * @return {cc.RenderState}
         */
        public getTopmost (
            _below : cc.RenderState 
        ) : cc.RenderState;

        /**
         *  Returns the texture that is going to be used for CC_Texture0.<br>
         * -- Added to be backwards compatible.
         * @method getTexture
         * @return {cc.Texture2D}
         */
        public getTexture (
        ) : cc.Texture2D;

        /**
         * Binds the render state for this RenderState and any of its parents, top-down,<br>
         * -- for the given pass.
         * @method bind
         * @param {cc.Pass} _pass
         */
        public bind (
            _pass : cc.Pass 
        ) : void;

        /**
         * 
         * @method getName
         * @return {string}
         */
        public getName (
        ) : string;

        /**
         * 
         * @method getStateBlock
         * @return {any}
         */
        public getStateBlock (
        ) : any;

        /**
         * 
         * @method setParent
         * @param {cc.RenderState} _parent
         */
        public setParent (
            _parent : cc.RenderState 
        ) : void;

        /**
         * Static initializer that is called during game startup.
         * @method initialize
         */
        public static initialize (
        ) : void;

        /**
         * Static finalizer that is called during game shutdown.
         * @method finalize
         */
        public static finalize (
        ) : void;

    }
    /**
     * @class Pass
     * @native
     */
    export class Pass 
        extends cc.RenderState
    {

        /**
         *  Unbinds the Pass.<br>
         * -- This method must be called AFTER calling the actual draw call
         * @method unbind
         */
        public unbind (
        ) : void;

        /**
         * 
         * @method bind
         * @param {(ctype.value_type<cc.Mat4>)} _modelView
         * @param {(boolean)} _bindAttributes?
         */
        public bind (
            _modelView : (ctype.value_type<cc.Mat4>), 
            _bindAttributes? : (boolean)
        ) : void;

        /**
         * Returns a clone (deep-copy) of this instance 
         * @method clone
         * @return {cc.Pass}
         */
        public clone (
        ) : cc.Pass;

        /**
         *  Returns the GLProgramState 
         * @method getGLProgramState
         * @return {cc.GLProgramState}
         */
        public getGLProgramState (
        ) : cc.GLProgramState;

        /**
         * Returns the vertex attribute binding for this pass.<br>
         * -- return The vertex attribute binding for this pass.
         * @method getVertexAttributeBinding
         * @return {cc.VertexAttribBinding}
         */
        public getVertexAttributeBinding (
        ) : cc.VertexAttribBinding;

        /**
         * 
         * @method getHash
         * @return {number}
         */
        public getHash (
        ) : number;

        /**
         * Sets a vertex attribute binding for this pass.<br>
         * -- When a mesh binding is set, the VertexAttribBinding will be automatically<br>
         * -- bound when the bind() method is called for the pass.<br>
         * -- param binding The VertexAttribBinding to set (or NULL to remove an existing binding).
         * @method setVertexAttribBinding
         * @param {cc.VertexAttribBinding} _binding
         */
        public setVertexAttribBinding (
            _binding : cc.VertexAttribBinding 
        ) : void;

        /**
         * 
         * @method create
         * @param {cc.Technique} _parent
         * @return {cc.Pass}
         */
        public static create (
            _parent : cc.Technique 
        ) : cc.Pass;

        /**
         *  Creates a Pass with a GLProgramState.
         * @method createWithGLProgramState
         * @param {cc.Technique} _parent
         * @param {cc.GLProgramState} _programState
         * @return {cc.Pass}
         */
        public static createWithGLProgramState (
            _parent : cc.Technique, 
            _programState : cc.GLProgramState 
        ) : cc.Pass;

    }
    /**
     * @class Technique
     * @native
     */
    export class Technique 
        extends cc.RenderState
    {

        /**
         *  Returns the number of Passes in the Technique 
         * @method getPassCount
         * @return {number}
         */
        public getPassCount (
        ) : number;

        /**
         *  Returns a new clone of the Technique 
         * @method clone
         * @return {cc.Technique}
         */
        public clone (
        ) : cc.Technique;

        /**
         *  Adds a new pass to the Technique.<br>
         * -- Order matters. First added, first rendered
         * @method addPass
         * @param {cc.Pass} _pass
         */
        public addPass (
            _pass : cc.Pass 
        ) : void;

        /**
         *  Returns the list of passes 
         * @method getPasses
         * @return {cc.Pass[]}
         */
        public getPasses (
        ) : cc.Pass[];

        /**
         *  Returns the name of the Technique 
         * @method getName
         * @return {string}
         */
        public getName (
        ) : string;

        /**
         *  Returns the Pass at given index 
         * @method getPassByIndex
         * @param {number} _index
         * @return {cc.Pass}
         */
        public getPassByIndex (
            _index : number 
        ) : cc.Pass;

        /**
         * 
         * @method create
         * @param {cc.Material} _parent
         * @return {cc.Technique}
         */
        public static create (
            _parent : cc.Material 
        ) : cc.Technique;

        /**
         *  Creates a new Technique with a GLProgramState.<br>
         * -- Method added to support legacy code
         * @method createWithGLProgramState
         * @param {cc.Material} _parent
         * @param {cc.GLProgramState} _state
         * @return {cc.Technique}
         */
        public static createWithGLProgramState (
            _parent : cc.Material, 
            _state : cc.GLProgramState 
        ) : cc.Technique;

    }
    /**
     * @class Material
     * @native
     */
    export class Material 
        extends cc.RenderState
    {

        /**
         *  returns a clone (deep-copy) of the material 
         * @method clone
         * @return {cc.Material}
         */
        public clone (
        ) : cc.Material;

        /**
         *  Returns the number of Techniques in the Material. 
         * @method getTechniqueCount
         * @return {number}
         */
        public getTechniqueCount (
        ) : number;

        /**
         * / sets the material name
         * @method setName
         * @param {string} _name
         */
        public setName (
            _name : string 
        ) : void;

        /**
         *  Returns a Technique by index. <br>
         * -- returns `nullptr` if the index is invalid.
         * @method getTechniqueByIndex
         * @param {number} _index
         * @return {cc.Technique}
         */
        public getTechniqueByIndex (
            _index : number 
        ) : cc.Technique;

        /**
         * / returns the material name
         * @method getName
         * @return {string}
         */
        public getName (
        ) : string;

        /**
         *  Returns the list of Techniques 
         * @method getTechniques
         * @return {cc.Technique[]}
         */
        public getTechniques (
        ) : cc.Technique[];

        /**
         *  Sets the current technique 
         * @method setTechnique
         * @param {string} _techniqueName
         */
        public setTechnique (
            _techniqueName : string 
        ) : void;

        /**
         *  Returns a Technique by its name.<br>
         * -- returns `nullptr` if the Technique can't be found.
         * @method getTechniqueByName
         * @param {string} _name
         * @return {cc.Technique}
         */
        public getTechniqueByName (
            _name : string 
        ) : cc.Technique;

        /**
         *  Adds a Technique into the Material 
         * @method addTechnique
         * @param {cc.Technique} _technique
         */
        public addTechnique (
            _technique : cc.Technique 
        ) : void;

        /**
         *  Returns the Technique used by the Material 
         * @method getTechnique
         * @return {cc.Technique}
         */
        public getTechnique (
        ) : cc.Technique;

        /**
         * Creates a Material using the data from the Properties object defined at the specified URL,<br>
         * -- where the URL is of the format "<file-path>.<extension>#<namespace-id>/<namespace-id>/.../<namespace-id>"<br>
         * -- (and "#<namespace-id>/<namespace-id>/.../<namespace-id>" is optional).<br>
         * -- param url The URL pointing to the Properties object defining the material.<br>
         * -- return A new Material or NULL if there was an error.
         * @method createWithFilename
         * @param {string} _path
         * @return {cc.Material}
         */
        public static createWithFilename (
            _path : string 
        ) : cc.Material;

        /**
         *  Creates a Material with a GLProgramState.<br>
         * -- It will only contain one Technique and one Pass.<br>
         * -- Added in order to support legacy code.
         * @method createWithGLStateProgram
         * @param {cc.GLProgramState} _programState
         * @return {cc.Material}
         */
        public static createWithGLStateProgram (
            _programState : cc.GLProgramState 
        ) : cc.Material;

        /**
         * Creates a material from the specified properties object.<br>
         * -- param materialProperties The properties object defining the<br>
         * -- material (must have namespace equal to 'material').<br>
         * -- return A new Material.
         * @method createWithProperties
         * @param {cc.Properties} _materialProperties
         * @return {cc.Material}
         */
        public static createWithProperties (
            _materialProperties : cc.Properties 
        ) : cc.Material;

    }
    /**
     * @class TextureCache
     * @native
     */
    export class TextureCache 
    {

        /**
         *  Reload texture from the image file.<br>
         * -- If the file image hasn't loaded before, load it.<br>
         * -- Otherwise the texture will be reloaded from the file image.<br>
         * -- param fileName It's the related/absolute path of the file image.<br>
         * -- return True if the reloading is succeed, otherwise return false.
         * @method reloadTexture
         * @param {string} _fileName
         * @return {boolean}
         */
        public reloadTexture (
            _fileName : string 
        ) : boolean;

        /**
         *  Unbind all bound image asynchronous load callbacks.<br>
         * -- since v3.1
         * @method unbindAllImageAsync
         */
        public unbindAllImageAsync (
        ) : void;

        /**
         *  Deletes a texture from the cache given a its key name.<br>
         * -- param key It's the related/absolute path of the file image.<br>
         * -- since v0.99.4
         * @method removeTextureForKey
         * @param {string} _key
         */
        public removeTextureForKey (
            _key : string 
        ) : void;

        /**
         *  Purges the dictionary of loaded textures.<br>
         * -- Call this method if you receive the "Memory Warning".<br>
         * -- In the short term: it will free some resources preventing your app from being killed.<br>
         * -- In the medium term: it will allocate more resources.<br>
         * -- In the long term: it will be the same.
         * @method removeAllTextures
         */
        public removeAllTextures (
        ) : void;

        /**
         *  Returns a Texture2D object given a file image.<br>
         * -- If the file image was not previously loaded, it will create a new Texture2D object and it will return it.<br>
         * -- Otherwise it will load a texture in a new thread, and when the image is loaded, the callback will be called with the Texture2D as a parameter.<br>
         * -- The callback will be called from the main thread, so it is safe to create any cocos2d object from the callback.<br>
         * -- Supported image extensions: .png, .jpg<br>
         * -- param filepath A null terminated string.<br>
         * -- param callback A callback function would be invoked after the image is loaded.<br>
         * -- since v0.8
         * @method addImageAsync
         * @param {string} _filepath
         * @param {(arg0:cc.Texture2D) => void} _callback
         */
        public addImageAsync (
            _filepath : string, 
            _callback : (arg0:cc.Texture2D) => void 
        ) : void;

        /**
         * js NA<br>
         * -- lua NA
         * @method getDescription
         * @return {string}
         */
        public getDescription (
        ) : string;

        /**
         *  Output to CCLOG the current contents of this TextureCache.<br>
         * -- This will attempt to calculate the size of each texture, and the total texture memory in use.<br>
         * -- since v1.0
         * @method getCachedTextureInfo
         * @return {string}
         */
        public getCachedTextureInfo (
        ) : string;

        /**
         *  Returns a Texture2D object given an Image.<br>
         * -- If the image was not previously loaded, it will create a new Texture2D object and it will return it.<br>
         * -- Otherwise it will return a reference of a previously loaded image.<br>
         * -- param key The "key" parameter will be used as the "key" for the cache.<br>
         * -- If "key" is nil, then a new texture will be created each time.
         * @method addImage
         * @param {(cc.Image) | (string)} _image | filepath
         * @param {(string)} _key?
         * @return {cc.Texture2D}
         */
        public addImage (
            _image_filepath : (cc.Image) | (string), 
            _key? : (string)
        ) : cc.Texture2D;

        /**
         *  Unbind a specified bound image asynchronous callback.<br>
         * -- In the case an object who was bound to an image asynchronous callback was destroyed before the callback is invoked,<br>
         * -- the object always need to unbind this callback manually.<br>
         * -- param filename It's the related/absolute path of the file image.<br>
         * -- since v3.1
         * @method unbindImageAsync
         * @param {string} _filename
         */
        public unbindImageAsync (
            _filename : string 
        ) : void;

        /**
         *  Returns an already created texture. Returns nil if the texture doesn't exist.<br>
         * -- param key It's the related/absolute path of the file image.<br>
         * -- since v0.99.5
         * @method getTextureForKey
         * @param {string} _key
         * @return {cc.Texture2D}
         */
        public getTextureForKey (
            _key : string 
        ) : cc.Texture2D;

        /**
         * Get the file path of the texture<br>
         * -- param texture A Texture2D object pointer.<br>
         * -- return The full path of the file.
         * @method getTextureFilePath
         * @param {cc.Texture2D} _texture
         * @return {string}
         */
        public getTextureFilePath (
            _texture : cc.Texture2D 
        ) : string;

        /**
         *  Reload texture from a new file.<br>
         * -- This function is mainly for editor, won't suggest use it in game for performance reason.<br>
         * -- param srcName Original texture file name.<br>
         * -- param dstName New texture file name.<br>
         * -- since v3.10
         * @method renameTextureWithKey
         * @param {string} _srcName
         * @param {string} _dstName
         */
        public renameTextureWithKey (
            _srcName : string, 
            _dstName : string 
        ) : void;

        /**
         *  Removes unused textures.<br>
         * -- Textures that have a retain count of 1 will be deleted.<br>
         * -- It is convenient to call this method after when starting a new Scene.<br>
         * -- since v0.8
         * @method removeUnusedTextures
         */
        public removeUnusedTextures (
        ) : void;

        /**
         *  Deletes a texture from the cache given a texture.
         * @method removeTexture
         * @param {cc.Texture2D} _texture
         */
        public removeTexture (
            _texture : cc.Texture2D 
        ) : void;

        /**
         * Called by director, please do not called outside.
         * @method waitForQuit
         */
        public waitForQuit (
        ) : void;

        /**
         * 
         * @method getCachedTextureSummarizeInfo
         * @return {string}
         */
        public getCachedTextureSummarizeInfo (
        ) : string;

        /**
         * js ctor
         * @method TextureCache
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class Device
     * @native
     */
    export class Device 
    {

        /**
         * To enable or disable accelerometer.
         * @method setAccelerometerEnabled
         * @param {boolean} _isEnabled
         */
        public static setAccelerometerEnabled (
            _isEnabled : boolean 
        ) : void;

        /**
         * Sets the interval of accelerometer.
         * @method setAccelerometerInterval
         * @param {number} _interval
         */
        public static setAccelerometerInterval (
            _interval : number 
        ) : void;

        /**
         * Vibrate for the specified amount of time.<br>
         * -- If vibrate is not supported, then invoking this method has no effect.<br>
         * -- Some platforms limit to a maximum duration of 5 seconds.<br>
         * -- Duration is ignored on iOS due to API limitations.<br>
         * -- param duration The duration in seconds.
         * @method vibrate
         * @param {number} _duration
         */
        public static vibrate (
            _duration : number 
        ) : void;

        /**
         * Controls whether the screen should remain on.<br>
         * -- param keepScreenOn One flag indicating that the screen should remain on.
         * @method setKeepScreenOn
         * @param {boolean} _keepScreenOn
         */
        public static setKeepScreenOn (
            _keepScreenOn : boolean 
        ) : void;

        /**
         * Gets the DPI of device<br>
         * -- return The DPI of device.
         * @method getDPI
         * @return {number}
         */
        public static getDPI (
        ) : number;

        /**
         * Get device 'unique' identifier.
         * @method getIdentifier
         * @return {string}
         */
        public static getIdentifier (
        ) : string;

        /**
         * Get device os version.
         * @method getOSVersion
         * @return {string}
         */
        public static getOSVersion (
        ) : string;

    }
    /**
     * @class SAXParser
     * @native
     */
    export abstract class PlistParser 
    {

        public parse(data: string):any;

        /**
         * js NA<br>
         * -- lua NA
         * @method init
         * @param {string} _encoding
         * @return {boolean}
         */
        public init (
            _encoding : string 
        ) : boolean;

    }
    /**
     * @class Application
     * @native
     */
    export abstract class Application 
    {

        /**
         * brief Get target platform
         * @method getTargetPlatform
         * @return {number}
         */
        public getTargetPlatform (
        ) : number;

        /**
         * brief Get current language config<br>
         * -- return Current language config
         * @method getCurrentLanguage
         * @return {number}
         */
        public getCurrentLanguage (
        ) : number;

        /**
         * brief Get application store identifier.
         * @method getPackageIdentifier
         * @return {string}
         */
        public getPackageIdentifier (
        ) : string;

        /**
         * brief Get application version.
         * @method getVersion
         * @return {string}
         */
        public getVersion (
        ) : string;

        /**
         * brief Open url in default browser<br>
         * -- param String with url to open.<br>
         * -- return true if the resource located by the URL was successfully opened; otherwise false.
         * @method openURL
         * @param {string} _url
         * @return {boolean}
         */
        public openURL (
            _url : string 
        ) : boolean;

        /**
         * brief    Get current application instance.<br>
         * -- return Current application instance pointer.
         * @method getInstance
         * @return {cc.Application}
         */
        public static getInstance (
        ) : cc.Application;

    }
    /**
     * @class AnimationCache
     * @native
     */
    export class AnimationCache 
    {

        /**
         *  Returns a Animation that was previously added.<br>
         * -- If the name is not found it will return nil.<br>
         * -- You should retain the returned copy if you are going to use it.<br>
         * -- return A Animation that was previously added. If the name is not found it will return nil.
         * @method getAnimation
         * @param {string} _name
         * @return {cc.Animation}
         */
        public getAnimation (
            _name : string 
        ) : cc.Animation;

        /**
         *  Adds a Animation with a name.<br>
         * -- param animation An animation.<br>
         * -- param name The name of animation.
         * @method addAnimation
         * @param {cc.Animation} _animation
         * @param {string} _name
         */
        public addAnimation (
            _animation : cc.Animation, 
            _name : string 
        ) : void;

        /**
         * 
         * @method init
         * @return {boolean}
         */
        public init (
        ) : boolean;

        /**
         *  Adds an animation from an NSDictionary.<br>
         * -- Make sure that the frames were previously loaded in the SpriteFrameCache.<br>
         * -- param dictionary An NSDictionary.<br>
         * -- param plist The path of the relative file,it use to find the plist path for load SpriteFrames.<br>
         * -- since v1.1<br>
         * -- js NA
         * @method addAnimationsWithDictionary
         * @param {any} _dictionary
         * @param {string} _plist
         */
        public addAnimationsWithDictionary (
            _dictionary : any, 
            _plist : string 
        ) : void;

        /**
         *  Deletes a Animation from the cache.<br>
         * -- param name The name of animation.
         * @method removeAnimation
         * @param {string} _name
         */
        public removeAnimation (
            _name : string 
        ) : void;

        /**
         *  Adds an animation from a plist file.<br>
         * -- Make sure that the frames were previously loaded in the SpriteFrameCache.<br>
         * -- since v1.1<br>
         * -- js addAnimations<br>
         * -- lua addAnimations<br>
         * -- param plist An animation from a plist file.
         * @method addAnimationsWithFile
         * @param {string} _plist
         */
        public addAnimations (
            _plist : string 
        ) : void;

        /**
         *  Purges the cache. It releases all the Animation objects and the shared instance.<br>
         * -- js NA
         * @method destroyInstance
         */
        public static destroyInstance (
        ) : void;

        /**
         *  Returns the shared instance of the Animation cache <br>
         * -- js NA
         * @method getInstance
         * @return {cc.AnimationCache}
         */
        public static getInstance (
        ) : cc.AnimationCache;

        /**
         * js ctor
         * @method AnimationCache
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class SpriteBatchNode
     * @native
     */
    export class SpriteBatchNode 
        extends cc.Node
    {
        public static readonly DEFAULT_CAPACITY: number;
        /**
         *  Returns the TextureAtlas object. 
         * return The TextureAtlas object.
         * @method getTextureAtlas
         * @return {cc.TextureAtlas}
         */
        public textureAtlas : cc.TextureAtlas;
        /* Can't find native property getDescendants implementation of cc::SpriteBatchNode. using any type */
        public readonly descendants : any;
        /**
         * 
         * @method getTexture
         * @return {cc.Texture2D}
         */
        public texture : cc.Texture2D;


        /**
         *  Append the child. <br>
         * -- param sprite A Sprite.
         * @method appendChild
         * @param {cc.Sprite} _sprite
         */
        public appendChild (
            _sprite : cc.Sprite 
        ) : void;

        /**
         * 
         * @method reorderBatch
         * @param {boolean} _reorder
         */
        public reorderBatch (
            _reorder : boolean 
        ) : void;

        /**
         * 
         * @method getTexture
         * @return {cc.Texture2D}
         */
        public getTexture (
        ) : cc.Texture2D;

        /**
         * 
         * @method setTexture
         * @param {cc.Texture2D} _texture
         */
        public setTexture (
            _texture : cc.Texture2D 
        ) : void;

        /**
         *  Removes a child given a certain index. It will also cleanup the running actions depending on the cleanup parameter.<br>
         * -- param index A certain index.<br>
         * -- param doCleanup Whether or not to cleanup the running actions.<br>
         * -- warning Removing a child from a SpriteBatchNode is very slow.
         * @method removeChildAtIndex
         * @param {number} _index
         * @param {boolean} _doCleanup
         */
        public removeChildAtIndex (
            _index : number, 
            _doCleanup : boolean 
        ) : void;

        /**
         *  Remove a sprite from Atlas. <br>
         * -- param sprite A Sprite.
         * @method removeSpriteFromAtlas
         * @param {cc.Sprite} _sprite
         */
        public removeSpriteFromAtlas (
            _sprite : cc.Sprite 
        ) : void;

        /**
         * 
         * @method addSpriteWithoutQuad
         * @param {cc.Sprite} _child
         * @param {number} _z
         * @param {number} _aTag
         * @return {cc.SpriteBatchNode}
         */
        public addSpriteWithoutQuad (
            _child : cc.Sprite, 
            _z : number, 
            _aTag : number 
        ) : cc.SpriteBatchNode;

        /**
         *  Get the nearest index from the sprite in z.<br>
         * -- param sprite The parent sprite.<br>
         * -- param z Z order for drawing priority.<br>
         * -- return Index.
         * @method atlasIndexForChild
         * @param {cc.Sprite} _sprite
         * @param {number} _z
         * @return {number}
         */
        public atlasIndexForChild (
            _sprite : cc.Sprite, 
            _z : number 
        ) : number;

        /**
         *  Increase the Atlas Capacity. 
         * @method increaseAtlasCapacity
         */
        public increaseAtlasCapacity (
        ) : void;

        /**
         *  Get the Min image block index,in all child. <br>
         * -- param sprite The parent sprite.<br>
         * -- return Index.
         * @method lowestAtlasIndexInChild
         * @param {cc.Sprite} _sprite
         * @return {number}
         */
        public lowestAtlasIndexInChild (
            _sprite : cc.Sprite 
        ) : number;

        /**
         * lua NA
         * @method getBlendFunc
         * @return {cc.BlendFunc}
         */
        public getBlendFunc (
        ) : cc.BlendFunc;

        /**
         *  initializes a SpriteBatchNode with a texture2d and capacity of children.<br>
         * -- The capacity will be increased in 33% in runtime if it runs out of space.
         * @method initWithTexture
         * @param {cc.Texture2D} _tex
         * @param {number} _capacity
         * @return {boolean}
         */
        public initWithTexture (
            _tex : cc.Texture2D, 
            _capacity : number 
        ) : boolean;

        /**
         *  Sets the TextureAtlas object. <br>
         * -- param textureAtlas The TextureAtlas object.
         * @method setTextureAtlas
         * @param {cc.TextureAtlas} _textureAtlas
         */
        public setTextureAtlas (
            _textureAtlas : cc.TextureAtlas 
        ) : void;

        /**
         *  reserves capacity for the batch node.<br>
         * -- If the current capacity is bigger, nothing happens.<br>
         * -- otherwise, a new capacity is allocated 
         * @method reserveCapacity
         * @param {number} _newCapacity
         */
        public reserveCapacity (
            _newCapacity : number 
        ) : void;

        /**
         *  Inserts a quad at a certain index into the texture atlas. The Sprite won't be added into the children array.<br>
         * -- This method should be called only when you are dealing with very big AtlasSprite and when most of the Sprite won't be updated.<br>
         * -- For example: a tile map (TMXMap) or a label with lots of characters (LabelBMFont).
         * @method insertQuadFromSprite
         * @param {cc.Sprite} _sprite
         * @param {number} _index
         */
        public insertQuadFromSprite (
            _sprite : cc.Sprite, 
            _index : number 
        ) : void;

        /**
         *  initializes a SpriteBatchNode with a file image (.png, .jpeg, .pvr, etc) and a capacity of children.<br>
         * -- The capacity will be increased in 33% in runtime if it runs out of space.<br>
         * -- The file will be loaded using the TextureMgr.<br>
         * -- js init<br>
         * -- lua init
         * @method initWithFile
         * @param {string} _fileImage
         * @param {number} _capacity
         * @return {boolean}
         */
        public initWithFile (
            _fileImage : string, 
            _capacity : number 
        ) : boolean;

        /**
         * code<br>
         * -- When this function bound into js or lua,the parameter will be changed.<br>
         * -- In js: var setBlendFunc(var src, var dst).<br>
         * -- endcode<br>
         * -- lua NA 
         * @method setBlendFunc
         * @param {cc.BlendFunc} _blendFunc
         */
        public setBlendFunc (
            _blendFunc : cc.BlendFunc 
        ) : void;

        /**
         *  Rebuild index with a sprite all child. <br>
         * -- param parent The parent sprite.<br>
         * -- param index The child index.<br>
         * -- return Index.
         * @method rebuildIndexInOrder
         * @param {cc.Sprite} _parent
         * @param {number} _index
         * @return {number}
         */
        public rebuildIndexInOrder (
            _parent : cc.Sprite, 
            _index : number 
        ) : number;

        /**
         *  Returns the TextureAtlas object. <br>
         * -- return The TextureAtlas object.
         * @method getTextureAtlas
         * @return {cc.TextureAtlas}
         */
        public getTextureAtlas (
        ) : cc.TextureAtlas;

        /**
         *  Get the Max image block index,in all child.<br>
         * -- param sprite The parent sprite.<br>
         * -- return Index.
         * @method highestAtlasIndexInChild
         * @param {cc.Sprite} _sprite
         * @return {number}
         */
        public highestAtlasIndexInChild (
            _sprite : cc.Sprite 
        ) : number;

        /**
         *  Creates a SpriteBatchNode with a file image (.png, .jpeg, .pvr, etc) and capacity of children.<br>
         * -- The capacity will be increased in 33% in runtime if it runs out of space.<br>
         * -- The file will be loaded using the TextureMgr.<br>
         * -- param fileImage A file image (.png, .jpeg, .pvr, etc).<br>
         * -- param capacity The capacity of children.<br>
         * -- return Return an autorelease object.
         * @method create
         * @param {string} _fileImage
         * @param {number} _capacity
         * @return {cc.SpriteBatchNode}
         */
        public static create (
            _fileImage : string, 
            _capacity : number 
        ) : cc.SpriteBatchNode;

        /**
         *  Creates a SpriteBatchNode with a texture2d and capacity of children.<br>
         * -- The capacity will be increased in 33% in runtime if it runs out of space.<br>
         * -- param tex A texture2d.<br>
         * -- param capacity The capacity of children.<br>
         * -- return Return an autorelease object.
         * @method createWithTexture
         * @param {cc.Texture2D} _tex
         * @param {number} _capacity
         * @return {cc.SpriteBatchNode}
         */
        public static createWithTexture (
            _tex : cc.Texture2D, 
            _capacity : number 
        ) : cc.SpriteBatchNode;

        /**
         * js ctor
         * @method SpriteBatchNode
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class SpriteFrameCache
     * @native
     */
    export abstract class SpriteFrameCache 
    {

        /**
         * 
         * @method reloadTexture
         * @param {string} _plist
         * @return {boolean}
         */
        public reloadTexture (
            _plist : string 
        ) : boolean;

        /**
         *  Adds multiple Sprite Frames from a plist file content. The texture will be associated with the created sprite frames. <br>
         * -- js NA<br>
         * -- lua addSpriteFrames<br>
         * -- param plist_content Plist file content string.<br>
         * -- param texture Texture pointer.
         * @method addSpriteFramesWithFileContent
         * @param {string} _plist_content
         * @param {cc.Texture2D} _texture
         */
        public addSpriteFramesWithFileContent (
            _plist_content : string, 
            _texture : cc.Texture2D 
        ) : void;

        /**
         *  Adds an sprite frame with a given name.<br>
         * -- If the name already exists, then the contents of the old name will be replaced with the new one.<br>
         * -- param frame A certain sprite frame.<br>
         * -- param frameName The name of the sprite frame.
         * @method addSpriteFrame
         * @param {cc.SpriteFrame} _frame
         * @param {string} _frameName
         */
        public addSpriteFrame (
            _frame : cc.SpriteFrame, 
            _frameName : string 
        ) : void;

        /**
         *  Adds multiple Sprite Frames from a plist file. The texture will be associated with the created sprite frames.<br>
         * -- since v0.99.5<br>
         * -- js addSpriteFrames<br>
         * -- lua addSpriteFrames<br>
         * -- param plist Plist file name.<br>
         * -- param textureFileName Texture file name.
         * @method addSpriteFramesWithFile
         * @param {(string)} _plist
         * @param {(string) | (cc.Texture2D)} _textureFileName | texture?
         * @return {void | string[]}
         */
        public addSpriteFrames (
            _plist : (string), 
            _textureFileName_texture? : (string) | (cc.Texture2D)
        ) : void | string[];

        /**
         *  Returns an Sprite Frame that was previously added.<br>
         * -- If the name is not found it will return nil.<br>
         * -- You should retain the returned copy if you are going to use it.<br>
         * -- js getSpriteFrame<br>
         * -- lua getSpriteFrame<br>
         * -- param name A certain sprite frame name.<br>
         * -- return The sprite frame.
         * @method getSpriteFrameByName
         * @param {string} _name
         * @return {cc.SpriteFrame}
         */
        public getSpriteFrame (
            _name : string 
        ) : cc.SpriteFrame;

        /**
         * 
         * @method getSpriteFramesByTexture
         * @param {cc.Texture2D} _texture
         * @return {string[]}
         */
        public getSpriteFramesByTexture (
            _texture : cc.Texture2D 
        ) : string[];

        /**
         *  Removes multiple Sprite Frames from a plist file.<br>
         * -- Sprite Frames stored in this file will be removed.<br>
         * -- It is convenient to call this method when a specific texture needs to be removed.<br>
         * -- since v0.99.5<br>
         * -- param plist The name of the plist that needs to removed.
         * @method removeSpriteFramesFromFile
         * @param {string} _plist
         */
        public removeSpriteFramesFromFile (
            _plist : string 
        ) : void;

        /**
         *  Initialize method.<br>
         * -- return if success return true.
         * @method init
         * @return {boolean}
         */
        public init (
        ) : boolean;

        /**
         *  Purges the dictionary of loaded sprite frames.<br>
         * -- Call this method if you receive the "Memory Warning".<br>
         * -- In the short term: it will free some resources preventing your app from being killed.<br>
         * -- In the medium term: it will allocate more resources.<br>
         * -- In the long term: it will be the same.
         * @method removeSpriteFrames
         */
        public removeSpriteFrames (
        ) : void;

        /**
         *  Removes unused sprite frames.<br>
         * -- Sprite Frames that have a retain count of 1 will be deleted.<br>
         * -- It is convenient to call this method after when starting a new Scene.<br>
         * -- js NA
         * @method removeUnusedSpriteFrames
         */
        public removeUnusedSpriteFrames (
        ) : void;

        /**
         *  Removes multiple Sprite Frames from a plist file content.<br>
         * -- Sprite Frames stored in this file will be removed.<br>
         * -- It is convenient to call this method when a specific texture needs to be removed.<br>
         * -- param plist_content The string of the plist content that needs to removed.<br>
         * -- js NA
         * @method removeSpriteFramesFromFileContent
         * @param {string} _plist_content
         */
        public removeSpriteFramesFromFileContent (
            _plist_content : string 
        ) : void;

        /**
         *  Deletes an sprite frame from the sprite frame cache. <br>
         * -- param name The name of the sprite frame that needs to removed.
         * @method removeSpriteFrameByName
         * @param {string} _name
         */
        public removeSpriteFrameByName (
            _name : string 
        ) : void;

        /**
         *  Check if multiple Sprite Frames from a plist file have been loaded.<br>
         * -- js NA<br>
         * -- lua NA<br>
         * -- param plist Plist file name.<br>
         * -- return True if the file is loaded.
         * @method isSpriteFramesWithFileLoaded
         * @param {string} _plist
         * @return {boolean}
         */
        public isSpriteFramesWithFileLoaded (
            _plist : string 
        ) : boolean;

        /**
         *  Removes all Sprite Frames associated with the specified textures.<br>
         * -- It is convenient to call this method when a specific texture needs to be removed.<br>
         * -- since v0.995.<br>
         * -- param texture The texture that needs to removed.
         * @method removeSpriteFramesFromTexture
         * @param {cc.Texture2D} _texture
         */
        public removeSpriteFramesFromTexture (
            _texture : cc.Texture2D 
        ) : void;

        /**
         *  Destroys the cache. It releases all the Sprite Frames and the retained instance.<br>
         * -- js NA
         * @method destroyInstance
         */
        public static destroyInstance (
        ) : void;

        /**
         *  Returns the shared instance of the Sprite Frame cache.<br>
         * -- return The instance of the Sprite Frame Cache.<br>
         * -- js NA
         * @method getInstance
         * @return {cc.SpriteFrameCache}
         */
        public static getInstance (
        ) : cc.SpriteFrameCache;

    }
    /**
     * @class TextFieldTTF
     * @native
     */
    export class TextFieldTTF 
        extends cc.Label
    {
        /* Can't find native property getString implementation of cc::TextFieldTTF. using any type */
        public string : any;


        /**
         * Query the currently inputed character count.<br>
         * -- return The total input character count.
         * @method getCharCount
         * @return {number}
         */
        public getCharCount (
        ) : number;

        /**
         * Set char showing cursor.<br>
         * -- js NA
         * @method setCursorChar
         * @param {number} _cursor
         */
        public setCursorChar (
            _cursor : number 
        ) : void;

        /**
         * Set enable secure text entry representation.<br>
         * -- If you want to display password in TextField, this option is very helpful.<br>
         * -- param value Whether or not to display text with secure text entry.<br>
         * -- js NA
         * @method setSecureTextEntry
         * @param {boolean} _value
         */
        public setSecureTextEntry (
            _value : boolean 
        ) : void;

        /**
         * Set enable cursor use.<br>
         * -- js NA
         * @method setCursorEnabled
         * @param {boolean} _enabled
         */
        public setCursorEnabled (
            _enabled : boolean 
        ) : void;

        /**
         * Query the color of place holder.<br>
         * -- return The place holder color.
         * @method getColorSpaceHolder
         * @return {ctype.value_type<cc.Color>}
         */
        public getColorSpaceHolder (
        ) : ctype.value_type<cc.Color>;

        /**
         *  Initializes the TextFieldTTF with a font name and font size. 
         * @method initWithPlaceHolder
         * @param {(string)} _placeholder
         * @param {(string) | (ctype.value_type<cc.Size>)} _fontName | dimensions
         * @param {(number) | (cc.TextHAlignment)} _fontSize | alignment
         * @param {(string)} _fontName?
         * @param {(number)} _fontSize?
         * @return {boolean}
         */
        public initWithPlaceHolder (
            _placeholder : (string), 
            _fontName_dimensions : (string) | (ctype.value_type<cc.Size>), 
            _fontSize_alignment : (number), 
            _fontName? : (string), 
            _fontSize? : (number)
        ) : boolean;

        /**
         * Append to input text of TextField.<br>
         * -- param text The append text of TextField.
         * @method appendString
         * @param {string} _text
         */
        public appendString (
            _text : string 
        ) : void;

        /**
         * 
         * @method getPasswordTextStyle
         * @return {string}
         */
        public getPasswordTextStyle (
        ) : string;

        /**
         * 
         * @method setPasswordTextStyle
         * @param {string} _text
         */
        public setPasswordTextStyle (
            _text : string 
        ) : void;

        /**
         * Change the placeholder color.<br>
         * -- param color The placeholder color in Color4B.
         * @method setColorSpaceHolder
         * @param {(ctype.value_type<cc.Color>)} _color
         */
        public setColorSpaceHolder (
            _color : (ctype.value_type<cc.Color>)
        ) : void;

        /**
         * brief    End text input and close keyboard.
         * @method detachWithIME
         * @return {boolean}
         */
        public detachWithIME (
        ) : boolean;

        /**
         * Change placeholder text.<br>
         * -- place holder text displayed when there is no text in the text field.<br>
         * -- param text  The placeholder string.
         * @method setPlaceHolder
         * @param {string} _text
         */
        public setPlaceHolder (
            _text : string 
        ) : void;

        /**
         * Set cursor position to hit letter, if enabled<br>
         * -- js NA
         * @method setCursorFromPoint
         * @param {ctype.value_type<cc.Point>} _point
         * @param {cc.Camera} _camera
         */
        public setCursorFromPoint (
            _point : ctype.value_type<cc.Point>, 
            _camera : cc.Camera 
        ) : void;

        /**
         * Query whether the currently display mode is secure text entry or not.<br>
         * -- return Whether current text is displayed as secure text entry.<br>
         * -- js NA
         * @method isSecureTextEntry
         * @return {boolean}
         */
        public isSecureTextEntry (
        ) : boolean;

        /**
         * Query the placeholder string.<br>
         * -- return The placeholder string.
         * @method getPlaceHolder
         * @return {string}
         */
        public getPlaceHolder (
        ) : string;

        /**
         * Set cursor position, if enabled<br>
         * -- js NA
         * @method setCursorPosition
         * @param {number} _cursorPosition
         */
        public setCursorPosition (
            _cursorPosition : number 
        ) : void;

        /**
         * brief    Open keyboard and receive input text.
         * @method attachWithIME
         * @return {boolean}
         */
        public attachWithIME (
        ) : boolean;

        /**
         *  Creates a TextFieldTTF from a fontname and font size.<br>
         * -- js NA
         * @method textFieldWithPlaceHolder
         * @param {(string)} _placeholder
         * @param {(string) | (ctype.value_type<cc.Size>)} _fontName | dimensions
         * @param {(number) | (cc.TextHAlignment)} _fontSize | alignment
         * @param {(string)} _fontName?
         * @param {(number)} _fontSize?
         * @return {cc.TextFieldTTF}
         */
        public static create (
            _placeholder : (string), 
            _fontName_dimensions : (string) | (ctype.value_type<cc.Size>), 
            _fontSize_alignment : (number), 
            _fontName? : (string), 
            _fontSize? : (number)
        ) : cc.TextFieldTTF;

        /**
         * Default constructor.<br>
         * -- js ctor
         * @method TextFieldTTF
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class ParallaxNode
     * @native
     */
    export class ParallaxNode 
        extends cc.Node
    {

        /**
         * 
         * @method getParallaxArray
         * @return {cc._ccArray}
         */
        public getParallaxArray (
        ) : cc._ccArray;

        /**
         *  Adds a child to the container with a local z-order, parallax ratio and position offset.<br>
         * -- param child A child node.<br>
         * -- param z Z order for drawing priority.<br>
         * -- param parallaxRatio A given parallax ratio.<br>
         * -- param positionOffset A given position offset.
         * @method addChild
         * @param {cc.Node} _child
         * @param {number} _z
         * @param {ctype.value_type<cc.Point>} _parallaxRatio
         * @param {ctype.value_type<cc.Point>} _positionOffset
         */
        public addChild (
            _child : cc.Node, 
            _z : number, 
            _parallaxRatio : ctype.value_type<cc.Point>, 
            _positionOffset : ctype.value_type<cc.Point> 
        ) : void;

        /**
         * 
         * @method removeAllChildrenWithCleanup
         * @param {boolean} _cleanup
         */
        public removeAllChildrenWithCleanup (
            _cleanup : boolean 
        ) : void;

        /**
         *  Sets an array of layers for the Parallax node.<br>
         * -- param parallaxArray An array of layers for the Parallax node.
         * @method setParallaxArray
         * @param {cc._ccArray} _parallaxArray
         */
        public setParallaxArray (
            _parallaxArray : cc._ccArray 
        ) : void;

        /**
         *  Create a Parallax node. <br>
         * -- return An autoreleased ParallaxNode object.
         * @method create
         * @return {cc.ParallaxNode}
         */
        public static create (
        ) : cc.ParallaxNode;

        /**
         *  Adds a child to the container with a z-order, a parallax ratio and a position offset<br>
         * -- It returns self, so you can chain several addChilds.<br>
         * -- since v0.8<br>
         * -- js ctor
         * @method ParallaxNode
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class TMXObjectGroup
     * @native
     */
    export class TMXObjectGroup 
    {

        /**
         *  Sets the offset position of child objects. <br>
         * -- param offset The offset position of child objects.
         * @method setPositionOffset
         * @param {ctype.value_type<cc.Point>} _offset
         */
        public setPositionOffset (
            _offset : ctype.value_type<cc.Point> 
        ) : void;

        /**
         *  Return the value for the specific property name. <br>
         * -- param propertyName The specific property name.<br>
         * -- return Return the value for the specific property name.<br>
         * -- js NA
         * @method getProperty
         * @param {string} _propertyName
         * @return {cc.Value}
         */
        public getProperty (
            _propertyName : string 
        ) : cc.Value;

        /**
         *  Gets the offset position of child objects. <br>
         * -- return The offset position of child objects.
         * @method getPositionOffset
         * @return {ctype.value_type<cc.Point>}
         */
        public getPositionOffset (
        ) : ctype.value_type<cc.Point>;

        /**
         *  Return the dictionary for the specific object name.<br>
         * -- It will return the 1st object found on the array for the given name.<br>
         * -- return Return the dictionary for the specific object name.
         * @method getObject
         * @param {string} _objectName
         * @return {any}
         */
        public getObject (
            _objectName : string 
        ) : any;

        /**
         * 
         * @method getObjects
         * @return {cc.Value[]}
         */
        public getObjects (
        ) : cc.Value[];

        /**
         *  Set the group name. <br>
         * -- param groupName A string,it is used to set the group name.
         * @method setGroupName
         * @param {string} _groupName
         */
        public setGroupName (
            _groupName : string 
        ) : void;

        /**
         * 
         * @method getProperties
         * @return {any}
         */
        public getProperties (
        ) : any;

        /**
         *  Get the group name. <br>
         * -- return The group name.
         * @method getGroupName
         * @return {string}
         */
        public getGroupName (
        ) : string;

        /**
         *  Sets the list of properties.<br>
         * -- param properties The list of properties.
         * @method setProperties
         * @param {any} _properties
         */
        public setProperties (
            _properties : any 
        ) : void;

        /**
         *  Sets the array of the objects.<br>
         * -- param objects The array of the objects.
         * @method setObjects
         * @param {cc.Value[]} _objects
         */
        public setObjects (
            _objects : cc.Value[] 
        ) : void;

        /**
         * js ctor
         * @method TMXObjectGroup
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class TMXLayerInfo
     * @native
     */
    export class TMXLayerInfo 
    {

        /**
         * 
         * @method setProperties
         * @param {any} _properties
         */
        public setProperties (
            _properties : any 
        ) : void;

        /**
         * 
         * @method getProperties
         * @return {any}
         */
        public getProperties (
        ) : any;

        /**
         * js ctor
         * @method TMXLayerInfo
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class TMXTilesetInfo
     * @native
     */
    export class TMXTilesetInfo 
    {

        /**
         * 
         * @method getRectForGID
         * @param {number} _gid
         * @return {ctype.value_type<cc.Rect>}
         */
        public getRectForGID (
            _gid : number 
        ) : ctype.value_type<cc.Rect>;

        /**
         * js ctor
         * @method TMXTilesetInfo
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class TMXMapInfo
     * @native
     */
    export class TMXMapInfo 
    {

        /**
         * 
         * @method setObjectGroups
         * @param {cc.TMXObjectGroup[]} _groups
         */
        public setObjectGroups (
            _groups : cc.TMXObjectGroup[] 
        ) : void;

        /**
         * 
         * @method setTileSize
         * @param {ctype.value_type<cc.Size>} _tileSize
         */
        public setTileSize (
            _tileSize : ctype.value_type<cc.Size> 
        ) : void;

        /**
         *  initializes a TMX format with a  tmx file 
         * @method initWithTMXFile
         * @param {string} _tmxFile
         * @return {boolean}
         */
        public initWithTMXFile (
            _tmxFile : string 
        ) : boolean;

        /**
         * / map orientation
         * @method getOrientation
         * @return {number}
         */
        public getOrientation (
        ) : number;

        /**
         * / is storing characters?
         * @method isStoringCharacters
         * @return {boolean}
         */
        public isStoringCharacters (
        ) : boolean;

        /**
         * 
         * @method setLayers
         * @param {cc.TMXLayerInfo[]} _layers
         */
        public setLayers (
            _layers : cc.TMXLayerInfo[] 
        ) : void;

        /**
         *  initializes parsing of an XML file, either a tmx (Map) file or tsx (Tileset) file 
         * @method parseXMLFile
         * @param {string} _xmlFilename
         * @return {boolean}
         */
        public parseXMLFile (
            _xmlFilename : string 
        ) : boolean;

        /**
         * / parent element
         * @method getParentElement
         * @return {number}
         */
        public getParentElement (
        ) : number;

        /**
         * 
         * @method setTMXFileName
         * @param {string} _fileName
         */
        public setTMXFileName (
            _fileName : string 
        ) : void;

        /**
         * 
         * @method parseXMLString
         * @param {string} _xmlString
         * @return {boolean}
         */
        public parseXMLString (
            _xmlString : string 
        ) : boolean;

        /**
         * 
         * @method getLayers
         * @return {cc.TMXLayerInfo[]}
         */
        public getLayers (
        ) : cc.TMXLayerInfo[];

        /**
         * 
         * @method getTilesets
         * @return {cc.TMXTilesetInfo[]}
         */
        public getTilesets (
        ) : cc.TMXTilesetInfo[];

        /**
         * / parent GID
         * @method getParentGID
         * @return {number}
         */
        public getParentGID (
        ) : number;

        /**
         * 
         * @method setParentElement
         * @param {number} _element
         */
        public setParentElement (
            _element : number 
        ) : void;

        /**
         *  initializes a TMX format with an XML string and a TMX resource path 
         * @method initWithXML
         * @param {string} _tmxString
         * @param {string} _resourcePath
         * @return {boolean}
         */
        public initWithXML (
            _tmxString : string, 
            _resourcePath : string 
        ) : boolean;

        /**
         * 
         * @method setParentGID
         * @param {number} _gid
         */
        public setParentGID (
            _gid : number 
        ) : void;

        /**
         * / layer attribs
         * @method getLayerAttribs
         * @return {number}
         */
        public getLayerAttribs (
        ) : number;

        /**
         * / tiles width & height
         * @method getTileSize
         * @return {ctype.value_type<cc.Size>}
         */
        public getTileSize (
        ) : ctype.value_type<cc.Size>;

        /**
         * 
         * @method getTileProperties
         * @return {any}
         */
        public getTileProperties (
        ) : any;

        /**
         * 
         * @method getExternalTilesetFileName
         * @return {string}
         */
        public getExternalTilesetFileName (
        ) : string;

        /**
         * 
         * @method getObjectGroups
         * @return {cc.TMXObjectGroup[]}
         */
        public getObjectGroups (
        ) : cc.TMXObjectGroup[];

        /**
         * 
         * @method getTMXFileName
         * @return {string}
         */
        public getTMXFileName (
        ) : string;

        /**
         * 
         * @method setCurrentString
         * @param {string} _currentString
         */
        public setCurrentString (
            _currentString : string 
        ) : void;

        /**
         * 
         * @method setProperties
         * @param {any} _properties
         */
        public setProperties (
            _properties : any 
        ) : void;

        /**
         * 
         * @method setOrientation
         * @param {number} _orientation
         */
        public setOrientation (
            _orientation : number 
        ) : void;

        /**
         * 
         * @method setTileProperties
         * @param {any} _tileProperties
         */
        public setTileProperties (
            _tileProperties : any 
        ) : void;

        /**
         * 
         * @method setMapSize
         * @param {ctype.value_type<cc.Size>} _mapSize
         */
        public setMapSize (
            _mapSize : ctype.value_type<cc.Size> 
        ) : void;

        /**
         * 
         * @method setStoringCharacters
         * @param {boolean} _storingCharacters
         */
        public setStoringCharacters (
            _storingCharacters : boolean 
        ) : void;

        /**
         * / map width & height
         * @method getMapSize
         * @return {ctype.value_type<cc.Size>}
         */
        public getMapSize (
        ) : ctype.value_type<cc.Size>;

        /**
         * 
         * @method setTilesets
         * @param {cc.TMXTilesetInfo[]} _tilesets
         */
        public setTilesets (
            _tilesets : cc.TMXTilesetInfo[] 
        ) : void;

        /**
         * 
         * @method getProperties
         * @return {any}
         */
        public getProperties (
        ) : any;

        /**
         * 
         * @method getCurrentString
         * @return {string}
         */
        public getCurrentString (
        ) : string;

        /**
         * 
         * @method setLayerAttribs
         * @param {number} _layerAttribs
         */
        public setLayerAttribs (
            _layerAttribs : number 
        ) : void;

        /**
         *  creates a TMX Format with a tmx file 
         * @method create
         * @param {string} _tmxFile
         * @return {cc.TMXMapInfo}
         */
        public static create (
            _tmxFile : string 
        ) : cc.TMXMapInfo;

        /**
         *  creates a TMX Format with an XML string and a TMX resource path 
         * @method createWithXML
         * @param {string} _tmxString
         * @param {string} _resourcePath
         * @return {cc.TMXMapInfo}
         */
        public static createWithXML (
            _tmxString : string, 
            _resourcePath : string 
        ) : cc.TMXMapInfo;

        /**
         * js ctor
         * @method TMXMapInfo
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class TMXLayer
     * @native
     */
    export class TMXLayer 
        extends cc.SpriteBatchNode
    {
        /* Can't find native property getTiles implementation of cc::TMXLayer. using any type */
        public tiles : any;
        /* Can't find native property getTileset implementation of cc::TMXLayer. using any type */
        public tileset : any;
        /**
         *  Layer orientation, which is the same as the map orientation.
         * return Layer orientation, which is the same as the map orientation.
         * @method getLayerOrientation
         * @return {number}
         */
        public layerOrientation : number;
        /**
         *  Properties from the layer. They can be added using Tiled.
         * return Properties from the layer. They can be added using Tiled.
         * @method getProperties
         * @return {any}
         */
        public properties : any;
        /**
         *  Get the layer name. 
         * return The layer name.
         * @method getLayerName
         * @return {string}
         */
        public layerName : string;
        /* Can't find native property _getLayerWidth implementation of cc::TMXLayer. using any type */
        public layerWidth : any;
        /* Can't find native property _getLayerHeight implementation of cc::TMXLayer. using any type */
        public layerHeight : any;
        /* Can't find native property _getTileWidth implementation of cc::TMXLayer. using any type */
        public tileWidth : any;
        /* Can't find native property _getTileHeight implementation of cc::TMXLayer. using any type */
        public tileHeight : any;
        /* Can't find native property getTexture implementation of cc::TMXLayer. using any type */
        public texture : any;


        /**
         *  Returns the tile gid at a given tile coordinate. It also returns the tile flags.<br>
         * -- This method requires the tile map has not been previously released (eg. don't call [layer releaseMap]).<br>
         * -- param tileCoordinate The tile coordinate.<br>
         * -- param flags Tile flags.<br>
         * -- return Returns the tile gid at a given tile coordinate. It also returns the tile flags.
         * @method getTileGIDAt
         * @param {ctype.value_type<cc.Point>} _tileCoordinate
         * @param {cc.TMXTileFlags_} _flags
         * @return {number}
         */
        public getTileGIDAt (
            _tileCoordinate : ctype.value_type<cc.Point>, 
            _flags : cc.TMXTileFlags_ 
        ) : number;

        /**
         *  Returns the position in points of a given tile coordinate.<br>
         * -- param tileCoordinate The tile coordinate.<br>
         * -- return The position in points of a given tile coordinate.
         * @method getPositionAt
         * @param {ctype.value_type<cc.Point>} _tileCoordinate
         * @return {ctype.value_type<cc.Point>}
         */
        public getPositionAt (
            _tileCoordinate : ctype.value_type<cc.Point> 
        ) : ctype.value_type<cc.Point>;

        /**
         *  Set layer orientation, which is the same as the map orientation.<br>
         * -- param orientation Layer orientation,which is the same as the map orientation.
         * @method setLayerOrientation
         * @param {number} _orientation
         */
        public setLayerOrientation (
            _orientation : number 
        ) : void;

        /**
         *  Dealloc the map that contains the tile position from memory.<br>
         * -- Unless you want to know at runtime the tiles positions, you can safely call this method.<br>
         * -- If you are going to call layer->tileGIDAt() then, don't release the map.
         * @method releaseMap
         */
        public releaseMap (
        ) : void;

        /**
         *  Set a pointer to the map of tiles.<br>
         * -- param tiles A pointer to the map of tiles.
         * @method setTiles
         * @param {number} _tiles
         */
        public setTiles (
            _tiles : number 
        ) : void;

        /**
         *  Size of the layer in tiles.<br>
         * -- return Size of the layer in tiles.
         * @method getLayerSize
         * @return {ctype.value_type<cc.Size>}
         */
        public getLayerSize (
        ) : ctype.value_type<cc.Size>;

        /**
         *  Set the size of the map's tile.<br>
         * -- param size The size of the map's tile.
         * @method setMapTileSize
         * @param {ctype.value_type<cc.Size>} _size
         */
        public setMapTileSize (
            _size : ctype.value_type<cc.Size> 
        ) : void;

        /**
         *  Layer orientation, which is the same as the map orientation.<br>
         * -- return Layer orientation, which is the same as the map orientation.
         * @method getLayerOrientation
         * @return {number}
         */
        public getLayerOrientation (
        ) : number;

        /**
         *  Set an Properties from to layer.<br>
         * -- param properties It is used to set the layer Properties.
         * @method setProperties
         * @param {any} _properties
         */
        public setProperties (
            _properties : any 
        ) : void;

        /**
         *  Set the layer name.<br>
         * -- param layerName The layer name.
         * @method setLayerName
         * @param {string} _layerName
         */
        public setLayerName (
            _layerName : string 
        ) : void;

        /**
         *  Removes a tile at given tile coordinate. <br>
         * -- param tileCoordinate The tile coordinate.
         * @method removeTileAt
         * @param {ctype.value_type<cc.Point>} _tileCoordinate
         */
        public removeTileAt (
            _tileCoordinate : ctype.value_type<cc.Point> 
        ) : void;

        /**
         *  Initializes a TMXLayer with a tileset info, a layer info and a map info.<br>
         * -- param tilesetInfo An tileset info.<br>
         * -- param layerInfo A layer info.<br>
         * -- param mapInfo A map info.<br>
         * -- return If initializes successfully, it will return true.
         * @method initWithTilesetInfo
         * @param {cc.TMXTilesetInfo} _tilesetInfo
         * @param {cc.TMXLayerInfo} _layerInfo
         * @param {cc.TMXMapInfo} _mapInfo
         * @return {boolean}
         */
        public initWithTilesetInfo (
            _tilesetInfo : cc.TMXTilesetInfo, 
            _layerInfo : cc.TMXLayerInfo, 
            _mapInfo : cc.TMXMapInfo 
        ) : boolean;

        /**
         *  Creates the tiles. 
         * @method setupTiles
         */
        public setupTiles (
        ) : void;

        /**
         *  Sets the tile gid (gid = tile global id) at a given tile coordinate.<br>
         * -- The Tile GID can be obtained by using the method "tileGIDAt" or by using the TMX editor -> Tileset Mgr +1.<br>
         * -- If a tile is already placed at that position, then it will be removed.<br>
         * -- Use withFlags if the tile flags need to be changed as well.<br>
         * -- param gid The tile gid.<br>
         * -- param tileCoordinate The tile coordinate.<br>
         * -- param flags The tile flags.
         * @method setTileGID
         * @param {(number)} _gid
         * @param {(ctype.value_type<cc.Point>)} _tileCoordinate
         * @param {(cc.TMXTileFlags_)} _flags?
         */
        public setTileGID (
            _gid : (number), 
            _tileCoordinate : (ctype.value_type<cc.Point>), 
            _flags? : (number)
        ) : void;

        /**
         *  Size of the map's tile (could be different from the tile's size).<br>
         * -- return The size of the map's tile.
         * @method getMapTileSize
         * @return {ctype.value_type<cc.Size>}
         */
        public getMapTileSize (
        ) : ctype.value_type<cc.Size>;

        /**
         *  Return the value for the specific property name.<br>
         * -- param propertyName The specific property name.<br>
         * -- return Return the value for the specific property name.
         * @method getProperty
         * @param {string} _propertyName
         * @return {cc.Value}
         */
        public getProperty (
            _propertyName : string 
        ) : cc.Value;

        /**
         *  Set size of the layer in tiles.<br>
         * -- param size Size of the layer in tiles.
         * @method setLayerSize
         * @param {ctype.value_type<cc.Size>} _size
         */
        public setLayerSize (
            _size : ctype.value_type<cc.Size> 
        ) : void;

        /**
         *  Get the layer name. <br>
         * -- return The layer name.
         * @method getLayerName
         * @return {string}
         */
        public getLayerName (
        ) : string;

        /**
         *  Set tileset information for the layer.<br>
         * -- param info The tileset information for the layer.<br>
         * -- js NA
         * @method setTileSet
         * @param {cc.TMXTilesetInfo} _info
         */
        public setTileSet (
            _info : cc.TMXTilesetInfo 
        ) : void;

        /**
         *  Tileset information for the layer. <br>
         * -- return Tileset information for the layer.
         * @method getTileSet
         * @return {cc.TMXTilesetInfo}
         */
        public getTileSet (
        ) : cc.TMXTilesetInfo;

        /**
         *  Properties from the layer. They can be added using Tiled.<br>
         * -- return Properties from the layer. They can be added using Tiled.
         * @method getProperties
         * @return {any}
         */
        public getProperties (
        ) : any;

        /**
         *  Returns the tile (Sprite) at a given a tile coordinate.<br>
         * -- The returned Sprite will be already added to the TMXLayer. Don't add it again.<br>
         * -- The Sprite can be treated like any other Sprite: rotated, scaled, translated, opacity, color, etc.<br>
         * -- You can remove either by calling:<br>
         * -- - layer->removeChild(sprite, cleanup);<br>
         * -- - or layer->removeTileAt(Vec2(x,y));<br>
         * -- param tileCoordinate A tile coordinate.<br>
         * -- return Returns the tile (Sprite) at a given a tile coordinate.
         * @method getTileAt
         * @param {ctype.value_type<cc.Point>} _tileCoordinate
         * @return {cc.Sprite}
         */
        public getTileAt (
            _tileCoordinate : ctype.value_type<cc.Point> 
        ) : cc.Sprite;

        /**
         *  Creates a TMXLayer with an tileset info, a layer info and a map info.<br>
         * -- param tilesetInfo An tileset info.<br>
         * -- param layerInfo A layer info.<br>
         * -- param mapInfo A map info.<br>
         * -- return An autorelease object.
         * @method create
         * @param {cc.TMXTilesetInfo} _tilesetInfo
         * @param {cc.TMXLayerInfo} _layerInfo
         * @param {cc.TMXMapInfo} _mapInfo
         * @return {cc.TMXLayer}
         */
        public static create (
            _tilesetInfo : cc.TMXTilesetInfo, 
            _layerInfo : cc.TMXLayerInfo, 
            _mapInfo : cc.TMXMapInfo 
        ) : cc.TMXLayer;

        /**
         * js ctor
         * @method TMXLayer
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class TMXTiledMap
     * @native
     */
    export class TMXTiledMap 
        extends cc.Node
    {
        /**
         *  Properties. 
         * return Properties.
         * @method getProperties
         * @return {any}
         */
        public properties : any;
        /**
         *  Map orientation. 
         * return Map orientation.
         * @method getMapOrientation
         * @return {number}
         */
        public mapOrientation : number;
        /**
         * 
         * @method getObjectGroups
         * @return {cc.TMXObjectGroup[]}
         */
        public objectGroups : cc.TMXObjectGroup[];
        /* Can't find native property _getMapWidth implementation of cc::TMXTiledMap. using any type */
        public mapWidth : any;
        /* Can't find native property _getMapHeight implementation of cc::TMXTiledMap. using any type */
        public mapHeight : any;
        /* Can't find native property _getTileWidth implementation of cc::TMXTiledMap. using any type */
        public tileWidth : any;
        /* Can't find native property _getTileHeight implementation of cc::TMXTiledMap. using any type */
        public tileHeight : any;


        /**
         *  Set the object groups. <br>
         * -- param groups The object groups.
         * @method setObjectGroups
         * @param {cc.TMXObjectGroup[]} _groups
         */
        public setObjectGroups (
            _groups : cc.TMXObjectGroup[] 
        ) : void;

        /**
         *  Return the value for the specific property name. <br>
         * -- param propertyName The specific property name.<br>
         * -- return Return the value for the specific property name.
         * @method getProperty
         * @param {string} _propertyName
         * @return {cc.Value}
         */
        public getProperty (
            _propertyName : string 
        ) : cc.Value;

        /**
         * 
         * @method getLayerNum
         * @return {number}
         */
        public getLayerNum (
        ) : number;

        /**
         *  Set the map's size property measured in tiles. <br>
         * -- param mapSize The map's size property measured in tiles.
         * @method setMapSize
         * @param {ctype.value_type<cc.Size>} _mapSize
         */
        public setMapSize (
            _mapSize : ctype.value_type<cc.Size> 
        ) : void;

        /**
         *  Return the TMXObjectGroup for the specific group. <br>
         * -- param groupName The group Name.<br>
         * -- return A Type of TMXObjectGroup.
         * @method getObjectGroup
         * @param {string} _groupName
         * @return {cc.TMXObjectGroup}
         */
        public getObjectGroup (
            _groupName : string 
        ) : cc.TMXObjectGroup;

        /**
         * 
         * @method getObjectGroups
         * @return {cc.TMXObjectGroup[]}
         */
        public getObjectGroups (
        ) : cc.TMXObjectGroup[];

        /**
         * 
         * @method getResourceFile
         * @return {string}
         */
        public getResourceFile (
        ) : string;

        /**
         *  initializes a TMX Tiled Map with a TMX file 
         * @method initWithTMXFile
         * @param {string} _tmxFile
         * @return {boolean}
         */
        public initWithTMXFile (
            _tmxFile : string 
        ) : boolean;

        /**
         *  The tiles's size property measured in pixels. <br>
         * -- return The tiles's size property measured in pixels.
         * @method getTileSize
         * @return {ctype.value_type<cc.Size>}
         */
        public getTileSize (
        ) : ctype.value_type<cc.Size>;

        /**
         *  The map's size property measured in tiles. <br>
         * -- return The map's size property measured in tiles.
         * @method getMapSize
         * @return {ctype.value_type<cc.Size>}
         */
        public getMapSize (
        ) : ctype.value_type<cc.Size>;

        /**
         *  initializes a TMX Tiled Map with a TMX formatted XML string and a path to TMX resources 
         * @method initWithXML
         * @param {string} _tmxString
         * @param {string} _resourcePath
         * @return {boolean}
         */
        public initWithXML (
            _tmxString : string, 
            _resourcePath : string 
        ) : boolean;

        /**
         *  Properties. <br>
         * -- return Properties.
         * @method getProperties
         * @return {any}
         */
        public getProperties (
        ) : any;

        /**
         *  Assigns properties to argument value, returns true if it did found properties <br>
         * -- for that GID and did assigned a value, else it returns false.<br>
         * -- param GID The tile GID.<br>
         * -- param value Argument value.<br>
         * -- return Return true if it did found properties for that GID and did assigned a value, else it returns false.
         * @method getPropertiesForGID
         * @param {(number)} _GID
         * @param {(cc.Value)} _value?
         * @return {boolean | cc.Value}
         */
        public getPropertiesForGID (
            _GID : (number), 
            _value? : (cc.Value)
        ) : boolean | cc.Value;

        /**
         *  Set the tiles's size property measured in pixels. <br>
         * -- param tileSize The tiles's size property measured in pixels.
         * @method setTileSize
         * @param {ctype.value_type<cc.Size>} _tileSize
         */
        public setTileSize (
            _tileSize : ctype.value_type<cc.Size> 
        ) : void;

        /**
         *  Set the properties.<br>
         * -- param properties A  Type of ValueMap to set the properties.
         * @method setProperties
         * @param {any} _properties
         */
        public setProperties (
            _properties : any 
        ) : void;

        /**
         *  Return the TMXLayer for the specific layer. <br>
         * -- param layerName A specific layer.<br>
         * -- return The TMXLayer for the specific layer.
         * @method getLayer
         * @param {string} _layerName
         * @return {cc.TMXLayer}
         */
        public getLayer (
            _layerName : string 
        ) : cc.TMXLayer;

        /**
         *  Map orientation. <br>
         * -- return Map orientation.
         * @method getMapOrientation
         * @return {number}
         */
        public getMapOrientation (
        ) : number;

        /**
         *  Set map orientation. <br>
         * -- param mapOrientation The map orientation.
         * @method setMapOrientation
         * @param {number} _mapOrientation
         */
        public setMapOrientation (
            _mapOrientation : number 
        ) : void;

        /**
         *  Creates a TMX Tiled Map with a TMX file.<br>
         * -- param tmxFile A TMX file.<br>
         * -- return An autorelease object.
         * @method create
         * @param {string} _tmxFile
         * @return {cc.TMXTiledMap}
         */
        public static create (
            _tmxFile : string 
        ) : cc.TMXTiledMap;

        /**
         *  Initializes a TMX Tiled Map with a TMX formatted XML string and a path to TMX resources. <br>
         * -- param tmxString A TMX formatted XML string.<br>
         * -- param resourcePath The path to TMX resources.<br>
         * -- return An autorelease object.<br>
         * -- js NA
         * @method createWithXML
         * @param {string} _tmxString
         * @param {string} _resourcePath
         * @return {cc.TMXTiledMap}
         */
        public static createWithXML (
            _tmxString : string, 
            _resourcePath : string 
        ) : cc.TMXTiledMap;

        /**
         * js ctor
         * @method TMXTiledMap
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class TileMapAtlas
     * @native
     */
    export class TileMapAtlas 
        extends cc.AtlasNode
    {

        /**
         *  initializes a TileMap with a tile file (atlas) with a map file and the width and height of each tile in points.<br>
         * -- The file will be loaded using the TextureMgr.
         * @method initWithTileFile
         * @param {string} _tile
         * @param {string} _mapFile
         * @param {number} _tileWidth
         * @param {number} _tileHeight
         * @return {boolean}
         */
        public initWithTileFile (
            _tile : string, 
            _mapFile : string, 
            _tileWidth : number, 
            _tileHeight : number 
        ) : boolean;

        /**
         *  dealloc the map from memory 
         * @method releaseMap
         */
        public releaseMap (
        ) : void;

        /**
         * Query TGA image info.<br>
         * -- return The TGA image info.
         * @method getTGAInfo
         * @return {cc.sImageTGA}
         */
        public getTGAInfo (
        ) : cc.sImageTGA;

        /**
         * Returns a tile from position x,y.<br>
         * -- For the moment only channel R is used
         * @method getTileAt
         * @param {ctype.value_type<cc.Point>} _position
         * @return {ctype.value_type<cc.Color>}
         */
        public getTileAt (
            _position : ctype.value_type<cc.Point> 
        ) : ctype.value_type<cc.Color>;

        /**
         *  sets a tile at position x,y.<br>
         * -- For the moment only channel R is used
         * @method setTile
         * @param {ctype.value_type<cc.Color>} _tile
         * @param {ctype.value_type<cc.Point>} _position
         */
        public setTile (
            _tile : ctype.value_type<cc.Color>, 
            _position : ctype.value_type<cc.Point> 
        ) : void;

        /**
         * Set the TGA image info for TileMapAtlas<br>
         * -- param TGAInfo The TGA info in sImageTGA.
         * @method setTGAInfo
         * @param {cc.sImageTGA} _TGAInfo
         */
        public setTGAInfo (
            _TGAInfo : cc.sImageTGA 
        ) : void;

        /**
         *  creates a TileMap with a tile file (atlas) with a map file and the width and height of each tile in points.<br>
         * -- The tile file will be loaded using the TextureMgr.
         * @method create
         * @param {string} _tile
         * @param {string} _mapFile
         * @param {number} _tileWidth
         * @param {number} _tileHeight
         * @return {cc.TileMapAtlas}
         */
        public static create (
            _tile : string, 
            _mapFile : string, 
            _tileWidth : number, 
            _tileHeight : number 
        ) : cc.TileMapAtlas;

        /**
         * js ctor
         * @method TileMapAtlas
         * @constructor
         */
        public constructor (
        );

    }
    /**
     * @class SimpleAudioEngine
     * @native
     */
    export abstract class AudioEngine 
    {

        /**
         * Preload background music.<br>
         * -- param filePath The path of the background music file.<br>
         * -- js NA<br>
         * -- lua preloadMusic
         * @method preloadBackgroundMusic
         * @param {string} _filePath
         */
        public preloadMusic (
            _filePath : string 
        ) : void;

        /**
         * Stop playing background music.<br>
         * -- param releaseData If release the background music data or not.As default value is false.<br>
         * -- js stopMusic<br>
         * -- lua stopMusic
         * @method stopBackgroundMusic
         */
        public stopMusic (
        ) : void;

        /**
         * Stop all playing sound effects.
         * @method stopAllEffects
         */
        public stopAllEffects (
        ) : void;

        /**
         * The volume of the background music within the range of 0.0 as the minimum and 1.0 as the maximum.<br>
         * -- js getMusicVolume<br>
         * -- lua getMusicVolume
         * @method getBackgroundMusicVolume
         * @return {number}
         */
        public getMusicVolume (
        ) : number;

        /**
         * Resume playing background music.<br>
         * -- js resumeMusic<br>
         * -- lua resumeMusic
         * @method resumeBackgroundMusic
         */
        public resumeMusic (
        ) : void;

        /**
         * Set the volume of background music.<br>
         * -- param volume must be within the range of 0.0 as the minimum and 1.0 as the maximum.<br>
         * -- js setMusicVolume<br>
         * -- lua setMusicVolume
         * @method setBackgroundMusicVolume
         * @param {number} _volume
         */
        public setMusicVolume (
            _volume : number 
        ) : void;

        /**
         * Preload a compressed audio file.<br>
         * -- The compressed audio will be decoded to wave, then written into an internal buffer in SimpleAudioEngine.<br>
         * -- param filePath The path of the effect file.<br>
         * -- js NA
         * @method preloadEffect
         * @param {string} _filePath
         */
        public preloadEffect (
            _filePath : string 
        ) : void;

        /**
         * Indicates whether the background music is playing.<br>
         * -- return <i>true</i> if the background music is playing, otherwise <i>false</i>.<br>
         * -- js isMusicPlaying<br>
         * -- lua isMusicPlaying
         * @method isBackgroundMusicPlaying
         * @return {boolean}
         */
        public isMusicPlaying (
        ) : boolean;

        /**
         * The volume of the effects within the range of 0.0 as the minimum and 1.0 as the maximum.
         * @method getEffectsVolume
         * @return {number}
         */
        public getEffectsVolume (
        ) : number;

        /**
         * Indicates whether any background music can be played or not.<br>
         * -- return <i>true</i> if background music can be played, otherwise <i>false</i>.<br>
         * -- js willPlayMusic<br>
         * -- lua willPlayMusic
         * @method willPlayBackgroundMusic
         * @return {boolean}
         */
        public willPlayMusic (
        ) : boolean;

        /**
         * Pause playing sound effect.<br>
         * -- param soundId The return value of function playEffect.
         * @method pauseEffect
         * @param {number} _soundId
         */
        public pauseEffect (
            _soundId : number 
        ) : void;

        /**
         * Play sound effect with a file path, pitch, pan and gain.<br>
         * -- param filePath The path of the effect file.<br>
         * -- param loop Determines whether to loop the effect playing or not. The default value is false.<br>
         * -- param pitch Frequency, normal value is 1.0. Will also change effect play time.<br>
         * -- param pan   Stereo effect, in the range of [-1..1] where -1 enables only left channel.<br>
         * -- param gain  Volume, in the range of [0..1]. The normal value is 1.<br>
         * -- return The sound id.<br>
         * -- note Full support is under development, now there are limitations:<br>
         * -- - no pitch effect on Samsung Galaxy S2 with OpenSL backend enabled;<br>
         * -- - no pitch/pan/gain on win32.
         * @method playEffect
         * @param {string} _filePath
         * @param {boolean} _loop
         * @param {number} _pitch
         * @param {number} _pan
         * @param {number} _gain
         * @return {number}
         */
        public playEffect (
            _filePath : string, 
            _loop? : boolean, 
            _pitch? : number, 
            _pan? : number, 
            _gain? : number 
        ) : number;

        /**
         * Rewind playing background music.<br>
         * -- js rewindMusic<br>
         * -- lua rewindMusic
         * @method rewindBackgroundMusic
         */
        public rewindMusic (
        ) : void;

        /**
         * Play background music.<br>
         * -- param filePath The path of the background music file,or the FileName of T_SoundResInfo.<br>
         * -- param loop Whether the background music loop or not.<br>
         * -- js playMusic<br>
         * -- lua playMusic
         * @method playBackgroundMusic
         * @param {string} _filePath
         * @param {boolean} _loop
         */
        public playMusic (
            _filePath : string, 
            _loop : boolean 
        ) : void;

        /**
         * Resume all playing sound effect.
         * @method resumeAllEffects
         */
        public resumeAllEffects (
        ) : void;

        /**
         * Set the volume of sound effects.<br>
         * -- param volume must be within the range of 0.0 as the minimum and 1.0 as the maximum.
         * @method setEffectsVolume
         * @param {number} _volume
         */
        public setEffectsVolume (
            _volume : number 
        ) : void;

        /**
         * Stop playing sound effect.<br>
         * -- param soundId The return value of function playEffect.
         * @method stopEffect
         * @param {number} _soundId
         */
        public stopEffect (
            _soundId : number 
        ) : void;

        /**
         * Pause playing background music.<br>
         * -- js pauseMusic<br>
         * -- lua pauseMusic
         * @method pauseBackgroundMusic
         */
        public pauseMusic (
        ) : void;

        /**
         * Pause all playing sound effect.
         * @method pauseAllEffects
         */
        public pauseAllEffects (
        ) : void;

        /**
         * Unload the preloaded effect from internal buffer.<br>
         * -- param filePath The path of the effect file.
         * @method unloadEffect
         * @param {string} _filePath
         */
        public unloadEffect (
            _filePath : string 
        ) : void;

        /**
         * Resume playing sound effect.<br>
         * -- param soundId The return value of function playEffect.
         * @method resumeEffect
         * @param {number} _soundId
         */
        public resumeEffect (
            _soundId : number 
        ) : void;

        /**
         * Release the shared Engine object.<br>
         * -- warning It must be called before the application exit, or it will lead to memory leaks.<br>
         * -- lua destroyInstance
         * @method end
         */
        public static end (
        ) : void;

        /**
         * Returns a shared instance of the SimpleAudioEngine.<br>
         * -- js NA
         * @method getInstance
         * @return {cc.SimpleAudioEngine}
         */
        public static getInstance (
        ) : cc.SimpleAudioEngine;

    }
    /**
     * @class ComponentJS
     * @native
     */
    export abstract class ComponentJS 
        extends cc.Component
    {

    }
}