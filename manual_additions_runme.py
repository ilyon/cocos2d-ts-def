
import os
import json

current_path = os.path.abspath(os.path.join(os.path.dirname(__file__)))
manual_additions = os.path.join(current_path, 'manual_additions.json')
auto_additions = os.path.join(current_path, 'auto_props_api.json')

run_watermark = "// DONT CHANGE This File has been changed by another auto progress\n\n\n"

output_dir = os.path.join(current_path, "replaced")


def smart_class_def_finder(source, class_name, addition):
    source_lines = source.split('\n')
    for i in xrange(len(source_lines)):
        line = source_lines[i]
        
        if line.find(" class {0} ".format(class_name)) != -1:
            i += 1
            line = source_lines[i]
            if line.find(" extends") != -1:
                i += 1
                
            i += 1
            
            # Append line after {
            source_lines.insert(i, addition)
            return '\n'.join(source_lines)
            
    return source


def main():
    
    if not os.path.isdir(output_dir):
        os.mkdir(output_dir)
    
    inputs = [ auto_additions, manual_additions ]
    
    replacers = []
    for input in inputs:
        f = open(input)
        additions_json = json.loads(f.read())
        f.close()
    
        replacers += additions_json.get("replacers")
    
    sources = { }
    to_sources = { }
    
    for entry in replacers:
        source = entry.get("source")
        _to = entry.get("to")
        
        f = open(os.path.join(current_path, source))
        sources[source] = f.read().decode("utf-8")
        f.close()
        
        if os.path.isfile(_to):
            f = open(os.path.join(current_path, _to))
            to_sources[_to] = f.read().decode("utf-8")
            f.close()
        
    for entry in replacers:
        source = entry.get("source")
        _to = entry.get("to")
        _from = entry.get("from", None)
        _class = entry.get("class", None)
        
        if _to in to_sources:
            _to = to_sources[_to]
        
        content = sources[source]
        if _from is not None:
            sources[source] = content.replace(_from, _to)
        elif _class is not None:
            sources[source] = smart_class_def_finder(content, _class, _to)
    
    for source in sources:
        content = sources[source]
        if content.startswith(run_watermark):
            print "ERROR: Script run already on file: {0}".format(source)
            continue
        
        content = run_watermark + content
        
        out_file = os.path.join(output_dir, source)
        out_dir = os.path.dirname(out_file)
        
        if not os.path.isdir(out_dir):
            os.mkdir(out_dir)
        
        f = open(out_file, 'w')
        f.write(content.encode("utf-8"))
        f.close()
    
    print "SUCCESS"



if __name__ == '__main__':
    main()