        /**
         *  Properties. 
         * return Properties.
         * @method getProperties
         * @return {any}
         */
        public properties : any;
        /**
         *  Map orientation. 
         * return Map orientation.
         * @method getMapOrientation
         * @return {number}
         */
        public mapOrientation : number;
        /**
         * 
         * @method getObjectGroups
         * @return {cc.TMXObjectGroup[]}
         */
        public objectGroups : cc.TMXObjectGroup[];
        /* Can't find native property _getMapWidth implementation of cc::TMXTiledMap. using any type */
        public mapWidth : any;
        /* Can't find native property _getMapHeight implementation of cc::TMXTiledMap. using any type */
        public mapHeight : any;
        /* Can't find native property _getTileWidth implementation of cc::TMXTiledMap. using any type */
        public tileWidth : any;
        /* Can't find native property _getTileHeight implementation of cc::TMXTiledMap. using any type */
        public tileHeight : any;
