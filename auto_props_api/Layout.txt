        public static readonly BG_COLOR_NONE: number;
        public static readonly BG_COLOR_SOLID: number;
        public static readonly BG_COLOR_GRADIENT: number;
        public static readonly ABSOLUTE: number;
        public static readonly LINEAR_VERTICAL: number;
        public static readonly LINEAR_HORIZONTAL: number;
        public static readonly RELATIVE: number;
        public static readonly CLIPPING_STENCIL: number;
        public static readonly CLIPPING_SCISSOR: number;
        /**
         * Gets if layout is clipping enabled.
         * return if layout is clipping enabled.
         * @method isClippingEnabled
         * @return {boolean}
         */
        public clippingEnabled : boolean;
        /**
         * Change the clipping type of layout.
         * On default, the clipping type is `ClippingType::STENCIL`.
         * see `ClippingType`
         * param type The clipping type of layout.
         * @method setClippingType
         * @param {ccui.Layout::ClippingType} _type
         */
        public readonly clippingType : void;
        /**
         * Query layout type.
         * return Get the layout type.
         * @method getLayoutType
         * @return {number}
         */
        public layoutType : number;
