        public static readonly EVENT_SELECTED: number;
        public static readonly EVENT_UNSELECTED: number;
        /* Can't find native property getSelected implementation of ccui::CheckBox. using any type */
        public selected : any;
        /* Can't find native property _getAnchorX implementation of ccui::CheckBox. using any type */
        public anchorX : any;
        /* Can't find native property _getAnchorY implementation of ccui::CheckBox. using any type */
        public anchorY : any;
        /* Can't find native property isFlippedX implementation of ccui::CheckBox. using any type */
        public flippedX : any;
        /* Can't find native property isFlippedY implementation of ccui::CheckBox. using any type */
        public flippedY : any;
        /* Can't find native property _getWidth implementation of ccui::CheckBox. using any type */
        public width : any;
        /* Can't find native property _getHeight implementation of ccui::CheckBox. using any type */
        public height : any;
