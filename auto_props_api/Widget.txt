        public static readonly BRIGHT_STYLE_NORMAL: number;
        public static readonly BRIGHT_STYLE_HIGH_LIGHT: number;
        public static readonly TYPE_WIDGET: number;
        public static readonly TYPE_CONTAINER: number;
        public static readonly LOCAL_TEXTURE: number;
        public static readonly PLIST_TEXTURE: number;
        public static readonly TOUCH_BEGAN: number;
        public static readonly TOUCH_MOVED: number;
        public static readonly TOUCH_ENDED: number;
        public static readonly TOUCH_CANCELED: number;
        public static readonly SIZE_ABSOLUTE: number;
        public static readonly SIZE_PERCENT: number;
        public static readonly POSITION_ABSOLUTE: number;
        public static readonly POSITION_PERCENT: number;
        public static readonly LEFT: number;
        public static readonly RIGHT: number;
        public static readonly UP: number;
        public static readonly DOWN: number;
        /* Can't find native property _getXPercent implementation of ccui::Widget. using any type */
        public xPercent : any;
        /* Can't find native property _getYPercent implementation of ccui::Widget. using any type */
        public yPercent : any;
        /* Can't find native property _getWidthPercent implementation of ccui::Widget. using any type */
        public widthPercent : any;
        /* Can't find native property _getHeightPercent implementation of ccui::Widget. using any type */
        public heightPercent : any;
        /* Can't find native property getWidgetParent implementation of ccui::Widget. using any type */
        public readonly widgetParent : any;
        /**
         * Determines if the widget is enabled or not.
         * return true if the widget is enabled, false if the widget is disabled.
         * @method isEnabled
         * @return {boolean}
         */
        public enabled : boolean;
        /**
         * Query whether widget is focused or not.
         * return  whether the widget is focused or not
         * @method isFocused
         * @return {boolean}
         */
        public focused : boolean;
        /**
         * Gets the size type of widget.
         * see `SizeType`
         * @method getSizeType
         * @return {number}
         */
        public sizeType : number;
        /* Can't find native property getWidgetType implementation of ccui::Widget. using any type */
        public readonly widgetType : any;
        /**
         * Determines if the widget is touch enabled
         * return true if the widget is touch enabled, false if the widget is touch disabled.
         * @method isTouchEnabled
         * @return {boolean}
         */
        public touchEnabled : boolean;
        /* Can't find native property isUpdateEnabled implementation of ccui::Widget. using any type */
        public updateEnabled : any;
        /**
         * Determines if the widget is bright
         * return true if the widget is bright, false if the widget is dark.
         * @method isBright
         * @return {boolean}
         */
        public bright : boolean;
        /* Can't find native property getName implementation of ccui::Widget. using any type */
        public name : any;
        /**
         * Get the action tag.
         * return Action tag.
         * @method getActionTag
         * @return {number}
         */
        public actionTag : number;
        /* Can't find native property getPositionX implementation of ccui::Widget. using any type */
        public x : any;
        /* Can't find native property getPositionY implementation of ccui::Widget. using any type */
        public y : any;
        /* Can't find native property _getWidth implementation of ccui::Widget. using any type */
        public width : any;
        /* Can't find native property _getHeight implementation of ccui::Widget. using any type */
        public height : any;
        /**
         * Returns the flag which indicates whether the widget is flipped horizontally or not.
         * It not only flips the texture of the widget, but also the texture of the widget's children.
         * Also, flipping relies on widget's anchor point.
         * Internally, it just use setScaleX(-1) to flip the widget.
         * return true if the widget is flipped horizontally, false otherwise.
         * @method isFlippedX
         * @return {boolean}
         */
        public flippedX : boolean;
        /**
         * Return the flag which indicates whether the widget is flipped vertically or not.
         * It not only flips the texture of the widget, but also the texture of the widget's children.
         * Also, flipping relies on widget's anchor point.
         * Internally, it just use setScaleY(-1) to flip the widget.
         * return true if the widget is flipped vertically, false otherwise.
         * @method isFlippedY
         * @return {boolean}
         */
        public flippedY : boolean;
        /* Can't find native property getChildren implementation of ccui::Widget. using any type */
        public readonly children : any;
        /* Can't find native property getChildrenCount implementation of ccui::Widget. using any type */
        public readonly childrenCount : any;
