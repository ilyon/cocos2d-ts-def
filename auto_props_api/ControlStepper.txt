        /* Can't find native property getWraps implementation of cc::ControlStepper. using any type */
        public wraps : any;
        /**
         * 
         * @method getValue
         * @return {number}
         */
        public value : number;
        /* Can't find native property getMinimumValue implementation of cc::ControlStepper. using any type */
        public minValue : any;
        /* Can't find native property getMaximumValue implementation of cc::ControlStepper. using any type */
        public maxValue : any;
        /* Can't find native property getStepValue implementation of cc::ControlStepper. using any type */
        public stepValue : any;
        /**
         * 
         * @method isContinuous
         * @return {boolean}
         */
        public readonly continuous : boolean;
        /**
         * 
         * @method getMinusSprite
         * @return {cc.Sprite}
         */
        public minusSprite : cc.Sprite;
        /**
         * 
         * @method getPlusSprite
         * @return {cc.Sprite}
         */
        public plusSprite : cc.Sprite;
        /**
         * 
         * @method getMinusLabel
         * @return {cc.Label}
         */
        public minusLabel : cc.Label;
        /* Can't find native property getPlusSLabel implementation of cc::ControlStepper. using any type */
        public plusSLabel : any;
